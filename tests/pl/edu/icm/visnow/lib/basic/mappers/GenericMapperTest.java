//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import javax.swing.SwingUtilities;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.edu.icm.jscic.FieldType;

import pl.edu.icm.visnow.application.application.Application;
import pl.edu.icm.visnow.engine.commands.LinkAddCommand;
import pl.edu.icm.visnow.engine.core.LinkName;
import pl.edu.icm.visnow.gui.widgets.RunButton;
import pl.edu.icm.visnow.lib.basic.Argument;

import pl.edu.icm.visnow.system.utils.usermessage.UserMessage;

import pl.edu.icm.visnow.lib.basic.mappers.AnimatedStream.AnimatedStream;
import pl.edu.icm.visnow.lib.basic.mappers.Axes3D.Axes3D;
import pl.edu.icm.visnow.lib.basic.mappers.CellCenters.CellCenters;
import pl.edu.icm.visnow.lib.basic.mappers.CityPlot.CityPlot;
import pl.edu.icm.visnow.lib.basic.mappers.DiffusionStream.Streamlines;
// import pl.edu.icm.visnow.lib.basic.mappers.DitheredGlyphs.DitheredGlyphs; // Moved to experimental projects (common subdirectory) as of 2015/01/15
import pl.edu.icm.visnow.lib.basic.mappers.FieldMapper.FieldMapper;
import pl.edu.icm.visnow.lib.basic.mappers.Glyphs.Glyphs;
import pl.edu.icm.visnow.lib.basic.mappers.Graph.GraphObject;
import pl.edu.icm.visnow.lib.basic.mappers.Graph3D.Graph3D;
import pl.edu.icm.visnow.lib.basic.mappers.InterpolatedSlice.InterpolatedSlice;
import pl.edu.icm.visnow.lib.basic.mappers.Isolines.Isolines;
import pl.edu.icm.visnow.lib.basic.mappers.Isosurface.Isosurface;
import pl.edu.icm.visnow.lib.basic.mappers.Isovolume.Isovolume;
import pl.edu.icm.visnow.lib.basic.mappers.LineSlice.LineSlice;
import pl.edu.icm.visnow.lib.basic.mappers.ObjectFlow.ObjectFlow;
import pl.edu.icm.visnow.lib.basic.mappers.ParallelCoordinates.ParallelCoordinates;
import pl.edu.icm.visnow.lib.basic.mappers.PlanarSlice.PlanarSlice;
import pl.edu.icm.visnow.lib.basic.mappers.RegularFieldSlice.RegularFieldSlice;
import pl.edu.icm.visnow.lib.basic.mappers.RibbonPlot.RibbonPlot;
import pl.edu.icm.visnow.lib.basic.mappers.ScaleCells.ScaleCells;
import pl.edu.icm.visnow.lib.basic.mappers.SegmentedSurfaces.SegmentedSurfaces;
import pl.edu.icm.visnow.lib.basic.mappers.Skeletonizer.Skeletonizer;
import pl.edu.icm.visnow.lib.basic.mappers.SurfaceComponents.SurfaceComponents;
// import pl.edu.icm.visnow.lib.basic.mappers.TensorGlyphs.TensorGlyphs; // Moved to experimental projects (common subdirectory) as of 2015/01/15
import pl.edu.icm.visnow.lib.basic.mappers.TextGlyphs.TextGlyphs;
import pl.edu.icm.visnow.lib.basic.mappers.Trajectories.Trajectories;
import pl.edu.icm.visnow.lib.basic.mappers.TubeGlyphs.TubeGlyphs;
import pl.edu.icm.visnow.lib.basic.mappers.VectorFieldDisplacement.VectorFieldDisplacement;
import pl.edu.icm.visnow.lib.basic.mappers.VolumeRenderer.VolumeRenderer;
import pl.edu.icm.visnow.lib.basic.testdata.DevelopmentTestRegularField.Geometries.AffineOrthogonal;
import pl.edu.icm.visnow.lib.basic.testdata.TestIrregularFieldFlow.TestIrregularFieldFlow;
import pl.edu.icm.visnow.lib.basic.testdata.VNLogo.VNLogo;
import pl.edu.icm.visnow.lib.basic.viewers.Viewer2D.Viewer2D;

import pl.edu.icm.visnow.lib.utils.tests.application.SimpleApplication;

import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessageListener;

/**
 * A mapper test suite.
 * Used for generic testing of mappers in VisNow.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl)
 */
@RunWith(Parameterized.class)
public class GenericMapperTest
{

    private static final int SLEEP_TIME = 100;

    private static UserMessage currentErrorMessage = null;
    private static Argument currentArgument = null;

    public GenericMapperTest(Argument arg)
    {
        this.currentArgument = arg;
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Parameterized.Parameters
    public static Collection<Argument[]> module()
    {
        Argument[][] parameters = {
            {new Argument(AnimatedStream.class, "inField", FieldType.FIELD_IRREGULAR, 1, TestIrregularFieldFlow.class.getName())},
            {new Argument(Axes3D.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(Axes3D.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Axes3D.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(CellCenters.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(CityPlot.class, "inField", FieldType.FIELD_REGULAR, 2)}, // {new Argument(DitheredGlyphs.class, "inField", FieldType.FIELD_REGULAR, 3)}, // Moved to experimental projects (common subdirectory) as of 2015/01/15
            {new Argument(FieldMapper.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(FieldMapper.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(FieldMapper.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(FieldMapper.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(Glyphs.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(Glyphs.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Glyphs.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Glyphs.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(GraphObject.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(Graph3D.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(InterpolatedSlice.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(InterpolatedSlice.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(Isolines.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Isolines.class, "inField", FieldType.FIELD_IRREGULAR, 2)},
            {new Argument(Isosurface.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Isosurface.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(Isovolume.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(Isovolume.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Isovolume.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Isovolume.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(LineSlice.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(LineSlice.class, "inField", FieldType.FIELD_REGULAR, 3)},
            //{new Argument(ObjectFlow.class, "inField", FieldType.FIELD_REGULAR, 1)}, //requires time-dependent field
            //{new Argument(ObjectFlow.class, "inField", FieldType.FIELD_REGULAR, 2)}, //requires time-dependent field
            //{new Argument(ObjectFlow.class, "inField", FieldType.FIELD_REGULAR, 3)}, //requires time-dependent field
            {new Argument(ObjectFlow.class, "inField", FieldType.FIELD_IRREGULAR, 3, TestIrregularFieldFlow.class.getName())},
            {new Argument(ParallelCoordinates.class, "inField", FieldType.FIELD_REGULAR, 1, Argument.TEST_REGULAR_FIELD_MODULE_NAME, Viewer2D.class.getName())},
            {new Argument(ParallelCoordinates.class, "inField", FieldType.FIELD_REGULAR, 2, Argument.TEST_REGULAR_FIELD_MODULE_NAME, Viewer2D.class.getName())},
            {new Argument(ParallelCoordinates.class, "inField", FieldType.FIELD_REGULAR, 3, Argument.TEST_REGULAR_FIELD_MODULE_NAME, Viewer2D.class.getName())},
            {new Argument(ParallelCoordinates.class, "inField", FieldType.FIELD_IRREGULAR, 3, Argument.TEST_IRREGULAR_FIELD_MODULE_NAME, Viewer2D.class.getName())},
            {new Argument(PlanarSlice.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(RegularFieldSlice.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(RibbonPlot.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(ScaleCells.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(ScaleCells.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(ScaleCells.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(ScaleCells.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(SegmentedSurfaces.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Skeletonizer.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Skeletonizer.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Streamlines.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Streamlines.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(SurfaceComponents.class, "inField", FieldType.FIELD_IRREGULAR, 3, VNLogo.class.getName())},
            // {new Argument(TensorGlyphs.class, "inField", FieldType.FIELD_REGULAR, 3)},  // Moved to experimental projects (common subdirectory) as of 2015/01/15
            {new Argument(TextGlyphs.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(TextGlyphs.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(TextGlyphs.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(TextGlyphs.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            //{new Argument(Trajectories.class, "inField", FieldType.FIELD_REGULAR, 1)}, //requires time-dependent field
            //{new Argument(Trajectories.class, "inField", FieldType.FIELD_REGULAR, 2)}, //requires time-dependent field
            //{new Argument(Trajectories.class, "inField", FieldType.FIELD_REGULAR, 3)}, //requires time-dependent field
            {new Argument(Trajectories.class, "inField", FieldType.FIELD_IRREGULAR, 3, TestIrregularFieldFlow.class.getName())},
            {new Argument(TubeGlyphs.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(VectorFieldDisplacement.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(VectorFieldDisplacement.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(VectorFieldDisplacement.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(VectorFieldDisplacement.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(VolumeRenderer.class, "inField", FieldType.FIELD_REGULAR, 3)}};
        return Arrays.asList(parameters);
    }

    @Test
    public void genericMapperTest()
    {
        System.out.println("Testing: " + currentArgument.getModuleName());

        try {
            VisNow.mainBlocking(new String[]{}, true);
            VisNow.initLogging(true);
            VisNow.get().addUserMessageListener(new UserMessageListener()
            {
                @Override
                public void newMessage(UserMessage message)
                {
                    if (message.getLevel() == Level.ERROR) {
                        currentErrorMessage = message;
                    } else {
                        currentErrorMessage = null;
                    }
                }
            });

            Application application = new Application("Test of " + currentArgument.getModuleSimpleName(), true);
            VisNow.get().getMainWindow().getApplicationsPanel().addApplication(application);

            String viewerModuleVNName = SimpleApplication.addModule(application, Argument.VIEWER_MODULE_NAME, 110);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            String currentModuleVNName = SimpleApplication.addModule(application, currentArgument.getModuleName(), 60);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(currentModuleVNName, "outObj", viewerModuleVNName, "inObject"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            } catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(currentModuleVNName, "outObj", viewerModuleVNName, "inObject"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            }

            // Add module genering test data (and do some parameter tuning in case of a regular field).
            String testModuleVNName = null;
            if (currentArgument.getFieldType() == FieldType.FIELD_REGULAR) {
                testModuleVNName = SimpleApplication.addModule(application, currentArgument.getTestFieldModuleName(), 10);
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                final pl.edu.icm.visnow.engine.core.Parameters p = application.getEngine().getModule(testModuleVNName).getCore().getParameters();
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try {
                            // p.set(TestRegularFieldShared.DIMENSION_LENGTH, 2);
                            int currentDimension = currentArgument.getDimension();
                            switch (currentDimension) {
                                case 3:
                                    p.setValue("dims", new int[]{12, 5, 3});
                                    break;
                                case 2:
                                    p.setValue("dims", new int[]{3, 10});
                                    break;
                                case 1:
                                    p.setValue("dims", new int[]{7});
                                    break;
                                default:
                                    throw new RuntimeException("Seems like argument provided dimension != 1, 2 or 3. No other dimensions are supported.");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            fail(currentArgument.getModuleSimpleName() + ": " + ex.toString());
                        }
                        p.setValue("geometryClassName", AffineOrthogonal.class.getSimpleName());
                    }
                });
                currentErrorMessage = null;
                p.fireStateChanged();
                Thread.sleep(SLEEP_TIME);
            } else {
                testModuleVNName = SimpleApplication.addModule(application, currentArgument.getTestFieldModuleName(), 10);
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                currentErrorMessage = null;
            }

            // Add final link and run the module in case Run Button is present.
            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(testModuleVNName, "outField", currentModuleVNName, currentArgument.getInputPortName()), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            } catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(testModuleVNName, "outField", currentModuleVNName, currentArgument.getInputPortName()), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            }

            final pl.edu.icm.visnow.engine.core.Parameters p = application.getEngine().getModule(currentModuleVNName).getCore().getParameters();
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    HashMap<String, pl.edu.icm.visnow.engine.core.Parameter> map = p.getParameters();
                    if (map.containsKey("Running message"))
                        p.setValue("Running message", RunButton.RunState.RUN_DYNAMICALLY);
                }
            });

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            VisNow.get().getMainWindow().getApplicationsPanel().removeApplication(application, true);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail(currentArgument.getModuleSimpleName() + ": " + ex.toString());
        }
        assertFalse(currentErrorMessage + "", currentErrorMessage != null);
    }

}
