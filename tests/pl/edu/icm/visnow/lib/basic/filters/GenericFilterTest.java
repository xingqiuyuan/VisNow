//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters;

import java.lang.reflect.InvocationTargetException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.runners.Parameterized;
import org.junit.runner.RunWith;
import pl.edu.icm.jscic.FieldType;

import pl.edu.icm.visnow.application.application.Application;
import pl.edu.icm.visnow.engine.commands.LinkAddCommand;
import pl.edu.icm.visnow.engine.core.LinkName;
import pl.edu.icm.visnow.gui.widgets.RunButton;
import pl.edu.icm.visnow.lib.basic.Argument;

import pl.edu.icm.visnow.lib.utils.tests.application.SimpleApplication;

import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessage;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessageListener;

import pl.edu.icm.visnow.lib.basic.filters.AccumulateIrregularFields.FieldAccumulator;
import pl.edu.icm.visnow.lib.basic.filters.AccumulateTimesteps.AccumulateTimesteps;
import pl.edu.icm.visnow.lib.basic.filters.AddMargins.AddMargins;
import pl.edu.icm.visnow.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiser;
import pl.edu.icm.visnow.lib.basic.filters.Boundaries.Boundaries;
import pl.edu.icm.visnow.lib.basic.filters.Canny.Canny;
import pl.edu.icm.visnow.lib.basic.filters.CellToNode.CellToNode;
import pl.edu.icm.visnow.lib.basic.filters.ComponentCalculator.ComponentCalculator;
import pl.edu.icm.visnow.lib.basic.filters.ComponentOperations.ComponentOperations;
import pl.edu.icm.visnow.lib.basic.filters.CropDown.CropDown;
import pl.edu.icm.visnow.lib.basic.filters.DropCoords.DropCoords;
import pl.edu.icm.visnow.lib.basic.filters.FFT.FFT;
import pl.edu.icm.visnow.lib.basic.filters.GaussianFilter.GaussianFilter;
import pl.edu.icm.visnow.lib.basic.filters.IntensityEqualizer.IntensityEqualizer;
import pl.edu.icm.visnow.lib.basic.filters.InterpolateToMesh.InterpolateToMesh;
import pl.edu.icm.visnow.lib.basic.filters.InterpolationToRegularField.InterpolationToRegularField;
import pl.edu.icm.visnow.lib.basic.filters.LocalOperations.LocalOperations;
import pl.edu.icm.visnow.lib.basic.filters.MulticomponentHistogram.MulticomponentHistogram;
import pl.edu.icm.visnow.lib.basic.filters.PeronaMalikDiffusion.PeronaMalikDiffusion;
import pl.edu.icm.visnow.lib.basic.filters.RadialCoordinates.RadialCoordinates;
import pl.edu.icm.visnow.lib.basic.filters.RegionComponents.RegionComponents;
import pl.edu.icm.visnow.lib.basic.filters.RegularFieldDifferentialOperations.RegularFieldDifferentialOperations;
import pl.edu.icm.visnow.lib.basic.filters.RegularToIrregularField.RegularToIrregularField;
import pl.edu.icm.visnow.lib.basic.filters.SimpleProjection.SimpleProjection;
import pl.edu.icm.visnow.lib.basic.filters.SmoothDown.SmoothDown;
import pl.edu.icm.visnow.lib.basic.filters.SplineInterpolation.SplineInterpolation;
import pl.edu.icm.visnow.lib.basic.filters.Triangulation.Triangulation;
import pl.edu.icm.visnow.lib.basic.testdata.DevelopmentTestRegularField.Geometries.AffineOrthogonal;
import pl.edu.icm.visnow.lib.basic.viewers.Viewer3D.Viewer3D;

/**
 * A filter test suite.
 * Used for generic testing of filters in VisNow.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl)
 */
@RunWith(Parameterized.class)
public class GenericFilterTest
{

    private static final String VIEWER_MODULE_NAME = Viewer3D.class.getName();
    private static final int SLEEP_TIME = 100;

    private static UserMessage currentErrorMessage = null;
    private static Argument currentArgument = null;

    public GenericFilterTest(Argument arg)
    {
        this.currentArgument = arg;
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
        currentErrorMessage = null;
    }

    @After
    public void tearDown()
    {
    }

    @Parameterized.Parameters
    public static Collection<Argument[]> module()
    {
        Argument[][] parameters = {
            {new Argument(FieldAccumulator.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(AccumulateTimesteps.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(AccumulateTimesteps.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(AccumulateTimesteps.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(AccumulateTimesteps.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(AddMargins.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(AddMargins.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(AddMargins.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(AnisotropicDenoiser.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Boundaries.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(Canny.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Canny.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(CellToNode.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(ComponentCalculator.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(ComponentCalculator.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(ComponentCalculator.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(ComponentCalculator.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(ComponentOperations.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(ComponentOperations.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(ComponentOperations.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(ComponentOperations.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            //{new Argument(Convolution.class, "inFieldData", FieldType.FIELD_REGULAR, 3)}, //Two input ports
            // {new Argument(CorrelationAnalysis.class, "inField", FieldType.FIELD_REGULAR)}, // Complicated, but still testable. No geometry output.
            {new Argument(CropDown.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(CropDown.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(CropDown.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(DropCoords.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(DropCoords.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(DropCoords.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(FFT.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(FFT.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(FFT.class, "inField", FieldType.FIELD_REGULAR, 3)},
            // {new Argument(FieldCombiner.class, "inFields", FieldType.FIELD_REGULAR)}, // No geometry output.
            {new Argument(GaussianFilter.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(GaussianFilter.class, "inField", FieldType.FIELD_REGULAR, 3)},
            // {new Argument(GroupTransform.class, "inField", FieldType.FIELD_REGULAR)}, // Only geometry input. No input ports!
            {new Argument(IntensityEqualizer.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(IntensityEqualizer.class, "inField", FieldType.FIELD_REGULAR, 3)},
            //{new Argument(InterpolateToMesh.class, "inField", FieldType.FIELD_REGULAR, 2)}, //requires nSpace 2
            {new Argument(InterpolateToMesh.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(InterpolateToMesh.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(InterpolationToRegularField.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            {new Argument(LocalOperations.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(LocalOperations.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(MulticomponentHistogram.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(MulticomponentHistogram.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(MulticomponentHistogram.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(MulticomponentHistogram.class, "inField", FieldType.FIELD_IRREGULAR, 3)},
            // {new Argument(MultiVolumeSegmentation.class, "inField", FieldType.FIELD_REGULAR)}, // No geometry output.
            {new Argument(PeronaMalikDiffusion.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(PeronaMalikDiffusion.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(PeronaMalikDiffusion.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(RadialCoordinates.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(RadialCoordinates.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(RadialCoordinates.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(RegionComponents.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(RegionComponents.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(RegionComponents.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(RegularFieldDifferentialOperations.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(RegularFieldDifferentialOperations.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(RegularToIrregularField.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(RegularToIrregularField.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(RegularToIrregularField.class, "inField", FieldType.FIELD_REGULAR, 3)},
            // {new Argument(ReindexRegularField.class, "inField", FieldType.FIELD_REGULAR)}, // No geometry output.
            // {new Argument(SegmentationMask.class, "inField", FieldType.FIELD_REGULAR)}, // No geometry output.
            {new Argument(SimpleProjection.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(SimpleProjection.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(SmoothDown.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(SmoothDown.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(SplineInterpolation.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(SplineInterpolation.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(SplineInterpolation.class, "inField", FieldType.FIELD_REGULAR, 3)},
            // {new Argument(TransformGeometry.class, "inField", FieldType.FIELD_REGULAR)}, // No geometry output.
            {new Argument(Triangulation.class, "inField", FieldType.FIELD_REGULAR, 1)},
            {new Argument(Triangulation.class, "inField", FieldType.FIELD_REGULAR, 2)},
            {new Argument(Triangulation.class, "inField", FieldType.FIELD_REGULAR, 3)},
            {new Argument(Triangulation.class, "inField", FieldType.FIELD_IRREGULAR, 3)}
        // {new Argument(VolumeSegmentation.class, "inField", FieldType.FIELD_REGULAR)}, // Complicated, but still testable. No geometry output.
        };
        return Arrays.asList(parameters);
    }

    @Test
    public void genericFilterTest() throws InterruptedException, InvocationTargetException
    {
        System.out.println("Testing: " + currentArgument.getModuleName());

        try {
            VisNow.mainBlocking(new String[]{}, true);
            VisNow.initLogging(true);
            VisNow.get().addUserMessageListener(new UserMessageListener()
            {
                @Override
                public void newMessage(UserMessage message)
                {
                    if (message.getLevel() == Level.ERROR)
                        currentErrorMessage = message;
                }
            });

            Application application = new Application("Test of " + currentArgument.getModuleSimpleName(), true);
            VisNow.get().getMainWindow().getApplicationsPanel().addApplication(application);

            String viewerModuleVNName = SimpleApplication.addModule(application, VIEWER_MODULE_NAME, 110);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }
            String currentModuleVNName = SimpleApplication.addModule(application, currentArgument.getModuleName(), 60);
            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(currentModuleVNName, "outObj", viewerModuleVNName, "inObject"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            } catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(currentModuleVNName, "outObj", viewerModuleVNName, "inObject"), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            }
            // Add module genering test data (and do some parameter tuning in case of a regular field).
            String testModuleVNName = null;
            if (currentArgument.getFieldType() == FieldType.FIELD_REGULAR) {
                testModuleVNName = SimpleApplication.addModule(application, currentArgument.getTestFieldModuleName(), 10);
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
                final pl.edu.icm.visnow.engine.core.Parameters p = application.getEngine().getModule(testModuleVNName).getCore().getParameters();
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try {
                            // p.set(TestRegularFieldShared.DIMENSION_LENGTH, 2);
                            int currentDimension = currentArgument.getDimension();
                            switch (currentDimension) {
                                case 3:
                                    p.setValue("dims", new int[]{12, 5, 3});
                                    break;
                                case 2:
                                    p.setValue("dims", new int[]{3, 10});
                                    break;
                                case 1:
                                    p.setValue("dims", new int[]{7});
                                    break;
                                default:
                                    throw new RuntimeException("Seems like argument provided dimension != 1, 2 or 3. No other dimensions are supported.");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            fail(currentArgument.getModuleSimpleName() + ": " + ex.toString());
                        }
                        p.setValue("geometryClassName", AffineOrthogonal.class.getSimpleName());
                    }
                });
                currentErrorMessage = null;
                p.fireStateChanged();
                Thread.sleep(SLEEP_TIME);
            } else {
                testModuleVNName = SimpleApplication.addModule(application, currentArgument.getTestFieldModuleName(), 10);
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            }

            // Add final link and run the module in case Run Button is present.
            try {
                application.getReceiver().receive(new LinkAddCommand(new LinkName(testModuleVNName, "outField", currentModuleVNName, currentArgument.getInputPortName()), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            }
            catch (Throwable ex) {
                Thread.sleep(10000);
                application.getReceiver().receive(new LinkAddCommand(new LinkName(testModuleVNName, "outField", currentModuleVNName, currentArgument.getInputPortName()), true));
                while (application.isLockupBusy()) {
                    Thread.sleep(SLEEP_TIME);
                }
            }

            final pl.edu.icm.visnow.engine.core.Parameters p = application.getEngine().getModule(currentModuleVNName).getCore().getParameters();
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    HashMap<String, pl.edu.icm.visnow.engine.core.Parameter> map = p.getParameters();
                    if (map.containsKey("Running message"))
                        p.setValue("Running message", RunButton.RunState.RUN_DYNAMICALLY);
                }
            });

            while (application.isLockupBusy()) {
                Thread.sleep(SLEEP_TIME);
            }

            VisNow.get().getMainWindow().getApplicationsPanel().removeApplication(application, true);
        } catch (Exception ex) {
            ex.printStackTrace();
            fail(currentArgument.getModuleSimpleName() + ": " + ex.toString());
        }
        assertFalse(currentErrorMessage + "", currentErrorMessage != null);
    }
}
