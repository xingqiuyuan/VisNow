/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package doc;

import java.util.HashMap;
import java.util.Map;
import pl.edu.icm.visnow.viscript.ViscriptStandardMethods;
import pl.edu.icm.jscic.dataarrays.DataArray;

public class MyScript extends pl.edu.icm.visnow.viscript.CompiledViscript
{

    @Override
    public Map<String, Object> run(Map<String, Object> bindings)
    {
        Map<String, Object> outputBindings = new HashMap<String, Object>();

        float[] linear2 = ((DataArray) bindings.get("linear2")).getRawFloatArray().getData();
        int _linear2_dims_0 = ((int[]) bindings.get("linear2_dims"))[0];
        int _linear2_dims_1 = ((int[]) bindings.get("linear2_dims"))[1];
        int _linear2_dims_2 = ((int[]) bindings.get("linear2_dims"))[2];
        float[] dipole = ((DataArray) bindings.get("dipole")).getRawFloatArray().getData();
        int _dipole_dims_0 = ((int[]) bindings.get("dipole_dims"))[0];
        int _dipole_dims_1 = ((int[]) bindings.get("dipole_dims"))[1];
        int _dipole_dims_2 = ((int[]) bindings.get("dipole_dims"))[2];
        float[] vortex = ((DataArray) bindings.get("vortex")).getRawFloatArray().getData();
        int _vortex_dims_0 = ((int[]) bindings.get("vortex_dims"))[0];
        int _vortex_dims_1 = ((int[]) bindings.get("vortex_dims"))[1];
        int _vortex_dims_2 = ((int[]) bindings.get("vortex_dims"))[2];
        float[] semielipsoid = ((DataArray) bindings.get("semielipsoid")).getRawFloatArray().getData();
        int _semielipsoid_dims_0 = ((int[]) bindings.get("semielipsoid_dims"))[0];
        int _semielipsoid_dims_1 = ((int[]) bindings.get("semielipsoid_dims"))[1];
        int _semielipsoid_dims_2 = ((int[]) bindings.get("semielipsoid_dims"))[2];
        float[] linear = ((DataArray) bindings.get("linear")).getRawFloatArray().getData();
        int _linear_dims_0 = ((int[]) bindings.get("linear_dims"))[0];
        int _linear_dims_1 = ((int[]) bindings.get("linear_dims"))[1];
        int _linear_dims_2 = ((int[]) bindings.get("linear_dims"))[2];
        float[] ident = ((DataArray) bindings.get("ident")).getRawFloatArray().getData();
        int _ident_dims_0 = ((int[]) bindings.get("ident_dims"))[0];
        int _ident_dims_1 = ((int[]) bindings.get("ident_dims"))[1];
        int _ident_dims_2 = ((int[]) bindings.get("ident_dims"))[2];
        float[] trig_function = ((DataArray) bindings.get("trig_function")).getRawFloatArray().getData();
        int _trig_function_dims_0 = ((int[]) bindings.get("trig_function_dims"))[0];
        int _trig_function_dims_1 = ((int[]) bindings.get("trig_function_dims"))[1];
        int _trig_function_dims_2 = ((int[]) bindings.get("trig_function_dims"))[2];
        float[] Hopf = ((DataArray) bindings.get("Hopf")).getRawFloatArray().getData();
        int _Hopf_dims_0 = ((int[]) bindings.get("Hopf_dims"))[0];
        int _Hopf_dims_1 = ((int[]) bindings.get("Hopf_dims"))[1];
        int _Hopf_dims_2 = ((int[]) bindings.get("Hopf_dims"))[2];
        float[] gaussians_bits = ((DataArray) bindings.get("gaussians_bits")).getRawFloatArray().getData();
        int _gaussians_bits_dims_0 = ((int[]) bindings.get("gaussians_bits_dims"))[0];
        int _gaussians_bits_dims_1 = ((int[]) bindings.get("gaussians_bits_dims"))[1];
        int _gaussians_bits_dims_2 = ((int[]) bindings.get("gaussians_bits_dims"))[2];
        float[] gaussians = ((DataArray) bindings.get("gaussians")).getRawFloatArray().getData();
        int _gaussians_dims_0 = ((int[]) bindings.get("gaussians_dims"))[0];
        int _gaussians_dims_1 = ((int[]) bindings.get("gaussians_dims"))[1];
        int _gaussians_dims_2 = ((int[]) bindings.get("gaussians_dims"))[2];

        int _tab_dims_0 = (int) (_gaussians_dims_0);
        int _tab_dims_1 = (int) (_gaussians_dims_1);
        int _tab_dims_2 = (int) (_gaussians_dims_2);
        float[] tab = new float[_tab_dims_0 * _tab_dims_1 * _tab_dims_2];
        ;
        for (int _x = 0; _x < (_tab_dims_0); _x++)
            for (int _y = 0; _y < (_tab_dims_1); _y++)
                for (int _z = 0; _z < (_tab_dims_2); _z++) {
                    int loopTempOffset = (_x) + (_y) * (_tab_dims_0) + (_z) * (_tab_dims_0) * (_tab_dims_1);
                    tab[loopTempOffset] = ((gaussians[loopTempOffset] * gaussians[loopTempOffset]) + trig_function[loopTempOffset]);
                }
        ;
        ;
        outputBindings.put("tab", tab);
        outputBindings.put("_dims", new int[]{
            _tab_dims_0, _tab_dims_1, _tab_dims_2
        });

        return outputBindings;
    }
}
