//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.geometries.utils;

import static org.apache.commons.math3.util.FastMath.*;
import javax.vecmath.Color3f;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.datamaps.ColorMapManager;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.dataarrays.ByteDataArray;
import pl.edu.icm.jscic.dataarrays.ComplexDataArray;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.dataarrays.DataObjectInterface;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.ObjectLargeArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.visnow.geometries.parameters.ComponentColorMap;
import pl.edu.icm.visnow.geometries.parameters.ColorComponentParams;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.TransparencyParams;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ColorMapper
{

    public static void hsvtorgb(byte[] colors)
    {
        float hue, sat, val, f, p, q, t;
        int m;
        for (int i = 0; i < colors.length; i += 3) {
            hue = (0xff & colors[i]) / 42.6f;
            sat = (0xff & colors[i + 1]) / 255f;
            val = (0xff & colors[i + 2]) / 255f;
            m = (int) (floor(hue));
            f = hue - m;
            p = val * (1 - sat);
            q = val * (1 - (sat * f));
            t = val * (1 - (sat * (1 - f)));
            switch (m) {
                case 0:
                    colors[i] = (byte) (0xff & (int) (255 * val));
                    colors[i + 1] = (byte) (0xff & (int) (255 * t));
                    colors[i + 2] = (byte) (0xff & (int) (255 * p));
                    break;
                case 1:
                    colors[i] = (byte) (0xff & (int) (255 * q));
                    colors[i + 1] = (byte) (0xff & (int) (255 * val));
                    colors[i + 2] = (byte) (0xff & (int) (255 * p));
                    break;
                case 2:
                    colors[i] = (byte) (0xff & (int) (255 * p));
                    colors[i + 1] = (byte) (0xff & (int) (255 * val));
                    colors[i + 2] = (byte) (0xff & (int) (255 * t));
                    break;
                case 3:
                    colors[i] = (byte) (0xff & (int) (255 * p));
                    colors[i + 1] = (byte) (0xff & (int) (255 * q));
                    colors[i + 2] = (byte) (0xff & (int) (255 * val));
                    break;
                case 4:
                    colors[i] = (byte) (0xff & (int) (255 * t));
                    colors[i + 1] = (byte) (0xff & (int) (255 * p));
                    colors[i + 2] = (byte) (0xff & (int) (255 * val));
                    break;
                case 5:
                    colors[i] = (byte) (0xff & (int) (255 * val));
                    colors[i + 1] = (byte) (0xff & (int) (255 * p));
                    colors[i + 2] = (byte) (0xff & (int) (255 * q));
                    break;
            }
        }
    }

    public static byte[] mapByteColor(float[] data, ComponentColorMap cMap)
    {
        byte[] colorMapLUT = cMap.getRGBByteColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        byte[] colors = new byte[4];
        colors[3] = (byte) 0xff;
        double v = 0;
        int cIndex = 0;
        if (data.length == 1)
            v = data[0];
        else {
            v = 0;
            for (int j = 0; j < data.length; j++)
                v += data[j] * data[j];
            v = sqrt(v);
        }
        if (!cMap.isWrap())
            cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
        else
            cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
        for (int j = 0; j < 3; j++)
            colors[j] = colorMapLUT[cIndex + j];
        return colors;
    }

    public static int mapIntColor(float[] data, ComponentColorMap cMap)
    {
        byte[] bc = mapByteColor(data, cMap);
        return bc[0] << 16 | bc[1] << 8 | bc[2] | 0x7f << 24;
    }

    public static byte[] modByteColorSaturationValue(float[] data, float low, float mlow, float mup, float d, boolean modSat, byte[] color)
    {
        byte[] colors = new byte[4];
        colors[3] = (byte) 0xff;
        double u;
        int vl = data.length;
        if (vl == 1)
            u = data[0] - low;
        else {
            u = 0;
            for (int j = 0; j < vl; j++)
                u += data[j] * data[j];
            u = mlow + d * sqrt(u);
        }
        u = max(mlow, min(mup, u));

        int[] c = new int[]{0xff & color[0], 0xff & color[1], 0xff & color[2]};
        if (modSat) {
            int m = 0;
            for (int i = 0; i < 3; i++)
                if (m < c[i])
                    m = c[i];
            for (int i = 0; i < 3; i++)
                colors[i] = (byte) (0xff & (int) (m - u * (m - c[i])));
        } else if (u <= 1)
            for (int i = 0; i < 3; i++)
                colors[i] = (byte) (0xff & (int) (u * c[i]));
        else {
            double w = Math.min(1, Math.max(0, 2 - u));
            for (int i = 0; i < 3; i++)
                colors[i] = (byte) (0xff & (int) (255 - w * (255 - c[i])));
        }
        return colors;
    }

    public static int modIntColorSaturationValue(float[] data, float low, float mlow, float mup, float d, boolean modSat, int intColor)
    {
        byte[] bc0 = new byte[]{(byte) (0xff & (intColor >> 16)),
                                (byte) (0xff & (intColor >> 8)),
                                (byte) (0xff & intColor),
                                (byte) 0xff};
        byte[] bc = modByteColorSaturationValue(data, low, mlow, mup, d, modSat, bc0);
        return bc[0] << 16 | bc[1] << 8 | bc[2] | 0x7f << 24;
    }

    public static byte[] blendColors(float[] data, ComponentColorMap cMap, byte[] col, float blendRatio)
    {
        byte[] colorMapLUT = cMap.getRGBByteColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        double v = 0;
        int cIndex = 0;
        if (data.length == 1)
            v = data[0];
        else {
            v = 0;
            for (int j = 0; j < data.length; j++)
                v += data[j] * data[j];
            v = sqrt(v);
        }
        if (!cMap.isWrap())
            cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
        else
            cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
        byte[] colors = new byte[4];
        for (int j = 0; j < 3; j++)
            colors[j] = (byte) (0xff & (int) ((1 - blendRatio) * (0xff & col[j]) +
                blendRatio * (0xff & colorMapLUT[cIndex + j])));
        colors[3] = (byte) 0xff;
        return colors;
    }

    public static byte mapTransparency(float[] data, float d, float low, byte[] tMap)
    {
        double v = 0;
        if (data.length == 1)
            v = data[0];
        else {
            v = 0;
            for (int j = 0; j < data.length; j++)
                v += data[j] * data[j];
            v = sqrt(v);
        }
        int tIndex = (int) (d * (v - low));
        if (tIndex < 0)
            tIndex = 0;
        if (tIndex > 255)
            tIndex = 255;
        return tMap[tIndex];
    }

    public static void mapArrayPart(float[] colorMappedData, int colorMappedDataVlen, float[] colorModData, int colorModDataVlen, float[] transpData, int transpDataVlen,
                                    byte[] mask, DataMappingParams dataMappingParams, int start, int end, byte[] colors)
    {
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        byte[] colorMapLUT = cMap.getRGBByteColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        for (int i = start; i < end; i++) {
            double v = 0;
            int cIndex = 0;
            if (colorMappedDataVlen == 1)
                v = colorMappedData[i];
            else {
                v = 0;
                for (int j = 0; j < colorMappedDataVlen; j++)
                    v += colorMappedData[i * colorMappedDataVlen + j] * colorMappedData[i * colorMappedDataVlen + j];
                v = sqrt(v);
            }
            if (!cMap.isWrap())
                cIndex = max(0, min(3 * nColors, 3 * (int) (d * (v - low))));
            else
                cIndex = 3 * (((int) (d * (v - low) + 1000 * nColors)) % nColors);
            for (int j = 0; j < 3; j++)
                colors[4 * i + j] = colorMapLUT[cIndex + j];
            colors[4 * i + 3] = (byte) 0xff;
        }
        byte[] old = new byte[4];
        if (colorModData != null) {
            switch (dataMappingParams.getColorMapModification()) {
                case DataMappingParams.BLEND_MAP_MODIFICATION: {
                    float blendRatio = dataMappingParams.getBlendRatio();
                    cMap = dataMappingParams.getColorMap1();
                    float[] dta = new float[colorModDataVlen];
                    for (int i = start; i < end; i++) {
                        System.arraycopy(colorModData, i * colorModDataVlen, dta, 0, colorModDataVlen);
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(dta, cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
                    break;
                }
                case DataMappingParams.SAT_MAP_MODIFICATION: {
                    ColorComponentParams satParams = dataMappingParams.getSatParams();
                    float mlow = satParams.getColorComponentMax();
                    low = satParams.getDataMin();
                    d = (1 - mlow) / (satParams.getDataMax() - low);
                    for (int i = start; i < end; i++) {
                        double u = 0;
                        if (colorModDataVlen == 1)
                            u = mlow + d * (colorModData[i] - low);
                        else {
                            u = 0;
                            for (int j = 0; j < colorModDataVlen; j++)
                                u += colorModData[i * colorModDataVlen + j] * colorModData[i * colorModDataVlen + j];
                            u = mlow + d * (sqrt(u) - low);
                        }
                        u = max(mlow, min(satParams.getDataMax(), u));
                        int m = 0;
                        for (int j = 0; j < 3; j++)
                            if (m < colors[4 * i + j])
                                m = colors[4 * i + j];
                        for (int j = 0; j < 3; j++)
                            colors[4 * i + j] = (byte) (0xff & (int) (m - u * (m - colors[4 * i + j])));
                    }
                    break;
                }
                case DataMappingParams.VAL_MAP_MODIFICATION: {
                    ColorComponentParams satParams = dataMappingParams.getSatParams();
                    float mlow = satParams.getColorComponentMax();
                    low = satParams.getDataMin();
                    d = (1 - mlow) / (satParams.getDataMax() - low);
                    for (int i = start; i < end; i++) {
                        double u = 0;
                        if (colorModDataVlen == 1)
                            u = mlow + d * (colorModData[i] - low);
                        else {
                            u = 0;
                            for (int j = 0; j < colorModDataVlen; j++)
                                u += colorModData[i * colorModDataVlen + j] * colorModData[i * colorModDataVlen + j];
                            u = mlow + d * (sqrt(u) - low);
                        }
                        u = max(mlow, min(satParams.getDataMax(), u));
                        if (u <= 1)
                            for (int j = 0; j < 3; j++)
                                colors[4 * i + j] = (byte) (0xff & (int) (u * colors[4 * i + j]));
                        else {
                            double w = Math.min(1, Math.max(0, 2 - u));
                            for (int j = 0; j < 3; j++)
                                colors[4 * i + j] = (byte) (0xff & (int) (255 - w * (255 - colors[4 * i + j])));
                        }
                    }
                    break;
                }
                default:
                    throw new UnsupportedOperationException("Unsupported type of colormap modification.");
            }
        }
        if (transpData != null) {
            TransparencyParams tParams = dataMappingParams.getTransparencyParams();
            int[] tMap = tParams.getMap();
            low = tParams.getComponentRange().getLow();
            d = 255 / (tParams.getComponentRange().getUp() - low);
            int tIndex;
            double v;
            for (int i = start; i < end; i++) {
                if (transpDataVlen == 1)
                    v = transpData[i];
                else {
                    v = 0;
                    for (int j = 0, k = transpDataVlen * i; j < transpDataVlen; j++, k++)
                        v += transpData[k] * transpData[k];
                    v = sqrt(v);
                }
                tIndex = (int) (d * (v - low));
                if (tIndex < 0)
                    tIndex = 0;
                if (tIndex > 255)
                    tIndex = 255;
                colors[4 * i + 3] = (byte) (0xff & tMap[tIndex]);
            }
        }
        if (mask != null)
            for (int i = start; i < end; i++)
                if (mask[i] == 0)
                    colors[4 * i + 3] = 0;
    }

    private static class Map implements Runnable
    {

        private int nThreads = 1;
        private int iThread = 0;
        private int nData;
        private float[] colorMappedData;
        private int colorMappedDataVlen;
        private float[] colorModData;
        private int colorModDataVlen;
        private float[] transpData;
        private int transpDataVlen;
        private DataMappingParams dataMappingParams;
        private byte[] mask;
        private byte[] colors;

        public Map(int nData, int nThreads, int iThread, float[] colorMappedData, int colorMappedDataVlen, float[] colorModData, int colorModDataVlen,
                   float[] transpData, int transpDataVlen, byte[] mask, DataMappingParams dataMappingParams, byte[] colors)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.nData = nData;
            this.colorMappedData = colorMappedData;
            this.colorMappedDataVlen = colorMappedDataVlen;
            this.colorModData = colorModData;
            this.colorModDataVlen = colorModDataVlen;
            this.transpData = transpData;
            this.transpDataVlen = transpDataVlen;
            this.dataMappingParams = dataMappingParams;
            this.mask = mask;
            this.colors = colors;
        }

        @Override
        public void run()
        {
            mapArrayPart(colorMappedData, colorMappedDataVlen, colorModData, colorModDataVlen, transpData, transpDataVlen, mask, dataMappingParams,
                         (iThread * nData) / nThreads, ((iThread + 1) * nData) / nThreads, colors);
        }
    }

    public static int[] mapArraysToInt(float[] colorMappedData, int colorMappedDataVlen, float[] colorModData, int colorModDataVlen, float[] transpData, int transpDataVlen,
                                       DataMappingParams dataMappingParams, int nThreads, int[] colors)
    {
        byte[] bColors = mapArrays(colorMappedData, colorMappedDataVlen, colorModData, colorModDataVlen, transpData, transpDataVlen, dataMappingParams, nThreads, null);
        if (bColors == null)
            return null;
        int nData = bColors.length / 4;
        if (colors == null || colors.length != nData)
            colors = new int[nData];
        for (int i = 0, j = 0; i < colors.length; i++, j += 4)
            colors[i] = (0xff & bColors[j]) << 16 |
                (0xff & bColors[j + 1]) << 8 |
                (0xff & bColors[j + 2]) |
                (0xff & bColors[j + 3]) << 24;
        return colors;
    }

    public static int[] mapArraysToInt(float[] colorMappedData, int colorMappedDataVlen, float[] colorModData, int colorModDataVlen, float[] transpData, int transpDataVlen,
                                       byte[] mask, DataMappingParams dataMappingParams, int nThreads, int[] colors)
    {
        byte[] bColors = mapArrays(colorMappedData, colorMappedDataVlen, colorModData, colorModDataVlen, transpData, transpDataVlen, mask, dataMappingParams, nThreads, null);
        if (bColors == null)
            return null;
        int nData = bColors.length / 4;
        if (colors == null || colors.length != nData)
            colors = new int[nData];
        for (int i = 0, j = 0; i < colors.length; i++, j += 4)
            colors[i] = (0xff & bColors[j]) << 16 |
                (0xff & bColors[j + 1]) << 8 |
                (0xff & bColors[j + 2]) |
                (0xff & bColors[j + 3]) << 24;
        return colors;
    }

    public static byte[] mapArrays(float[] colorMappedData, int colorMappedDataVlen, float[] colorModData, int colorModDataVlen, float[] transpData, int transpDataVlen,
                                   DataMappingParams dataMappingParams, int nThreads, byte[] colors)
    {
        if (colorMappedData == null ||
            colorMappedData.length % colorMappedDataVlen != 0)
            return null;
        int nData = colorMappedData.length / colorMappedDataVlen;
        if (colorModData != null && colorModData.length != nData * colorModDataVlen)
            return null;
        if (transpData != null && transpData.length != nData * transpDataVlen)
            return null;
        if (colors == null || colors.length != 4 * nData)
            colors = new byte[4 * nData];

        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread]
                = new Thread(new Map(nData, nThreads, iThread, colorMappedData, colorMappedDataVlen, colorModData, colorModDataVlen, transpData, transpDataVlen,
                                     null, dataMappingParams, colors));
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return colors;
    }

    public static byte[] mapArrays(float[] colorMappedData, int colorMappedDataVlen, float[] colorModData, int colorModDataVlen, float[] transpData, int transpDataVlen,
                                   byte[] mask, DataMappingParams dataMappingParams, int nThreads, byte[] colors)
    {
        if (colorMappedData == null ||
            colorMappedData.length % colorMappedDataVlen != 0)
            return null;
        int nData = colorMappedData.length / colorMappedDataVlen;
        if (colorModData != null && colorModData.length != nData * colorModDataVlen)
            return null;
        if (transpData != null && transpData.length != nData * transpDataVlen)
            return null;
        if (mask != null && mask.length != nData)
            mask = null;
        if (colors == null || colors.length != 4 * nData)
            colors = new byte[4 * nData];

        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread]
                = new Thread(new Map(nData, nThreads, iThread, colorMappedData, colorMappedDataVlen, colorModData, colorModDataVlen, transpData, transpDataVlen,
                                     mask, dataMappingParams, colors));
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread].start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return colors;
    }

    public static byte[] mapColorsIndexed(DataContainer data, DataMappingParams dataMappingParams, int[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        if (colors == null || colors.length != 4 * nInd)
            colors = new byte[4 * nInd];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColorsIndexedColormapped(data, dataMappingParams, indices, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapColorsIndexedRGB(data, dataMappingParams, indices, col, colors);
                break;
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = 0; i < nInd; i++)
                if (valid.getByte(indices[i]) == 0) {
                    colors[4 * i] = bc[0];
                    colors[4 * i + 1] = bc[1];
                    colors[4 * i + 2] = bc[2];
                    colors[4 * i + 3] = (byte) 127;
                }
        }
        return colors;
    }

    public static byte[] mapColorsIndexed(DataContainer data, DataMappingParams dataMappingParams, long[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        if (colors == null || colors.length != 4 * nInd)
            colors = new byte[4 * nInd];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColorsIndexedColormapped(data, dataMappingParams, indices, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapColorsIndexedRGB(data, dataMappingParams, indices, col, colors);
                break;
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = 0; i < nInd; i++)
                if (valid.getByte(indices[i]) == 0) {
                    colors[4 * i] = bc[0];
                    colors[4 * i + 1] = bc[1];
                    colors[4 * i + 2] = bc[2];
                    colors[4 * i + 3] = (byte) 127;
                }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedColormapped(DataContainer data, DataMappingParams dataMappingParams, int[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ColorComponentParams modParams = null;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION)
            modParams = dataMappingParams.getSatParams();
        else if (dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION)
            modParams = dataMappingParams.getValParams();
        DataArray colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null || !colData.isNumeric()) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 100
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") && colData.getType() == DataArrayType.FIELD_DATA_BYTE && colData.getVectorLength() >= 3) {
            UnsignedByteLargeArray cData = ((ByteDataArray) colData).getRawArray();
            int vlen = colData.getVectorLength();
            for (int i = 0; i < nInd; i++)
                for (long j = 0, k = 4 * i, l = vlen * indices[i]; j < vlen; j++, k++, l++)
                    colors[(int) k] = cData.getByte(l);
            if (vlen == 3)
                for (int i = 3; i < colors.length; i += 4)
                    colors[i] = (byte) 255;
            return colors;
        }
        switch (colData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                for (int i = 0; i < nInd; i++) {
                    float[] cplx = new float[]{reData.getFloat(indices[i]), imData.getFloat(indices[i])};
                    System.arraycopy(mapByteColor(cplx, cMap), 0, colors, 4 * i, 4);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    float[] objvals = new float[colData.getVectorLength()];
                    for (int j = 0; j < objvals.length; j++)
                        objvals[j] = ((DataObjectInterface) oData.get(objvals.length * indices[i] + j)).toFloat();
                    System.arraycopy(mapByteColor(objvals, cMap), 0, colors, 4 * i, 4);
                }
                break;
            default:
                for (int i = 0; i < nInd; i++)
                    System.arraycopy(mapByteColor(colData.getFloatElement(indices[i]), cMap), 0, colors, 4 * i, 4);

        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;

        if ((dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION ||
            dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION) && modParams != null) {
            boolean modSat = (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION);
            DataArray modData = data.getComponent(modParams.getDataComponentName());
            if (modData == null)
                return colors;
            int vl = modData.getVectorLength();
            float low, d;
            if (vl == 1)
                low = modParams.getDataMin();
            else
                low = 0;
            float mlow = modParams.getColorComponentMin();
            float mup = modSat ? 1 : modParams.getColorComponentMax();
            d = (mup - mlow) / (modParams.getDataMax() - low);
            byte[] old = new byte[4];
            switch (modData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) modData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) modData).getFloatImaginaryArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] cplx = new float[]{reData.getFloat(indices[i]), imData.getFloat(indices[i])};
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(cplx, low, mlow, mup, d, modSat, old), 0, colors, 4 * i, 4);
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) modData.getRawArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] objvals = new float[vl];
                        for (int j = 0; j < vl; j++)
                            objvals[j] = ((DataObjectInterface) oData.get(vl * indices[i] + j)).toFloat();
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(objvals, low, mlow, mup, d, modSat, old), 0, colors, 4 * i, 4);
                    }
                    break;
                default:
                    for (int i = 0; i < nInd; i++) {
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(modData.getFloatElement(indices[i]), low, mlow, mup, d, modSat, old), 0, colors, 4 * i, 4);
                    }
            }
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.BLEND_MAP_MODIFICATION) {
            float blendRatio = dataMappingParams.getBlendRatio();
            cMap = dataMappingParams.getColorMap1();
            colData = data.getComponent(cMap.getDataComponentName());
            if (colData == null) colData = data.getComponent(0);
            byte[] old = new byte[4];
            switch (colData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] cplx = new float[]{reData.getFloat(indices[i]), imData.getFloat(indices[i])};
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(cplx, cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] objvals = new float[colData.getVectorLength()];
                        for (int j = 0; j < objvals.length; j++)
                            objvals[j] = ((DataObjectInterface) oData.get(objvals.length * indices[i] + j)).toFloat();
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(objvals, cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
                    break;
                default:
                    for (int i = 0; i < nInd; i++) {
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(colData.getFloatElement(indices[i]), cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
            }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedColormapped(DataContainer data, DataMappingParams dataMappingParams, long[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ColorComponentParams modParams = null;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION)
            modParams = dataMappingParams.getSatParams();
        else if (dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION)
            modParams = dataMappingParams.getValParams();
        DataArray colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null || !colData.isNumeric()) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 100
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") && colData.getType() == DataArrayType.FIELD_DATA_BYTE && colData.getVectorLength() >= 3) {
            UnsignedByteLargeArray cData = ((ByteDataArray) colData).getRawArray();
            int vlen = colData.getVectorLength();
            for (int i = 0; i < nInd; i++)
                for (long j = 0, k = 4 * i, l = vlen * indices[i]; j < vlen; j++, k++, l++)
                    colors[(int) k] = cData.getByte(l);
            if (vlen == 3)
                for (int i = 3; i < colors.length; i += 4)
                    colors[i] = (byte) 255;
            return colors;
        }
        switch (colData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                for (int i = 0; i < nInd; i++) {
                    float[] cplx = new float[]{reData.getFloat(indices[i]), imData.getFloat(indices[i])};
                    System.arraycopy(mapByteColor(cplx, cMap), 0, colors, 4 * i, 4);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    float[] objvals = new float[colData.getVectorLength()];
                    for (int j = 0; j < objvals.length; j++)
                        objvals[j] = ((DataObjectInterface) oData.get(objvals.length * indices[i] + j)).toFloat();
                    System.arraycopy(mapByteColor(objvals, cMap), 0, colors, 4 * i, 4);
                }
                break;
            default:
                for (int i = 0; i < nInd; i++)
                    System.arraycopy(mapByteColor(colData.getFloatElement(indices[i]), cMap), 0, colors, 4 * i, 4);

        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;

        if ((dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION ||
            dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION) && modParams != null) {
            boolean modSat = (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION);
            DataArray modData = data.getComponent(modParams.getDataComponentName());
            if (modData == null)
                return colors;
            int vl = modData.getVectorLength();
            float low, d;
            if (vl == 1)
                low = modParams.getDataMin();
            else
                low = 0;
            float mlow = modParams.getColorComponentMin();
            float mup = modSat ? 1 : modParams.getColorComponentMax();
            d = (mup - mlow) / (modParams.getDataMax() - low);
            byte[] old = new byte[4];
            switch (modData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) modData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) modData).getFloatImaginaryArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] cplx = new float[]{reData.getFloat(indices[i]), imData.getFloat(indices[i])};
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(cplx, low, mlow, mup, d, modSat, old), 0, colors, 4 * i, 4);
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) modData.getRawArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] objvals = new float[vl];
                        for (int j = 0; j < vl; j++)
                            objvals[j] = ((DataObjectInterface) oData.get(vl * indices[i] + j)).toFloat();
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(objvals, low, mlow, mup, d, modSat, old), 0, colors, 4 * i, 4);
                    }
                    break;
                default:
                    for (int i = 0; i < nInd; i++) {
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(modData.getFloatElement(indices[i]), low, mlow, mup, d, modSat, old), 0, colors, 4 * i, 4);
                    }
            }
        }
        for (int i = 3; i < colors.length; i += 4)
            colors[i] = (byte) 255;
        if (dataMappingParams.getColorMapModification() == DataMappingParams.BLEND_MAP_MODIFICATION) {
            float blendRatio = dataMappingParams.getBlendRatio();
            cMap = dataMappingParams.getColorMap1();
            colData = data.getComponent(cMap.getDataComponentName());
            if (colData == null) colData = data.getComponent(0);
            byte[] old = new byte[4];
            switch (colData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] cplx = new float[]{reData.getFloat(indices[i]), imData.getFloat(indices[i])};
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(cplx, cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0; i < nInd; i++) {
                        float[] objvals = new float[colData.getVectorLength()];
                        for (int j = 0; j < objvals.length; j++)
                            objvals[j] = ((DataObjectInterface) oData.get(objvals.length * indices[i] + j)).toFloat();
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(objvals, cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
                    break;
                default:
                    for (int i = 0; i < nInd; i++) {
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(blendColors(colData.getFloatElement(indices[i]), cMap, old, blendRatio), 0, colors, 4 * i, 4);
                    }
            }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedRGB(DataContainer data, DataMappingParams dataMappingParams, int[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        float minData = 0, maxData = 0, minC = 0, scale = 1;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        double v;
        int vl;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());
        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue())
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            for (int i = 3; i < colors.length; i += 4)
                colors[i] = (byte) 255;
            return colors;
        }
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                byte c = (byte) (0xff & (int) (255 * rgbParams[ncomp].getColorComponentMin()));
                for (int i = ncomp; i < colors.length; i += 3)
                    colors[i] = c;
                continue;
            }

            minC = 255 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (255 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                    float v1,
                     v2;
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData.getFloat(indices[i]);
                            v2 = imData.getFloat(indices[i]);
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (long j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(indices[i])).toFloat();
                        else {
                            v = 0;
                            for (long j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                default:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(indices[i]);
                        else {
                            v = 0;
                            for (long j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;

            }
        }
        return colors;
    }

    private static byte[] mapColorsIndexedRGB(DataContainer data, DataMappingParams dataMappingParams, long[] indices, Color3f col, byte[] colors)
    {
        int nInd = indices.length;
        float minData = 0, maxData = 0, minC = 0, scale = 1;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        double v;
        int vl;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());
        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue())
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            for (int i = 3; i < colors.length; i += 4)
                colors[i] = (byte) 255;
            return colors;
        }
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                byte c = (byte) (0xff & (int) (255 * rgbParams[ncomp].getColorComponentMin()));
                for (int i = ncomp; i < colors.length; i += 3)
                    colors[i] = c;
                continue;
            }

            minC = 255 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (255 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                    float v1,
                     v2;
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData.getFloat(indices[i]);
                            v2 = imData.getFloat(indices[i]);
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (long j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(indices[i])).toFloat();
                        else {
                            v = 0;
                            for (long j = 0, k = vl * indices[i]; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                default:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0, l = ncomp; i < nInd; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(indices[i]);
                        else {
                            v = 0;
                            for (long j = 0, k = vl * indices[i]; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;

            }
        }
        return colors;
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int start, int end, int tStart, Color3f col, byte[] colors)
    {
        if (start < 0 || start >= end || end > data.getNElements())
            return null;
        if (colors == null || colors.length < 4 * (tStart + end - start))
            colors = new byte[4 * (tStart + end - start)];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColormapped(data, dataMappingParams, start, end, tStart, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapRGB(data, dataMappingParams, start, end, tStart, col, colors);
                break;
        }
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = start, l = tStart; i < end; i++, l++)
                if (valid.getByte(i) == 0) {
                    colors[4 * l] = bc[0];
                    colors[4 * l + 1] = bc[1];
                    colors[4 * l + 2] = bc[2];
                    colors[4 * l + 3] = (byte) 127;
                }
        }
        return colors;
    }

    public static byte[] mapColormapped(LargeArray colData, int colVlen, LargeArray modData, int modVlen, DataMappingParams dataMappingParams, Color3f col, byte[] colors)
    {
        return mapColormapped(colData, colVlen, modData, modVlen, dataMappingParams, 0, (int) colData.length(), 0, col, colors);
    }

    public static byte[] mapColormapped(LargeArray colData, int colVlen, LargeArray modData, int modVlen, DataMappingParams dataMappingParams, int start, int end, int tStart,
                                        Color3f col, byte[] colors)
    {
        if (colors == null || colors.length < 4 * (tStart + end - start))
            colors = new byte[4 * (tStart + end - start)];
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ColorComponentParams modParams = null;
        switch (dataMappingParams.getColorMapModification()) {
            case DataMappingParams.SAT_MAP_MODIFICATION:
                modParams = dataMappingParams.getSatParams();
                break;
            case DataMappingParams.VAL_MAP_MODIFICATION:
                modParams = dataMappingParams.getValParams();
                break;
            case DataMappingParams.BLEND_MAP_MODIFICATION:
                break;
            default:
                throw new UnsupportedOperationException("Unsupported type of colormap modification.");
        }
        if (colData == null) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 255
            };
            for (int i = 4 * tStart; i < 4 * (tStart + end - start);)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int vl = colVlen;
        float[] v = new float[vl];
        switch (colData.getType()) {
            case COMPLEX_FLOAT:
                FloatLargeArray reData = ((ComplexFloatLargeArray) colData).getRealArray();
                FloatLargeArray imData = ((ComplexFloatLargeArray) colData).getImaginaryArray();
                for (int i = start, it = tStart; i < end; i++, it++) {
                    float[] cplx = new float[]{reData.getFloat(i), imData.getFloat(i)};
                    System.arraycopy(mapByteColor(cplx, cMap), 0, colors, 4 * it, 4);
                }
                break;
            case OBJECT:
                for (int i = start, it = tStart; i < end; i++, it++) {
                    for (int j = 0; j < vl; j++)
                        v[j] = ((DataObjectInterface) colData.get(vl * i + j)).toFloat();
                    System.arraycopy(mapByteColor(v, cMap), 0, colors, 4 * it, 4);
                }
                break;
            default:
                for (int i = start, it = tStart; i < end; i++, it++)
                    System.arraycopy(mapByteColor(colData.getFloatData(v, vl * i, vl * (i + 1), 1), cMap), 0, colors, 4 * it, 4);
                break;
        }

        return colors;
    }

    private static byte[] mapColormapped(DataContainer data, DataMappingParams dataMappingParams, int start, int end, int tStart, Color3f col, byte[] colors)
    {
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ColorComponentParams modParams = null;
        switch (dataMappingParams.getColorMapModification()) {
            case DataMappingParams.SAT_MAP_MODIFICATION:
                modParams = dataMappingParams.getSatParams();
                break;
            case DataMappingParams.VAL_MAP_MODIFICATION:
                modParams = dataMappingParams.getValParams();
                break;
            case DataMappingParams.BLEND_MAP_MODIFICATION:
                break;
            default:
        }
        DataArray colData = null;
        if (cMap.getDataComponentName() != null)
            colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue()),
                (byte) 255
            };
            for (int i = 4 * tStart; i < 4 * (tStart + end - start);)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") && colData.getType() == DataArrayType.FIELD_DATA_BYTE && colData.getVectorLength() == 3) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            for (int i = start, k = 4 * tStart, l = 3 * start; i < end; i++) {
                for (int j = 0; j < 3; j++, k++, l++)
                    colors[k] = cData[l];
                colors[k] = (byte) 255;
                k += 1;
            }
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") && colData.getType() == DataArrayType.FIELD_DATA_BYTE && colData.getVectorLength() == 4) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            System.arraycopy(cData, 4 * start, colors, 4 * tStart, 4 * (end - start));
            return colors;
        }
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                for (int i = start, it = tStart; i < end; i++, it++) {
                    float[] cplx = new float[]{reData.getFloat(i), imData.getFloat(i)};
                    System.arraycopy(mapByteColor(cplx, cMap), 0, colors, 4 * it, 4);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                for (int i = start, it = tStart; i < end; i++, it++) {
                    float[] objvals = new float[colData.getVectorLength()];
                    for (int j = 0; j < objvals.length; j++)
                        objvals[j] = ((DataObjectInterface) oData.get(objvals.length * i)).toFloat();
                    System.arraycopy(mapByteColor(objvals, cMap), 0, colors, 4 * it, 4);
                }
                break;
            default:
                for (int i = start, it = tStart; i < end; i++, it++)
                    System.arraycopy(mapByteColor(colData.getFloatElement(i), cMap), 0, colors, 4 * it, 4);
        }

        if (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION ||
            dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION) {
            boolean modSat = (dataMappingParams.getColorMapModification() == DataMappingParams.SAT_MAP_MODIFICATION);
            DataArray modData = null;
            if (modParams != null && modParams.getDataComponentName() != null)
                modData = data.getComponent(modParams.getDataComponentName());
            if (modData == null)
                return colors;
            vl = modData.getVectorLength();
            if (vl == 1)
                low = modParams.getDataMin();
            else
                low = 0;
            float mlow = modParams.getColorComponentMin();
            float mup = modParams.getColorComponentMax();
            d = (1 - mlow) / (modParams.getDataMax() - low);
            byte[] old = new byte[4];
            switch (modData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) modData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) modData).getFloatImaginaryArray();
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        float[] cplx = new float[]{reData.getFloat(i), imData.getFloat(i)};
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(cplx, low, mlow, mup, d, modSat, old), 0, colors, l, 4);
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) modData.getRawArray();
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        float[] objvals = new float[vl];
                        for (int j = 0; j < vl; j++)
                            objvals[j] = ((DataObjectInterface) oData.get(vl * i + j)).toFloat();
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(objvals, low, mlow, mup, d, modSat, old), 0, colors, l, 4);
                    }
                    break;
                default:
                    for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                        System.arraycopy(colors, 4 * i, old, 0, 4);
                        System.arraycopy(modByteColorSaturationValue(modData.getFloatElement(i), low, mlow, mup, d, modSat, old), 0, colors, l, 4);
                    }
            }
        }
        if (dataMappingParams.getColorMapModification() == DataMappingParams.BLEND_MAP_MODIFICATION) {
            float blendRatio = dataMappingParams.getBlendRatio();
            cMap = dataMappingParams.getColorMap1();
            colData = data.getComponent(cMap.getDataComponentName());
            byte[] old = new byte[4];
            if (colData != null)
                switch (colData.getType()) {
                    case FIELD_DATA_COMPLEX:
                        FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                        FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                        for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                            float[] cplx = new float[]{reData.getFloat(i), imData.getFloat(i)};
                            System.arraycopy(colors, 4 * i, old, 0, 4);
                            System.arraycopy(blendColors(cplx, cMap, old, blendRatio), 0, colors, l, 4);
                        }
                        break;
                    case FIELD_DATA_OBJECT:
                        ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                        for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                            float[] objvals = new float[colData.getVectorLength()];
                            for (int j = 0; j < objvals.length; j++)
                                objvals[j] = ((DataObjectInterface) oData.get(objvals.length * i + j)).toFloat();
                            System.arraycopy(colors, 4 * i, old, 0, 4);
                            System.arraycopy(blendColors(objvals, cMap, old, blendRatio), 0, colors, l, 4);
                        }
                        break;
                    default:
                        for (int i = start, l = 4 * tStart; i < end; i++, l += 4) {
                            System.arraycopy(colors, 4 * i, old, 0, 4);
                            System.arraycopy(blendColors(colData.getFloatElement(i), cMap, old, blendRatio), 0, colors, l, 4);
                        }
                }
        }
        return colors;
    }

    private static byte[] mapRGB(DataContainer data, DataMappingParams dataMappingParams, int start, int end, int tStart, Color3f col, byte[] colors)
    {
        float minData = 0, maxData = 0, minC = 0, scale = 1;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        int vl;
        double v;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());
        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            byte[] bc = new byte[]{
                (byte) (0xff & col.get().getRed()),
                (byte) (0xff & col.get().getGreen()),
                (byte) (0xff & col.get().getBlue())
            };
            for (int i = 0; i < colors.length;)
                for (int j = 0; j < bc.length; j++, i++)
                    colors[i] = bc[j];
            return colors;
        }
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                byte c = (byte) (0xff & (int) (255 * rgbParams[ncomp].getColorComponentMin()));
                for (int i = ncomp; i < colors.length; i += 4)
                    colors[i] = c;
                continue;
            }

            minC = 200 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (200 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                    float v1,
                     v2;
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData.getFloat(i);
                            v2 = imData.getFloat(i);
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(i)).toFloat();
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }
                    break;
                default:
                    LargeArray dData = colData.getRawArray();
                    for (int i = start, l = 4 * tStart + ncomp; i < end; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(i);
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] = (byte) (0xff & (int) (minC + scale * (v - minData)));
                    }

            }
        }
        return colors;
    }

    public static int[] map(DataContainer data, DataMappingParams dataMappingParams,
                            Color3f col, int[] colors,
                            boolean transpose, int n0, int n1)
    {
        int n = (int) data.getNElements();
        if (colors == null || colors.length < n)
            colors = new int[n];
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                colors = mapColormapped(data, dataMappingParams, col, colors);
                break;
            case DataMappingParams.RGB:
                colors = mapRGB(data, dataMappingParams, col, colors);
                break;
        }
        if (data instanceof Field && ((Field) data).hasMask()) {
            LogicLargeArray valid = ((Field) data).getCurrentMask();
            float[] c = new float[3];
            col.get(c);
            byte[] bc = new byte[4];
            for (int i = 0; i < c.length; i++)
                bc[i] = (byte) (0xff & (int) (255 * c[i]));
            for (int i = 0, l = 0; i < n; i++, l++)
                if (valid.getByte(i) == 0)
                    colors[l] = bc[0] << 16 | bc[1] << 8 | bc[2] | 0x7f << 24;
        }
        if (transpose && n0 * n1 == n) {
            int[] tcolors = new int[n];
            for (int i = 0; i < n1; i++)
                for (int j = 0; j < n0; j++)
                    tcolors[j * n1 + i] = colors[i * n0 + j];
            System.arraycopy(tcolors, 0, colors, 0, n);
        }
        return colors;
    }

    private static int intModifySaturation(int original, double saturation)
    {
        int r = 0xff & (original >> 16);
        int g = 0xff & (original >> 8);
        int b = 0xff & original;
        int m = r;
        if (g > m)
            m = g;
        if (b > m)
            m = b;
        int res = (0xff << 24) |
            ((0xff & (int) (m - saturation * (m - r))) << 16) |
            ((0xff & (int) (m - saturation * (m - g))) << 8) |
            (0xff & (int) (m - saturation * (m - b)));
        return res;
    }

    private static int intModifyValue(int original, double value)
    {
        int r = 0xff & (original >> 16);
        int g = 0xff & (original >> 8);
        int b = 0xff & original;
        int m = r;
        if (g > m)
            m = g;
        if (b > m)
            m = b;
        int res = (0xff << 24) |
            ((0xff & (int) (value * r)) << 16) |
            ((0xff & (int) (value * g)) << 8) |
            (0xff & (int) (value * b));
        return res;
    }

    private static void intModifyValueSaturation(DataContainer data, DataMappingParams dataMappingParams, Color3f col, int[] colors)
    {
        boolean modVal = (dataMappingParams.getColorMapModification() == DataMappingParams.VAL_MAP_MODIFICATION);
        int n = (int) data.getNElements();
        float low = 0;
        ColorComponentParams modMapParams = dataMappingParams.getSatParams();
        DataArray modData = data.getComponent(modMapParams.getDataComponentName());
        if (modData == null)
            return;
        int vl = modData.getVectorLength();
        if (vl == 1)
            low = modMapParams.getDataMin();
        float mlow = modMapParams.getColorComponentMin();
        float d = (1 - mlow) / (modMapParams.getDataMax() - low);
        double u;
        switch (modData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) modData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) modData).getFloatImaginaryArray();
                float v1,
                 v2;
                for (int i = 0, l = 0; i < n; i++, l++) {
                    if (vl == 1) {
                        v1 = reData.getFloat(i);
                        v2 = imData.getFloat(i);
                        u = mlow + d * (sqrt(v1 * v1 + v2 * v2) - low);
                    } else {
                        u = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            u += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    colors[l] &= 0xff << 24;
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) modData.getRawArray();
                for (int i = 0, l = 0; i < n; i++, l++) {
                    if (vl == 1)
                        u = mlow + d * (((DataObjectInterface) oData.get(i)).toFloat() - low);
                    else {
                        u = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            u += tmp * tmp;
                        }
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }
                break;
            default:
                for (int i = 0, l = 0; i < n; i++, l++) {
                    float[] val = modData.getFloatElement(i);
                    if (vl == 1)
                        u = mlow + d * (val[0] - low);
                    else {
                        u = 0;
                        for (int j = 0; j < vl; j++)
                            u += val[j] * val[j];
                        u = mlow + d * sqrt(u);
                    }
                    u = max(mlow, min(1, u));
                    if (modVal)
                        colors[l] = intModifyValue(colors[l], u);
                    else
                        colors[l] = intModifySaturation(colors[l], u);
                }

        }
    }

    private static int intBlend(int c0, int c1, float value)
    {
        int r0 = 0xff & (c0 >> 16);
        int g0 = 0xff & (c0 >> 8);
        int b0 = 0xff & c0;
        int r1 = 0xff & (c1 >> 16);
        int g1 = 0xff & (c1 >> 8);
        int b1 = 0xff & c1;
        int res = (0xff << 24) |
            ((0xff & (int) ((1 - value) * r0 + value * r1)) << 16) |
            ((0xff & (int) ((1 - value) * g0 + value * g1)) << 8) |
            (0xff & (int) ((1 - value) * b0 + value * b1));
        return res;
    }

    private static void intBlend(DataContainer data, DataMappingParams dataMappingParams, Color3f col, int[] colors)
    {
        int n = (int) data.getNElements();
        ComponentColorMap modMap = dataMappingParams.getColorMap1();
        boolean wrapCMap = modMap.isWrap();
        DataArray colData = data.getComponent(modMap.getDataComponentName());
        float blendRatio = dataMappingParams.getBlendRatio();
        if (colData == null)
            return;
        int[] colorMapILUT = modMap.getARGBColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = modMap.getDataMin();
        float d = nColors / (modMap.getDataMax() - low);
        int cIndex;
        double v;
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                float v1,
                 v2;
                if (reData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1) {
                        v1 = reData.getFloat(i);
                        v2 = imData.getFloat(i);
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                if (oData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }
                break;
            default:
                for (int i = 0, it = 0; i < n; i++, it++) {
                    float[] val = colData.getFloatElement(i);
                    if (vl == 1)
                        v = val[0];
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += val[j] * val[j];
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = intBlend(colors[it], colorMapILUT[cIndex], blendRatio);
                }

        }
    }

    private static int[] mapColormapped(DataContainer data, DataMappingParams dataMappingParams, Color3f col, int[] colors)
    {
        int n = (int) data.getNElements();
        ComponentColorMap cMap = dataMappingParams.getColorMap0();
        ColorComponentParams modMapParams = null;
        boolean wrapCMap = cMap.isWrap();
        DataArray colData = data.getComponent(cMap.getDataComponentName());
        if (colData == null) {
            for (int i = 0; i < n; i++)
                colors[i] = 0xff << 24 |
                    ((0xff & col.get().getRed()) << 16) |
                    ((0xff & col.get().getGreen()) << 8) |
                    (0xff & col.get().getBlue());
            return colors;
        }
        if (colData.getUserData(0).equalsIgnoreCase("colors") &&
            colData.getType() == DataArrayType.FIELD_DATA_BYTE) {
            byte[] cData = (byte[]) colData.getRawArray().getData();
            if (colData.getVectorLength() == 3) {
                for (int i = 0; i < n; i++)
                    colors[i] = 0xff << 24 |
                        ((0xff & cData[3 * i + 2]) << 16) |
                        ((0xff & cData[3 * i + 1]) << 8) |
                        (0xff & cData[3 * i]);
                return colors;
            } else if (colData.getVectorLength() == 4) {
                for (int i = 0; i < n; i++)
                    colors[i] = ((0xff & cData[4 * i + 3]) << 24) |
                        ((0xff & cData[4 * i + 2]) << 16) |
                        ((0xff & cData[4 * i + 1]) << 8) |
                        (0xff & cData[4 * i]);
                return colors;
            }
        }
        int[] colorMapILUT = cMap.getARGBColorTable();
        int nColors = ColorMapManager.SAMPLING_TABLE - 1;
        float low = cMap.getDataMin();
        float d = nColors / (cMap.getDataMax() - low);
        int cIndex;
        double v;
        int vl = colData.getVectorLength();
        switch (colData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                float v1,
                 v2;
                if (reData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1) {
                        v1 = reData.getFloat(i);
                        v2 = imData.getFloat(i);
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                if (oData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }
                break;
            default:
                LargeArray dData = colData.getRawArray();
                if (dData.length() < n * vl)
                    break;
                for (int i = 0, it = 0; i < n; i++, it++) {
                    if (vl == 1)
                        v = dData.getDouble(i);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    if (!wrapCMap)
                        cIndex = max(0, min(nColors, (int) (d * (v - low))));
                    else
                        cIndex = (((int) (d * (v - low) + 1000 * nColors)) % nColors);
                    colors[it] = colorMapILUT[cIndex];
                }

        }
        switch (dataMappingParams.getColorMapModification()) {
            case DataMappingParams.SAT_MAP_MODIFICATION:
                modMapParams = dataMappingParams.getSatParams();
                if (modMapParams != null && modMapParams.getDataComponentName() != null)
                    intModifyValueSaturation(data, dataMappingParams, col, colors);
                break;
            case DataMappingParams.VAL_MAP_MODIFICATION:
                modMapParams = dataMappingParams.getValParams();
                if (modMapParams != null && modMapParams.getDataComponentName() != null)
                    intModifyValueSaturation(data, dataMappingParams, col, colors);
                break;
            case DataMappingParams.BLEND_MAP_MODIFICATION:
                cMap = dataMappingParams.getColorMap1();
                if (cMap != null && cMap.getDataComponentName() != null)
                    intBlend(data, dataMappingParams, col, colors);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported type of colormap modification.");
        }

        return colors;
    }

    private static int[] mapRGB(DataContainer data, DataMappingParams dataMappingParams, Color3f col, int[] colors)
    {
        int n = (int) data.getNElements();
        float minData, maxData, minC, scale;
        ColorComponentParams[] rgbParams = new ColorComponentParams[]{
            dataMappingParams.getRedParams(),
            dataMappingParams.getGreenParams(),
            dataMappingParams.getBlueParams()
        };
        DataArray[] components = new DataArray[3];
        DataArray colData;
        double v;
        int vl;
        for (int i = 0; i < rgbParams.length; i++)
            components[i] = data.getComponent(rgbParams[i].getDataComponentName());

        if ((components[0] == null || !components[0].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric()) &&
            (components[1] == null || !components[1].isNumeric())) {
            int bc = (0xff << 24) |
                ((0xff & col.get().getRed()) << 16) |
                ((0xff & col.get().getGreen()) << 8) |
                ((0xff & col.get().getBlue()));
            for (int i = 0; i < colors.length; i++)
                colors[i] = bc;
            return colors;
        }

        for (int i = 0; i < colors.length; i++)
            colors[i] = 0xff << 24;
        for (int ncomp = 0; ncomp < 3; ncomp++) {
            int shift = 0;
            if (ncomp == 0)
                shift = 16;
            if (ncomp == 1)
                shift = 8;
            colData = components[ncomp];
            if (colData == null || !colData.isNumeric()) {
                int c = (int) (255 * rgbParams[ncomp].getColorComponentMin()) << shift;
                for (int i = ncomp; i < colors.length; i += 4)
                    colors[i] |= c;
                continue;
            }

            minC = 200 * rgbParams[ncomp].getColorComponentMin();
            minData = rgbParams[ncomp].getDataMin();
            maxData = rgbParams[ncomp].getDataMax();
            scale = (200 - minC) / (maxData - minData);
            vl = colData.getVectorLength();
            switch (colData.getType()) {
                case FIELD_DATA_COMPLEX:
                    FloatLargeArray reData = ((ComplexDataArray) colData).getFloatRealArray();
                    FloatLargeArray imData = ((ComplexDataArray) colData).getFloatImaginaryArray();
                    float v1,
                     v2;
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1) {
                            v1 = reData.getFloat(i);
                            v2 = imData.getFloat(i);
                            v = sqrt(v1 * v1 + v2 * v2);
                        } else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }
                    break;
                case FIELD_DATA_OBJECT:
                    ObjectLargeArray oData = (ObjectLargeArray) colData.getRawArray();
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1)
                            v = ((DataObjectInterface) oData.get(i)).toFloat();
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++) {
                                double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                                v += tmp * tmp;
                            }
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }
                    break;
                default:
                    LargeArray dData = colData.getRawArray();
                    for (int i = 0, l = 4 * 0 + ncomp; i < n; i++, l += 4) {
                        if (vl == 1)
                            v = dData.getDouble(i);
                        else {
                            v = 0;
                            for (int j = 0, k = vl * i; j < vl; j++, k++)
                                v += dData.getDouble(k) * dData.getDouble(k);
                            v = sqrt(v);
                        }
                        if (v < minData)
                            v = minData;
                        if (v > maxData)
                            v = maxData;
                        colors[l] |= (0xff & (int) (minC + scale * (v - minData))) << shift;
                    }

            }
        }
        return colors;
    }

    public static byte[] mapTransparencyIndexed(DataContainer data, TransparencyParams params, int[] indices, byte[] colors)
    {
        DataArray trData = data.getComponent(params.getComponentRange().getComponentName());
        if (trData == null) {
            for (int i = 3; i < colors.length; i += 4)
                colors[i] = (byte) (0xff);
            return colors;
        }
        int nInd = indices.length;
        if (colors == null || colors.length != 4 * nInd)
            colors = new byte[4 * nInd];
        int[] transparencyMap = params.getMap();
        byte[] tMap = new byte[transparencyMap.length];
        for (int i = 0; i < tMap.length; i++)
            tMap[i] = (byte) (0xff & transparencyMap[i]);
        float low = params.getComponentRange().getLow();
        float up = params.getComponentRange().getUp();
        float d = 255 / (up - low);
        switch (trData.getType()) {
            case FIELD_DATA_COMPLEX:
                float[] reData = (float[]) ((ComplexDataArray) trData).getFloatRealArray().getData();
                float[] imData = (float[]) ((ComplexDataArray) trData).getFloatImaginaryArray().getData();
                for (int i = 0; i < nInd; i++) {
                    float[] cplx = new float[]{reData[indices[i]], imData[indices[i]]};
                    colors[4 * i + 3] = mapTransparency(cplx, d, low, tMap);
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) trData.getRawArray();
                for (int i = 0; i < nInd; i++) {
                    float[] objvals = new float[trData.getVectorLength()];
                    for (int j = 0; j < objvals.length; j++)
                        objvals[j] = ((DataObjectInterface) oData.get(objvals.length * indices[i] + j)).toFloat();
                    colors[4 * i + 3] = mapTransparency(objvals, d, low, tMap);
                }
                break;
            default:
                for (int i = 0; i < nInd; i++)
                    colors[4 * i + 3] = mapTransparency(trData.getFloatElement(indices[i]), d, low, tMap);
        }
        return colors;
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, int start, int end, int tStart, byte[] colors)
    {
        if (start < 0 || start >= end || end > data.getNElements() || colors == null || colors.length < 4 * end)
            return null;
        DataArray trData = data.getComponent(params.getComponentRange().getComponentName());
        if (trData == null) {
            for (int i = 4 * tStart + 3; i < 4 * (tStart + end - start); i += 4)
                colors[i] = (byte) (0xff);
            return colors;
        }
        int[] transparencyMap = params.getMap();
        byte[] tMap = new byte[transparencyMap.length];
        for (int i = 0; i < tMap.length; i++)
            tMap[i] = (byte) (0xff & transparencyMap[i]);
        float low = params.getComponentRange().getLow();
        float up = params.getComponentRange().getUp();
        float d = 255 / (up - low);
        int tIndex;
        double v;
        int vl = trData.getVectorLength();
        switch (trData.getType()) {
            case FIELD_DATA_COMPLEX:
                FloatLargeArray reData = ((ComplexDataArray) trData).getFloatRealArray();
                FloatLargeArray imData = ((ComplexDataArray) trData).getFloatImaginaryArray();
                float v1,
                 v2;
                if (reData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1) {
                        v1 = reData.getFloat(i);
                        v2 = imData.getFloat(i);
                        v = sqrt(v1 * v1 + v2 * v2);
                    } else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += sqrt(reData.getFloat(k) * reData.getFloat(k) + imData.getFloat(k) * imData.getFloat(k));
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }
                break;
            case FIELD_DATA_OBJECT:
                ObjectLargeArray oData = (ObjectLargeArray) trData.getRawArray();
                if (oData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = ((DataObjectInterface) oData.get(i)).toFloat();
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++) {
                            double tmp = ((DataObjectInterface) oData.get(k)).toFloat();
                            v += tmp * tmp;
                        }
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }
                break;
            default:
                LargeArray dData = trData.getRawArray();
                if (dData.length() < end * vl)
                    break;
                for (int i = start, it = tStart; i < end; i++, it++) {
                    if (vl == 1)
                        v = dData.getDouble(i);
                    else {
                        v = 0;
                        for (int j = 0, k = vl * i; j < vl; j++, k++)
                            v += dData.getDouble(k) * dData.getDouble(k);
                        v = sqrt(v);
                    }
                    tIndex = (int) (d * (v - low));
                    if (tIndex < 0)
                        tIndex = 0;
                    if (tIndex > 255)
                        tIndex = 255;
                    colors[4 * it + 3] = tMap[tIndex];
                }

        }
        return colors;
    }

    public static byte[] mapTimeValidityTransparency(int[] timeRange, float currentTime, int start, int end, int tStart, byte[] colors)
    {
        if (start < 0 || start >= end || end > timeRange.length / 2 || colors == null || colors.length < 4 * end)
            return null;
        for (int i = 2 * start, it = 4 * tStart + 3; i < 2 * end; i += 2, it += 4)
            if (currentTime < timeRange[i] || timeRange[i + 1] < currentTime)
                colors[it] = 0;
        return colors;
    }

    public static void setTransparencyMask(byte[] colors, LogicLargeArray mask, int start, int end, int tStart)
    {
        if (colors == null || colors.length < 4 * end)
            return;
        for (int i = start, j = 4 * tStart + 3; i < end; i++, j += 4)
            if (mask.getByte(i) == 0)
                colors[j] = 0;
    }

    public static void setTransparencyMask(byte[] colors, LogicLargeArray mask, int start, int end)
    {
        if (colors == null || colors.length < 4 * end)
            return;
        setTransparencyMask(colors, mask, start, end, 0);
    }

    public static void setTransparencyMask(byte[] colors, LogicLargeArray mask)
    {
        if (colors == null || mask == null || colors.length != 4 * mask.length())
            return;
        setTransparencyMask(colors, mask, 0, (int) mask.length());
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int start, int end, Color3f col, byte[] colors)
    {
        return map(data, dataMappingParams, start, end, start, col, colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int start, int end, byte[] colors)
    {
        return map(data, dataMappingParams, start, end, new Color3f(1, 1, 1), colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, int nData, Color3f col, byte[] colors)
    {
        return map(data, dataMappingParams, 0, (int) data.getNElements(), col, colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, Color3f col, byte[] colors)
    {
        return map(data, dataMappingParams, (int) data.getNElements(), col, colors);
    }

    public static byte[] map(DataContainer data, DataMappingParams dataMappingParams, byte[] colors)
    {
        return map(data, dataMappingParams, (int) data.getNElements(), new Color3f(1, 1, 1), colors);
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, int start, int end, byte[] colors)
    {
        if (colors == null || colors.length < 4 * end)
            return null;
        return mapTransparency(data, params, start, end, start, colors);
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, int nData, byte[] colors)
    {
        return mapTransparency(data, params, 0, (int) data.getNElements(), colors);
    }

    public static byte[] mapTransparency(DataContainer data, TransparencyParams params, byte[] colors)
    {
        return mapTransparency(data, params, (int) data.getNElements(), colors);
    }

    private ColorMapper()
    {
    }
}
