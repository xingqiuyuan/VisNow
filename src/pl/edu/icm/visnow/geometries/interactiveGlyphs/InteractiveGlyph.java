//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.geometries.interactiveGlyphs;

import javax.media.j3d.Node;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickType;

/**
 *
 * @author know
 */


public class InteractiveGlyph extends OpenBranchGroup 
        
{
    public static enum GlyphType {BOX, PLANE, LINE, SPHERE, CIRCLE, RECTANGLE, POINT, PLANAR_LINE};
    
    protected OpenBranchGroup intermediate = new OpenBranchGroup("intermediate");
    protected Node additionalGeometry =  null;
    protected Glyph glyph;
    protected InteractiveGlyphParams params = new InteractiveGlyphParams(this);
    protected InteractiveGlyphGUI computeUI = new InteractiveGlyphGUI();
    
    public InteractiveGlyph()
    {
        setName("interactiveGlyph");
        glyph = new SphereGlyph(params);
        computeUI.setParams(params);
        addChild(intermediate);
        
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                update();
            }    
        });
    }
    
    public InteractiveGlyph(GlyphType type)
    {
        switch (type) {
            case PLANE:
                glyph = new PlaneGlyph(params);
                break;
            case SPHERE:
                glyph = new SphereGlyph(params);
                break;
            case LINE:
                glyph = new LineGlyph(params);
                break;
            case RECTANGLE:
                glyph = new RectangleGlyph(params);
                break;
            case CIRCLE:
                glyph = new CircleGlyph(params);
                break;
            case POINT:
                glyph = new PointGlyph(params);
                break;
            case PLANAR_LINE:
                glyph = new PlanarLineGlyph(params);
                break;
            default:
                glyph = new BoxGlyph(params);
                break;
        }
        computeUI.setParams(params);
        intermediate.addChild(glyph);
    }
    
    public void setType(GlyphType type)
    {
        if (type == glyph.getType())
            return;
        if (glyph != null)
            glyph.detach();
        switch (type) {
            case PLANE:
                glyph = new PlaneGlyph(params);
                break;
            case SPHERE:
                glyph = new SphereGlyph(params);
                break;
            case LINE:
                glyph = new LineGlyph(params);
                break;
            case RECTANGLE:
                glyph = new RectangleGlyph(params);
                break;
            case CIRCLE:
                glyph = new CircleGlyph(params);
                break;
            case POINT:
                glyph = new PointGlyph(params);
                break;
            case PLANAR_LINE:
                glyph = new PlanarLineGlyph(params);
                break;
            default:
                glyph = new BoxGlyph(params);
                break;
        }
        glyph.update();
        intermediate.addChild(glyph);
        computeUI.updateWidgetVisibility();
        if (computeUI != null)
            computeUI.updateRadiusSliderTitle(glyph.getRadiusSliderTitle());
    }
    
    public void addGeometry(Node geometry)
    {
        intermediate.addChild(geometry);
        additionalGeometry = geometry;
    }
    
    public void setColors(boolean dark)
    {
        params.darkColors(dark);
        if (glyph != null) 
            glyph.updateColors();
    }
    
    public void hide()
    {
        removeAllChildren();
    }
    
    public void show()
    {
        try {
            if (this.numChildren() < 1) {
                addChild(intermediate);
            }
            
        } catch (Exception e) {
        }
    }
    
    public boolean isAdjusting()
    {
        return params.adjusting >= 0;
    }
    
    public void setField(Field field)
    {
        params.setDimension(field.getTrueNSpace() == 2 ? 2 : 3);
        if (field.hasCoords())
            params.setCoords(field.getCoords(0));
        else
        {
            float[][] extents = field.getExtents();
            float[] llb = extents[0];
            float[] xt = new float[3];
            for (int i = 0; i < xt.length; i++)
                xt[i] = extents[1][i] - llb[i];
            float[] c = new float[24];
            for (int i = 0; i < 8; i++) {
                int[] k = {i & 1, (i >> 1) & 1, (i >> 2) & 1};
                for (int j = 0; j < 3; j++) 
                    c[3 * i + j] = llb[j] + k[j] * xt[j];
            }
            params.setCoords(new FloatLargeArray(c));
        }
    }
    
    public void update()
    {
        if (glyph != null) 
            glyph.update();
        if (params.isShow())
            show();
        else
            hide();
    }

    public Glyph getGlyph() {
        return glyph;
    }
    
    public InteractiveGlyphParams getParams() {
        return params;
    }

    public InteractiveGlyphGUI getComputeUI() {
        return computeUI;
    }
    
    public synchronized void addParameterChangelistener(ParameterChangeListener listener)
    {
        params.addParameterChangelistener(listener);
    }

    public synchronized void clearParameterChangelisteners()
    {
        params.clearParameterChangeListeners();
    }
    
    public float[] getBoxVerts(int trueDim)
    {
        float[] boxVerts;
        float[] center = params.getCenter();
        float[][] v = new float[3][3];
        for (int i = 0; i < 3; i++) {
            v[0][i] = params.getuScale() * params.getU()[i];
            v[1][i] = params.getvScale() * params.getV()[i];
            v[2][i] = params.getwScale() * params.getW()[i];
        }
        if (trueDim == 3) {
            boxVerts = new float[24];
            for (int i = -1, m = 0; i < 2; i += 2) 
                for (int j = -1; j < 2; j+= 2) 
                    for (int k = -1; k < 2; k += 2) 
                        for (int l = 0; l < 3; l++, m++) 
                            boxVerts[m] = center[l] + i * v[2][l] + j * v[1][l] + k * v[0][l];
        }
        else {
            boxVerts = new float[12];
            for (int i = -1, m = 0; i < 2; i += 2) 
                for (int j = -1; j < 2; j+= 2)
                        for (int l = 0; l < 3; l++, m++)  
                            boxVerts[m] = center[l] + i * v[1][l] + j * v[0][l]; 
        }
        return boxVerts;       
    }
    
    public Pick3DListener getPick3DListener() {
        return pick3DListener;
    }
    
    /**
     * Pick3DListener
     * <p/>
     * No getter is required if
     * <code>parameters</code> store an object of class
     * <code>Params</code> and that object stores this Pick3DListener and overrides
     * <code>getPick3DListener()</code> method.
     */
    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            params.setCenter(e.getPoint());
        }
    };

}
