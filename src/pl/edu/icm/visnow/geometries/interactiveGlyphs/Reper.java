//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>


package pl.edu.icm.visnow.geometries.interactiveGlyphs;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineStripArray;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class Reper extends OpenBranchGroup
{
    protected IndexedLineStripArray glyphLines       = 
            new IndexedLineStripArray(15,  
                                      GeometryArray.COORDINATES | GeometryArray.COLOR_3, 
                                      18, new int[] {3, 3, 3, 3, 3, 3});
    protected OpenShape3D lineShape                  = new OpenShape3D("lines");
    protected float[]   glyphVerts = new float[45];
    protected float[][] currentColors = InteractiveGlyphParams.LIGHT_COLORS;
    protected float[]   lineColors = {0, .6f, .6f, .6f, .6f, 0, .6f, 0, .6f};
    
    public Reper(InteractiveGlyphParams params)
    {
        super();
        glyphVerts = new float[45];
        update(new float[]{0, 0, 0}, 1, 1, 1,
               new float[]{1, 0, 0}, new float[]{0, 1, 0}, new float[]{0, 0, 1});
        glyphLines.setCoordinateIndices(0, new int[] {0,  1,  2,  3,  1,  4, 
                                                      5,  6,  7,  8,  6,  9,
                                                     10, 11, 12, 13, 11, 14});
        glyphLines.setColorIndices(0, new int[] {0, 0, 0, 0, 0, 0, 
                                                 1, 1, 1, 1, 1, 1, 
                                                 2, 2, 2, 2, 2, 2});
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
        lineShape.addGeometry(glyphLines);
        addChild(lineShape);
        currentColors = params.getCurrentColors();
        for (int i = 0; i < 3; i++)
            System.arraycopy(currentColors[(i + 1) % 3], 0, lineColors, 3 * i, 3);
    }
    
    public void updateColors(float[][] currentColors)
    {
        for (int i = 0; i < 3; i++)
            System.arraycopy(currentColors[(i + 1) % 3], 0, lineColors, 3 * i, 3);
    }
    
    private void addArrow(float[] center, float scale, 
                          float[] uu, float[] vv, float[] ww, int s)
    {
        float t = (float)Math.sqrt(3) * .05f;
        for (int i = 0; i < 3; i++) {
            float c = center[i];
            float u = scale * uu[i];
            float v = scale * vv[i];
            float w = scale * ww[i];
            glyphVerts[s + i]       = c - u;
            glyphVerts[s + 3 + i]   = c + u;
            glyphVerts[s + 6 + i]   = c + .85f * u + .07f * v;
            glyphVerts[s + 9 + i]   = c + .85f * u - .035f * v - t * w;
            glyphVerts[s + 12 + i]  = c + .85f * u - .035f * v + t * w;
        }
    }
    
    public void update(float[] center, 
                       float scaleU, float scaleV, float scaleW,
                       float[] uu, float[] vv, float[] ww)
    {
        float[][] r = new float[][] {uu, vv, ww};
        float[] scale = new float[] {scaleU, scaleV, scaleW};
        for (int i = 0; i < 3; i++) 
            addArrow(center, scale[i], r[i], r[(i + 1) % 3], r[(i + 2) % 3], 15 * i);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
    }
}
