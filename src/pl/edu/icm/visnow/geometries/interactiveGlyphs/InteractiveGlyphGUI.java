//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.geometries.interactiveGlyphs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams.*;
import pl.edu.icm.visnow.gui.widgets.FloatSlider;
import pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider;
import pl.edu.icm.visnow.gui.widgets.UnboundedRoller.UnboundedRoller;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 *
 * @author know
 */
public class InteractiveGlyphGUI extends javax.swing.JPanel
{
    protected boolean rangeTypeGUI = true;
    InteractiveGlyphParams params;
    
    protected int axis = 2;
    protected float scaleFactor = 1;
    protected float[][] colors = InteractiveGlyphParams.LIGHT_COLORS;
    protected Color wColor = new Color(colors[0][0], colors[0][1], colors[0][2]);
    protected Color uColor = new Color(colors[1][0], colors[1][1], colors[1][2]);
    protected Color vColor = new Color(colors[2][0], colors[2][1], colors[2][2]);
    /**
     * Creates new form SimplePlanarSliceGUI
     */
    public InteractiveGlyphGUI()
    {
        initComponents();
        uRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setAdjusting(uRoller.isAdjusting() ? 0 : -1);
                params.setRotation(new float[] {uRoller.getValue(), 
                                                vRoller.getValue(), 
                                                wRoller.getValue()});
            }
        });
        vRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setAdjusting(vRoller.isAdjusting() ? 1 : -1);
                params.setRotation(new float[] {uRoller.getValue(), 
                                                vRoller.getValue(), 
                                                wRoller.getValue()});
            }
        });
        wRoller.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                params.setAdjusting(wRoller.isAdjusting() ? 2 : -1);
                params.setRotation(new float[] {uRoller.getValue(), 
                                                vRoller.getValue(), 
                                                wRoller.getValue()});
            }
        });
        updateSliderBorders(InteractiveGlyphParams.LIGHT_COLORS);
        rangePanel.setVisible(rangeTypeGUI);
        centerPanel.setVisible(!rangeTypeGUI);
    }
    
    public void updateSliderBorders(float[][] colors)
    {
        wColor = new Color(colors[0][0], colors[0][1], colors[0][2]);
        uColor = new Color(colors[1][0], colors[1][1], colors[1][2]);
        vColor = new Color(colors[2][0], colors[2][1], colors[2][2]);
        uRoller.setBackground(uColor);
        vRoller.setBackground(vColor);
        wRoller.setBackground(wColor);
        uSlider.setBorder(BorderFactory.createLineBorder(uColor, 2));
        vSlider.setBorder(BorderFactory.createLineBorder(vColor, 2));
        wSlider.setBorder(BorderFactory.createLineBorder(wColor, 2));
        uRangeSlider.setBorder(BorderFactory.createLineBorder(uColor, 2));
        vRangeSlider.setBorder(BorderFactory.createLineBorder(vColor, 2));
        wRangeSlider.setBorder(BorderFactory.createLineBorder(wColor, 2));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        GridBagConstraints gridBagConstraints;

        buttonGroup1 = new ButtonGroup();
        initOrientationPanel = new JPanel();
        xButton = new JRadioButton();
        yButton = new JRadioButton();
        zButton = new JRadioButton();
        rotationsPanel = new JPanel();
        uRoller = new UnboundedRoller();
        vRoller = new UnboundedRoller();
        wRoller = new UnboundedRoller();
        glyphScaleSlider = new JSlider();
        resetButton = new JButton();
        hideGlyphBox = new JCheckBox();
        rangePanel = new JPanel();
        uRangeSlider = new ExtendedFloatSubRangeSlider();
        vRangeSlider = new ExtendedFloatSubRangeSlider();
        wRangeSlider = new ExtendedFloatSubRangeSlider();
        centerPanel = new JPanel();
        uSlider = new FloatSlider();
        vSlider = new FloatSlider();
        wSlider = new FloatSlider();
        radiusSlider = new JSlider();
        jPanel2 = new JPanel();

        setPreferredSize(new Dimension(220, 844));
        setLayout(new GridBagLayout());

        initOrientationPanel.setBorder(BorderFactory.createTitledBorder("initial orientation"));
        initOrientationPanel.setPreferredSize(new Dimension(220, 50));
        initOrientationPanel.setLayout(new GridLayout(1, 0));

        buttonGroup1.add(xButton);
        xButton.setText("x"); // NOI18N
        xButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                xButtonActionPerformed(evt);
            }
        });
        initOrientationPanel.add(xButton);

        buttonGroup1.add(yButton);
        yButton.setText("y"); // NOI18N
        yButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                yButtonActionPerformed(evt);
            }
        });
        initOrientationPanel.add(yButton);

        buttonGroup1.add(zButton);
        zButton.setSelected(true);
        zButton.setText("z"); // NOI18N
        zButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                zButtonActionPerformed(evt);
            }
        });
        initOrientationPanel.add(zButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(initOrientationPanel, gridBagConstraints);

        rotationsPanel.setBorder(BorderFactory.createTitledBorder("rotations (in local coordinate system)"));
        rotationsPanel.setMinimumSize(new Dimension(190, 47));
        rotationsPanel.setPreferredSize(new Dimension(220, 50));
        rotationsPanel.setLayout(new GridBagLayout());

        uRoller.setBackground(uColor);
        uRoller.setMinimumSize(new Dimension(60, 20));
        uRoller.setPreferredSize(new Dimension(200, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        rotationsPanel.add(uRoller, gridBagConstraints);

        vRoller.setBackground(vColor);
        vRoller.setMinimumSize(new Dimension(60, 20));
        vRoller.setName(""); // NOI18N
        vRoller.setPreferredSize(new Dimension(200, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        rotationsPanel.add(vRoller, gridBagConstraints);

        wRoller.setBackground(wColor);
        wRoller.setMinimumSize(new Dimension(60, 20));
        wRoller.setName(""); // NOI18N
        wRoller.setPreferredSize(new Dimension(200, 30));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        rotationsPanel.add(wRoller, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(4, 0, 4, 0);
        add(rotationsPanel, gridBagConstraints);

        glyphScaleSlider.setMajorTickSpacing(20);
        glyphScaleSlider.setMinorTickSpacing(5);
        glyphScaleSlider.setPaintLabels(true);
        glyphScaleSlider.setPaintTicks(true);
        glyphScaleSlider.setBorder(BorderFactory.createTitledBorder("glyph scale"));
        glyphScaleSlider.setMaximumSize(new Dimension(32767, 65));
        glyphScaleSlider.setMinimumSize(new Dimension(36, 55));
        glyphScaleSlider.setOpaque(false);
        glyphScaleSlider.setPreferredSize(new Dimension(200, 60));
        glyphScaleSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                glyphScaleSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(glyphScaleSlider, gridBagConstraints);

        resetButton.setText("reset");
        resetButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                resetButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        add(resetButton, gridBagConstraints);

        hideGlyphBox.setText("hide glyph");
        hideGlyphBox.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                hideGlyphBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(hideGlyphBox, gridBagConstraints);

        rangePanel.setBorder(BorderFactory.createTitledBorder("box geometric dimensions"));
        rangePanel.setLayout(new GridBagLayout());

        uRangeSlider.setBorder(BorderFactory.createLineBorder(new Color(204, 0, 102)));
        uRangeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                uRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        rangePanel.add(uRangeSlider, gridBagConstraints);

        vRangeSlider.setBorder(BorderFactory.createLineBorder(new Color(51, 102, 255)));
        vRangeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                vRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        rangePanel.add(vRangeSlider, gridBagConstraints);

        wRangeSlider.setBorder(BorderFactory.createLineBorder(new Color(0, 204, 0)));
        wRangeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                wRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        rangePanel.add(wRangeSlider, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(rangePanel, gridBagConstraints);

        centerPanel.setLayout(new GridBagLayout());

        uSlider.setBorder(BorderFactory.createLineBorder(uColor, 2));
        uSlider.setMin(-1.0F);
        uSlider.setShowingFields(false);
        uSlider.setVal(0.0F);
        uSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                uSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        centerPanel.add(uSlider, gridBagConstraints);

        vSlider.setBorder(BorderFactory.createLineBorder(vColor, 2));
        vSlider.setMin(-1.0F);
        vSlider.setShowingFields(false);
        vSlider.setVal(0.0F);
        vSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                vSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        centerPanel.add(vSlider, gridBagConstraints);

        wSlider.setBorder(BorderFactory.createLineBorder(wColor, 2));
        wSlider.setMin(-1.0F);
        wSlider.setShowingFields(false);
        wSlider.setVal(0.0F);
        wSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                wSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        centerPanel.add(wSlider, gridBagConstraints);

        radiusSlider.setMajorTickSpacing(20);
        radiusSlider.setMinorTickSpacing(5);
        radiusSlider.setPaintLabels(true);
        radiusSlider.setPaintTicks(true);
        radiusSlider.setBorder(BorderFactory.createTitledBorder("circle/sphere radius"));
        radiusSlider.setMaximumSize(new Dimension(32767, 65));
        radiusSlider.setMinimumSize(new Dimension(36, 55));
        radiusSlider.setOpaque(false);
        radiusSlider.setPreferredSize(new Dimension(200, 60));
        radiusSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                radiusSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        centerPanel.add(radiusSlider, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(centerPanel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel2, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void xButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_xButtonActionPerformed
    {//GEN-HEADEREND:event_xButtonActionPerformed
        setAxis();
    }//GEN-LAST:event_xButtonActionPerformed

    private void yButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_yButtonActionPerformed
    {//GEN-HEADEREND:event_yButtonActionPerformed
        setAxis();
    }//GEN-LAST:event_yButtonActionPerformed

    private void zButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_zButtonActionPerformed
    {//GEN-HEADEREND:event_zButtonActionPerformed
        setAxis();
    }//GEN-LAST:event_zButtonActionPerformed

    private void wSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_wSliderStateChanged
    {//GEN-HEADEREND:event_wSliderStateChanged
        
        params.setAdjusting(wSlider.isAdjusting() ? 5 : -1);
        params.setShiftW(wSlider.getVal());
        if (wSlider.isAdjusting())
            return;
        float s = wSlider.getVal();
        wSlider.setMin(wSlider.getMin() - s);
        wSlider.setMax(wSlider.getMax() - s);
        wSlider.setVal(0);
    }//GEN-LAST:event_wSliderStateChanged

    private void glyphScaleSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_glyphScaleSliderStateChanged
    {//GEN-HEADEREND:event_glyphScaleSliderStateChanged
        params.setScale(scaleFactor * (float)Math.exp((glyphScaleSlider.getValue() - 60) / 20.f));
    }//GEN-LAST:event_glyphScaleSliderStateChanged

    private void uSliderStateChanged(ChangeEvent evt) {//GEN-FIRST:event_uSliderStateChanged
        params.setAdjusting(uSlider.isAdjusting() ? 3 : -1); 
        params.setShiftU(uSlider.getVal());
        if (uSlider.isAdjusting())
            return;
        float s = uSlider.getVal();
        uSlider.setMin(uSlider.getMin() - s);
        uSlider.setMax(uSlider.getMax() - s);
        uSlider.setVal(0);
    }//GEN-LAST:event_uSliderStateChanged

    private void vSliderStateChanged(ChangeEvent evt) {//GEN-FIRST:event_vSliderStateChanged
        params.setAdjusting(vSlider.isAdjusting() ? 4 : -1);
        params.setShiftV(vSlider.getVal());
        if (vSlider.isAdjusting())
            return;
        float s = vSlider.getVal();
        vSlider.setMin(vSlider.getMin() - s);
        vSlider.setMax(vSlider.getMax() - s);
        vSlider.setVal(0);
    }//GEN-LAST:event_vSliderStateChanged

    private boolean rangeSliderUpdating = false;
    
    private void uRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_uRangeSliderStateChanged
    {//GEN-HEADEREND:event_uRangeSliderStateChanged
        if (rangeSliderUpdating)
            return;
        params.setAdjusting(uRangeSlider.isAdjusting() ? 3 : -1);
        float s = (uRangeSlider.getUp() + uRangeSlider.getLow()) / 2;
        float r = (uRangeSlider.getUp() - uRangeSlider.getLow()) / 2;
        params.setuScale(r);
        params.setShiftU(s);
        if (uRangeSlider.isAdjusting())
            return;
        rangeSliderUpdating = true;
        uRangeSlider.setMinMax(uRangeSlider.getMin() - s, uRangeSlider.getMax() - s);
        uRangeSlider.setLowUp(-r, r);
        rangeSliderUpdating = false;
    }//GEN-LAST:event_uRangeSliderStateChanged

    private void vRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_vRangeSliderStateChanged
    {//GEN-HEADEREND:event_vRangeSliderStateChanged
        if (rangeSliderUpdating)
            return;
        params.setAdjusting(vRangeSlider.isAdjusting() ? 4 : -1);
        float s = (vRangeSlider.getUp() + vRangeSlider.getLow()) / 2;
        float r = (vRangeSlider.getUp() - vRangeSlider.getLow()) / 2;
        params.setvScale(r);
        params.setShiftV(s);
        if (vRangeSlider.isAdjusting())
            return;
        rangeSliderUpdating = true;
        vRangeSlider.setMinMax(vRangeSlider.getMin() - s, vRangeSlider.getMax() - s);
        vRangeSlider.setLowUp(-r, r);
        rangeSliderUpdating = false;
    }//GEN-LAST:event_vRangeSliderStateChanged

    private void wRangeSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_wRangeSliderStateChanged
    {//GEN-HEADEREND:event_wRangeSliderStateChanged
        if (rangeSliderUpdating)
            return;
        params.setAdjusting(wRangeSlider.isAdjusting() ? 5 : -1);
        float s = (wRangeSlider.getUp() + wRangeSlider.getLow()) / 2;
        float r = (wRangeSlider.getUp() - wRangeSlider.getLow()) / 2;
        params.setwScale(r);
        params.setShiftW(s);
        if (wRangeSlider.isAdjusting())
            return;
        rangeSliderUpdating = true;
        wRangeSlider.setMinMax(wRangeSlider.getMin() - s, wRangeSlider.getMax() - s);
        wRangeSlider.setLowUp(-r, r);
        rangeSliderUpdating = false;
    }//GEN-LAST:event_wRangeSliderStateChanged

    private void hideGlyphBoxActionPerformed(ActionEvent evt) {//GEN-FIRST:event_hideGlyphBoxActionPerformed
        params.setShow(!hideGlyphBox.isSelected());
    }//GEN-LAST:event_hideGlyphBoxActionPerformed

    private void resetButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
        params.reset();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void radiusSliderStateChanged(ChangeEvent evt)//GEN-FIRST:event_radiusSliderStateChanged
    {//GEN-HEADEREND:event_radiusSliderStateChanged
        params.setAdjusting(radiusSlider.getValueIsAdjusting() ? 6 : -1);
        params.setRadius(scaleFactor * (float)Math.exp((radiusSlider.getValue() - 60) / 20.f));
    }//GEN-LAST:event_radiusSliderStateChanged

    
    public void updateSliderBounds(boolean resetVal)
    {
        params.setActive(false);
        if (resetVal) {
            uRangeSlider.setParams(params.getuMinMax()[0], params.getuMinMax()[1],
                                   params.getuMinMax()[0], params.getuMinMax()[1]);
            vRangeSlider.setParams(params.getvMinMax()[0], params.getvMinMax()[1],
                                   params.getvMinMax()[0], params.getvMinMax()[1]);
            wRangeSlider.setParams(params.getwMinMax()[0], params.getwMinMax()[1],
                                   params.getwMinMax()[0], params.getwMinMax()[1]);
        } 
        else {
            uRangeSlider.setMinMax(params.getuMinMax()[0], params.getuMinMax()[1]);
            vRangeSlider.setMinMax(params.getvMinMax()[0], params.getvMinMax()[1]);
            wRangeSlider.setMinMax(params.getwMinMax()[0], params.getwMinMax()[1]);
        }
        uSlider.setMinMax(params.getuMinMax()[0], params.getuMinMax()[1]);
        vSlider.setMinMax(params.getvMinMax()[0], params.getvMinMax()[1]);
        wSlider.setMinMax(params.getwMinMax()[0], params.getwMinMax()[1]);
        if (resetVal) {
            uSlider.setVal((params.getuMinMax()[0] + params.getuMinMax()[1]) / 2);
            vSlider.setVal((params.getvMinMax()[0] + params.getvMinMax()[1]) / 2);
            wSlider.setVal((params.getwMinMax()[0] + params.getwMinMax()[1]) / 2);
        }
        scaleFactor = (params.getuMinMax()[1] + params.getvMinMax()[1] + params.getwMinMax()[1]) / 3;
        glyphScaleSlider.setValue(60);
        params.setActive(true);
        params.setScale(scaleFactor);
        params.setRadius(scaleFactor * (float)Math.exp((radiusSlider.getValue() - 60) / 20.f));
    }
    
    public void clearRotationRollers()
    {
        uRoller.setValue(0);
        vRoller.setValue(0);
        wRoller.setValue(0);
    }

    private void setAxis()
    {
        if (xButton.isSelected())
            axis = 0;
        else if (yButton.isSelected())
            axis = 1;
        else if (zButton.isSelected())
            axis = 2;
        params.setActive(false);
        wSlider.setVal(0);
        updateSliderBounds(true);
        clearRotationRollers();
        params.setActive(true);
        params.setAxis(axis);
    }

    public void setParams(InteractiveGlyphParams params)
    {
        this.params = params;
        params.setGUI(this);
        updateWidgetVisibility();
    }
    
    public void updateWidgetVisibility()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                int dim = params.getDimension();
                int vis = params.getVisibleWidgets();
                rangeTypeGUI = (vis & (U_RANGE_VIS | V_RANGE_VIS | W_RANGE_VIS)) != 0;
                uRoller.setVisible((vis & U_ROT_VIS) != 0);
                vRoller.setVisible((vis & V_ROT_VIS) != 0);
                wRoller.setVisible((vis & W_ROT_VIS) != 0);
                uRangeSlider.setVisible((vis & U_RANGE_VIS) != 0);
                vRangeSlider.setVisible((vis & V_RANGE_VIS) != 0);
                wRangeSlider.setVisible((vis & W_RANGE_VIS) != 0);
                uSlider.setVisible((vis & U_TRANS_VIS) != 0);
                vSlider.setVisible((vis & V_TRANS_VIS) != 0);
                wSlider.setVisible((vis & W_TRANS_VIS) != 0);
                glyphScaleSlider.setVisible((vis & SCALE_VIS) != 0);
                radiusSlider.setVisible((vis & RADIUS_VIS) != 0);
                initOrientationPanel.setVisible((vis & AXES_VIS) != 0);
                zButton.setVisible(dim == 3);
                rangePanel.setVisible(rangeTypeGUI);
                centerPanel.setVisible(!rangeTypeGUI);
                zButton.setSelected(dim == 3);
                xButton.setSelected(dim == 2);
            }
        });
    }
    
    public void updateRadiusSliderTitle(String title)
    {
        ((TitledBorder)radiusSlider.getBorder()).setTitle(title);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected ButtonGroup buttonGroup1;
    protected JPanel centerPanel;
    protected JSlider glyphScaleSlider;
    protected JCheckBox hideGlyphBox;
    protected JPanel initOrientationPanel;
    protected JPanel jPanel2;
    protected JSlider radiusSlider;
    protected JPanel rangePanel;
    protected JButton resetButton;
    protected JPanel rotationsPanel;
    protected ExtendedFloatSubRangeSlider uRangeSlider;
    protected UnboundedRoller uRoller;
    protected FloatSlider uSlider;
    protected ExtendedFloatSubRangeSlider vRangeSlider;
    protected UnboundedRoller vRoller;
    protected FloatSlider vSlider;
    protected ExtendedFloatSubRangeSlider wRangeSlider;
    protected UnboundedRoller wRoller;
    protected FloatSlider wSlider;
    protected JRadioButton xButton;
    protected JRadioButton yButton;
    protected JRadioButton zButton;
    // End of variables declaration//GEN-END:variables
}
