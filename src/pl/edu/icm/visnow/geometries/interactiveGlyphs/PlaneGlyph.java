//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>


package pl.edu.icm.visnow.geometries.interactiveGlyphs;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineArray;
import javax.media.j3d.IndexedQuadArray;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.PLANE;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class PlaneGlyph extends Glyph
{
    protected float[] planeVerts = {-1, -1, 0,
                                     1, -1, 0,
                                     1,  1, 0,
                                    -1,  1, 0};
    
    public PlaneGlyph(InteractiveGlyphParams params)
    {
        super(params);
        reper = new Reper(params);
        type = PLANE;
        setName("plane glyph");
        visibleWidgets = U_ROT_VIS   | V_ROT_VIS   | W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS | W_TRANS_VIS | 
                         SCALE_VIS   | AXES_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {-1, -1, 0,
                                   1, -1, 0,
                                   1,  1, 0,
                                  -1,  1, 0,
                                   0,  0, 0,
                                   0,  0, 1.5f};
        glyphRects = new IndexedQuadArray(4, GeometryArray.COORDINATES, 4);
        glyphRects.setCoordinateIndices(0, new int[] {0, 1, 2, 3});
        glyphRects.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphRects.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphRects.setCoordinates(0, planeVerts);
        surfShape.setAppearance(surfApp);
        surfShape.addGeometry(glyphRects);
        glyphGroup.addChild(surfShape);
        currentColors = params.getCurrentColors();
        addChild(reper);
    }
    
    @Override
    public void updateColors()
    {
        reper.updateColors(currentColors);
    }
    
    @Override
    public void update()
    {
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float u = params.scale * params.u[i];
            float v = params.scale * params.v[i];
            
            planeVerts[i]      = c - u - v;
            planeVerts[3 + i]  = c + u - v;
            planeVerts[6 + i]  = c + u + v;
            planeVerts[9 + i]  = c - u + v;
        }
        glyphRects.setCoordinates(0, planeVerts);
        float s = params.scale;
        reper.update(params.center, .3f * s, .3f * s, .6f * s, 
                     params.u, params.v, params.w);
    }
}
