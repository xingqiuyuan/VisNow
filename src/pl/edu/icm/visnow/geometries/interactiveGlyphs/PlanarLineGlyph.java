//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>


package pl.edu.icm.visnow.geometries.interactiveGlyphs;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineArray;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.PLANAR_LINE;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class PlanarLineGlyph extends Glyph
{
    protected float[] lineColors = { 1, 0, 0, 0, 0, 1};

    
    public PlanarLineGlyph(InteractiveGlyphParams params)
    {
        super(params);
        type = PLANAR_LINE;
        setName("plaar line glyph");
        visibleWidgets = W_ROT_VIS |
                         U_TRANS_VIS | V_TRANS_VIS |
                         SCALE_VIS   | X_AXIS_VIS | Y_AXIS_VIS;
        params.setVisibleWidgets(visibleWidgets);
        glyphVerts = new float[] {-1.f,    0,  0,
                                   1.f,    0,  0,
                                     0, -.3f,  0,
                                     0,  .3f,  0};
        glyphLines = new IndexedLineArray(6,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, 6);
        glyphLines.setCoordinateIndices(0, new int[] {0, 1, 2, 3});
        glyphLines.setColorIndices(0, new int[] {0, 0, 1, 1});
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
        glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
        lineShape.setAppearance(lineApp);
        lineShape.addGeometry(glyphLines);
        params.setAxis(0);
        glyphGroup.addChild(lineShape);
    }
    
    @Override
    public void update()
    {
        for (int i = 0; i < 2; i++) 
            for (int j = 0; j < 3; j++) 
                lineColors[3 * i + j] = currentColors[i + 1][j];
        for (int i = 0; i < 3; i++) {
            float c = params.center[i];
            float u = params.scale * params.u[i];
            float v = params.scale * params.v[i];
            
            glyphVerts[i]      = c - u;
            glyphVerts[3 + i]  = c + u;
            glyphVerts[6 + i]  = c - .3f * v;
            glyphVerts[9 + i]  = c + .3f * v;
        }
        glyphLines.setCoordinates(0, glyphVerts);
        glyphLines.setColors(0, lineColors);
    }
}
