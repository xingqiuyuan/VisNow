//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>


package pl.edu.icm.visnow.geometries.interactiveGlyphs;

import java.util.ArrayList;
import java.util.Arrays;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class InteractiveGlyphParams
{
    public static final int U_ROT_VIS   = 1;
    public static final int V_ROT_VIS   = 1 << 1;
    public static final int W_ROT_VIS   = 1 << 2;
    public static final int U_TRANS_VIS = 1 << 3;
    public static final int V_TRANS_VIS = 1 << 4;
    public static final int W_TRANS_VIS = 1 << 5;
    public static final int U_RAD_VIS   = 1 << 6;
    public static final int V_RAD_VIS   = 1 << 7;
    public static final int W_RAD_VIS   = 1 << 8;
    public static final int U_RANGE_VIS = 1 << 9;
    public static final int V_RANGE_VIS = 1 << 10;
    public static final int W_RANGE_VIS = 1 << 11;
    public static final int SCALE_VIS   = 1 << 12;
    public static final int AXES_VIS    = 7 << 13;
    public static final int X_AXIS_VIS  = 1 << 13;
    public static final int Y_AXIS_VIS  = 1 << 14;
    public static final int Z_AXIS_VIS  = 1 << 15;
    public static final int RADIUS_VIS  = 1 << 16;
    
    protected InteractiveGlyph glyph;
    protected InteractiveGlyphGUI gui;
    protected int visibleWidgets = 0;
    protected String scaleSliderTitle = "glyph scale";
    
    protected FloatLargeArray coords;
    public static final float[][] LIGHT_COLORS = {{0,  1,  1},   {1,  0,  1},   {1,  1,  0}};
    public static final float[][] DARK_COLORS  = {{0,.6f,.6f}, {.6f,  0,.6f}, {.6f,.6f,  0}};
    protected float[][] currentColors = LIGHT_COLORS;
    
    protected boolean active = true;

    protected int     dimension = 3;
    protected int     axis      = 2;
    protected float   shiftU    = 0;
    protected float[] uMinMax   = {0, 0};
    protected float   shiftV    = 0;
    protected float[] vMinMax   = {0, 0};
    protected float   shiftW    = 0;
    protected float[] wMinMax   = {0, 0};
    protected float[] rotations = {0, 0, 0};
    
    protected int adjusting = -1;
    
    protected float   radius      = .5f;
    protected float   scale       = 1.f;
    protected float   uScale      = 1.f;
    protected float   vScale      = 1.f;
    protected float   wScale      = 1.f;
    protected boolean show        = true;
    protected float[] center      = {0, 0, 0}; 
    protected float[] u           = {1, 0, 0}; 
    protected float[] v           = {0, 1, 0}; 
    protected float[] w           = {0, 0, 1};
    protected float[] baseCenter  = {0, 0, 0};  
    protected float[] baseU       = {1, 0, 0}; 
    protected float[] baseV       = {0, 1, 0}; 
    protected float[] baseW       = {0, 0, 1}; 

    public InteractiveGlyphParams()
    {
    }
    
    public InteractiveGlyphParams(InteractiveGlyph parent)
    {
        this.glyph = parent;
    }

    public void setGUI(InteractiveGlyphGUI gui) {
        this.gui = gui;
    }
    
    public void setGlyph(InteractiveGlyph glyph)
    {
        this.glyph = glyph;
    }
    
    public GlyphType getGlyphType()
    {
       return glyph.getGlyph().type; 
    }

    public float[][] getCurrentColors() {
        return currentColors;
    }

    public void setCurrentColors(float[][] currentColors) {
        this.currentColors = currentColors;
        if (glyph != null) 
            glyph.update();
    }
    
    public void darkColors(boolean dark)
    {
        setCurrentColors(dark ? DARK_COLORS : LIGHT_COLORS);
        if (gui != null)
            gui.updateSliderBorders(currentColors);
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
        if (gui != null)
            gui.updateWidgetVisibility();
    }
    
    public void setCoords(FloatLargeArray coords) 
    {
        this.coords = coords;
        if (coords != null)
            reset();
    }
    
    public void reset()
    {
        for (int i = 0; i < 3; i++) 
            baseU[i] = baseV[i] = baseW[i] = u[i] = v[i] = w[i] = 0;
        baseU[0] = baseV[1] = baseW[2] = u[0] = v[1] = w[2] = 1;
        resetPosition();
    }
    
    public void resetPosition()
    {
        long n = coords.length();
        float[][] dir = {u, v, w};
        float[][] minmax = {uMinMax, vMinMax, wMinMax};
        Arrays.fill(center, 0);
        for (int d = 0; d < 3; d++) {
            float rmin = Float.MAX_VALUE;
            float rmax = -Float.MAX_VALUE;
            for (long i = 0; i < n; i += 3) {
                float f = dir[d][0] * coords.get(i) +
                          dir[d][1] * coords.get(i + 1) +
                          dir[d][2] * coords.get(i + 2);
                if (f < rmin)
                    rmin = f;
                if (f > rmax)
                    rmax = f;
            }
            float r = (rmin + rmax)/ 2;
            for (int i = 0; i < 3; i++) 
                center[i] += r * dir[d][i];
            r = (rmax - rmin)/ 2;
            minmax[d][0] = -r;
            minmax[d][1] = r;
        }
        uScale = uMinMax[1];
        vScale = vMinMax[1];
        wScale = wMinMax[1];
        scale = radius = Math.max(uScale, Math.max(vScale, wScale));
        
        glyph.update();
        storeReper();
        if (gui != null)
            gui.updateSliderBounds(true);
    }

    public void setAdjusting(int adjusting) {
        this.adjusting = adjusting;
    }

    public float getScale() {
        return scale;
    }

    public float getuScale() {
        return uScale;
    }

    public float getvScale() {
        return vScale;
    }

    public float getwScale() {
        return wScale;
    }

    public float[] getCenter() {
        return center;
    }

    public float[] getU() {
        return u;
    }

    public float[] getV() {
        return v;
    }

    public float[] getW() {
        return w;
    }

    public float[] getuMinMax() {
        return uMinMax;
    }

    public void setuMinMax(float[] uMinMax) {
        this.uMinMax = uMinMax;
    }

    public float[] getvMinMax() {
        return vMinMax;
    }

    public void setvMinMax(float[] vMinMax) {
        this.vMinMax = vMinMax;
    }

    public float[] getwMinMax() {
        return wMinMax;
    }

    public void setwMinMax(float[] wMinMax) {
        this.wMinMax = wMinMax;
    }

    public float getRadius()
    {
        return radius;
    }

    public void setRadius(float radius)
    {
        this.radius = radius;
        glyph.update();
        fireParameterChanged("glyph");
    }

    public boolean isShow() {
        return show;
    }

   public void setShow(boolean show) {
        this.show = show;
        glyph.update();
    }

    public void setCenter(float[] center) {
        System.arraycopy(center, 0, this.center, 0, center.length);
        updateReper(-1);
        fireParameterChanged("glyph");
    }

    public float getShiftU()
    {
        return shiftU;
    }

    public float getShiftV()
    {
        return shiftV;
    }

    public float getShiftW()
    {
        return shiftW;
    }
    
    public void setShiftU(float shiftU) {
        this.shiftU = shiftU;
        updateReper(adjusting);
        if (adjusting < 0) 
            fireParameterChanged("glyph");
    }

    public void setShiftV(float shiftV) {
        this.shiftV = shiftV;
        updateReper(adjusting);
        if (adjusting < 0) 
            fireParameterChanged("glyph");
    }

    public void setShiftW(float shiftW) {
        this.shiftW = shiftW;
        updateReper(adjusting);
        if (adjusting < 0) 
            fireParameterChanged("glyph");
    }

    public void setRotation(float[] rotations) {
        
        this.rotations = rotations;
        updateReper(adjusting);
        if (adjusting < 0) {
            updateAxesExtents();
            if (gui != null)
                gui.clearRotationRollers();
            fireParameterChanged("glyph");
        }
    }

    public void setScale(float scale) {
        this.scale = scale;
        glyph.update();
    }

    public void setuScale(float uScale) {
        this.uScale = uScale;
        updateReper(adjusting);
        if (adjusting < 0) {
            fireParameterChanged("glyph");
        }
    }

    public void setvScale(float vScale) {
        this.vScale = vScale;
        updateReper(adjusting);
        if (adjusting < 0)
            fireParameterChanged("glyph");
    }

    public void setwScale(float wScale) {
        this.wScale = wScale;
        updateReper(adjusting);
        if (adjusting < 0) {
            fireParameterChanged("glyph");
        }
    }

    public void setAxis(int axis)
    {
        if (axis >= dimension)
            return;
        this.axis = axis;
        if (coords == null)
            return;
        long n = coords.length();
        float[][] dir = {w, u, v};
        if (dimension == 3) {
            for (int i = 0; i < 3; i++) 
                for (int j = 0; j < 3; j++) 
                    dir[i][j] = 0;
            switch (axis) {
            case 0:
                w[0] = u[1] = v[2] = 1;
                break;
            case 1:
                w[1] = u[0] = v[2] = 1;
                break;
            case 2:
                w[2] = u[0] = v[1] = 1;
                break;
            }
        }
        else {
            for (int i = 0; i < 3; i++) 
                for (int j = 0; j < 3; j++) 
                    dir[i][j] = 0;
            if (axis == 0)
                dir[0][2] = dir[1][0] = dir[2][1] = 1;
            else {
                dir[2][0] = dir[0][2] = 1;
                dir[1][1] = -1;
            }
        }
        float[][] minmax = {wMinMax, uMinMax, vMinMax};
        Arrays.fill(center, 0);
        for (int d = 0; d < 3; d++) {
            float rmin = Float.MAX_VALUE;
            float rmax = -Float.MAX_VALUE;
            for (long i = 0; i < n; i += 3) {
                float f = dir[d][0] * coords.get(i) +
                          dir[d][1] * coords.get(i + 1) +
                          dir[d][2] * coords.get(i + 2);
                if (f < rmin)
                    rmin = f;
                if (f > rmax)
                    rmax = f;
            }
            float r = (rmin + rmax)/ 2;
            for (int i = 0; i < 3; i++) 
                center[i] += r * dir[d][i];
            r = (rmax - rmin)/ 2;
            minmax[d][0] = -r;
            minmax[d][1] = r;
        }
        uScale = uMinMax[1];
        vScale = vMinMax[1];
        wScale = wMinMax[1];
        scale = Math.max(uScale, Math.max(vScale, wScale));
        glyph.update();
        storeReper();
        if (gui != null)
            gui.updateSliderBounds(true);
        fireParameterChanged("glyph");
            
    }
    
    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getVisibleWidgets()
    {
        return visibleWidgets;
    }

    public void setVisibleWidgets(int visibleWidgets)
    {
        this.visibleWidgets = visibleWidgets;
    }

    public void updateReper(int adjusting)
    {
        float[][] reper = {u, v, w};
        float[][] baseReper = {baseU, baseV, baseW};
        if (adjusting < 0)
            storeReper();
        else if (adjusting < 3 && dimension == 3 || adjusting == 2) // plane glyphs can be rotated around z axis only
        {
            float[] ux = reper[(adjusting + 1) % 3];
            float[] vx = reper[(adjusting + 2) % 3];
            float[] ub = baseReper[(adjusting + 1) % 3];
            float[] vb = baseReper[(adjusting + 2) % 3];
            double angle = rotations[adjusting] * Math.PI / 180;
            float c0 = (float)Math.cos(angle);
            float s0 = (float)Math.sin(angle);
            for (int i = 0; i < 3; i++) {
                ux[i] = c0 * ub[i] - s0 * vb[i];
                vx[i] = s0 * ub[i] + c0 * vb[i];
            }
        }
        else if (adjusting < 6) {
            float[] shift = {shiftU, shiftV, shiftW};
            if (dimension == 2)
                shiftW = 0;    // plane glyphs can be translated only in xy plane
            for (int i = 0; i < 3; i++) 
                center[i] = baseCenter[i] + shift[adjusting - 3] * reper[adjusting - 3][i];
        }
        glyph.update();
    }
    
    public void storeReper()
    {
        System.arraycopy(center, 0, baseCenter, 0, 3);
        System.arraycopy(w, 0, baseW, 0, 3);
        System.arraycopy(u, 0, baseU, 0, 3);
        System.arraycopy(v, 0, baseV, 0, 3);
    }
 
    public void updateAxesExtents()
    {
        if (coords == null)
            return;
        long n = coords.length();
        float[][] dir = {u, v, w};
        float[][] minmax = {uMinMax, vMinMax, wMinMax};
        float r = 0;
        for (int d = 0; d < 3; d++) {
            float rmin = Float.MAX_VALUE;
            float rmax = -Float.MAX_VALUE;
            for (long i = 0; i < n; i += 3) {
                float f = dir[d][0] * (coords.get(i)     - center[0]) +
                          dir[d][1] * (coords.get(i + 1) - center[1]) +
                          dir[d][2] * (coords.get(i + 2) - center[2]);
                if (f < rmin)
                    rmin = f;
                if (f > rmax)
                    rmax = f;
            }
            minmax[d][0] = rmin;
            minmax[d][1] = rmax;
            if (r < rmin + rmax)
                r = rmin + rmax;
        }
        scale = r;
        if (gui != null)
            gui.updateSliderBounds(false);
    }
       
    //<editor-fold defaultstate="collapsed" desc=" Parameter change listeners ">
    
    /**
     * Utility field holding list of listeners of particular parameters change events
     */
    private ArrayList<ParameterChangeListener> listeners = new ArrayList<>();
    
    /**
     * Registers parameter change listener to receive events.
     * @param listener The listener to register.
     */
    public synchronized void addParameterChangelistener(ParameterChangeListener listener)
    {
        listeners.add(listener);
    }

    /**
     * Removes parameter change listener from the list.
     * @param listener The listener to remove.
     */
    public synchronized void removeParameterChangeListener(ParameterChangeListener listener)
    {
        listeners.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     */
    public synchronized void clearParameterChangeListeners()
    {
        listeners.clear();
    }

    public void fireParameterChanged(String parameter)
    {
        if (active && adjusting == -1)
            for (ParameterChangeListener listener : listeners)
                listener.parameterChanged(parameter);
    }
    //</editor-fold>
}
