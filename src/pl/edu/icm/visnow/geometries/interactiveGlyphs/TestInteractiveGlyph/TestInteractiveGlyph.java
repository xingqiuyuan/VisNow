//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.geometries.interactiveGlyphs.TestInteractiveGlyph;

import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.TestInteractiveGlyph.TestInteractiveGlyphShared.TYPE;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class TestInteractiveGlyph extends OutFieldVisualizationModule
{

    
    public static InputEgg[]  inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected Field inField = null;
    
    protected int mergeCounter = 0;
    protected GlyphType type;
    protected InteractiveGlyph glyph;
    protected InteractiveGlyphParams glyphParams;
    protected InteractiveGlyphGUI glyphUI;
    protected GUI computeUI = new GUI();
    protected ParameterChangeListener glyphListener = new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                System.out.print("center");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getCenter()[i]);
                System.out.println("");
                System.out.print("     u");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getuScale() * glyphParams.getU()[i]);
                System.out.println("");
                System.out.print("     v");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getvScale() * glyphParams.getV()[i]);
                System.out.println("");
                System.out.print("     w");
                for (int i = 0; i < 3; i++) 
                    System.out.printf(" %8.3f", glyphParams.getwScale() * glyphParams.getW()[i]);
                System.out.println("");
            }
        };
    
    public TestInteractiveGlyph()
    {
        type        = BOX;
        glyph       = new InteractiveGlyph(type);
        glyphParams = glyph.getParams();
        glyphUI     = glyph.getComputeUI();
        glyphUI.setParams(glyphParams);
        
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                type        = parameters.get(TYPE);
                outObj.removeNode(glyph);
                glyph.setType(type);
                outObj.addNode(glyph);
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI.setParameters(parameters);
                computeUI.addUI(glyphUI);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        glyph.addParameterChangelistener(glyphListener);
        outObj.addNode(glyph);
    }   

    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(TYPE, BOX)
        };
    }
    
    protected FloatLargeArray fieldCoords = null;
    
    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            inField = newField;
            if (inField.hasCoords())
                fieldCoords = inField.getCoords(0);
            else
            {
                float[][] xt = inField.getExtents();
                float[] coords = new float[24];
                for (int i = 0; i < 8; i++) {
                    int l = i;
                    for (int j = 0; j < 3; j++) {
                        coords[3 * i + j] = xt[l%2][j];
                        l /= 2;
                    }
                }
                fieldCoords = new FloatLargeArray(coords);
            }
            if (0 < inField.getTrueNSpace()) {
                glyphParams.setDimension(inField.getTrueNSpace());
                computeUI.setDimension(inField.getTrueNSpace());
                if (inField.getTrueNSpace() == 3)
                    type = BOX;
                if (inField.getTrueNSpace() == 2)
                    type = RECTANGLE;
                
            }
            glyphParams.setCoords(fieldCoords);
            
            setOutputValue("outField", new VNField(inField));
            
            prepareOutputGeometry();
            outObj.addNode(glyph);
            show();
        }
    }
}
