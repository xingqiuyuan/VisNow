//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.geometries.objects;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineStripArray;
import javax.media.j3d.PointArray;
import org.apache.log4j.Logger;
import pl.edu.icm.jlargearrays.ByteLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LongLargeArray;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.PointFieldDisplayParams;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.geometries.utils.ColorMapper;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import static pl.edu.icm.visnow.geometries.objects.generics.ObjectCapabilities.*;

import static pl.edu.icm.visnow.geometries.objects.RegularFieldGeometry.resetGeometry;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PointFieldGeometry extends FieldGeometry
{

    private static final Logger LOGGER = Logger.getLogger(PointFieldGeometry.class);
    protected OpenShape3D pointShape = new OpenShape3D();
    protected OpenShape3D frameShape = new OpenShape3D();
    protected LogicLargeArray lastMask = null;
    protected int colorMode = DataMappingParams.UNCOLORED;
    protected int currentColorMode = -1;
    protected int nTriangleStrips = 0;
    protected int nLineStrips = 0;
    protected int nNodePoints = 0;
    protected int[] dims = null;
    protected int[] lineStripCounts = null;
    protected int[] triangleStripCounts = null;
    protected PointFieldDisplayParams fieldDisplayParams;
    protected RenderEventListener renderEventListener;
    protected PointArray nodeArr = null;
    protected IndexedLineStripArray boxArr = null;

    public PointFieldGeometry(String name)
    {
        super(name);
        geometries.removeAllChildren();
        geometries.addChild(pointShape);
        geometries.addChild(frameShape);
        renderEventListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (ignoreUpdate)
                    return;
                int extent = e.getUpdateExtent();
                int cMode = dataMappingParams.getColorMode();
                if (currentColorMode < 0) {
                    currentColorMode = cMode;
                    updateShapes();
                    return;
                }
                if (extent == RenderEvent.COLORS) {
                    validateColorMode();
                    if (resetGeometry[currentColorMode][cMode])
                        updateShapes();
                    else
                        updateColors();
                    currentColorMode = cMode;
                    return;
                }
                if (extent == RenderEvent.COORDS)
                    updateCoords();
                if (extent == RenderEvent.GEOMETRY)
                    updateShapes();
                currentColorMode = cMode;
            }
        };
    }

    @Override
    public Field getField()
    {
        return field;
    }

    public boolean setField(Field inField, boolean createParams)
    {
        return setField(inField);
    }

    @Override
    public boolean setField(Field inField)
    {
        if (inField == null)
            return false;
        field = inField;
        return true;
    }

    private void generateNodeIndices()
    {
        if (field.getCurrentMask() == null) {
            nNodePoints = dims[0];
            coordIndices = new IntLargeArray(nNodePoints, false);
            long k = 0;
            for (long j = 0; j < dims[0]; j++, k++) {
                coordIndices.setLong_safe(k, j);
            }
        } else {
            LogicLargeArray mask = field.getCurrentMask();
            nNodePoints = 0;
            for (long i = 0; i < mask.length(); i++) {
                if (mask.getByte(i) == 1)
                    nNodePoints++;
            }
            coordIndices = new IntLargeArray(nNodePoints, true);
            long m = 0;
            for (long j = 0; j < dims[0]; j++) {
                if (mask.getByte(j) == 1)
                    coordIndices.setLong(m++, j);
            }
        }
    }

    public void generateColoredNodes()
    {
        if (dims.length != 1)
            return;
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new PointArray((int) nNodes,
                                 GeometryArray.COORDINATES |
                                 GeometryArray.COLOR_4 |
                                 GeometryArray.USE_COORD_INDEX_ONLY |
                                 GeometryArray.BY_REFERENCE);
        setStandardCapabilities(nodeArr);
        if (detach)
            geometry.postattach();
    }

    public void generateNodes()
    {
        if (dims.length != 1)
            return;
        boolean detach = geometry.postdetach();
        generateNodeIndices();
        if (nNodePoints == 0)
            return;
        nodeArr = new PointArray((int) nNodes,
                                 GeometryArray.COORDINATES |
                                 GeometryArray.USE_COORD_INDEX_ONLY |
                                 GeometryArray.BY_REFERENCE);
        setStandardCapabilities(nodeArr);
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords()
    {
        updateCoords(!ignoreUpdate);
    }

    @Override
    public void updateCoords(boolean force)
    {
        if (!force || field == null)
            return;
        boolean detach = geometry.postdetach();

        //      coords = field.getCurrentCoords();
        if (nodeArr != null)
            nodeArr.setCoordRefFloat(coords.getData());
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords(FloatLargeArray newCoords)
    {
        if (field == null || newCoords.length() != 3 * field.getNNodes()) {
            System.out.println("bad new coords");
            return;
        }
        boolean detach = geometry.postdetach();
        coords = newCoords;
        if (nodeArr != null)
            nodeArr.setCoordRefFloat(coords.getData());
        if (detach)
            geometry.postattach();
    }

    public void updateColors()
    {
        boolean detach = geometry.postdetach();
        if (colors == null || colors.length() != 4 * field.getNNodes())
            colors = new ByteLargeArray(4 * field.getNNodes(), true);
        ColorMapper.map(field, dataMappingParams, renderingParams.getDiffuseColor(), colors.getData());
        ColorMapper.mapTransparency(field, dataMappingParams.getTransparencyParams(), colors.getData());
        if (nodeArr != null)
            nodeArr.setColorRefByte(colors.getData());
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateDataMap()
    {
        boolean detach = geometry.postdetach();
        if (dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED ||
                dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED2D ||
                dataMappingParams.getColorMode() == DataMappingParams.COLORED)
            updateColors();
        if (detach)
            geometry.postattach();
    }

    public void updateShapes()
    {
        updateGeometry((renderingParams.getDisplayMode() & RenderingParams.POINT_CELLS) != 0,
                       (renderingParams.getDisplayMode() & RenderingParams.OUTLINE_BOX) != 0);
        if (geometries.getParent() == null)
            transformedGeometries.addChild(geometries);
        if (transformedGeometries.getParent() == null)
            geometry.addChild(transformedGeometries);
    }

    public void updateData()
    {
        boolean restructure = false;
        LogicLargeArray mask = field.getCurrentMask();
        if (mask != null)
            if (lastMask == null || lastMask.length() != mask.length()) {
                restructure = true;
                lastMask = mask;
            } else {
                for (int i = 0; i < mask.length(); i++)
                    if (mask.getByte(i) != lastMask.getByte(i)) {
                        restructure = true;
                        break;
                    }
            }
        updateExtents();
        if (restructure)
            updateShapes();
        else {
            updateColors();
            updateCoords();
        }
    }

    public void updateGeometry(RegularField inField, boolean showNodes, boolean showBox)
    {
        if (field != inField && !setField(inField))
            return;
        updateGeometry(showNodes, showBox);
    }

    public void updateGeometry(boolean showNodes, boolean showBox)
    {
        boolean detach = geometry.postdetach();
        if (pointShape != null)
            pointShape.removeAllGeometries();
        if (frameShape != null)
            frameShape.removeAllGeometries();
        nodeArr = null;
        boxArr = null;

        if (showNodes) {
            switch (dataMappingParams.getColorMode()) {
                case DataMappingParams.COLORMAPPED:
                case DataMappingParams.RGB:
                    generateColoredNodes();
                    break;
                case DataMappingParams.UVTEXTURED:
                case DataMappingParams.UNCOLORED:
                    generateNodes();
                    break;
            }
            pointShape.addGeometry(nodeArr);
        }
        if (showBox) {
            //updateExtents();
            float[][] ext = field.getPreferredExtents();
            float[] boxVerts = new float[24];
            boxArr = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            });
            boxArr.setCoordinateIndices(0, new int[]{
                0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7
            });
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    for (int k = 0; k < 2; k++) {
                        int m = 3 * (4 * i + 2 * j + k);
                        boxVerts[m] = ext[k][0];
                        boxVerts[m + 1] = ext[j][1];
                        boxVerts[m + 2] = ext[i][2];
                    }
            boxArr.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
            boxArr.setCoordinates(0, boxVerts);
            frameShape.addGeometry(boxArr);
        }

        structureChanged = false;
        updateCoords();
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
            case DataMappingParams.RGB:
                updateColors();
                break;
            case DataMappingParams.UVTEXTURED:
                break;
            case DataMappingParams.UNCOLORED:
                break;
        }
        appearance = renderingParams.getAppearance();
        appearance.setUserData(this);
        lineAppearance = renderingParams.getLineAppearance();
        lineAppearance.setUserData(this);
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }

        lineAppearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        lineAppearance.setLineAttributes(renderingParams.getLineAppearance().getLineAttributes());
        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
        pointShape.setAppearance(lineAppearance);
        frameShape.setAppearance(lineAppearance);
        if (detach)
            geometry.postattach();
    }

    public OpenBranchGroup getGeometry(RegularField inField)
    {
        updateGeometry(inField);
        return geometry;
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return null;
        field = (RegularField) inField;
        return getGeometry((RegularField) inField);
    }

    @Override
    public void createGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return;
        updateGeometry((RegularField) inField);
    }

    public void updateGeometry(RegularField inField)
    {
        setField(inField);
        updateShapes();
    }

    @Override
    public void updateGeometry(Field inField)
    {
        if (inField instanceof RegularField)
            updateGeometry((RegularField) inField);
    }

    private void validateColorMode()
    {
        colorMode = dataMappingParams.getColorMode();
        // check if color mode and selected components combination is valid; fall back to UNCOLORED otherwise
        switch (dataMappingParams.getColorMode()) {
            case DataMappingParams.COLORMAPPED:
                if (field.getComponent(dataMappingParams.getColorMap0().getDataComponentName()) != null)
                    return;
                break;
            case DataMappingParams.RGB:
                if (field.getComponent(dataMappingParams.getRedParams().getDataComponentName()) != null ||
                        field.getComponent(dataMappingParams.getGreenParams().getDataComponentName()) != null ||
                        field.getComponent(dataMappingParams.getBlueParams().getDataComponentName()) != null)
                    return;
                break;
        }
        colorMode = DataMappingParams.UNCOLORED;
    }

    @Override
    public void updateGeometry()
    {
        if (ignoreUpdate)
            return;
        dataMappingParams.setParentObjectSize((int) nNodes);
        updateShapes();
    }

    @Override
    public OpenBranchGroup getGeometry()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public PointFieldGeometry()
    {
    }
}
