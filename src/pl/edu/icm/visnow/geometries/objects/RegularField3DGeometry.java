//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.geometries.objects;

import javax.media.j3d.BranchGroup;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineStripArray;
import javax.media.j3d.IndexedTriangleStripArray;
import javax.media.j3d.LineStripArray;
import javax.media.j3d.PointArray;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Color3f;
import javax.vecmath.Vector3f;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import static pl.edu.icm.jlargearrays.LargeArrayUtils.arraycopy;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.events.LightDirectionEvent;
import pl.edu.icm.visnow.geometries.events.LightDirectionListener;
import pl.edu.icm.visnow.geometries.objects.generics.OpenAppearance;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenColoringAttributes;
import pl.edu.icm.visnow.geometries.objects.generics.OpenLineAttributes;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.RegularField3DParams;
import pl.edu.icm.visnow.geometries.parameters.PresentationParams;
import pl.edu.icm.visnow.geometries.utils.ColorMapper;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import pl.edu.icm.visnow.geometries.utils.TextureVolumeRenderer;
import pl.edu.icm.visnow.lib.utils.field.RegularFieldProjection;
import pl.edu.icm.visnow.lib.utils.field.SliceRegularField;
import pl.edu.icm.visnow.system.main.VisNow;
import static pl.edu.icm.visnow.geometries.objects.generics.ObjectCapabilities.*;
import pl.edu.icm.visnow.geometries.parameters.VolumeShadingParams;
import static pl.edu.icm.visnow.geometries.parameters.VolumeShadingParams.SHADING_COMPONENT;
import static pl.edu.icm.visnow.geometries.parameters.VolumeShadingParams.SHADING_INTENSITY;
import static pl.edu.icm.visnow.geometries.parameters.VolumeShadingParams.SHADING_RANGE;
import static pl.edu.icm.visnow.geometries.parameters.VolumeShadingParams.SHADING_TYPE;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class RegularField3DGeometry extends RegularFieldGeometry
{

    private class Face
    {
        private RegularField fld = null;

        private FloatLargeArray coords = null;
        private FloatLargeArray normals = null;
        private int[] dims;
        private long nNodes;
        private long nTriangleStrips, nTriangleIndices;
        private IntLargeArray triangleStripCounts, triangleCoordIndices;
        private boolean surfaceOrientation = true;
        private IndexedTriangleStripArray triangleArr = null;
        private byte[] colors = null;

        public void setField(RegularField inField)
        {
            fld = inField;
            dims = fld.getDims();
            nNodes = (long)dims[0] * (long)dims[1];
            updateGeometry();
        }

        public void setSurfaceOrientation(boolean surfaceOrientation)
        {
            this.surfaceOrientation = surfaceOrientation;
        }

        private void generateTriangleIndices()
        {
            nTriangleStrips = dims[1] - 1;
            triangleStripCounts = new IntLargeArray(nTriangleStrips, false);
            for (long i = 0; i < nTriangleStrips; i++)
                triangleStripCounts.setInt(i, 2 * dims[0]);
            nTriangleIndices = 2l * (long) dims[0] * (long) (dims[1] - 1);
            triangleCoordIndices = new IntLargeArray(nTriangleIndices, false);
            if (surfaceOrientation)
                for (long i = 0, k = 0; i < dims[1] - 1; i++)
                    for (long j = 0; j < dims[0]; j++, k += 2) {
                        triangleCoordIndices.setLong(k, i * dims[0] + j);
                        triangleCoordIndices.setLong(k + 1, (i + 1) * dims[0] + j);
                    }
            else {
                for (int i = 0, k = 0; i < dims[1] - 1; i++)
                    for (int j = 0; j < dims[0]; j++, k += 2) {
                        triangleCoordIndices.setLong(k, (i + 1) * dims[0] + j);
                        triangleCoordIndices.setLong(k + 1, i * dims[0] + j);
                    }
            }
        }

        public void generateColoredTriangles()
        {
            if (dims.length != 2)
                return;
            generateTriangleIndices();
            if (nTriangleStrips == 0)
                return;
            triangleArr = new IndexedTriangleStripArray((int) nNodes,
                                                        GeometryArray.COORDINATES |
                                                        GeometryArray.NORMALS |
                                                        GeometryArray.COLOR_4 |
                                                        GeometryArray.USE_COORD_INDEX_ONLY |
                                                        GeometryArray.BY_REFERENCE,
                                                        (int) nTriangleIndices, triangleStripCounts.getData());
            setIndexingCapabilities(triangleArr);
            setStandardCapabilities(triangleArr);
            triangleArr.setCoordinateIndices(0, triangleCoordIndices.getData());
        }

        public void updateCoords()
        {
            if (fld == null)
                return;
            coords = fld.getCurrentCoords();
            if(coords == null) {
                coords = fld.getCoordsFromAffine();
            }
            normals = fld.getNormals() == null ? null : fld.getNormals().clone();
            if (normals == null) {
                normals = new FloatLargeArray(coords.length(), true);
                float[] v = new float[3];
                float[] v0 = new float[3];
                float[] v1 = new float[3];
                for (int i = 0; i < v1.length; i++) {
                    v1[i] = coords.getFloat(6 + i) - coords.getFloat(i);
                    v0[i] = coords.getFloat(3 + i) - coords.getFloat(i);
                }
                v[0] = v0[1] * v1[2] - v0[2] * v0[1];
                v[1] = v0[2] * v1[0] - v0[0] * v0[2];
                v[2] = v0[0] * v1[1] - v0[1] * v0[0];
                float vn = (float) Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
                for (int i = 0; i < v.length; i++)
                    v[i] /= vn;
                for (long i = 0; i < normals.length(); i += 3)
                    arraycopy(v, 0, normals, i, 3);
            }
            if (!surfaceOrientation)
                for (long i = 0; i < normals.length(); i++)
                    normals.setFloat(i, -normals.getFloat(i));
            if (triangleArr != null) {
                triangleArr.setCoordRefFloat(coords.getData());
                triangleArr.setNormalRefFloat(normals.getData());
            }
        }

        private class MapColors implements Runnable
        {

            int nThreads = 1;
            int iThread = 0;
            int nNds = (int) fld.getNNodes();

            public MapColors(int nThreads, int iThread)
            {
                this.nThreads = nThreads;
                this.iThread = iThread;
            }

            @Override
            public void run()
            {
                int kstart = nNds * iThread / nThreads;
                int kend = nNds * (iThread + 1) / nThreads;
                if (iThread == 0) {
                    int a = 1;
                }
                ColorMapper.map(fld, dataMappingParams,
                                kstart, kend, kstart, renderingParams.getDiffuseColor(), colors);

            }
        }

        public void updateColors()
        {
            if (colors == null || colors.length != 4 * regularField.getNNodes())
                colors = new byte[4 * (int) fld.getNNodes()];
            int nThreads = pl.edu.icm.visnow.system.main.VisNow.availableProcessors();
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++) {
                workThreads[iThread] = new Thread(new MapColors(nThreads, iThread));
                workThreads[iThread].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {

                }
            if (triangleArr != null && (triangleArr.getVertexFormat() & GeometryArray.COLOR_4) != 0)
                triangleArr.setColorRefByte(colors);
        }

        public void updateGeometry()
        {
            triangleArr = null;
            generateColoredTriangles();
            updateCoords();
            updateColors();
        }

        public IndexedTriangleStripArray getTriangleArray()
        {
            if (triangleArr == null)
                updateGeometry();
            return triangleArr;
        }
    }

    protected RegularField3DParams content3DParams = null;
    protected VolumeShadingParams shadingParams = null;
    protected FloatLargeArray crds = null;
    protected Face[][] faces = new Face[3][2];
    protected RegularField[][] faceFields = new RegularField[3][2];
    protected OpenLineAttributes boxLineAttr = new OpenLineAttributes();
    protected OpenColoringAttributes boxColorAttr = new OpenColoringAttributes();
    protected OpenAppearance boxApp = new OpenAppearance();
    protected OpenAppearance facesApp = new OpenAppearance();
    protected OpenShape3D outlineShape = new OpenShape3D();
    protected OpenShape3D gridShape = new OpenShape3D();
    protected OpenShape3D facesShape = new OpenShape3D();
    protected int currentFaceMode = -1;
    protected float[] boxVerts;
    protected IndexedLineStripArray box = null;
    protected int nGridVerts;
    protected int[][] gridCrds;
    protected float[] gridVerts;
    protected GeometryArray grid = null;
    protected long[] gridIndices = null;
    protected byte[] cols = null;
    protected TextureVolumeRenderer volRender;
    protected boolean needPow2Texture;
    protected float[] lightDir = new float[] {(float)Math.sqrt(3), (float)Math.sqrt(3), (float)Math.sqrt(3)};
    protected float[] lastLightDir = new float[3];
    static Logger logger = Logger.getLogger(RegularField3DGeometry.class);
    
    
    private final void init()
    {
        needPow2Texture = VisNow.get().getMainConfig().isPower2TextureNeeded();
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 2; j++)
                faces[i][j] = new Face();
        boxApp.setLineAttributes(boxLineAttr);
        boxApp.setColoringAttributes(boxColorAttr);
        facesShape.setAppearance(facesApp);
        geometries.addChild(outlineShape);
        geometries.addChild(gridShape);
        geometries.addChild(facesShape);
        renderEventListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (ignoreUpdate)
                    return;
                if (e.getUpdateExtent() == RenderEvent.COLORS) {
                    updateGridColors();
                    updateFaceColors();
                    if (volRender != null)
                        volRender.updateTextureColors();
                }
                if (e.getUpdateExtent() == RenderEvent.TRANSPARENCY && volRender != null)
                    volRender.updateTextureTransparency();
                else if (e.getUpdateExtent() == RenderEvent.COORDS)
                    updateCoords();
                else if (e.getUpdateExtent() != RenderEvent.GEOMETRY)
                    updateDataMap();
                else
                    updateGeometry();
            }
        };
        
        lightDirectionListener = new LightDirectionListener()
        {
            @Override
            public void lightChanged(LightDirectionEvent e)
            {
                Vector3f ld = new Vector3f(), tld = new Vector3f();
                Transform3D ltr = new Transform3D();
                e.getLight().getLocalToVworld(ltr);
                e.getLight().getDirection(tld);
                ltr.transform(tld, ld);
                ld.get(lightDir);
                boolean newLightDir = false;
                float ldNorm = 0;
                for (int i = 0; i < 3; i++) {
                    if (lastLightDir[i] != lightDir[i])
                        newLightDir = true;
                    lastLightDir[i] = lightDir[i];
                    ldNorm +=  lightDir[i] * lightDir[i];
                }
                ldNorm = 1 / (float)Math.sqrt(ldNorm);
                for (int i = 0; i < 3; i++) 
                    lightDir[i] *= ldNorm;
                if (newLightDir && volRender != null) {
                    volRender.setLightDir(lightDir);
                }
            }
        };
        
    }

    public RegularField3DGeometry(String name)
    {
        super(name);
        init();
    }
        

    public RegularField3DGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams);
        init();
    }

    /**
     * Set the value of fieldDisplayParams
     *
     * @param fieldDisplayParams new value of fieldDisplayParams
     */
    @Override
    public void setFieldDisplayParams(PresentationParams fieldDisplayParams)
    {
        super.setFieldDisplayParams(fieldDisplayParams);
        content3DParams = fieldDisplayParams.getContent3DParams();
        content3DParams.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                updateGeometry();
            }
        });
        shadingParams = content3DParams.getVolumeShadingParams();
        shadingParams.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                switch (name) {
                case SHADING_TYPE:
                    switch (shadingParams.getShadingType()) {
                    case TRANSPARENCY_GRADIENT:
                    case COMPONENT_GRADIENT:
                        if (volRender != null) 
                            volRender.updateShading();
                        break;
                    default:
                    }
                    if (volRender != null) {
                        volRender.setShadingType();
                        volRender.updateTextureColors();
                    }
                    break;
                case SHADING_COMPONENT:
                    if (volRender != null) 
                        volRender.computeComponentGradient();
                    break;
                case SHADING_RANGE:
                    if (volRender != null) 
                        volRender.setGradientRange();
                    break;
                case SHADING_INTENSITY:
                    if (volRender != null) 
                        volRender.setShadingIntensity();
                    break;
                }
            }
        });

        dataMappingParams = fieldDisplayParams.getDataMappingParams();
        dataMappingParams.addRenderEventListener(renderEventListener);
    }

    /**
     * Get the value of field
     *
     * @return the value of field
     */
    public Field getInField()
    {
        return regularField;
    }

    /**
     * Set the value of field
     *
     * @param inField new value of field
     * <p>
     * @return false if inField is empty or is not 3D regular field, true if
     *         successfully set
     */
    @Override
    public boolean setField(RegularField inField)
    {
        currentFaceMode = -1;
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3)
            return false;
        super.setField(inField);
        dims = inField.getDims();
        nNodes = (int) inField.getNNodes();
        coords = !inField.hasCoords() ? null : inField.getCurrentCoords();
        regularField = inField;
        field = regularField;
        fieldDisplayParams.updateFieldSchema(regularField.getSchema(), true);
        makeFaces();
        updateGeometry();
        return true;
    }

    private void updateBoxCoords()
    {
        boolean detach = geometry.postdetach();
        if (coords == null) {
            float[][] af = regularField.getAffine();
            for (int i = 0; i < 3; i++) {
                boxVerts[i] = af[3][i];
                boxVerts[i + 3] = af[3][i] + (dims[0] - 1) * af[0][i];
                boxVerts[i + 6] = af[3][i] + (dims[1] - 1) * af[1][i];
                boxVerts[i + 9] = af[3][i] + (dims[1] - 1) * af[1][i] + (dims[0] - 1) * af[0][i];
                boxVerts[i + 12] = af[3][i] + (dims[2] - 1) * af[2][i];
                boxVerts[i + 15] = af[3][i] + (dims[2] - 1) * af[2][i] + (dims[0] - 1) * af[0][i];
                boxVerts[i + 18] = af[3][i] + (dims[2] - 1) * af[2][i] + (dims[1] - 1) * af[1][i];
                boxVerts[i + 21] = af[3][i] + (dims[2] - 1) * af[2][i] + (dims[1] - 1) * af[1][i] + (dims[0] - 1) * af[0][i];
            }
        } else {
            int k = 0;
            long l = 0;
            int s = 3;
            for (int i = 0; i < dims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * (dims[1] - 1);
            for (int i = 0; i < dims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * dims[1] * (dims[2] - 1);
            for (int i = 0; i < dims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * dims[1] * (dims[2] - 1) + 3 * dims[0] * (dims[1] - 1);
            for (int i = 0; i < dims[0]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);

            // i1 axis edges
            s = 3 * dims[0];
            l = 0;
            for (int i = 0; i < dims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * (dims[0] - 1);
            for (int i = 0; i < dims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * dims[1] * (dims[2] - 1);
            for (int i = 0; i < dims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * dims[1] * (dims[2] - 1) + 3 * (dims[0] - 1);
            for (int i = 0; i < dims[1]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);

            // i2 axis edges
            s = 3 * dims[0] * dims[1];
            l = 0;
            for (int i = 0; i < dims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * (dims[0] - 1);
            for (int i = 0; i < dims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * (dims[1] - 1);
            for (int i = 0; i < dims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
            l = 3 * dims[0] * (dims[1] - 1) + 3 * (dims[0] - 1);
            for (int i = 0; i < dims[2]; i++, l += s)
                for (int j = 0; j < 3; j++, k++)
                    boxVerts[k] = coords.getFloat(l + j);
        }
        box.setCoordinates(0, boxVerts);
        if (detach)
            geometry.postattach();
    }

    private void makeOutlineBox()
    {
        boolean detach = geometry.postdetach();
        outlineShape.removeAllGeometries();
        if (regularField == null) {
            if (detach)
                geometry.postattach();
            return;
        }

        if (coords == null) {
            boxVerts = new float[24];
            box = new IndexedLineStripArray(8, GeometryArray.COORDINATES, 24, new int[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            });

            box.setCoordinateIndices(0, new int[]{
                0, 1, 2, 3, 4, 5, 6, 7, 0, 2, 1, 3, 4, 6, 5, 7, 0, 4, 1, 5, 2, 6, 3, 7
            });
        } else {
            boxVerts = new float[3 * 4 * (dims[0] + dims[1] + dims[2])];
            box = new IndexedLineStripArray(4 * (dims[0] + dims[1] + dims[2]),
                                            GeometryArray.COORDINATES,
                                            4 * (dims[0] + dims[1] + dims[2]),
                                            new int[]{
                                                dims[0], dims[0], dims[0], dims[0],
                                                dims[1], dims[1], dims[1], dims[1],
                                                dims[2], dims[2], dims[2], dims[2]
                                            });

            int[] cInd = new int[4 * (dims[0] + dims[1] + dims[2])];
            for (int i = 0; i < cInd.length; i++)
                cInd[i] = i;
            box.setCoordinateIndices(0, cInd);
        }
        box.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        updateBoxCoords();

        boxLineAttr.setLineWidth(content3DParams.getGridWidth());
        boxColorAttr.setColor(new Color3f(content3DParams.getBoxColor()));
        outlineShape.addGeometry(box);
        outlineShape.setAppearance(boxApp);
        if (detach)
            geometry.postattach();
    }

    private void makeFaces()
    {
        boolean orientation = true;
        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++)
            dimsl[i] = dims[i];
        if (!regularField.hasCoords()) {
            float[][] af = regularField.getAffine();
            float det = 0;
            for (int i = 0; i < 3; i++) {
                det += af[0][i] * af[1][(i + 1) % 3] * af[2][(i + 2) % 3];
                det -= af[2][i] * af[1][(i + 1) % 3] * af[0][(i + 2) % 3];
            }
            orientation = det > 0;
        }
        for (int i = 0; i < 3; i++) {
            faceFields[i][0] = SliceRegularField.sliceField(regularField, i, 0);
            faceFields[i][1] = SliceRegularField.sliceField(regularField, i, dims[i] - 1);
            if (content3DParams.getDataMap() != RegularField3DParams.SLICE) {
                faceFields[i][0].removeComponents();
                faceFields[i][1].removeComponents();
                if (regularField.hasMask()) {
                    LogicLargeArray outMask = RegularFieldProjection.computeMaskProjection(regularField, i);
                    faceFields[i][0].setCurrentMask(outMask);
                    faceFields[i][1].setCurrentMask(outMask);
                    for (int j = 0; j < regularField.getNComponents(); j++) {
                        DataArray da = RegularFieldProjection.dataArrayProjection(regularField.getComponent(j),
                                regularField.getMask(0),
                                dimsl, i, content3DParams.getDataMap());
                        if (da != null) {
                            faceFields[i][0].addComponent(da);
                            faceFields[i][1].addComponent(da.cloneShallow());
                        }
                    }
                } else
                    for (int j = 0; j < regularField.getNComponents(); j++) {
                        DataArray da = RegularFieldProjection.dataArrayProjection(regularField.getComponent(j),
                                                                                  dimsl, i, content3DParams.getDataMap());
                        if (da != null) {
                            faceFields[i][0].addComponent(da);
                            faceFields[i][1].addComponent(da.cloneShallow());
                        }
                    }
            }
            for (int j = 0; j < 2; j++)
                faces[i][j].setField(faceFields[i][j]);
        }
        faces[1][1].setSurfaceOrientation(!orientation);
        faces[0][0].setSurfaceOrientation(!orientation);
        faces[2][0].setSurfaceOrientation(!orientation);
        faces[1][0].setSurfaceOrientation(orientation);
        faces[0][1].setSurfaceOrientation(orientation);
        faces[2][1].setSurfaceOrientation(orientation);

    }

    private void updateGridCoords()
    {
        boolean detach = geometry.postdetach();
        if (coords == null) {
            float[][] af = regularField.getAffine();
            int n = 0;
            for (int j = 0; j < nGridVerts; j++) {
                long i = gridIndices[j];
                int i0 = (int) (i % dims[0]);
                i /= dims[0];
                int i1 = (int) (i % dims[1]);
                int i2 = (int) (i / dims[1]);
                for (int m = 0; m < 3; m++, n++)
                    gridVerts[n] = af[3][m] + i0 * af[0][m] + i1 * af[1][m] + i2 * af[2][m];
            }
        } else {
            int n = 0;
            for (int j = 0; j < nGridVerts; j++) {
                long i = gridIndices[j];
                for (int m = 0; m < 3; m++, n++)
                    gridVerts[n] = coords.getFloat(3 * i + m);
            }
        }
        if (content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_LINES)
            grid.setCoordinates(0, gridVerts);
        else if (content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_POINTS)
            grid.setCoordinates(0, gridVerts);

        if (detach)
            geometry.postattach();
    }

    private void makeGrid()
    {
        int grDens = (dims[0] + dims[1] + dims[2]) / content3DParams.getGridLines();
        if (grDens < 1)
            grDens = 1;
        int[] gridDens = new int[]{
            dims[0] / grDens < 2 ? 2 : dims[0] / grDens,
            dims[1] / grDens < 2 ? 2 : dims[1] / grDens,
            dims[2] / grDens < 2 ? 2 : dims[2] / grDens
        };
        gridCrds = new int[3][];
        for (int i = 0; i < gridCrds.length; i++)
            if (gridDens[i] <= 1)
                gridCrds[i] = new int[]{
                    0, dims[i] - 1
                };
            else {
                int l = (dims[i] - 1) / gridDens[i] + 1;
                gridCrds[i] = new int[(dims[i] - 1) / l + 1];
                int m = (dims[i] - 1) % l;
                for (int j = 0, k = 0; j < gridCrds[i].length; j++) {
                    gridCrds[i][j] = k;
                    k += l;
                    if (j < m)
                        k += 1;
                }
            }
        if (content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_LINES) {
            int nGridLines = gridCrds[0].length * gridCrds[1].length + gridCrds[0].length * gridCrds[2].length + gridCrds[1].length * gridCrds[2].length;
            nGridVerts = gridCrds[0].length * gridCrds[1].length * dims[2] + gridCrds[0].length * gridCrds[2].length * dims[1] + gridCrds[1].length * gridCrds[2].length * dims[0];
            gridIndices = new long[nGridVerts];
            int[] gridPolylines = new int[nGridLines];
            int n = 0, m = 0;
            for (int i = 0; i < gridCrds[1].length; i++)
                for (int j = 0; j < gridCrds[0].length; j++, m++) {
                    gridPolylines[m] = dims[2];
                    long l = (long) gridCrds[1][i] * dims[0] + gridCrds[0][j];
                    for (int k = 0; k < dims[2]; k++, l += (long)dims[0] * (long)dims[1], n++)
                        gridIndices[n] = l;
                }
            for (int i = 0; i < gridCrds[2].length; i++)
                for (int j = 0; j < gridCrds[0].length; j++, m++) {
                    gridPolylines[m] = dims[1];
                    long l = (long) gridCrds[2][i] * dims[0] * dims[1] + gridCrds[0][j];
                    for (int k = 0; k < dims[1]; k++, l += dims[0], n++)
                        gridIndices[n] = l;
                }
            for (int i = 0; i < gridCrds[2].length; i++)
                for (int j = 0; j < gridCrds[1].length; j++, m++) {
                    gridPolylines[m] = dims[0];
                    long l = ((long) gridCrds[2][i] * dims[1] + gridCrds[1][j]) * dims[0];
                    for (int k = 0; k < dims[0]; k++, l += 1, n++)
                        gridIndices[n] = l;
                }

            grid = new LineStripArray(nGridVerts,
                                      GeometryArray.COORDINATES | GeometryArray.COLOR_4,
                                      gridPolylines);
        } else if (content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_POINTS) {
            nGridVerts = gridDens[0] * gridDens[1] * gridDens[2];
            gridIndices = new long[nGridVerts];
            for (int i = 0, n = 0; i < gridCrds[2].length; i++)
                for (int j = 0; j < gridCrds[1].length; j++)
                    for (int k = 0; k < gridCrds[0].length; k++, n++)
                        gridIndices[n] = ((long) gridCrds[2][i] * dims[1] + gridCrds[1][j]) * dims[0] + gridCrds[0][k];
            grid = new PointArray(nGridVerts, GeometryArray.COORDINATES | GeometryArray.COLOR_4);
        }
        gridVerts = new float[3 * nGridVerts];
        grid.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
        grid.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
        gridShape.addGeometry(grid);
        OpenAppearance gridApp = new OpenAppearance();
        gridApp.copyValuesFrom(boxApp);
        gridApp.getPointAttributes().setPointSize(content3DParams.getGridWidth());
        gridShape.setAppearance(gridApp);
        updateGridCoords();
        updateGridColors();
    }

    public void updateGeometry(RegularField inField)
    {
        if (!setField(inField) || content3DParams == null)
            return;
        updateGeometry();
    }

    public void createGeometry(RegularField inField)
    {
        if (!setField(inField))
            return;
        updateGeometry(inField);
    }

    @Override
    public void createGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return;
        createGeometry((RegularField) inField);
    }

    public OpenBranchGroup getGeometry(RegularField inField)
    {
        createGeometry(inField);
        return geometry;
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        if (!(inField instanceof RegularField))
            return null;
        return getGeometry((RegularField) inField);
    }
    
    public LightDirectionListener getLightDirectionListener()
    {
        return lightDirectionListener;
    }

    public void updateFaceColors()
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 2; j++)
                faces[i][j].updateColors();
    }

    public void updateGridColors()
    {
        if (content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_POINTS ||
                 content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_LINES) {
            cols = ColorMapper.mapColorsIndexed(regularField, dataMappingParams, gridIndices,
                                                dataMappingParams.getDefaultColor(), cols);
            grid.setColors(0, cols);
        } else
            boxColorAttr.setColor(new Color3f(content3DParams.getBoxColor()));
    }

    @Override
    public void updateDataMap()
    {
        if (dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED ||
            dataMappingParams.getColorMode() == DataMappingParams.COLORMAPPED2D ||
            dataMappingParams.getColorMode() == DataMappingParams.COLORED)
            updateGridColors();
        updateFaceColors();
    }

    @Override
    public void updateCoords(FloatLargeArray coords)
    {
        this.coords = coords;
        updateCoords(true);
    }

    @Override
    public void updateCoords()
    {
        updateCoords(!ignoreUpdate);
    }

    @Override
    public void updateCoords(boolean force)
    {
        if (!force || regularField == null)
            return;

        boolean detach = geometry.postdetach();
        coords = regularField.getCurrentCoords() == null ? null : regularField.getCurrentCoords();
        if (content3DParams.isOutline())
            makeOutlineBox();
        else
            outlineShape.removeAllGeometries();
        updateBoxCoords();
        if (content3DParams.getGridType() >= RegularField3DParams.GRID_TYPE_POINTS)
            updateGridCoords();
        for (int i = 0; i < 3; i++) {
            SliceRegularField.sliceCoordsUpdate(regularField, i, 0, faceFields[i][0]);
            SliceRegularField.sliceCoordsUpdate(regularField, i, dims[i] - 1, faceFields[i][1]);
            for (int j = 0; j < 2; j++)
                faces[i][j].updateCoords();
        }
        if (detach)
            geometry.postattach();
    }

    protected int lastDir = 0;

    public void updateProjection(LocalToWindow locToWin)
    {
        if (volRender == null || locToWin == null)
            return;

        int d = 0;

        if (volRender.getNormalVectors() != null) {
            Transform3D t3d = new Transform3D();
            volRender.getNormalVectors().getTransform(t3d);
            d = locToWin.getDir(t3d);
        } else
            d = locToWin.getDir();

        if (d != lastDir || (d > 0 && volRender.getDir() != lastDir - 1) || (d <= 0 && volRender.getDir() != -lastDir - 1)) {
            if (d > 0)
                volRender.setDir(d - 1, false);
            else
                volRender.setDir(-d - 1, true);
            lastDir = d;
        }
    }

    @Override
    public void updateGeometry(Field inField)
    {
        if (inField instanceof RegularField)
            updateGeometry((RegularField) inField);
    }

    @Override
    public void updateGeometry()
    {
        if (content3DParams == null || ignoreUpdate)
            return;

        boolean detach = geometry.postdetach();
        renderingParams.setCullMode(content3DParams.showInside()
                ? PolygonAttributes.CULL_FRONT
                : PolygonAttributes.CULL_NONE);

        for (int i = geometries.numChildren() - 1; i >= 0; i--)
            if (geometries.getChild(i) instanceof BranchGroup)
                geometries.removeChild(i);
        if (content3DParams.isOutline())
            makeOutlineBox();
        else
            outlineShape.removeAllGeometries();
        gridShape.removeAllGeometries();
        if (content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_LINES ||
                 content3DParams.getGridType() == RegularField3DParams.GRID_TYPE_POINTS)
            makeGrid();

        if (content3DParams.getGridType() == RegularField3DParams.RENDER_VOLUME) {
            if (volRender != null)
                geometries.removeChild(volRender);
            if (!regularField.hasCoords()) {
                volRender = new TextureVolumeRenderer(regularField, dataMappingParams, shadingParams, needPow2Texture);
                volRender.updateMesh();
                volRender.updateTexture();
                geometries.addChild(volRender);
            }
        }

        facesShape.removeAllGeometries();
        if (content3DParams.showInside())
            facesApp.getPolygonAttributes().setCullFace(PolygonAttributes.CULL_BACK);
        else
            facesApp.getPolygonAttributes().setCullFace(PolygonAttributes.CULL_NONE);
        facesApp.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
        int i = 0, j = 0;
        try {
            if (content3DParams.getDataMap() != currentFaceMode) {
                currentFaceMode = content3DParams.getDataMap();
                makeFaces();
            }
            for (i = 0; i < 3; i++)
                for (j = 0; j < 2; j++)
                    if (content3DParams.isSurFaces(i, j) ||
                            content3DParams.getFacesDisplay() == RegularField3DParams.BOX ||
                            content3DParams.getFacesDisplay() == RegularField3DParams.INSIDE_BOX) {
                        IndexedTriangleStripArray itsa = faces[i][j].getTriangleArray();
                        facesShape.addGeometry(itsa);
                    }
        } catch (javax.media.j3d.RestrictedAccessException ex) {
            logger.error("access " + i + " " + j);
        } catch (javax.media.j3d.MultipleParentException ex) {
            logger.error("parent " + i + " " + j);
        }

        if (geometries.getParent() == null)
            transformedGeometries.addChild(geometries);
        if (transformedGeometries.getParent() == null)
            geometry.addChild(transformedGeometries);

        if (detach)
            geometry.postattach();
    }

    @Override
    public Field getField()
    {
        return regularField;
    }
}
