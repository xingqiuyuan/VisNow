//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.geometries.objects;

import javax.media.j3d.IndexedLineStripArray;
import javax.media.j3d.IndexedPointArray;
import javax.media.j3d.IndexedTriangleStripArray;
import javax.media.j3d.Transform3D;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.geometries.events.LightDirectionListener;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.PresentationParams;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEventListener;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public abstract class RegularFieldGeometry extends FieldGeometry
{

    protected RegularField regularField;
    protected int colorMode = DataMappingParams.UNCOLORED;
    protected int currentColorMode = -1;
    protected long nTriangleStrips = 0;
    protected int nLineStrips = 0;
    protected long nNodePoints = 0;
    protected int[] dims = null;
    protected IntLargeArray lineStripCounts = null;
    protected IntLargeArray triangleStripCounts = null;
    protected IndexedTriangleStripArray triangleArr = null;
    protected IndexedLineStripArray edgeArr = null;
    protected IndexedPointArray nodeArr = null;
    protected IndexedLineStripArray boxArr = null;
    protected PresentationParams fieldDisplayParams;
    protected RenderEventListener renderEventListener;
    protected static final boolean[][] resetGeometry
            = {{false, true, true, true, true, true, true, true, true},
               {true, false, false, false, false, false, false, false, true},
               {true, false, false, false, false, false, false, false, true},
               {true, false, false, false, false, false, false, false, true},
               {true, false, false, false, false, false, false, false, true},
               {true, false, false, false, false, false, false, false, true},
               {true, false, false, false, false, false, false, false, true},
               {true, false, false, false, false, false, false, false, true},
               {true, true, true, true, true, true, true, true, false}
            };

    public RegularFieldGeometry(String name)
    {
        super(name);
        transformedGeometries.addChild(geometries);
        geometry.addChild(transformedGeometries);
    }

    public RegularFieldGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams);
        transformedGeometries.addChild(geometries);
        geometry.addChild(transformedGeometries);
    }


    /**
     * Set the value of field
     *
     * @param inField new value of field
     * <p>
     * @return if field is correct (1D)
     */
    
    public boolean setField(RegularField inField)
    {
        if (inField == null)
            return false;
//        fieldDisplayParams.updateFieldSchema(inField.getSchema(), true);
        structureChanged = (regularField == null || !inField.isStructureCompatibleWith(regularField));
        dataChanged = (regularField == null || structureChanged || !inField.isDataCompatibleWith(regularField));
        rangeChanged = !dataChanged && !inField.isFullyCompatibleWith(regularField);
        dims = inField.getDims();
        nNodes = (int) inField.getNNodes();
        regularField = inField;
        field = regularField;
        name = inField.getName();
        if (structureChanged || dataChanged) 
        {
            clearAllGeometry();
            fieldDisplayParams.getTransformParams().addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent evt)
                {
                    transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
                }
            });
            transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
        }
        return true;
    }

    /**
     * Set the value of fieldDisplayParams
     *
     * @param fieldDisplayParams new value of fieldDisplayParams
     */
    public void setFieldDisplayParams(PresentationParams fieldDisplayParams)
    {
        if (fieldDisplayParams == null)
            return;
        this.fieldDisplayParams = fieldDisplayParams;
        if (dataMappingParams != null)
            dataMappingParams.removeRenderEventListener(renderEventListener);
        if (renderingParams != null)
            renderingParams.removeRenderEventListener(renderEventListener);
        dataMappingParams = fieldDisplayParams.getDataMappingParams();
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        renderingParams = fieldDisplayParams.getRenderingParams();
        transformParams = fieldDisplayParams.getTransformParams();
        appearance = renderingParams.getAppearance();
        appearance.setUserData(this);
        lineAppearance = renderingParams.getLineAppearance();
        lineAppearance.setUserData(this);
        if (renderEventListener != null) {
            dataMappingParams.addRenderEventListener(renderEventListener);
            renderingParams.addRenderEventListener(renderEventListener);
        }
        transformParams = fieldDisplayParams.getTransformParams();
        fieldDisplayParams.getTransformParams().addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
            }
        });
        transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
    }
    
    public void setData(RegularField inField, PresentationParams fieldDisplayParams)
    {
        if (inField == null || fieldDisplayParams == null)
            return;
        clearParamListeners();
        clearAllGeometry();
    }

    /**
     * Clears render event listeners from dataMappingParams and renderingParams;
     * this has to be done to remove all references to this RegularFieldGeometry
     * object. Listeners (with reference to this are passed in
     * setFieldDisplayParams and setField methods).
     */
    public void clearParamListeners()
    {
        if (dataMappingParams != null)
            dataMappingParams.removeRenderEventListener(renderEventListener);
        if (renderingParams != null)
            renderingParams.removeRenderEventListener(renderEventListener);
    }

    @Override
    public OpenBranchGroup getGeometry()
    {
        updateGeometry();
        return geometry;
    }

    public OpenBranchGroup getGeometryObject()
    {
        return geometry;
    }

    @Override
    public boolean setField(Field inField)
    {
        if (inField == null || !(inField instanceof RegularField))
            return false;
        return setField((RegularField) inField);
    }

    @Override
    public Field getField()
    {
        return regularField;
    }

    public int getDimension()
    {
        if (regularField == null)
            return -1;
        return regularField.getDimNum();
    }
    
    public static final RegularFieldGeometry create(RegularField field, 
                                                    PresentationParams presentationParams)
    {
        if (field == null)
            return null;
        switch (field.getDimNum())
        {
        case 1:
            return new  RegularField1DGeometry(field.getName(), presentationParams);
        case 2:
            return new  RegularField2DGeometry(field.getName(), presentationParams);
        case 3:
            return new  RegularField3DGeometry(field.getName(), presentationParams);
        default:
            return null;
        }
    }
}
