//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.geometries.objects;

import java.util.ArrayList;
import javax.media.j3d.J3DGraphics2D;
import javax.media.j3d.MultipleParentException;
import javax.media.j3d.Transform3D;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.parameters.PresentationParams;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;

/**
 *
 * @author Krzysztof S. Nowinski
 * <p>
 * University of Warsaw, ICM
 */
public class IrregularFieldGeometry extends FieldGeometry
{
    protected PresentationParams presentationParams = null;
    protected ArrayList<CellSetGeometry> cellSetGeometries = new ArrayList<CellSetGeometry>();
    protected IrregularField field = null;
    static Logger logger = Logger.getLogger(IrregularFieldGeometry.class);
    protected ColormapLegend fldColormapLegend = colormapLegend;

    public IrregularFieldGeometry(String name, PresentationParams presentationParams)
    {
        super(name, presentationParams);
        this.presentationParams = presentationParams;
        transformedGeometries.addChild(geometries);
        geometry.addChild(transformedGeometries);
    }

    public IrregularFieldGeometry(PresentationParams presentationParams)
    {
        super(presentationParams);
        this.presentationParams = presentationParams;
        transformedGeometries.addChild(geometries);
        geometry.addChild(transformedGeometries);
    }

    private void updateCellSetGeometries()
    {
        boolean detach = geometry.postdetach();
        cellSetGeometries.clear();
        clearAllGeometry();
        geometries.removeAllChildren();
        for (int iSet = 0; iSet < field.getNCellSets(); iSet++) {
            CellSet cSet = field.getCellSet(iSet);
            CellSetGeometry cSetGeometry = new CellSetGeometry(cSet.getName());
            cSetGeometry.setParams(presentationParams.getChild(iSet));
            presentationParams.getChild(iSet).setActive(true);
            cSetGeometry.setInData(field, cSet);
            cellSetGeometries.add(cSetGeometry);
            geometries.addChild(cSetGeometry);
            addBgrColorListener(cSetGeometry.getBgrColorListener());
        }
//        for (int iSet = 0; iSet < field.getNCellSets(); iSet++) 
//           cellSetGeometries.get(iSet).setInData(field, field.getCellSet(iSet));
        if (detach)
            geometry.postattach();
    }

    public void setData(IrregularField inField)
    {
        setData(inField, "");
    }

    public void setData(IrregularField inField, String presentationParamString)
    {
        if (inField == null || (int) inField.getNNodes() < 1)
            return;
        setIgnoreUpdate(true);
        
        presentationParams.setInField(inField);
        if (!presentationParamString.isEmpty())
            presentationParams.restorePassivelyValuesFrom(presentationParamString);
        structureChanged = (field == null || !inField.isStructureCompatibleWith(this.field));
        dataChanged = (field == null || !inField.isDataCompatibleWith(this.field));
        this.field = inField;
        name = field.getName();
        if (cellSetGeometries != null) {
            for (CellSetGeometry cellSetGeometry : cellSetGeometries)
                cellSetGeometry.getDataMappingParams().clearRenderEventListeners();
            updateCellSetGeometries();
            transformParams = presentationParams.getTransformParams();
            colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
            presentationParams.getTransformParams().addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent evt)
                {
                    transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
                }
            });
            transformedGeometries.setTransform(new Transform3D(transformParams.getTransform()));
            if (field.getNCellSets() == 1)
                colormapLegend = cellSetGeometries.get(0).getColormapLegend();
            else
                colormapLegend = fldColormapLegend;
        }
        setIgnoreUpdate(false);
    }

    @Override
    public void setIgnoreUpdate(boolean ignoreUpdate)
    {
        this.ignoreUpdate = ignoreUpdate;
        for (CellSetGeometry cellSetGeometrie : cellSetGeometries)
            cellSetGeometrie.setIgnoreUpdate(ignoreUpdate);
    }

    public ArrayList<CellSetGeometry> getCellSetGeometries()
    {
        return cellSetGeometries;
    }

    public CellSetGeometry getCellSetGeometry(int i)
    {
        if (cellSetGeometries != null && i >= 0 && i < cellSetGeometries.size())
            return cellSetGeometries.get(i);
        return null;
    }

    @Override
    public void updateCoords(FloatLargeArray coords)
    {
        boolean detach = geometry.postdetach();
        this.coords = coords;
        if (field != null)
            for (int i = 0; i < field.getNCellSets(); i++)
                cellSetGeometries.get(i).updateCoords(coords);
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords(boolean force)
    {
        if (field == null || !force)
            return;
        boolean detach = geometry.postdetach();
        coords = field.getCurrentCoords() == null ? null : field.getCurrentCoords();
        for (CellSetGeometry cellSetGeometry : cellSetGeometries)
            cellSetGeometry.updateCoords(force);
        if (detach)
            geometry.postattach();
    }

    @Override
    public void updateCoords()
    {
        if (field == null || ignoreUpdate)
            return;
        boolean detach = geometry.postdetach();
        coords = field.getCurrentCoords() == null ? null : field.getCurrentCoords();
        for (CellSetGeometry cellSetGeometry : cellSetGeometries)
            cellSetGeometry.updateCoords();
        if (detach)
            geometry.postattach();
    }

    public void updateTextureCoords()
    {
        if (field != null) {
            boolean detach = geometry.postdetach();
            for (CellSetGeometry cellSetGeometry : cellSetGeometries)
                cellSetGeometry.updateTextureCoords();
            if (detach)
                geometry.postattach();
        }
    }

    @Override
    public void updateDataMap()
    {
        if (field != null) {
            boolean detach = geometry.postdetach();
            for (CellSetGeometry cellSetGeometry : cellSetGeometries)
                cellSetGeometry.updataDataMap();
            if (detach)
                geometry.postattach();
        }
    }

    public void updateColors()
    {
        if (field != null) {
            boolean detach = geometry.postdetach();
            for (CellSetGeometry cellSetGeometry : cellSetGeometries)
                cellSetGeometry.updateColors();
            if (detach)
                geometry.postattach();
        }
    }

    @Override
    public void createGeometry(Field inField)
    {
        updateGeometry();
    }

    @Override
    public OpenBranchGroup getGeometry()
    {
        updateGeometry();
        return geometry;
    }

    @Override
    public void updateGeometry()
    {
        if (ignoreUpdate || field == null)
            return;

        boolean detach = geometry.postdetach();
        try {
            transformedGeometries.removeAllChildren();
        } catch (Exception e) {
        }
        if (cellSetGeometries != null)
            for (CellSetGeometry cellSetGeometry : cellSetGeometries)
                cellSetGeometry.updateGeometry();
        if (geometries.getParent() != null)
            geometries.detach();
        try {
            transformedGeometries.addChild(geometries);
        } catch (MultipleParentException e) {
            logger.error("multiple parent in " + this + " updateGeometry");
        }
//        logger.debug("updating - re-attached");
        if (detach)
            geometry.postattach();
    }

    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
    {
//        System.out.println("IF drawLocal2D");
        for (CellSetGeometry cellSetGeom : cellSetGeometries)
            cellSetGeom.draw2D(vGraphics, ltw, w, h);
    }

    @Override
    public OpenBranchGroup getGeometry(Field inField)
    {
        return null;
    }

    @Override
    public void updateGeometry(Field inField)
    {
    }

    public PresentationParams getFieldDisplayParams()
    {
        newParams = false;
        return presentationParams;
    }

    public ColormapLegend getColormapLegend(int i)
    {
        if (cellSetGeometries == null || i < 0 ||
                i >= cellSetGeometries.size() ||
                cellSetGeometries.get(i) == null)
            return null;
        return cellSetGeometries.get(i).getColormapLegend();
    }

    @Override
    public boolean setField(Field inField)
    {
        if (inField == null || !(inField instanceof IrregularField))
            return false;
        setData((IrregularField) inField);
        return true;
    }

    public void updateCellSetData(int n)
    {
        if (n >= 0 && n < field.getNCellSets()) {
            cellSetGeometries.get(n).setName(field.getCellSet(n).getName());
            cellSetGeometries.get(n).setPicked(field.getCellSet(n).isSelected());
        }
    }

    @Override
    public Field getField()
    {
        return field;
    }

}
