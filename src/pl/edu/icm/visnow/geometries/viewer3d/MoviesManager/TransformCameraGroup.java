package pl.edu.icm.visnow.geometries.viewer3d.MoviesManager;

import java.util.ArrayList;
import javax.media.j3d.Transform3D;
import pl.edu.icm.visnow.geometries.objects.generics.OpenTransformGroup;

/**
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * This class extends the OpenTranfromGroup class to allow addition of the listeners for monitoring transformation change.
 * 
 * 
 */


public class TransformCameraGroup extends OpenTransformGroup{
    
    
    ArrayList<Listener> listeners = new ArrayList<>();
    
    /**
     * creates a new transform camera group
     */
    public TransformCameraGroup(){
        
    }
    
    /**
     * creates a new transform camera group with a specific name
     * @param name specific name for the camera transform group.
     */
    
    public TransformCameraGroup(String name){
        super.setName(name);
    }
    
    /**
     * interface for camera group transform changed listeners
     */
    public interface Listener{
        public void transformCameraGroupChanged(Transform3D newTransform);
    }
    
    /**
     * sets new transform of the transform camera group
     * @param newTransform - new transform
     */
    
    @Override
    public void setTransform(Transform3D newTransform){
        super.setTransform(newTransform);
        fireTransformChanged(newTransform);
    }
    
    
    private void fireTransformChanged(Transform3D newTransform) {
        for (Listener l : listeners)
            l.transformCameraGroupChanged(newTransform);
    }
    
    /**
     * adds a transform change listener to the list
     * @param l listener
     * @return true (as specified by Collection.add)
     */
    
    public boolean addTransformCameraGroupListener(Listener l){
        return listeners.add(l);
    }
    
    /**
     * removes a transform changed listener from the list
     * @param l listener
     * @return true if this list contained the specified element, false otherwise.
     */
    
    public boolean removeTransformCameraGroupListener(Listener l){
        return listeners.remove(l);
    }
    
}
