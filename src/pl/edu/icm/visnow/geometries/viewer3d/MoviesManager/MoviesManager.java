package pl.edu.icm.visnow.geometries.viewer3d.MoviesManager;

import java.util.ArrayList;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineStripArray;
import javax.vecmath.Point3d;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.geometries.objects.GeometryObject;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;
import pl.edu.icm.visnow.lib.utils.curves.Cubic;

/**
 * This class holds and manage information about key frames of a movie.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * /**
 * This class holds and manage information about key frames of a movie.
 * 
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 */


public class MoviesManager {

    protected static Logger LOGGER = Logger.getLogger(MoviesManager.class);
    
    private ArrayList<MovieFrame> keyFrames;
    private GeometryObject trajOutObj;
    
    /**
     * This method creates geometry object representing camera trajectory. Trajectory is interpolated based on key frames.
     * @param isClosed if true than the starting point is also the ending point and the trajectory is closed.
     * @param nKnots number of interpolation points on every segment between key frames. Must be greater than 0
     * @return camera trajectory or null if number of key frames is lower than 4
     */
    
    public GeometryObject createTrajectorieGeometry(boolean isClosed, int nKnots)
    {
        if(!(nKnots>0)){
            throw new IllegalArgumentException("Interpolation knots number = "+nKnots+", Interpolation knots number must be greater than 0");
        }

        int nKeyFrames = keyFrames.size();
        if(nKeyFrames<4){
            return null;
        }
        
        Point3d[] vertsl;
        if(isClosed){
            vertsl = new Point3d[nKeyFrames + 1];
            vertsl[nKeyFrames] = new Point3d(keyFrames.get(0).getCameraPosition());
        }else{
            vertsl = new Point3d[nKeyFrames];
        }
        
        for (int i = 0; i < nKeyFrames; i++) {
            vertsl[i] = new Point3d(keyFrames.get(i).getCameraPosition());
        }

        ArrayList<Point3d> interpolatedPoints = interpolateCameraTrajectory(vertsl, nKnots);
        int nFrames = interpolatedPoints.size();
        
        Point3d[] nVertsl = new Point3d[nFrames];
        
        for (int i = 0; i < nFrames; i++) {
            nVertsl[i] = new Point3d(interpolatedPoints.get(i));
        }
        
        int[] pIndexl = new int[2*(nFrames - 1)];
        
        for (int i = 0; i < nFrames - 1; i++) {
            pIndexl[2*i] = i;
            pIndexl[2*i + 1] = i+1;
        }
        
        trajOutObj = new GeometryObject("trajectory");
        
        int[] stripsl = new int[nFrames - 1];
        for (int i = 0; i < stripsl.length; i++) {
            stripsl[i] = 2;
        }

        IndexedLineStripArray glyph = new IndexedLineStripArray(nFrames,
                                          GeometryArray.COORDINATES,
                                          2 * nFrames, stripsl);
        
        glyph.setCoordinates(0, nVertsl);
        glyph.setCoordinateIndices(0, pIndexl);
        OpenShape3D pickLine = new OpenShape3D();
        pickLine.addGeometry(glyph);
        OpenBranchGroup pLine = new OpenBranchGroup();
        pLine.addChild(pickLine);
        trajOutObj.addNode(pLine);
        
        return trajOutObj;
    }
    
    private static ArrayList<Point3d> interpolateCameraTrajectory(Point3d[] points, int nKnots)
    {
        ArrayList<Point3d> newPoints = new ArrayList<>();
        
        int nSplines = points.length - 1;
        
        float[][][] splines = Cubic.calcCubic3D(nSplines, points);
        
        float[][] splinesX = splines[0];
        float[][] splinesY = splines[1];
        float[][] splinesZ = splines[2];
        float t;
        
        for (int i = 0; i < nSplines; i++) {
            for (int j = 0; j < nKnots; j++) {
                t=j/(float)nKnots;
                newPoints.add(new Point3d(Cubic.eval(splinesX[i], t), Cubic.eval(splinesY[i], t), Cubic.eval(splinesZ[i], t)));
            }
        }
        newPoints.add(new Point3d(Cubic.eval(splinesX[nSplines - 1], 1), Cubic.eval(splinesY[nSplines - 1], 1), Cubic.eval(splinesZ[nSplines - 1], 1)));
        
      return newPoints;
    }
    
    /**
     * init key frame list
     */
    public MoviesManager(){
        keyFrames = new ArrayList<>();
    }   
    
    /**
     * adds new frame to the key frames list
     * @param frame new frame
     */
    public void addKeyFrame(MovieFrame frame)
    {
        keyFrames.add(frame);
    }
    /**
     * removes frame from the key frames list
     * @param idx index of the frame to be removed
     */
    public void removeKeyFrame(int idx)
    {
        if(idx>=0 || idx<keyFrames.size()){
            keyFrames.remove(idx);
        }else{
            LOGGER.error(new IndexOutOfBoundsException("idx"+idx+"; size of keyFrames list = "+keyFrames.size()));
        }
    }

    /**
     * Key frames list.
     * @return key frames list 
     */
    
    public ArrayList<MovieFrame> getKeyFrames() {
        return keyFrames;
    }
    
    /**
     * This method returns camera trajectory geometry object
     * @return geometry object representing camera trajectory 
     */
    public GeometryObject getTrajectoryGeometryObject() {
        return trajOutObj;
    }
    
}
