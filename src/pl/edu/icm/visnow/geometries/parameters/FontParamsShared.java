package pl.edu.icm.visnow.geometries.parameters;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author norkap
 */


public class FontParamsShared {
    
    public static final ParameterName<Boolean> FONT_PARAMS_THREE_DIMENSIONAL = new ParameterName("three dimensional");
    
    // % from 0.1 to 100
    public static final ParameterName<Float> FONT_PARAMS_GLYPHS_SIZE = new ParameterName("glyphs size");
    
    public static final ParameterName<Float> FONT_PARAMS_PRECISION = new ParameterName("precision");
    
    public static final ParameterName<String> FONT_PARAMS_FONT_NAME = new ParameterName("font name");
    
    public static final ParameterName<Integer> FONT_PARAMS_FONT_TYPE = new ParameterName("font type");
    
    public static final ParameterName<Integer> FONT_PARAMS_COLOR = new ParameterName("color");
    
    public static final ParameterName<Integer> FONT_PARAMS_BRIGHTNESS = new ParameterName("brightness");
    
    public static List<Parameter> createDefaultParametersAsList()
    {
        
        List<Parameter> l = new ArrayList<>();
        l.add(new Parameter<>(FONT_PARAMS_THREE_DIMENSIONAL, false));
        l.add(new Parameter<>(FONT_PARAMS_GLYPHS_SIZE, .02f));
        l.add(new Parameter<>(FONT_PARAMS_PRECISION, 3.0f));
        l.add(new Parameter<>(FONT_PARAMS_FONT_NAME, "sans-serif"));
        l.add(new Parameter<>(FONT_PARAMS_FONT_TYPE, Font.PLAIN));
        l.add(new Parameter<>(FONT_PARAMS_COLOR, Integer.MAX_VALUE));
        l.add(new Parameter<>(FONT_PARAMS_BRIGHTNESS, 50));
        
        return l;
    }
}
