/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.engine;

import pl.edu.icm.visnow.engine.element.ElementKiller;

public class Engine_multiqueue_try implements Runnable, ElementKiller
{

    public void run()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isKilled()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    //
    //    private static boolean debug = false;
    //
    //    private HashMap<String, ModuleBox> modules;
    //    private HashMap<LinkName, Link> links;
    //
    //
    //    private boolean killed = false;
    //    public void kill() {
    //        killed = true;
    //        VNLogger.log("KILLED!", this, "engine");
    //        //getApplication().stopProgress(); //TODO - progressBar
    //        getApplication().releaseAccess();
    //    }
    //
    //    public boolean isKilled() {
    //        return killed;
    //    }
    //
    //    public boolean getPermission(ModuleElement element) {
    //        return true;
    //    }
    //
    //    private int moduleNumber;
    //
    //    public int nextModuleNumber() {
    //        ++moduleNumber;
    //        return moduleNumber-1;
    //    }
    //
    //    public HashMap<String, ModuleBox> getModules() {return modules;}
    //    public HashMap<LinkName, Link> getLinks() {return links;}
    //
    //    public ModuleBox getModule(String name) {return modules.get(name);}
    //    public Link getLink(LinkName name) {return links.get(name);}
    //
    //    private EngineCommandExecutor executor;
    //    private Application application;
    //
    //    public EngineCommandExecutor getExecutor() {return executor;}
    //    public Application getApplication() {return application;}
    //
    //    public Engine(Application application) {
    ////        super("Engine");
    ////        this.killer = this;
    //        this.modules = new HashMap<String, ModuleBox>();
    //        this.links = new HashMap<LinkName, Link>();
    //        this.executor = new EngineCommandExecutor(this);
    //        this.application = application;
    //        this.moduleNumber = 1;
    //    }
    //
    //  //  @Override
    //    private LinkedBlockingQueue<Message> startQueue = new LinkedBlockingQueue<Message>();;
    //    public LinkedBlockingQueue<Message> getStartQueue() {
    //        VNLogger.log("MESSAGE PUT", this, "engine");
    //        return startQueue;
    //    }
    //
    //    public boolean isNewActionPossible() {return true;}
    //    public boolean isModuleActionPossible() {return true;}
    //
    //
    //
    //    private Message lastMessage;
    //    protected Message getLastMessage() {
    //        return lastMessage;
    //    }
    //
    //    protected void gotException(VNSystemEngineException ex) {
    //        Displayer.display(200907100945L, ex, this, "Exception in engine queue.");
    //    }
    //
    //
    //    public void run() {
    //      //if(debug) System.out.println("I AM RUNNING! {"+this+"}");
    //      try {
    //         //System.out.println("waiting... "+this);
    //         while (nextMessage()) {
    //         }
    //      } catch (InterruptedException ex) {
    //         Displayer.ddisplay(200907100810L, ex, this, "Interrupted.");
    //      } catch (VNSystemEngineException ex) {
    //         Displayer.display(200907100811L, ex, this, "Elementary exception (Ooops!).");
    //      }
    //      //if(debug) System.out.println("I AM NOT RUNNING :( {"+this+"}");
    //   }
    //
    //    public boolean nextMessage() throws InterruptedException, VNSystemEngineException {
    //        //if(debug) System.out.println("................ on "+getQueue().toString());
    //
    //
    //        lastMessage = startQueue.take();
    //
    //        Runnable r = new Runnable() {
    //            public void run() {
    //                switch(getLastMessage().getType()) {
    //                    case Message.KILL:
    //                        try {
    //                            onKillMessage();
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //                    case Message.NOTIFY:
    //                        try {
    //                            onNotifyMessage(getLastMessage());
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //                    case Message.READY:
    //                        try {
    //                            onReadyMessage(getLastMessage());
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //                    case Message.ACTION:
    //                        try {
    //                            onActionMessage(getLastMessage());
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //                    case Message.INACTION:
    //                        try {
    //                            onInactionMessage(getLastMessage());
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //                    case Message.DONE:
    //                        try {
    //                            onDoneMessage(getLastMessage());
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //                    default:
    //                        try {
    //                            onOtherMessage(getLastMessage());
    //                        } catch (VNSystemEngineException ex) {
    //                            gotException(ex);
    //                        }
    //                        break;
    //        }
    //            }
    //        };
    //        new Thread(r).start();
    //        return true;
    //    }
    //
    //    @Override
    //    protected void onOtherMessage(Message message) throws VNSystemEngineException {
    //        switch(message.getType()) {
    //            case Message.START_ACTION:
    //                onStartActionMessage(message);
    //                return;
    //        }
    //
    //    }
    //
    //    protected void onStartActionMessage(Message message) {
    //
    //
    //        try {
    //            getApplication().getAccess();
    //            VNLogger.log("START! from module ["+message.getSender()+"]\n\n", this, "engine");
    //            killed =false;
    ////            getApplication().getScene().getScenePanel().getProgress().init(); //TODO - progressBar
    //            if(message.getSender() instanceof ModuleElement) {
    //                try {
    //                    ((ModuleElement)message.getSender()).getModuleBox().getCore().onWaveStarting();
    //                } catch(Exception e) {
    //                     Displayer.ddisplay(42, e, this,
    //                        "An error occured in function \"onWaveStarting\" "+
    //                        "of module \""+message.getSender().getName()+"\".\n"+
    //                        "Please report this exception to the module core developer.\n"+
    //                        "The application flow will be terminated."
    //                        );
    //                     killed = true;
    //                     getApplication().releaseAccess();
    //                     return;
    //                }
    //            }
    //            message.getSender().getQueue().put(new Message(this, Message.NOTIFY));
    //            if (debug) {
    //                System.out.println("sent message to " + message.getSender());
    //            }
    //        } catch (InterruptedException ex) {
    //            ex.printStackTrace();
    //        }
    //    }
    //
    //    @Override
    //    protected void onReadyMessage(Message message) throws VNSystemEngineException {
    //        try {
    //            //      getApplication().startProgress(); //TODO - progressBar
    //            message.getSender().getQueue().put(new Message(this, Message.ACTION));
    //        } catch (InterruptedException ex) {
    //            throw new VNSystemEngineException(
    //                        200910260000L,
    //                        "Ready message propagation interrupted",
    //                        ex,
    //                        this,
    //                        Thread.currentThread()
    //                        );
    //        }
    //    }
    //
    //    @Override
    //    protected void onDoneMessage(Message message) throws VNSystemEngineException {
    //        //getApplication().stopProgress(); //TODO - progressBar
    //        VNLogger.log("FINISH!\n\n", this, "engine");
    //        if(message.getSender() instanceof ModuleElement) {
    //            try {
    //                ((ModuleElement)message.getSender()).getModuleBox().getCore().onWaveFinalizing();
    //            } catch(Exception e) {
    //                 Displayer.ddisplay(42, e, this,
    //                    "An error occured in function \"onWaveFinalizing\" "+
    //                    "of module \""+message.getSender().getName()+"\".\n"+
    //                    "Please report this exception to the module core developer.\n"+
    //                    "The application flow will be terminated."
    //                    );
    //                 killed = true;
    //                 getApplication().releaseAccess();
    //                 return;
    //            }
    //        }
    //        getApplication().releaseAccess();
    //        // TODO: finish action
    //    }
    //
    //    private void onFinishActionMessage(Message message) {
    //
    //    }
    //
    //    public void writeFlow(String info) {
    //        //System.out.println(info);
    //    }
    //
    //    public void unkill() {
    //        killed = false;
    //    }
    //
    //
    //
    //
    //
    //
    //    public Collection<ModuleBox> getTopologicalModules() throws VNApplicationNetLoopException {
    //        //if(true) return this.getModules().values();
    //
    //        Vector<ModuleBox> ret = new Vector<ModuleBox>();
    //
    //
    //        HashMap<ModuleBox, ModuleNode> nodes = new HashMap<ModuleBox, ModuleNode>();
    //        for(ModuleBox m: this.getModules().values()) nodes.put(m, new ModuleNode(m));
    //        for(Map.Entry<ModuleBox, ModuleNode> en: nodes.entrySet()) {
    //            Iterator<Link> i = en.getKey().iterator(false, true);
    //            while(i.hasNext()) {
    //                //Link li = i.next();
    //                en.getValue().edges.add(nodes.get(i.next().getInput().getModuleBox()));
    //            }
    //        }
    //
    //        Vector<ModuleNode> ready = new Vector<ModuleNode>();
    //        for(ModuleNode mn: nodes.values()) {
    //            if(mn.inValue==0) ready.add(mn);
    //        }
    //
    ////        String p = "";
    ////        for(ModuleNode mn: ready) p += mn.node.getName()+";";
    ////        System.out.println("READY ["+p+"]");
    ////        p="";
    ////        for(ModuleNode mn: nodes.values()) p+= " ("+mn.node.getName()+"::"+mn.inValue+")";
    ////        System.out.println("ALL |->"+p+" <-|");
    //
    //        while(!ready.isEmpty()) {
    //
    //
    //            ModuleNode next = ready.remove(ready.size()-1);
    //            ret.add(next.node);
    //            for(ModuleNode mn: next.edges) {
    //                mn.inValue--;
    //                if(mn.inValue==0) ready.add(mn);
    //            }
    //        }
    //
    //        if(ret.size() < nodes.size()) throw new VNApplicationNetLoopException();
    //
    //        return ret;
    //    }
    //
    //    public void clearState() {
    //        throw new UnsupportedOperationException("Not yet implemented");
    //    }
    //
    //}
    //
    //
    //
    //
    //
    //
    //class ModuleNode {
    //    protected ModuleBox node;
    //    protected int inValue;
    //    public ModuleNode(ModuleBox box) {
    //        this.node = box;
    //        int i = 0;
    //        for(Input inp: box.getInputs().getInputs().values()) {
    //            i+= inp.getLinks().size();
    //        }
    //        this.inValue = i;
    //        this.edges = new Vector<ModuleNode>();
    //    }
    //    protected Vector<ModuleNode> edges;
}
//
