//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.gui.widgets.utils.hyperrectangle;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.gui.widgets.utils.drag2D.DirectionalDrag;
import pl.edu.icm.visnow.gui.widgets.utils.drag2D.DragBundle;
import static pl.edu.icm.visnow.gui.widgets.utils.hyperrectangle.Hyperrectangle.style1DHalfGapPx;
import static pl.edu.icm.visnow.gui.widgets.utils.hyperrectangle.Hyperrectangle.style1DHeightHalfPx;
import static pl.edu.icm.visnow.gui.widgets.utils.hyperrectangle.Hyperrectangle.zScaleRatio;
import static pl.edu.icm.visnow.gui.widgets.utils.hyperrectangle.Hyperrectangle.zXRatio;
import static pl.edu.icm.visnow.gui.widgets.utils.hyperrectangle.Hyperrectangle.zYRatio;

/**
 *
 * @author szpak
 */
public class HyperrectangleUtils
{
    private static final Logger LOGGER = Logger.getLogger(HyperrectangleUtils.class);

    //default style
    //    private Color baseColor = new Color(0x555555);
    //    private Color baseFillColor = new Color(0xdddddd);
    //    private Color selectionColor = new Color(0x333333);
    //    private Color selectionFillColor = new Color(0xf7f7f7);
    //    private Color intersectionColor = new Color(0x333333);
    //    private Color intersectionFillColor = new Color(0xffffff);
    //
    //    private Stroke baseStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1, new float[]{4, 4}, 0);
    //    //1D finishing bars
    //    private Stroke baseStrokeFull = new BasicStroke(1);
    //    private Stroke selectionStroke = new BasicStroke(1);
    //    private Stroke intersectionStroke = new BasicStroke(1);
    private static Color baseColor = new Color(0x555555);
//    private Color baseFillColor = new Color(0xdddddd);
    private static Color baseFillColor = new Color(0xdd, 0xdd, 0xdd, 192);
    private static Color selectionColor = new Color(0x333333);
//    private Color selectionColor = new Color(0x777777);
    private static Color selectionFillColor = new Color(0xf7f7f7);
    private static Color selectionDummyColor = new Color(0x555555);
//    private Color selectionDummyColor = new Color(0x999999);
    private static Color intersectionColor = new Color(0x333333);
    private static Color intersectionFillColor = new Color(0xff, 0xff, 0xff, 192);
    private static Color projectionColor = new Color(0x777777);
    private static Color projectionFillColor = new Color(0xcc, 0xcc, 0xcc, 192);

    private static BasicStroke baseStroke = new BasicStroke(1);
    //1D finishing bars
    private static BasicStroke baseStrokeFull = new BasicStroke(1);
    private static BasicStroke selectionStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1, new float[]{4, 4}, 0);
    private static BasicStroke selectionStrokeFull = new BasicStroke(1);
    private static BasicStroke intersectionStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1, new float[]{4, 4}, 0);
    private static BasicStroke projectionStroke = new BasicStroke(1);
//    private Stroke intersectionStroke = new BasicStroke(1);
    private static BasicStroke selectionDummyStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1, new float[]{1f, 6f}, 0);
//    private Stroke selectionDummyStroke = new BasicStroke(1);

//    private Stroke baseStroke = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1, new float[]{4, 4}, 0);
//    //1D finishing bars
//    private Stroke baseStrokeFull = new BasicStroke(1);
//    private Stroke selectionStroke = new BasicStroke(1);
//    private Stroke selectionStrokeFull = new BasicStroke(1);
//    private Stroke intersectionStroke = new BasicStroke(1);
////    private Stroke intersectionStroke = new BasicStroke(1);
//    private Stroke selectionDummyStroke = new BasicStroke(1);
////    private Stroke selectionDummyStroke = new BasicStroke(1);
    //default colors/strokes
    // get drag set ?? (directionaldrag / directionaldoubledrag + dragging edge(s)?)
    private static final float cornerAttractionPc = 0.2f;
    private static final float edges3DAttractionPc = 0.3f;
    private static final int maximumAttractionPx = 30;

    /**
     * Draws rectangle using default style, right-hand orientation, and bottom-left as a begin of coordinate system.
     * <p>
     * This method scales base and selection as much as possible in specified width x height space.
     */
    public static void draw(Graphics2D graphics2D, int width, int height, Hyperrectangle base, Hyperrectangle selection)
    {
        if (base.numberOfDimensions != selection.numberOfDimensions)
            throw new IllegalArgumentException("number of dimensions in base and selection don't match");

        /**
         * 2px here are considered as length == 1 (see fitToScreen for description)
         */
        if (width < 2 || height < 2) {
            LOGGER.warn("width and height (" + width + " x " + height + ") need to be >= 2.");
            return;
        }

        Hyperrectangle scaledBase = base.clone();
        scaledBase.scale(1.2, true);
        Hyperrectangle intersection = base.clone();
        intersection.intersectWith(selection);
        Hyperrectangle union = Hyperrectangle.union(base, selection);
        //scale in 3D
        if (base.numberOfDimensions == 3) union = Hyperrectangle.union(scaledBase, selection);//union.scale(1.2, true);

        double[] worldBounds = get2DWorldBounds(width, height, union.getVertices2DProjection());
        double worldStartX = worldBounds[0];
        double worldStartY = worldBounds[1];
        double worldScale = worldBounds[2];

        if (base.numberOfDimensions == 1) {
            double displacement = (style1DHalfGapPx + style1DHeightHalfPx + 1) / worldScale;
            selection.draw(graphics2D, width, height, worldStartX, worldStartY + displacement, worldScale, selectionStroke, selectionColor, selectionFillColor);
            base.draw(graphics2D, width, height, worldStartX, worldStartY - displacement, worldScale, baseStroke, baseColor, baseFillColor);
        } else if (base.numberOfDimensions == 2) {
            base.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, null, null, baseFillColor);
            selection.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, null, null, selectionFillColor);
            intersection.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, null, null, intersectionFillColor);

            selection.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, selectionStroke, selectionColor, null);
            base.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, baseStroke, baseColor, null);
        } else if (base.numberOfDimensions == 3) {
            double[] scaledBaseSides = new double[]{scaledBase.offset[0] + scaledBase.size[0], scaledBase.offset[1] + scaledBase.size[1], scaledBase.offset[2] + scaledBase.size[2]};

            PointInt[][] baseProjection = convertToScreenPoints(width, height, worldStartX, worldStartY, worldScale, base.projectOnPlanes(scaledBaseSides));
            PointInt[][] intersectionProjection = convertToScreenPoints(width, height, worldStartX, worldStartY, worldScale, intersection.projectOnPlanes(scaledBaseSides));

//            base.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, null, null, baseFillColor);
//            base.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, baseStroke, baseColor, baseFillColor);
//
//            selection.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, selectionStroke, selectionColor, null);
            PointInt[][] frame = convertToScreenPoints(width, height, worldStartX, worldStartY, worldScale, selection.getSubFrame(scaledBaseSides, true));

            for (int i = 0; i < frame.length; i++) {
                PointInt[] pointInts = frame[i];

                graphics2D.setStroke(selectionStroke);
                graphics2D.setColor(selectionColor);
                graphics2D.drawLine(pointInts[0].x, pointInts[0].y, pointInts[1].x, pointInts[1].y);
            }

            intersection.draw(graphics2D, width, height, worldStartX, worldStartY, worldScale, intersectionStroke, intersectionColor, intersectionFillColor);

            for (int i = 0; i < 3; i++) {
//
                Polygon basePolygon = new Polygon(new int[]{baseProjection[i][0].x, baseProjection[i][1].x, baseProjection[i][2].x, baseProjection[i][3].x},
                                                  new int[]{baseProjection[i][0].y, baseProjection[i][1].y, baseProjection[i][2].y, baseProjection[i][3].y},
                                                  4);
                Polygon intersectionPolygon = new Polygon(new int[]{intersectionProjection[i][0].x, intersectionProjection[i][1].x, intersectionProjection[i][2].x, intersectionProjection[i][3].x},
                                                          new int[]{intersectionProjection[i][0].y, intersectionProjection[i][1].y, intersectionProjection[i][2].y, intersectionProjection[i][3].y},
                                                          4);
                graphics2D.setColor(baseFillColor);
                graphics2D.fill(basePolygon);
//                graphics2D.setColor(selectionFillColor);
//                graphics2D.fill(selectionProjection);
                if (!intersection.isEmpty()) {
                    graphics2D.setColor(projectionFillColor);
                    graphics2D.fill(intersectionPolygon);
                }
//
                if (!intersection.isEmpty()) {
                    graphics2D.setStroke(projectionStroke);
                    graphics2D.setColor(projectionColor);
                    graphics2D.draw(intersectionPolygon);
                }
                graphics2D.setStroke(baseStroke);
                graphics2D.setColor(baseColor);
                graphics2D.draw(basePolygon);
            }

            frame = convertToScreenPoints(width, height, worldStartX, worldStartY, worldScale, selection.getSubFrame(scaledBaseSides, false));

            for (int i = 0; i < frame.length; i++) {
                PointInt[] pointInts = frame[i];

                graphics2D.setStroke(selectionStroke);
                graphics2D.setColor(selectionColor);
                graphics2D.drawLine(pointInts[0].x, pointInts[0].y, pointInts[1].x, pointInts[1].y);
            }

        } else throw new UnsupportedOperationException("Higher dimensions are not supported for drawing right now");
    }

    /**
     * Calculates and returns {worldStartX, worldStartY, worldScale} for given point set and screen size.
     */
    private static double[] get2DWorldBounds(int width, int height, PointDouble[] doublePoints)
    {
        double sceneWidth = 0;
        double sceneHeight = 0;
        double sceneStartX = Double.MAX_VALUE;
        double sceneStartY = Double.MAX_VALUE;
        double sceneEndX = Double.MIN_VALUE;
        double sceneEndY = Double.MIN_VALUE;

        for (PointDouble point : doublePoints) {
            sceneStartX = Math.min(sceneStartX, point.x);
            sceneStartY = Math.min(sceneStartY, point.y);
            sceneEndX = Math.max(sceneEndX, point.x);
            sceneEndY = Math.max(sceneEndY, point.y);
        }

        sceneWidth = sceneEndX - sceneStartX;
        sceneHeight = sceneEndY - sceneStartY;

        //find out which dimension fit first
        boolean heightFits = sceneHeight / sceneWidth / (height - 1) * (width - 1) >= 1;

        double sceneScale;
        if (heightFits) sceneScale = (height - 1) / sceneHeight;
        else sceneScale = (width - 1) / sceneWidth;

        double paddingX = ((double) (width - 1) / sceneScale - sceneWidth) / 2;
        double paddingY = ((double) (height - 1) / sceneScale - sceneHeight) / 2;

        return new double[]{sceneStartX - paddingX, sceneStartY - paddingY, sceneScale};
    }

    /**
     * Scales, centers and flips horizontally in screen coordinates.
     * <p>
     * Here - in contrast to Graphics object - coordinates lie in exact center of each pixel, so if you want
     * to draw line of length 1 which will be scaled with factor 1.0 than it will be painted on 2 pixels - on starting pixel (coordinate 0), and ending one
     * (coordinate 1).
     * <p>
     * @return points in pixel coordinates
     */
    static PointInt[] convertToScreenPoints(int width, int height, double worldStartX, double worldStartY, double worldScale, PointDouble[] doublePoints)
    {
        PointInt[] intPoints = new PointInt[doublePoints.length];
        for (int i = 0; i < intPoints.length; i++)
            intPoints[i] = new PointInt((int) Math.round((doublePoints[i].x - worldStartX) * worldScale),
                                        (height - 1) - (int) Math.round((doublePoints[i].y - worldStartY) * worldScale));
        return intPoints;
    }

    static PointInt[][] convertToScreenPoints(int width, int height, double worldStartX, double worldStartY, double worldScale, PointDouble[][] doublePoints)
    {
        PointInt[][] intPoints = new PointInt[doublePoints.length][];
        for (int i = 0; i < doublePoints.length; i++)
            intPoints[i] = convertToScreenPoints(width, height, worldStartX, worldStartY, worldScale, doublePoints[i]);

        return intPoints;
    }

    /**
     * Creates drag bundle based on mouse pointer x,y. Proper drag region is find on selection hyperrectangle, and then proper cursor, drag type and ratio are
     * chosen for dragging.
     */
    public static DragBundle getSelectionDragBundle(Hyperrectangle base, Hyperrectangle selection, int width, int height, int x, int y)
    {
        //TODO: refactor similar to draw (generic world bounds)
        if (base.numberOfDimensions == 1) {
            PointDouble[] scenePoints = new PointDouble[]{
                new PointDouble(0, 0), new PointDouble(base.size[0], 0),
                new PointDouble(selection.offset[0], 0), new PointDouble(selection.offset[0] + selection.size[0], 0)};

            double[] worldBounds = get2DWorldBounds(width, height, scenePoints);
            PointInt[] points = convertToScreenPoints(width, height, worldBounds[0], worldBounds[1], worldBounds[2], scenePoints);

            int cornerAttractionPx = Math.min(maximumAttractionPx, Math.round(cornerAttractionPc * (points[3].x - points[2].x)));
            int cornerWidth = maximumAttractionPx + cornerAttractionPx;

            Endpoint endpoint = Endpoint.NONE;
            Cursor cursor = null;

            if (Math.abs(height / 2 + style1DHeightHalfPx + style1DHalfGapPx - y) < maximumAttractionPx) {
                if (x >= points[2].x - maximumAttractionPx && x < points[2].x + cornerAttractionPx) {
                    endpoint = Endpoint.LOW;
                    cursor = Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
                } else if (x > points[3].x - cornerAttractionPx && x <= points[3].x + maximumAttractionPx) {
                    endpoint = Endpoint.HIGH;
                    cursor = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
                } else if (x >= points[2].x + cornerAttractionPx && x <= points[3].x - cornerAttractionPx) {
                    endpoint = Endpoint.BOTH;
                    cursor = Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
                }
                DirectionalDrag drag = new DirectionalDrag(x, y, 1, 0);
                double ratio = 1 / get2DWorldBounds(width, height, scenePoints)[2];
                return new DragBundle(drag, 0, endpoint, ratio, cursor);
            } else return null;
        } else if (base.numberOfDimensions == 2) {
            Hyperrectangle intersection = base.clone();
            intersection.intersectWith(selection);
            PointDouble[] scenePoints = new PointDouble[]{
                new PointDouble(0, 0),
                new PointDouble(base.size[0], base.size[1]),
                new PointDouble(selection.offset[0], selection.offset[1]),
                new PointDouble(selection.offset[0] + selection.size[0], selection.offset[1] + selection.size[1]),
                new PointDouble(intersection.offset[0], intersection.offset[1]),
                new PointDouble(intersection.offset[0] + intersection.size[0], intersection.offset[1] + intersection.size[1])};

            double[] worldBounds = get2DWorldBounds(width, height, scenePoints);
            PointInt[] points = convertToScreenPoints(width, height, worldBounds[0], worldBounds[1], worldBounds[2], scenePoints);

            int sx1 = points[2].x;
            int sy1 = points[3].y;
            int sx2 = points[3].x;
            int sy2 = points[2].y;
            int cornerAttractionPx = Math.min(maximumAttractionPx, Math.round(cornerAttractionPc * Math.min(sx2 - sx1, sy2 - sy1)));

            DirectionalDrag dragHorizontal = new DirectionalDrag(x, y, 1, 0);
            DirectionalDrag dragVertical = new DirectionalDrag(x, y, 0, 1);
            double ratio = 1 / get2DWorldBounds(width, height, scenePoints)[2];

            //section of selection into topleft, top, topright, middleleft, middle, middleright, bottomleft, bottom, bottomright            
            if (y >= sy1 - maximumAttractionPx && y < sy1 + cornerAttractionPx) {
                if (x >= sx1 - maximumAttractionPx && x < sx1 + cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, 1, Endpoint.LOW, Endpoint.HIGH, ratio, -ratio, Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
                if (x > sx1 + cornerAttractionPx && x < sx2 - cornerAttractionPx)
                    return new DragBundle(dragVertical, 1, Endpoint.HIGH, -ratio, Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
                if (x <= sx2 + maximumAttractionPx && x > sx2 - cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, 1, Endpoint.HIGH, Endpoint.HIGH, ratio, -ratio, Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
                else return null;
            } else if (y >= sy1 + cornerAttractionPx && y <= sy2 - cornerAttractionPx) {
                if (x >= sx1 - maximumAttractionPx && x < sx1 + cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, Endpoint.LOW, ratio, Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
                if (x > sx1 + cornerAttractionPx && x < sx2 - cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, 1, Endpoint.BOTH, Endpoint.BOTH, ratio, -ratio, Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                if (x <= sx2 + maximumAttractionPx && x > sx2 - cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, Endpoint.HIGH, ratio, Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
                else return null;
            } else if (y <= sy2 + maximumAttractionPx && y > sy2 - cornerAttractionPx) {
                if (x >= sx1 - maximumAttractionPx && x < sx1 + cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, 1, Endpoint.LOW, Endpoint.LOW, ratio, -ratio, Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
                if (x > sx1 + cornerAttractionPx && x < sx2 - cornerAttractionPx)
                    return new DragBundle(dragVertical, 1, Endpoint.LOW, -ratio, Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
                if (x <= sx2 + maximumAttractionPx && x > sx2 - cornerAttractionPx)
                    return new DragBundle(dragHorizontal, 0, 1, Endpoint.HIGH, Endpoint.LOW, ratio, -ratio, Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
                else return null;
            } else return null;
        } else if (base.numberOfDimensions == 3) {
            Hyperrectangle scaledBase = base.clone();
            scaledBase.scale(1.2, true);
            Hyperrectangle intersection = base.clone();
            intersection.intersectWith(selection);
            Hyperrectangle union = Hyperrectangle.union(base, selection);
            //scale in 3D
            if (base.numberOfDimensions == 3) union = Hyperrectangle.union(scaledBase, selection);//union.scale(1.2, true);

            double[] worldBounds = get2DWorldBounds(width, height, union.getVertices2DProjection());
            PointInt[] points = convertToScreenPoints(width, height, worldBounds[0], worldBounds[1], worldBounds[2], selection.getVertices2DProjection());

            Polygon outline = new Polygon(new int[]{points[2].x, points[3].x, points[1].x, points[5].x, points[4].x, points[6].x},
                                          new int[]{points[2].y, points[3].y, points[1].y, points[5].y, points[4].y, points[6].y},
                                          6);
            Polygon faceZ = new Polygon(new int[]{points[4].x, points[6].x, points[7].x, points[5].x},
                                        new int[]{points[4].y, points[6].y, points[7].y, points[5].y},
                                        4);
            Polygon faceY = new Polygon(new int[]{points[6].x, points[2].x, points[3].x, points[7].x},
                                        new int[]{points[6].y, points[2].y, points[3].y, points[7].y},
                                        4);
            Polygon faceX = new Polygon(new int[]{points[7].x, points[3].x, points[1].x, points[5].x},
                                        new int[]{points[7].y, points[3].y, points[1].y, points[5].y},
                                        4);

            DirectionalDrag dragHorizontal = new DirectionalDrag(x, y, 1, 0);
            DirectionalDrag dragVertical = new DirectionalDrag(x, y, 0, 1);
            DirectionalDrag dragYZ = new DirectionalDrag(x, y, 0, 1, zXRatio, -zYRatio);
            DirectionalDrag dragXZ = new DirectionalDrag(x, y, 1, 0, zXRatio, -zYRatio);
            DirectionalDrag dragPerpendicular = new DirectionalDrag(x, y, zXRatio, -zYRatio);
            double ratio = 1 / worldBounds[2];

            if (outline.contains(x, y)) {
                if (faceX.contains(x, y)) {
                    double faceWidth = points[6].y - points[2].y;
                    double distX = (x - points[5].x) / faceWidth;
                    if (distX > edges3DAttractionPc)
                        return new DragBundle(dragYZ, 1, 2, Endpoint.BOTH, Endpoint.BOTH, -ratio, -ratio / zScaleRatio, Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    else
                        return new DragBundle(dragPerpendicular, 2, Endpoint.HIGH, -ratio / zScaleRatio, Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
                } else if (faceY.contains(x, y)) {
                    double faceHeight = points[6].y - points[2].y;
                    double distY = (points[6].y - y) / faceHeight;
                    if (distY > edges3DAttractionPc)
                        return new DragBundle(dragXZ, 0, 2, Endpoint.BOTH, Endpoint.BOTH, ratio, -ratio / zScaleRatio, Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    else
                        return new DragBundle(dragPerpendicular, 2, Endpoint.HIGH, -ratio / zScaleRatio, Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
                } else if (faceZ.contains(x, y)) {
                    double faceHeight = points[4].y - points[6].y;
                    double faceWidth = points[5].x - points[4].x;
                    double distX = (points[5].x - x) / faceWidth;
                    double distY = (y - points[6].y) / faceHeight;
                    if (Math.min(distX, distY) > edges3DAttractionPc)
                        return new DragBundle(dragHorizontal, 0, 1, Endpoint.BOTH, Endpoint.BOTH, ratio, -ratio, Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    else {
                        if (distX < distY)
                            return new DragBundle(dragHorizontal, 0, Endpoint.HIGH, ratio, Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
                        else
                            return new DragBundle(dragVertical, 1, Endpoint.HIGH, -ratio, Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
                    }

                } else return null;
            } else {
                int dimension = -1;
                double distance = Double.MAX_VALUE;

                double currentDistance;
                currentDistance = Math.min(new Line2D.Double(points[4].x, points[4].y, points[6].x, points[6].y).ptSegDist(x, y),
                                           new Line2D.Double(points[6].x, points[6].y, points[2].x, points[2].y).ptSegDist(x, y));
                if (currentDistance < Math.min(distance, maximumAttractionPx)) {
                    dimension = 0;
                    distance = currentDistance;
                }
                currentDistance = Math.min(new Line2D.Double(points[4].x, points[4].y, points[5].x, points[5].y).ptSegDist(x, y),
                                           new Line2D.Double(points[5].x, points[5].y, points[1].x, points[1].y).ptSegDist(x, y));
                if (currentDistance < Math.min(distance, maximumAttractionPx)) {
                    dimension = 1;
                    distance = currentDistance;
                }
                currentDistance = Math.min(new Line2D.Double(points[2].x, points[2].y, points[3].x, points[3].y).ptSegDist(x, y),
                                           new Line2D.Double(points[3].x, points[3].y, points[1].x, points[1].y).ptSegDist(x, y));
                if (currentDistance < Math.min(distance, maximumAttractionPx)) {
                    dimension = 2;
                    distance = currentDistance;
                }

                if (dimension == 0) return new DragBundle(dragHorizontal, dimension, Endpoint.LOW, ratio, Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
                else if (dimension == 1)
                    return new DragBundle(dragVertical, dimension, Endpoint.LOW, -ratio, Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
                else if (dimension == 2)
                    return new DragBundle(dragPerpendicular, dimension, Endpoint.LOW, -ratio / zScaleRatio, Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
                else return null;
            }

        } else return null;
    }
}
