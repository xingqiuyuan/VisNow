package pl.edu.icm.visnow.gui.widgets;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Arrays;
import javax.swing.JFrame;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 * @author szpak
 */
public class SectionPanel extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(SectionPanel.class);
    private Component viewport = null;
    private SectionHeader sectionHeader;

    /**
     * Creates new form SectionPanel
     */
    public SectionPanel()
    {
        initComponents();
    }

    private void initComponents()
    {
        super.setLayout(new BorderLayout());

        sectionHeader = new SectionHeader();
        super.add(sectionHeader, BorderLayout.NORTH);
    }

    @Override
    public int getComponentCount()
    {
        //FIXME: ??? (netbeans gui builder hack)
        if (Arrays.toString(Thread.currentThread().getStackTrace()).indexOf("netbeans") >= 0)
            return super.getComponentCount() - 1;
        else return super.getComponentCount();
    }

    @Override
    public void add(Component comp, Object constraints)
    {
        if (viewport != null) throw new IllegalStateException("Only one viewport is permitted!");
        else {
            viewport = comp;
            super.add(comp, BorderLayout.CENTER);
        }
    }

    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        
        JFrame f = new JFrame();
        SectionPanel s = new SectionPanel();

        f.add(s);
        f.setSize(200, 200);
        f.setLocationRelativeTo(null);

        f.setVisible(true);

    }
}
