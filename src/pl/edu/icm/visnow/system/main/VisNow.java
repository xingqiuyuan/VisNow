//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.system.main;

import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Pattern;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ToolTipManager;
import jcuda.driver.*;
import static jcuda.driver.CUdevice_attribute.*;
import static jcuda.driver.JCudaDriver.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import pl.edu.icm.visnow.application.application.Application;
import pl.edu.icm.visnow.engine.core.ModuleCore;
import pl.edu.icm.visnow.engine.error.Displayer;
import pl.edu.icm.visnow.engine.exception.VNOuterIOException;
import pl.edu.icm.visnow.engine.exception.VNSystemException;
import pl.edu.icm.visnow.engine.library.LibraryCore;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.ReadVisNowField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.config.MainConfig;
import pl.edu.icm.visnow.system.framework.MainWindow;
import pl.edu.icm.visnow.system.framework.ScreenInfo;
import pl.edu.icm.visnow.system.libraries.AttachWizard;
import pl.edu.icm.visnow.system.libraries.MainLibraries;
import pl.edu.icm.visnow.system.libraries.MainTypes;
import pl.edu.icm.visnow.system.swing.UIStyle;
import pl.edu.icm.visnow.system.utils.usermessage.Level;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessage;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessageDispatcher;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessageListener;

public class VisNow {

    private static final Logger LOGGER = Logger.getLogger(VisNow.class);
    public final static String MAIN_CLASS_NAME = "pl.edu.icm.visnow.system.main.VisNow";
    public final static String TITLE = "VisNow";
    public final static String VERSION = "1.3";
    public final static String CONFIG_DIR = ".visnow";
    public final static String CONFIG_VERSION = "0.75";
    public final static String PROJECT_NAME = "VisNow";
    public static final String LOG_OUTPUT_DIR = "log"; //subdir of CONFIG_DIR, place for *.log files (has to be equal as path defined in log4j_*.properties)
    private static final String LOG_CONFIG_TEMPLATES_DIR = "config_templates"; //directory of config templates which are copied to CONFIG_DIR
    private static final String LOG_CONFIG = "vnlog4j_default.properties"; //default configuration for logging in standard mode
    private static final String LOG_CONFIG_DEBUG = "vnlog4j_debug.properties"; //default configuration for logging in debug mode (turned on with command-line '-debug')
    public static final Locale LOCALE = Locale.US;
    public static int performance = 1;
    public static int defaultColorMap = 0;
    public static java.io.File tmpDir = null;
    public static String tmpDirName = "";
    private static boolean devel = false;
    
    public static File getTmpDir() {
        if (tmpDir == null) {
            try {
                tmpDir = java.io.File.createTempFile("visnow", "");
                tmpDir.delete();
                tmpDir.mkdir();
            } catch (Exception e) {
            }
        }
        return tmpDir;
    }

    public static String getTmpDirPath() {
        return getTmpDir().getAbsolutePath();
    }

    public void finish() {
        mainConfig.saveConfig();
        mainConfig.saveColorMaps();
        ConcurrencyUtils.shutdownThreadPoolAndAwaitTermination();
        /*
         * TODO: write libraries
         */
    }

    //<editor-fold defaultstate="collapsed" desc=" PATH/DEVELOPMENT ">
    private String operatingFolder;

    public String getOperatingFolder() {
        return operatingFolder;
    }

    public boolean isDevelopment() {
        return devel;
    }

    private String jarPath;

    public String getJarPath() {
        return jarPath;
    }

    public long getMemoryMax() {
        return memoryMax;
    }

    public long getMemoryAvailable() {
        return (memoryMax - getMemoryUsed());
    }

    private static long getMemoryUsed() {
        Runtime r = Runtime.getRuntime();
        r.gc();
        long total = r.totalMemory();
        long free = r.freeMemory();
        return total - free;
    }

    public static long getMemoryFree() {
        if (visnow != null) {
            return visnow.getMemoryAvailable();
        } else {
            return (getMemoryJVM() - getMemoryUsed());
        }
    }

    /**
     *
     * @return project directory when running from NetBeans, VisNow.jar directory otherwise
     */
    private static String findOperatingFolder() {
        URL url;
        try {
            url = Class.forName(MAIN_CLASS_NAME).getProtectionDomain().getCodeSource().getLocation();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Main class not found. How is everything working?");
        }
        String runningPath = url.toString();

        try {
            runningPath = URLDecoder.decode(runningPath, Charset.defaultCharset().name());
        } catch (UnsupportedEncodingException ex) {
            runningPath = runningPath.replaceAll("%20", " ");
        }

        if (runningPath.startsWith("file:/")) {
            runningPath = runningPath.substring(5);
        }
        if (runningPath.startsWith(":")) {
            runningPath = runningPath.substring(1);
        }
//
//        //String fileStr;
//        if (runningPath.endsWith("build/classes/")) { //RUNNING FROM NETBEANS
//            return runningPath.substring(0, runningPath.length() - "build/classes/".length());
//        } else { //RUNNING FROM JAR
//            if (runningPath.endsWith("VisNow.jar")) {
//                return runningPath.substring(0, runningPath.length() - "VisNow.jar".length());
//            } else {
//                LOGGER.error("Incorrect running path. Continue anyway");
//                return runningPath;
//            }
//        }

        if (runningPath.endsWith("build/classes/")) { //RUNNING FROM NETBEANS
            runningPath = runningPath.substring(0, runningPath.length() - "build/classes/".length());
        } else { //RUNNING FROM JAR
            if (runningPath.endsWith("VisNow.jar")) {
                runningPath = runningPath.substring(0, runningPath.length() - "VisNow.jar".length());
            } else {
                LOGGER.error("Incorrect running path. Continue anyway");
            }
        }

        boolean isWindows = (getOsType() == OsType.OS_WINDOWS);
        if (isWindows) {
            runningPath = runningPath.substring(1);
            runningPath = runningPath.replaceAll("/", "\\\\");
        }
        return runningPath;
    }

    private static String findJarPath() {
        URL url;
        try {
            url = Class.forName(MAIN_CLASS_NAME).getProtectionDomain().getCodeSource().getLocation();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Main class not found. How is everything working?");
        }
        String jarPath = url.toString();

        try {
            jarPath = URLDecoder.decode(jarPath, Charset.defaultCharset().name());
        } catch (UnsupportedEncodingException ex) {
            jarPath = jarPath.replaceAll("%20", " ");
        }

        if (jarPath.startsWith("file:/")) {
            jarPath = jarPath.substring(5);
        }
        if (jarPath.startsWith(":")) {
            jarPath = jarPath.substring(1);
        }

        //String fileStr;
        if (jarPath.endsWith("build/classes/")) { //RUNNING FROM NETBEANS
            devel = true;
            jarPath = jarPath.substring(0, jarPath.length() - 14);
            return jarPath + "dist/" + PROJECT_NAME + ".jar";
        } else { //RUNNING FROM JAR
            devel = false;
            return jarPath;
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" FRAME ">
    private MainWindow mainFrame;

    public MainWindow getMainWindow() {
        return mainFrame;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" CONFIG/LIBRARIES/TYPES/COLORMAPS ">
    private MainLibraries mainLibraries;

    public MainLibraries getMainLibraries() {
        return mainLibraries;
    }

    private MainConfig mainConfig;

    public MainConfig getMainConfig() {
        return mainConfig;
    }

    private MainTypes mainTypes;

    public MainTypes getMainTypes() {
        return mainTypes;
    }

    private AttachWizard attachWizard;

    public AttachWizard getAttachWizard() {
        return attachWizard;
    }

    private static boolean debug = false;

    public static boolean isDebug() {
        return debug;
    }

    //TODO: this method along with findOperatingFolder should be deeply tested across different operating systems
    private static void initConfigDir() {
        File file = new File(new File(System.getProperty("user.home")) + File.separator + ".visnow");
        if (!file.exists()) {
            file.mkdir();
        }
        file = new File(file.getPath() + File.separator + VisNow.CONFIG_VERSION);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    private void initConfig() {
        try {
            mainConfig = new MainConfig(jarPath);
        } catch (VNOuterIOException ex) {
            Displayer.display(200903281600L, ex, this, "IO error while initiating configuration.");
        }
    }

    private void initTypes() {
        mainTypes = new MainTypes();
    }

    private void initLibraries() {
        mainLibraries = new MainLibraries();
    }

    private void initWizard() {
        attachWizard = new AttachWizard();
    }

    private HelpSet helpSet = null;

    public HelpSet getHelpSet() {
        return helpSet;
    }

    private HelpBroker helpBroker = null;

    public HelpBroker getHelpBroker() {
        return helpBroker;
    }

    private void initLaf() {
        try {

//            javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
//            javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
//            javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
//            javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.synth.SynthLookAndFeel");
//            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            if (getOsType() == OsType.OS_MAC) {
                javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            }

        } catch (Exception ex) {
            LOGGER.info("Failed to set look-and-feel");
        }
    }

    private void initHelp() {
        if (jarPath == null) {
            return;
        }

        String helpJarPath = jarPath.substring(0, jarPath.lastIndexOf("/")) + "/doc/VisNowHelp.jar";
        File helpJarFile = new File(helpJarPath);

        String helpHS = "vnhelp_HS.hs";
        try {
            URLClassLoader loader = new URLClassLoader(new URL[]{
                helpJarFile.toURI().toURL()
            });
            URL hsURL = HelpSet.findHelpSet(loader, helpHS);
            if (hsURL == null) {
                throw new Exception();
            }
            helpSet = new HelpSet(VisNow.class.getClassLoader(), hsURL);
        } catch (Exception ee) {
            helpSet = null;
            helpBroker = null;
            LOGGER.warn("Help system NOT initialized - HelpSet not found");
            return;
        }
        helpBroker = helpSet.createHelpBroker();
        helpBroker.setSize(new Dimension(1000, 600));

    }

    public void showHelp(String topicID) {
        if (helpBroker == null) {
            return;
        }

        if (topicID == null) {
            topicID = "visnow.nohelp";
        }
        try {
            VisNow.get().getHelpBroker().setCurrentID(topicID);
            VisNow.get().getHelpBroker().setDisplayed(true);
        } catch (Exception ee) {
            try {
                VisNow.get().getHelpBroker().setCurrentID("visnow.nohelp");
                VisNow.get().getHelpBroker().setDisplayed(true);
            } catch (Exception eee) {
                LOGGER.error("ERROR: help system not initialized or corrupted!");
            }
        }
    }

    //</editor-fold>
    private static final String[] VTK81LIBS = {"vtksys-8.1", "vtkzlib-8.1", "vtklz4-8.1", "vtkexpat-8.1", "vtkCommonCore-8.1", "vtkWrappingJava-8.1", "vtkCommonCoreJava",
        "vtkCommonMath-8.1", "vtkCommonMathJava", "vtkCommonTransforms-8.1", "vtkCommonTransformsJava", "vtkCommonMisc-8.1", "vtkCommonMiscJava", "vtkCommonSystem-8.1", "vtkCommonSystemJava", "vtkCommonDataModel-8.1", "vtkCommonDataModelJava", "vtkCommonExecutionModel-8.1",
        "vtkCommonExecutionModelJava", "vtkIOCore-8.1", "vtkIOCoreJava", "vtkIOLegacy-8.1", "vtkIOLegacyJava", "vtkIOXMLParser-8.1", "vtkIOXMLParserJava",
        "vtkIOXML-8.1", "vtkIOXMLJava", "vtkFiltersCore-8.1", "vtkFiltersCoreJava", "vtkCommonComputationalGeometry-8.1", "vtkCommonComputationalGeometryJava", "vtkFiltersGeneral-8.1", "vtkFiltersGeneralJava"};
    private static ArrayList<String> loadedNativeLibraries = new ArrayList<String>();

    public static boolean isNativeLibraryLoaded(String library) {
        return loadedNativeLibraries.contains(library);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Native ">
    private void loadNativeLibraries() {
//        LOGGER.info("Initializing native libraries...");
        String libDir = getOperatingFolder();

        LOGGER.info("    os.name: " + System.getProperty("os.name"));
        LOGGER.info("    os.arch: " + System.getProperty("os.arch"));

        boolean isLinux = (getOsType() == OsType.OS_LINUX);
        boolean isWindows = (getOsType() == OsType.OS_WINDOWS);
        boolean isMac = (getOsType() == OsType.OS_MAC);
        boolean is64 = VisNow.isJreArch64();
        String sep = File.separator;

        if (!isLinux && !isWindows && !isMac) {
            LOGGER.warn("    native libraries not found (you should use Linux, Windows or macOS)");
            return;
        }

        if (devel) {
            libDir += sep + "dist" + sep + "lib" + sep + "native";
        } else {
            libDir += sep + "lib" + sep + "native";
        }

        if (!(new File(libDir)).exists()) {
            LOGGER.error("    native libraries directory not found: " + libDir);
            return;
        }

        if (isLinux && (new File(libDir + sep + "linux")).exists()) {
            libDir += sep + "linux";
            if (is64) {
                libDir += sep + "x86_64";
            } else {
                libDir += sep + "x86";
            }
        }
        else if (isMac && (new File(libDir + sep + "macOS")).exists()) {
            libDir += sep + "macOS";
            if (is64) {
                libDir += sep + "x86_64";
            } else {
                libDir += sep + "x86";
            }
        }
        else if (isWindows && (new File(libDir + sep + "windows")).exists()) {
            libDir += sep + "windows";
            if (is64) {
                libDir += sep + "win64";
            } else {
                libDir += sep + "win32";
            }
        }

        File nativeLibDir = new File(libDir);
        if (!nativeLibDir.exists()) {
            LOGGER.warn("    native libraries directory not found");
            return;
        }

        String string = "    Loading native shared library for ";
        if (isLinux) {
            string += "Linux ";
        }
        else if (isMac) {
            string += "macOS ";
        }
        else if (isWindows) {
            string += "Windows ";
        }

        if (is64) {
            string += "64-bit";
        } else if (isWindows) {
            string += "32-bit";
        }
        string += ".";
        LOGGER.info(string);

        String[] nativeLibraries = nativeLibDir.list();
        if (nativeLibraries.length > 0) {
            reorderLibraries(nativeLibraries);
        }
        for (String lib : nativeLibraries) {
            File libF = new File(libDir + sep + lib);
            if (!libF.isDirectory()) {
//                LOGGER.debug(lib + " is not a directory, ommiting!");
                continue;
            }

            String jlp = System.getProperty("java.library.path");
            jlp = jlp + File.pathSeparator + libDir + sep + lib;
            try {
                System.setProperty("java.library.path", jlp);
                Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
                fieldSysPath.setAccessible(true);
                fieldSysPath.set(null, null);
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                //ex.printStackTrace();
                LOGGER.warn("Loading " + lib + " failed on setting library path");
                continue;
            }
            boolean successOnAllFiles = true;
            ArrayList<String> failedReads = new ArrayList<>();
            ArrayList<String> failedReadsReason = new ArrayList<>();
            if (lib.equals("vtk")) {
                for (String libToRead : VTK81LIBS) {
                    if(isMac && libToRead.contains("8.1")) {
                        libToRead += ".1";
                    }
                    try {
                        System.loadLibrary(libToRead);
                    } catch (UnsatisfiedLinkError err) {
                        failedReads.add(libToRead);
                        failedReadsReason.add(err.getMessage());
                        successOnAllFiles = false;
                    }
                }
            } else {
                String[] libraryFiles = libF.list(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return (name.toLowerCase().endsWith(".dll") || name.toLowerCase().endsWith(".so") || name.toLowerCase().endsWith(".dylib"));
                    }
                });
                for (String libToRead : libraryFiles) {
                    if (libToRead.startsWith("lib")) {
                        libToRead = libToRead.substring(3);
                    }
                    if (libToRead.endsWith(".so")) {
                        libToRead = libToRead.substring(0, libToRead.lastIndexOf(".so"));
                    } else if (libToRead.endsWith(".dylib")) {
                        libToRead = libToRead.substring(0, libToRead.lastIndexOf(".dylib"));
                    } else if (libToRead.endsWith(".dll")) {
                        libToRead = libToRead.substring(0, libToRead.lastIndexOf(".dll"));
                    }
                    try {
                        System.loadLibrary(libToRead);
                    } catch (UnsatisfiedLinkError err) {
                        failedReads.add(libToRead);
                        failedReadsReason.add(err.getMessage());
                        successOnAllFiles = false;
                    }
                }
            }
            if (successOnAllFiles) {
//                LOGGER.info("Loaded " + lib + " native library.");
                loadedNativeLibraries.add(lib);
            } else {
                String failMessage = "Loading " + lib + " failed on library:";
                for (int i = 0; i < failedReads.size(); i++) {
                    failMessage += " " + failedReads.get(i) + " (Reason: " + failedReadsReason.get(i) + ") ";
                }
                LOGGER.warn(failMessage);
            }
        }
    }

    /**
     * Changes the order of loading of library directories. Any dependencies should be explicitly expressed in this method.
     * <p>
     * @param libraries the directories to be reordered.
     */
    private static void reorderLibraries(String[] libraries) {
        // For now, Nvidia CUDA native libraries should be loaded first, so that
        // JCuda wrapper natives can load without errors.   
        Pattern p = Pattern.compile("cuda-.*");

        if (p.matcher(libraries[0]).matches()) {
            return;
        }

        for (int i = 0; i < libraries.length; ++i) {
            if (p.matcher(libraries[i]).matches()) {
                String tmp = libraries[0];
                libraries[0] = libraries[i];
                libraries[i] = tmp;
                break;
            }
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" **MAIN** ">
    private static VisNow visnow;

    public static VisNow get() {
        return visnow;
    }

    //WTF co to robi?
    private void profile() {
        JLabel jl = new JLabel("VisNow");
        jl.setFont(new java.awt.Font("Tahoma", 1, 9));
        jl.getFontMetrics(jl.getFont()).charsWidth(
                jl.getText().toCharArray(),
                0,
                jl.getText().length());
    }

    private VisNow() {
        profile();
    }

    private void init(String args[]) throws VNSystemException {
        init(args, true, false);
    }

    protected String args[];
    protected boolean frameVisible;

    private void init(final String args[], final boolean frameVisible, final boolean disableStartupViewers) throws VNSystemException {
        this.args = args;
        this.frameVisible = frameVisible;
        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                UIStyle.initStyle();
                initMemory();
                jarPath = findJarPath();
                operatingFolder = findOperatingFolder();

                renderSplashFrame(0.2f, "Loading native libraries...");
                loadNativeLibraries();
                renderSplashFrame(0.3f, "Initializing Java3D...");
                initJava3D();
                renderSplashFrame(0.4f, "Initializing config...");
                initLibraries();
                initTypes();
                initConfig();
                renderSplashFrame(0.5f, "Initializing module wizard...");
                initWizard();
                renderSplashFrame(0.6f, "Initializing help...");
                initHelp();
                initLaf();

                renderSplashFrame(0.7f, "Creating main window...");
                mainFrame = new MainWindow();
                LOGGER.debug(getDeviceResolution());
                mainFrame.setBounds(getPreferredMainWindowBounds(getDeviceResolution()));

                // Automatic application creation when started with a .vna filename as a parameter 
                boolean applicationLoaded = false;
                if (args.length != 0) {
                    for (String arg : args) {
                        if (arg.endsWith(".vna") || arg.endsWith(".VNA")) {
                            File f = new File(arg);
                            mainFrame.getMainMenu().betaOpenFile(f);
                            applicationLoaded = true;
                        }
                    }
                }
                if (!applicationLoaded) {
                    Application app = new Application(
                            "Untitled(" + mainFrame.getMainMenu().nextUntitled() + ")",
                            disableStartupViewers);
                    mainFrame.getApplicationsPanel().addApplication(app);
                    if (args.length != 0) {
                        for (String arg : args) {
                            // Initializing with the default reader when called with a .vnf filename as a parameter            
                            if (arg.endsWith(".vnf") || arg.endsWith(".VNF")) {
                                app.addModuleByName("VisNow field reader",
                                        "pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.ReadVisNowField",
                                        new Point(100, 20));
                                ReadVisNowField reader = (ReadVisNowField) app.getEngine().getModule("VisNow field reader").getCore();
                                reader.getParameters().setValue("File name", arg);
                            }
                        }
                    }
                }
                if (splash != null) {
                    try {
                        splash.close();
                    } catch (IllegalStateException ex) {
                    }
                }
                //TODO: test tool tip times
                //original delays are: 750 4000 500
                ToolTipManager.sharedInstance().setInitialDelay(250);
                ToolTipManager.sharedInstance().setDismissDelay(8000);
                ToolTipManager.sharedInstance().setReshowDelay(500);

                mainFrame.getApplicationsPanel().startPageSetVisible(!mainConfig.isStartPageHidden());
                mainFrame.setVisible(frameVisible);
                mainFrame.toFront();

            }
        });
    }

    /**
     * Returns device resolution in likely multi-device environment.
     */
    public static Rectangle getDeviceResolution() {
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();
    }

    /**
     *
     * @return main window bounds stored in preferences (if exist) or optimal (calculated) main window bounds
     */
    private static Rectangle getPreferredMainWindowBounds(Rectangle screenBounds) {
        String bounds = VisNow.get().getMainConfig().getMainWindowBounds();
        if (bounds == null || bounds.isEmpty()) {
            return getOptimalMainWindowBounds(screenBounds);
        } else {
            String[] b = bounds.split(",");
            return new Rectangle(Integer.parseInt(b[0].trim()), Integer.parseInt(b[1].trim()), Integer.parseInt(b[2].trim()), Integer.parseInt(b[3].trim()));
        }
    }

    /**
     * Calculates optimal bounds for main window depending on screen resolution. Optimal means:
     * <ul>stick together main window and viewer window; it will appear on the left (default) or right to the main window - depending on available space
     * <ul>align them at top edge
     * <ul>leave square area for viewer but not larger than half of the screen width
     * <ul>occupy maximum area for main window, not less than minimumMainWindowSize (unless screenSize is even less than that)
     * <p>
     * @return main window bounds: {offsetX, offsetY, width, height}
     */
    private static Rectangle getOptimalMainWindowBounds(Rectangle screenBounds) {
        int w = screenBounds.width;
        int h = screenBounds.height;
        int minimumMainWindowSize = 900;
        int viewerSize = Math.min(h, w / 2);
        int preferredWidth = Math.min(Math.max(w - viewerSize, minimumMainWindowSize), w);
        return new Rectangle(screenBounds.x + w - preferredWidth, screenBounds.y, preferredWidth, h - 100);
    }

    /**
     *
     * @return main window bounds stored in preferences (if exist) or optimal (calculated) main window bounds
     */
    public static Rectangle getPreferredViewerWindowBounds(Rectangle screenBounds, Rectangle mainWindowBounds) {
        String bounds = VisNow.get().getMainConfig().getViewer3DWindowBounds();
        if (bounds == null || bounds.isEmpty()) {
            return getOptimalViewerWindowBounds(screenBounds, mainWindowBounds);
        } else {
            String[] b = bounds.split(",");
            return new Rectangle(Integer.parseInt(b[0].trim()), Integer.parseInt(b[1].trim()), Integer.parseInt(b[2].trim()), Integer.parseInt(b[3].trim()));
        }
    }

    /**
     * Calculates optimal bounds for viewer window. Tries to place viewer window on the left or right of main window. See
     * {@link #getOptimalMainWindowBounds(java.awt.Rectangle)} for details.
     */
    public static Rectangle getOptimalViewerWindowBounds(Rectangle screenBounds, Rectangle mainWindowBounds) {
        int w = screenBounds.width;
        int h = screenBounds.height;
        int minimumViewerSize = 300;
        //enough space on the left
        if (mainWindowBounds.x - screenBounds.x >= minimumViewerSize) {
            int viewerEdgeSize = Math.max(minimumViewerSize, Math.min(h - mainWindowBounds.y + screenBounds.y, mainWindowBounds.x - screenBounds.x));
            return new Rectangle(mainWindowBounds.x - viewerEdgeSize, mainWindowBounds.y, viewerEdgeSize, viewerEdgeSize);
        } //enough space on the right
        else if (w - mainWindowBounds.x - mainWindowBounds.width + screenBounds.x >= minimumViewerSize) {
            int viewerEdgeSize = Math.max(minimumViewerSize, Math.min(h - mainWindowBounds.y + screenBounds.y, w - mainWindowBounds.width - mainWindowBounds.x + screenBounds.x));
            return new Rectangle(mainWindowBounds.x + mainWindowBounds.width, mainWindowBounds.y, viewerEdgeSize, viewerEdgeSize);
        } // else just take some small part of the screen
        else {
            int viewerEdgeSize = Math.max(minimumViewerSize, Math.min(h / 2, w / 3));
            return new Rectangle(screenBounds.x, screenBounds.y, viewerEdgeSize, viewerEdgeSize);
        }
    }

    public int getMainScreenID() {
        if (visnow == null) {
            return -1;
        }
        return ScreenInfo.getScreenID(visnow.getMainWindow());
    }

    public Dimension getMainScreenDimension() {
        if (visnow == null) {
            return new Dimension(0, 0);
        }
        return ScreenInfo.getScreenDimension(ScreenInfo.getScreenID(visnow.getMainWindow()));
    }

    public void initDefaultExceptionHandler() {
        ConcurrencyUtils.setThreadPool(Executors.newCachedThreadPool(new VisNowThreadFactory(new VisNowExceptionHandler())));
        Thread.setDefaultUncaughtExceptionHandler(new VisNowExceptionHandler());
    }

    public static void initLogging() {
        initLogging(false);
    }

    /**
     * Initializes logging which is: - creating log_output_dir (if not exists) - copy log4j*.properties into configuration directory (if already not there) -
     * reset/init log4j configuration
     */
    public static void initLogging(boolean forceDebug) {
        try {
            //create log output dir (has to be the same as one specified in log4j*.properties file)
            makeConfigDir(LOG_OUTPUT_DIR, false);
            //location of log config templates
            File logConfigTemplatesDir = new File(findOperatingFolder() + File.separator + LOG_CONFIG_TEMPLATES_DIR);
            //get config dir (place for log config templates)
            File configDir = getConfigDir();

            String logConfigFilename = isDebug() || forceDebug ? LOG_CONFIG_DEBUG : LOG_CONFIG;
            //copy log templates
            Files.copy(Paths.get(logConfigTemplatesDir.toString() + File.separator + logConfigFilename),
                    Paths.get(configDir.toString() + File.separator + logConfigFilename), StandardCopyOption.REPLACE_EXISTING);

            LogManager.resetConfiguration();
            PropertyConfigurator.configure(configDir.toString() + File.separator + logConfigFilename);
        } catch (IOException e) {
            LOGGER.error("Can't load logger configuration ", e);
        }
    }

    //global dispatcher for user messages
    private UserMessageDispatcher userMessageDispatcher;

    /**
     * Initializes global user message dispatcher with default listeners (terminal, status bar label, message panel).
     */
    private void initUserMessages() {
        LOGGER.info("");
        userMessageDispatcher = new UserMessageDispatcher();
        //1. Terminal output listener
        userMessageDispatcher.addListener(new UserMessageListener() {
            @Override
            public void newMessage(UserMessage message) {
                //System.out.println(message.getInfo(true));
            }
        });

        //2, 3. MainWindow listeners (status label, message panel)
        if (VisNow.get().mainFrame != null) {
            for (UserMessageListener uml : VisNow.get().mainFrame.getUserMessageListeners()) {
                userMessageDispatcher.addListener(uml);
            }
        }
    }

    public void addUserMessageListener(UserMessageListener userMessageListener) {
        userMessageDispatcher.addListener(userMessageListener);
    }

    /**
     * Sends <code>message</code> to all registered listeners.
     */
    public void userMessageSend(UserMessage message) {
        if (userMessageDispatcher == null) {
            return;
        }

        userMessageDispatcher.dispatch(message);
    }

    /**
     * Sends <code>message</code> to all registered listeners. Application name and source name are taken from {@code moduleCore}.
     * <p>
     * @param title message title
     * @param details message details
     */
    public void userMessageSend(ModuleCore moduleCore, String title, String details, Level level) {
        //TODO: find general solution for applications
        if (userMessageDispatcher != null) { //quick fix for custom applications
            if (moduleCore == null) {
                userMessageDispatcher.dispatch(new UserMessage("", "", title, details, level));
            } else if (moduleCore.getApplication() == null) {
                userMessageDispatcher.dispatch(new UserMessage("", moduleCore.getName(), title, details, level));
            } else {
                userMessageDispatcher.dispatch(new UserMessage(moduleCore.getApplication().getTitle(), moduleCore.getName(), title, details, level));
            }
        }
    }

    public void userMessageSend(LibraryCore libraryCore) {
        String applicationName = mainFrame.getApplicationsPanel().getCurrentApplication().getTitle();
        String sourceName = libraryCore.getName();
        String desc = (libraryCore.getShortDescription() == null) ? "" : libraryCore.getShortDescription();
        String details = (libraryCore.getLongDescription() == null) ? desc : libraryCore.getLongDescription();

        userMessageDispatcher.dispatch(new UserMessage(applicationName, sourceName, desc, details, Level.HELP));
    }

    private final static SplashScreen splash = SplashScreen.getSplashScreen();

    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                for (String arg : args) {
                    if (arg.equals("-debug")) {
                        debug = true;
                    }
                }
                Locale.setDefault(VisNow.LOCALE);

                renderSplashFrame(0.1f, "Initializing logging...");
                //TODO: test it see #initConfigDir
                //                initConfigDir();
                initLogging();
                //startupInfo();

                try {
                    visnow = new VisNow();
                    visnow.initDefaultExceptionHandler();

                    int argsCount = 0;
                    for (String arg : args) {
                        if (!arg.startsWith("-")) {
                            argsCount++;
                        }
                    }
                    String[] args2 = new String[argsCount];
                    int c = 0;
                    for (String arg : args) {
                        if (!arg.startsWith("-")) {
                            args2[c++] = arg;
                        }
                    }
                    visnow.init(args2);
                } catch (VNSystemException ex) {
                    Displayer.display(1010101010, ex, null, "Initialization failed.");
                    return;
                }

                visnow.initUserMessages();
            }
        });

    }

    public static void mainBlocking(final String[] args) {
        mainBlocking(args, true);
    }

    public static void mainBlocking(final String[] args, final boolean showMainFrame) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-debug")) {
                debug = true;
            }
        }

        Locale.setDefault(VisNow.LOCALE);

        try {
            visnow = new VisNow();
            visnow.initDefaultExceptionHandler();
            initLogging();
            visnow.init(args, showMainFrame, true);

        } catch (VNSystemException ex) {
            Displayer.display(1010101010, ex, null, "Initialization failed.");
        }
        visnow.initUserMessages();
    }

    private static void startupInfo() {
        LOGGER.info("");
        LOGGER.info("");
        LOGGER.info("-------- VisNow startup info --------");

        if (debug) {
            RuntimeMXBean RuntimemxBean = ManagementFactory.getRuntimeMXBean();
            List<String> aList = RuntimemxBean.getInputArguments();
            LOGGER.info(" * JVM startup flags:");
            for (int i = 0; i < aList.size(); i++) {
                LOGGER.info("    " + aList.get(i));
            }
            LOGGER.info("");
            Set<String> p = System.getProperties().stringPropertyNames();
            Iterator<String> ip = p.iterator();
            LOGGER.info(" * System properties:");
            String key;
            while (ip.hasNext()) {
                key = ip.next();
                LOGGER.info("    " + key + " = " + System.getProperty(key));
            }
            LOGGER.info("");

            Map<String, String> env = System.getenv();
            Set<String> envKeys = env.keySet();
            Iterator<String> envKeysI = envKeys.iterator();

            LOGGER.info(" * Environment variables:");
            while (envKeysI.hasNext()) {
                key = envKeysI.next();
                LOGGER.info("    " + key + " = " + env.get(key));
            }
            LOGGER.info("  ");
            LOGGER.info("------ Java Advanced Imaging info -------");
            String[] formats = ImageIO.getReaderFormatNames();
            String readerDescription, readerVendorName, readerVersion;
            ImageReader reader;
            ImageReaderSpi spi;
            for (int i = 0; i < formats.length; i++) {
                Iterator<ImageReader> tmpReaders = ImageIO.getImageReadersByFormatName(formats[i]);
                while (tmpReaders.hasNext()) {
                    reader = tmpReaders.next();
                    spi = reader.getOriginatingProvider();
                    readerDescription = spi.getDescription(Locale.US);
                    readerVendorName = spi.getVendorName();
                    readerVersion = spi.getVersion();
                    LOGGER.info("    " + formats[i] + ": " + readerDescription + " " + readerVendorName + " " + readerVersion);
                }
            }
            LOGGER.info("-----------------------------------------");

        } else {

            LOGGER.info(" * System properties:");
            LOGGER.info("    java.runtime.name = " + System.getProperty("java.runtime.name"));
            LOGGER.info("    java.vm.version = " + System.getProperty("java.vm.version"));
            LOGGER.info("    java.vm.vendor = " + System.getProperty("java.vm.vendor"));
            LOGGER.info("    java.vm.name = " + System.getProperty("java.vm.name"));
            LOGGER.info("    java.specification.version = " + System.getProperty("java.specification.version"));
            LOGGER.info("    java.runtime.version = " + System.getProperty("java.runtime.version"));
            LOGGER.info("    os.arch = " + System.getProperty("os.arch"));
            LOGGER.info("    os.name = " + System.getProperty("os.name"));
            LOGGER.info("    os.version = " + System.getProperty("os.version"));
            LOGGER.info("    java.library.path = " + System.getProperty("java.library.path"));
            LOGGER.info("    java.class.path = " + System.getProperty("java.class.path"));
            LOGGER.info("    java.ext.dirs = " + System.getProperty("java.ext.dirs"));
            LOGGER.info("");
            LOGGER.info(" * Environment variables:");
            LOGGER.info("    JAVA_HOME = " + System.getenv("JAVA_HOME"));
            LOGGER.info("    PATH = " + System.getenv("PATH"));
            LOGGER.info("    LD_LIBRARY_PATH = " + System.getenv("LD_LIBRARY_PATH"));
            LOGGER.info("    CLASSPATH = " + System.getenv("CLASSPATH"));
            LOGGER.info("-------------------------------------");

        }
        LOGGER.info("");
        LOGGER.info("");

    }

    private long memoryMax = Long.MAX_VALUE;

    private void initMemory() {
        memoryMax = getMemoryJVM();
    }

    public static long getMemoryJVM() {
        long memMax = Long.MAX_VALUE;

        RuntimeMXBean RuntimemxBean = ManagementFactory.getRuntimeMXBean();
        List<String> aList = RuntimemxBean.getInputArguments();

        for (int i = 0; i < aList.size(); i++) {
            String str = aList.get(i);
            if (debug) {
//                LOGGER.info("JVM input argument #" + i + ": " + str);
            }
            if (str.startsWith("-Xmx")) {
                String amount = str.substring(4, str.length() - 1);
                String unit = str.substring(str.length() - 1);
                try {
                    memMax = Long.parseLong(amount);
                    if (unit.equalsIgnoreCase("k")) {
                        memMax *= 1024L;
                    } else if (unit.equalsIgnoreCase("m")) {
                        memMax *= 1024L * 1024L;
                    } else if (unit.equalsIgnoreCase("g")) {
                        memMax *= 1024L * 1024L * 1024L;
                    }
                } catch (NumberFormatException ex) {
                    memMax = Long.MAX_VALUE;
                }
                if (debug) {
//                    LOGGER.info("VisNow started with maximum memory: " + memMax + " bytes");
                }
            }
        }
        return memMax;
    }

    private static void initJava3D() {
        LOGGER.info("Initializing Java3D... ");
        try {
            Class cl = Class.forName("javax.media.j3d.VirtualUniverse");
            VisNowJava3DInit.initJava3D(debug, LOGGER);
            persistJava3DState(true);
        } catch (HeadlessException ex) {
            LOGGER.fatal("ERROR: cannot initialize display!", ex);
            System.exit(1);
        } catch (VisNowJava3DInit.Java3DVersionException ex) {
            boolean oldState = readJava3DPersistedState();
            if (oldState) {
                LOGGER.fatal("ERROR: Java3D version " + ex.getVersion() + " is not supported. Minimum version is 1.5.2! Please refer to VisNow documentation.\nNOTE: Please note that VisNow was already running properly with Java3D on this machine.\nThis might mean that some external changes have been made to your Java3D installation.\nTry reinstalling Java3D or rerun VisNow installer.");
                try {
                    JOptionPane.showMessageDialog(null, "ERROR!\n\nJava3D version " + ex.getVersion() + " is not supported.\nMinimum version is 1.5.2!\n\nPlease refer to VisNow documentation.\n\n\n\nNOTE: Please note that VisNow was already running properly with Java3D on this machine.\nThis might mean that some external changes have been made to your Java3D installation.\nTry reinstalling Java3D or rerun VisNow installer.", "Java3D version error", JOptionPane.ERROR_MESSAGE);
                } catch (HeadlessException hex) {
                }
            } else {
                LOGGER.fatal("ERROR: Java3D version " + ex.getVersion() + " is not supported. Minimum version is 1.5.2! Please refer to VisNow documentation.");
                try {
                    JOptionPane.showMessageDialog(null, "ERROR!\n\nJava3D version " + ex.getVersion() + " is not supported.\nMinimum version is 1.5.2!\n\nPlease refer to VisNow documentation.\n\n", "Java3D version error", JOptionPane.ERROR_MESSAGE);
                } catch (HeadlessException hex) {
                }
            }
            System.exit(1);
        } catch (Exception ex) {
            boolean oldState = readJava3DPersistedState();
            if (oldState) {
                LOGGER.fatal("ERROR: cannot initialize Java3D library!\nNOTE: Please note that VisNow was already running properly with Java3D on this machine.\nThis might mean that some external changes have been made to your Java3D installation.\nTry reinstalling Java3D or rerun VisNow installer.", ex);
                try {
                    JOptionPane.showMessageDialog(null, "ERROR!\n\nCannot initialize Java3D library.\n\nPlease refer to VisNow documentation\nor contact your system adminstrator.\n\n\n\nNOTE: Please note that VisNow was already running properly with Java3D on this machine.\nThis might mean that some external changes have been made to your Java3D installation.\nTry reinstalling Java3D or rerun VisNow installer.", "Java3D error", JOptionPane.ERROR_MESSAGE);
                } catch (HeadlessException hex) {
                }
            } else {
                LOGGER.fatal("ERROR: cannot initialize Java3D library!");
                try {
                    JOptionPane.showMessageDialog(null, "ERROR!\n\nCannot initialize Java3D library.\n\nPlease refer to VisNow documentation\nor contact your system adminstrator.\n\n", "Java3D error", JOptionPane.ERROR_MESSAGE);
                } catch (HeadlessException hex) {
                }
            }
            System.exit(1);
        }
//        LOGGER.info("OK");
//        LOGGER.info("");
    }

    /**
     * Returns config dir (or its subdir).
     *
     * @param subdir subdir of config dir or empty/null for no subdir
     */
    public static File getConfigDir(String subdir, boolean withVersionSuffix) {
        String configDirFullPath;
        if (withVersionSuffix) {
            configDirFullPath = System.getProperty("user.home") + File.separator + CONFIG_DIR + File.separator + CONFIG_VERSION;
        } else {
            configDirFullPath = System.getProperty("user.home") + File.separator + CONFIG_DIR;
        }

        if (subdir != null && !subdir.equals("")) {
            configDirFullPath = configDirFullPath + File.separator + subdir;
        }
        File file = new File(configDirFullPath);
        return file;
    }

    /**
     * Returns config dir (with version suffix).
     */
    public static File getConfigDir() {
        return getConfigDir(null, true);
    }

    /**
     * Creates (if does not exist) and returns config dir (with version suffix).
     */
    public static File makeConfigDir(String subdir, boolean withVersionSuffix) throws IOException {
        File file = getConfigDir(subdir, withVersionSuffix);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new IOException("Can't create config directory [" + file.toString() + "]");
            }
        }
        return file;
    }

    private static void persistJava3DState(boolean state) {
        File file = new File(new File(System.getProperty("user.home")) + File.separator + ".visnow");
        if (!file.exists()) {
            file.mkdir();
        }
        file = new File(file.getPath() + File.separator + VisNow.CONFIG_VERSION);
        if (!file.exists()) {
            file.mkdir();
        }
        file = new File(file.getPath() + File.separator + "j3dinit");

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            out.write("" + state + "\n");
            out.close();
        } catch (IOException ex) {
        }
    }

    private static boolean readJava3DPersistedState() {
        File file = new File(new File(System.getProperty("user.home")) + File.separator + ".visnow" + File.separator + VisNow.CONFIG_VERSION + File.separator + "j3dinit");
        if (!file.exists()) {
            return false;
        }

        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String state = in.readLine();
            if ("true".equals(state)) {
                return true;
            }
            return false;
        } catch (FileNotFoundException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
    }

    //</editor-fold>
    public static enum OsType {

        OS_WINDOWS, OS_LINUX, OS_MAC, OS_SOLARIS, OS_AIX, OS_UNKNOWN
    }

    private static OsType osType = null;

    public static OsType getOsType() {
        if (osType == null) {
            String osName = System.getProperty("os.name");
            if (osName.startsWith("Windows") || osName.startsWith("windows")) {
                osType = OsType.OS_WINDOWS;
                return osType;
            } else if (osName.equalsIgnoreCase("Linux") || osName.equalsIgnoreCase("FreeBSD") || osName.contains("Unix")) {
                osType = OsType.OS_LINUX;
            } else if (osName.startsWith("Mac")) {
                osType = OsType.OS_MAC;
            } else if (osName.equalsIgnoreCase("Solaris")) {
                osType = OsType.OS_SOLARIS;
            } else if (osName.equalsIgnoreCase("AIX")) {
                osType = OsType.OS_AIX;
            } else {
                osType = OsType.OS_UNKNOWN;
            }
        }
        return osType;
    }

    public static enum CpuArch {

        CPU_X86, CPU_X86_64, CPU_PPC, CPU_PPC_64, CPU_SPARC, CPU_SPARCV9, CPU_OTHER
    }

    private static CpuArch jreArch = null;

    public static boolean isJreArch64() {
        CpuArch arch = getJreArch();
        return (arch == CpuArch.CPU_PPC_64 || arch == CpuArch.CPU_X86_64 || arch == CpuArch.CPU_SPARCV9);
    }

    public static CpuArch getJreArch() {
        if (jreArch == null) {
            String arch = System.getProperty("os.arch");

            if ((arch.contains("86") && arch.contains("64")) || arch.toLowerCase().contains("amd64")) {
                jreArch = CpuArch.CPU_X86_64;
            } else if ((arch.contains("86") && !arch.contains("64")) || arch.contains("i386")) {
                jreArch = CpuArch.CPU_X86;
            } else if ((arch.toLowerCase().contains("ppc") || arch.toLowerCase().contains("powerpc")) && arch.contains("64")) {
                jreArch = CpuArch.CPU_PPC_64;
            } else if ((arch.toLowerCase().contains("ppc") || arch.toLowerCase().contains("powerpc")) && !arch.contains("64")) {
                jreArch = CpuArch.CPU_PPC;
            } else if (arch.toLowerCase().contains("sparc") && arch.contains("9")) {
                jreArch = CpuArch.CPU_SPARCV9;
            } else if (arch.toLowerCase().contains("sparc") && !arch.contains("9")) {
                jreArch = CpuArch.CPU_SPARC;
            } else {
                jreArch = CpuArch.CPU_OTHER;
            }
        }
        return jreArch;
    }

    public static String getIconPath() {
        //return "/pl/edu/icm/visnow/gui/icons/vn.png";
        return "/pl/edu/icm/visnow/gui/icons/big/visnow.png";
    }

    //<editor-fold defaultstate="collapsed" desc=" **SPLASH** ">
    static final int PROGRESS_BAR_X_MARGIN = 2;
    static final int PROGRESS_BAR_Y_MARGIN = 60;
    static final int PROGRESS_BAR_HEIGHT = 10;
    static final int PROGRESS_TEXT_X_POSITION = 10;
    static final int PROGRESS_TEXT_Y_MARGIN = PROGRESS_BAR_Y_MARGIN + 5;
    static final int BOTTOM_TEXT_X_MARGIN = 10;
    static final int BOTTOM_TEXT_Y_MARGIN = 10;
    static final Font lowerLineFont = new Font("Dialog", Font.PLAIN, 10);

    private static void renderSplashFrame(float progress, String loadText) {
        renderSplashFrame(progress, loadText, TITLE + " v" + VERSION, "Interdisciplinary Centre for Mathematical and Computational Modelling, University of Warsaw");
    }

    private static void renderSplashFrame(float progress, String loadText, String bottomTextUpperLine, String bottomTextLowerLine) {
        if (splash == null) {
            return;
        }

        try {
            Graphics2D g = splash.createGraphics();
            if (g == null) {
                return;
            }

            if (!splash.isVisible()) {
                return;
            }

            Rectangle bounds = splash.getBounds();
            Font f = g.getFont();
            FontMetrics fm = g.getFontMetrics(f);
            java.awt.geom.Rectangle2D rect = fm.getStringBounds(loadText, g);
            int texth = (int) Math.ceil(rect.getHeight());
            g.setComposite(AlphaComposite.Clear);
            //g.setColor(Color.RED);
            g.fillRect(PROGRESS_TEXT_X_POSITION,
                    bounds.height - PROGRESS_TEXT_Y_MARGIN - texth - 5,
                    bounds.width - PROGRESS_TEXT_X_POSITION,
                    texth + 10);

            g.setFont(lowerLineFont);
            fm = g.getFontMetrics(g.getFont());
            rect = fm.getStringBounds(bottomTextLowerLine, g);
            int lowerLineTextHeight = (int) Math.ceil(rect.getHeight());
            g.fillRect(BOTTOM_TEXT_X_MARGIN,
                    bounds.height - BOTTOM_TEXT_Y_MARGIN - lowerLineTextHeight - 5,
                    bounds.width - BOTTOM_TEXT_X_MARGIN,
                    lowerLineTextHeight + 10);

            g.setFont(f);
            fm = g.getFontMetrics(g.getFont());
            rect = fm.getStringBounds(bottomTextUpperLine, g);
            texth = (int) Math.ceil(rect.getHeight());
            g.fillRect(BOTTOM_TEXT_X_MARGIN,
                    bounds.height - lowerLineTextHeight - BOTTOM_TEXT_Y_MARGIN - texth - 5,
                    bounds.width - BOTTOM_TEXT_X_MARGIN,
                    lowerLineTextHeight + 10);

            g.setPaintMode();
            //        g.setColor(Color.BLACK);
            g.setColor(new Color(0, 75, 50));
            g.drawString(loadText, PROGRESS_TEXT_X_POSITION, bounds.height - PROGRESS_TEXT_Y_MARGIN);
            g.drawString(bottomTextUpperLine, BOTTOM_TEXT_X_MARGIN, bounds.height - lowerLineTextHeight - BOTTOM_TEXT_Y_MARGIN);
            g.setFont(lowerLineFont);
            g.drawString(bottomTextLowerLine, BOTTOM_TEXT_X_MARGIN, bounds.height - BOTTOM_TEXT_Y_MARGIN);
            g.setFont(f);

            //        g.setColor(Color.BLACK);
            g.setColor(new Color(0, 150, 100));
            g.drawRect(PROGRESS_BAR_X_MARGIN,
                    bounds.height - PROGRESS_BAR_Y_MARGIN,
                    bounds.width - PROGRESS_BAR_X_MARGIN,
                    PROGRESS_BAR_HEIGHT);
            int progressWidth = bounds.width - 2 * PROGRESS_BAR_X_MARGIN;
            int done = (int) (progressWidth * progress);
            g.fillRect(PROGRESS_BAR_X_MARGIN,
                    bounds.height - PROGRESS_BAR_Y_MARGIN,
                    PROGRESS_BAR_X_MARGIN + done,
                    PROGRESS_BAR_HEIGHT);
            if (progress >= 1.0f) {
                g.fillRect(PROGRESS_BAR_X_MARGIN,
                        bounds.height - PROGRESS_BAR_Y_MARGIN,
                        bounds.width - PROGRESS_BAR_X_MARGIN,
                        PROGRESS_BAR_HEIGHT);
            }

            splash.update();
        } catch (IllegalStateException ex) {
        }
    }

    //</editor-fold>
    public static int availableProcessors() {
        if (visnow == null) {
            return Runtime.getRuntime().availableProcessors();
        }
        return VisNow.get().getMainConfig().getNAvailableThreads();
    }

    public static String javaVersionString() {
        return System.getProperty("java.version");
    }

    public static int getJavaVersion() {
        String jv = System.getProperty("java.version");
        jv = jv.substring(0, 5);
        int jVersion0 = 0;
        int jVersion1 = 0;
        int jVersion2 = 0;
        jVersion0 = Integer.parseInt(jv.substring(0, 1));
        jVersion1 = Integer.parseInt(jv.substring(2, 3));
        jVersion2 = Integer.parseInt(jv.substring(4, 5));
        return jVersion1;
    }

    public static String getJava3DVersionString() {
        return (String) javax.media.j3d.VirtualUniverse.getProperties().get("j3d.version");
    }

    public static int getJava3DVersion() {
        Map<String, Object> vuMap = javax.media.j3d.VirtualUniverse.getProperties();
        String j3dVersion = (String) vuMap.get("j3d.version");
        j3dVersion = j3dVersion.substring(0, 5);
        int j3dVersion0 = 0;
        int j3dVersion1 = 0;
        int j3dVersion2 = 0;
        j3dVersion0 = Integer.parseInt(j3dVersion.substring(0, 1));
        j3dVersion1 = Integer.parseInt(j3dVersion.substring(2, 3));
        j3dVersion2 = Integer.parseInt(j3dVersion.substring(4, 5));
        return j3dVersion1;
    }

    public int getPerformance() {
        return performance;
    }

    public void setPerformance(int performance) {
        VisNow.performance = performance;
    }

    public int getDefaultColorMap() {
        return VisNow.get().getMainConfig().getDefaultColorMap();
    }

    public void setDefaultColorMap(int n) {
        VisNow.defaultColorMap = n;
    }

    //<editor-fold defaultstate="collapsed" desc=" COPROCESSORS ">
    /**
     * Method, that checks, if a CUDA device is available for computation.
     * <p>
     * @return The answer.
     */
    public static boolean hasCudaDevice() {
        return getCudaDevice() >= 0;
    }

    /**
     * Method, that returns CUDA device, that can be used for computation.
     * <p>
     * @return The device (device ordinal) available for computation. If there is no such device, -1 is returned.
     */
    public static int getCudaDevice() {
        final String JCUDA_LIBRARY_STRING = "JCuda-0.6.0";
        final int NO_DEVICE = -1;

        if (!isNativeLibraryLoaded(JCUDA_LIBRARY_STRING)) {
            return NO_DEVICE;
        }

        JCudaDriver.setExceptionsEnabled(true);
        try {
            cuInit(0);
        } catch (Exception e) {
            return NO_DEVICE;
        }

        // Obtain the number of devices.
        int deviceCountArray[] = {0};
        cuDeviceGetCount(deviceCountArray);
        int deviceCount = deviceCountArray[0];
        LOGGER.info("Found " + deviceCount + " device(s), chosing the best one...");

        int deviceOrdinal = NO_DEVICE;
        String deviceName = "";

        for (int i = 0; i < deviceCount; i++) {
            CUdevice device = new CUdevice();
            cuDeviceGet(device, i);

            // Obtain the device name
            byte[] deviceNameRaw = new byte[1024];
            cuDeviceGetName(deviceNameRaw, deviceNameRaw.length, device);

            // Obtain the device attribute
            int array[] = {0};
            cuDeviceGetAttribute(array, CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT, device);
            int value = array[0];

            if (value == 0 || i == 0) {
                deviceOrdinal = i;
                deviceName = createString(deviceNameRaw);
            }
        }
        LOGGER.info("Chosen device " + deviceOrdinal + " (" + deviceName + ").");
        return deviceOrdinal;
    }

    /**
     * Creates a String from a zero-terminated byte array.
     * <p>
     * @param bytes The byte array to convert to String.
     * <p>
     * @return The String.
     */
    private static String createString(byte bytes[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            char c = (char) bytes[i];
            if (c == 0) {
                break;
            }
            sb.append(c);
        }
        return sb.toString();
    }
    //</editor-fold>

    /**
     * Mock for VisNow main class to have ability of testing different components that need VN config.
     */
    public static void mockVisNow() {
        visnow = new VisNow();
        visnow.jarPath = findJarPath();
        visnow.initConfig();
    }

    private class VisNowExceptionHandler implements Thread.UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            LOGGER.error("Uncaught exception occurred in thread " + t, e);
            userMessageSend(null, "Uncaught exception occurred in thread " + t, "See log for details", Level.ERROR);
        }

    }

    private class VisNowThreadFactory implements ThreadFactory {

        private final ThreadFactory defaultFactory = Executors.defaultThreadFactory();

        private final Thread.UncaughtExceptionHandler handler;

        VisNowThreadFactory(Thread.UncaughtExceptionHandler handler) {
            this.handler = handler;
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread t = defaultFactory.newThread(r);
            t.setUncaughtExceptionHandler(handler);
            return t;
        }
    };
}
