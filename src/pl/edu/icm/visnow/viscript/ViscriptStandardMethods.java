/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ViscriptStandardMethods
{

    public static float min(float a, float b)
    {
        return Math.min(a, b);
    }

    public static float sqrt(float a)
    {
        return (float) Math.sqrt(a);
    }

    public static float max(float a, float b)
    {
        return Math.max(a, b);
    }

    public static float avg(float[] inField)
    {
        float r = 0;
        for (int i = 0; i < inField.length; i++) {
            r += inField[i];
        }
        return r / inField.length;
    }

    public static float min(float[] inField)
    {
        float r = Float.MAX_VALUE;
        for (int i = 0; i < inField.length; i++) {
            if (inField[i] < r) {
                r = inField[i];
            }
        }
        return r;
    }

    public static float max(float[] inField)
    {
        float r = Float.MIN_VALUE;
        for (int i = 0; i < inField.length; i++) {
            if (inField[i] > r) {
                r = inField[i];
            }
        }
        return r;
    }

    public static float[] sum(float[] a, float[] b)
    {
        float[] r = new float[a.length];
        for (int i = 0; i < a.length; i++) {
            r[i] = a[i] + b[i];
        }
        return r;
    }

    public static float log(float a)
    {
        return a < 0 ? 0 : (float) Math.log(a);
    }

    public static float exp(float a)
    {
        return (float) Math.exp(a);
    }

    public static float size(float[] a)
    {
        return a.length;
    }

    public static void print(float a)
    {
        System.out.println(a);
    }

    private ViscriptStandardMethods()
    {
    }
}
