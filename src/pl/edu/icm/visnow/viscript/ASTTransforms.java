/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

import pl.edu.icm.visnow.viscript.VNScriptTree;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.antlr.runtime.tree.TreeWizard;
import pl.edu.icm.visnow.viscript.types.VNScriptContext;
import pl.edu.icm.visnow.viscript.types.VNScriptType;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ASTTransforms
{

    // === funkcje pomocnicze ===
    private static class Pair
    {

        public final CommonTree commonTree;
        public final int index;

        public Pair(CommonTree commonTree, int index)
        {
            this.commonTree = commonTree;
            this.index = index;
        }
    }

    private static Pair getBlock(CommonTree t)
    {
        if (t.getParent() == null) {
            return null;
        } else if (t.getParent().getType() == VNScriptTree.BLOCK_SCOPE) {
            return new Pair((CommonTree) t.getParent(), t.childIndex);
        } else {
            return getBlock((CommonTree) t.getParent());
        }
    }

    @SuppressWarnings("unchecked")
    private static void insertChild(CommonTree parent, CommonTree child, int offset)
    {
        for (int i = offset; i < parent.getChildCount(); i++) {
            parent.getChild(i).setChildIndex(parent.getChild(i).getChildIndex() + 1);
        }
        child.setChildIndex(offset);
        parent.getChildren().add(child);
        for (int i = offset + 1; i < parent.getChildCount(); i++) {
            parent.setChild(i, parent.getChild(i - 1));
        }
        parent.setChild(offset, child);
    }

    // === END: funkcje pomocnicze ===
    public static void pushUpArrayExpressionsInMethodArguments(CommonTree t, TreeWizard wiz)
    {
        // TODO: zrobic
        // powinno wypychac do najblizszego bloku wyrazenia tablicowe bedace
        // argumentami funkcji
        // fun(tab1 + tab2);  <=>   temp_tab = tab1 + tab2;
        //                          fun(temp_tab);
    }

    public static void pushUpMethodCalls(CommonTree t, TreeWizard wiz, VNScriptContext context)
    {
        // TODO: poprawic dzialanie
        // powinno wypychac tylko te metody, ktore niezaleza od elementow tablic w
        // wyrazeniu (tzn. w argumentach nie moze byc Array/SubArray/ArrayExp)
        switch (t.getType()) {
            case VNScriptParser.METHOD_CALL:
                int index = t.childIndex;

                Pair p = getBlock(t);
                if (p.commonTree != t.getParent().getParent().getParent()) {
                    // TODO: nadac unikalny identyfikator
                    String tempVariable = context.createVariable(VNScriptType.ARRAY);

                    t.getParent().setChild(index, (Tree) wiz.create("IDENT[" + tempVariable + "]"));

                    Tree t2 = (Tree) wiz.create("(EXPR (ASSIGN[=] IDENT[" + tempVariable + "]))");
                    t2.getChild(0).addChild(t);
                    t.setParent(t2.getChild(0));

                    insertChild(p.commonTree, (CommonTree) t2, p.index);
                }
            default:
                if (t.getChildCount() > 0) {
                    CommonTree child = (CommonTree) t.getChild(0);
                    pushUpMethodCalls(child, wiz, context);
                    while (child.getChildIndex() + 1 < t.getChildCount()) {
                        child = (CommonTree) t.getChild(child.getChildIndex() + 1);
                        pushUpMethodCalls(child, wiz, context);
                    }
                }
                break;
        }
    }

    private ASTTransforms()
    {
    }
}
