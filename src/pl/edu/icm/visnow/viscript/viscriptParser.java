/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/staff/lyczek/viscript.g 2010-02-22 14:46:42
import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;

public class viscriptParser extends Parser
{

    public static final String[] tokenNames = new String[]{
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "WHILE_OP", "FOR_OP", "IF_OP", "ELSE_OP", "RANGE", "BEGIN_RANGE", "END_RANGE", "JUMP_RANGE", "TAB_PARAMS", "CALL", "COMPONENT", "TAB", "PROG", "ASSIGN_OP", "LOG_OP", "ADD_OP", "MUL_OP", "ID", "INT", "WS", "';'", "'('", "')'", "'{'", "'}'", "':'", "','", "'.'", "'['", "']'"
    };
    public static final int T__29 = 29;
    public static final int T__28 = 28;
    public static final int T__27 = 27;
    public static final int T__26 = 26;
    public static final int T__25 = 25;
    public static final int T__24 = 24;
    public static final int ELSE_OP = 7;
    public static final int ADD_OP = 19;
    public static final int TAB = 15;
    public static final int RANGE = 8;
    public static final int INT = 22;
    public static final int IF_OP = 6;
    public static final int ID = 21;
    public static final int EOF = -1;
    public static final int ASSIGN_OP = 17;
    public static final int T__30 = 30;
    public static final int T__31 = 31;
    public static final int PROG = 16;
    public static final int T__32 = 32;
    public static final int WS = 23;
    public static final int JUMP_RANGE = 11;
    public static final int T__33 = 33;
    public static final int END_RANGE = 10;
    public static final int TAB_PARAMS = 12;
    public static final int FOR_OP = 5;
    public static final int CALL = 13;
    public static final int LOG_OP = 18;
    public static final int COMPONENT = 14;
    public static final int WHILE_OP = 4;
    public static final int MUL_OP = 20;
    public static final int BEGIN_RANGE = 9;

    // delegates
    // delegators
    public viscriptParser(TokenStream input)
    {
        this(input, new RecognizerSharedState());
    }

    public viscriptParser(TokenStream input, RecognizerSharedState state)
    {
        super(input, state);

    }

    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor)
    {
        this.adaptor = adaptor;
    }

    public TreeAdaptor getTreeAdaptor()
    {
        return adaptor;
    }

    public String[] getTokenNames()
    {
        return viscriptParser.tokenNames;
    }

    public String getGrammarFileName()
    {
        return "/home/staff/lyczek/viscript.g";
    }

    public static class prog_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "prog"
    // /home/staff/lyczek/viscript.g:25:1: prog : ( stm )* -> ^( PROG ( stm )* ) ;
    public final viscriptParser.prog_return prog() throws RecognitionException
    {
        viscriptParser.prog_return retval = new viscriptParser.prog_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        viscriptParser.stm_return stm1 = null;

        RewriteRuleSubtreeStream stream_stm = new RewriteRuleSubtreeStream(adaptor, "rule stm");
        try {
            // /home/staff/lyczek/viscript.g:25:6: ( ( stm )* -> ^( PROG ( stm )* ) )
            // /home/staff/lyczek/viscript.g:25:8: ( stm )*
            {
                // /home/staff/lyczek/viscript.g:25:8: ( stm )*
                loop1:
                do {
                    int alt1 = 2;
                    int LA1_0 = input.LA(1);

                    if (((LA1_0 >= WHILE_OP && LA1_0 <= IF_OP) || (LA1_0 >= ID && LA1_0 <= INT) || (LA1_0 >= 24 && LA1_0 <= 25))) {
                        alt1 = 1;
                    }

                    switch (alt1) {
                        case 1: // /home/staff/lyczek/viscript.g:0:0: stm
                        {
                            pushFollow(FOLLOW_stm_in_prog150);
                            stm1 = stm();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_stm.add(stm1.getTree());

                        }
                        break;

                        default:
                            break loop1;
                    }
                } while (true);

                // AST REWRITE
                // elements: stm
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 25:13: -> ^( PROG ( stm )* )
                    {
                        // /home/staff/lyczek/viscript.g:25:16: ^( PROG ( stm )* )
                        {
                            CommonTree root_1 = (CommonTree) adaptor.nil();
                            root_1 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(PROG, "PROG"), root_1);

                            // /home/staff/lyczek/viscript.g:25:23: ( stm )*
                            while (stream_stm.hasNext()) {
                                adaptor.addChild(root_1, stream_stm.nextTree());

                            }
                            stream_stm.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "prog"
    public static class stm_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "stm"
    // /home/staff/lyczek/viscript.g:27:1: stm : ( aexp ';' | while_op | for_op | if_op | ';' );
    public final viscriptParser.stm_return stm() throws RecognitionException
    {
        viscriptParser.stm_return retval = new viscriptParser.stm_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal3 = null;
        Token char_literal7 = null;
        viscriptParser.aexp_return aexp2 = null;

        viscriptParser.while_op_return while_op4 = null;

        viscriptParser.for_op_return for_op5 = null;

        viscriptParser.if_op_return if_op6 = null;

        CommonTree char_literal3_tree = null;
        CommonTree char_literal7_tree = null;

        try {
            // /home/staff/lyczek/viscript.g:27:5: ( aexp ';' | while_op | for_op | if_op | ';' )
            int alt2 = 5;
            switch (input.LA(1)) {
                case ID:
                case INT:
                case 25: {
                    alt2 = 1;
                }
                break;
                case WHILE_OP: {
                    alt2 = 2;
                }
                break;
                case FOR_OP: {
                    alt2 = 3;
                }
                break;
                case IF_OP: {
                    alt2 = 4;
                }
                break;
                case 24: {
                    alt2 = 5;
                }
                break;
                default:
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 2, 0, input);

                    throw nvae;
            }

            switch (alt2) {
                case 1: // /home/staff/lyczek/viscript.g:27:7: aexp ';'
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_aexp_in_stm169);
                    aexp2 = aexp();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, aexp2.getTree());
                    char_literal3 = (Token) match(input, 24, FOLLOW_24_in_stm171);
                    if (state.failed)
                        return retval;

                }
                break;
                case 2: // /home/staff/lyczek/viscript.g:28:4: while_op
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_while_op_in_stm177);
                    while_op4 = while_op();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, while_op4.getTree());

                }
                break;
                case 3: // /home/staff/lyczek/viscript.g:29:4: for_op
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_for_op_in_stm182);
                    for_op5 = for_op();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, for_op5.getTree());

                }
                break;
                case 4: // /home/staff/lyczek/viscript.g:30:4: if_op
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_if_op_in_stm187);
                    if_op6 = if_op();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, if_op6.getTree());

                }
                break;
                case 5: // /home/staff/lyczek/viscript.g:31:4: ';'
                {
                    root_0 = (CommonTree) adaptor.nil();

                    char_literal7 = (Token) match(input, 24, FOLLOW_24_in_stm192);
                    if (state.failed)
                        return retval;

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "stm"
    public static class while_op_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "while_op"
    // /home/staff/lyczek/viscript.g:33:1: while_op : WHILE_OP '(' cexp ')' '{' prog '}' -> ^( WHILE_OP cexp prog ) ;
    public final viscriptParser.while_op_return while_op() throws RecognitionException
    {
        viscriptParser.while_op_return retval = new viscriptParser.while_op_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token WHILE_OP8 = null;
        Token char_literal9 = null;
        Token char_literal11 = null;
        Token char_literal12 = null;
        Token char_literal14 = null;
        viscriptParser.cexp_return cexp10 = null;

        viscriptParser.prog_return prog13 = null;

        CommonTree WHILE_OP8_tree = null;
        CommonTree char_literal9_tree = null;
        CommonTree char_literal11_tree = null;
        CommonTree char_literal12_tree = null;
        CommonTree char_literal14_tree = null;
        RewriteRuleTokenStream stream_WHILE_OP = new RewriteRuleTokenStream(adaptor, "token WHILE_OP");
        RewriteRuleTokenStream stream_25 = new RewriteRuleTokenStream(adaptor, "token 25");
        RewriteRuleTokenStream stream_26 = new RewriteRuleTokenStream(adaptor, "token 26");
        RewriteRuleTokenStream stream_27 = new RewriteRuleTokenStream(adaptor, "token 27");
        RewriteRuleTokenStream stream_28 = new RewriteRuleTokenStream(adaptor, "token 28");
        RewriteRuleSubtreeStream stream_prog = new RewriteRuleSubtreeStream(adaptor, "rule prog");
        RewriteRuleSubtreeStream stream_cexp = new RewriteRuleSubtreeStream(adaptor, "rule cexp");
        try {
            // /home/staff/lyczek/viscript.g:33:10: ( WHILE_OP '(' cexp ')' '{' prog '}' -> ^( WHILE_OP cexp prog ) )
            // /home/staff/lyczek/viscript.g:33:12: WHILE_OP '(' cexp ')' '{' prog '}'
            {
                WHILE_OP8 = (Token) match(input, WHILE_OP, FOLLOW_WHILE_OP_in_while_op202);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_WHILE_OP.add(WHILE_OP8);

                char_literal9 = (Token) match(input, 25, FOLLOW_25_in_while_op204);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_25.add(char_literal9);

                pushFollow(FOLLOW_cexp_in_while_op206);
                cexp10 = cexp();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_cexp.add(cexp10.getTree());
                char_literal11 = (Token) match(input, 26, FOLLOW_26_in_while_op208);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_26.add(char_literal11);

                char_literal12 = (Token) match(input, 27, FOLLOW_27_in_while_op210);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_27.add(char_literal12);

                pushFollow(FOLLOW_prog_in_while_op212);
                prog13 = prog();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_prog.add(prog13.getTree());
                char_literal14 = (Token) match(input, 28, FOLLOW_28_in_while_op214);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_28.add(char_literal14);

                // AST REWRITE
                // elements: prog, WHILE_OP, cexp
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 33:47: -> ^( WHILE_OP cexp prog )
                    {
                        // /home/staff/lyczek/viscript.g:33:50: ^( WHILE_OP cexp prog )
                        {
                            CommonTree root_1 = (CommonTree) adaptor.nil();
                            root_1 = (CommonTree) adaptor.becomeRoot(stream_WHILE_OP.nextNode(), root_1);

                            adaptor.addChild(root_1, stream_cexp.nextTree());
                            adaptor.addChild(root_1, stream_prog.nextTree());

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "while_op"
    public static class for_op_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "for_op"
    // /home/staff/lyczek/viscript.g:35:1: for_op : FOR_OP '(' (a= aexp )? ';' (b= cexp )? ';' (c= aexp )? ')' '{' prog '}' -> ^( FOR_OP $a $b $c prog ) ;
    public final viscriptParser.for_op_return for_op() throws RecognitionException
    {
        viscriptParser.for_op_return retval = new viscriptParser.for_op_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token FOR_OP15 = null;
        Token char_literal16 = null;
        Token char_literal17 = null;
        Token char_literal18 = null;
        Token char_literal19 = null;
        Token char_literal20 = null;
        Token char_literal22 = null;
        viscriptParser.aexp_return a = null;

        viscriptParser.cexp_return b = null;

        viscriptParser.aexp_return c = null;

        viscriptParser.prog_return prog21 = null;

        CommonTree FOR_OP15_tree = null;
        CommonTree char_literal16_tree = null;
        CommonTree char_literal17_tree = null;
        CommonTree char_literal18_tree = null;
        CommonTree char_literal19_tree = null;
        CommonTree char_literal20_tree = null;
        CommonTree char_literal22_tree = null;
        RewriteRuleTokenStream stream_FOR_OP = new RewriteRuleTokenStream(adaptor, "token FOR_OP");
        RewriteRuleTokenStream stream_24 = new RewriteRuleTokenStream(adaptor, "token 24");
        RewriteRuleTokenStream stream_25 = new RewriteRuleTokenStream(adaptor, "token 25");
        RewriteRuleTokenStream stream_26 = new RewriteRuleTokenStream(adaptor, "token 26");
        RewriteRuleTokenStream stream_27 = new RewriteRuleTokenStream(adaptor, "token 27");
        RewriteRuleTokenStream stream_28 = new RewriteRuleTokenStream(adaptor, "token 28");
        RewriteRuleSubtreeStream stream_prog = new RewriteRuleSubtreeStream(adaptor, "rule prog");
        RewriteRuleSubtreeStream stream_cexp = new RewriteRuleSubtreeStream(adaptor, "rule cexp");
        RewriteRuleSubtreeStream stream_aexp = new RewriteRuleSubtreeStream(adaptor, "rule aexp");
        try {
            // /home/staff/lyczek/viscript.g:35:8: ( FOR_OP '(' (a= aexp )? ';' (b= cexp )? ';' (c= aexp )? ')' '{' prog '}' -> ^( FOR_OP $a $b $c prog ) )
            // /home/staff/lyczek/viscript.g:35:10: FOR_OP '(' (a= aexp )? ';' (b= cexp )? ';' (c= aexp )? ')' '{' prog '}'
            {
                FOR_OP15 = (Token) match(input, FOR_OP, FOLLOW_FOR_OP_in_for_op233);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_FOR_OP.add(FOR_OP15);

                char_literal16 = (Token) match(input, 25, FOLLOW_25_in_for_op235);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_25.add(char_literal16);

                // /home/staff/lyczek/viscript.g:35:22: (a= aexp )?
                int alt3 = 2;
                int LA3_0 = input.LA(1);

                if (((LA3_0 >= ID && LA3_0 <= INT) || LA3_0 == 25)) {
                    alt3 = 1;
                }
                switch (alt3) {
                    case 1: // /home/staff/lyczek/viscript.g:0:0: a= aexp
                    {
                        pushFollow(FOLLOW_aexp_in_for_op239);
                        a = aexp();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_aexp.add(a.getTree());

                    }
                    break;

                }

                char_literal17 = (Token) match(input, 24, FOLLOW_24_in_for_op242);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_24.add(char_literal17);

                // /home/staff/lyczek/viscript.g:35:34: (b= cexp )?
                int alt4 = 2;
                int LA4_0 = input.LA(1);

                if (((LA4_0 >= ID && LA4_0 <= INT) || LA4_0 == 25)) {
                    alt4 = 1;
                }
                switch (alt4) {
                    case 1: // /home/staff/lyczek/viscript.g:0:0: b= cexp
                    {
                        pushFollow(FOLLOW_cexp_in_for_op246);
                        b = cexp();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_cexp.add(b.getTree());

                    }
                    break;

                }

                char_literal18 = (Token) match(input, 24, FOLLOW_24_in_for_op249);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_24.add(char_literal18);

                // /home/staff/lyczek/viscript.g:35:46: (c= aexp )?
                int alt5 = 2;
                int LA5_0 = input.LA(1);

                if (((LA5_0 >= ID && LA5_0 <= INT) || LA5_0 == 25)) {
                    alt5 = 1;
                }
                switch (alt5) {
                    case 1: // /home/staff/lyczek/viscript.g:0:0: c= aexp
                    {
                        pushFollow(FOLLOW_aexp_in_for_op253);
                        c = aexp();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_aexp.add(c.getTree());

                    }
                    break;

                }

                char_literal19 = (Token) match(input, 26, FOLLOW_26_in_for_op256);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_26.add(char_literal19);

                char_literal20 = (Token) match(input, 27, FOLLOW_27_in_for_op258);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_27.add(char_literal20);

                pushFollow(FOLLOW_prog_in_for_op260);
                prog21 = prog();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_prog.add(prog21.getTree());
                char_literal22 = (Token) match(input, 28, FOLLOW_28_in_for_op262);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_28.add(char_literal22);

                // AST REWRITE
                // elements: prog, b, FOR_OP, c, a
                // token labels: 
                // rule labels: retval, b, c, a
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);
                    RewriteRuleSubtreeStream stream_b = new RewriteRuleSubtreeStream(adaptor, "rule b", b != null ? b.tree : null);
                    RewriteRuleSubtreeStream stream_c = new RewriteRuleSubtreeStream(adaptor, "rule c", c != null ? c.tree : null);
                    RewriteRuleSubtreeStream stream_a = new RewriteRuleSubtreeStream(adaptor, "rule a", a != null ? a.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 35:70: -> ^( FOR_OP $a $b $c prog )
                    {
                        // /home/staff/lyczek/viscript.g:35:73: ^( FOR_OP $a $b $c prog )
                        {
                            CommonTree root_1 = (CommonTree) adaptor.nil();
                            root_1 = (CommonTree) adaptor.becomeRoot(stream_FOR_OP.nextNode(), root_1);

                            adaptor.addChild(root_1, stream_a.nextTree());
                            adaptor.addChild(root_1, stream_b.nextTree());
                            adaptor.addChild(root_1, stream_c.nextTree());
                            adaptor.addChild(root_1, stream_prog.nextTree());

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "for_op"
    public static class if_op_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "if_op"
    // /home/staff/lyczek/viscript.g:37:1: if_op : IF_OP '(' cexp ')' '{' a= prog '}' ( ELSE_OP '{' b= prog '}' )? -> ^( IF_OP cexp ^( $a) ( ^( $b) )? ) ;
    public final viscriptParser.if_op_return if_op() throws RecognitionException
    {
        viscriptParser.if_op_return retval = new viscriptParser.if_op_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token IF_OP23 = null;
        Token char_literal24 = null;
        Token char_literal26 = null;
        Token char_literal27 = null;
        Token char_literal28 = null;
        Token ELSE_OP29 = null;
        Token char_literal30 = null;
        Token char_literal31 = null;
        viscriptParser.prog_return a = null;

        viscriptParser.prog_return b = null;

        viscriptParser.cexp_return cexp25 = null;

        CommonTree IF_OP23_tree = null;
        CommonTree char_literal24_tree = null;
        CommonTree char_literal26_tree = null;
        CommonTree char_literal27_tree = null;
        CommonTree char_literal28_tree = null;
        CommonTree ELSE_OP29_tree = null;
        CommonTree char_literal30_tree = null;
        CommonTree char_literal31_tree = null;
        RewriteRuleTokenStream stream_IF_OP = new RewriteRuleTokenStream(adaptor, "token IF_OP");
        RewriteRuleTokenStream stream_ELSE_OP = new RewriteRuleTokenStream(adaptor, "token ELSE_OP");
        RewriteRuleTokenStream stream_25 = new RewriteRuleTokenStream(adaptor, "token 25");
        RewriteRuleTokenStream stream_26 = new RewriteRuleTokenStream(adaptor, "token 26");
        RewriteRuleTokenStream stream_27 = new RewriteRuleTokenStream(adaptor, "token 27");
        RewriteRuleTokenStream stream_28 = new RewriteRuleTokenStream(adaptor, "token 28");
        RewriteRuleSubtreeStream stream_prog = new RewriteRuleSubtreeStream(adaptor, "rule prog");
        RewriteRuleSubtreeStream stream_cexp = new RewriteRuleSubtreeStream(adaptor, "rule cexp");
        try {
            // /home/staff/lyczek/viscript.g:37:7: ( IF_OP '(' cexp ')' '{' a= prog '}' ( ELSE_OP '{' b= prog '}' )? -> ^( IF_OP cexp ^( $a) ( ^( $b) )? ) )
            // /home/staff/lyczek/viscript.g:37:9: IF_OP '(' cexp ')' '{' a= prog '}' ( ELSE_OP '{' b= prog '}' )?
            {
                IF_OP23 = (Token) match(input, IF_OP, FOLLOW_IF_OP_in_if_op288);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_IF_OP.add(IF_OP23);

                char_literal24 = (Token) match(input, 25, FOLLOW_25_in_if_op290);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_25.add(char_literal24);

                pushFollow(FOLLOW_cexp_in_if_op292);
                cexp25 = cexp();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_cexp.add(cexp25.getTree());
                char_literal26 = (Token) match(input, 26, FOLLOW_26_in_if_op294);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_26.add(char_literal26);

                char_literal27 = (Token) match(input, 27, FOLLOW_27_in_if_op296);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_27.add(char_literal27);

                pushFollow(FOLLOW_prog_in_if_op299);
                a = prog();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_prog.add(a.getTree());
                char_literal28 = (Token) match(input, 28, FOLLOW_28_in_if_op301);
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_28.add(char_literal28);

                // /home/staff/lyczek/viscript.g:37:42: ( ELSE_OP '{' b= prog '}' )?
                int alt6 = 2;
                int LA6_0 = input.LA(1);

                if ((LA6_0 == ELSE_OP)) {
                    alt6 = 1;
                }
                switch (alt6) {
                    case 1: // /home/staff/lyczek/viscript.g:37:43: ELSE_OP '{' b= prog '}'
                    {
                        ELSE_OP29 = (Token) match(input, ELSE_OP, FOLLOW_ELSE_OP_in_if_op304);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_ELSE_OP.add(ELSE_OP29);

                        char_literal30 = (Token) match(input, 27, FOLLOW_27_in_if_op306);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_27.add(char_literal30);

                        pushFollow(FOLLOW_prog_in_if_op310);
                        b = prog();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_prog.add(b.getTree());
                        char_literal31 = (Token) match(input, 28, FOLLOW_28_in_if_op312);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_28.add(char_literal31);

                    }
                    break;

                }

                // AST REWRITE
                // elements: a, b, IF_OP, cexp
                // token labels: 
                // rule labels: retval, b, a
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);
                    RewriteRuleSubtreeStream stream_b = new RewriteRuleSubtreeStream(adaptor, "rule b", b != null ? b.tree : null);
                    RewriteRuleSubtreeStream stream_a = new RewriteRuleSubtreeStream(adaptor, "rule a", a != null ? a.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 37:68: -> ^( IF_OP cexp ^( $a) ( ^( $b) )? )
                    {
                        // /home/staff/lyczek/viscript.g:37:71: ^( IF_OP cexp ^( $a) ( ^( $b) )? )
                        {
                            CommonTree root_1 = (CommonTree) adaptor.nil();
                            root_1 = (CommonTree) adaptor.becomeRoot(stream_IF_OP.nextNode(), root_1);

                            adaptor.addChild(root_1, stream_cexp.nextTree());
                            // /home/staff/lyczek/viscript.g:37:84: ^( $a)
                            {
                                CommonTree root_2 = (CommonTree) adaptor.nil();
                                root_2 = (CommonTree) adaptor.becomeRoot(stream_a.nextNode(), root_2);

                                adaptor.addChild(root_1, root_2);
                            }
                            // /home/staff/lyczek/viscript.g:37:90: ( ^( $b) )?
                            if (stream_b.hasNext()) {
                                // /home/staff/lyczek/viscript.g:37:90: ^( $b)
                                {
                                    CommonTree root_2 = (CommonTree) adaptor.nil();
                                    root_2 = (CommonTree) adaptor.becomeRoot(stream_b.nextNode(), root_2);

                                    adaptor.addChild(root_1, root_2);
                                }

                            }
                            stream_b.reset();

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "if_op"
    public static class aexp_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "aexp"
    // /home/staff/lyczek/viscript.g:39:1: aexp : ( lvalue ASSIGN_OP aexp -> ^( ASSIGN_OP lvalue aexp ) | cexp );
    public final viscriptParser.aexp_return aexp() throws RecognitionException
    {
        viscriptParser.aexp_return retval = new viscriptParser.aexp_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ASSIGN_OP33 = null;
        viscriptParser.lvalue_return lvalue32 = null;

        viscriptParser.aexp_return aexp34 = null;

        viscriptParser.cexp_return cexp35 = null;

        CommonTree ASSIGN_OP33_tree = null;
        RewriteRuleTokenStream stream_ASSIGN_OP = new RewriteRuleTokenStream(adaptor, "token ASSIGN_OP");
        RewriteRuleSubtreeStream stream_lvalue = new RewriteRuleSubtreeStream(adaptor, "rule lvalue");
        RewriteRuleSubtreeStream stream_aexp = new RewriteRuleSubtreeStream(adaptor, "rule aexp");
        try {
            // /home/staff/lyczek/viscript.g:39:6: ( lvalue ASSIGN_OP aexp -> ^( ASSIGN_OP lvalue aexp ) | cexp )
            int alt7 = 2;
            int LA7_0 = input.LA(1);

            if ((LA7_0 == ID)) {
                int LA7_1 = input.LA(2);

                if ((synpred10_viscript())) {
                    alt7 = 1;
                } else if ((true)) {
                    alt7 = 2;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            } else if ((LA7_0 == INT || LA7_0 == 25)) {
                alt7 = 2;
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1: // /home/staff/lyczek/viscript.g:39:9: lvalue ASSIGN_OP aexp
                {
                    pushFollow(FOLLOW_lvalue_in_aexp343);
                    lvalue32 = lvalue();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_lvalue.add(lvalue32.getTree());
                    ASSIGN_OP33 = (Token) match(input, ASSIGN_OP, FOLLOW_ASSIGN_OP_in_aexp345);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_ASSIGN_OP.add(ASSIGN_OP33);

                    pushFollow(FOLLOW_aexp_in_aexp348);
                    aexp34 = aexp();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_aexp.add(aexp34.getTree());

                    // AST REWRITE
                    // elements: lvalue, aexp, ASSIGN_OP
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (CommonTree) adaptor.nil();
                        // 39:32: -> ^( ASSIGN_OP lvalue aexp )
                        {
                            // /home/staff/lyczek/viscript.g:39:35: ^( ASSIGN_OP lvalue aexp )
                            {
                                CommonTree root_1 = (CommonTree) adaptor.nil();
                                root_1 = (CommonTree) adaptor.becomeRoot(stream_ASSIGN_OP.nextNode(), root_1);

                                adaptor.addChild(root_1, stream_lvalue.nextTree());
                                adaptor.addChild(root_1, stream_aexp.nextTree());

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 2: // /home/staff/lyczek/viscript.g:40:5: cexp
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_cexp_in_aexp364);
                    cexp35 = cexp();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, cexp35.getTree());

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "aexp"
    public static class cexp_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "cexp"
    // /home/staff/lyczek/viscript.g:42:1: cexp : exp2 ( LOG_OP exp2 )* ;
    public final viscriptParser.cexp_return cexp() throws RecognitionException
    {
        viscriptParser.cexp_return retval = new viscriptParser.cexp_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token LOG_OP37 = null;
        viscriptParser.exp2_return exp236 = null;

        viscriptParser.exp2_return exp238 = null;

        CommonTree LOG_OP37_tree = null;

        try {
            // /home/staff/lyczek/viscript.g:42:6: ( exp2 ( LOG_OP exp2 )* )
            // /home/staff/lyczek/viscript.g:42:8: exp2 ( LOG_OP exp2 )*
            {
                root_0 = (CommonTree) adaptor.nil();

                pushFollow(FOLLOW_exp2_in_cexp373);
                exp236 = exp2();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, exp236.getTree());
                // /home/staff/lyczek/viscript.g:42:13: ( LOG_OP exp2 )*
                loop8:
                do {
                    int alt8 = 2;
                    int LA8_0 = input.LA(1);

                    if ((LA8_0 == LOG_OP)) {
                        alt8 = 1;
                    }

                    switch (alt8) {
                        case 1: // /home/staff/lyczek/viscript.g:42:14: LOG_OP exp2
                        {
                            LOG_OP37 = (Token) match(input, LOG_OP, FOLLOW_LOG_OP_in_cexp376);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                LOG_OP37_tree = (CommonTree) adaptor.create(LOG_OP37);
                                root_0 = (CommonTree) adaptor.becomeRoot(LOG_OP37_tree, root_0);
                            }
                            pushFollow(FOLLOW_exp2_in_cexp379);
                            exp238 = exp2();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, exp238.getTree());

                        }
                        break;

                        default:
                            break loop8;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "cexp"
    public static class exp2_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "exp2"
    // /home/staff/lyczek/viscript.g:44:1: exp2 : mulExp ( ADD_OP mulExp )* ;
    public final viscriptParser.exp2_return exp2() throws RecognitionException
    {
        viscriptParser.exp2_return retval = new viscriptParser.exp2_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ADD_OP40 = null;
        viscriptParser.mulExp_return mulExp39 = null;

        viscriptParser.mulExp_return mulExp41 = null;

        CommonTree ADD_OP40_tree = null;

        try {
            // /home/staff/lyczek/viscript.g:44:6: ( mulExp ( ADD_OP mulExp )* )
            // /home/staff/lyczek/viscript.g:44:8: mulExp ( ADD_OP mulExp )*
            {
                root_0 = (CommonTree) adaptor.nil();

                pushFollow(FOLLOW_mulExp_in_exp2390);
                mulExp39 = mulExp();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, mulExp39.getTree());
                // /home/staff/lyczek/viscript.g:44:15: ( ADD_OP mulExp )*
                loop9:
                do {
                    int alt9 = 2;
                    int LA9_0 = input.LA(1);

                    if ((LA9_0 == ADD_OP)) {
                        alt9 = 1;
                    }

                    switch (alt9) {
                        case 1: // /home/staff/lyczek/viscript.g:44:16: ADD_OP mulExp
                        {
                            ADD_OP40 = (Token) match(input, ADD_OP, FOLLOW_ADD_OP_in_exp2393);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                ADD_OP40_tree = (CommonTree) adaptor.create(ADD_OP40);
                                root_0 = (CommonTree) adaptor.becomeRoot(ADD_OP40_tree, root_0);
                            }
                            pushFollow(FOLLOW_mulExp_in_exp2396);
                            mulExp41 = mulExp();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, mulExp41.getTree());

                        }
                        break;

                        default:
                            break loop9;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "exp2"
    public static class mulExp_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "mulExp"
    // /home/staff/lyczek/viscript.g:46:1: mulExp : atom ( MUL_OP atom )* ;
    public final viscriptParser.mulExp_return mulExp() throws RecognitionException
    {
        viscriptParser.mulExp_return retval = new viscriptParser.mulExp_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token MUL_OP43 = null;
        viscriptParser.atom_return atom42 = null;

        viscriptParser.atom_return atom44 = null;

        CommonTree MUL_OP43_tree = null;

        try {
            // /home/staff/lyczek/viscript.g:46:8: ( atom ( MUL_OP atom )* )
            // /home/staff/lyczek/viscript.g:46:10: atom ( MUL_OP atom )*
            {
                root_0 = (CommonTree) adaptor.nil();

                pushFollow(FOLLOW_atom_in_mulExp407);
                atom42 = atom();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    adaptor.addChild(root_0, atom42.getTree());
                // /home/staff/lyczek/viscript.g:46:15: ( MUL_OP atom )*
                loop10:
                do {
                    int alt10 = 2;
                    int LA10_0 = input.LA(1);

                    if ((LA10_0 == MUL_OP)) {
                        alt10 = 1;
                    }

                    switch (alt10) {
                        case 1: // /home/staff/lyczek/viscript.g:46:16: MUL_OP atom
                        {
                            MUL_OP43 = (Token) match(input, MUL_OP, FOLLOW_MUL_OP_in_mulExp410);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0) {
                                MUL_OP43_tree = (CommonTree) adaptor.create(MUL_OP43);
                                root_0 = (CommonTree) adaptor.becomeRoot(MUL_OP43_tree, root_0);
                            }
                            pushFollow(FOLLOW_atom_in_mulExp413);
                            atom44 = atom();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                adaptor.addChild(root_0, atom44.getTree());

                        }
                        break;

                        default:
                            break loop10;
                    }
                } while (true);

            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "mulExp"
    public static class range_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "range"
    // /home/staff/lyczek/viscript.g:48:1: range : (a= cexp )? ( ':' (b= cexp )? ( ':' c= cexp )? )? -> ^( RANGE ^( BEGIN_RANGE ( $a)? ) ^( END_RANGE ( $b)? ) ^( JUMP_RANGE ( $c)? ) ) ;
    public final viscriptParser.range_return range() throws RecognitionException
    {
        viscriptParser.range_return retval = new viscriptParser.range_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal45 = null;
        Token char_literal46 = null;
        viscriptParser.cexp_return a = null;

        viscriptParser.cexp_return b = null;

        viscriptParser.cexp_return c = null;

        CommonTree char_literal45_tree = null;
        CommonTree char_literal46_tree = null;
        RewriteRuleTokenStream stream_29 = new RewriteRuleTokenStream(adaptor, "token 29");
        RewriteRuleSubtreeStream stream_cexp = new RewriteRuleSubtreeStream(adaptor, "rule cexp");
        try {
            // /home/staff/lyczek/viscript.g:48:7: ( (a= cexp )? ( ':' (b= cexp )? ( ':' c= cexp )? )? -> ^( RANGE ^( BEGIN_RANGE ( $a)? ) ^( END_RANGE ( $b)? ) ^( JUMP_RANGE ( $c)? ) ) )
            // /home/staff/lyczek/viscript.g:48:9: (a= cexp )? ( ':' (b= cexp )? ( ':' c= cexp )? )?
            {
                // /home/staff/lyczek/viscript.g:48:9: (a= cexp )?
                int alt11 = 2;
                int LA11_0 = input.LA(1);

                if (((LA11_0 >= ID && LA11_0 <= INT) || LA11_0 == 25)) {
                    alt11 = 1;
                }
                switch (alt11) {
                    case 1: // /home/staff/lyczek/viscript.g:48:10: a= cexp
                    {
                        pushFollow(FOLLOW_cexp_in_range428);
                        a = cexp();

                        state._fsp--;
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_cexp.add(a.getTree());

                    }
                    break;

                }

                // /home/staff/lyczek/viscript.g:48:19: ( ':' (b= cexp )? ( ':' c= cexp )? )?
                int alt14 = 2;
                int LA14_0 = input.LA(1);

                if ((LA14_0 == 29)) {
                    alt14 = 1;
                }
                switch (alt14) {
                    case 1: // /home/staff/lyczek/viscript.g:48:20: ':' (b= cexp )? ( ':' c= cexp )?
                    {
                        char_literal45 = (Token) match(input, 29, FOLLOW_29_in_range433);
                        if (state.failed)
                            return retval;
                        if (state.backtracking == 0)
                            stream_29.add(char_literal45);

                        // /home/staff/lyczek/viscript.g:48:24: (b= cexp )?
                        int alt12 = 2;
                        int LA12_0 = input.LA(1);

                        if (((LA12_0 >= ID && LA12_0 <= INT) || LA12_0 == 25)) {
                            alt12 = 1;
                        }
                        switch (alt12) {
                            case 1: // /home/staff/lyczek/viscript.g:48:25: b= cexp
                            {
                                pushFollow(FOLLOW_cexp_in_range438);
                                b = cexp();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_cexp.add(b.getTree());

                            }
                            break;

                        }

                        // /home/staff/lyczek/viscript.g:48:34: ( ':' c= cexp )?
                        int alt13 = 2;
                        int LA13_0 = input.LA(1);

                        if ((LA13_0 == 29)) {
                            alt13 = 1;
                        }
                        switch (alt13) {
                            case 1: // /home/staff/lyczek/viscript.g:48:35: ':' c= cexp
                            {
                                char_literal46 = (Token) match(input, 29, FOLLOW_29_in_range443);
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_29.add(char_literal46);

                                pushFollow(FOLLOW_cexp_in_range447);
                                c = cexp();

                                state._fsp--;
                                if (state.failed)
                                    return retval;
                                if (state.backtracking == 0)
                                    stream_cexp.add(c.getTree());

                            }
                            break;

                        }

                    }
                    break;

                }

                // AST REWRITE
                // elements: b, a, c
                // token labels: 
                // rule labels: retval, b, c, a
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);
                    RewriteRuleSubtreeStream stream_b = new RewriteRuleSubtreeStream(adaptor, "rule b", b != null ? b.tree : null);
                    RewriteRuleSubtreeStream stream_c = new RewriteRuleSubtreeStream(adaptor, "rule c", c != null ? c.tree : null);
                    RewriteRuleSubtreeStream stream_a = new RewriteRuleSubtreeStream(adaptor, "rule a", a != null ? a.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 48:50: -> ^( RANGE ^( BEGIN_RANGE ( $a)? ) ^( END_RANGE ( $b)? ) ^( JUMP_RANGE ( $c)? ) )
                    {
                        // /home/staff/lyczek/viscript.g:48:53: ^( RANGE ^( BEGIN_RANGE ( $a)? ) ^( END_RANGE ( $b)? ) ^( JUMP_RANGE ( $c)? ) )
                        {
                            CommonTree root_1 = (CommonTree) adaptor.nil();
                            root_1 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(RANGE, "RANGE"), root_1);

                            // /home/staff/lyczek/viscript.g:48:61: ^( BEGIN_RANGE ( $a)? )
                            {
                                CommonTree root_2 = (CommonTree) adaptor.nil();
                                root_2 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(BEGIN_RANGE, "BEGIN_RANGE"), root_2);

                                // /home/staff/lyczek/viscript.g:48:75: ( $a)?
                                if (stream_a.hasNext()) {
                                    adaptor.addChild(root_2, stream_a.nextTree());

                                }
                                stream_a.reset();

                                adaptor.addChild(root_1, root_2);
                            }
                            // /home/staff/lyczek/viscript.g:48:80: ^( END_RANGE ( $b)? )
                            {
                                CommonTree root_2 = (CommonTree) adaptor.nil();
                                root_2 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(END_RANGE, "END_RANGE"), root_2);

                                // /home/staff/lyczek/viscript.g:48:92: ( $b)?
                                if (stream_b.hasNext()) {
                                    adaptor.addChild(root_2, stream_b.nextTree());

                                }
                                stream_b.reset();

                                adaptor.addChild(root_1, root_2);
                            }
                            // /home/staff/lyczek/viscript.g:48:97: ^( JUMP_RANGE ( $c)? )
                            {
                                CommonTree root_2 = (CommonTree) adaptor.nil();
                                root_2 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(JUMP_RANGE, "JUMP_RANGE"), root_2);

                                // /home/staff/lyczek/viscript.g:48:110: ( $c)?
                                if (stream_c.hasNext()) {
                                    adaptor.addChild(root_2, stream_c.nextTree());

                                }
                                stream_c.reset();

                                adaptor.addChild(root_1, root_2);
                            }

                            adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "range"
    public static class params_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "params"
    // /home/staff/lyczek/viscript.g:50:1: params : cexp ( ',' cexp )* -> ( cexp )+ ;
    public final viscriptParser.params_return params() throws RecognitionException
    {
        viscriptParser.params_return retval = new viscriptParser.params_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal48 = null;
        viscriptParser.cexp_return cexp47 = null;

        viscriptParser.cexp_return cexp49 = null;

        CommonTree char_literal48_tree = null;
        RewriteRuleTokenStream stream_30 = new RewriteRuleTokenStream(adaptor, "token 30");
        RewriteRuleSubtreeStream stream_cexp = new RewriteRuleSubtreeStream(adaptor, "rule cexp");
        try {
            // /home/staff/lyczek/viscript.g:50:8: ( cexp ( ',' cexp )* -> ( cexp )+ )
            // /home/staff/lyczek/viscript.g:50:10: cexp ( ',' cexp )*
            {
                pushFollow(FOLLOW_cexp_in_params490);
                cexp47 = cexp();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_cexp.add(cexp47.getTree());
                // /home/staff/lyczek/viscript.g:50:15: ( ',' cexp )*
                loop15:
                do {
                    int alt15 = 2;
                    int LA15_0 = input.LA(1);

                    if ((LA15_0 == 30)) {
                        alt15 = 1;
                    }

                    switch (alt15) {
                        case 1: // /home/staff/lyczek/viscript.g:50:16: ',' cexp
                        {
                            char_literal48 = (Token) match(input, 30, FOLLOW_30_in_params493);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_30.add(char_literal48);

                            pushFollow(FOLLOW_cexp_in_params495);
                            cexp49 = cexp();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_cexp.add(cexp49.getTree());

                        }
                        break;

                        default:
                            break loop15;
                    }
                } while (true);

                // AST REWRITE
                // elements: cexp
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 50:27: -> ( cexp )+
                    {
                        if (!(stream_cexp.hasNext())) {
                            throw new RewriteEarlyExitException();
                        }
                        while (stream_cexp.hasNext()) {
                            adaptor.addChild(root_0, stream_cexp.nextTree());

                        }
                        stream_cexp.reset();

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "params"
    public static class tab_params_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "tab_params"
    // /home/staff/lyczek/viscript.g:52:1: tab_params : range ( ',' range )* -> ( range )+ ;
    public final viscriptParser.tab_params_return tab_params() throws RecognitionException
    {
        viscriptParser.tab_params_return retval = new viscriptParser.tab_params_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal51 = null;
        viscriptParser.range_return range50 = null;

        viscriptParser.range_return range52 = null;

        CommonTree char_literal51_tree = null;
        RewriteRuleTokenStream stream_30 = new RewriteRuleTokenStream(adaptor, "token 30");
        RewriteRuleSubtreeStream stream_range = new RewriteRuleSubtreeStream(adaptor, "rule range");
        try {
            // /home/staff/lyczek/viscript.g:52:12: ( range ( ',' range )* -> ( range )+ )
            // /home/staff/lyczek/viscript.g:52:14: range ( ',' range )*
            {
                pushFollow(FOLLOW_range_in_tab_params511);
                range50 = range();

                state._fsp--;
                if (state.failed)
                    return retval;
                if (state.backtracking == 0)
                    stream_range.add(range50.getTree());
                // /home/staff/lyczek/viscript.g:52:20: ( ',' range )*
                loop16:
                do {
                    int alt16 = 2;
                    int LA16_0 = input.LA(1);

                    if ((LA16_0 == 30)) {
                        alt16 = 1;
                    }

                    switch (alt16) {
                        case 1: // /home/staff/lyczek/viscript.g:52:21: ',' range
                        {
                            char_literal51 = (Token) match(input, 30, FOLLOW_30_in_tab_params514);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_30.add(char_literal51);

                            pushFollow(FOLLOW_range_in_tab_params516);
                            range52 = range();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_range.add(range52.getTree());

                        }
                        break;

                        default:
                            break loop16;
                    }
                } while (true);

                // AST REWRITE
                // elements: range
                // token labels: 
                // rule labels: retval
                // token list labels: 
                // rule list labels: 
                // wildcard labels: 
                if (state.backtracking == 0) {
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                    root_0 = (CommonTree) adaptor.nil();
                    // 52:33: -> ( range )+
                    {
                        if (!(stream_range.hasNext())) {
                            throw new RewriteEarlyExitException();
                        }
                        while (stream_range.hasNext()) {
                            adaptor.addChild(root_0, stream_range.nextTree());

                        }
                        stream_range.reset();

                    }

                    retval.tree = root_0;
                }
            }

            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "tab_params"
    public static class lvalue_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "lvalue"
    // /home/staff/lyczek/viscript.g:54:1: lvalue : ( ID ( '.' lvalue )? -> ^( ID ^( COMPONENT ( lvalue )? ) ) | ID '[' tab_params ']' ( '.' lvalue )? -> ^( ID ^( TAB_PARAMS tab_params ) ^( COMPONENT ( lvalue )? ) ) );
    public final viscriptParser.lvalue_return lvalue() throws RecognitionException
    {
        viscriptParser.lvalue_return retval = new viscriptParser.lvalue_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ID53 = null;
        Token char_literal54 = null;
        Token ID56 = null;
        Token char_literal57 = null;
        Token char_literal59 = null;
        Token char_literal60 = null;
        viscriptParser.lvalue_return lvalue55 = null;

        viscriptParser.tab_params_return tab_params58 = null;

        viscriptParser.lvalue_return lvalue61 = null;

        CommonTree ID53_tree = null;
        CommonTree char_literal54_tree = null;
        CommonTree ID56_tree = null;
        CommonTree char_literal57_tree = null;
        CommonTree char_literal59_tree = null;
        CommonTree char_literal60_tree = null;
        RewriteRuleTokenStream stream_32 = new RewriteRuleTokenStream(adaptor, "token 32");
        RewriteRuleTokenStream stream_31 = new RewriteRuleTokenStream(adaptor, "token 31");
        RewriteRuleTokenStream stream_ID = new RewriteRuleTokenStream(adaptor, "token ID");
        RewriteRuleTokenStream stream_33 = new RewriteRuleTokenStream(adaptor, "token 33");
        RewriteRuleSubtreeStream stream_lvalue = new RewriteRuleSubtreeStream(adaptor, "rule lvalue");
        RewriteRuleSubtreeStream stream_tab_params = new RewriteRuleSubtreeStream(adaptor, "rule tab_params");
        try {
            // /home/staff/lyczek/viscript.g:54:8: ( ID ( '.' lvalue )? -> ^( ID ^( COMPONENT ( lvalue )? ) ) | ID '[' tab_params ']' ( '.' lvalue )? -> ^( ID ^( TAB_PARAMS tab_params ) ^( COMPONENT ( lvalue )? ) ) )
            int alt19 = 2;
            int LA19_0 = input.LA(1);

            if ((LA19_0 == ID)) {
                int LA19_1 = input.LA(2);

                if ((LA19_1 == 32)) {
                    alt19 = 2;
                } else if ((LA19_1 == EOF || (LA19_1 >= ASSIGN_OP && LA19_1 <= MUL_OP) || LA19_1 == 24 || LA19_1 == 26 || (LA19_1 >= 29 && LA19_1 <= 31) || LA19_1 == 33)) {
                    alt19 = 1;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 19, 1, input);

                    throw nvae;
                }
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1: // /home/staff/lyczek/viscript.g:54:10: ID ( '.' lvalue )?
                {
                    ID53 = (Token) match(input, ID, FOLLOW_ID_in_lvalue532);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_ID.add(ID53);

                    // /home/staff/lyczek/viscript.g:54:13: ( '.' lvalue )?
                    int alt17 = 2;
                    int LA17_0 = input.LA(1);

                    if ((LA17_0 == 31)) {
                        alt17 = 1;
                    }
                    switch (alt17) {
                        case 1: // /home/staff/lyczek/viscript.g:54:14: '.' lvalue
                        {
                            char_literal54 = (Token) match(input, 31, FOLLOW_31_in_lvalue535);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_31.add(char_literal54);

                            pushFollow(FOLLOW_lvalue_in_lvalue537);
                            lvalue55 = lvalue();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_lvalue.add(lvalue55.getTree());

                        }
                        break;

                    }

                    // AST REWRITE
                    // elements: ID, lvalue
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (CommonTree) adaptor.nil();
                        // 54:27: -> ^( ID ^( COMPONENT ( lvalue )? ) )
                        {
                            // /home/staff/lyczek/viscript.g:54:30: ^( ID ^( COMPONENT ( lvalue )? ) )
                            {
                                CommonTree root_1 = (CommonTree) adaptor.nil();
                                root_1 = (CommonTree) adaptor.becomeRoot(stream_ID.nextNode(), root_1);

                                // /home/staff/lyczek/viscript.g:54:35: ^( COMPONENT ( lvalue )? )
                                {
                                    CommonTree root_2 = (CommonTree) adaptor.nil();
                                    root_2 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(COMPONENT, "COMPONENT"), root_2);

                                    // /home/staff/lyczek/viscript.g:54:47: ( lvalue )?
                                    if (stream_lvalue.hasNext()) {
                                        adaptor.addChild(root_2, stream_lvalue.nextTree());

                                    }
                                    stream_lvalue.reset();

                                    adaptor.addChild(root_1, root_2);
                                }

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;
                case 2: // /home/staff/lyczek/viscript.g:55:4: ID '[' tab_params ']' ( '.' lvalue )?
                {
                    ID56 = (Token) match(input, ID, FOLLOW_ID_in_lvalue557);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_ID.add(ID56);

                    char_literal57 = (Token) match(input, 32, FOLLOW_32_in_lvalue559);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_32.add(char_literal57);

                    pushFollow(FOLLOW_tab_params_in_lvalue561);
                    tab_params58 = tab_params();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_tab_params.add(tab_params58.getTree());
                    char_literal59 = (Token) match(input, 33, FOLLOW_33_in_lvalue563);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_33.add(char_literal59);

                    // /home/staff/lyczek/viscript.g:55:25: ( '.' lvalue )?
                    int alt18 = 2;
                    int LA18_0 = input.LA(1);

                    if ((LA18_0 == 31)) {
                        alt18 = 1;
                    }
                    switch (alt18) {
                        case 1: // /home/staff/lyczek/viscript.g:55:26: '.' lvalue
                        {
                            char_literal60 = (Token) match(input, 31, FOLLOW_31_in_lvalue565);
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_31.add(char_literal60);

                            pushFollow(FOLLOW_lvalue_in_lvalue567);
                            lvalue61 = lvalue();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_lvalue.add(lvalue61.getTree());

                        }
                        break;

                    }

                    // AST REWRITE
                    // elements: lvalue, tab_params, ID
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (CommonTree) adaptor.nil();
                        // 55:39: -> ^( ID ^( TAB_PARAMS tab_params ) ^( COMPONENT ( lvalue )? ) )
                        {
                            // /home/staff/lyczek/viscript.g:55:42: ^( ID ^( TAB_PARAMS tab_params ) ^( COMPONENT ( lvalue )? ) )
                            {
                                CommonTree root_1 = (CommonTree) adaptor.nil();
                                root_1 = (CommonTree) adaptor.becomeRoot(stream_ID.nextNode(), root_1);

                                // /home/staff/lyczek/viscript.g:55:47: ^( TAB_PARAMS tab_params )
                                {
                                    CommonTree root_2 = (CommonTree) adaptor.nil();
                                    root_2 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(TAB_PARAMS, "TAB_PARAMS"), root_2);

                                    adaptor.addChild(root_2, stream_tab_params.nextTree());

                                    adaptor.addChild(root_1, root_2);
                                }
                                // /home/staff/lyczek/viscript.g:55:72: ^( COMPONENT ( lvalue )? )
                                {
                                    CommonTree root_2 = (CommonTree) adaptor.nil();
                                    root_2 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(COMPONENT, "COMPONENT"), root_2);

                                    // /home/staff/lyczek/viscript.g:55:84: ( lvalue )?
                                    if (stream_lvalue.hasNext()) {
                                        adaptor.addChild(root_2, stream_lvalue.nextTree());

                                    }
                                    stream_lvalue.reset();

                                    adaptor.addChild(root_1, root_2);
                                }

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "lvalue"
    public static class rvalue_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "rvalue"
    // /home/staff/lyczek/viscript.g:57:1: rvalue : ( INT | ID '(' ( params )? ')' -> ^( CALL ID ( params )? ) );
    public final viscriptParser.rvalue_return rvalue() throws RecognitionException
    {
        viscriptParser.rvalue_return retval = new viscriptParser.rvalue_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token INT62 = null;
        Token ID63 = null;
        Token char_literal64 = null;
        Token char_literal66 = null;
        viscriptParser.params_return params65 = null;

        CommonTree INT62_tree = null;
        CommonTree ID63_tree = null;
        CommonTree char_literal64_tree = null;
        CommonTree char_literal66_tree = null;
        RewriteRuleTokenStream stream_ID = new RewriteRuleTokenStream(adaptor, "token ID");
        RewriteRuleTokenStream stream_25 = new RewriteRuleTokenStream(adaptor, "token 25");
        RewriteRuleTokenStream stream_26 = new RewriteRuleTokenStream(adaptor, "token 26");
        RewriteRuleSubtreeStream stream_params = new RewriteRuleSubtreeStream(adaptor, "rule params");
        try {
            // /home/staff/lyczek/viscript.g:57:8: ( INT | ID '(' ( params )? ')' -> ^( CALL ID ( params )? ) )
            int alt21 = 2;
            int LA21_0 = input.LA(1);

            if ((LA21_0 == INT)) {
                alt21 = 1;
            } else if ((LA21_0 == ID)) {
                alt21 = 2;
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1: // /home/staff/lyczek/viscript.g:57:10: INT
                {
                    root_0 = (CommonTree) adaptor.nil();

                    INT62 = (Token) match(input, INT, FOLLOW_INT_in_rvalue597);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0) {
                        INT62_tree = (CommonTree) adaptor.create(INT62);
                        adaptor.addChild(root_0, INT62_tree);
                    }

                }
                break;
                case 2: // /home/staff/lyczek/viscript.g:58:4: ID '(' ( params )? ')'
                {
                    ID63 = (Token) match(input, ID, FOLLOW_ID_in_rvalue602);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_ID.add(ID63);

                    char_literal64 = (Token) match(input, 25, FOLLOW_25_in_rvalue604);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_25.add(char_literal64);

                    // /home/staff/lyczek/viscript.g:58:11: ( params )?
                    int alt20 = 2;
                    int LA20_0 = input.LA(1);

                    if (((LA20_0 >= ID && LA20_0 <= INT) || LA20_0 == 25)) {
                        alt20 = 1;
                    }
                    switch (alt20) {
                        case 1: // /home/staff/lyczek/viscript.g:0:0: params
                        {
                            pushFollow(FOLLOW_params_in_rvalue606);
                            params65 = params();

                            state._fsp--;
                            if (state.failed)
                                return retval;
                            if (state.backtracking == 0)
                                stream_params.add(params65.getTree());

                        }
                        break;

                    }

                    char_literal66 = (Token) match(input, 26, FOLLOW_26_in_rvalue609);
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        stream_26.add(char_literal66);

                    // AST REWRITE
                    // elements: params, ID
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if (state.backtracking == 0) {
                        retval.tree = root_0;
                        RewriteRuleSubtreeStream stream_retval = new RewriteRuleSubtreeStream(adaptor, "rule retval", retval != null ? retval.tree : null);

                        root_0 = (CommonTree) adaptor.nil();
                        // 58:23: -> ^( CALL ID ( params )? )
                        {
                            // /home/staff/lyczek/viscript.g:58:26: ^( CALL ID ( params )? )
                            {
                                CommonTree root_1 = (CommonTree) adaptor.nil();
                                root_1 = (CommonTree) adaptor.becomeRoot((CommonTree) adaptor.create(CALL, "CALL"), root_1);

                                adaptor.addChild(root_1, stream_ID.nextNode());
                                // /home/staff/lyczek/viscript.g:58:36: ( params )?
                                if (stream_params.hasNext()) {
                                    adaptor.addChild(root_1, stream_params.nextTree());

                                }
                                stream_params.reset();

                                adaptor.addChild(root_0, root_1);
                            }

                        }

                        retval.tree = root_0;
                    }
                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "rvalue"
    public static class atom2_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "atom2"
    // /home/staff/lyczek/viscript.g:60:1: atom2 : ( rvalue | lvalue );
    public final viscriptParser.atom2_return atom2() throws RecognitionException
    {
        viscriptParser.atom2_return retval = new viscriptParser.atom2_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        viscriptParser.rvalue_return rvalue67 = null;

        viscriptParser.lvalue_return lvalue68 = null;

        try {
            // /home/staff/lyczek/viscript.g:60:7: ( rvalue | lvalue )
            int alt22 = 2;
            int LA22_0 = input.LA(1);

            if ((LA22_0 == INT)) {
                alt22 = 1;
            } else if ((LA22_0 == ID)) {
                int LA22_2 = input.LA(2);

                if ((LA22_2 == 25)) {
                    alt22 = 1;
                } else if ((LA22_2 == EOF || (LA22_2 >= LOG_OP && LA22_2 <= MUL_OP) || LA22_2 == 24 || LA22_2 == 26 || (LA22_2 >= 29 && LA22_2 <= 33))) {
                    alt22 = 2;
                } else {
                    if (state.backtracking > 0) {
                        state.failed = true;
                        return retval;
                    }
                    NoViableAltException nvae
                        = new NoViableAltException("", 22, 2, input);

                    throw nvae;
                }
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1: // /home/staff/lyczek/viscript.g:60:9: rvalue
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_rvalue_in_atom2629);
                    rvalue67 = rvalue();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, rvalue67.getTree());

                }
                break;
                case 2: // /home/staff/lyczek/viscript.g:61:4: lvalue
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_lvalue_in_atom2634);
                    lvalue68 = lvalue();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, lvalue68.getTree());

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "atom2"
    public static class atom_return extends ParserRuleReturnScope
    {

        CommonTree tree;

        public Object getTree()
        {
            return tree;
        }
    };

    // $ANTLR start "atom"
    // /home/staff/lyczek/viscript.g:63:1: atom : ( atom2 | '(' cexp ')' );
    public final viscriptParser.atom_return atom() throws RecognitionException
    {
        viscriptParser.atom_return retval = new viscriptParser.atom_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal70 = null;
        Token char_literal72 = null;
        viscriptParser.atom2_return atom269 = null;

        viscriptParser.cexp_return cexp71 = null;

        CommonTree char_literal70_tree = null;
        CommonTree char_literal72_tree = null;

        try {
            // /home/staff/lyczek/viscript.g:63:6: ( atom2 | '(' cexp ')' )
            int alt23 = 2;
            int LA23_0 = input.LA(1);

            if (((LA23_0 >= ID && LA23_0 <= INT))) {
                alt23 = 1;
            } else if ((LA23_0 == 25)) {
                alt23 = 2;
            } else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return retval;
                }
                NoViableAltException nvae
                    = new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1: // /home/staff/lyczek/viscript.g:63:8: atom2
                {
                    root_0 = (CommonTree) adaptor.nil();

                    pushFollow(FOLLOW_atom2_in_atom643);
                    atom269 = atom2();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, atom269.getTree());

                }
                break;
                case 2: // /home/staff/lyczek/viscript.g:64:4: '(' cexp ')'
                {
                    root_0 = (CommonTree) adaptor.nil();

                    char_literal70 = (Token) match(input, 25, FOLLOW_25_in_atom648);
                    if (state.failed)
                        return retval;
                    pushFollow(FOLLOW_cexp_in_atom651);
                    cexp71 = cexp();

                    state._fsp--;
                    if (state.failed)
                        return retval;
                    if (state.backtracking == 0)
                        adaptor.addChild(root_0, cexp71.getTree());
                    char_literal72 = (Token) match(input, 26, FOLLOW_26_in_atom653);
                    if (state.failed)
                        return retval;

                }
                break;

            }
            retval.stop = input.LT(-1);

            if (state.backtracking == 0) {

                retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
                adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);

        } finally {
        }
        return retval;
    }

    // $ANTLR end "atom"
    // $ANTLR start synpred10_viscript
    public final void synpred10_viscript_fragment() throws RecognitionException
    {
        // /home/staff/lyczek/viscript.g:39:9: ( lvalue ASSIGN_OP aexp )
        // /home/staff/lyczek/viscript.g:39:9: lvalue ASSIGN_OP aexp
        {
            pushFollow(FOLLOW_lvalue_in_synpred10_viscript343);
            lvalue();

            state._fsp--;
            if (state.failed)
                return;
            match(input, ASSIGN_OP, FOLLOW_ASSIGN_OP_in_synpred10_viscript345);
            if (state.failed)
                return;
            pushFollow(FOLLOW_aexp_in_synpred10_viscript348);
            aexp();

            state._fsp--;
            if (state.failed)
                return;

        }
    }

    // $ANTLR end synpred10_viscript
    // Delegated rules
    public final boolean synpred10_viscript()
    {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred10_viscript_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: " + re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed = false;
        return success;
    }

    public static final BitSet FOLLOW_stm_in_prog150 = new BitSet(new long[]{0x0000000003600072L});
    public static final BitSet FOLLOW_aexp_in_stm169 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_stm171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_while_op_in_stm177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_for_op_in_stm182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_if_op_in_stm187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_stm192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WHILE_OP_in_while_op202 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_while_op204 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_cexp_in_while_op206 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_while_op208 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_while_op210 = new BitSet(new long[]{0x0000000003600070L});
    public static final BitSet FOLLOW_prog_in_while_op212 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_while_op214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FOR_OP_in_for_op233 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_for_op235 = new BitSet(new long[]{0x0000000003600000L});
    public static final BitSet FOLLOW_aexp_in_for_op239 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_for_op242 = new BitSet(new long[]{0x0000000003600000L});
    public static final BitSet FOLLOW_cexp_in_for_op246 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_for_op249 = new BitSet(new long[]{0x0000000006600000L});
    public static final BitSet FOLLOW_aexp_in_for_op253 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_for_op256 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_for_op258 = new BitSet(new long[]{0x0000000003600070L});
    public static final BitSet FOLLOW_prog_in_for_op260 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_for_op262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_OP_in_if_op288 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_if_op290 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_cexp_in_if_op292 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_if_op294 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_if_op296 = new BitSet(new long[]{0x0000000003600070L});
    public static final BitSet FOLLOW_prog_in_if_op299 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_if_op301 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_ELSE_OP_in_if_op304 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_if_op306 = new BitSet(new long[]{0x0000000003600070L});
    public static final BitSet FOLLOW_prog_in_if_op310 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_if_op312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lvalue_in_aexp343 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_ASSIGN_OP_in_aexp345 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_aexp_in_aexp348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_cexp_in_aexp364 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_exp2_in_cexp373 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_LOG_OP_in_cexp376 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_exp2_in_cexp379 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_mulExp_in_exp2390 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_ADD_OP_in_exp2393 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_mulExp_in_exp2396 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_atom_in_mulExp407 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_MUL_OP_in_mulExp410 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_atom_in_mulExp413 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_cexp_in_range428 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_29_in_range433 = new BitSet(new long[]{0x0000000022600002L});
    public static final BitSet FOLLOW_cexp_in_range438 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_29_in_range443 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_cexp_in_range447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_cexp_in_params490 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_30_in_params493 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_cexp_in_params495 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_range_in_tab_params511 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_30_in_tab_params514 = new BitSet(new long[]{0x0000000062600000L});
    public static final BitSet FOLLOW_range_in_tab_params516 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_ID_in_lvalue532 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_31_in_lvalue535 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_lvalue_in_lvalue537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_lvalue557 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_lvalue559 = new BitSet(new long[]{0x0000000062600000L});
    public static final BitSet FOLLOW_tab_params_in_lvalue561 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_lvalue563 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_31_in_lvalue565 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_lvalue_in_lvalue567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_rvalue597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_rvalue602 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_rvalue604 = new BitSet(new long[]{0x0000000006600000L});
    public static final BitSet FOLLOW_params_in_rvalue606 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_rvalue609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rvalue_in_atom2629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lvalue_in_atom2634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_atom2_in_atom643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_atom648 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_cexp_in_atom651 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_atom653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lvalue_in_synpred10_viscript343 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_ASSIGN_OP_in_synpred10_viscript345 = new BitSet(new long[]{0x0000000002600000L});
    public static final BitSet FOLLOW_aexp_in_synpred10_viscript348 = new BitSet(new long[]{0x0000000000000002L});

}
