/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.CommonToken;
import org.antlr.runtime.CommonTokenStream;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Viscript
{

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Viscript.class);
    private static final String javaClassTemplate
        = "import java.util.HashMap;\n" +
        "import java.util.Map;\n" +
        "%s\n\n" +
        "public class %s extends  pl.edu.icm.visnow.viscript.CompiledViscript {\n\n" +
        "@Override\n" +
        "public Map<String, Object> run(Map<String, Object> bindings) {\n" +
        "Map<String, Object> outputBindings = new HashMap<String, Object>();\n\n" +
        "%s\n\n" +
        "return outputBindings;\n" +
        "}\n\n}\n";
    private final String script;
    private final String className;
    private CommonTree tree;
    private String javaSource;
    private ArrayList<Method> methods;
    private Map<String, Variable> variables;
    private int temporaryComponentCount;
    private int temporaryFloatCount;
    private int temporaryIntCount;
    private int temporaryBoolCount;

    public Viscript(String script, String className)
    {
        this.script = script;
        this.className = className;
        this.variables = new HashMap<String, Variable>();
        this.methods = new ArrayList<Method>();
        this.temporaryComponentCount = 0;
        this.temporaryFloatCount = 0;
        this.temporaryIntCount = 0;
        this.temporaryBoolCount = 0;
    }

    // FUNCTIONS
    public void declareStandardMethods()
    {
        this.methods.addAll(Arrays.asList(ViscriptStandardMethods.class.getMethods()));
    }

    public void declareMethod(Method m)
    {
        this.methods.add(m);
    }

    public boolean functionExists(String id)
    {
        for (Method m : methods) {
            if (m.getName().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public Class functionReturnType(String id)
    {
        for (Method m : methods) {
            if (m.getName().equals(id)) {
                return m.getReturnType();
            }
        }
        return null;
    }

    public Method getFunction(String id)
    {
        for (Method m : methods) {
            if (m.getName().equals(id)) {
                return m;
            }
        }
        return null;
    }

    public Class functionParameterType(String id, int parameterIndex)
    {
        for (Method m : methods) {
            if (m.getName().equals(id)) {
                return m.getParameterTypes()[parameterIndex];
            }
        }
        return null;
    }

    // VARIABLES
    public boolean variableExists(String id)
    {
        return variables.containsKey(id);
    }

    public Variable getVariable(String id)
    {
        return variables.get(id);
    }

    public void declareVariable(String id, Variable variable)
    {
        variables.put(id, variable);
    }

    public StringPair parseTree2(CommonTree ct)
    {
        String s1 = "", s2 = "";
        ExpType expType = ExpType.STMT;

        log.info(viscriptParser.tokenNames[ct.getType()] + " " + ct.getText() + " " + ct.getChildCount());

        switch (ct.getType()) {
            case 0:
            case viscriptParser.PROG: {
                for (int i = 0; i < ct.getChildCount(); i++) {
                    CommonTree child = (CommonTree) ct.getChild(i);
                    StringPair childResult = parseTree2(child);
                    s2 += String.format("%s %s;\n", childResult.getS1(), childResult.getS2());
                }
            }
            break;
            case viscriptParser.ID: {
                if (ct.getChildCount() == 2) {
                    CommonTree tabParamsChild = (CommonTree) ct.getChild(0);
                    CommonTree componentChild = (CommonTree) ct.getChild(1);

                    StringPair tabParamsChildResult = parseTree2(tabParamsChild);
                    s1 += tabParamsChildResult.getS1();
                    parseTree2(componentChild);
                    s2 += String.format("%s[%s]", ct.getText(), tabParamsChildResult);
                    expType = ExpType.FLOAT;

                } else if (ct.getChildCount() == 1) {
                    CommonTree componentChild = (CommonTree) ct.getChild(0);
                    if (ct.getChild(0).getType() == viscriptParser.TAB_PARAMS) {
                        parseTree2(componentChild);
                        s2 += ct.getText();
                    } else if (ct.getChild(0).getType() == viscriptParser.COMPONENT) {
                        if (variableExists(ct.getText()) && variables.get(ct.getText()).getVariableType() == VariableType.COMPONENT) {
                            expType = ExpType.COMPONENT;
                            s2 += String.format("%s[i]", ct.getText());
                        } else {
                            parseTree2(componentChild);
                            s2 += ct.getText();
                        }
                    }
                } else {
                    s2 += ct.getText();
                    if (variables.get(ct.getText()).getVariableType() == VariableType.COMPONENT) {
                        expType = ExpType.COMPONENT;
                    }
                }
            }
            break;
            case viscriptParser.TAB_PARAMS: {
                for (int i = 0; i < ct.getChildCount(); i++) {
                    CommonTree child = (CommonTree) ct.getChild(i);
                    StringPair childResult = parseTree2(child);
                    s1 += childResult.getS1();
                    switch (i) {
                        case 0:
                            s2 += childResult;
                            break;
                        case 1:
                            s2 += String.format(" + aDims[0] * %s", childResult);
                            break;
                        case 2:
                            s2 += String.format(" + aDims[0] * aDims[1] * %s", childResult);
                            break;
                    }
                }
            }
            break;
            case viscriptParser.COMPONENT: {
                if (ct.getChildCount() == 1) {
                    CommonTree component = (CommonTree) ct.getChild(0);
                    if (component.getText().equals("dims")) {
                    } else if (component.getText().equals("ndims")) {
                    } else {
                        log.error(String.format("wrong component name '%s'", component.getText()));
                    }
                    parseTree2(component);
                }
            }
            break;
            case viscriptParser.RANGE: {
                CommonTree beginRange = (CommonTree) ct.getChild(0);
                CommonTree endRange = (CommonTree) ct.getChild(1);
                CommonTree jumpRange = (CommonTree) ct.getChild(2);

                StringPair beginRangeResult = parseTree2(beginRange);
                StringPair endRangeResult = parseTree2(endRange);
                StringPair jumpRangeResult = parseTree2(jumpRange);
                s1 += beginRangeResult.getS1();
                s2 += beginRangeResult;
            }
            break;
            case viscriptParser.BEGIN_RANGE:
            case viscriptParser.END_RANGE:
            case viscriptParser.JUMP_RANGE: {
                if (ct.getChildCount() == 1) {
                    CommonTree range = (CommonTree) ct.getChild(0);
                    StringPair rangeResult = parseTree2(range);
                    s1 += rangeResult.getS1();
                    s2 += rangeResult;
                }
            }
            break;
            case viscriptParser.LOG_OP:
            case viscriptParser.ADD_OP:
            case viscriptParser.MUL_OP: {
                CommonTree child0 = (CommonTree) ct.getChild(0);
                CommonTree child1 = (CommonTree) ct.getChild(1);

                StringPair child0result = parseTree2(child0);
                StringPair child1result = parseTree2(child1);

                s1 += child0result.getS1() + child1result.getS1();
                s2 += String.format("(%s %s %s)", child0result, ct.getText(), child1result);
                expType = child0result.getExpType();
            }
            break;
            case viscriptParser.INT: {
                s2 += ct.getText();
                expType = ExpType.FLOAT;
            }
            break;
            case viscriptParser.WHILE_OP: {
                CommonTree conditionChild = (CommonTree) ct.getChild(0);
                CommonTree bodyChild = (CommonTree) ct.getChild(1);

                StringPair condition = parseTree2(conditionChild);
                s1 += condition.getS1();
                StringPair body = parseTree2(bodyChild);
                s2 += String.format("%stempBool%d = %s;\nwhile (tempBool%d) {\n%s\n%s\n%s tempBool%d = %s;\n}\n", condition.getS1(), temporaryBoolCount, condition.getS2(), temporaryBoolCount, body.getS1(), body.getS2(), condition.getS1(), temporaryBoolCount, condition.getS2());
                declareVariable("tempBool" + temporaryBoolCount, new Variable(VariableType.BOOL, false));
                temporaryBoolCount++;
            }
            break;
            case viscriptParser.FOR_OP: {
                CommonTree initChild = (CommonTree) ct.getChild(0);
                CommonTree conditionChild = (CommonTree) ct.getChild(1);
                CommonTree loopChild = (CommonTree) ct.getChild(2);
                CommonTree bodyChild = (CommonTree) ct.getChild(3);

                StringPair initChildresult = parseTree2(initChild);
                StringPair conditionChildresult = parseTree2(conditionChild);
                StringPair loopChildresult = parseTree2(loopChild);
                StringPair bodyChildresult = parseTree2(bodyChild);

                s2 += String.format("%s\n%s;\n%stempBool%d = %s;\nwhile (tempBool%d) {\n%s\n%s\n%s\n%s;\n  %s tempBool%d = %s;\n}\n", initChildresult.getS1(), initChildresult.getS2(), conditionChildresult.getS1(), temporaryBoolCount, conditionChildresult.getS2(), temporaryBoolCount, bodyChildresult.getS1(), bodyChildresult.getS2(), loopChildresult.getS1(), loopChildresult.getS2(), conditionChildresult.getS1(), temporaryBoolCount, conditionChildresult.getS2());
                declareVariable("tempBool" + temporaryBoolCount, new Variable(VariableType.BOOL, false));
                temporaryBoolCount++;
            }
            break;
            case viscriptParser.IF_OP: {
                CommonTree conditionChild = (CommonTree) ct.getChild(0);
                CommonTree bodyChild = (CommonTree) ct.getChild(1);

                StringPair conditionChildresult = parseTree2(conditionChild);
                StringPair bodyChildresult = parseTree2(bodyChild);
                if (ct.getChildCount() == 3) {
                    CommonTree elseBodyChild = (CommonTree) ct.getChild(2);
                    StringPair elseBodyChildresult = parseTree2(elseBodyChild);
                    s2 += String.format("%s\ntempBool%d = %s;\n if (tempBool%d) { %s\n %s} else {%s\n %s}", conditionChildresult.getS1(), temporaryBoolCount, conditionChildresult.getS2(), temporaryBoolCount, bodyChildresult.getS1(), bodyChildresult.getS2(), elseBodyChildresult.getS1(), elseBodyChildresult.getS2());
                } else {
                    s2 += String.format("%s\ntempBool%d = %s;\n if (tempBool%d) { %s\n %s}", conditionChildresult.getS1(), temporaryBoolCount, conditionChildresult.getS2(), temporaryBoolCount, bodyChildresult.getS1(), bodyChildresult.getS2());
                }
                declareVariable("tempBool" + temporaryBoolCount, new Variable(VariableType.BOOL, false));
                temporaryBoolCount++;
            }
            break;
            case viscriptParser.ASSIGN_OP: {
                CommonTree leftChild = (CommonTree) ct.getChild(0);
                CommonTree rightChild = (CommonTree) ct.getChild(1);

                StringPair rightChildresult = parseTree2(rightChild);
                if (!variableExists(leftChild.getText())) {
                    declareVariable(leftChild.getText(), new Variable(rightChildresult.getExpType()));
                }

                StringPair leftChildresult = parseTree2(leftChild);

                log.info(leftChildresult.getS1());
                log.info(rightChildresult.getS1());

                if (leftChildresult.getExpType() == ExpType.COMPONENT) {
                    switch (variables.get(leftChild.getText()).getNDims()) {
                        case 1:
                            s2 += String.format("%s %s for (int x = 0, i = 0; x < %s.length; x++, i++) {\n %s %s (%s);\n}\n",
                                                leftChildresult.getS1(), rightChildresult.getS1(),
                                                leftChild.getText(), leftChildresult,
                                                ct.getText(), rightChildresult);
                            break;
                        case 2:
                            s2 += String.format("%s %s for (int y = 0, i = 0; y < %s.length; y++) {\n   for (int x = 0; x < %s.length; x++, i++) { \n %s %s (%s);\n   }\n}\n",
                                                leftChildresult.getS1(), rightChildresult.getS1(),
                                                leftChild.getText(), leftChild.getText(), leftChildresult,
                                                ct.getText(), rightChildresult);
                            break;
                        case 3:
                            s2 += String.format("%s %s for (int z = 0, i = 0; z < %s.length; z++) {\n   for (int y = 0; y < %s.length; y++) {\n      for (int x = 0; x < %s.length; x++, i++) {\n %s %s (%s);\n      }   \n}\n}\n",
                                                leftChildresult.getS1(), rightChildresult.getS1(),
                                                leftChild.getText(), leftChild.getText(), leftChild.getText(), leftChildresult,
                                                ct.getText(), rightChildresult);
                            break;
                    }
                } else {
                    s2 += String.format("%s %s\n %s %s (%s);\n", leftChildresult.getS1(), rightChildresult.getS1(), leftChildresult, ct.getText(), rightChildresult);
                }

                expType = leftChildresult.getExpType();
            }
            break;
            case viscriptParser.CALL: {
                String fun = ct.getChild(0).getText();
                if (!functionExists(fun)) {
                    log.error(String.format("function '%s' not found!", fun));
                }
                // TODO: sprawdzenie liczby parametrow
                String retId = "";
                if (functionReturnType(fun).equals(float.class)) {
                    retId = String.format("tempFloat%d", temporaryFloatCount++);
                } else {
                    retId = String.format("tempComp%d", temporaryComponentCount++);
                }

                String v = "";
                for (int i = 1; i < ct.getChildCount(); i++) {
                    String id = ct.getChild(i).getText();
                    switch (ct.getChild(i).getType()) {
                        case viscriptParser.ID:
                            if (!variableExists(id)) {
                                log.error(String.format("variable '%s' not declared", id));
                            }
                            if (!functionParameterType(fun, i - 1).equals(getVariable(id).getVariableClass())) {
                                log.error(String.format("variable '%s' type != function '%d' parameter type", id, i - 1));
                            }
                            v += ct.getChild(i).getText();
                            break;
                        case viscriptParser.INT:
                            if (!functionParameterType(fun, i - 1).equals(float.class)) {
                                log.error(String.format("function '%d' parameter type != float", i - 1));
                            }
                            v += ct.getChild(i).getText();
                            break;
                        default:
                            String parId = "";
                            if (functionParameterType(fun, i - 1).equals(float.class)) {
                                parId = String.format("tempFloat%d", temporaryFloatCount++);
                                declareVariable(parId, new Variable(VariableType.FLOAT, false));
                            } else {
                                parId = String.format("tempComp%d", temporaryComponentCount++);
                                declareVariable(parId, new Variable(VariableType.COMPONENT, false));
                            }
                            CommonTree assign = new CommonTree(new CommonToken(viscriptParser.ASSIGN_OP, "="));
                            CommonTree tmpId = new CommonTree(new CommonToken(viscriptParser.ID, parId));
                            assign.addChild(tmpId);
                            assign.addChild(ct.getChild(i));
                            StringPair vv = parseTree2(assign);
                            if (!functionParameterType(fun, i - 1).equals(vv.getExpType().getExpTypeClass())) {
                                log.error(String.format("variable '%s' type != function '%d' parameter type", parId, i - 1));
                            }

                            s1 += vv.getS1();
                            s1 += vv;
                            v += parId;
                            break;
                    }
                    if (i != ct.getChildCount() - 1) {
                        v += ", ";
                    }
                }

                log.info("s1: " + s1);

                Method m = getFunction(fun);
                if (functionReturnType(fun).equals(float.class)) {
                    s2 += retId;
                    s1 += String.format("%s = %s.%s (%s);\n", retId, m.getDeclaringClass().getCanonicalName(), m.getName(), v);
                    declareVariable(retId, new Variable(VariableType.FLOAT, false));
                    expType = ExpType.FLOAT;
                } else if (functionReturnType(fun).equals(float[].class)) {
                    s2 += retId + "[i]";
                    s1 += String.format("%s = %s.%s (%s);\n", retId, m.getDeclaringClass().getCanonicalName(), m.getName(), v);
                    declareVariable(retId, new Variable(VariableType.COMPONENT, false));
                    expType = ExpType.COMPONENT;
                } else {
                    s2 += String.format("%s.%s (%s);\n", m.getDeclaringClass().getCanonicalName(), m.getName(), v);
                    expType = ExpType.STMT;
                }
            }
        }
        return new StringPair(s1, s2, expType);
    }

    public String printVariables()
    {
        String s = "";
        for (Entry<String, Variable> e : variables.entrySet()) {
            s += String.format("%s [%s] %s\n", e.getKey(), e.getValue().getVariableType(), e.getValue().isInputVariable());
        }
        return s;
    }

    public void generateTree()
    {
        try {
            byte[] currentXMLBytes = script.getBytes();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(currentXMLBytes);
            ANTLRInputStream input = new ANTLRInputStream(byteArrayInputStream);
            viscriptLexer lexer = new viscriptLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            viscriptParser parser = new viscriptParser(tokens);
            viscriptParser.prog_return ret = parser.prog();
            tree = (CommonTree) ret.getTree();
        } catch (RecognitionException ex) {
            Logger.getLogger(Viscript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Viscript.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected String getImportsSource()
    {
        String s = "";
        return s;
    }

    protected String getInitVariablesSource()
    {
        String s = "";
        for (Entry<String, Variable> e : variables.entrySet()) {
            Variable v = e.getValue();
            if (v.isInputVariable()) {
                String className = v.getVariableClass().getCanonicalName();
                if (v.getVariableClass().getCanonicalName().equals("float")) {
                    className = "Float";
                }
                s += String.format("%s %s = (%s) bindings.get(\"%s\");\n", v.getVariableClass().getCanonicalName(), e.getKey(), className, e.getKey());
            } else if (!v.isInitated()) {
                if (v.getVariableClass().equals(float.class)) {
                    s += String.format("float %s = 0;\n", e.getKey());
                } else if (v.getVariableClass().equals(boolean.class)) {
                    s += String.format("boolean %s = false;\n", e.getKey());
                } else if (v.getVariableClass().equals(int.class)) {
                    s += String.format("int %s = 0;\n", e.getKey());
                } else {
                    s += String.format("float[] %s = new float[(Integer) bindings.get(\"componentSize\")];\n", e.getKey());
                }

            }
        }
        return s;
    }

    protected String getOutputVariablesSource()
    {
        String s = "";
        for (Entry<String, Variable> e : variables.entrySet()) {
            Variable v = e.getValue();
            s += String.format("outputBindings.put(\"%s\", (%s) %s);\n", e.getKey(), v.getVariableClass().getCanonicalName(), e.getKey());
        }
        return s;
    }

    public void parse()
    {
        for (Entry<String, Variable> e : variables.entrySet()) {
            e.getValue().setInputVariable(true);
        }
        generateTree();

        String javaKernel = parseTree2(tree).getS2();
        javaKernel = getInitVariablesSource() + javaKernel;
        javaKernel += getOutputVariablesSource();
        javaSource = String.format(javaClassTemplate, getImportsSource(), className, javaKernel);
    }

    public String getJavaSource()
    {
        return javaSource;
    }
}
