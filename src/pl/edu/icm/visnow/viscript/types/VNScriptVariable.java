/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript.types;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNScriptVariable
{

    public final VNScriptType type;
    public final int veclen;
    public int dimensions;
    private boolean output;
    private boolean input;
    private boolean used;

    public VNScriptVariable(VNScriptType type, int veclen, boolean input)
    {
        this.type = type;
        this.veclen = veclen;
        this.input = input;
    }

    public boolean isUsed()
    {
        return used;
    }

    public void setUsed(boolean used)
    {
        this.used = used;
    }

    public VNScriptVariable(VNScriptType type, int veclen)
    {
        this.type = type;
        this.veclen = veclen;
    }

    public VNScriptVariable(VNScriptType type, int veclen, int dimensions)
    {
        this.type = type;
        this.veclen = veclen;
        this.dimensions = dimensions;
    }

    public VNScriptVariable(VNScriptType type, int veclen, int dimensions, boolean input)
    {
        this.type = type;
        this.veclen = veclen;
        this.dimensions = dimensions;
        this.input = input;
    }

    public boolean isOutput()
    {
        return output;
    }

    public void setOutput(boolean output)
    {
        this.output = output;
    }

    public boolean isInput()
    {
        return input;
    }

    public void setInput(boolean input)
    {
        this.input = input;
    }
}
