/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript.types;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNScriptExpr
{

    protected VNScriptExpr e1, e2;
    protected String text;

    public VNScriptExpr()
    {
        this.text = "";
    }

    public VNScriptExpr(String text)
    {
        this.text = text;
    }

    public VNScriptExpr(VNScriptExpr e1, VNScriptExpr e2, String text)
    {
        this.text = text;
        this.e1 = e1;
        this.e2 = e2;
    }

    public VNScriptExpr(VNScriptExpr e1, String text)
    {
        this.text = text;
        this.e1 = e1;
    }

    public VNScriptExpr(String text, VNScriptExpr e2)
    {
        this.text = text;
        this.e2 = e2;
    }

    @Override
    public String toString()
    {
        if (e1 != null && e2 != null) {
            return String.format("(%s %s %s)", e1.toString(), text, e2.toString());
        } else if (e1 != null) {
            return String.format("%s%s", e1.toString(), text);
        } else if (e2 != null) {
            return String.format("%s%s", text, e2.toString());
        } else {
            return text;
        }
    }

    public String toString(int vec)
    {
        if (e1 != null && e2 != null) {
            return String.format("(%s %s %s)", e1.toString(vec), text, e2.toString(vec));
        } else if (e1 != null) {
            return String.format("%s%s", e1.toString(vec), text);
        } else if (e2 != null) {
            return String.format("%s%s", text, e2.toString(vec));
        } else {
            return text;
        }
    }

    public VNScriptType getType()
    {

        if (e1 != null && e2 != null) {
            if (e1.getType() == VNScriptType.ARRAY ||
                e2.getType() == VNScriptType.ARRAY) {
                return VNScriptType.ARRAY;
            } else {
                return VNScriptType.SCALAR;
            }
        } else if (e1 != null) {
            return e1.getType();
        } else if (e2 != null) {
            return e2.getType();
        } else {
            return VNScriptType.UNDEFINED;
        }

    }

    public String toString(boolean asIdent)
    {
        return toString();
    }
}
