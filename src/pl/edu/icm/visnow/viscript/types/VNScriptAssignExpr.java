/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript.types;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VNScriptAssignExpr extends VNScriptExpr
{

    private VNScriptContext context;

    public VNScriptAssignExpr(VNScriptExpr e1, VNScriptExpr e2, String text, VNScriptContext context)
    {
        super(e1, e2, text);
        this.context = context;
    }

    private String assignScalar()
    {
        String result = "";
        if (e2.getType() != VNScriptType.SCALAR) {
            // System.err.println("trying to assign not scalar value to scalar");
        }
        result += e1.toString() + " = " + e2.toString();

        return result;
    }

    private String assignArray()
    {
        String result = "";
        //        if (e1 instanceof VNScriptArray && e2 instanceof VNScriptArray && text.equals("=")) {
        //            result += String.format("System.arraycopy(%s, 0, %s, 0, %s.length)", e2.toString(true), e1.toString(true), e2.toString(true));
        //        } else {
        VNScriptArray sa = (VNScriptArray) e1;
        String assignOperator = text;

        // loop begins
        result += sa.createLoopsInit().replaceAll("\\$ident\\$", sa.ident);

        // inside loop
        result += String.format("int loopTempOffset = (int) (%s);\n", sa.createLoopOffsetVariable().replaceAll("\\$ident\\$", sa.ident));

        if (sa.veclen > 1) {
            for (int k = 0; k < sa.veclen; k++) {
                result += String.format("%s %s %s;\n", sa.toString(k), assignOperator, e2.toString(k));
                // System.err.println(e2.getClass());
            }
        } else {
            result += String.format("%s %s %s;\n", sa, assignOperator, e2);
        }

        // loop ends
        result += sa.createLoopsEnd();
        //        }
        return result;
    }

    @Override
    public String toString()
    {
        String result = "";

        if (!(e1 instanceof VNScriptSubArray ||
            e1 instanceof VNScriptArray ||
            e1 instanceof VNScriptScalar)) {
            // System.err.println("wrong type in left side of assign");
        }

        if (e1 instanceof VNScriptSubArray) {
            VNScriptSubArray sa = (VNScriptSubArray) e1;
            if (sa.isSingle()) {
                result += assignScalar();
            } else {
                result += assignArray();
            }
        } else if (e1 instanceof VNScriptArray) {
            result += assignArray();
        } else if (e1 instanceof VNScriptScalar) {
            result += assignScalar();
        } else if (e1 instanceof VNScriptUndefined) {
            if (e2 instanceof VNScriptMethodCall) {
                VNScriptMethodCall mc = (VNScriptMethodCall) e2;
                if (mc.text.equals("array")) {
                    // deklaracja nowej tablicy z veclen = 1
                    String size = "";
                    for (int d = 0; d < mc.getArgumentCount(); d++) {
                        result += "int _" + e1.text + "_dims_" + d + " = (int) (" + mc.args[d] + ");\n";
                        if (d != 0) {
                            size += " * ";
                        }
                        size += "_" + e1.text + "_dims_" + d;
                    }
                    result += "float[] " + e1.text + " = new float[" + size + "];\n";
                    context.putVariable(e1.text, new VNScriptVariable(VNScriptType.ARRAY, 1, mc.getArgumentCount()));

                } else if (mc.text.equals("varray")) {
                    // deklaracja nowej tablicy z dowolnym veclen
                    for (int d = 1; d < mc.getArgumentCount(); d++) {
                        result += "_" + e1.text + "_dims_" + (d - 1) + " = " + mc.args[d] + ";\n";
                    }
                    context.putVariable(e1.text, new VNScriptVariable(VNScriptType.ARRAY, Integer.parseInt(mc.args[0].text), mc.getArgumentCount()));
                }
            } else {
                context.putVariable(e1.text, new VNScriptVariable(VNScriptType.SCALAR, 1));
                result = "float " + assignScalar();
            }
        }

        return result;
    }
}
