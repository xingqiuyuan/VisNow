/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.viscript;

/**
 * @author Michał Łyczek (lyczek@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Variable
{

    private final VariableType variableType;
    private int[] dims;
    private boolean inputVariable;
    private boolean initated;

    public Variable(VariableType variableType, boolean initated)
    {
        this.variableType = variableType;
        this.inputVariable = false;
        this.initated = initated;
    }

    public Variable(VariableType variableType)
    {
        this.variableType = variableType;
        this.inputVariable = false;
        this.initated = false;
        this.dims = null;
    }

    public Variable(VariableType variableType, int[] dims, boolean initated)
    {
        this.variableType = variableType;
        this.inputVariable = false;
        this.initated = initated;
        this.dims = dims;
    }

    public Variable(ExpType expType)
    {
        switch (expType) {
            case FLOAT:
                this.variableType = VariableType.FLOAT;
                this.dims = null;
                break;
            case COMPONENT:
                this.variableType = VariableType.COMPONENT;
                this.dims = new int[]{0, 0, 0};
                break;
            default:
                this.variableType = VariableType.FLOAT;
        }
        this.inputVariable = false;
        this.initated = false;
        this.dims = null;
    }

    public Variable(ExpType expType, int[] dims, boolean initated)
    {
        switch (expType) {
            case FLOAT:
                this.variableType = VariableType.FLOAT;
                this.dims = null;
                break;
            case COMPONENT:
                this.variableType = VariableType.COMPONENT;
                this.dims = dims;
                break;
            default:
                this.variableType = VariableType.FLOAT;
        }
        this.inputVariable = false;
        this.initated = initated;
        this.dims = null;
    }

    public VariableType getVariableType()
    {
        return variableType;
    }

    public Class getVariableClass()
    {
        return variableType.getVariableTypeClass();
    }

    public void setInputVariable(boolean inputVariable)
    {
        this.inputVariable = inputVariable;
    }

    public boolean isInputVariable()
    {
        return inputVariable;
    }

    public boolean isInitated()
    {
        return initated;
    }

    public int[] getDims()
    {
        return dims;
    }

    public int getNDims()
    {
        return (dims == null) ? 0 : dims.length;
    }
}
