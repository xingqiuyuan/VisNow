//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.numeric;

import static java.lang.Math.min;
import java.util.Arrays;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 * @author know
 */


public class EuclideanDistanceMap
{

    private EuclideanDistanceMap()
    {
    }
    
    
    private  static class AxisProcessor implements Runnable
    {
        private int[] start;
        private int step;
        private int nSteps;
        private int[] map;

        public AxisProcessor(int[] start, int step, int nSteps, int[] map)
        {
            this.start = start;
            this.step = step;
            this.nSteps = nSteps;
            this.map = map;
        }
        
        @Override
        public void run()
        {
            int[] row = new int[nSteps];
            for (int nLine = 0; nLine < start.length; nLine++) {
                for (int i = 0, j = start[nLine]; i < row.length; i++, j += step) 
                    row[i] = map[j];
                for (int i = 0; i < row.length; i++) {
                }
                for (int i = 0, j = start[nLine]; i < row.length; i++, j += step) 
                    map[j] = row[i];
            }
        }
    }
    
    private static void updateAxisMaps(int[] dims, int axis, int[] map)
    {
        int step = 1;
        int[] start = null;
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            int dk, kstart = 0, kend = 1, n;
            switch (dims.length)
            {
            case 3:
                switch (axis)
                {
                case 2:
                    n = dims[0] * dims[1];
                    step  = dims[0] * dims[1];
                    dk = n / nThreads;
                    kstart = iThread * dk + min(iThread, n % nThreads);
                    kend = (iThread + 1) * dk + min(iThread + 1, n % nThreads);
                    start = new int[kend - kstart];
                    for (int i = 0; i < kend - kstart; i++) 
                        start[i] = kstart + i;
                    break;
                case 1:
                    n = dims[0] * dims[2];
                    step  = dims[0];
                    dk = n / nThreads;
                    kstart = iThread * dk + min(iThread, n % nThreads);
                    kend = (iThread + 1) * dk + min(iThread + 1, n % nThreads);
                    start = new int[kend - kstart];
                    for (int i = 0; i < kend - kstart; i++) 
                    {
                        int l = kstart + i;
                        start[i] = l % dims[0] + (dims[0] * dims[1] * (l / dims[0]));
                    }
                    break;
                case 0:
                    n = dims[1] * dims[2];
                    step  = 1;
                    dk = n / nThreads;
                    kstart = iThread * dk + min(iThread, n % nThreads);
                    kend = (iThread + 1) * dk + min(iThread + 1, n % nThreads);
                    start = new int[kend - kstart];
                    for (int i = 0; i < kend - kstart; i++) 
                        start[i] = (kstart + i) * dims[0];
                    break;
                }
                break;
            case 2:
                switch (axis)
                {
                case 1:
                    n = dims[0];
                    step  = dims[0];
                    dk = n / nThreads;
                    kstart = iThread * dk + min(iThread, n % nThreads);
                    kend = (iThread + 1) * dk + min(iThread + 1, n % nThreads);
                    start = new int[kend - kstart];
                    for (int i = 0; i < kend - kstart; i++) 
                        start[i] = kstart + i;
                    break;
                case 0:
                    n = dims[1];
                    step  = 1;
                    dk = n / nThreads;
                    kstart = iThread * dk + min(iThread, n % nThreads);
                    kend = (iThread + 1) * dk + min(iThread + 1, n % nThreads);
                    start = new int[kend - kstart];
                    for (int i = 0; i < kend - kstart; i++) 
                        start[i] = (kstart + i) * dims[0];
                    break;
                }
                break;
            case 1: break;
            }
            workThreads[iThread] = new Thread(new AxisProcessor(start, step, dims[0], map));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
    }
    
    public static float[] createMap2D(int[] dims, int[][] v, float[] setCoords)
    {
        int[] imap = new int[dims[0] * dims[1]];
        Arrays.fill(imap, -1);
        for (int i = 0; i < setCoords.length; i += 3) {
            int i0 = (int)(setCoords[i] + .5);
            int i1 = (int)(setCoords[i + 1] + .5);
            if (i0 >= 0 && i0 < dims[0] &&
                i1 >= 0 && i1 < dims[1])
                imap[i1 * dims[0] + i0] = 0;
        }
        float[] map = new float[imap.length];
        for (int i = 0; i < map.length; i++) 
            map[i] = (float)Math.sqrt(imap[i]);
        return map;
    }
    
    public static float[] createMap2D(int[] dims, float[] setCoords)
    {
        return null;
//        return createMap2D(dims, new float[][] {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, 0}}, setCoords);
    }
    
    
    public static void main(String args[])
    {
        int[] dims = {10,10};
        float[] coords = {3,3,0, 4,3,0, 4,6,0, 8,3,0};
        float[] map = createMap2D(dims, coords);
        for (int i = 0; i < map.length; i += 10) {
            for (int j = 0; j < 10; j++) 
                System.out.printf("%7.3f ", Math.sqrt(map[i+j]));
            System.out.println("");
            
        }
    }
}
