/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.vtk;

import java.io.File;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.lib.utils.field.MergeIrregularField;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;
import vtk.vtkCell;
import vtk.vtkCellData;
import vtk.vtkDataArray;
import vtk.vtkDataObject;
import vtk.vtkDataSetTriangleFilter;
import vtk.vtkDoubleArray;
import vtk.vtkFloatArray;
import vtk.vtkGenericDataObjectReader;
import vtk.vtkIntArray;
import vtk.vtkLongArray;
import vtk.vtkMultiBlockDataSet;
import vtk.vtkNativeLibrary;
import vtk.vtkPointData;
import vtk.vtkPointSet;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkRectilinearGrid;
import vtk.vtkShortArray;
import vtk.vtkUnsignedCharArray;
import vtk.vtkUnstructuredGrid;
import vtk.vtkXMLGenericDataObjectReader;

/**
 *
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 * @author creed Interdisciplinary Centre for Mathematical and Computational
 * Modelling
 */
public class VTKNativeCore extends VTKCore {

    private static final Logger LOGGER = Logger.getLogger(VTKNativeCore.class);

    /**
     * Creates a new <code>VTKNativeCore</code> object.
     */
    public VTKNativeCore() {
        vtkNativeLibrary.DisableOutputWindow(logFile);
    }

    @Override
    public Field readVTK(String filename, ByteOrder order) {
        Field res = loadVTK(filename);
        if (res == null) {
            VTKJavaCore javaCore = new VTKJavaCore();
            return javaCore.readVTK(filename, order);
        }
        return res;
    }

    @Override
    public Field readVTK(File filename, ByteOrder order) {
        return readVTK(filename.getAbsolutePath(), order);
    }

    private static vtkDataObject triangulateVTK(vtkDataObject data) {
        vtkDataSetTriangleFilter triangleFilter = new vtkDataSetTriangleFilter();
        triangleFilter.SetInputData(data);
        triangleFilter.Update();
        return triangleFilter.GetOutput();
    }

    private static Field loadVTK(String filename) {
        vtkGenericDataObjectReader readerLegacy = new vtkGenericDataObjectReader();
        readerLegacy.SetFileName(filename);
        readerLegacy.Update();
        vtkDataObject data = readerLegacy.GetOutput();
        if (data != null) {
            if (readerLegacy.IsFilePolyData() != 0) //        if( readerLegacy.OpenVTKFile() != 0 && readerLegacy.ReadHeader() !=0 )
            {
                LOGGER.info("using legacy reader & polydata");
                return vtkPointSet2vnField(readerLegacy.GetPolyDataOutput());
            } else if (readerLegacy.IsFileUnstructuredGrid() != 0) {
                LOGGER.info("using legacy reader & unstructured grid");
                return vtkPointSet2vnField(readerLegacy.GetUnstructuredGridOutput());
            } else if (readerLegacy.IsFileRectilinearGrid() != 0) {
                LOGGER.info("using legacy reader & rectilinear grid");
                return vtkRectilinearGrid2vnField(readerLegacy.GetRectilinearGridOutput());
            } else {
                LOGGER.error("legacy reader is OK, trying to triangulate...");
                VisNow.get().userMessageSend(null, "Unsupported cells in VTK file - triangulating.", "Dataset consists of VTK cells that are not supported in VisNow. Such cells will be triangulated.", Level.WARNING);
                data = triangulateVTK(data);
                if (data instanceof vtkPolyData) {
                    return vtkPointSet2vnField((vtkPolyData) data);
                } else if (data instanceof vtkUnstructuredGrid) {
                    return vtkPointSet2vnField((vtkUnstructuredGrid) data);
                } else if (data instanceof vtkRectilinearGrid) {
                    return vtkRectilinearGrid2vnField((vtkRectilinearGrid) data);
                } else {
                    LOGGER.error("legacy reader is OK, but neither polydata nor rectilinear grid nor unstuctured grid");
                }
            }
        }

        vtkXMLGenericDataObjectReader readerXML = new vtkXMLGenericDataObjectReader();
        readerXML.SetFileName(filename);
        readerXML.Update();
        data = readerXML.GetOutput();
        if (data != null) {
            if (readerXML.GetPolyDataOutput() != null) {
                LOGGER.info("using XML reader & polydata");
                return vtkPointSet2vnField(readerXML.GetPolyDataOutput());
            } else if (readerXML.GetUnstructuredGridOutput() != null) {
                LOGGER.info("using XML reader & unstructured data");
                return vtkPointSet2vnField(readerXML.GetUnstructuredGridOutput());
            } else if (readerXML.GetMultiBlockDataSetOutput() != null) {
                LOGGER.info("using XML reader & multi block");
                return vtkMultiBlock2vnField(readerXML.GetMultiBlockDataSetOutput());
            } else {
                LOGGER.error("neither PolyData nor Unstructured Data nor Multi Block, trying to triangulate...");
                VisNow.get().userMessageSend(null, "Unsupported cells in VTK file - triangulating.", "Dataset consists of VTK cells that are not supported in VisNow. Such cells will be triangulated.", Level.WARNING);
                data = triangulateVTK(data);
                if (data instanceof vtkPolyData) {
                    return vtkPointSet2vnField((vtkPolyData) data);
                } else if (data instanceof vtkUnstructuredGrid) {
                    return vtkPointSet2vnField((vtkUnstructuredGrid) data);
                } else if (data instanceof vtkMultiBlockDataSet) {
                    return vtkMultiBlock2vnField((vtkMultiBlockDataSet) data);
                } else {
                    LOGGER.error("neither PolyData nor Unstructured Data nor Multi Block, giving up");
                    return null;
                }
            }
        } else {
            LOGGER.error("neither legacy nor XML file, giving up");
            return null;
        }
    }

    private static IrregularField vtkPointSet2vnField(vtkPointSet pointSet) {
        if (pointSet == null) {
            throw new IllegalArgumentException("Input argument cannot be null");
        }

        vtkPoints points = pointSet.GetPoints();

        IrregularField outField = new IrregularField(points.GetNumberOfPoints());

        if (!LoadCoords(outField, points)) {
            return null;
        }
        if (!LoadPointData(outField, pointSet.GetPointData())) {
            return null;
        }
        if (!LoadCells(outField, pointSet)) {
            VisNow.get().userMessageSend(null, "Unsupported cells in VTK file - triangulating.", "Dataset consists of VTK cells that are not supported in VisNow. Such cells will be triangulated.", Level.WARNING);
            pointSet = (vtkPointSet) triangulateVTK((vtkDataObject) pointSet);
            if (!LoadCells(outField, pointSet)) {
                VisNow.get().userMessageSend(null, "Failed to triangulate unsupported VTK cells.", "", Level.ERROR);
                return null;
            }
        }
        if (!LoadCellData(outField.getCellSet(0), pointSet.GetCellData())) {
            return null;
        }
        AddNullDataIfNone(outField);

        return outField;
    }

    private static int vtkToVnCellType(int vtkCellType) {
        int[] vtkCellTypes = new int[]{1, 3, 5, 9, 10, 14, 13, 12};
        return ArrayUtils.indexOf(vtkCellTypes, vtkCellType);
    }

    private static boolean LoadCoords(IrregularField field, vtkPoints points) {
        if (field == null || points == null) {
            return false;
        }
        int noDims = 3;
        int nPoints = points.GetNumberOfPoints();

        // load geometry        
        LOGGER.info("[points]");
        LOGGER.info("number of points: " + nPoints);

        float[] coords = new float[nPoints * noDims];
        for (int n = 0; n < nPoints; n++) {
            double[] p = points.GetPoint(n);
            coords[n * noDims] = (float) p[0];
            coords[n * noDims + 1] = (float) p[1];
            coords[n * noDims + 2] = (float) p[2];
        }
        field.setCurrentCoords(new FloatLargeArray(coords));
        return true;
    }

    private static boolean LoadPointData(Field field, vtkPointData pointData) {
        if (field == null || pointData == null) {
            return false;
        }
        int nArrays = pointData.GetNumberOfArrays();
        int nComponentsTotal = pointData.GetNumberOfComponents();
        int nTuples = pointData.GetNumberOfTuples();
        LOGGER.info("[[point data]]");
        LOGGER.info("number of arrays: " + nArrays);
        LOGGER.info("number of components: " + nComponentsTotal);
        LOGGER.info("number of tuples: " + nTuples);

        for (int i = 0; i < nArrays; i++) {
            vtkDataArray array = pointData.GetArray(i);
            if (array == null) {
                LOGGER.warn("vtkPointData at index " + i + " is not a vtkDataArray.");
                VisNow.get().userMessageSend(null, "vtkPointData at index " + i + " is not a vtkDataArray.", "", Level.WARNING);
                continue;
            }
            int nComponents = array.GetNumberOfComponents();
            assert (nTuples == array.GetNumberOfTuples());
            String name = array.GetName();
            LOGGER.info("[array " + i + ", name: " + name + "]");
            LOGGER.info("number of components: " + nComponents);
            LOGGER.info("number of tuples: " + nTuples);

            if (name.isEmpty()) {
                name = "points_data_" + Integer.toString(i);
            }
            field.addComponent(vtkDataArray2vnDataArray(array, name));
        }
        return true;
    }

    private static boolean LoadCells(IrregularField field, vtkPointSet polyData) {
        if (field == null || polyData == null) {
            return false;
        }
        final int nCells = polyData.GetNumberOfCells();
        LOGGER.info("[cells]");
        LOGGER.info("cells: " + nCells);
        LOGGER.info("points: " + polyData.GetNumberOfPoints());
        if (nCells <= 0) {
            return false;
        }
        // count number of cells of each type        
        int[] nIndices = new int[Cell.getNProperCellTypes()];
        for (int i = 0; i < polyData.GetNumberOfCells(); i++) {
            vtkCell cell = polyData.GetCell(i);
            if (cell == null) {
                LOGGER.warn("vtkPointSet at index " + i + " is not a vtkCell.");
                VisNow.get().userMessageSend(null, "vtkPointSet at index " + i + " is not a vtkCell.", "", Level.WARNING);
                continue;
            }
            final int vnCellType = vtkToVnCellType(cell.GetCellType());
            if (vnCellType == -1) {
                LOGGER.error("VTK cell type " + cell.GetCellType() + " not supported, trying to triangulate...");
                return false;
            } else {
                nIndices[vnCellType]++;
            }
        }

        // create index arrays        
        int[][] indices = new int[Cell.getNProperCellTypes()][];
        int[][] data_indices = new int[Cell.getNProperCellTypes()][];
        for (int i = 0; i < indices.length; i++) {
            indices[i] = new int[nIndices[i] * CellType.getType(i).getNVertices()];
            data_indices[i] = new int[nIndices[i]];
        }

        int[] is = new int[Cell.getNProperCellTypes()];
        Arrays.fill(is, 0);

        for (int i = 0; i < polyData.GetNumberOfCells(); i++) {
            vtkCell cell = polyData.GetCell(i);
            if (cell == null) {
                LOGGER.warn("vtkPointSet at index " + i + " is not a vtkCell.");
                VisNow.get().userMessageSend(null, "vtkPointSet at index " + i + " is not a vtkCell.", "", Level.WARNING);
                continue;
            }
            int vnCellType = vtkToVnCellType(cell.GetCellType());
            if (vnCellType == -1) {
                continue;
            }
            assert (cell.GetNumberOfPoints() == CellType.getType(vnCellType).getNVertices());
            data_indices[vnCellType][is[vnCellType] / CellType.getType(vnCellType).getNVertices()] = i;
            for (int j = 0; j < CellType.getType(vnCellType).getNVertices(); j++) {
                indices[vnCellType][is[vnCellType]++] = cell.GetPointId(j);
            }
        }

        CellSet cellSet = new CellSet();
        for (int i = 0; i < indices.length; i++) {
            if (is[i] == 0) {
                continue;
            }
            CellArray cellArray = new CellArray(CellType.getType(i), indices[i], null, data_indices[i]);
            cellSet.setCellArray(cellArray);
        }
        cellSet.generateDisplayData(field.getCurrentCoords());

        field.addCellSet(cellSet);
        return true;
    }

    private static boolean LoadCellData(CellSet cellSet, vtkCellData cellData) {
        if (cellSet == null || cellData == null) {
            return false;
        }

        LOGGER.info("[[cell data]]");
        int nArrays = cellData.GetNumberOfArrays();
        LOGGER.info("number of arrays: " + nArrays);
        for (int i = 0; i < nArrays; i++) {
            vtkDataArray array = cellData.GetArray(i);
            if (array == null) {
                LOGGER.warn("vtkCellData at index " + i + " is not a vtkDataArray.");
                VisNow.get().userMessageSend(null, "vtkCellData at index " + i + " is not a vtkDataArray.", "", Level.WARNING);
                continue;
            }
            int nComponents = array.GetNumberOfComponents();
            int nTuples = array.GetNumberOfTuples();
            final String name = array.GetName();

            LOGGER.info("[array " + i + ", name: " + name + "]");
            LOGGER.info("number of components: " + nComponents);
            LOGGER.info("number of tuples: " + nTuples);

            cellSet.addComponent(vtkDataArray2vnDataArray(array, name));
        }
        return true;
    }

    private static IrregularField vtkMultiBlock2vnField(vtkMultiBlockDataSet multiBlockDataSet) {
        if (multiBlockDataSet == null) {
            throw new IllegalArgumentException("Input argument cannot be null");
        }

        int nBlocks = multiBlockDataSet.GetNumberOfBlocks();

        IrregularField outField = null;

        for (int i = 0; i < nBlocks; i++) {
            vtkDataObject block = multiBlockDataSet.GetBlock(i);
            if (block == null) {
                LOGGER.warn("vtkMultiBlockDataSet at index " + i + " is not a vtkDataObject.");
                VisNow.get().userMessageSend(null, "vtkMultiBlockDataSet at index " + i + " is not a vtkDataObject.", "", Level.WARNING);
                continue;
            }
            final String blockType = block.GetClassName();
            LOGGER.info("block " + i + " class name: " + blockType);
            if ("vtkUnstructuredGrid".equals(blockType)) {
                vtkPointSet pointSet = (vtkPointSet) block;
                IrregularField field = vtkPointSet2vnField(pointSet);

                outField = MergeIrregularField.merge(outField, field, 0, true);
            } else {
                LOGGER.error("Sorry, this (" + blockType + ") type of block is not supported.");
            }
        }

        return outField;
    }

    private static float[] GetDataFromVtkDataArray(vtkDataArray array) {
        if (array == null) {
            throw new IllegalArgumentException("Input argument cannot be null");
        }
        int nComponents = array.GetNumberOfComponents();
        int nTuples = array.GetNumberOfTuples();

        float[] data = new float[nTuples * nComponents];
        for (int j = 0; j < nComponents; j++) {
            for (int k = 0; k < nTuples; k++) {
                data[k * nComponents + j] = (float) array.GetComponent(k, j);
            }
        }
        return data;
    }

    private static DataArray vtkDataArray2vnDataArray(vtkDataArray array, String name) {
        if (array == null) {
            throw new IllegalArgumentException("Input argument cannot be null");
        }
        if (array instanceof vtkUnsignedCharArray) {
            return DataArray.create(((vtkUnsignedCharArray) array).GetJavaArray(), array.GetNumberOfComponents(), name);
        } else if (array instanceof vtkShortArray) {
            return DataArray.create(((vtkShortArray) array).GetJavaArray(), array.GetNumberOfComponents(), name);
        } else if (array instanceof vtkIntArray) {
            return DataArray.create(((vtkIntArray) array).GetJavaArray(), array.GetNumberOfComponents(), name);
        } else if (array instanceof vtkLongArray) {
            return DataArray.create(((vtkLongArray) array).GetJavaArray(), array.GetNumberOfComponents(), name);
        } else if (array instanceof vtkFloatArray) {
            return DataArray.create(((vtkFloatArray) array).GetJavaArray(), array.GetNumberOfComponents(), name);
        } else if (array instanceof vtkDoubleArray) {
            return DataArray.create(((vtkDoubleArray) array).GetJavaArray(), array.GetNumberOfComponents(), name);
        } else {// fallback to float
            LOGGER.warn("data array " + array.GetClassName() + " not supported yet, converting to float");
            return DataArray.create(GetDataFromVtkDataArray(array), array.GetNumberOfComponents(), name);
        }
    }

    private static RegularField vtkRectilinearGrid2vnField(vtkRectilinearGrid grid) {
        if (grid == null) {
            throw new IllegalArgumentException("Input argument cannot be null");
        }
        int[] dims = grid.GetDimensions();
        LOGGER.info("[[rectlinear grid]]");
        LOGGER.info("dims: " + Arrays.toString(dims));

        RegularField field = new RegularField(dims);

        if (!LoadPointData(field, grid.GetPointData())) {
            return null;
        }

        return field;
    }

    private static void AddNullDataIfNone(Field field) {
        if (field == null) {
            throw new IllegalArgumentException("Input argument cannot be null");
        }
        if (field.getNComponents() > 0) // there is node data 
        {
            return;
        }
        if (field instanceof IrregularField) {
            for (CellSet cs : ((IrregularField) field).getCellSets()) {
                if (cs.getNComponents() > 0) // there is cell data
                {
                    return;
                }
            }
        }

        float[] data = new float[(int) field.getNNodes()];
        field.addComponent(DataArray.create(data, 1, "zeros"));
    }
}
