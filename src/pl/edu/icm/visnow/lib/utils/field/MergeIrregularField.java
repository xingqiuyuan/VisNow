///<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.field;

import java.util.Collection;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArrayType;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class MergeIrregularField
{
    /**
     * Creates a cell set merged from cell sets given as arguments merging their respective cell arrays
     * @param cellSet0: first merged cell set
     * @param cellSet1: second merged cell set
     * @param nodeOffset: 0 if cs1 and cs2 share the same set of nodes (merging two cell sets from the same field) 
     * <p>
     * if cellSet0 and cellSet1 come from different fields, nNodes must be set to the number of nodes of the field containing cs1;
     * Warning! there is no control on the nNodes parameter; There is no check for duplicate cells in the case of merging cell sets within a field.
     * @return merged cell set
     */
    public static CellSet add(CellSet cellSet0, CellSet cellSet1, int nodeOffset)
    {
        int[] nodes = null;
        byte[] or = null;
        CellSet out = new CellSet();
        out.setName(cellSet0.getName());
        for (int i = 0; i < cellSet0.getCellArrays().length; i++) {
            CellArray ca0 = cellSet0.getCellArrays()[i];
            CellArray ca1 = cellSet1.getCellArray(CellType.getType(i));
            if (ca0 != null && ca1 != null) {
                int[] nodes0 = ca0.getNodes();
                int[] nodes1 = ca1.getNodes();
                if (nodes0 == null)
                    nodes = nodes1;
                else if (nodes1 == null)
                    nodes = nodes0;
                else {
                    nodes = new int[nodes0.length + nodes1.length];
                    System.arraycopy(nodes0, 0, nodes, 0, nodes0.length);
                    if (nodeOffset == 0)
                        System.arraycopy(nodes1, 0, nodes, nodes0.length, nodes1.length);
                    else
                        for (int j = 0, k = nodes0.length; j < nodes1.length; j++, k++)
                            nodes[k] = nodes1[j] + nodeOffset;
                }
                byte[] or0 = ca0.getOrientations();
                byte[] or1 = ca1.getOrientations();
                if (or0 == null)
                    or = or1;
                else if (or1 == null)
                    or = or0;
                else {
                    or = new byte[or0.length + or1.length];
                    System.arraycopy(or0, 0, or, 0, or0.length);
                    System.arraycopy(or1, 0, or, or0.length, or1.length);
                }
                out.setCellArray(new CellArray(ca0.getType(), nodes, or, null));
            } 
            else if (ca1 != null) {
                if (nodeOffset == 0)
                    out.setCellArray(ca1.cloneDeep());
                else {
                    int[] nodes1 = ca1.getNodes();
                    nodes = nodes1.clone();
                    for (int j = 0; j < nodes1.length; j++)
                        nodes[j] += nodeOffset;
                    or = ca1.getOrientations().clone();
                    out.setCellArray(new CellArray(ca1.getType(), nodes, or, null));
                }
            }
            else if (ca0 != null) {
                out.setCellArray(ca0.cloneDeep());
            }
        }
        for (int i = 0; i < cellSet0.getBoundaryCellArrays().length; i++) {
            CellArray ca0 = cellSet0.getBoundaryCellArrays()[i];
            CellArray ca1 = cellSet1.getBoundaryCellArray(CellType.getType(i));
            if (ca0 == null && ca1 == null)
                continue;
            if (ca1 == null) {
                out.setCellArray(ca0.cloneDeep());
                continue;
            }
            if (ca0 == null) {
                int[] nodes1 = ca1.getNodes();
                nodes = nodes1.clone();
                for (int j = 0; j < nodes1.length; j++)
                    nodes[j] += nodeOffset;
                or = ca1.getOrientations().clone();
                out.setCellArray(new CellArray(ca1.getType(), nodes, or, null));
                continue;
            }
            int[] nodes0 = ca0.getNodes();
            int[] nodes1 = ca1.getNodes();
            if (nodes0 == null)
                nodes = nodes1;
            else if (nodes1 == null)
                nodes = nodes0;
            else {
                nodes = new int[nodes0.length + nodes1.length];
                System.arraycopy(nodes0, 0, nodes, 0, nodes0.length);
                for (int j = 0, k = nodes0.length; j < nodes1.length; j++, k++)
                    nodes[k] = nodes1[j] + nodeOffset;
            }
            byte[] or0 = ca0.getOrientations();
            byte[] or1 = ca1.getOrientations();
            if (or0 == null)
                or = or1;
            else if (or1 == null)
                or = or0;
            else {
                or = new byte[or0.length + or1.length];
                System.arraycopy(or0, 0, or, 0, or0.length);
                System.arraycopy(or1, 0, or, or0.length, or1.length);
            }
            out.setBoundaryCellArray(new CellArray(ca0.getType(), nodes, or, null));
        }
        return out;
    }
    
    public static final TimeData merge(TimeData timeData0, TimeData timeData1)
    {
        DataArrayType dType = timeData0.getType();
        if (dType != timeData1.getType())
            return null;
        LargeArrayType type = dType.toLargeArrayType();
        TimeData result = new TimeData(dType);
        long n0 = timeData0.length();
        long n1 = timeData1.length();
        long n = n0 + n1;
        float[] t0 = timeData0.getTimesAsArray();
        float[] t1 = timeData1.getTimesAsArray();
        for (int i = 0; i < t0.length; i++) {
            float t = t0[i];
            LargeArray val0 = timeData0.getValue(t);
            LargeArray val1 = timeData1.getValue(t);
            LargeArray val = LargeArrayUtils.create(type, n);
            LargeArrayUtils.arraycopy(val0, 0, val, 0, n0);
            LargeArrayUtils.arraycopy(val1, 0, val, n0, n1);
            result.setValue(val, t);
        }
        for (int i = 0; i < t0.length; i++) {
            float t = t1[i];
            if (timeData0.isTimestep(t))
                continue;
            LargeArray val0 = timeData0.getValue(t);
            LargeArray val1 = timeData1.getValue(t);
            LargeArray val = LargeArrayUtils.create(type, n);
            LargeArrayUtils.arraycopy(val0, 0, val, 0, n0);
            LargeArrayUtils.arraycopy(val1, 0, val, n0, n1);
            result.setValue(val, t);
        }
        return result;
    }
    
    /**
     * Merges two irregular fields
     * @param inField0: first merged field
     * @param inField1: second merged field
     * @param separate: if false, cell sets from inField0 and inField1 with equal names will be merged,
     * otherwise, they will form separate cell sets in the resulting field
     * @param mergeCounter: used as suffix to new cell set names (only if separate is true or merged fields have different cell set names); 
     * if merge is used to accumulate a series of fields, 
     * increase mergeCounter at each operation to ensure that different cell sets will have different names
     * @return merged field (nNodes is the sum of inField0.nNodes and inField1.nNodes, compatible components are merged,
     * non-compatible components are discarded
     * and cell sets merged or concatenated according to the separate parameter
     * Warning: only current data will be merged, thus merged field has no nontrivial time data
     */
    public static IrregularField merge(IrregularField inField0, IrregularField inField1, int mergeCounter, boolean separate)
    {
        return merge(inField0, inField1, mergeCounter, separate, false);
    }
    /**
     * Merges two irregular fields
     * @param inField0: first merged field
     * @param inField1: second merged field
     * @param separate: if false, cell sets from inField0 and inField1 with equal names will be merged,
     * otherwise, they will form separate cell sets in the resulting field
     * @param mergeCounter: used as suffix to new cell set names (only if separate is true or merged fields have different cell set names); 
     * if merge is used to accumulate a series of fields, 
     * increase mergeCounter at each operation to ensure that different cell sets will have different names
     * @param mergeTimeData if true, timelines of node components in both merged fields are merged
     * @return merged field (nNodes is the sum of inField0.nNodes and inField1.nNodes, compatible components are merged,
     * non-compatible components are discarded
     * and cell sets merged or concatenated according to the separate parameter
     */
    public static IrregularField merge(IrregularField inField0, IrregularField inField1, int mergeCounter, boolean separate, boolean mergeTimeData)
    {
        if ((inField0 == null || inField0.getNNodes() == 0) && (inField1 != null && inField1.getNNodes() != 0))
            return inField1.cloneShallow();
        if ((inField0 != null && inField0.getNNodes() != 0) && (inField1 == null || inField1.getNNodes() == 0))
            return inField0.cloneShallow();
        if ((inField0 == null || inField0.getNNodes() == 0) && (inField1 == null || inField1.getNNodes() == 0))
            return null;
        int nNodes1 = (int) inField1.getNNodes();
        int nNodes0 = (int) inField0.getNNodes();
        int nNodes = nNodes1 + nNodes0;
        IrregularField outField = new IrregularField(nNodes);
        if (mergeTimeData) {
            outField.setCoords(merge(inField0.getCoords(), inField1.getCoords()));
            for (int i = 0; i < inField0.getNComponents(); i++) {
                DataArray inDA0 = inField0.getComponent(i);
                int vlen = inDA0.getVectorLength();
                String name = inDA0.getName();
                DataArray inDA1 = inField1.getComponent(name);
                if (inDA1 == null || inDA1.getType() != inDA0.getType() || inDA1.getVectorLength() != vlen)
                    continue;
                double min  = Math.min(inDA0.getPreferredMinValue(), inDA1.getPreferredMinValue());
                double pmin = Math.min(inDA0.getPreferredPhysMinValue(), inDA1.getPreferredPhysMinValue());
                double max  = Math.max(inDA0.getPreferredMaxValue(), inDA1.getPreferredMaxValue());
                double pmax = Math.max(inDA0.getPreferredPhysMaxValue(), inDA1.getPreferredPhysMaxValue());
                outField.addComponent(DataArray.create(merge(inDA0.getTimeData(), inDA1.getTimeData()), 
                                                       vlen, inDA0.getName(), inDA0.getUnit(), inDA0.getUserData()).
                                      preferredRanges(min, max, pmin, pmax));
            }
        }
        else {
            float[] coords = new float[3 * nNodes];
            System.arraycopy(inField0.getCurrentCoords().getData(), 0, coords, 0, 3 * nNodes0);
            System.arraycopy(inField1.getCurrentCoords().getData(), 0, coords, 3 * nNodes0, 3 * nNodes1);
            outField.setCurrentCoords(new FloatLargeArray(coords));
            for (int i = 0; i < inField0.getNComponents(); i++) {
                DataArray inDA0 = inField0.getComponent(i);
                int vlen = inDA0.getVectorLength();
                String name = inDA0.getName();
                DataArray inDA1 = inField1.getComponent(name);
                if (inDA1 == null || inDA1.getType() != inDA0.getType() || inDA1.getVectorLength() != vlen)
                    continue;
                LargeArray outB = LargeArrayUtils.create(inDA1.getRawArray().getType(), vlen * nNodes, false);
                LargeArrayUtils.arraycopy(inDA0.getRawArray(), 0, outB, 0, vlen * nNodes0);
                LargeArrayUtils.arraycopy(inDA1.getRawArray(), 0, outB, vlen * nNodes0, vlen * nNodes1);
                outField.addComponent(DataArray.create(outB, vlen, inDA0.getName(), inDA0.getUnit(), inDA0.getUserData()));
            }
        }
        if (outField.getNComponents() == 0) {
            int[] dummy = new int[nNodes];
            for (int i = 0; i < nNodes0; i++) 
                dummy[i] = 0;
            for (int i = nNodes0; i < nNodes1; i++) 
                dummy[i] = 1;
            outField.addComponent(DataArray.create(dummy, 1, "dummy"));
        }
        boolean sep = separate;
        if (inField0.getNCellSets() != inField1.getNCellSets())
            sep = true;
        if (sep) {
            for (CellSet cs : inField0.getCellSets())
                outField.addCellSet(cs);
            for (CellSet cs : inField1.getCellSets()) {
                CellSet ncs = new CellSet();
                ncs.setName(cs.getName() + mergeCounter);
                ncs.setSelected(true);
                for (int i = 0; i < cs.getNComponents(); i++)
                    ncs.addComponent(cs.getComponent(i).cloneShallow());
                for (int i = 0; i < cs.getBoundaryCellArrays().length; i++) {
                    if (cs.getBoundaryCellArray(CellType.getType(i)) == null)
                        continue;
                    int[] inNds = cs.getBoundaryCellArray(CellType.getType(i)).getNodes();
                    int[] outNds = new int[inNds.length];
                    for (int j = 0; j < outNds.length; j++)
                        outNds[j] = inNds[j] + nNodes0;
                    byte[] inOr = cs.getBoundaryCellArray(CellType.getType(i)).getOrientations();
                    byte[] outOr = new byte[inOr.length];
                    System.arraycopy(inOr, 0, outOr, 0, inOr.length);
                    int[] inIds = cs.getBoundaryCellArray(CellType.getType(i)).getDataIndices();
                    int[] outIds = null;
                    if (inIds != null) {
                        outIds = new int[inIds.length];
                        System.arraycopy(inIds, 0, outIds, 0, inIds.length);
                    }
                    ncs.setBoundaryCellArray(new CellArray(cs.getBoundaryCellArray(CellType.getType(i)).getType(), outNds, outOr, outIds));
                }
                for (int i = 0; i < cs.getCellArrays().length; i++) {
                    if (cs.getCellArray(CellType.getType(i)) == null)
                        continue;
                    int[] inNds = cs.getCellArray(CellType.getType(i)).getNodes();
                    int[] outNds = new int[inNds.length];
                    for (int j = 0; j < outNds.length; j++)
                        outNds[j] = inNds[j] + nNodes0;
                    byte[] inOr = cs.getCellArray(CellType.getType(i)).getOrientations();
                    byte[] outOr = new byte[inOr.length];
                    System.arraycopy(inOr, 0, outOr, 0, inOr.length);
                    int[] inIds = cs.getCellArray(CellType.getType(i)).getDataIndices();
                    int[] outIds = null;
                    if (inIds != null) {
                        outIds = new int[inIds.length];
                        System.arraycopy(inIds, 0, outIds, 0, inIds.length);
                    }
                    ncs.setCellArray(new CellArray(cs.getCellArray(CellType.getType(i)).getType(), outNds, outOr, outIds));
                }
                outField.addCellSet(ncs);
            }
        } else {
            for (int i = 0; i < inField0.getNCellSets(); i++)
                outField.addCellSet(add(inField0.getCellSet(i), inField1.getCellSet(i), (int) inField0.getNNodes()));
        }
        return outField;
    }
    
    public static IrregularField merge(Collection<IrregularField> inFields, boolean separate)
    {
        IrregularField out = null;
        int mergeCounter = 0;
        for (IrregularField inField : inFields) {
            out = merge(out, inField, mergeCounter, separate);
            mergeCounter += 1;
        }
        return out;
    }

    private MergeIrregularField()
    {
    }
}
