//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>


package pl.edu.icm.visnow.lib.utils.field;

import java.util.Arrays;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class RegularFieldToIrregularField {
    public static final IrregularField createIrregular(RegularField inField)
    {        int[] dims = inField.getDims();
        int[] offsets = null;
        switch (dims.length) {
            case 1:
                offsets = new int[]{0, 1};
                break;
            case 2:
                offsets = new int[]{0, 1, dims[0] + 1, dims[0]};
                break;
            case 3:

                offsets = new int[]{0, 1, dims[0] + 1, dims[0],
                                    dims[0] * dims[1], dims[0] * dims[1] + 1, dims[0] * dims[1] + dims[0] + 1, dims[0] * dims[1] + dims[0]};
                break;
        }

        int nNodes = (int) inField.getNNodes();
        int nOutNodes = nNodes;
        FloatLargeArray coords;
        boolean isMask = inField.hasMask();
        int[] indices = null;

        CellSet cs = new CellSet("cells");
        if (isMask) {
            LogicLargeArray mask = inField.getCurrentMask();
            indices = new int[nNodes];
            nOutNodes = 0;
            Arrays.fill(indices, -1);
            for (int i = 0; i < nNodes; i++)
                if (mask.getBoolean(i)) {
                    indices[i] = nOutNodes;
                    nOutNodes += 1;
                }
            coords = new FloatLargeArray(3 * (long)nOutNodes, false);
            if (inField.getCurrentCoords() != null) {
                float[] inCoords = inField.getCurrentCoords().getData();
                for (int i = 0, j = 0; i < nNodes; i++)
                    if (mask.getBoolean(i)) {
                        LargeArrayUtils.arraycopy(inCoords, 3 * i, coords, 3 * j, 3);
                        j += 1;
                    }
            } else {
                float[][] affine = inField.getAffine();
                switch (dims.length) {
                    case 1:
                        for (long k = 0, l = 0, ii = 0; k < dims[0]; k++, ii++)
                            if (mask.getBoolean(ii))
                                for (int m = 0; m < 3; m++, l++)
                                    coords.setFloat(l, affine[3][m] + k * affine[0][m]);
                        break;
                    case 2:
                        for (long j = 0, l = 0, ii = 0; j < dims[1]; j++)
                            for (int k = 0; k < dims[0]; k++, ii++)
                                if (mask.getBoolean(ii))
                                    for (int m = 0; m < 3; m++, l++)
                                        coords.setFloat(l, affine[3][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                    case 3:
                        for (long i = 0, l = 0, ii = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0; k < dims[0]; k++, ii++)
                                    if (mask.getBoolean(ii))
                                        for (int m = 0; m < 3; m++, l++)
                                            coords.setFloat(l, affine[3][m] + i * affine[2][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                }
            }

            int[] outNodes = null;
            int[] tmpOutNodes = null;
            int nOutCells = 0;
            switch (dims.length) {
                case 1:
                    tmpOutNodes = new int[2 * (dims[0] - 1)];
                    for (int k = 0, l = 0; k < dims[0] - 1; k++)
                        if (indices[k] >= 0 && indices[k + 1] >= 0) {
                            tmpOutNodes[l] = indices[k];
                            tmpOutNodes[l + 1] = indices[k + 1];
                            l += 2;
                            nOutCells += 1;
                        }
                    outNodes = new int[2 * nOutCells];
                    System.arraycopy(tmpOutNodes, 0, outNodes, 0, outNodes.length);
                    cs.addCells(new CellArray(CellType.SEGMENT, outNodes, null, null));
                    break;
                case 2:
                    tmpOutNodes = new int[4 * (dims[1] - 1) * (dims[0] - 1)];
                    for (int j = 0, l = 0; j < dims[1] - 1; j++)
                        for (int k = 0, m = j * dims[0]; k < dims[0] - 1; k++, m++)
                            if (indices[m] >= 0 && indices[m + 1] >= 0 &&
                                indices[m + dims[0]] >= 0 && indices[m + dims[0] + 1] >= 0) {
                                tmpOutNodes[l] = indices[m];
                                tmpOutNodes[l + 1] = indices[m + 1];
                                tmpOutNodes[l + 2] = indices[m + 1 + dims[0]];
                                tmpOutNodes[l + 3] = indices[m + dims[0]];
                                l += 4;
                                nOutCells += 1;
                            }
                    outNodes = new int[4 * nOutCells];
                    System.arraycopy(tmpOutNodes, 0, outNodes, 0, outNodes.length);
                    cs.addCells(new CellArray(CellType.QUAD, outNodes, null, null));
                    break;
                case 3:
                    tmpOutNodes = new int[8 * (dims[2] - 1) * (dims[1] - 1) * (dims[0] - 1)];
                    for (int i = 0, l = 0; i < dims[2] - 1; i++)
                        for (int j = 0; j < dims[1] - 1; j++)
                            for (int k = 0, m = (i * dims[1] + j) * dims[0]; k < dims[0] - 1; k++, m++)
                                if (indices[m] >= 0 && indices[m + 1] >= 0 &&
                                    indices[m + dims[0]] >= 0 && indices[m + dims[0] + 1] >= 0 &&
                                    indices[m + dims[0] * dims[1]] >= 0 &&
                                    indices[m + dims[0] * dims[1] + 1] >= 0 &&
                                    indices[m + dims[0] * dims[1] + dims[0]] >= 0 &&
                                    indices[m + dims[0] * dims[1] + dims[0] + 1] >= 0) {
                                    tmpOutNodes[l] = indices[m];
                                    tmpOutNodes[l + 1] = indices[m + 1];
                                    tmpOutNodes[l + 2] = indices[m + 1 + dims[0]];
                                    tmpOutNodes[l + 3] = indices[m + dims[0]];
                                    tmpOutNodes[l + 4] = indices[m + dims[0] * dims[1]];
                                    tmpOutNodes[l + 5] = indices[m + 1 + dims[0] * dims[1]];
                                    tmpOutNodes[l + 6] = indices[m + 1 + dims[0] + dims[0] * dims[1]];
                                    tmpOutNodes[l + 7] = indices[m + dims[0] + dims[0] * dims[1]];
                                    l += 8;
                                    nOutCells += 1;
                                }
                    outNodes = new int[8 * nOutCells];
                    System.arraycopy(tmpOutNodes, 0, outNodes, 0, outNodes.length);
                    cs.addCells(new CellArray(CellType.HEXAHEDRON, outNodes, null, null));
                    break;
            }
        } else {
            nOutNodes = nNodes;
            if (inField.getCurrentCoords() != null)
                coords = inField.getCurrentCoords();
            else {
                coords = new FloatLargeArray(3 * (long)nOutNodes, false);
                float[][] affine = inField.getAffine();
                switch (dims.length) {
                    case 1:
                        for (long k = 0, l = 0; k < dims[0]; k++)
                            for (int m = 0; m < 3; m++, l++)
                                coords.setFloat(l, affine[3][m] + k * affine[0][m]);
                        break;
                    case 2:
                        for (long j = 0, l = 0; j < dims[1]; j++)
                            for (int k = 0; k < dims[0]; k++)
                                for (int m = 0; m < 3; m++, l++)
                                    coords.setFloat(l, affine[3][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                    case 3:
                        for (long i = 0, l = 0; i < dims[2]; i++)
                            for (int j = 0; j < dims[1]; j++)
                                for (int k = 0; k < dims[0]; k++)
                                    for (int m = 0; m < 3; m++, l++)
                                        coords.setFloat(l, affine[3][m] + i * affine[2][m] + j * affine[1][m] + k * affine[0][m]);
                        break;
                }
            }

            int[] outNodes = null;
            byte[] orientations = null;
            switch (dims.length) {
                case 1:
                    outNodes = new int[2 * (dims[0] - 1)];
                    orientations = new byte[dims[0] - 1];
                    for (int k = 0, l = 0, m = 0; k < dims[0] - 1; k++, m++) {
                        outNodes[l] = k;
                        outNodes[l + 1] = outNodes[l] + 1;
                        l += 2;
                        orientations[m] = 1;
                    }
                    cs.addCells(new CellArray(CellType.SEGMENT, outNodes, orientations, null));
                    break;
                case 2:
                    outNodes = new int[4 * (dims[1] - 1) * (dims[0] - 1)];
                    orientations = new byte[(dims[1] - 1) * (dims[0] - 1)];
                    for (int j = 0, l = 0, m = 0; j < dims[1] - 1; j++)
                        for (int k = 0; k < dims[0] - 1; k++, m++) {
                            for (int p = 0, q = j * dims[0] + k; p < 4; p++, l++)
                                outNodes[l] = q + offsets[p];
                            orientations[m] = 1;
                        }
                    cs.addCells(new CellArray(CellType.QUAD, outNodes, orientations, null));
                    break;
                case 3:
                    outNodes = new int[8 * (dims[2] - 1) * (dims[1] - 1) * (dims[0] - 1)];
                    orientations = new byte[(dims[2] - 1) * (dims[1] - 1) * (dims[0] - 1)];
                    for (int i = 0, l = 0, m = 0; i < dims[2] - 1; i++)
                        for (int j = 0; j < dims[1] - 1; j++)
                            for (int k = 0; k < dims[0] - 1; k++, m++) {
                                for (int p = 0, q = (i * dims[1] + j) * dims[0] + k; p < 8; p++, l++)
                                    outNodes[l] = q + offsets[p];
                                orientations[m] = 1;
                            }
                    cs.addCells(new CellArray(CellType.HEXAHEDRON, outNodes, orientations, null));
                    break;
            }
        }
        IrregularField outIrregularField = new IrregularField(nOutNodes);
        outIrregularField.setCurrentCoords(coords);
        cs.generateDisplayData(coords);
        outIrregularField.addCellSet(cs);
        if (isMask) {
            for (DataArray inDa : inField.getComponents()) {
                int vlen = inDa.getVectorLength();
                LargeArray outB = LargeArrayUtils.create(inDa.getRawArray().getType(), vlen * nOutNodes);
                for (int i = 0, j = 0; i < nNodes; i++)
                    if (indices[i] >= 0) {
                        LargeArrayUtils.arraycopy(inDa.getRawArray(), vlen * i, outB, vlen * j, vlen);
                        j += 1;
                    }
                DataArray da = DataArray.create(outB, vlen, inDa.getName()).unit(inDa.getUnit()).userData(inDa.getUserData());
                da.setPreferredRanges(inDa.getPreferredMinValue(), inDa.getPreferredMaxValue(), inDa.getPreferredPhysMinValue(), inDa.getPreferredPhysMaxValue());
                outIrregularField.addComponent(da);
            }
        } else
            for (DataArray inDa : inField.getComponents())
                outIrregularField.addComponent(inDa.cloneShallow());
        return outIrregularField;
    }

    private RegularFieldToIrregularField()
    {
    }
    
}
