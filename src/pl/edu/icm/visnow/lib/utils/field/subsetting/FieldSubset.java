//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.field.subsetting;

import java.util.ArrayList;
import java.util.Arrays;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams;
import pl.edu.icm.visnow.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import static pl.edu.icm.visnow.lib.utils.field.subsetting.GeometricSubsetParams.Depth.*;
import static pl.edu.icm.visnow.lib.utils.field.subsetting.GeometricSubsetParams.Position.*;
import pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling)
 */
public class FieldSubset
{

    private static LogicLargeArray generateMask(GeometricSubsetParams params, long nNodes, FloatLargeArray fieldCoords)
    {
        InteractiveGlyphParams geometryParams = params.getGeometryParams();
        LogicLargeArray mask = new LogicLargeArray(nNodes);
        float[] center = geometryParams.getCenter();
        switch (params.getSubsetGeometry()) {
            case SPHERE:
            case CIRCLE:
                float r2 = geometryParams.getRadius() * geometryParams.getRadius();
                for (long i = 0; i < mask.length(); i++) {
                    float r = 0;
                    for (int j = 0; j < 3; j++) {
                        float f = fieldCoords.getFloat(3 * i + j) - center[j];
                        r += f * f;
                    }
                    mask.setBoolean(i, r <= r2);
                }
                break;
            case BOX:
            case RECTANGLE:
                float rU = geometryParams.getuScale();
                float rV = geometryParams.getvScale();
                float rW = geometryParams.getwScale();
                float[] u = geometryParams.getU();
                float[] v = geometryParams.getV();
                float[] w = geometryParams.getW();
                for (long i = 0; i < mask.length(); i++) {
                    float ru = 0, rv = 0, rw = 0;
                    for (int j = 0; j < 3; j++) {
                        float f = fieldCoords.getFloat(3 * i + j) - center[j];
                        ru += f * u[j];
                        rv += f * v[j];
                        rw += f * w[j];
                    }
                    mask.setBoolean(i, Math.abs(ru) <= rU && Math.abs(rv) <= rV && Math.abs(rw) <= rW);
                }
                break;
        }
        return mask;
    }

    private static LogicLargeArray generateMask(GeometricSubsetParams params, Field field)
    {
        ComponentSubrange range = params.getSubrange();
        LogicLargeArray mask = new LogicLargeArray(field.getNNodes());
        LargeArray inD = field.getComponent(range.getComponentName()).getRawArray(0);
        int vlen = field.getComponent(range.getComponentName()).getVectorLength();
        double low = range.getLow();
        double up  = range.getUp();
        if (vlen == 1)
            for (long j = 0; j < mask.length(); j++)
                mask.setByte(j,inD.getDouble(j) < low || inD.getDouble(j) > up ? (byte)0 : (byte)1);
        else {
            double ll = low * low;
            double uu = up * up;
            float[] v = new float[vlen];
            for (long i = 0; i < mask.length(); i++) {
                inD.getFloatData(v, vlen * i, vlen * (i + 1), vlen);
                double vn = 0;
                for (int j = 0; j < v.length; j++)
                    vn += v[j] * v[j];
                mask.setByte(i, vn < ll || vn > uu ? (byte)0 : (byte)1);
            }
        }
        return mask;
    }

    private static LogicLargeArray generateMask(GeometricSubsetParams params, int[] dims, float[][] affine)
    {
        long nNodes = 1;
        for (int i = 0; i < dims.length; i++) 
            nNodes *= dims[i];
        InteractiveGlyphParams geometryParams = params.getGeometryParams();
        LogicLargeArray mask = new LogicLargeArray(nNodes);
        float[] center = geometryParams.getCenter();
        float[] p2 = new float[3];
        switch (params.getSubsetGeometry()) {
            case SPHERE:
            case CIRCLE:
                float r2 = geometryParams.getRadius() * geometryParams.getRadius();
                for (int l = 0; l < 3; l++)
                    p2[l] = affine[3][l] - center[l];
                switch (dims.length) {
                case 3:
                    for (int i = 0, m = 0; i < dims[2]; i++) {
                        float[] p1 = new float[3];
                        System.arraycopy(p2, 0, p1, 0, 3);
                        for (int j = 0; j < dims[1]; j++) {
                            float[] p0 = new float[3];
                            System.arraycopy(p1, 0, p0, 0, 3);
                            for (int k = 0; k < dims[0]; k++, m++) {
                                float r = 0;
                                for (int l = 0; l < 3; l++)
                                    r += p0[l] * p0[l];
                                mask.setBoolean(m, r < r2);
                                for (int l = 0; l < 3; l++)
                                    p0[l] += affine[0][l];
                            }
                            for (int l = 0; l < 3; l++)
                                p1[l] += affine[1][l];
                        }
                        for (int l = 0; l < 3; l++)
                            p2[l] += affine[2][l];
                    }
                    break;
                case 2:
                    float[] p1 = new float[3];
                    System.arraycopy(p2, 0, p1, 0, 3);
                    for (int j = 0, m = 0; j < dims[1]; j++) {
                        float[] p0 = new float[3];
                        System.arraycopy(p1, 0, p0, 0, 3);
                        for (int k = 0; k < dims[0]; k++, m++) {
                            float r = 0;
                            for (int l = 0; l < 3; l++)
                                r += p0[l] * p0[l];
                            mask.setBoolean(m, r < r2);
                            for (int l = 0; l < 3; l++)
                                p0[l] += affine[0][l];
                        }
                        for (int l = 0; l < 3; l++)
                            p1[l] += affine[1][l];
                    }
                    break;
                case 1:
                    float[] p0 = new float[3];
                    System.arraycopy(p2, 0, p0, 0, 3);
                    for (int k = 0, m = 0; k < dims[0]; k++, m++) {
                        float r = 0;
                        for (int l = 0; l < 3; l++)
                            r += p0[l] * p0[l];
                        mask.setBoolean(m, r < r2);
                        for (int l = 0; l < 3; l++)
                            p0[l] += affine[0][l];
                    }
                    break;
                }
                break;
            case BOX:
            case RECTANGLE:
                float rU = geometryParams.getuScale();
                float rV = geometryParams.getvScale();
                float rW = geometryParams.getwScale();
                float[] u = geometryParams.getU();
                float[] v = geometryParams.getV();
                float[] w = geometryParams.getW();
                for (int l = 0; l < 3; l++)
                    p2[l] = affine[3][l] - center[l];
                switch (dims.length) {
                case 3:
                    for (int i = 0, m = 0; i < dims[2]; i++) {
                        float[] p1 = new float[3];
                        System.arraycopy(p2, 0, p1, 0, 3);
                        for (int j = 0; j < dims[1]; j++) {
                            float[] p0 = new float[3];
                            System.arraycopy(p1, 0, p0, 0, 3);
                            for (int k = 0; k < dims[0]; k++, m++) {
                                float ru = 0, rv = 0, rw = 0;
                                for (int l = 0; l < 3; l++) {
                                    ru += p0[l] * u[l];
                                    rv += p0[l] * v[l];
                                    rw += p0[l] * w[l];
                                }
                                mask.setBoolean(m, Math.abs(ru) <= rU
                                        && Math.abs(rv) <= rV
                                        && Math.abs(rw) <= rW);
                                for (int l = 0; l < 3; l++)
                                    p0[l] += affine[0][l];
                            }
                            for (int l = 0; l < 3; l++)
                                p1[l] += affine[1][l];
                        }
                        for (int l = 0; l < 3; l++)
                            p2[l] += affine[2][l];
                    }
                    break;
                case 2:
                    float[] p1 = new float[3];
                    System.arraycopy(p2, 0, p1, 0, 3);
                    for (int j = 0, m = 0; j < dims[1]; j++) {
                        float[] p0 = new float[3];
                        System.arraycopy(p1, 0, p0, 0, 3);
                        for (int k = 0; k < dims[0]; k++, m++) {
                            float ru = 0, rv = 0, rw = 0;
                            for (int l = 0; l < 3; l++) {
                                ru += p0[l] * u[l];
                                rv += p0[l] * v[l];
                                rw += p0[l] * w[l];
                            }
                            mask.setBoolean(m, Math.abs(ru) <= rU
                                    && Math.abs(rv) <= rV
                                    && Math.abs(rw) <= rW);
                            for (int l = 0; l < 3; l++)
                                p0[l] += affine[0][l];
                        }
                        for (int l = 0; l < 3; l++)
                            p1[l] += affine[1][l];
                    }
                    break;
                case 1:
                    float[] p0 = new float[3];
                    System.arraycopy(p2, 0, p0, 0, 3);
                    for (int k = 0, m = 0; k < dims[0]; k++, m++) {
                        float ru = 0, rv = 0, rw = 0;
                        for (int l = 0; l < 3; l++) {
                            ru += p0[l] * u[l];
                            rv += p0[l] * v[l];
                            rw += p0[l] * w[l];
                        }
                        mask.setBoolean(m, Math.abs(ru) <= rU
                                && Math.abs(rv) <= rV
                                && Math.abs(rw) <= rW);
                        for (int l = 0; l < 3; l++)
                            p0[l] += affine[0][l];
                    }
                    break;
                }
                break;
            default:
        }
        return mask;
    }
    
    private static boolean cellPosition(GeometricSubsetParams params, boolean isIn, boolean isOut)
    {
        boolean addCell = false;
        switch (params.getSubsetType()) {
            case FULLY_IN:
                if (!isOut)
                    addCell = true;
                break;
            case IN:
                if (isIn)
                    addCell = true;
                break;
            case BOUNDARY:
            case BOUNDARY_SURFACE:
                if (isIn && isOut)
                    addCell = true;
                break;
            case OUT:
                if (isOut)
                    addCell = true;
                break;
            case FULLY_OUT:
                if (!isIn)
                    addCell = true;
                break;
        }
        return addCell;
    }

    private static boolean cellStatus(GeometricSubsetParams params, int[] offsets, LogicLargeArray mask, long m)
    {
        boolean isIn = false, isOut = false;
        for (int offset : offsets)
            if (mask.getBoolean(m + offset))
                isIn = true;
            else
                isOut = true;
        return cellPosition(params, isIn, isOut);
    }

    private static boolean cellStatus(GeometricSubsetParams params, LogicLargeArray mask, int m, int nCellNodes, int[] nodes)
    {
        boolean isIn = false, isOut = false;
        for (int i = m; i < m + nCellNodes; i++)
            if (mask.getBoolean(nodes[i]))
                isIn = true;
            else
                isOut = true;
        return cellPosition(params, isIn, isOut);
    }
    
    public static final LargeArray select(LargeArray in, int vlen, LogicLargeArray mask)
    {
        if (mask == null)
            return in;
        if (in == null || in.length() != vlen * mask.length())
            return null;
        long nOut = 0;
        for (long i = 0; i < mask.length(); i++)
            if (mask.getBoolean(i))
                nOut += 1;
        LargeArray out = LargeArrayUtils.create(in.getType(), vlen * nOut);
        for (long i = 0, l = 0; i < mask.length(); i++)
            if (mask.getBoolean(i)) {
                LargeArrayUtils.arraycopy(in, i * vlen, out, l * vlen, vlen);
                l += 1;
            }
        return out;
    }
    
    public static final TimeData select(TimeData inData, int vlen, int nOutNodes, LogicLargeArray mask)
    {
        float[] inTimes = inData.getTimesAsArray();
        TimeData outTimeData = new TimeData(inData.getType());
        for (int i = 0; i < inTimes.length; i++) 
            outTimeData.setValue(select(inData.getValues().get(i), vlen, mask), inTimes[i]);
        return outTimeData;
    }
    
    public static final DataArray select(DataArray inData, int nOutNodes, LogicLargeArray mask)
    {
        int vlen = inData.getVectorLength();
        return DataArray.create(select(inData.getTimeData(), vlen, nOutNodes, mask), 
                                vlen, inData.getName(), inData.getUnit(), inData.getUserData()).
                                preferredRanges(inData.getPreferredMinValue(),     inData.getPreferredMaxValue(), 
                                                inData.getPreferredPhysMinValue(), inData.getPreferredPhysMaxValue());
    }
    
    private static IrregularField createSubset(RegularField inField, 
                                               int nOutNodes, LogicLargeArray mask, 
                                               long nCells, LogicLargeArray cellMask, 
                                               IntLargeArray reindex)
    {
        int[] dims = inField.getDims();
        int cellSize = 1 << dims.length;
        int[] offsets = inField.getCellNodeOffsets();
        if (nOutNodes < 1)
            return null;
        IrregularField out = new IrregularField(nOutNodes);
        if (inField.hasCoords()) 
            out.setCoords(select(inField.getCoords(), 3, nOutNodes, mask));
        else {
            float[][] affine = inField.getAffine();
            long l = 0, m = 0;
            FloatLargeArray outCoords = new FloatLargeArray(3 * nOutNodes);
            switch (dims.length) {
            case 3:
                for (int i = 0;  i < dims[2]; i++)
                    for (int j = 0; j < dims[1]; j++) 
                        for (int k = 0; k < dims[0]; k++, l++) 
                            if (mask.getBoolean(l)) {
                                float[] p = new float[3];
                                for (int n = 0; n < 3; n++) 
                                    p[n] = affine[3][n] + 
                                       i * affine[2][n] + j * affine[1][n] + k * affine[0][n];
                                LargeArrayUtils.arraycopy(p, 0, outCoords, m, 3);
                                m += 3;
                            }
                break;
            case 2:
                for (int j = 0; j < dims[1]; j++) 
                    for (int k = 0; k < dims[0]; k++, l++) 
                        if (mask.getBoolean(l)) {
                            float[] p = new float[3];
                            for (int n = 0; n < 3; n++) 
                                p[n] = affine[3][n] + j * affine[1][n] + k * affine[0][n];
                            LargeArrayUtils.arraycopy(p, 0, outCoords, m, 3);
                            m += 3;
                        }
                break;
            case 1:
                for (int k = 0; k < dims[0]; k++, l++) 
                    if (mask.getBoolean(l)) {
                        float[] p = new float[3];
                        for (int n = 0; n < 3; n++) 
                            p[n] = affine[3][n] + k * affine[0][n];
                        LargeArrayUtils.arraycopy(p, 0, outCoords, m, 3);
                        m += 3;
                    }
                break;
            }
            out.setCoords(outCoords, 0);
        }
        for (DataArray da : inField.getComponents()) 
            out.addComponent(select(da, nOutNodes, mask));
        float[] indices = new float[dims.length * nOutNodes];
        switch (dims.length) {
        case 3:
            for (int i = 0, l = 0, m = 0;  i < dims[2]; i++)
                for (int j = 0; j < dims[1]; j++) 
                    for (int k = 0; k < dims[0]; k++, l++) 
                        if (mask.getBoolean(l)) {
                            indices[m]     = k;
                            indices[m + 1] = j;
                            indices[m + 2] = i;
                            m += 3;
                        }
            break;
        case 2:
            for (int j = 0, l = 0, m = 0; j < dims[1]; j++) 
                for (int k = 0; k < dims[0]; k++, l++) 
                    if (mask.getBoolean(l)) {
                        indices[m]     = k;
                        indices[m + 1] = j;
                        m += 2;
                    }
            break;
        case 1:
            for (int k = 0, l = 0, m = 0; k < dims[0]; k++, l++) 
                if (mask.getBoolean(l)) {
                    indices[m]     = k;
                    m += 1;
                }
            break;
        }
        out.addComponent(DataArray.create(indices, dims.length, SubsetGeometryComponents.INDEX_COORDS));
        int[] nodes = new int[cellSize * (int)nCells];
        int l = 0, m = 0;
        CellType cellType = CellType.HEXAHEDRON;
        switch (dims.length) {
        case 3:
            for (int i = 0;  i < dims[2]; i++)
                for (int j = 0; j < dims[1]; j++) 
                    for (int k = 0; k < dims[0]; k++, l++) 
                        if (cellMask.getBoolean(l)) 
                            for (int n = 0; n < cellSize; n++, m++)
                                nodes[m] = reindex.getInt(l + offsets[n]);
            break;
        case 2:
            cellType = CellType.QUAD;
            for (int j = 0; j < dims[1]; j++) 
                for (int k = 0; k < dims[0]; k++, l++) 
                    if (cellMask.getBoolean(l)) 
                        for (int n = 0; n < cellSize; n++, m++)
                            nodes[m] = reindex.getInt(l + offsets[n]);
            break;
        case 1:
            cellType = CellType.SEGMENT;
            for (int k = 0; k < dims[0]; k++, l++) 
                if (cellMask.getBoolean(l)) 
                    for (int n = 0; n < cellSize; n++, m++)
                        nodes[m] = reindex.getInt(l + offsets[n]);
            break;
        }
        byte[] orientations = new byte[(int)nCells];
        Arrays.fill(orientations, (byte)1);
        CellArray cells = new CellArray(cellType, nodes, orientations, null);
        CellSet cs = new CellSet("regular_field_cells");
        cs.addCells(cells);
        out.addCellSet(cs);
        cs.generateDisplayData(out.getCoords(0));
        return out;
    }
    
    private static Field createSubset(RegularField inField, GeometricSubsetParams params)
    {
        LogicLargeArray mask;
        int[] dims = inField.getDims();
        int cellSize = 1 << dims.length;
        int[] offsets = inField.getCellNodeOffsets();
        if (params.isGeometricCrop()) {
            if (inField.hasCoords())
                mask = generateMask(params, inField.getNNodes(), inField.getCoords(0));
            else 
                mask = generateMask(params, dims, inField.getAffine());
        }
        else
            mask = generateMask(params, inField);
        LogicLargeArray cellMask = new LogicLargeArray(inField.getNNodes());
        long l = 0;
        switch (dims.length) {
        case 3:
            for (int i = 0;  i < dims[2]; i++)
                for (int j = 0; j < dims[1]; j++) 
                    for (int k = 0; k < dims[0]; k++, l++) 
                        if (i == dims[2] - 1 || j == dims[1] - 1 || k == dims[0] - 1) 
                            cellMask.setBoolean(l, false);
                        else
                            cellMask.setBoolean(l, cellStatus(params, offsets, mask, l));
            break;
        case 2:
            for (int j = 0; j < dims[1]; j++) 
                for (int k = 0; k < dims[0]; k++, l++) 
                    if (j == dims[1] - 1 || k == dims[0] - 1) 
                        cellMask.setBoolean(l, false);
                    else
                        cellMask.setBoolean(l, cellStatus(params, offsets, mask, l));
            break;
        case 1:
            for (int k = 0; k < dims[0]; k++, l++) 
                if (k == dims[0] - 1) 
                    cellMask.setBoolean(l, false);
                else
                    cellMask.setBoolean(l, cellStatus(params, offsets, mask, l));
            break;
        }
        if (params.getProcessingDepth() == MASK) {
            for (long i = 0; i < mask.length(); i++)
                mask.setBoolean(i, false);
            RegularField outField = inField.cloneShallow();
            switch (dims.length) {
            case 3:
                for (int i = 0;  i < dims[2] - 1; i++)
                    for (int j = 0; j < dims[1] - 1; j++) {
                        l = (i * dims[1] + j) * dims[0];
                        for (int k = 0; k < dims[0] - 1; k++, l++) 
                            if (cellMask.getBoolean(l))
                                for (int offset : offsets)
                                    mask.setBoolean(l + offset, true);
                    }
                break;
            case 2:
                for (int j = 0; j < dims[1] - 1; j++) {
                    l = j * dims[0];
                    for (int k = 0; k < dims[0] - 1; k++, l++) 
                        if (cellMask.getBoolean(l))
                            for (int offset : offsets)
                                mask.setBoolean(l + offset, true);
                }
                break;
            case 1:
                for (l = 0; l < dims[0] - 1; l++) 
                    if (cellMask.getBoolean(l))
                        for (int offset : offsets)
                            mask.setBoolean(l + offset, true);
                break;
            }
            outField.setMask(mask, 0);
            return outField;
        }
        long nCells = 0;
        for (long i = 0; i < cellMask.length(); i++) 
            if (cellMask.getBoolean(i))
                nCells += 1;
        if (nCells * cellSize > Integer.MAX_VALUE / 4) {
            System.out.println("Too many cells remaining - VisNow does not support large irregular fields");
            return null;
        }
        if (params.getSubsetType() == BOUNDARY_SURFACE) {
            int nOutNodes = (int)nCells;
            IrregularField out = new IrregularField(nOutNodes);
            float[] outCoords = new float[3 * nOutNodes];
            int[] inNodeOffsets = inField.getCellNodeOffsets();
            int m = 0;
            if (inField.hasCoords()) {
                FloatLargeArray inCoords = inField.getCurrentCoords();
                for (long i = 0; i < cellMask.length(); i++) 
                    if (cellMask.getBoolean(i)) {
                        float[] v = new float[3];
                        Arrays.fill(v, 0);
                        for (int j = 0; j < inNodeOffsets.length; j++) {
                            long k = i + inNodeOffsets[j];
                            for (int n = 0; n < 3; n++)
                                v[n] += inCoords.get(k);
                        }
                        for (int n = 0; n < 3; n++, m++)
                        outCoords[m] = v[n] / inNodeOffsets.length;    
                    }
            }
            else {
                float[][] affine = inField.getAffine();
                l = 0;
                m = 0;
                switch (dims.length) {
                case 3:
                    for (int i = 0;  i < dims[2]; i++)
                        for (int j = 0; j < dims[1]; j++) 
                            for (int k = 0; k < dims[0]; k++, l++) 
                                if (mask.getBoolean(l)) 
                                    for (int n = 0; n < 3; n++, m++) 
                                         outCoords[m] = affine[3][n] + 
                                                       (i + .5f) * affine[2][n] + 
                                                       (j + .5f) * affine[1][n] + 
                                                       (k + .5f) * affine[0][n];
                    break;
                case 2:
                    for (int j = 0; j < dims[1]; j++) 
                        for (int k = 0; k < dims[0]; k++, l++) 
                            if (mask.getBoolean(l))
                                for (int n = 0; n < 3; n++, m++) 
                                    outCoords[m] = affine[3][n] + 
                                                   (j + .5f) * affine[1][n] + 
                                                   (k + .5f) * affine[0][n];
                    break;
                case 1:
                    for (int k = 0; k < dims[0]; k++, l++) 
                        if (mask.getBoolean(l)) 
                            for (int n = 0; n < 3; n++, m++) 
                                outCoords[m] = affine[3][n] + (k + .5f) * affine[0][n];
                    break;
                }
                out.setCoords(new FloatLargeArray(outCoords), 0);
            }
            out.addComponent(DataArray.create(outCoords, dims.length, SubsetGeometryComponents.INDEX_COORDS));
            for (DataArray da : inField.getComponents()) {

            }
            int[] nodes = new int[nOutNodes];
            for (int i = 0; i < nodes.length; i++)
                nodes[i] = i;
            byte[] orientations = new byte[(int) nCells];
            Arrays.fill(orientations, (byte) 1);
            CellArray cells = new CellArray(CellType.POINT, nodes, orientations, null);
            CellSet cs = new CellSet("regular_field_cells");
            cs.addCells(cells);
            out.addCellSet(cs);
            cs.generateDisplayData(out.getCoords(0));
            return out;

        }
        else {
            IntLargeArray reindex = new IntLargeArray(inField.getNNodes());
            for (long i = 0; i < inField.getNNodes(); i++) 
                reindex.setInt(i, -1);
            l = 0;
            switch (dims.length) {
            case 3:
                for (int i = 0;  i < dims[2]; i++)
                    for (int j = 0; j < dims[1]; j++) 
                        for (int k = 0; k < dims[0]; k++, l++) 
                            if (cellMask.getBoolean(l)) 
                                for (int m = 0; m < offsets.length; m++)
                                    reindex.setInt(l + offsets[m], 1);
                break;
            case 2:
                for (int j = 0; j < dims[1]; j++) 
                    for (int k = 0; k < dims[0]; k++, l++) 
                        if (cellMask.getBoolean(l)) 
                            for (int m = 0; m < offsets.length; m++)
                                reindex.setInt(l + offsets[m], 1);
                break;
            case 1:
                for (int k = 0; k < dims[0]; k++, l++) 
                    if (cellMask.getBoolean(l)) 
                        for (int m = 0; m < offsets.length; m++)
                            reindex.setInt(l + offsets[m], 1);
                break;
            }    
            int nOutNodes = 0;
            for (long i = 0; i < inField.getNNodes(); i++) 
                if (reindex.getInt(i) == 1) {
                    reindex.setInt(i, nOutNodes);
                    nOutNodes += 1;
                    mask.setBoolean(i, true);
                }
                else
                    mask.setBoolean(i, false);
            return createSubset(inField, nOutNodes, mask, nCells, cellMask, reindex);
        }
    }
    
    private static Field createSubset(IrregularField inField, GeometricSubsetParams params)
    {
        LogicLargeArray mask;
        long nInNodes = inField.getNNodes();
        if (params.isGeometricCrop())
            mask = generateMask(params, inField.getNNodes(), inField.getCoords(0));
        else
            mask = generateMask(params, inField);
        if (params.getProcessingDepth() == MASK) {
            IrregularField outField = inField.cloneShallow();
            outField.setMask(mask, 0);
            return outField;
        }
        if (params.getSubsetType() == BOUNDARY_SURFACE) {
            FloatLargeArray inCoords = inField.getCurrentCoords();
            int nOutNodes = 0;
            for (CellSet cs : inField.getCellSets()) 
                for (CellArray ca : cs.getCellArrays()) 
                    if (ca != null) {
                        int nCellNodes = ca.getNCellNodes();
                        int[] nodes = ca.getNodes();
                        for (int i = 0; i < ca.getNCells(); i++) 
                            if (cellStatus(params, mask, i * nCellNodes, nCellNodes, nodes))
                                nOutNodes+= 1; 
                    }
            IrregularField out = new IrregularField(nOutNodes);
            float[] outCoords = new float[3 * nOutNodes];
            int m = 0;
            for (CellSet cs : inField.getCellSets()) 
                for (CellArray ca : cs.getCellArrays()) 
                    if (ca != null) {
                        int nCellNodes = ca.getNCellNodes();
                        int[] nodes = ca.getNodes();
                        for (int i = 0; i < ca.getNCells(); i++)
                            if (cellStatus(params, mask, i * nCellNodes, nCellNodes, nodes)) {
                                float[] v = new float[] {0, 0, 0};
                                for (int j = i * nCellNodes; j < (i + 1) * nCellNodes; j++) 
                                    for (int n = 0, k = 3 * nodes[j]; n < 3; n++, k++)
                                        v[n] += inCoords.get(k);
                                for (int n = 0; n < 3; n++, m++)
                                    outCoords[m] = v[n] / nCellNodes;   
                            }
                    }
            out.setCoords(new FloatLargeArray(outCoords), 0);
            int[] outNodes = new int[nOutNodes];
            for (int i = 0; i < nOutNodes; i++)
                outNodes[i] = i;
            out.addComponent(DataArray.create(outNodes, 1, "dummy"));
            byte[] orientations = new byte[nOutNodes];
            Arrays.fill(orientations, (byte) 1);
            CellArray cells = new CellArray(CellType.POINT, outNodes, orientations, null);
            CellSet outCS = new CellSet("field_cells");
            outCS.addCells(cells);
            out.addCellSet(outCS);
            outCS.generateDisplayData(out.getCoords(0));
            return out;
        }
        else {
            IntLargeArray reindex = new IntLargeArray(nInNodes);
            for (long i = 0; i < reindex.length(); i++) 
                reindex.setInt(i, -1);

            for (CellSet cs : inField.getCellSets()) 
                for (CellArray ca : cs.getCellArrays()) 
                    if (ca != null) {
                        int nCellNodes = ca.getNCellNodes();
                        int[] nodes = ca.getNodes();
                        for (int i = 0; i < ca.getNCells(); i++) {
                            if (cellStatus(params, mask, i * nCellNodes, nCellNodes, nodes))
                                for (int j = i * nCellNodes; j < (i + 1) * nCellNodes; j++)
                                    reindex.setInt(nodes[j], 1);
                        }
                    }
            int nOutNodes = 0;
            for (long i = 0; i < nInNodes; i++) 
                if (reindex.getInt(i) == 1) {
                    reindex.setInt(i, nOutNodes);
                    nOutNodes += 1;
                    mask.setBoolean(i, true);
                }
                else
                    mask.setBoolean(i, false);
            if (nOutNodes < 1)
                return null;
            IrregularField out = new IrregularField(nOutNodes);
            out.setCoords(select(inField.getCoords(), 3, nOutNodes, mask));
            for (DataArray dataArr : inField.getComponents()) 
                out.addComponent(select(dataArr, nOutNodes, mask));
            for (CellSet cs : inField.getCellSets()) {
                CellSet outCs = new CellSet(cs.getName());
                boolean nonEmpty = false;
                for (CellArray ca : cs.getCellArrays()) 
                    if (ca != null) {
                        int nCellNodes = ca.getNCellNodes();
                        int[] nodes = ca.getNodes();
                        int nOutCells = 0;
                        boolean[] retain = new boolean[ca.getNCells()];
                        Arrays.fill(retain, true);
                    selectCellLoop:        
                        for (int i = 0; i < ca.getNCells(); i++) {
                            for (int j = i * nCellNodes; j < (i + 1) * nCellNodes; j++)
                                if (reindex.getInt(nodes[j]) == -1) {
                                    retain[i] = false;
                                    continue selectCellLoop;
                                }
                            nOutCells += 1;
                        }
                        if (nOutCells == 0)
                            continue;
                        int[] outNodes = new int[nCellNodes * nOutCells];
                        byte[] inOrientations = ca.getOrientations();
                        byte[] outOrientations = new byte[nOutCells];
                        for (int i = 0, l = 0; i < ca.getNCells(); i++)
                            if (retain[i]) {
                                for (int j = 0; j < nCellNodes; j++)
                                    outNodes[l * nCellNodes + j] = reindex.getInt(nodes[i * nCellNodes + j]); 
                                outOrientations[l] = inOrientations[i];
                                l += 1;
                            }
                        nonEmpty = true;
                        CellArray outCa = new CellArray(ca.getType(), outNodes, outOrientations, null);
                        outCs.addCells(outCa);
                    }
                if (nonEmpty)
                    out.addCellSet(outCs);
            }
            return out;
        }
    }
    
    public static final Field createSubset(Field inField, GeometricSubsetParams params)
    {
        if (inField instanceof RegularField)
            return createSubset((RegularField)inField, params);
        else 
            return createSubset((IrregularField)inField, params);
    }
    
    public static final IrregularField cleanField(IrregularField inField)
    {
        LogicLargeArray mask = new LogicLargeArray(inField.getNNodes(), true);
        
        for (CellSet cs : inField.getCellSets()) 
            for (CellArray ca : cs.getCellArrays()) 
                if (ca != null) {
                    int[] nodes = ca.getNodes();
                    for (int node : nodes)
                        mask.setBoolean(node, true);
                }
        IntLargeArray reindex = new IntLargeArray(mask.length());
        int nOutNodes = 0;
        for (int i = 0; i < mask.length(); i++) 
            if (mask.getBoolean(i)) {
                reindex.setInt(i, nOutNodes);
                nOutNodes += 1;
            }
        
        IrregularField outField = new IrregularField(nOutNodes);
        
        outField.setCoords(FieldSubset.select(inField.getCoords(), 3, nOutNodes, mask));
        for (DataArray da : inField.getComponents()) 
            outField.addComponent(FieldSubset.select(da, nOutNodes, mask));
        for (CellSet cs : inField.getCellSets()) {
            CellSet outCS = new CellSet(cs.getName());
            for (CellArray ca : cs.getCellArrays()) 
                if (ca != null) {
                    int[] inNodes = ca.getNodes();
                    int[] outNodes = new int[inNodes.length];
                    for (int i = 0; i < inNodes.length; i++) 
                        outNodes[i] = reindex.getInt(inNodes[i]);
                    CellArray outCA = new CellArray(ca.getType(), outNodes, 
                                                    ca.getOrientations(), ca.getDataIndices());
                    outCS.addCells(outCA);
                }   
            outField.addCellSet(outCS);
        }
        return outField;
    }
    
    private FieldSubset()
    {
        
    }
}
