/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils.field;

import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.utils.SliceUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class SliceRegularField
{

    private static final String[] coordNames
        = {
            "x", "y", "z"
        };

    /**
     * Creates a new instance of CropRegularField
     */
    public SliceRegularField()
    {
    }

    public static void sliceCoordsUpdate(RegularField inField, int axis, int slice, RegularField outField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3 ||
            axis < 0 || axis >= 3 ||
            slice < 0 || slice >= inField.getDims()[axis])
            return;
        int[] dims = inField.getDims();
        int[] outDims = outField.getDims();
        if (outDims == null || outDims.length != 2 || outDims[0] * outDims[1] * dims[axis] != (int) inField.getNNodes())
            return;
        if (!inField.hasCoords()) {
            float[][] inAffine = inField.getAffine();
            float[][] outAffine = new float[4][3];
            switch (axis) {
                case 0:
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[0][i];
                        outAffine[0][i] = inAffine[1][i];
                        outAffine[1][i] = inAffine[2][i];
                        outAffine[2][i] = 0;
                    }
                    break;
                case 1:
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[1][i];
                        outAffine[0][i] = inAffine[0][i];
                        outAffine[1][i] = inAffine[2][i];
                        outAffine[2][i] = 0;
                    }
                    break;
                case 2:
                    for (int i = 0; i < 3; i++) {
                        outAffine[3][i] = inAffine[3][i] + slice * inAffine[2][i];
                        outAffine[0][i] = inAffine[0][i];
                        outAffine[1][i] = inAffine[1][i];
                        outAffine[2][i] = 0;
                    }
                    break;
            }
            outField.setAffine(outAffine);
        } else {
            outField.setCoords(inField.getCoords().get2DSlice(inField.getLDims(), axis, (long)slice, 3));
        }
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
    }

    /**
     * @param inField
     * @param axis
     * @param slice
     * @param outField
     * @return false if slice is empty (empty outField should be set)
     */
    public static boolean sliceUpdate(RegularField inField, int axis, int slice, RegularField outField)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3 ||
            axis < 0 || axis >= 3 ||
            slice < 0 || slice >= inField.getDims()[axis] || outField == null) {
            return false;
        }
        long[] dims = inField.getLDims();
        long[] outDims = outField.getLDims();
        if (outDims == null || outDims.length != 2 || outDims[0] * outDims[1] * dims[axis] != (int) inField.getNNodes())
            return false;
        LogicLargeArray validOut = null;
        if (inField.hasMask()) {
            long m = dims[0] * dims[1];
            long l = dims[0];
            LogicLargeArray mask = inField.getCurrentMask();
            switch (axis) {
                case 0:
                    validOut = (LogicLargeArray) SliceUtils.get2DSlice(mask, slice, dims[1], l, dims[2], m, 1);
                    break;
                case 1:
                    validOut = (LogicLargeArray) SliceUtils.get2DSlice(mask, slice * l, dims[0], 1, dims[2], m, 1);
                    break;
                case 2:
                    validOut = (LogicLargeArray) SliceUtils.get2DSlice(mask, slice * m, dims[0], 1, dims[1], l, 1);
                    break;
                default:
                    throw new IllegalArgumentException("axis has to be 0, 1, or 2");
            }
            outField.setCurrentMask(validOut);
        }
        sliceCoordsUpdate(inField, axis, slice, outField);
        for (int n = 0; n < inField.getNComponents(); n++) {
            DataArray inDataArr = inField.getComponent(n);
            DataArray outDataArr = inDataArr.get2DSlice(dims, axis, slice);
            outDataArr.setPreferredRanges(inDataArr.getPreferredMinValue(), inDataArr.getPreferredMaxValue(), inDataArr.getPreferredPhysMinValue(), inDataArr.getPreferredPhysMaxValue());
            outDataArr.setCurrentTime(inField.getCurrentTime());
            outField.setComponent(outDataArr, n);
        }

        return true;
    }

    public static RegularField sliceField(RegularField inField, int axis, int slice)
    {
        if (inField == null || inField.getDims() == null || inField.getDims().length != 3 ||
            axis < 0 || axis >= 3 ||
            slice < 0 || slice >= inField.getDims()[axis])
            return null;
        int[] dims = inField.getDims();
        int[] outDims = new int[2];
        if (axis == 0) {
            outDims[0] = dims[1];
            outDims[1] = dims[2];
        } else if (axis == 1) {
            outDims[0] = dims[0];
            outDims[1] = dims[2];
        } else {
            outDims[0] = dims[0];
            outDims[1] = dims[1];
        }
        RegularField outField = new RegularField(outDims);
        sliceUpdate(inField, axis, slice, outField);
        outField.setCurrentTime(inField.getCurrentTime());
        return outField;
    }
}
