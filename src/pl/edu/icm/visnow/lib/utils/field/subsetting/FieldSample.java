//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.field.subsetting;

import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling)
 */
public class FieldSample
{
    private FieldSample()
    {
        
    }
    
    public static TimeData sample(TimeData in, int sampleSize, int veclen, LogicLargeArray mask)
    {
        TimeData out = new TimeData(in.getType());
        for (int it = 0; it < in.getNSteps(); it++) {
            LargeArray inVals = in.getValues().get(it);
            LargeArray outVals = LargeArrayUtils.create(inVals.getType(), sampleSize * veclen);
            for (long i = 0, l = 0; i < mask.length(); i++)
                if (mask.getBoolean(i)) {
                    LargeArrayUtils.arraycopy(inVals, veclen * i, outVals, veclen * l, veclen);
                    l += 1;
                }
            out.setValue(outVals, in.getTime(it));
        }
        return out;
    }
    
    public static DataArray sample(DataArray in, int sampleSize, LogicLargeArray mask)
    {
        return DataArray.create(sample(in.getTimeData(), sampleSize, in.getVectorLength(), mask),
                                in.getVectorLength(), in.getName(), in.getUnit(), in.getUserData());
    }

    public static Field randomSample(Field in, int sampleSize, boolean sampleComponents)
    {
        long nNodes = in.getNNodes();
        long nEffNodes = nNodes;
        LogicLargeArray mask = new LogicLargeArray(nNodes, true);
        LogicLargeArray inMask = null;
        if (sampleSize >= nNodes)
            return in.cloneShallow();
        if (in.hasMask()) {
            inMask = in.getMask(0);
            nEffNodes = 0;
            for (long i = 0; i < nNodes; i++)
                if (inMask.getBoolean(i))
                    nEffNodes += 1;
            int done = 0;
            if (sampleSize < nEffNodes / 2)
                while (done < sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (!mask.getBoolean(i) &&  inMask.getBoolean(i)) {
                        mask.setBoolean(i, true);
                        done += 1;
                    }
                }
            else {
                for (long i = 0; i < nNodes; i++)
                    mask.setBoolean(i, inMask.getBoolean(i));
                while (done < nEffNodes - sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (mask.getBoolean(i)) {
                        mask.setBoolean(i, false);
                        done += 1;
                    }
                }
            }
        }
        else {
            int done = 0;
            if (sampleSize < nNodes / 2)
                while (done < sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (!mask.getBoolean(i)) {
                        mask.setBoolean(i, true);
                        done += 1;
                    }
                }
            else {
                for (long i = 0; i < nNodes; i++)
                    mask.setBoolean(i, true);
                while (done < nEffNodes - sampleSize) {
                    long i = (long) (Math.random() * nNodes);
                    if (mask.getBoolean(i)) {
                        mask.setBoolean(i, false);
                        done += 1;
                    }
                }
            }
        }
            
        IrregularField out = new IrregularField(sampleSize);
        if (in.hasCoords())
            out.setCoords(sample(in.getCoords(), sampleSize, 3, mask));
        else if (!(in instanceof RegularField))
            return null;
        else {
            RegularField rIn = (RegularField) in;
            long[] dims = rIn.getLDims();
            float[][] aff = rIn.getAffine();
            FloatLargeArray outCrds = new FloatLargeArray(3 * sampleSize);
            for (long i = 0, l = 0; i < nNodes; i++)
                if (mask.get(i) != 0) {
                    float[] p = new float[3];
                    System.arraycopy(aff[3], 0, p, 0, 3);
                    long ii = i;
                    for (int j = 0; j < dims.length; j++) {
                        long ind = ii % dims[j];
                        ii /= dims[j];
                        for (int k = 0; k < 3; k++)
                            p[k] += ind * aff[j][k];
                    }
                    for (int k = 0; k < p.length; k++)
                        outCrds.setFloat(3 * l + k, p[k]);
                }
            out.setCoords(outCrds, 0);
        }
        if (sampleComponents) 
            for (DataArray component : in.getComponents()) 
                out.addComponent(sample(component, sampleSize, mask));
        else {
            FloatLargeArray d = new FloatLargeArray(sampleSize);
            for (long i = 0; i < sampleSize; i++)
                d.setFloat(i, i);
            out.addComponent(DataArray.create(d, 1, "dummy"));
        }
        return out;
    }

}
