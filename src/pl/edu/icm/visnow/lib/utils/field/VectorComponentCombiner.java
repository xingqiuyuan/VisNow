//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.field;

import java.util.ArrayList;
import pl.edu.icm.jlargearrays.ByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;

/**
 *
 * @author know
 */
public class VectorComponentCombiner
{

    private static char[][] vComponentNames = {{'X', 'Y', 'Z'}, {'x', 'y', 'z'},
                                               {'U', 'V', 'W'}, {'u', 'v', 'w'},
                                               {'0', '1', '2'}, {'1', '2', '3'}};

    private static class VectorComponentData
    {
        String name;
        int vLen;

        public VectorComponentData(String name, int vLen)
        {
            this.name = name;
            this.vLen = vLen;
        }
    }

    private static VectorComponentData vectorComponent(DataContainer inData,int k)
    {
        DataArray da = inData.getComponent(k);
        if (!da.isNumeric() || da.getVectorLength() != 1)
            return null;
        int vLen = 1;
        String[] names = inData.getComponentNames();
        int nNames = names.length;
        String cName = names[k];
        int nameLength = cName.length();
        String cmpName;
        for (char[] vCmpNames : vComponentNames) {
            if (cName.charAt(0) == vCmpNames[0]) {
                cmpName = cName.substring(1);
                for (int i = 1; i < Math.min(3, nNames - k); i++)
                    if (names[k + i].length() == nameLength &&
                        names[k + i].charAt(0) == vCmpNames[i] &&
                        cmpName.equals(names[k + i].substring(1)) &&
                        inData.getComponent(k + i).isFullyCompatibleWith(da, false))
                        vLen = i + 1;
                if (vLen > 1)
                    return new VectorComponentData(cmpName, vLen);
            }
            
            if (cName.charAt(nameLength - 1) == vCmpNames[0]) {
                cmpName = cName.substring(0, nameLength - 1);
                for (int i = 1; i < Math.min(3, nNames - k); i++)
                    if (names[k + i].length() == nameLength &&
                        names[k + i].charAt(nameLength - 1) == vCmpNames[i] &&
                        cmpName.equals(names[k + i].substring(0, nameLength - 1)) &&
                        inData.getComponent(k + i).isFullyCompatibleWith(da, false))
                        vLen = i + 1;
                if (cmpName.isEmpty()) cmpName = "merged_cmp";
                if (vLen > 1) 
                    return new VectorComponentData(cmpName.replaceAll("^[\\W_]*","").
                                                           replaceAll("[\\W_]*$",""), vLen);
            }
        }
        return null;
    }
    
    private static void combineVectorsInContainer(DataContainer inData)
    {
        int nInData = inData.getNComponents();
        long nElements = inData.getNElements();
        boolean[] retain = new boolean[nInData];
        VectorComponentData[] mergedData = new VectorComponentData[nInData];
        for (int i = 0; i < mergedData.length; i++) 
            mergedData[i] = null;
        for (int k = 0; k < nInData; k++) {
            VectorComponentData vData = vectorComponent(inData, k);
            if (vData == null) {
                retain[k] = true;
                continue;
            }
            for (int i = k; i < k + vData.vLen; i++) 
                retain[i] = false;
            mergedData[k] = vData;
            k += vData.vLen;
        }
        ArrayList<DataArray> outComponents = new ArrayList<>();
        for (int k = 0; k < nInData; k++) 
            if (retain[k])
                outComponents.add(inData.getComponent(k));
            else if (mergedData[k] != null) {
                DataArray inDA = inData.getComponent(k);
                System.out.println("merging "+mergedData[k].name);
                int vlen = mergedData[k].vLen;
                int nFrames = inDA.getNFrames();
                TimeData outTD = new TimeData(inDA.getType());
                for (int i = 0; i < nFrames; i++) {
                    switch (inDA.getType()) {
                    case FIELD_DATA_BYTE:
                        ByteLargeArray outB = new ByteLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            ByteLargeArray inB = (ByteLargeArray)inData.getComponent(k + j).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outB.setByte(vlen * l + j, inB.get(l));
                        }
                        outTD.setValue(outB, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_SHORT:
                        ShortLargeArray outS = new ShortLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            ShortLargeArray inS = (ShortLargeArray)inData.getComponent(k + j).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outS.setShort(vlen * l + j, inS.get(l));
                        }
                        outTD.setValue(outS, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_INT:
                        IntLargeArray outI = new IntLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            IntLargeArray inI = (IntLargeArray)inData.getComponent(k + j).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outI.setInt(vlen * l + j, inI.get(l));
                        }
                        outTD.setValue(outI, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_FLOAT:
                        FloatLargeArray outF = new FloatLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            FloatLargeArray inF = (FloatLargeArray)inData.getComponent(k + j).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outF.setFloat(vlen * l + j, inF.get(l));
                        }
                        outTD.setValue(outF, inDA.getTimeData().getTime(i));
                        break;
                    case FIELD_DATA_DOUBLE:
                        DoubleLargeArray outD = new DoubleLargeArray(vlen * nElements);
                        for (int j = 0; j < vlen; j++) {
                            DoubleLargeArray inD = (DoubleLargeArray)inData.getComponent(k + j).
                                                                  getTimeData().getValues().get(i);
                            for (long l = 0; l < nElements; l++) 
                                outD.setDouble(vlen * l + j, inD.get(l));
                        }
                        outTD.setValue(outD, inDA.getTimeData().getTime(i));
                        break;
                    }
                }
                outComponents.add(DataArray.create(outTD, vlen, mergedData[k].name, 
                                                   inDA.getUnit(), inDA.getUserData()));
            }
        inData.removeComponents();
        inData.setComponents(outComponents);
    }
    
    public static RegularField combineVectors(RegularField inField)
    {
        combineVectorsInContainer(inField);
        return inField;
    }
    
    public static IrregularField combineVectors(IrregularField inField)
    {
        combineVectorsInContainer(inField);
        for (CellSet cs : inField.getCellSets()) 
            combineVectorsInContainer(cs);
        return inField;
    }
}
