//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.graphing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import static javax.swing.SwingConstants.CENTER;
import static javax.swing.SwingConstants.LEFT;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.gui.icons.IconsContainer;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.INDEX_COORDS;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.NODE_POSITIONS;

/**
 *
 * @author know
 */

public class ComponentDisplayController extends javax.swing.JPanel {

    class ComponentTableModel extends DefaultTableModel
    {

        private static final long serialVersionUID = -5065823568201214687L;
        protected int columns = 6;
        protected int selectedRow = -1;
        protected int selectedColumn = -1;

        public ComponentTableModel()
        {
            super(new String[]{
                "component", "min", "avg", "max", "<html>\u03c3</html>"
            }, 0);
        }

        @Override
        public Class getColumnClass(int c)
        {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int row, int col)
        {
            return false;
        }

        public void setSelectedRow(int selectedRow) {
            this.selectedRow = selectedRow;
        }
        
    }

    class ComponentRenderer extends DefaultTableCellRenderer
    {
        private static final long serialVersionUID = 1L;
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(CENTER);
            setForeground(data[row].getColor());
            setBackground(params.getWindowBgrColor());
            setText(value.toString());
            return this;
        }
    }
    
    ComboBoxRenderer shadingBoxRenderer = new ComboBoxRenderer();

    class IconString
    {

        ImageIcon icon;
        String string;

        public IconString(ImageIcon icon, String string)
        {
            this.icon = icon;
            this.string = string;
        }

        public ImageIcon getIcon()
        {
            return icon;
        }

        public String getString()
        {
            return string;
        }

        @Override
        public String toString()
        {
            return string;
        }
    }

    protected IconString[] elements = {
        new IconString(new ImageIcon(IconsContainer.getCnt()), "continuous"),
        new IconString(new ImageIcon(IconsContainer.getDashed()), "dashed"),
        new IconString(new ImageIcon(IconsContainer.getDotted()), "dotted"),
        new IconString(new ImageIcon(IconsContainer.getDashDotted()), "dashdotted"),
        new IconString(new ImageIcon(IconsContainer.getDotDashDot()), "dot-dash-dot"),
        new IconString(new ImageIcon(IconsContainer.getDashDashDot()), "dash-dash-dot")};

    class ComplexCellRenderer implements ListCellRenderer
    {

        protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index,
                                                      boolean isSelected, boolean cellHasFocus)
        {
            Icon theIcon = null;
            String theText = null;

            JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                                                                                    isSelected, cellHasFocus);

            if (value instanceof IconString) {
                theIcon = ((IconString) value).getIcon();
                theText = ((IconString) value).getString();
            } else
                theText = "";
            if (theIcon != null)
                renderer.setIcon(theIcon);
            renderer.setText(theText);
            return renderer;
        }
    }
    
    class ComboBoxRenderer extends JLabel implements ListCellRenderer {
        public ComboBoxRenderer() {
            setOpaque(true);
            setHorizontalAlignment(LEFT);
            setVerticalAlignment(CENTER);
        }

        /*
         * This method finds the image and text corresponding
         * to the selected value and returns the label, set up
         * to display the text and image.
         */
        @Override
        public Component getListCellRendererComponent(
                                           JList list,
                                           Object value,
                                           int index,
                                           boolean isSelected,
                                           boolean cellHasFocus) {
            //Get the selected index. (The index param isn't
            //always valid, so just use the value.)
            IconString val = (IconString)value;
            //Set the icon and text.  If icon was null, say so.
            setIcon(val.getIcon());
            setText(val.getString());
            return this;
        }
    }

    protected ComponentTableModel componentTableModel = new ComponentTableModel();
    protected DataContainer inField = null;
    protected GraphParams params = null;
    protected int[] indices = null;
    protected Color[] defaultColors = new Color[]{
        new Color(1.f, .5f, .5f), new Color(1.f, 1.f, .5f), new Color(.5f, 1.f, .5f),
        new Color(.5f, 1.f, 1.f), new Color(.5f, .5f, 1.f), new Color(1.f, .5f, 1.f),
        new Color(.8f, .8f, .8f)};
    protected String[] selectedComponents = new String[] {};
    protected DisplayedData[] data;
    protected float[][] timeRanges;
    protected float[] timeRange = new  float[2];
    protected boolean active = true;
    protected DisplayedData updatedData;
    
    
    protected int clickedRow = -1;
    protected boolean[] selectedRows = {};
    
    /**
     * Creates new form componentDisplayController
     */
    public ComponentDisplayController() {
        initComponents();
        setTableLAF();     
        Dimension dim = new Dimension(280, 150);
        presentationFrame.setSize(dim);
        presentationFrame.setPreferredSize(dim);
        componentTable.setRowSelectionAllowed(true);
        componentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        componentTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent lse) {
                if (!lse.getValueIsAdjusting()) 
                    fire();
            }
        });
        componentTable.addMouseListener(new java.awt.event.MouseAdapter()
        {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                if (evt.getButton() != MouseEvent.BUTTON1) {
                    selectedRows = new boolean[componentTable.getRowCount()];
                    for (int i = 0; i < selectedRows.length; i++) 
                        selectedRows[i] = componentTable.isRowSelected(i);
                    clickedRow = componentTable.rowAtPoint(evt.getPoint());
                    updatedData = data[clickedRow];
                    colorEditor.setColor(updatedData.getColor());
                    widthSlder.setValue((int)(30 * updatedData.getWidth()));
                    strokeCombo.setSelectedIndex(updatedData.getStrokeIndex());
                    presentationFrame.setLocation(evt.getXOnScreen(), evt.getYOnScreen());
                    presentationFrame.setVisible(true);
                }
            }
        });
        Object popup = strokeCombo.getUI().getAccessibleChild(strokeCombo, 0);
        if (popup instanceof BasicComboPopup) {
            BasicComboPopup strokePopup = (BasicComboPopup) popup;
            JList strokeList = strokePopup.getList();
            strokePopup.removeAll();
            strokeList.setCellRenderer(new ComplexCellRenderer());
            strokeList.setSize(200, 150);
            strokeList.setMinimumSize(new Dimension(180, 150));
            strokeList.setPreferredSize(new Dimension(180, 150));
            strokeList.setMaximumSize(new Dimension(180, 150));
            strokeList.setFixedCellHeight(24);
            strokePopup.setSize(200, 150);
            strokePopup.setMinimumSize(new Dimension(184, 150));
            strokePopup.setPreferredSize(new Dimension(184, 150));
            strokePopup.setMaximumSize(new Dimension(184, 150));
            strokePopup.add(strokeList, BorderLayout.CENTER);
        }
    }

    private void setTableLAF()
    {
        componentTable.setFont(new java.awt.Font("Dialog", 0, 10));
        componentTable.setAutoscrolls(false);
        componentTable.setGridColor(new java.awt.Color(200, 200, 200));
        TableColumnModel colModel = componentTable.getColumnModel();
        colModel.getColumn(0).setCellRenderer(new ComponentRenderer());
        colModel.getColumn(0).setPreferredWidth(120);
        colModel.getColumn(1).setPreferredWidth(35);
        colModel.getColumn(2).setPreferredWidth(35);
        colModel.getColumn(3).setPreferredWidth(35);
        colModel.getColumn(4).setPreferredWidth(35);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        presentationFrame = new javax.swing.JFrame();
        colorEditor = new pl.edu.icm.visnow.gui.widgets.ColorEditor();
        widthSlder = new javax.swing.JSlider();
        strokeCombo = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        applyButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        componentTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        presentationFrame.getContentPane().setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        presentationFrame.getContentPane().add(colorEditor, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 9, 0);
        presentationFrame.getContentPane().add(widthSlder, gridBagConstraints);

        strokeCombo.setModel(new DefaultComboBoxModel(elements)
        );
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        presentationFrame.getContentPane().add(strokeCombo, gridBagConstraints);

        jLabel9.setText("color");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        presentationFrame.getContentPane().add(jLabel9, gridBagConstraints);

        jLabel10.setText("width");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        presentationFrame.getContentPane().add(jLabel10, gridBagConstraints);

        jLabel11.setText("stroke");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 9);
        presentationFrame.getContentPane().add(jLabel11, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                okButtonActionPerformed(evt);
            }
        });
        jPanel1.add(okButton);

        applyButton.setText("Apply");
        applyButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                applyButtonActionPerformed(evt);
            }
        });
        jPanel1.add(applyButton);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cancelButtonActionPerformed(evt);
            }
        });
        jPanel1.add(cancelButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        presentationFrame.getContentPane().add(jPanel1, gridBagConstraints);

        setLayout(new java.awt.BorderLayout());

        jScrollPane1.setMinimumSize(new java.awt.Dimension(180, 100));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(220, 150));

        componentTable.setModel(componentTableModel);
        jScrollPane1.setViewportView(componentTable);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jLabel1.setText("<html>select component(s) to graph<p>right click to change color</html>");
        add(jLabel1, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_okButtonActionPerformed
    {//GEN-HEADEREND:event_okButtonActionPerformed
        if (updatedData != null) {
            updatedData.setColor(colorEditor.getColor());
            updatedData.setStroke(strokeCombo.getSelectedIndex());
            updatedData.setWidth(widthSlder.getValue() / 30f);
            if (params != null)
                params.setDisplayedData(data);
        }
        componentTable.repaint();
        presentationFrame.setVisible(false);
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cancelButtonActionPerformed
    {//GEN-HEADEREND:event_cancelButtonActionPerformed
        presentationFrame.setVisible(false);
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_applyButtonActionPerformed
    {//GEN-HEADEREND:event_applyButtonActionPerformed
        if (updatedData != null) {
            updatedData.setColor(colorEditor.getColor());
            updatedData.setStroke(strokeCombo.getSelectedIndex());
            updatedData.setWidth(widthSlder.getValue() / 30f);
            if (params != null)
                params.setDisplayedData(data);
        }
        componentTable.repaint();
    }//GEN-LAST:event_applyButtonActionPerformed

    private float cnvToPhysVal(DataArray da, float v)
    {
        double r = (v - da.getPreferredMinValue()) / (da.getPreferredMaxValue() - da.getPreferredMinValue());
        return (float) (da.getPreferredPhysMinValue() + r * (da.getPreferredPhysMaxValue() - da.getPreferredPhysMinValue()));
    }
    
    private boolean skip(DataArray da)
    {
        if (!da.isNumeric())
            return true;
        String name = da.getName();
        return (name.equalsIgnoreCase(NODE_POSITIONS) || 
                name.equalsIgnoreCase(INDEX_COORDS));
    }

    
    public void setInField(DataContainer inField)
    {
        if (inField == null)
            return;
        if (this.inField != null
                && this.inField.isCompatibleWith(inField)) {
            this.inField = inField;
        }
        else {
            active =false;
            if (params != null)
                params.setActive(false);
            this.inField = inField;
            ArrayList<DataArray> inData = inField.getComponents();
            indices = new int[inData.size()];
            componentTableModel.setRowCount(0);
            int n = 0;
            boolean initSel = false;
            for (int i = 0; i < inData.size(); i++) {
                DataArray dta = inData.get(i);
                if (skip(dta))
                    continue;
                indices[n] = i;
                n += 1;
            }
            if (n == 0)
                return;
            data = new DisplayedData[n];
            timeRanges = new float[n][2];
            for (int i = 0; i < n; i++) 
                data[i] = new DisplayedData(indices[i], defaultColors[i % defaultColors.length]);
            for (int i = 0; i < n; i++) {
                DataArray dta = inData.get(indices[i]);
                timeRanges[i][0] = dta.getStartTime();
                timeRanges[i][1] = dta.getEndTime();
                componentTableModel.addRow(new Object[]{dta.getName(), 
                    String.format("%7.3f", dta.getPreferredPhysMinValue()),
                    String.format("%7.3f", cnvToPhysVal(dta, (float)dta.getMeanValue())),
                    String.format("%7.3f", dta.getPreferredPhysMaxValue()),
                    String.format("%7.3f", cnvToPhysVal(dta, (float)dta.getMeanSquaredValue()))});
                for (String selectedComponent : selectedComponents)
                    if (dta.getName().equalsIgnoreCase(selectedComponent)) {
                        initSel = true;
                        componentTable.setRowSelectionInterval(i, i);
                        data[i].setDisplayed(true);
                    }
            }
            if (!initSel) {
                componentTableModel.setSelectedRow(0);
                componentTable.setRowSelectionInterval(0, 0);
                data[0].setDisplayed(true);
            }
            updatedData = null;
        }
        active =true;
        if (params != null) {
            params.setDisplayedData(data);
            params.setActive(true);
        }
    }

    /**
     * Set the value of params
     *
     * @param params new value of params
     */
    public void setParams(GraphParams params)
    {
        this.params = params;
        params.setDisplayedData(data);
    }

    private void fire()
    {
        if (params == null || !active) 
            return;
        for (int i = 0; i < componentTable.getRowCount(); i++) {
            data[i].setDisplayed(componentTable.isRowSelected(i));
        }
        params.updateTable();
        fireStateChanged();
    }

    public DisplayedData[] getDisplayedData() {
        return data;
    }
    
    public int getNDisplayedData()
    {
        int n = 0;
        for (DisplayedData displayedData : data) 
            if (displayedData.isDisplayed())
                n += 1;
        return n;
    }

    public float[] getTimeRange() {
        timeRange[0] =  Float.MAX_VALUE;
        timeRange[1] = -Float.MAX_VALUE;
        for (int i = 0; i < data.length; i++) 
            if (data[i].isDisplayed()) {
                timeRange[0] = Math.min(timeRange[0], timeRanges[i][0]);
                timeRange[1] = Math.max(timeRange[1], timeRanges[i][1]);
            }
        return timeRange;
    }
    
    /**
     * Utility field holding list of global ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * <p/>
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * <p/>
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     * <p/>
     */
    public synchronized void clearChangeListeners()
    {
        changeListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event (calls
     * <code>stateChanged()</code> on each listener in
     * <code>changeListenerList</code>).
     */
    public void fireStateChanged()
    {
        ChangeEvent e = new ChangeEvent(this);
        for (int i = 0; i < changeListenerList.size(); i++) {
            changeListenerList.get(i).stateChanged(e);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyButton;
    private javax.swing.JButton cancelButton;
    private pl.edu.icm.visnow.gui.widgets.ColorEditor colorEditor;
    private javax.swing.JTable componentTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton okButton;
    private javax.swing.JFrame presentationFrame;
    private javax.swing.JComboBox<String> strokeCombo;
    private javax.swing.JSlider widthSlder;
    // End of variables declaration//GEN-END:variables
}
