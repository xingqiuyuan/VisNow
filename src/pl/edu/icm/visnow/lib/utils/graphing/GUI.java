//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.graphing;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.DataContainerSchema;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArraySchema;
import pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.FloatSubRangeModel;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.INDEX_COORDS;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.LINE_SLICE_COORDS;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.NODE_POSITIONS;

/**
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling)
 */
public class GUI extends javax.swing.JPanel
{
    protected DataContainerSchema lastFieldSchema = null;
    protected DataContainerSchema inFieldSchema = null;
    protected GraphParams params = null;
    protected int[] indices = null;
    protected Color bgrColor = Color.BLACK;
    protected Color[] defaultColors = new Color[]{
        new Color(1.f, .5f, .5f), new Color(1.f, 1.f, .5f), new Color(.5f, 1.f, .5f),
        new Color(.5f, 1.f, 1.f), new Color(.5f, .5f, 1.f), new Color(1.f, .5f, 1.f),
        new Color(.8f, .8f, .8f)};
    protected String[] selectedComponents = new String[] {};
    protected ChangeListener bgrColorListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e) {
            componentDisplayController.repaint();
        }
    };
    
    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();
        componentDisplayController.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateComponentTable();
            }
        });
        bgrColorEditor.setVisible(false);
        legendPositionPanel.setVisible(false);
        horizontalExtentsSlider.setExtent(.1f);
        horizontalExtentsSlider.setPolicy(FloatSubRangeModel.PUSH);
        verticalExtentsSlider.setExtent(.1f);
        verticalExtentsSlider.setPolicy(FloatSubRangeModel.PUSH);
    }
    
    private boolean skip(DataArraySchema da)
    {
        if (!da.isNumeric())
            return true;
        String name = da.getName();
        return (name.equalsIgnoreCase(NODE_POSITIONS) || 
                name.equalsIgnoreCase(INDEX_COORDS));
    }

    public void setInField(Field inField)
    {
        inFieldSchema = inField.getSchema();
        params.setActive(false);
        ArrayList<String> argNames = new ArrayList<>();
        argNames.add("index");
        if (inField.hasCoords()) {
            int n = inField.getTrueNSpace();
            argNames.add("x");
            if (n < 0 || n > 1)
                argNames.add("y");
            if (n < 0 || n > 2)
                argNames.add("z");
        }
        if (lastFieldSchema == null || inFieldSchema == null || 
            !inFieldSchema.isCompatibleWith(lastFieldSchema, true)) {
            if (inField instanceof RegularField && ((RegularField)inField).getDimNum() > 1 ||
                inField instanceof IrregularField && !((IrregularField)inField).hasCells1D())
                pointCloudsBox.setSelected(true);
            for (int i = 0; i < inFieldSchema.getNComponents(); i++) {
                DataArraySchema dta = inFieldSchema.getComponentSchema(i);
                if (!skip(dta) && dta.getVectorLength() == 1)
                    argNames.add(dta.getName());
            }
            String[] argNamesArray = new String[argNames.size()];
            int lineCoord = -1;
            int previousArg = -1;
            for (int i = 0; i < argNames.size(); i++) {
                String argName = argNamesArray[i] = argNames.get(i);
                if (argName.equalsIgnoreCase(LINE_SLICE_COORDS))
                    lineCoord = i;
                if (argName.equalsIgnoreCase(params.getArgument()))
                     previousArg = i;   
            }
            
            argumentSelector.setModel(new DefaultComboBoxModel(argNamesArray));
            if (previousArg >= 0)
                argumentSelector.setSelectedIndex(previousArg);
            else if (lineCoord >= 0)
                argumentSelector.setSelectedIndex(lineCoord);
            else
                argumentSelector.setSelectedIndex(0);
            params.setArgument(argNamesArray[argumentSelector.getSelectedIndex()]);
            componentDisplayController.setInField(inField);
            float[] timeRange = componentDisplayController.getTimeRange();
            setTimeRange(timeRange[0], timeRange[1]);
        }
        params.setActive(true);
    }

    /**
     * Set the value of params
     *
     * @param params new value of params
     */
    public void setParams(GraphParams params)
    {
        this.params = params;
        componentDisplayController.setParams(params);
    }

    public void setMultiGraphGUI(JPanel multiGraphPostionPanel)
    {
        singleGraphPositionPanel.setVisible(false);
        GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        positionPanel.add(multiGraphPostionPanel, gridBagConstraints);
        legendPositionPanel.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        dataPanel = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        argumentSelector = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        argumentRangeSlider = new pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider();
        valuesPanel = new javax.swing.JPanel();
        valueRangeSlider = new pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider();
        commonRangeBox = new javax.swing.JCheckBox();
        componentDisplayController = new pl.edu.icm.visnow.lib.utils.graphing.ComponentDisplayController();
        pointCloudsBox = new javax.swing.JCheckBox();
        timeSlider = new pl.edu.icm.visnow.gui.widgets.FloatSlider();
        positionPanel = new javax.swing.JPanel();
        orientationCheckBox = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        singleGraphPositionPanel = new javax.swing.JPanel();
        setOriginButton = new javax.swing.JButton();
        setTopRightButton = new javax.swing.JButton();
        horizontalExtentsSlider = new pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        verticalExtentsSlider = new pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider();
        resetButton = new javax.swing.JButton();
        legendPositionPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        colorLegendXSlider = new javax.swing.JSlider();
        jLabel8 = new javax.swing.JLabel();
        colorLegendYSlider = new javax.swing.JSlider();
        presentationPanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        fgrColorEditor = new pl.edu.icm.visnow.gui.widgets.ColorEditor();
        autoColorBox = new javax.swing.JCheckBox();
        fontSizeSlider = new javax.swing.JSlider();
        lineWidthSlider = new pl.edu.icm.visnow.gui.widgets.FloatSlider();
        jPanel2 = new javax.swing.JPanel();
        titleField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        windowBgrButton = new javax.swing.JRadioButton();
        userBgrButton = new javax.swing.JRadioButton();
        bgrColorEditor = new pl.edu.icm.visnow.gui.widgets.ColorEditor();
        bgrTransparencySlider = new javax.swing.JSlider();
        frameBox = new javax.swing.JCheckBox();
        fillBox = new javax.swing.JCheckBox();
        refreshButton = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();

        setBackground(new java.awt.Color(204, 204, 204));
        setMinimumSize(new java.awt.Dimension(180, 600));
        setPreferredSize(new java.awt.Dimension(230, 700));
        setLayout(new java.awt.GridBagLayout());

        jTabbedPane1.setBackground(new java.awt.Color(237, 237, 237));
        jTabbedPane1.setInheritsPopupMenu(true);
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(150, 530));
        jTabbedPane1.setName("jTabbedPane1"); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(225, 540));

        dataPanel.setName("dataPanel"); // NOI18N
        dataPanel.setPreferredSize(new java.awt.Dimension(220, 503));
        dataPanel.setLayout(new java.awt.GridBagLayout());

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("Argument (x-axis)"));
        jPanel9.setMinimumSize(new java.awt.Dimension(150, 95));
        jPanel9.setName("jPanel9"); // NOI18N
        jPanel9.setOpaque(false);
        jPanel9.setPreferredSize(new java.awt.Dimension(200, 100));
        jPanel9.setLayout(new java.awt.GridBagLayout());

        argumentSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "index", "x", "y", "z" }));
        argumentSelector.setName("argumentSelector"); // NOI18N
        argumentSelector.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                argumentSelectorItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 41;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel9.add(argumentSelector, gridBagConstraints);

        jLabel3.setText("variable"); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 6, 0, 6);
        jPanel9.add(jLabel3, gridBagConstraints);

        argumentRangeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("range"));
        argumentRangeSlider.setName("argumentRangeSlider"); // NOI18N
        argumentRangeSlider.setPaintLabels(true);
        argumentRangeSlider.setPaintTicks(true);
        argumentRangeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                argumentRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 287;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel9.add(argumentRangeSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dataPanel.add(jPanel9, gridBagConstraints);

        valuesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Values shown"));
        valuesPanel.setMinimumSize(new java.awt.Dimension(180, 250));
        valuesPanel.setName("valuesPanel"); // NOI18N
        valuesPanel.setPreferredSize(new java.awt.Dimension(250, 330));
        valuesPanel.setLayout(new java.awt.GridBagLayout());

        valueRangeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("value range"));
        valueRangeSlider.setName("valueRangeSlider"); // NOI18N
        valueRangeSlider.setPaintLabels(true);
        valueRangeSlider.setPaintTicks(true);
        valueRangeSlider.setPreferredSize(new java.awt.Dimension(200, 60));
        valueRangeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                valueRangeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        valuesPanel.add(valueRangeSlider, gridBagConstraints);

        commonRangeBox.setText("common range for all values");
        commonRangeBox.setName("commonRangeBox"); // NOI18N
        commonRangeBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                commonRangeBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        valuesPanel.add(commonRangeBox, gridBagConstraints);

        componentDisplayController.setName("componentDisplayController"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        valuesPanel.add(componentDisplayController, gridBagConstraints);

        pointCloudsBox.setText("show data as point clouds");
        pointCloudsBox.setName("pointCloudsBox"); // NOI18N
        pointCloudsBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                pointCloudsBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 0);
        valuesPanel.add(pointCloudsBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        dataPanel.add(valuesPanel, gridBagConstraints);

        timeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("time"));
        timeSlider.setMaximumSize(new java.awt.Dimension(300, 80));
        timeSlider.setMinimumSize(new java.awt.Dimension(170, 64));
        timeSlider.setName("timeSlider"); // NOI18N
        timeSlider.setPreferredSize(new java.awt.Dimension(200, 64));
        timeSlider.setShowingFields(false);
        timeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                timeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        dataPanel.add(timeSlider, gridBagConstraints);

        jTabbedPane1.addTab("Data", dataPanel);

        positionPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        positionPanel.setName("positionPanel"); // NOI18N
        positionPanel.setPreferredSize(new java.awt.Dimension(288, 505));
        positionPanel.setLayout(new java.awt.GridBagLayout());

        orientationCheckBox.setText("vertical orientation");
        orientationCheckBox.setName("orientationCheckBox"); // NOI18N
        orientationCheckBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                orientationCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = -53;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        positionPanel.add(orientationCheckBox, gridBagConstraints);

        jPanel7.setName("jPanel7"); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 180, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        positionPanel.add(jPanel7, gridBagConstraints);

        singleGraphPositionPanel.setName("singleGraphPositionPanel"); // NOI18N
        singleGraphPositionPanel.setLayout(new java.awt.GridBagLayout());

        setOriginButton.setText("set lower left corner");
        setOriginButton.setName("setOriginButton"); // NOI18N
        setOriginButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                setOriginButtonActionPerformed(evt);
            }
        });
        singleGraphPositionPanel.add(setOriginButton, new java.awt.GridBagConstraints());

        setTopRightButton.setText("set top right corner");
        setTopRightButton.setName("setTopRightButton"); // NOI18N
        setTopRightButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                setTopRightButtonActionPerformed(evt);
            }
        });
        singleGraphPositionPanel.add(setTopRightButton, new java.awt.GridBagConstraints());

        horizontalExtentsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Relative horizontal extents"));
        horizontalExtentsSlider.setLow(10.0F);
        horizontalExtentsSlider.setName("horizontalExtentsSlider"); // NOI18N
        horizontalExtentsSlider.setUp(30.0F);
        horizontalExtentsSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                horizontalExtentsSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        singleGraphPositionPanel.add(horizontalExtentsSlider, gridBagConstraints);

        verticalExtentsSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Relative vertical extents"));
        verticalExtentsSlider.setLow(5.0F);
        verticalExtentsSlider.setName("verticalExtentsSlider"); // NOI18N
        verticalExtentsSlider.setUp(25.0F);
        verticalExtentsSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                verticalExtentsSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        singleGraphPositionPanel.add(verticalExtentsSlider, gridBagConstraints);

        resetButton.setText("reset graph area");
        resetButton.setName("resetButton"); // NOI18N
        resetButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resetButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        singleGraphPositionPanel.add(resetButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        positionPanel.add(singleGraphPositionPanel, gridBagConstraints);

        legendPositionPanel.setName("legendPositionPanel"); // NOI18N
        legendPositionPanel.setLayout(new java.awt.GridBagLayout());

        jLabel7.setText("color legend x");
        jLabel7.setName("jLabel7"); // NOI18N
        legendPositionPanel.add(jLabel7, new java.awt.GridBagConstraints());

        colorLegendXSlider.setMajorTickSpacing(5);
        colorLegendXSlider.setPaintTicks(true);
        colorLegendXSlider.setValue(100);
        colorLegendXSlider.setName("colorLegendXSlider"); // NOI18N
        colorLegendXSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                colorLegendXSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(8, 8, 8, 8);
        legendPositionPanel.add(colorLegendXSlider, gridBagConstraints);

        jLabel8.setText("color legend y");
        jLabel8.setName("jLabel8"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        legendPositionPanel.add(jLabel8, gridBagConstraints);

        colorLegendYSlider.setMajorTickSpacing(5);
        colorLegendYSlider.setPaintTicks(true);
        colorLegendYSlider.setValue(1);
        colorLegendYSlider.setName("colorLegendYSlider"); // NOI18N
        colorLegendYSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                colorLegendYSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 8, 8, 8);
        legendPositionPanel.add(colorLegendYSlider, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        positionPanel.add(legendPositionPanel, gridBagConstraints);

        jTabbedPane1.addTab("Arrangement", positionPanel);

        presentationPanel.setName("presentationPanel"); // NOI18N
        presentationPanel.setLayout(new java.awt.GridBagLayout());

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("borders, labels and axes color"));
        jPanel5.setName("jPanel5"); // NOI18N
        jPanel5.setLayout(new java.awt.GridBagLayout());

        fgrColorEditor.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        fgrColorEditor.setBrightness(100);
        fgrColorEditor.setName("fgrColorEditor"); // NOI18N
        fgrColorEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                fgrColorEditorStateChanged(evt);
            }
        });

        javax.swing.GroupLayout fgrColorEditorLayout = new javax.swing.GroupLayout(fgrColorEditor);
        fgrColorEditor.setLayout(fgrColorEditorLayout);
        fgrColorEditorLayout.setHorizontalGroup(
            fgrColorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 152, Short.MAX_VALUE)
        );
        fgrColorEditorLayout.setVerticalGroup(
            fgrColorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 19, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 104;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 11, 0, 10);
        jPanel5.add(fgrColorEditor, gridBagConstraints);

        autoColorBox.setSelected(true);
        autoColorBox.setText("auto");
        autoColorBox.setName("autoColorBox"); // NOI18N
        autoColorBox.setPreferredSize(new java.awt.Dimension(120, 24));
        autoColorBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                autoColorBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        jPanel5.add(autoColorBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        presentationPanel.add(jPanel5, gridBagConstraints);

        fontSizeSlider.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        fontSizeSlider.setMajorTickSpacing(5);
        fontSizeSlider.setMaximum(20);
        fontSizeSlider.setMinimum(5);
        fontSizeSlider.setMinorTickSpacing(1);
        fontSizeSlider.setPaintLabels(true);
        fontSizeSlider.setPaintTicks(true);
        fontSizeSlider.setValue(10);
        fontSizeSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "font size", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        fontSizeSlider.setName("fontSizeSlider"); // NOI18N
        fontSizeSlider.setPreferredSize(new java.awt.Dimension(200, 81));
        fontSizeSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                fontSizeSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 3, 2, 3);
        presentationPanel.add(fontSizeSlider, gridBagConstraints);

        lineWidthSlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "line width", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        lineWidthSlider.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lineWidthSlider.setMax(6.0F);
        lineWidthSlider.setMinimumSize(new java.awt.Dimension(90, 70));
        lineWidthSlider.setName("lineWidthSlider"); // NOI18N
        lineWidthSlider.setPreferredSize(new java.awt.Dimension(200, 81));
        lineWidthSlider.setShowingFields(false);
        lineWidthSlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                lineWidthSliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 4, 2, 4);
        presentationPanel.add(lineWidthSlider, gridBagConstraints);

        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setLayout(new java.awt.GridBagLayout());

        titleField.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        titleField.setName("titleField"); // NOI18N
        titleField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                titleFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 283;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanel2.add(titleField, gridBagConstraints);

        jLabel5.setText("title");
        jLabel5.setName("jLabel5"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 6, 2, 6);
        jPanel2.add(jLabel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 9);
        presentationPanel.add(jPanel2, gridBagConstraints);

        jLabel1.setText("x axis label");
        jLabel1.setName("jLabel1"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = -68;
        gridBagConstraints.ipady = -15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        presentationPanel.add(jLabel1, gridBagConstraints);

        jLabel2.setText("y axis label");
        jLabel2.setName("jLabel2"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = -68;
        gridBagConstraints.ipady = -15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        presentationPanel.add(jLabel2, gridBagConstraints);

        jPanel1.setName("jPanel1"); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 345, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        presentationPanel.add(jPanel1, gridBagConstraints);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("graph frame and background"));
        jPanel3.setMinimumSize(new java.awt.Dimension(153, 132));
        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setPreferredSize(new java.awt.Dimension(280, 142));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        buttonGroup1.add(windowBgrButton);
        windowBgrButton.setSelected(true);
        windowBgrButton.setText("window background color");
        windowBgrButton.setName("windowBgrButton"); // NOI18N
        windowBgrButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                windowBgrButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(windowBgrButton, gridBagConstraints);

        buttonGroup1.add(userBgrButton);
        userBgrButton.setText("user color");
        userBgrButton.setName("userBgrButton"); // NOI18N
        userBgrButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                userBgrButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 76;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(userBgrButton, gridBagConstraints);

        bgrColorEditor.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        bgrColorEditor.setBrightness(0);
        bgrColorEditor.setName("bgrColorEditor"); // NOI18N
        bgrColorEditor.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                bgrColorEditorStateChanged(evt);
            }
        });

        javax.swing.GroupLayout bgrColorEditorLayout = new javax.swing.GroupLayout(bgrColorEditor);
        bgrColorEditor.setLayout(bgrColorEditorLayout);
        bgrColorEditorLayout.setHorizontalGroup(
            bgrColorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 162, Short.MAX_VALUE)
        );
        bgrColorEditorLayout.setVerticalGroup(
            bgrColorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 22, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 49;
        gridBagConstraints.ipady = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 7);
        jPanel3.add(bgrColorEditor, gridBagConstraints);

        bgrTransparencySlider.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "opacity", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog.plain", 1, 12))); // NOI18N
        bgrTransparencySlider.setName("bgrTransparencySlider"); // NOI18N
        bgrTransparencySlider.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                bgrTransparencySliderStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 243;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 0, 6);
        jPanel3.add(bgrTransparencySlider, gridBagConstraints);

        frameBox.setText("draw frame");
        frameBox.setName("frameBox"); // NOI18N
        frameBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                frameBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(frameBox, gridBagConstraints);

        fillBox.setSelected(true);
        fillBox.setText("fill area");
        fillBox.setName("fillBox"); // NOI18N
        fillBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                fillBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel3.add(fillBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        presentationPanel.add(jPanel3, gridBagConstraints);

        jTabbedPane1.addTab("Presentation", presentationPanel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.weightx = 1.0;
        add(jTabbedPane1, gridBagConstraints);

        refreshButton.setText("refresh");
        refreshButton.setName("refreshButton"); // NOI18N
        refreshButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                refreshButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        add(refreshButton, gridBagConstraints);

        jPanel4.setName("jPanel4"); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 350, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        add(jPanel4, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void fgrColorEditorStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_fgrColorEditorStateChanged
    {//GEN-HEADEREND:event_fgrColorEditorStateChanged
        if (params != null)
            params.setColor(fgrColorEditor.getColor());
    }//GEN-LAST:event_fgrColorEditorStateChanged

    private void fontSizeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_fontSizeSliderStateChanged
    {//GEN-HEADEREND:event_fontSizeSliderStateChanged
        if (params != null && !fontSizeSlider.getValueIsAdjusting())
            params.setFontSize(fontSizeSlider.getValue() / 1000.f);
    }//GEN-LAST:event_fontSizeSliderStateChanged

    private void lineWidthSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_lineWidthSliderStateChanged
    {//GEN-HEADEREND:event_lineWidthSliderStateChanged
        if (params != null && !lineWidthSlider.isAdjusting())
            params.setLineWidth(lineWidthSlider.getVal());
    }//GEN-LAST:event_lineWidthSliderStateChanged

    private void titleFieldActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_titleFieldActionPerformed
    {//GEN-HEADEREND:event_titleFieldActionPerformed
        if (params != null)
            params.setTitle(titleField.getText());
    }//GEN-LAST:event_titleFieldActionPerformed

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_refreshButtonActionPerformed
    {//GEN-HEADEREND:event_refreshButtonActionPerformed
        if (params != null)
            params.setRefresh(true);
    }//GEN-LAST:event_refreshButtonActionPerformed

    private void orientationCheckBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_orientationCheckBoxActionPerformed
    {//GEN-HEADEREND:event_orientationCheckBoxActionPerformed
        if (params != null)
            params.setVertical(orientationCheckBox.isSelected());
    }//GEN-LAST:event_orientationCheckBoxActionPerformed

    private void argumentSelectorItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_argumentSelectorItemStateChanged
    {//GEN-HEADEREND:event_argumentSelectorItemStateChanged
        if (params != null)
            params.setArgument((String) argumentSelector.getSelectedItem());
    }//GEN-LAST:event_argumentSelectorItemStateChanged

    private void argumentRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_argumentRangeSliderStateChanged
    {//GEN-HEADEREND:event_argumentRangeSliderStateChanged

        float t0 = argumentRangeSlider.getBottomValue();
        float t1 = argumentRangeSlider.getTopValue();
        float v = (float).01 * (argumentRangeSlider.getMaximum() - argumentRangeSlider.getMinimum());
        if (t1 - t0 < .01 * v) {
            float t = t0 + t1;
            t1 = (t + v) / 2;
            t0 = (t - v) / 2;
        }
        if (params != null && !argumentRangeSlider.isAdjusting())
            params.setArgumentRange(t0, t1);      
    }//GEN-LAST:event_argumentRangeSliderStateChanged

    private void valueRangeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_valueRangeSliderStateChanged
    {//GEN-HEADEREND:event_valueRangeSliderStateChanged
        float t0 = valueRangeSlider.getBottomValue();
        float t1 = valueRangeSlider.getTopValue();
        float v = (float).01 * (valueRangeSlider.getMaximum() - valueRangeSlider.getMinimum());
        if (t1 - t0 < .01 * v) {
            float t = t0 + t1;
            t1 = (t + v) / 2;
            t0 = (t - v) / 2;
        }
        if (params != null && !valueRangeSlider.isAdjusting())
            params.setValueRange(t0, t1);
    }//GEN-LAST:event_valueRangeSliderStateChanged

    private void frameBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_frameBoxActionPerformed
    {//GEN-HEADEREND:event_frameBoxActionPerformed
        if (params != null)
            params.setDrawFrame(frameBox.isSelected());
    }//GEN-LAST:event_frameBoxActionPerformed

    private void fillBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_fillBoxActionPerformed
    {//GEN-HEADEREND:event_fillBoxActionPerformed
        boolean fill = fillBox.isSelected();
        if (params != null)
            params.setFillRect(fill);
        bgrColorEditor.setEnabled(fill);
        windowBgrButton.setEnabled(fill);
        userBgrButton.setEnabled(fill);
        bgrTransparencySlider.setEnabled(fill);
    }//GEN-LAST:event_fillBoxActionPerformed

    private void bgrColorEditorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_bgrColorEditorStateChanged
        if (params != null)
            params.setUserBgrColor(bgrColorEditor.getColor());
        
    }//GEN-LAST:event_bgrColorEditorStateChanged

    private void bgrSelect()
    {
        if (params != null) {
            params.setAutoBgr(windowBgrButton.isSelected());
            bgrColorEditor.setVisible(userBgrButton.isSelected());
        }
    }

    private void windowBgrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_windowBgrButtonActionPerformed
        bgrSelect();
    }//GEN-LAST:event_windowBgrButtonActionPerformed

    private void userBgrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userBgrButtonActionPerformed
        bgrSelect();
    }//GEN-LAST:event_userBgrButtonActionPerformed

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_resetButtonActionPerformed
    {//GEN-HEADEREND:event_resetButtonActionPerformed
        if (params != null) {
            params.setParameterActive(false);
            horizontalExtentsSlider.setLowUp(10.f, 30.f);
            verticalExtentsSlider.setLowUp(5.f, 25.f);
            params.setParameterActive(true);
            params.resetGraphArea();
        }
    }//GEN-LAST:event_resetButtonActionPerformed

    private void timeSliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_timeSliderStateChanged
    {//GEN-HEADEREND:event_timeSliderStateChanged
        if (params != null)
            params.setTime(timeSlider.getVal());
    }//GEN-LAST:event_timeSliderStateChanged

    private void bgrTransparencySliderStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_bgrTransparencySliderStateChanged
    {//GEN-HEADEREND:event_bgrTransparencySliderStateChanged
        if (params != null)
            params.setBgrTransparency(.01f * bgrTransparencySlider.getValue());
    }//GEN-LAST:event_bgrTransparencySliderStateChanged

    private void pointCloudsBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pointCloudsBoxActionPerformed
        if (params != null)
            params.setDrawPointCloud(pointCloudsBox.isSelected());
    }//GEN-LAST:event_pointCloudsBoxActionPerformed

    private void commonRangeBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_commonRangeBoxActionPerformed
        valueRangeSlider.setValues(0, 0, 100, 100);
        if (params != null) {
            params.setCommonRange(commonRangeBox.isSelected());
        }
    }//GEN-LAST:event_commonRangeBoxActionPerformed

    private void colorLegendXSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_colorLegendXSliderStateChanged
        if (params != null)
            params.setLegendXPosition(colorLegendXSlider.getValue() /100f);
    }//GEN-LAST:event_colorLegendXSliderStateChanged

    private void colorLegendYSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_colorLegendYSliderStateChanged
        if (params != null)
            params.setLegendYPosition(colorLegendYSlider.getValue() /100f);
    }//GEN-LAST:event_colorLegendYSliderStateChanged

    private void setOriginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setOriginButtonActionPerformed
        if (params != null)
            params.setUpdateOrigin(true);
    }//GEN-LAST:event_setOriginButtonActionPerformed

    private void setTopRightButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setTopRightButtonActionPerformed
        if (params != null)
            params.setUpdateTop(true);
    }//GEN-LAST:event_setTopRightButtonActionPerformed

    private void horizontalExtentsSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_horizontalExtentsSliderStateChanged
        if (params != null) {  
            params.setGraphWidth((horizontalExtentsSlider.getUp() - horizontalExtentsSlider.getLow()) / 100);
            params.setXPosition(horizontalExtentsSlider.getLow() / 100);
        }
    }//GEN-LAST:event_horizontalExtentsSliderStateChanged

    private void verticalExtentsSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_verticalExtentsSliderStateChanged
        if (params != null) {
            params.setGraphHeight((verticalExtentsSlider.getUp() - verticalExtentsSlider.getLow()) / 100);
            params.setYPosition(1 - verticalExtentsSlider.getLow() / 100);
        }
    }//GEN-LAST:event_verticalExtentsSliderStateChanged

    private void autoColorBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_autoColorBoxActionPerformed
    {//GEN-HEADEREND:event_autoColorBoxActionPerformed
        if (params != null)
            params.setColorAuto(autoColorBox.isSelected());
    }//GEN-LAST:event_autoColorBoxActionPerformed

    private final void updateComponentTable()
    {
        if (params != null)
            params.setDisplayedData(componentDisplayController.getDisplayedData());
        float[] timeRange = componentDisplayController.getTimeRange();
        setTimeRange(timeRange[0], timeRange[1]);
    }
    
    public void updatePositionSliders(float x, float w, float y, float h)
    {
        horizontalExtentsSlider.setLowUp(100 * x, 100 * (x + w));
        verticalExtentsSlider.setLowUp(100 * (1 - y), 100 * (1 - y + h));
    }
   
    public void setTimeRange(float startTime, float endTime) 
    {
        if (startTime < endTime) {
            timeSlider.setEnabled(true);
            timeSlider.setMinMax(startTime, endTime);
        }
        else 
            timeSlider.setEnabled(false);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider argumentRangeSlider;
    private javax.swing.JComboBox argumentSelector;
    private javax.swing.JCheckBox autoColorBox;
    private pl.edu.icm.visnow.gui.widgets.ColorEditor bgrColorEditor;
    private javax.swing.JSlider bgrTransparencySlider;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JSlider colorLegendXSlider;
    private javax.swing.JSlider colorLegendYSlider;
    private javax.swing.JCheckBox commonRangeBox;
    private pl.edu.icm.visnow.lib.utils.graphing.ComponentDisplayController componentDisplayController;
    private javax.swing.JPanel dataPanel;
    private pl.edu.icm.visnow.gui.widgets.ColorEditor fgrColorEditor;
    private javax.swing.JCheckBox fillBox;
    private javax.swing.JSlider fontSizeSlider;
    private javax.swing.JCheckBox frameBox;
    private pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider horizontalExtentsSlider;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel legendPositionPanel;
    private pl.edu.icm.visnow.gui.widgets.FloatSlider lineWidthSlider;
    private javax.swing.JCheckBox orientationCheckBox;
    private javax.swing.JCheckBox pointCloudsBox;
    private javax.swing.JPanel positionPanel;
    private javax.swing.JPanel presentationPanel;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton setOriginButton;
    private javax.swing.JButton setTopRightButton;
    private javax.swing.JPanel singleGraphPositionPanel;
    private pl.edu.icm.visnow.gui.widgets.FloatSlider timeSlider;
    private javax.swing.JTextField titleField;
    private javax.swing.JRadioButton userBgrButton;
    private pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.FloatSubRangeSlider valueRangeSlider;
    private javax.swing.JPanel valuesPanel;
    private pl.edu.icm.visnow.gui.widgets.FloatSubRangeSlider.ExtendedFloatSubRangeSlider verticalExtentsSlider;
    private javax.swing.JRadioButton windowBgrButton;
    // End of variables declaration//GEN-END:variables
}
