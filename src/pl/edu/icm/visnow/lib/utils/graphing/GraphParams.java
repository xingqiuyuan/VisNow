//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.graphing;

import java.awt.Color;
import pl.edu.icm.visnow.engine.core.ParameterEgg;
import pl.edu.icm.visnow.engine.core.ParameterType;
import pl.edu.icm.visnow.engine.core.Parameters;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GraphParams extends Parameters
{
    public static final  Color[] DEFAULT_COLORS = new Color[]{
        new Color(1.f, .5f, .5f), new Color(1.f, 1.f, .5f), new Color(.5f, 1.f, .5f),
        new Color(.5f, 1.f, 1.f), new Color(.5f, .5f, 1.f), new Color(1.f, .5f, 1.f),
        new Color(.8f, .8f, .8f)};
    
    public static final String EXTENTS = "extents";
    
    public static final String X_POS = "x position";
    public static final String Y_POS = "y position";
    public static final String LEGEND_X_POS = "legend x position";
    public static final String LEGEND_Y_POS = "legend y position";
    public static final String GRAPH_WIDTH = "graph width";
    public static final String GRAPH_HEIGHT = "graph height";
    public static final String COLOR = "color";
    public static final String COLOR_AUTO = "color auto";
    public static final String FONT_SIZE = "font size";
    public static final String LINE_WIDTH = "line width";
    public static final String TITLE = "title";
    public static final String SHOW_TITLE_IN_FRAME = "show title in frame";
    public static final String DATA = "data";
    public static final String COMMON_RANGE = "common range";
    public static final String REFRESH = "refresh";
    public static final String ORIENTATION = "orientation";
    public static final String ARGUMENT = "argument";
    public static final String ARGUMENT_RANGE = "argument range";
    public static final String VALUE_RANGE = "value range";
    public static final String DRAW_FRAME = "draw frame";
    public static final String FILL_RECT = "fill rectangle";
    public static final String WINDOW_BGR_COLOR = "window background color";
    public static final String USER_BGR_COLOR = "background color";
    public static final String BGR_TRANSP = "background transparency";
    public static final String AUTO_BACKGROUND = "background color from window";
    public static final String UPDATE_ORIGIN = "update origin";
    public static final String UPDATE_TOP = "update top";
    public static final String TIME = "time";
    public static final String POINT_CLOUD = "point cloud";
    public static final String SHOW_TOOLTIP = "show tooltip";
    public static final String TOOLTIP_POSITION = "tooltip position";
    
    private static final float DEFAULT_X =  .1f;
    private static final float DEFAULT_Y =  .95f;
    private static final float DEFAULT_WIDTH =  .2f;
    private static final float DEFAULT_HEIGHT = .2f;
    private static final float DEFAULT_T =  0.f;

    protected static ParameterEgg[] eggs = new ParameterEgg[]{
        new ParameterEgg<Float>(X_POS,                 ParameterType.dependent, DEFAULT_X),
        new ParameterEgg<Float>(Y_POS,                 ParameterType.dependent, DEFAULT_Y),
        new ParameterEgg<Float>(LEGEND_X_POS,          ParameterType.dependent, 1f),
        new ParameterEgg<Float>(LEGEND_Y_POS,          ParameterType.dependent, .01f),
        new ParameterEgg<Float>(GRAPH_WIDTH,           ParameterType.dependent, DEFAULT_WIDTH),
        new ParameterEgg<Float>(GRAPH_HEIGHT,          ParameterType.dependent, DEFAULT_HEIGHT),
        new ParameterEgg<Color>(COLOR,                 ParameterType.independent, Color.WHITE),
        new ParameterEgg<Boolean>(COLOR_AUTO,          ParameterType.independent, true),
        new ParameterEgg<Float>(BGR_TRANSP,            ParameterType.dependent, .5f),
        new ParameterEgg<Color>(WINDOW_BGR_COLOR,      ParameterType.independent, Color.BLACK),
        new ParameterEgg<Color>(USER_BGR_COLOR,        ParameterType.independent, Color.BLACK),
        new ParameterEgg<Boolean>(AUTO_BACKGROUND,     ParameterType.independent, true),
        new ParameterEgg<Float>(FONT_SIZE,             ParameterType.independent, .01f),
        new ParameterEgg<Float>(LINE_WIDTH,            ParameterType.independent, 1.f),
        new ParameterEgg<String>(TITLE,                ParameterType.independent, ""),
        new ParameterEgg<Boolean>(SHOW_TITLE_IN_FRAME, ParameterType.independent, true),
        new ParameterEgg<String>(ARGUMENT,             ParameterType.independent, ""),
        new ParameterEgg<float[]>(ARGUMENT_RANGE,      ParameterType.dependent, null),
        new ParameterEgg<DisplayedData[]>(DATA,        ParameterType.independent, null),
        new ParameterEgg<Boolean>(COMMON_RANGE,        ParameterType.dependent, true),
        new ParameterEgg<float[]>(VALUE_RANGE,         ParameterType.dependent, null),
        new ParameterEgg<Float>(TIME,                  ParameterType.dependent, DEFAULT_T),
        new ParameterEgg<Boolean>(POINT_CLOUD,         ParameterType.independent, false),
        new ParameterEgg<Boolean>(ORIENTATION,         ParameterType.independent, false),
        new ParameterEgg<Boolean>(REFRESH,             ParameterType.independent, false),
        new ParameterEgg<Boolean>(DRAW_FRAME,          ParameterType.independent, false),
        new ParameterEgg<Boolean>(FILL_RECT,           ParameterType.independent, true),
        new ParameterEgg<Boolean>(UPDATE_ORIGIN,       ParameterType.independent, false),
        new ParameterEgg<Boolean>(UPDATE_TOP,          ParameterType.independent, false),
        new ParameterEgg<Boolean>(SHOW_TOOLTIP,        ParameterType.independent, false),
        new ParameterEgg<int[]>(TOOLTIP_POSITION,      ParameterType.independent, new int[] {0, 0}),
    };

    public GraphParams()
    {
        super(eggs);
        setValue(DATA, new DisplayedData[]{new DisplayedData(0, Color.WHITE)});
        setValue(ARGUMENT, "index");
        setValue(ARGUMENT_RANGE, new float[]{0, 100});
        
        setValue(COMMON_RANGE, false);
        setValue(VALUE_RANGE, new float[]{0, 100});
    }
    
/**
 * x coordinate of lower left graph area corner relative to the window 
 * @return x coordinate of lower left graph area corner
 */
    public float getXPosition()
    {
        return (Float) getValue(X_POS);
    }
    
/**
 * Setter for graph area position
 * @param x new x coordinate of lower left graph area corner relative to the window
 */
    public void setXPosition(float x)
    {
        setValue(X_POS, x);
        fireParameterChanged(EXTENTS);
    }
    
/**
 * y coordinate of lower left graph area corner relative to the window 
 * @return y coordinate of lower left graph area corner
 */
    public float getYPosition()
    {
        return (Float) getValue(Y_POS);
    }

/**
 * Setter for graph area position
 * @param y new y coordinate of lower left graph area corner relative to the window
 */
    public void setYPosition(float y)
    {
        setValue(Y_POS, y);
        fireParameterChanged(EXTENTS);
    }
    
    
/**
 * x coordinate of lower left graph legend area corner relative to the window 
 * @return x coordinate of lower left graph legend area corner
 */
    public float getLegendXPosition()
    {
        return (Float) getValue(LEGEND_X_POS);
    }
    
/**
 * Setter for graph legend area position
 * @param x new x coordinate of lower left graph legend area corner relative to the window
 */
    public void setLegendXPosition(float x)
    {
        setValue(LEGEND_X_POS, x);
        fireParameterChanged(EXTENTS);
    }
    
/**
 * y coordinate of lower left graph  legend area corner relative to the window 
 * @return y coordinate of lower left graph legend area corner
 */
    public float getLegendYPosition()
    {
        return (Float) getValue(LEGEND_Y_POS);
    }

/**
 * Setter for graph legend area position
 * @param y new y coordinate of lower left legend graph area corner relative to the window
 */
    public void setLegendYPosition(float y)
    {
        setValue(LEGEND_Y_POS, y);
        fireParameterChanged(EXTENTS);
    }
    
/**
 * width of the graph area relative to the window 
 * @return width of the graph area 
 */
    public float getGraphWidth()
    {
        return (Float) getValue(GRAPH_WIDTH);
    }
    
/**
 * Setter for graph area width
 * @param width new value of graph area width relative to the window width
 */
    public void setGraphWidth(float width)
    {
        setValue(GRAPH_WIDTH, width);
        fireParameterChanged(EXTENTS);
    }
    
/**
 * height of graph area relative to the window height
 * @return height of the graph area 
 */
    public float getGraphHeight()
    {
        return (Float) getValue(GRAPH_HEIGHT);
    }

/**
 * Setter for graph area position
 * @param height new value of graph area height relative to the window height
 */
    public void setGraphHeight(float height)
    {
        setValue(GRAPH_HEIGHT, height);
        fireParameterChanged(EXTENTS);
    }
    
    public void setBottomLeftCorner(float x, float y)
    {
        setValue(X_POS, x);
        setValue(Y_POS, y);
        fireParameterChanged(EXTENTS);
    }
    
    public void setTopRightCorner(float x, float y)
    {
        if (x - getXPosition() > .1f  && getYPosition() - y > .1f) {
            setValue(GRAPH_WIDTH, x - getXPosition());
            setValue(GRAPH_HEIGHT, getYPosition() - y);
            fireParameterChanged(EXTENTS);
        }
    }
    
    
    
 /**
  * resets graph area to default rectangle near lower left window area
  */   
    public void resetGraphArea()
    {
        setValue(X_POS, DEFAULT_X);
        setValue(Y_POS, DEFAULT_Y);
        setValue(GRAPH_WIDTH, DEFAULT_WIDTH);
        setValue(GRAPH_HEIGHT, DEFAULT_HEIGHT);
        fireParameterChanged(EXTENTS);
    }
    
    public Color getColor()
    {
        return (Color) getValue(COLOR);
    }

    public void setColor(Color color)
    {
        setValue(COLOR, color);
        fireParameterChanged(COLOR);
    }

    public Color getUserBgrColor()
    {
        return (Color) getValue(USER_BGR_COLOR);
    }

    public void setUserBgrColor(Color color)
    {
        setValue(USER_BGR_COLOR, color);
        fireParameterChanged(COLOR);
    }

    public Color getWindowBgrColor()
    {
        return (Color) getValue(WINDOW_BGR_COLOR);
    }

    public void setWindowBgrColor(Color color)
    {
        setValue(WINDOW_BGR_COLOR, color);
        fireParameterChanged(COLOR);
    }

    public Float getBgrTransparency()
    {
        return (Float) getValue(BGR_TRANSP);
    }

    public void setBgrTransparency(float val)
    {
        setValue(BGR_TRANSP, val);
        fireStateChanged();
    }

    public boolean isAutoBgr()
    {
        return (Boolean) getValue(AUTO_BACKGROUND);
    }

    public void setAutoBgr(boolean cl)
    {
        setValue(AUTO_BACKGROUND, cl);
        fireParameterChanged(AUTO_BACKGROUND);
    }

    public DisplayedData[] getDisplayedData()
    {
        return (DisplayedData[]) getValue(DATA);
    }

    public void setDisplayedData(DisplayedData[] displayedData)
    {
        setValue(DATA, displayedData);
        fireParameterChanged(DATA);
    }

    public float getFontSize()
    {
        return (Float) getValue(FONT_SIZE);
    }

    public void setFontSize(float size)
    {
        setValue(FONT_SIZE, size);
        fireParameterChanged(FONT_SIZE);
    }

    public float getLineWidth()
    {
        return (Float) getValue(LINE_WIDTH);
    }

    public void setLineWidth(float width)
    {
        setValue(LINE_WIDTH, width);
        fireParameterChanged(LINE_WIDTH);
    }

    public String getTitle()
    {
        return (String) getValue(TITLE);
    }

    public void setTitle(String title)
    {
        setValue(TITLE, title);
        fireParameterChanged(TITLE);
    }
/**
 * indicates if title is displayed in frame (if multiple graphs are shown, title is displayed in common legend frame)
 * @return true if title is displayed in graph frame
 */
    public boolean isTitleInFrame()
    {
        return (Boolean) getValue(SHOW_TITLE_IN_FRAME);
    }

    public void setTitleInFrame(boolean inFrame)
    {
        setValue(SHOW_TITLE_IN_FRAME, inFrame);
        fireParameterChanged(SHOW_TITLE_IN_FRAME);
    }

    public String getArgument()
    {
        return (String) getValue(ARGUMENT);
    }

    public void setArgument(String arg)
    {
        setValue(ARGUMENT, arg);
        fireParameterChanged(ARGUMENT);
    }

    public float[] getArgumentRange()
    {
        float[] arg = (float[]) getValue(ARGUMENT_RANGE);
        if (arg[0] < arg[1])
            return arg;
        float m = (arg[0] + arg[1]) / 2;
        return new float[] {m - .1f, m+ .1f};
    }

    public void setArgumentRange(float low, float up)
    {
        float[] argumentRange = getArgumentRange();
        argumentRange[0] = low;
        argumentRange[1] = up;
        fireParameterChanged(ARGUMENT);
    }
    public float[] getValueRange()
    {
        return (float[]) getValue(VALUE_RANGE);
    }

    public void setCommonRange(boolean cl)
    {
        setValue(COMMON_RANGE, cl);
        fireParameterChanged(DATA);
    }

    public boolean isCommonRange()
    {
        return (Boolean) getValue(COMMON_RANGE);
    }

    public void setValueRange(float low, float up)
    {
        float[] valueRange = getValueRange();
        valueRange[0] = low;
        valueRange[1] = up;
        fireParameterChanged(DATA);
    }

    public void updateTable()
    {
        fireParameterChanged(DATA);
    }

    public boolean drawPointCloud()
    {
        return (Boolean) getValue(POINT_CLOUD);
    }

    public void setDrawPointCloud(boolean cl)
    {
        setValue(POINT_CLOUD, cl);
        fireParameterChanged(POINT_CLOUD);
    }
    
    public boolean vertical()
    {
        return (Boolean) getValue(ORIENTATION);
    }

    public void setVertical(boolean v)
    {
        setValue(ORIENTATION, v);
        fireParameterChanged(ORIENTATION);
    }

    public boolean refresh()
    {
        boolean r = (Boolean) getValue(REFRESH);
        setValue(REFRESH, false);
        return r;
    }

    public void setRefresh(boolean r)
    {
        setValue(REFRESH, false);
        fireParameterChanged(EXTENTS);
    }
    
    public boolean drawFrame()
    {
        boolean r = (Boolean) getValue(DRAW_FRAME);
        return r;
    }

    public void setDrawFrame(boolean r)
    {
        setValue(DRAW_FRAME, r);
        fireParameterChanged(DRAW_FRAME);
    }


    public boolean fillRect()
    {
        boolean r = (Boolean) getValue(FILL_RECT);
        return r;
    }

    public void setFillRect(boolean r)
    {
        setValue(FILL_RECT, r);
        fireParameterChanged(FILL_RECT);
    }
    
    public boolean updateOrigin()
    {
        return (Boolean) getValue(UPDATE_ORIGIN);
    }

    public void setUpdateOrigin(boolean r)
    {
        setValue(UPDATE_ORIGIN, r);
        fireParameterChanged(UPDATE_ORIGIN);
    }

    public boolean updateTop()
    {
        return (Boolean) getValue(UPDATE_TOP);
    }

    public void setUpdateTop(boolean r)
    {
        setValue(UPDATE_TOP, r);
        fireParameterChanged(UPDATE_TOP);
    }

    public float getTime()
    {
        return (Float)getValue(TIME);
    }
    
    public void setTime(float val)
    {
        setValue(TIME, val);
        fireParameterChanged(DATA);
    }
/**
 * getter for coordinates of tooltip point 
 * @return window coordinates of tooltip point
 */
    public int[] getTooltipPosition()
    {
        return (int[]) getValue(TOOLTIP_POSITION);
    }
    
/**
 * Setter for coordinates of tooltip point 
 * @param x window coordinates of tooltip point
 */
    public void setTooltipPosition(int[] x)
    {
        setValue(TOOLTIP_POSITION, x);
        setValue(SHOW_TOOLTIP, true);
        fireParameterChanged(EXTENTS);
    }
    
    public boolean showTooltip()
    {
        return (Boolean)getValue(SHOW_TOOLTIP);
    }
    
    public void setTooltip(boolean show)
    {
        setValue(SHOW_TOOLTIP, show);
        fireParameterChanged(EXTENTS);
    }
    
    public boolean colorAuto()
    {
        return (Boolean)getValue(COLOR_AUTO);
    }
    
    public void setColorAuto(boolean auto)
    {
        setValue(COLOR_AUTO, auto);
    }
    
    
}
