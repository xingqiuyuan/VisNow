//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.graphing;

import pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay;
import pl.edu.icm.visnow.lib.utils.probeInterfaces.MultiDisplay;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import javax.media.j3d.J3DGraphics2D;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import static pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class MultiGraphs extends MultiDisplay
{
    protected GraphParams graphParams = new GraphParams();
    
    public MultiGraphs()
    {
        graphParams.setTitleInFrame(false);
    }
    
    @Override
    public void addDisplay(IrregularField field)
    {
        addDisplay(field, new float[] {0, 0, 0});
    }
    
    public void addDisplay(IrregularField field, float[] center)
    {
        if (field != null &&
            (probesPosition != TOP && probesPosition != BOTTOM || displays.size() * 180 < windowWidth) &&
            (probesPosition != LEFT && probesPosition != RIGHT || displays.size() * 180 < windowHeight)) {
            displays.add(new GraphDisplay(field, graphParams, probesPosition, pointerLine, center));
            updateValueRanges();
            for (ProbeDisplay display : displays) {
                GraphDisplay grDisp = (GraphDisplay) display;
                grDisp.getGraphWorld().setInData(minima, maxima, mMinima, mMaxima, pMinima, physMinima, coeffs, exponents);
                grDisp.getGraphWorld().updateDisplayedArguments();
                grDisp.getGraphWorld().updateDisplayedValues();
            }
        }
    }
    
    public GraphDisplay pickedDisplay(int ix, int iy)
    {
        for (ProbeDisplay display : displays) {
            GraphDisplay graph = (GraphDisplay) display;
            if (graph.getGraphWorld().pointInside(ix, iy)) 
                return graph;
        }
        return null;
    }

    protected int[] probeOffset = {30, 60};
    protected float relMargin = .025f;

    public void setRelMargin(float relMargin) {
        this.relMargin = relMargin;
    }
    
    protected int currentXPosition = 0;
    protected int currentYPosition = 0;
    protected boolean packing = false;
    
    @Override
    public void draw2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height)
    {   
        windowWidth  = width;
        windowHeight = height;
        if (displays == null || displays.isEmpty())
            return;
        ltw.update();
        GraphWorld graphWorld = ((GraphDisplay)displays.get(0)).getGraphWorld();
        graphWorld.setDrawLegend(true);
        graphWorld.setDrawXLegend(true);
        graphWorld.prepareGraph(gr, width, height);
        graphAreaWidth  = graphWorld.getGraphAreaWidth();
        graphAreaHeight = graphWorld.getGraphAreaHeight();
        if (probesPosition == AT_POINT)
            for (int i = 0; i < displays.size(); i++) {
                GraphDisplay graph = (GraphDisplay)displays.get(i);
                graph.getGraphWorld().setDrawLegend(i == 0);
                graph.getGraphWorld().setDrawXLegend(i == 0);
                graph.updateScreenCoords(ltw);
                graph.draw(gr, ltw, width, height);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
            }
        else {
            xSort = probesPosition == TOP || probesPosition == BOTTOM;
            for (ProbeDisplay display : displays) {
                GraphDisplay graph = (GraphDisplay) display;
                graph.updateScreenCoords(ltw);
                graph.setxSort(xSort);
            }
            Collections.sort(displays);
            switch (probesPosition) {
            case TOP:
                currentXPosition = (int)(width * relMargin);
                currentYPosition = graphAreaHeight + (int)(height * relMargin);
                break;
            case BOTTOM:
                currentXPosition = (int)(width * relMargin);
                currentYPosition = height -  (int)(height * relMargin);
                break;
            case LEFT:
                currentXPosition = (int)(width * relMargin);
                currentYPosition = graphAreaHeight + (int)(height * relMargin);
                break;
            case RIGHT:
                currentXPosition = width - graphAreaWidth;
                currentYPosition = graphAreaHeight + (int)(height * relMargin);
                break;
            }
            for (int i = 0; i < displays.size(); i++) {
                GraphDisplay display = (GraphDisplay)displays.get(i); 
                graphWorld = display.getGraphWorld();
                int ix = currentXPosition + gap / 2;
                int iy = currentYPosition + gap / 2;
                switch (probesPosition) {
                case TOP:
                    graphWorld.setDrawLegend(i == 0 && !graphParams.vertical() ||
                                             i == displays.size() - 1 && graphParams.vertical());
                    graphWorld.setDrawXLegend(i == displays.size() - 1 && !graphParams.vertical() ||
                                              i == 0 && graphParams.vertical());
                    graphWorld.prepareGraph(gr, width, height);
                    currentXPosition += graphWorld.getGraphAreaWidth() + gap;
                    break;
                case BOTTOM:
                    graphWorld.setDrawLegend(i == 0 && !graphParams.vertical() ||
                                             i == displays.size() - 1 && graphParams.vertical());
                    graphWorld.setDrawXLegend(i == displays.size() - 1 && !graphParams.vertical() ||
                                              i == 0 && graphParams.vertical());
                    graphWorld.prepareGraph(gr, width, height);
                    currentXPosition += graphWorld.getGraphAreaWidth() + gap;
                    break;
                case LEFT:
                    graphWorld.setDrawLegend(i == 0 && !graphParams.vertical() ||
                                             i == displays.size() - 1 && graphParams.vertical());
                    graphWorld.setDrawXLegend(i == 0 && graphParams.vertical() ||
                                              i == displays.size() - 1 && !graphParams.vertical());
                    graphWorld.prepareGraph(gr, width, height);
                    currentYPosition += graphWorld.getGraphAreaHeight() + gap;
                    if (i == 0 && !graphParams.vertical())
                        currentYPosition -= graphWorld.getGraphLegendArea()[1];
                    break;
                case RIGHT:
                    graphWorld.setDrawLegend(i == 0 && !graphParams.vertical() ||
                                             i == displays.size() - 1 && graphParams.vertical());
                    graphWorld.setDrawXLegend(i == 0 && graphParams.vertical() ||
                                              i == displays.size() - 1 && !graphParams.vertical());
                    graphWorld.prepareGraph(gr, width, height);
                    currentYPosition += graphWorld.getGraphAreaHeight() + gap;
                    if (i == 0 && !graphParams.vertical())
                        currentYPosition -= graphWorld.getGraphLegendArea()[1];
                    break;
                }
                display.drawOnMargin(gr, ltw, width, height, 1f, i, ix, iy); 
            }
        }
    }

    public GraphParams getGraphParams() {
        return graphParams;
    }

    public boolean isPacking()
    {
        return packing;
    }
    
    @Override
    public void updateTimeRange()
    {
        if (graphParams.getDisplayedData() == null || graphParams.getDisplayedData().length < 1)
            return;
        startTime =  Float.MAX_VALUE;
        endTime   = -Float.MAX_VALUE;
        DisplayedData[] displayedData = graphParams.getDisplayedData();
        try {        
            for (int i = 0, j = 0; i < displayedData.length; i++) 
            if (displayedData[i].isDisplayed()) {
                int iData = displayedData[i].getIndex();
                for (ProbeDisplay display : displays) {
                    DataArray da = display.getField().getComponent(iData);
                    if (da.getStartTime() < startTime)
                        startTime = da.getStartTime();
                    if (da.getEndTime() > endTime)
                        endTime = da.getEndTime();
                }
            }
        } catch (ConcurrentModificationException e) {
        }
        if (startTime > endTime)
            startTime = endTime = 0;
    }
    
}
