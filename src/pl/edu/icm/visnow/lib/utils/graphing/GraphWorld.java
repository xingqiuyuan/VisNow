//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.graphing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import java.util.Arrays;
import javax.media.j3d.J3DGraphics2D;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.lib.utils.Range;
import pl.edu.icm.visnow.geometries.objects.Geometry2D;
import static pl.edu.icm.visnow.lib.utils.graphing.GraphParams.ARGUMENT;
import static pl.edu.icm.visnow.lib.utils.graphing.GraphParams.DATA;
import static pl.edu.icm.visnow.lib.utils.graphing.GraphParams.TIME;
import pl.edu.icm.visnow.lib.utils.sort.IndirectComparator;
import pl.edu.icm.visnow.lib.utils.sort.IndirectSort;

/**
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public class GraphWorld  extends Geometry2D {

    protected GraphParams params;
    protected J3DGraphics2D gr = null;
    protected int width = 100, height = 100;
    
    protected Field inField = null;
    protected RegularField inRegularField = null;
    protected IrregularField inIrregularField = null;
    protected int dim;
    
    protected DataArray argData;
    protected DisplayedData[] displayedData;
    
    protected int nComps;
    
    protected boolean computeOrig = true;
    protected int origX = 0, 
                  origY = 0; // window coords of the lower left corner of graph area 
    
    protected int graphWidth, graphHeight, lastw = -1, lasth = -1;
    protected int graphAreaWidth, graphAreaHeight;
    protected int graphAreaX,     graphAreaY;
    protected Range hRange, hLegendRange, vRange, vLegendRange;
    protected float dh, dv;
    
    protected float graphXMin, graphXMax;
    protected float graphYMin, graphYMax;
    protected float graphHMin, graphHMax, graphVMin, graphVMax;
    protected String hFormat, vFormat;
    protected int leftMargin, rightMargin, topMargin, bottomMargin;

    protected float[] args;
    protected float physXMin = 0, physXMax = 10;
    protected String argName = "";

    protected float[][] values;
    protected float[] physMins, physMaxs;
    protected float[] minima, maxima;
    protected float[] mMinima, mMaxima;
    protected float[] pMinima, physMinima, coeffs;
    protected int[] exponents;
    
    protected float physYMin = 0, physYMax = 10;
    protected Color bgr = Color.BLACK;
    protected Color fgr = Color.LIGHT_GRAY;
    protected Color[] graphColors;
    protected Stroke[] graphStrokes;
    protected String[] valueNames = {""};
    protected String[] valueUnits = {""};
    protected int[] valueExponents = {0};
    
    protected int fontHeight = 15;
    protected String longestCmpName = "";
    protected int[] graphLegendArea = new int[2];
    
    protected boolean isMulti = false;
    protected boolean drawLegend = true;
    protected boolean drawXLegend = true;
    
    protected int sortedArgs = 0;
    
    public GraphWorld(GraphParams params, boolean isMulti, boolean drawLegend)
    {
        super();
        this.params = params;
        this.isMulti = isMulti;
        this.drawLegend = drawLegend;
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (inField == null)
                    return;
                switch (name) {
                case ARGUMENT:
                    updateDisplayedArguments();
                    break;
                case DATA:
                case TIME:
                    updateDisplayedValues();
                    break;
                default:
                }
            }
        });
        name = "graph world";
    }
    
    boolean isDark(Color c)
    {
        float[] v = new float[3];
        c.getRGBColorComponents(v);
        return 0.299 * v[0] + 0.587 * v[1] + 0.114 * v[2] < .5;
    }
    
    public final void setInField(Field inField)
    {
        this.inField = inField;
        if (inField instanceof RegularField) {
            inRegularField = (RegularField)inField;
            inIrregularField = null;
        }
        else {
            inIrregularField = (IrregularField)inField;
            inRegularField = null;
        }
        dim = (int)inField.getNNodes();
    }
    
    public void setInData(float[] minima,  float[] maxima,
                          float[] mMinima, float[] mMaxima,
                          float[] pMinima, float[] physMinima, 
                          float[] coeffs,  int[] exponents)
    {
        if (inField instanceof RegularField) {
            inRegularField = (RegularField)inField;
            inIrregularField = null;
        }
        else {
            inIrregularField = (IrregularField)inField;
            inRegularField = null;
        }
        dim = (int)inField.getNNodes();
        this.minima     = minima;
        this.maxima     = maxima;
        this.mMinima    = mMinima;
        this.mMaxima    = mMaxima;
        this.pMinima    = pMinima;
        this.physMinima = physMinima;
        this.coeffs     = coeffs;
        this.exponents  = exponents;
        updateDisplayedArguments();
        updateDisplayedValues();
    }
    
    public  void updateDisplayedArguments()
    {
        if (params.getDisplayedData() == null || params.getDisplayedData().length < 1)
            return;
        float[] inCoords = null;
        if (params.getArgument().equalsIgnoreCase("x") ||
            params.getArgument().equalsIgnoreCase("y") ||
            params.getArgument().equalsIgnoreCase("z")) {
            if (inField.hasCoords())
                inCoords = inField.getCoords(0).getData();
            else
                inCoords = inRegularField.getCoordsFromAffine().getData();
        }
        float xMin, xMax, d;
        if (inField == null || dim <= 0)
            return;
        args = new float[dim];
        switch (params.getArgument()) {
        case "index":
            for (int i = 0; i < dim; i++) 
                args[i] = i;
            break;
        case "x":
            for (int i = 0; i < dim; i++) 
                args[i] = inCoords[3 * i];
            break;
        case "y":
            for (int i = 0; i < dim; i++) 
                args[i] = inCoords[3 * i + 1];
            break;
        case "z":
            for (int i = 0; i < dim; i++) 
                args[i] =  inCoords[3 * i + 2];
            break;
        default:
            argData = inField.getComponent(params.getArgument());
            if (argData == null || !argData.isNumeric())
                return;
            argData.setCurrentTime(params.getTime());
            physXMin = (float)argData.getPreferredPhysMinValue();
            physXMax = (float)argData.getPreferredPhysMaxValue();
            xMin = (float)argData.getPreferredMinValue();
            xMax = (float)argData.getPreferredMaxValue();
            d = 1;
            if (xMax > xMin)
                d = (physXMax - physXMin) / (xMax - xMin);
            else {
                xMax -= .5f;
                xMin -= .5f;
            }
            LargeArray data;
            if (argData.getVectorLength() == 1)
                data = argData.getRawArray();
            else
                data = argData.getVectorNorms();
            for (int i = 0; i < dim; i++) 
                args[i] = physXMin + d * (data.getFloat(i) - xMin);
        }
        sortedArgs = (int)Math.signum(args[dim - 1] - args[0]);
        for (int i = 1; i < dim; i++) 
            if ((int)Math.signum(args[i] - args[i - 1]) != sortedArgs) {
                sortedArgs = 0;
                break;
            }
        setArgData(params.getArgument(), args);
    }
    

    public void updateDisplayedValues()
    {
        displayedData = params.getDisplayedData();
        if (displayedData == null || displayedData.length < 1 ||
            minima == null || maxima == null)
            return;
        int nData = 0;
        int maxCmpNameLen = 0;
        for (DisplayedData dData : displayedData)
            if (dData.isDisplayed())
                nData += 1; 
        values = new float[nData][dim];
        physMins = new float[nData];
        physMaxs = new float[nData];
        valueNames = new String[nData];
        valueUnits = new String[nData];
        valueExponents = new int[nData];
        graphColors = new Color[nData];
        graphStrokes = new Stroke[nData];
        for (int i = 0, j = 0; i < displayedData.length; i++) 
            if (displayedData[i].isDisplayed()) {
                int iData = displayedData[i].getIndex();
                argData = inField.getComponent(iData);
                if (argData == null || !argData.isNumeric())
                    continue;
                graphColors[j] = displayedData[i].getColor();
                graphStrokes[j] = displayedData[i].getStroke();
                valueNames[j] = argData.getName();
                valueUnits[j] = argData.getUnit();
                float[] data;
                if (argData.getVectorLength() == 1)
                    data = argData.getRawArray(params.getTime()).getFloatData();
                else {
                    int vLen = argData.getVectorLength();
                    float[]  dt = argData.getRawArray(params.getTime()).getFloatData();
                    data = new float[dim];
                    for (int k = 0; k < dim; k++) {
                        double norm = 0;
                        for (int l = 0; l < vLen; l++)
                            norm += dt[vLen * k + l] * dt[vLen * k + l];
                        data[k] = (float)Math.sqrt(norm);
                    }
                }
                
                float pMin  = (float)argData.getPreferredMinValue();
                float phMin = (float)argData.getPreferredPhysMinValue();
                float coeff = (float)((argData.getPreferredPhysMaxValue() - phMin) / 
                                                      (argData.getPreferredMaxValue() - pMin));
                for (int k = 0; k < dim; k++) 
                    values[j][k] = phMin + coeff * (data[k] - pMin);
                if (params.isCommonRange()) {
                    physMins[j] = mMinima[i];
                    physMaxs[j] = mMaxima[i];
                    float exp = valueExponents [j] = exponents[i];
                    float scale = (float)Math.pow(10, -exp);
                    for (int k = 0; k < dim; k++) 
                        values[j][k] *= scale;
                }
                else {
                    physMins[j] = minima[i];
                    physMaxs[j] = maxima[i];
                    valueExponents [j] = 0;
                }
                String ls = legendString(j);
                if (maxCmpNameLen < ls.length()) {
                    maxCmpNameLen = ls.length();
                    longestCmpName  = ls;
                }
                j += 1;
            }
        updateMinMax(physMins, physMaxs);
    }

    
    public int[] computeLegendArea()
    {
        fontHeight = (int) (height * params.getFontSize());
        gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
        FontMetrics fm = gr.getFontMetrics();
        if (params.vertical()) {
            graphLegendArea[0] = 2 * fontHeight + fm.stringWidth(longestCmpName);
            graphLegendArea[1] = values.length * (fontHeight + 8) + 10;
        }
        else {
            graphLegendArea[0] = 80 + 2 * fontHeight + fm.stringWidth(longestCmpName);
            graphLegendArea[1] = values.length * (fontHeight + 3) + 10;
        }
        if (!params.isTitleInFrame() && params.getTitle() != null && !params.getTitle().isEmpty()) { 
            graphLegendArea[0] = Math.max(graphLegendArea[0], 10 + (int)(1.7 * fm.stringWidth(params.getTitle())));
            graphLegendArea[1] += 2 * fontHeight + 5;
        }
        return graphLegendArea;
    }
    
    public boolean pointInside(int ix, int iy)
    {
        return ix >= origX && ix <= origX + graphWidth &&
               iy >= origY - graphHeight && iy <= origY;
    }

    void prepareGraph(J3DGraphics2D gr, int width, int height)
    {
        if (params.getDisplayedData() == null || params.getDisplayedData().length < 1)
            return;
        this.gr = gr;
        this.width = width;
        this.height = height;
        computeLegendArea();
        fontHeight = (int) (height * params.getFontSize());
        gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
        FontMetrics fm = gr.getFontMetrics();
        topMargin = bottomMargin = 2 * fontHeight; // bottom space for axis legend, top space for vertical axis label and title
        if (!params.vertical() && drawLegend)
            topMargin = graphLegendArea[1] + fontHeight;
        if (!params.getTitle().isEmpty() && params.isTitleInFrame())
            topMargin  += 2 * fontHeight;
        rightMargin = 10;
        if (params.vertical() && drawLegend)  // space for axis description + optional component names
            rightMargin += 2 * fontHeight + graphLegendArea[0];
        if (!params.vertical() && drawXLegend)
            rightMargin = (int)(2 * fontHeight + fm.stringWidth(argName + " "));

        graphXMax = physXMin + (physXMax - physXMin) * params.getArgumentRange()[1] / 100;
        graphXMin = physXMin + (physXMax - physXMin) * params.getArgumentRange()[0] / 100; // selected argument range
        graphYMax = physYMin + (physYMax - physYMin) * params.getValueRange()[1] / 100;
        graphYMin = physYMin + (physYMax - physYMin) * params.getValueRange()[0] / 100;    // selected values range

        graphWidth  = (int)(width  * params.getGraphWidth());  //horizontal graph area extent 
        graphHeight = (int)(height * params.getGraphHeight());   //vertical graph area extent 

        if (params.vertical()) {
            graphHMin = graphYMin;
            graphHMax = graphYMax;
            graphVMin = graphXMin;
            graphVMax = graphXMax;
        }
        else {
            graphVMin = graphYMin;
            graphVMax = graphYMax;
            graphHMin = graphXMin;
            graphHMax = graphXMax;
        }
        dh = graphWidth / (graphHMax - graphHMin);
        dv = graphHeight / (graphVMax - graphVMin);
        if (computeOrig) {       
            origX = (int) (width * params.getXPosition());
            origY = (int) (height * params.getYPosition());
        }
        
        /*
        computation of round values for ticks and labels for vertical axis
        */
        vRange       = new Range(graphHeight , graphVMin, graphVMax, false);
        vFormat = Range.formatString(graphVMin, graphVMax, graphHeight / (3 * fm.getHeight()), false);
        vLegendRange = new Range(graphHeight / (5 * fontHeight) , graphVMin, graphVMax, false);
        leftMargin = fm.stringWidth(String.format(vFormat, graphVMax)) + 2 * fontHeight;
        /*
        computetion of round values for ticks and labels for horizontal axis
        */
        hRange       = new Range(graphWidth , graphHMin, graphHMax, false);
        hFormat = Range.formatString(graphHMin, graphHMax, graphWidth / (10 * fm.getHeight()), false);
        int xll = fm.stringWidth("##.###    ");
        hFormat = Range.formatString(graphHMin, graphHMax, graphWidth / xll, false);
        hLegendRange = new Range(graphWidth / xll , graphHMin, graphHMax, false);
        graphAreaWidth = graphWidth + leftMargin + rightMargin;
        graphAreaHeight = graphHeight + topMargin + bottomMargin;
        graphAreaX = origX - leftMargin;
        graphAreaY = origY - graphHeight - topMargin;
    }
    
    public int[] getBackgroundDims()
    {
        return new int[]{graphAreaX, graphAreaY, graphAreaWidth, graphAreaHeight};
    }

    private void drawBackground()
    {   
        if (params.fillRect()) {
            bgr = params.isAutoBgr() ? params.getWindowBgrColor() : params.getUserBgrColor();
            float[] colCmp = new float[4];
            bgr.getComponents(colCmp);
            gr.setColor(new Color(colCmp[0], colCmp[1], colCmp[2], params.getBgrTransparency()));
            gr.fillRect(graphAreaX, graphAreaY, graphAreaWidth, graphAreaHeight);
        }
        if (params.drawFrame()) {
            gr.setStroke(new BasicStroke(params.getLineWidth()));
            gr.setColor(fgr);
            gr.drawRect(graphAreaX, graphAreaY, graphAreaWidth, graphAreaHeight);
        }
        if (params.getTitle() != null && !params.getTitle().isEmpty() && params.isTitleInFrame()) {
            gr.setColor(params.getColor());
            gr.setFont(new java.awt.Font("Dialog", 0, 2 * fontHeight));
            FontMetrics fm = gr.getFontMetrics();
            int titleWidth = fm.stringWidth(params.getTitle());
            gr.drawString(params.getTitle(), graphAreaX  + (graphAreaWidth - titleWidth) / 2, graphAreaY + 5 + 2 * fontHeight);
        }
        gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
    }
    
    private String legendString(int i)
    {
        String decimal = "";
        switch (valueExponents[i]) {
            case 0: 
                break;
            case 1:
                decimal = "*10";
                break;
            case 2:
                decimal = "*100";
                break;
            case 3:
                decimal = "*1000";
                break;
            case -1:
                decimal = "/10";
                break;
            case -2:
                decimal = "/100";
                break;
            case -3:
                decimal = "/1000";
                break;
            default:
                if (valueExponents[i] < 0)
                    decimal = "/10^" + Math.abs(valueExponents[i]);
                else
                    decimal = "*10^" + Math.abs(valueExponents[i]);
        }
        if (valueUnits[i] != null && 
           !valueUnits[i].isEmpty() && 
           !valueUnits[i].equals("1"))
            return valueNames[i] + "(" + decimal + valueUnits[i] + ")";
        else if (!decimal.isEmpty())
            return valueNames[i] + "(" + decimal + ")";
        else
            return valueNames[i];
    }
    
    public void drawColorLegend(int ix, int iy)
    {
        gr.setColor(fgr);
        int titleHeight = 0;
        if (params.getTitle() != null && !params.getTitle().isEmpty() && !params.isTitleInFrame()) {
            gr.setFont(new java.awt.Font("Dialog", 0, (int)(1.5 * fontHeight)));
            FontMetrics fm = gr.getFontMetrics();
            titleHeight = 2 * fontHeight + 5;
            int titleWidth = fm.stringWidth(params.getTitle());
            gr.drawString(params.getTitle(), ix  + (graphLegendArea[0] - titleWidth) / 2, iy + 2 * fontHeight);
            gr.setFont(new java.awt.Font("Dialog", 0, fontHeight));
        }
        iy += titleHeight;
        for (int i = 0; i < values.length; i++) {
            gr.setColor(graphColors[i]);
            gr.setStroke(graphStrokes[i]);
            if (params.vertical()) {
                gr.drawLine(ix + 5,  iy + i * (8 + fontHeight) + fontHeight + 6, 
                            ix + 75, iy + i * (8 + fontHeight) + fontHeight + 6);
                gr.drawString(legendString(i), ix + 5, iy + i  * (8 + fontHeight) + fontHeight + 2);
            }
            else {
                gr.drawLine(ix + 5,  iy + i * (3 + fontHeight) + fontHeight, 
                            ix + 75, iy + i * (3 + fontHeight) + fontHeight);
                gr.drawString(legendString(i), ix + 82, iy + (i + 1) * (3 + fontHeight));
            }
        }
        gr.setColor(params.getColor());
        gr.setStroke(new BasicStroke(params.getLineWidth()));
    }

    private void drawAxes()
    {   
        GeneralPath axes = new GeneralPath();
        gr.setStroke(new BasicStroke(params.getLineWidth()));
        gr.setColor(fgr);
        axes.moveTo(origX, origY - graphHeight);
        axes.lineTo(origX, origY);
        axes.lineTo(origX + graphWidth, origY);
        gr.draw(axes);
        if (drawXLegend) {
            if (params.vertical()) 
                gr.drawString(argName, origX - 5, origY - graphHeight - 5);
            else 
                gr.drawString(argName, origX + graphWidth + 5,  origY + 5);
        }   
        GeneralPath ticklines = new GeneralPath();
        gr.setStroke(new BasicStroke(1.f, BasicStroke.CAP_ROUND,
                                     BasicStroke.JOIN_ROUND, 1.0f, new float[]{1, 3}, 0));
        for (float horizontalTick = hLegendRange.getRange()[0]; 
                   horizontalTick <= hLegendRange.getRange()[1]; 
                   horizontalTick += hLegendRange.getStep()) 
            if (horizontalTick >= graphHMin  && horizontalTick <= graphHMax) {
                float xs = origX + (horizontalTick - graphHMin) * dh;
                ticklines.moveTo(xs, origY);
                ticklines.lineTo(xs, origY - graphHeight);
                gr.drawString(String.format(hFormat, horizontalTick), xs - 5, origY + 3 + fontHeight);
            }

        for (float verticalTick = vLegendRange.getRange()[0]; 
                   verticalTick <= vLegendRange.getRange()[1]; 
                   verticalTick += vLegendRange.getStep()) 
            if (verticalTick >= graphVMin  && verticalTick <= graphVMax) {
                float ys = origY - (verticalTick - graphVMin) * dv;
                ticklines.moveTo(origX, ys);
                ticklines.lineTo(origX + graphWidth, ys);
                gr.drawString(String.format(vFormat, verticalTick), graphAreaX + 5, ys);
            }
        gr.draw(ticklines);
    }

    private int[] toScr(float x, float y)
    {
        return new int[] {(int)(origX + (x - graphHMin) * dh), (int)(origY - (y - graphVMin) * dv)};
    }

    private float[] toScrFloat(float x, float y)
    {
        if (params.vertical())
            return new float[]{origX + (x - graphHMin) * dh, origY - (y - graphVMin) * dv};
        else
            return new float[]{origX + (y - graphHMin) * dh, origY - (x - graphVMin) * dv};
    }

    private float[] fromScr(int ix, int iy)
    {
        return new float[]{(ix - origX) / dh + graphHMin, (origY - iy) / dv + graphVMin};
    }
    
    private int[] nodes;
    
    private void drawGraphs()
    {
        boolean verticalPlot = params.vertical();
        byte mask[] = null;
        if (inField.hasMask())
            mask = inField.getCurrentMask().getData();
        gr.setClip(origX - 3 , origY - graphHeight - 3, graphWidth + 6, graphHeight + 6);
        for (int drawable = 0; drawable < values.length; drawable++) {
            gr.setColor(graphColors[drawable]);
            gr.setStroke(graphStrokes[drawable]);
            float[] grData = values[drawable];
            if (params.drawPointCloud()) {
                for (int i = 0; i < grData.length; i++) {
                    if (mask != null && mask[i] == 0)
                        continue;
                    int[] p = verticalPlot ? toScr(grData[i], args[i]) : toScr(args[i], grData[i]);
                    gr.drawLine(p[0], p[1], p[0], p[1] + 1);
                }
            } else if (inField instanceof RegularField) {
                GeneralPath path = new GeneralPath();
                float[] p = toScrFloat(grData[0], args[0]);
                path.moveTo(p[0], p[1]);
                for (int j = 1; j < grData.length; j++) {
                    p = toScrFloat(grData[j], args[j]);
                    if (mask != null && mask[j] == 0)
                        path.moveTo(p[0], p[1]);
                    else
                        path.lineTo(p[0], p[1]);
                }
                gr.draw(path);
            } else {
                byte[] drawnNodes = new byte[grData.length];
                Arrays.fill(drawnNodes, (byte) 0);
                GeneralPath path = new GeneralPath();
                for (CellSet cs : inIrregularField.getCellSets()) {
                    if (cs.getBoundaryCellArray(CellType.SEGMENT) != null) {
                        nodes = cs.getBoundaryCellArray(CellType.SEGMENT).getNodes();
                        int[] sorted = IndirectSort.indirectSort(nodes.length / 2, 
                                new IndirectComparator(){
                                    @Override
                                    public int compare(int i, int j)
                                    {
                                        return nodes[2 * i] < nodes[2 * j] ? 1 : -1;
                                    }
                                });
                        int k1 = -1, k0 = -1;
                        for (int i = 1; i < sorted.length; i ++) {
                            k0 = nodes[2 * sorted[i]];
                            drawnNodes[k0] = (byte) 1;
                            float[] p = toScrFloat(grData[k0], args[k0]);
                            if (k0 != k1)
                                path.moveTo(p[0], p[1]);
                            k1 = nodes[2 * sorted[i] + 1];
                            drawnNodes[k1] = (byte) 1;
                            p = toScrFloat(grData[k1], args[k1]);
                            path.lineTo(p[0], p[1]);
                        }
                    }
                }
                gr.draw(path);
                for (int i = 0; i < grData.length; i++) {
                    if (drawnNodes[i] == (byte) 0) {
                        int[] p = verticalPlot ? toScr(grData[i], args[i]) : toScr(args[i], grData[i]);
                        gr.drawLine(p[0], p[1], p[0], p[1]);
                    }
                }
            }
        }
        gr.setClip(0,0,width,height);
        gr.setStroke(new BasicStroke(params.getLineWidth()));
    }
    
    private void showTooltip(int ix, int iy)
    {
        if (params.drawPointCloud() || !pointInside(ix, iy))
            return;
        float[] v = fromScr(ix, iy);
        if (sortedArgs == 0)
            return;
        float arg = params.vertical() ? v[1] : v[0];
        int index = java.util.Arrays.binarySearch(args, arg);
        gr.setFont(new java.awt.Font(Font.DIALOG_INPUT, 0, fontHeight));
        FontMetrics fm = gr.getFontMetrics();
        int nameWidth = fm.stringWidth(params.getArgument());
        for (int i = 0; i < values.length; i++) {
            int w = fm.stringWidth(valueNames[i]);
            if (w > nameWidth)
                nameWidth = w;
        }
        nameWidth += 8;
        int rectWidth = fm.stringWidth("000000.000") + nameWidth + 15;
        int rectHeight = (values.length + 1) * (fontHeight + 4) + 4;
        //compute position
        int xPos = ix, yPos = iy;
        int lMin, lMax;
        if (yPos < rectHeight)
            yPos += rectHeight;
        if (xPos - nameWidth > 5)
            xPos -=  nameWidth;
        if (xPos + rectWidth > width)
            xPos = width - rectWidth;
        if (params.vertical()) {
            lMin = origX;
            lMax = origX + graphWidth;
        }
        else {
            lMin = origY;
            lMax = origY - graphHeight;
        }
        float[] colCmp = new float[3];
        bgr.getRGBColorComponents(colCmp);
        gr.setColor(new Color(colCmp[0], colCmp[1], colCmp[2], .5f + params.getBgrTransparency() / 2));
        gr.fillRect(xPos, yPos - rectHeight, rectWidth, rectHeight);
        gr.setColor(fgr);
        gr.drawRect(xPos, yPos - rectHeight, rectWidth, rectHeight);
        
        if (params.vertical()) 
            gr.drawLine(lMin, iy, lMax, iy);
        else
            gr.drawLine(ix, lMin, ix, lMax);
        int y = yPos - rectHeight + 3 + fontHeight;
        gr.drawString(params.getArgument(), xPos + 3, y);
        gr.drawString(String.format("%10.3f", arg), xPos + 6 + nameWidth, y);
        y += fontHeight + 6;
        for (int i = 0; i < values.length; i++) {
            gr.setColor(graphColors[i]);
            gr.drawString(valueNames[i], xPos + 3, y);
            if (index >= 0)    
                gr.drawString(String.format("%10.3f", values[i][index]), 
                              xPos + 6 + nameWidth, y);
            else {
                int k = -(index + 1);
                float t = 0;
                if (k >= args.length - 1) 
                    k -= 1;
                else if (k > 0) {
                    t = (arg - args[k - 1]) / (args[k] - args[k - 1]);
                }
                if (k == 0) 
                    gr.drawString(String.format("%10.3f", values[i][k]), 
                                  xPos + 6 + nameWidth, y);
                else
                    gr.drawString(String.format("%10.3f", t * values[i][k] + (1 - t) * values[i][k - 1]), 
                                  xPos + 6 + nameWidth, y);
            }  
            y += fontHeight + 3;
        }
        gr.setColor(fgr);
    }

    @Override
    public void draw2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height)
    {   
        this.gr = gr;
        bgr = params.isAutoBgr() ? params.getWindowBgrColor() : params.getUserBgrColor();
        fgr = isDark(bgr) ? Color.LIGHT_GRAY : Color.DARK_GRAY;
        float[] colCmp = new float[4];
        bgr.getComponents(colCmp);
        bgr = new Color(colCmp[0], colCmp[1], colCmp[2], params.getBgrTransparency());
        if (gr == null || params.getDisplayedData() == null || params.getDisplayedData().length < 1)
            return;
        RenderingHints hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, 
                                                  RenderingHints.VALUE_ANTIALIAS_ON);
        hints.put(RenderingHints.KEY_TEXT_ANTIALIASING, 
                  RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        hints.put(RenderingHints.KEY_RENDERING, 
                  RenderingHints.VALUE_RENDER_QUALITY);
        this.width  = width;
        this.height = height;
        prepareGraph(gr, width, height);
        drawBackground();
        if (values != null && values.length > 0 &&
            args != null && args.length > 1) {
            drawAxes();
            drawGraphs();
            if (drawLegend) {
                if (params.vertical())
                    drawColorLegend(origX + graphWidth + 3, 
                                    origY - graphLegendArea[1] + 2 * fontHeight);
                else
                    drawColorLegend(graphAreaX + 3 , graphAreaY + 3);
            }
        }
        if (params.showTooltip())
            showTooltip(params.getTooltipPosition()[0], params.getTooltipPosition()[1]);
    }
    
    public void setPhysXExtents(float physXMin, float physXMax) {
        this.physXMin = physXMin;
        this.physXMax = physXMax;
    }
    
    public void setArgData(String argName, float[] args)
    {
        this.argName = argName;  
        this.args = args;       
        physXMin = physXMax = args[0];
        for (int i = 1; i < args.length; i++) {
            if (physXMax < args[i])
                physXMax = args[i];
            if (physXMin > args[i])
                physXMin = args[i];
        }               
    }
    
    public void updateMinMax(float[] mins, float[] maxs)
    {
        physYMin = Float.MAX_VALUE;
        physYMax = -Float.MAX_VALUE;
        for (int i = 0; i < mins.length; i++) {
            if (mins[i] < physYMin)
                physYMin = mins[i];
            if (maxs[i] > physYMax)
                physYMax = maxs[i];
        }
    }
    
    public void setArgs(float[] args) {
        this.args = args;
    }

    public void setArgName(String argName) {
        this.argName = argName;
    }
    
    
    public int getOrigX()
    {
        return origX;
    }

    public int getOrigY()
    {
        return origY;
    }

    public int getGraphAreaWidth()
    {
        return graphAreaWidth;
    }

    public int getGraphAreaHeight()
    {
        return graphAreaHeight;
    }

    public int getLeftMargin()
    {
        return leftMargin;
    }

    public int getRightMargin()
    {
        return rightMargin;
    }

    public int getTopMargin()
    {
        return topMargin;
    }

    public int getBottomMargin()
    {
        return bottomMargin;
    }

    public int[] getGraphLegendArea() {
        return graphLegendArea;
    }
    
    public void setOrig(int origX, int origY)
    {
        this.origX = origX;
        this.origY = origY;
        computeOrig = false;
    }

    public void setComputeOrig(boolean computeOrig)
    {
        this.computeOrig = computeOrig;
    }

    public void setIsMulti(boolean isMulti) {
        this.isMulti = isMulti;
    }

    public void setDrawLegend(boolean drawLegend) {
        this.drawLegend = drawLegend;
    }

    public void setDrawXLegend(boolean drawXLegend) {
        this.drawXLegend = drawXLegend;
    }
    
}    
