
package pl.edu.icm.visnow.lib.utils.graphing;

import pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.geom.AffineTransform;
import javax.media.j3d.J3DGraphics2D;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;

/**
 *
 * @author know
 */
    
public class GraphDisplay extends ProbeDisplay
{
    private final GraphWorld graph;
    protected GraphParams graphParams;    

    public GraphDisplay(IrregularField fld, GraphParams graphParams, Position probesPosition, boolean pointerLine, float[] center)
    {
        int nNodes = (int)fld.getNNodes();
        float[] c = fld.getCoords(0).getData();
        crds = new float[6];
        System.arraycopy(c, 0, crds, 0, 3);
        System.arraycopy(c, 3 * (nNodes -1), crds, 3, 3);
        System.arraycopy(center, 0, this.center, 0, 3);
        this.fld = fld;
        graph = new GraphWorld(graphParams, true, false);
        graph.setComputeOrig(false);
        graph.setInField(fld);
        this.graphParams = graphParams;
        cornerCoords = new float[6];
        this.pointerLine = pointerLine;
        this.probesPosition = probesPosition;
    }
    
    public GraphDisplay(IrregularField fld, GraphParams graphParams, float[] center)
    {
        int nNodes = (int)fld.getNNodes();
        float[] c = fld.getCoords(0).getData();
        crds = new float[6];
        System.arraycopy(c, 0, crds, 0, 3);
        System.arraycopy(c, 3 * (nNodes -1), crds, 3, 3);
        this.fld = fld;
        graph = new GraphWorld(graphParams, true, false);
        graph.setComputeOrig(false);
        graph.setInField(fld);
        this.graphParams = graphParams;
        cornerCoords = new float[6];
        System.arraycopy(center, 0, this.center, 0, 3);
    }
    
    public void setGraphParams(GraphParams graphParams) {
        this.graphParams = graphParams;
    }
    

    @Override
    public void updateScreenCoords(LocalToWindow ltw)
    {
        int[] screenOrigin = new int[2];
        int[] screenEndU = new int[2];
        int[] screenEndV = new int[2];
        float[] corner = new float[3];
        int[] tmp = new int[2];
        for (int i = 0; i < 2; i++) {
            System.arraycopy(crds, 3 * i, corner, 0, 3);
            ltw.transformPt(corner, tmp);
            switch (i) {
                case 0:
                   System.arraycopy(tmp, 0, screenOrigin, 0, 2); 
                   System.arraycopy(tmp, 0, screenHandle, 0, 2); 
                case 1:
                   System.arraycopy(tmp, 0, screenEndU, 0, 2); 
                case 2:
                   System.arraycopy(tmp, 0, screenEndV, 0, 2); 
            }
        }
        for (int i = 0; i < 4; i++) {
            switch (probesPosition) {
            case TOP:
            case AT_POINT:
                if (tmp[1] < screenHandle[1])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case BOTTOM:
                if (tmp[1] > screenHandle[1])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case RIGHT:
                if (tmp[0] > screenHandle[0])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            case LEFT:
                if (tmp[0] < screenHandle[0])
                    System.arraycopy(tmp, 0, screenHandle, 0, 2);
                break;
            }
        }
    }

    @Override
    public void draw(J3DGraphics2D gr, LocalToWindow ltw, int width, int height)
        {
        gr.setTransform(new AffineTransform());
        gr.setStroke(new BasicStroke(graphParams.getLineWidth()));
        gr.setColor(selected ? Color.RED : graphParams.getColor());
        int[] tmp = new int[2];
        ltw.transformPt(center, tmp);
        gr.drawLine(tmp[0] - 3, tmp[1], tmp[0] + 3, tmp[1]);
        gr.drawLine(tmp[0], tmp[1] - 3, tmp[0], tmp[1] + 3);
        gr.drawLine(screenHandle[0] + probeOffset[0], screenHandle[1] - probeOffset[1], 
                    screenHandle[0],                  screenHandle[1]);
        gr.setColor(graphParams.getColor());
        graph.setOrig(screenHandle[0] + probeOffset[0], screenHandle[1] - probeOffset[1]);
        graph.prepareGraph(gr, width, height);
        graph.draw2D(gr, ltw, width, height);
        int[] graphRect = graph.getBackgroundDims();
        ixmin = graphRect[0];
        iymin = graphRect[1];
        ixmax = graphRect[0] + graphRect[2];
        iymax = graphRect[1] + graphRect[3];
    }

    @Override
    public void drawOnMargin(J3DGraphics2D gr, LocalToWindow ltw, int width, int height, 
                             float scale, int index, int ix, int iy)
    {
        int anchorX = 0, anchorY = 0;
        graph.setOrig(ix, iy);
        graph.prepareGraph(gr, width, height);
        gr.setStroke(new BasicStroke(graphParams.getLineWidth()));
        gr.setColor(selected ? Color.RED : graphParams.getColor());
        int[] graphRect = graph.getBackgroundDims();
        switch (probesPosition) {
            case TOP:
                anchorX = graphRect[0] + graphRect[2] / 2;
                anchorY = graphRect[1] + graphRect[3];
                break;
            case BOTTOM:
                anchorX = graphRect[0] + graphRect[2] / 2;
                anchorY = graphRect[1];
                break;
            case LEFT:
                anchorX = graphRect[0] + graphRect[2];
                anchorY = graphRect[1] + graphRect[3] / 2;
                break;
            case RIGHT:
                anchorX = graphRect[0];
                anchorY = graphRect[1] + graphRect[3] / 2;
                break;
        }
        if (pointerLine)
            gr.drawLine(anchorX, anchorY, screenHandle[0], screenHandle[1]);
        else {
            gr.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
            gr.drawString(""+index, anchorX - 10, anchorY + 18);
            gr.drawString(""+index, screenHandle[0], screenHandle[1]);
        }
        int[] tmp = new int[2];
        ltw.transformPt(center, tmp);
        gr.drawLine(tmp[0] - 5, tmp[1], tmp[0] + 5, tmp[1]);
        gr.drawLine(tmp[0], tmp[1] - 5, tmp[0], tmp[1] + 5);
        gr.setColor(graphParams.getColor());
        graph.setOrig(ix, iy);
        graph.draw2D(gr, ltw, width, height);
        ixmin = graphRect[0];
        iymin = graphRect[1];
        ixmax = graphRect[0] + graphRect[2];
        iymax = graphRect[1] + graphRect[3];
    }

    public GraphWorld getGraphWorld()
    {
        return graph;
    }

}
    