//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.expressions;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.math3.util.FastMath;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.DataArrayArithmetics;
import pl.edu.icm.jscic.utils.DataArrayStatistics;
import pl.edu.icm.jscic.utils.UnitUtils;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class ExpressionOperators
{

    public static final int LEFT_ASSOC = 0;
    public static final int RIGHT_ASSOC = 1;

    public static enum Operator
    {
        //<editor-fold defaultstate="collapsed" desc=" FUNCTION template ">
        /*
         * How to implement new function
         * 
         FUNCTION {
         @Override
         public String toString() {
         return "function";  //returns function name used in expression
         }

         @Override
         public int getPrecedence() {
         return 1; //returns function precedence, return 15 for functions or omit this Override
         }

         @Override
         public int getNArgumetns() {
         return 2; //returns number of arguments of, now only 1 is supported for functions, and 1 or 2 for operators, omit this Override to use 1
         }

         @Override
         public float[] evaluateFloat(int LEN, float[][] args) throws Exception {
         //returns single precision result array of size LEN,
         //first input argument is data size
         //second input argument is vector index to use (calculate veclen by length/LEN)
         //input arguments is N x float[] where N = getNArguments
         //use indexing of args[] arrays checking if length==1.
         //trow operatorException when evaluation is not possible

         float[] result = new float[LEN];
         int dj = args[1].length == 1 ? 0 : 1;
         int dk = args[0].length == 1 ? 0 : 1;
         for (int i = 0, j = 0, k = 0; i < LEN; i++, j += dj, k += dk) {
         result[i] = args[1][j] + args[0][k];
         }
         return result;
         }

         @Override
         public double[] evaluateDouble(int LEN, double[][] args) {
         //returns double precision result array of size LEN,
         //input arguments is N x double[] where N = getNArguments
         //use indexing of args[] arrays checking if length==1.

         double[] result = new double[LEN];
         int dj = args[1].length == 1 ? 0 : 1;
         int dk = args[0].length == 1 ? 0 : 1;
         for (int i = 0, j = 0, k = 0; i < LEN; i++, j += dj, k += dk) {
         result[i] = args[1][j] + args[0][k];
         }
         return result;
         }
         },
         */

        //</editor-fold>
        VECTOR
        {
            //<editor-fold defaultstate="collapsed" desc=" VECTOR ">            
            @Override
            public String toString()
            {
                return ",";
            }

            @Override
            public int getPrecedence()
            {
                return 1;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                String outUnit = "1";
                if (!ignoreUnits) {
                    if (!da1.isUnitless() && !da2.isUnitless()) {
                        if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                            throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                        }
                        outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                        if (doublePrecision) {
                            da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                            da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                        } else {
                            da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                            da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                        }
                    } else if (!(da1.isUnitless() && da2.isUnitless())) {
                        throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                    }
                }

                TimeData td1, td2;
                LargeArrayType out_type;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                final int veclen = veclen1 + veclen2;
                long len = da1.getNElements();

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    td1 = da1.getTimeData().convertToComplex();
                    td2 = da2.getTimeData().convertToComplex();
                    out_type = LargeArrayType.COMPLEX_FLOAT;
                } else {
                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                    out_type = doublePrecision ? LargeArrayType.DOUBLE : LargeArrayType.FLOAT;
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                for (Float time : timeSeries) {
                    final LargeArray a = td1.getValue(time);
                    final LargeArray b = td2.getValue(time);
                    final LargeArray res = LargeArrayUtils.create(out_type, len * veclen, false);
                    if (out_type != LargeArrayType.COMPLEX_FLOAT) {
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            for (long i = 0; i < len; i++) {
                                for (int v = 0; v < veclen; v++) {
                                    if (v < veclen2) {
                                        res.setDouble(i * veclen + v, b.getDouble(i * veclen2 + v));
                                    } else {
                                        res.setDouble(i * veclen + v, a.getDouble(i * veclen1 + (v - veclen2)));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        for (long k = firstIdx; k < lastIdx; k++) {
                                            for (int v = 0; v < veclen; v++) {
                                                if (v < veclen2) {
                                                    res.setDouble(k * veclen + v, b.getDouble(k * veclen2 + v));
                                                } else {
                                                    res.setDouble(k * veclen + v, a.getDouble(k * veclen1 + (v - veclen2)));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen; v++) {
                                        if (v < veclen2) {
                                            res.setDouble(i * veclen + v, b.getDouble(i * veclen2 + v));
                                        } else {
                                            res.setDouble(i * veclen + v, a.getDouble(i * veclen1 + (v - veclen2)));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        final ComplexFloatLargeArray ac = (ComplexFloatLargeArray) a;
                        final ComplexFloatLargeArray bc = (ComplexFloatLargeArray) b;
                        final ComplexFloatLargeArray resc = (ComplexFloatLargeArray) res;
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            for (long i = 0; i < len; i++) {
                                for (int v = 0; v < veclen; v++) {
                                    if (v < veclen2) {
                                        resc.setComplexFloat(i * veclen + v, bc.getComplexFloat(i * veclen2 + v));
                                    } else {
                                        resc.setComplexFloat(i * veclen + v, ac.getComplexFloat(i * veclen1 + (v - veclen2)));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        for (long k = firstIdx; k < lastIdx; k++) {
                                            for (int v = 0; v < veclen; v++) {
                                                if (v < veclen2) {
                                                    resc.setComplexFloat(k * veclen + v, bc.getComplexFloat(k * veclen2 + v));
                                                } else {
                                                    resc.setComplexFloat(k * veclen + v, ac.getComplexFloat(k * veclen1 + (v - veclen2)));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                for (long i = 0; i < length; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setDouble(i * veclen2 + v, a.getDouble(i) + b.getDouble(i * veclen2 + v));
                                    }
                                }
                            }
                        }
                    }
                    dataSeries.add(res);
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), veclen, "result", outUnit, null);
            }

            //</editor-fold>
        },
        SUM

        {
            //<editor-fold defaultstate="collapsed" desc=" SUM ">
            @Override
            public String toString()
            {
                return "+";
            }

            @Override
            public int getPrecedence()
            {
                return 3;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.addD(args[1], args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.addF(args[1], args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        DIFF

        {
            //<editor-fold defaultstate="collapsed" desc=" DIFF ">
            @Override
            public String toString()
            {
                return "-";
            }

            @Override
            public int getPrecedence()
            {
                return 3;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.diffD(args[1], args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.diffF(args[1], args[0], ignoreUnits);
                }
            }
            //</editor-fold>
        },
        MULT

        {
            //<editor-fold defaultstate="collapsed" desc=" MULT ">
            @Override
            public String toString()
            {
                return "*";
            }

            @Override
            public int getPrecedence()
            {
                return 5;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.multD(args[1], args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.multF(args[1], args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        DIV

        {
            //<editor-fold defaultstate="collapsed" desc=" DIV ">
            @Override
            public String toString()
            {
                return "/";
            }

            @Override
            public int getPrecedence()
            {
                return 5;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.divD(args[1], args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.divF(args[1], args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        POW

        {
            //<editor-fold defaultstate="collapsed" desc=" POW ">
            @Override
            public String toString()
            {
                return "^";
            }

            @Override
            public int getPrecedence()
            {
                return 10;
            }

            @Override
            public int getAssociativity()
            {
                return RIGHT_ASSOC;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (args[0].isConstant() && args[0].getType() != DataArrayType.FIELD_DATA_COMPLEX && (ignoreUnits || args[0].isUnitless())) {
                    if (doublePrecision) {
                        return DataArrayArithmetics.powD(args[1], args[0].getDoubleElement(0)[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.powF(args[1], args[0].getFloatElement(0)[0], ignoreUnits);
                    }
                } else {
                    if (doublePrecision) {
                        return DataArrayArithmetics.powD(args[1], args[0], ignoreUnits);
                    } else {
                        return DataArrayArithmetics.powF(args[1], args[0], ignoreUnits);
                    }
                }
            }
            //</editor-fold>
        },
        NEG

        {
            //<editor-fold defaultstate="collapsed" desc=" NEG ">
            @Override
            public String toString()
            {
                return "~";
            }

            @Override
            public int getPrecedence()
            {
                return 12;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.negD(args[0]);
                } else {
                    return DataArrayArithmetics.negF(args[0]);
                }
            }
            //</editor-fold>
        },
        SQRT

        {
            //<editor-fold defaultstate="collapsed" desc=" SQRT ">
            @Override
            public String toString()
            {
                return "sqrt";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.sqrtD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.sqrtF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        LOG

        {
            //<editor-fold defaultstate="collapsed" desc=" LOG ">
            @Override
            public String toString()
            {
                return "log";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.logD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.logF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        LOG10

        {
            //<editor-fold defaultstate="collapsed" desc=" LOG10 ">
            @Override
            public String toString()
            {
                return "log10";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.log10D(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.log10F(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        EXP

        {
            //<editor-fold defaultstate="collapsed" desc=" EXP ">
            @Override
            public String toString()
            {
                return "exp";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.expD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.expF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        ABS

        {
            //<editor-fold defaultstate="collapsed" desc=" ABS ">
            @Override
            public String toString()
            {
                return "abs";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.absD(args[0]);
                } else {
                    return DataArrayArithmetics.absF(args[0]);
                }
            }
            //</editor-fold>
        },
        SIN

        {
            //<editor-fold defaultstate="collapsed" desc=" SIN ">
            @Override
            public String toString()
            {
                return "sin";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.sinD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.sinF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        COS

        {
            //<editor-fold defaultstate="collapsed" desc=" COS ">            
            @Override
            public String toString()
            {
                return "cos";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.cosD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.cosF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>                    
        },
        TAN

        {
            //<editor-fold defaultstate="collapsed" desc=" TAN ">
            @Override
            public String toString()
            {
                return "tan";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.tanD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.tanF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        ASIN

        {
            //<editor-fold defaultstate="collapsed" desc=" ASIN ">

            @Override
            public String toString()
            {
                return "asin";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.asinD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.asinF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        ACOS

        {
            //<editor-fold defaultstate="collapsed" desc=" ACOS ">

            @Override
            public String toString()
            {
                return "acos";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.acosD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.acosF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        ATAN

        {
            //<editor-fold defaultstate="collapsed" desc=" ATAN ">

            @Override
            public String toString()
            {
                return "atan";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.atanD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.atanF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        SIG

        {
            //<editor-fold defaultstate="collapsed" desc=" SIG ">

            @Override
            public String toString()
            {
                return "signum";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                if (doublePrecision) {
                    return DataArrayArithmetics.signumD(args[0], ignoreUnits);
                } else {
                    return DataArrayArithmetics.signumF(args[0], ignoreUnits);
                }
            }

            //</editor-fold>
        },
        GT

        {
            //<editor-fold defaultstate="collapsed" desc=" GT ">
            @Override
            public String toString()
            {
                return ">";
            }

            @Override
            public int getPrecedence()
            {
                return 2;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                TimeData td1, td2;
                LargeArrayType out_type = LargeArrayType.LOGIC;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                long len = da1.getNElements();

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    throw new IllegalArgumentException("Cannot apply logic operation to complex arrays");
                } else {
                    String outUnit = "1";
                    if (!ignoreUnits) {
                        if (!da1.isUnitless() && !da2.isUnitless()) {
                            if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                                throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                            }                            
                        } else if (da1.isUnitless() && !da2.isUnitless()) {
                            da1.setUnit(da2.getUnit());
                        } else if (da2.isUnitless() && !da1.isUnitless()) {
                            da2.setUnit(da1.getUnit());
                        }
                        if(!(da1.isUnitless() && da2.isUnitless())) {
                            outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                            if (doublePrecision) {
                                da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                            } else {
                                da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                            }
                        }
                    }
                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                if (veclen1 == veclen2) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) > a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) > a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k * veclen1 + v) > a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k * veclen1 + v) > a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) > a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) > a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else if (veclen1 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) > a.getDouble(i));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) > a.getFloat(i));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getDouble(k * veclen2 + v) > a.getDouble(k));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getFloat(k * veclen2 + v) > a.getFloat(k));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) > a.getDouble(i));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) > a.getFloat(i));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }

                } else if (veclen2 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i) > a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i) > a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k) > a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k) > a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i) > a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i) > a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else {
                    throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
            }

            //</editor-fold>
        },
        LT

        {
            //<editor-fold defaultstate="collapsed" desc=" LT ">
            @Override
            public String toString()
            {
                return "<";
            }

            @Override
            public int getPrecedence()
            {
                return 2;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                TimeData td1, td2;
                LargeArrayType out_type = LargeArrayType.LOGIC;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                long len = da1.getNElements();

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    throw new IllegalArgumentException("Cannot apply logic operation to complex arrays");
                } else {
                    String outUnit = "1";
                    if (!ignoreUnits) {
                        if (!da1.isUnitless() && !da2.isUnitless()) {
                            if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                                throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                            }                            
                        } else if (da1.isUnitless() && !da2.isUnitless()) {
                            da1.setUnit(da2.getUnit());
                        } else if (da2.isUnitless() && !da1.isUnitless()) {
                            da2.setUnit(da1.getUnit());
                        }
                        if(!(da1.isUnitless() && da2.isUnitless())) {
                            outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                            if (doublePrecision) {
                                da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                            } else {
                                da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                            }
                        }
                    }
                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                if (veclen1 == veclen2) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) < a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) < a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k * veclen1 + v) < a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k * veclen1 + v) < a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) < a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) < a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else if (veclen1 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) < a.getDouble(i));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) < a.getFloat(i));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getDouble(k * veclen2 + v) < a.getDouble(k));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getFloat(k * veclen2 + v) < a.getFloat(k));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) < a.getDouble(i));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) < a.getFloat(i));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }

                } else if (veclen2 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i) < a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i) < a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k) < a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k) < a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i) < a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i) < a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else {
                    throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
            }

            //</editor-fold>
        },
        GET

        {
            //<editor-fold defaultstate="collapsed" desc=" GET ">
            @Override
            public String toString()
            {
                return ">=";
            }

            @Override
            public int getPrecedence()
            {
                return 2;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                TimeData td1, td2;
                LargeArrayType out_type = LargeArrayType.LOGIC;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                long len = da1.getNElements();

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    throw new IllegalArgumentException("Cannot apply logic operation to complex arrays");
                } else {
                    String outUnit = "1";
                    if (!ignoreUnits) {
                        if (!da1.isUnitless() && !da2.isUnitless()) {
                            if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                                throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                            }                            
                        } else if (da1.isUnitless() && !da2.isUnitless()) {
                            da1.setUnit(da2.getUnit());
                        } else if (da2.isUnitless() && !da1.isUnitless()) {
                            da2.setUnit(da1.getUnit());
                        }
                        if(!(da1.isUnitless() && da2.isUnitless())) {
                            outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                            if (doublePrecision) {
                                da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                            } else {
                                da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                            }
                        }
                    }
                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                if (veclen1 == veclen2) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) >= a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) >= a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k * veclen1 + v) >= a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k * veclen1 + v) >= a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) >= a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) >= a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else if (veclen1 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) >= a.getDouble(i));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) >= a.getFloat(i));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getDouble(k * veclen2 + v) >= a.getDouble(k));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getFloat(k * veclen2 + v) >= a.getFloat(k));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) >= a.getDouble(i));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) >= a.getFloat(i));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }

                } else if (veclen2 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i) >= a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i) >= a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k) >= a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k) >= a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i) >= a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i) >= a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else {
                    throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
            }
            //</editor-fold>
        },
        LET

        {
            //<editor-fold defaultstate="collapsed" desc=" LT ">
            @Override
            public String toString()
            {
                return "<=";
            }

            @Override
            public int getPrecedence()
            {
                return 2;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                TimeData td1, td2;
                LargeArrayType out_type = LargeArrayType.LOGIC;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                long len = da1.getNElements();

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    throw new IllegalArgumentException("Cannot apply logic operation to complex arrays");
                } else {
                    String outUnit = "1";
                    if (!ignoreUnits) {
                        if (!da1.isUnitless() && !da2.isUnitless()) {
                            if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                                throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                            }                            
                        } else if (da1.isUnitless() && !da2.isUnitless()) {
                            da1.setUnit(da2.getUnit());
                        } else if (da2.isUnitless() && !da1.isUnitless()) {
                            da2.setUnit(da1.getUnit());
                        }
                        if(!(da1.isUnitless() && da2.isUnitless())) {
                            outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                            if (doublePrecision) {
                                da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                            } else {
                                da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                            }
                        }
                    }

                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                if (veclen1 == veclen2) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) <= a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) <= a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k * veclen1 + v) <= a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k * veclen1 + v) <= a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) <= a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) <= a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else if (veclen1 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) <= a.getDouble(i));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) <= a.getFloat(i));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getDouble(k * veclen2 + v) <= a.getDouble(k));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, b.getFloat(k * veclen2 + v) <= a.getFloat(k));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) <= a.getDouble(i));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) <= a.getFloat(i));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }

                } else if (veclen2 == 1) {
                    for (Float time : timeSeries) {
                        final LargeArray a = td1.getValue(time);
                        final LargeArray b = td2.getValue(time);
                        final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                        if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                            if (doublePrecision) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getDouble(i) <= a.getDouble(i * veclen1 + v));
                                    }
                                }
                            } else {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, b.getFloat(i) <= a.getFloat(i * veclen1 + v));
                                    }
                                }
                            }
                        } else {
                            long k = len / nthreads;
                            Future[] threads = new Future[nthreads];
                            for (int j = 0; j < nthreads; j++) {
                                final long firstIdx = j * k;
                                final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                threads[j] = ConcurrencyUtils.submit(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (doublePrecision) {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getDouble(k) <= a.getDouble(k * veclen1 + v));
                                                }
                                            }
                                        } else {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, b.getFloat(k) <= a.getFloat(k * veclen1 + v));
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                            try {
                                ConcurrencyUtils.waitForCompletion(threads);
                            } catch (InterruptedException | ExecutionException ex) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i) <= a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i) <= a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            }
                        }
                        dataSeries.add(res);
                    }
                } else {
                    throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
            }
            //</editor-fold>
        },
        EQ

        {
            //<editor-fold defaultstate="collapsed" desc=" EQ ">
            @Override
            public String toString()
            {
                return "==";
            }

            @Override
            public int getPrecedence()
            {
                return 2;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                TimeData td1, td2;
                LargeArrayType out_type = LargeArrayType.LOGIC;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                long len = da1.getNElements();
                String outUnit = "1";
                if (!ignoreUnits) {
                    if (!ignoreUnits) {
                        if (!da1.isUnitless() && !da2.isUnitless()) {
                            if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                                throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                            }                            
                        } else if (da1.isUnitless() && !da2.isUnitless()) {
                            da1.setUnit(da2.getUnit());
                        } else if (da2.isUnitless() && !da1.isUnitless()) {
                            da2.setUnit(da1.getUnit());
                        }
                        if(!(da1.isUnitless() && da2.isUnitless())) {
                            outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                            if (doublePrecision) {
                                da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                            } else {
                                da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                                da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                            }
                        }
                    }
                }

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    td1 = da1.getTimeData().convertToComplex();
                    td2 = da2.getTimeData().convertToComplex();
                } else {
                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                if (veclen1 == veclen2) {
                    if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                        for (Float time : timeSeries) {
                            final LargeArray a = td1.getValue(time);
                            final LargeArray b = td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) == a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) == a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            if (doublePrecision) {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getDouble(k * veclen1 + v) == a.getDouble(k * veclen1 + v));
                                                    }
                                                }
                                            } else {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getFloat(k * veclen1 + v) == a.getFloat(k * veclen1 + v));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    if (doublePrecision) {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) == a.getDouble(i * veclen1 + v));
                                            }
                                        }
                                    } else {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) == a.getFloat(i * veclen1 + v));
                                            }
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    } else {
                        for (Float time : timeSeries) {
                            final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getValue(time);
                            final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, compareComplex(b.getComplexFloat(i * veclen1 + v), a.getComplexFloat(i * veclen1 + v)));
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, compareComplex(b.getComplexFloat(k * veclen1 + v), a.getComplexFloat(k * veclen1 + v)));
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, compareComplex(b.getComplexFloat(i * veclen1 + v), a.getComplexFloat(i * veclen1 + v)));
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    }
                } else if (veclen1 == 1) {
                    if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                        for (Float time : timeSeries) {
                            final LargeArray a = td1.getValue(time);
                            final LargeArray b = td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) == a.getDouble(i));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) == a.getFloat(i));
                                        }
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            if (doublePrecision) {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen2; v++) {
                                                        res.setBoolean(k * veclen2 + v, b.getDouble(k * veclen2 + v) == a.getDouble(k));
                                                    }
                                                }
                                            } else {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen2; v++) {
                                                        res.setBoolean(k * veclen2 + v, b.getFloat(k * veclen2 + v) == a.getFloat(k));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    if (doublePrecision) {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen2; v++) {
                                                res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) == a.getDouble(i));
                                            }
                                        }
                                    } else {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen2; v++) {
                                                res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) == a.getFloat(i));
                                            }
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    } else {
                        for (Float time : timeSeries) {
                            final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getValue(time);
                            final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, compareComplex(b.getComplexFloat(i * veclen2 + v), a.getComplexFloat(i)));
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, compareComplex(b.getComplexFloat(k * veclen2 + v), a.getComplexFloat(k)));
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, compareComplex(b.getComplexFloat(i * veclen2 + v), a.getComplexFloat(i)));
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    }

                } else if (veclen2 == 1) {
                    if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                        for (Float time : timeSeries) {
                            final LargeArray a = td1.getValue(time);
                            final LargeArray b = td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i) == a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i) == a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            if (doublePrecision) {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getDouble(k) == a.getDouble(k * veclen1 + v));
                                                    }
                                                }
                                            } else {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getFloat(k) == a.getFloat(k * veclen1 + v));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    if (doublePrecision) {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getDouble(i) == a.getDouble(i * veclen1 + v));
                                            }
                                        }
                                    } else {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getFloat(i) == a.getFloat(i * veclen1 + v));
                                            }
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    } else {
                        for (Float time : timeSeries) {
                            final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getValue(time);
                            final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, compareComplex(b.getComplexFloat(i), a.getComplexFloat(i * veclen1 + v)));
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, compareComplex(b.getComplexFloat(k), a.getComplexFloat(k * veclen1 + v)));
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, compareComplex(b.getComplexFloat(i), a.getComplexFloat(i * veclen1 + v)));
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
            }
            //</editor-fold>
        },
        NEQ

        {
            //<editor-fold defaultstate="collapsed" desc=" NEQ ">
            @Override
            public String toString()
            {
                return "!=";
            }

            @Override
            public int getPrecedence()
            {
                return 2;
            }

            @Override
            public int getNArgumetns()
            {
                return 2;
            }

            @Override
            public DataArray evaluate(long length, final boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                DataArray da1 = args[0], da2 = args[1];
                if (da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()) {
                    throw new IllegalArgumentException("da1 == null || !da1.isNumeric() || da2 == null || !da2.isNumeric() || da1.getNElements() != da2.getNElements()");
                }
                TimeData td1, td2;
                LargeArrayType out_type = LargeArrayType.LOGIC;
                final int veclen1 = da1.getVectorLength();
                final int veclen2 = da2.getVectorLength();
                long len = da1.getNElements();
                String outUnit = "1";
                if (!ignoreUnits) {
                    if (!da1.isUnitless() && !da2.isUnitless()) {
                        if (!UnitUtils.areValidAndCompatibleUnits(da1.getUnit(), da2.getUnit())) {
                            throw new IllegalArgumentException("Incompatible or invalid units: " + da1.getUnit() + " and " + da2.getUnit());
                        }                            
                    } else if (da1.isUnitless() && !da2.isUnitless()) {
                        da1.setUnit(da2.getUnit());
                    } else if (da2.isUnitless() && !da1.isUnitless()) {
                        da2.setUnit(da1.getUnit());
                    }
                    if(!(da1.isUnitless() && da2.isUnitless())) {
                        outUnit = UnitUtils.getDerivedUnit(da1.getUnit());
                        if (doublePrecision) {
                            da1 = UnitUtils.deepUnitConvertD(da1, outUnit);
                            da2 = UnitUtils.deepUnitConvertD(da2, outUnit);
                        } else {
                            da1 = UnitUtils.deepUnitConvertF(da1, outUnit);
                            da2 = UnitUtils.deepUnitConvertF(da2, outUnit);
                        }
                    }
                }

                if (da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    td1 = da1.getTimeData().convertToComplex();
                    td2 = da2.getTimeData().convertToComplex();
                } else {
                    td1 = da1.getTimeData();
                    td2 = da2.getTimeData();
                }
                ArrayList<Float> timeSeries1 = (ArrayList<Float>) td1.getTimesAsList().clone();
                ArrayList<Float> timeSeries2 = (ArrayList<Float>) td2.getTimesAsList().clone();
                timeSeries1.addAll(timeSeries2);
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeries1));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                int nthreads = (int) FastMath.min(len, ConcurrencyUtils.getNumberOfThreads());
                if (veclen1 == veclen2) {
                    if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                        for (Float time : timeSeries) {
                            final LargeArray a = td1.getValue(time);
                            final LargeArray b = td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) != a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) != a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            if (doublePrecision) {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getDouble(k * veclen1 + v) != a.getDouble(k * veclen1 + v));
                                                    }
                                                }
                                            } else {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getFloat(k * veclen1 + v) != a.getFloat(k * veclen1 + v));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    if (doublePrecision) {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getDouble(i * veclen1 + v) != a.getDouble(i * veclen1 + v));
                                            }
                                        }
                                    } else {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getFloat(i * veclen1 + v) != a.getFloat(i * veclen1 + v));
                                            }
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    } else {
                        for (Float time : timeSeries) {
                            final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getValue(time);
                            final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, !compareComplex(b.getComplexFloat(i * veclen1 + v), a.getComplexFloat(i * veclen1 + v)));
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, !compareComplex(b.getComplexFloat(k * veclen1 + v), a.getComplexFloat(k * veclen1 + v)));
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, !compareComplex(b.getComplexFloat(i * veclen1 + v), a.getComplexFloat(i * veclen1 + v)));
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    }
                } else if (veclen1 == 1) {
                    if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                        for (Float time : timeSeries) {
                            final LargeArray a = td1.getValue(time);
                            final LargeArray b = td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) != a.getDouble(i));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) != a.getFloat(i));
                                        }
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            if (doublePrecision) {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen2; v++) {
                                                        res.setBoolean(k * veclen2 + v, b.getDouble(k * veclen2 + v) != a.getDouble(k));
                                                    }
                                                }
                                            } else {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen2; v++) {
                                                        res.setBoolean(k * veclen2 + v, b.getFloat(k * veclen2 + v) != a.getFloat(k));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    if (doublePrecision) {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen2; v++) {
                                                res.setBoolean(i * veclen2 + v, b.getDouble(i * veclen2 + v) != a.getDouble(i));
                                            }
                                        }
                                    } else {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen2; v++) {
                                                res.setBoolean(i * veclen2 + v, b.getFloat(i * veclen2 + v) != a.getFloat(i));
                                            }
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    } else {
                        for (Float time : timeSeries) {
                            final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getValue(time);
                            final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen2, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen2; v++) {
                                        res.setBoolean(i * veclen2 + v, !compareComplex(b.getComplexFloat(i * veclen2 + v), a.getComplexFloat(i)));
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen2; v++) {
                                                    res.setBoolean(k * veclen2 + v, !compareComplex(b.getComplexFloat(k * veclen2 + v), a.getComplexFloat(k)));
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen2; v++) {
                                            res.setBoolean(i * veclen2 + v, !compareComplex(b.getComplexFloat(i * veclen2 + v), a.getComplexFloat(i)));
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    }

                } else if (veclen2 == 1) {
                    if (!(da1.getType() == DataArrayType.FIELD_DATA_COMPLEX || da2.getType() == DataArrayType.FIELD_DATA_COMPLEX)) {
                        for (Float time : timeSeries) {
                            final LargeArray a = td1.getValue(time);
                            final LargeArray b = td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                if (doublePrecision) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getDouble(i) != a.getDouble(i * veclen1 + v));
                                        }
                                    }
                                } else {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, b.getFloat(i) != a.getFloat(i * veclen1 + v));
                                        }
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            if (doublePrecision) {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getDouble(k) != a.getDouble(k * veclen1 + v));
                                                    }
                                                }
                                            } else {
                                                for (long k = firstIdx; k < lastIdx; k++) {
                                                    for (int v = 0; v < veclen1; v++) {
                                                        res.setBoolean(k * veclen1 + v, b.getFloat(k) != a.getFloat(k * veclen1 + v));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    if (doublePrecision) {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getDouble(i) != a.getDouble(i * veclen1 + v));
                                            }
                                        }
                                    } else {
                                        for (long i = 0; i < len; i++) {
                                            for (int v = 0; v < veclen1; v++) {
                                                res.setBoolean(i * veclen1 + v, b.getFloat(i) != a.getFloat(i * veclen1 + v));
                                            }
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    } else {
                        for (Float time : timeSeries) {
                            final ComplexFloatLargeArray a = (ComplexFloatLargeArray) td1.getValue(time);
                            final ComplexFloatLargeArray b = (ComplexFloatLargeArray) td2.getValue(time);
                            final LargeArray res = LargeArrayUtils.create(out_type, len * veclen1, false);
                            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                                for (long i = 0; i < len; i++) {
                                    for (int v = 0; v < veclen1; v++) {
                                        res.setBoolean(i * veclen1 + v, !compareComplex(b.getComplexFloat(i), a.getComplexFloat(i * veclen1 + v)));
                                    }
                                }
                            } else {
                                long k = len / nthreads;
                                Future[] threads = new Future[nthreads];
                                for (int j = 0; j < nthreads; j++) {
                                    final long firstIdx = j * k;
                                    final long lastIdx = (j == nthreads - 1) ? len : firstIdx + k;
                                    threads[j] = ConcurrencyUtils.submit(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            for (long k = firstIdx; k < lastIdx; k++) {
                                                for (int v = 0; v < veclen1; v++) {
                                                    res.setBoolean(k * veclen1 + v, !compareComplex(b.getComplexFloat(k), a.getComplexFloat(k * veclen1 + v)));
                                                }
                                            }
                                        }
                                    });
                                }
                                try {
                                    ConcurrencyUtils.waitForCompletion(threads);
                                } catch (InterruptedException | ExecutionException ex) {
                                    for (long i = 0; i < len; i++) {
                                        for (int v = 0; v < veclen1; v++) {
                                            res.setBoolean(i * veclen1 + v, !compareComplex(b.getComplexFloat(i), a.getComplexFloat(i * veclen1 + v)));
                                        }
                                    }
                                }
                            }
                            dataSeries.add(res);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Cannot apply logic operation to vectors of different length");
                }
                return DataArray.create(new TimeData(timeSeries, dataSeries, timeSeries.get(0)), FastMath.max(veclen1, veclen2), "result", "1", null);
            }
            //</editor-fold>
        },
        AVG

        {
            //<editor-fold defaultstate="collapsed" desc=" AVG ">

            @Override
            public String toString()
            {
                return "avg";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                double[][] avg = DataArrayStatistics.avg(args[0]);
                ArrayList<Float> timeSeries = (ArrayList<Float>) args[0].getTimeData().getTimesAsList();
                int size = timeSeries.size();
                long len = args[0].getNElements();
                int veclen = args[0].getVectorLength();
                if (doublePrecision) {
                    ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                    if (veclen == 1) {
                        for (int i = 0; i < size; i++) {
                            timeValues.add(i, new DoubleLargeArray(len, avg[i][0], true));
                        }
                    } else {
                        for (int i = 0; i < size; i++) {
                            DoubleLargeArray la = new DoubleLargeArray(len * veclen, false);
                            for (int j = 0; j < len; j++) {
                                for (int v = 0; v < veclen; v++) {
                                    la.setDouble(j * veclen + v, avg[i][v]);
                                }
                            }
                            timeValues.add(i, la);
                        }
                    }
                    return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                } else {
                    ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                    if (veclen == 1) {
                        for (int i = 0; i < size; i++) {
                            timeValues.add(i, new FloatLargeArray(len, (float) avg[i][0], true));
                        }
                    } else {
                        for (int i = 0; i < size; i++) {
                            FloatLargeArray la = new FloatLargeArray(len * veclen, false);
                            for (int j = 0; j < len; j++) {
                                for (int v = 0; v < veclen; v++) {
                                    la.setFloat(j * veclen + v, (float) avg[i][v]);
                                }
                            }
                            timeValues.add(i, la);
                        }
                    }
                    return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                }
            }
            //</editor-fold>
        },
        STDDEV

        {
            //<editor-fold defaultstate="collapsed" desc=" STDEV ">

            @Override
            public String toString()
            {
                return "stddev";
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                double[][] std = DataArrayStatistics.std(args[0]);
                ArrayList<Float> timeSeries = (ArrayList<Float>) args[0].getTimeData().getTimesAsList();
                int size = timeSeries.size();
                long len = args[0].getNElements();
                int veclen = args[0].getVectorLength();
                if (doublePrecision) {
                    ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                    if (veclen == 1) {
                        for (int i = 0; i < size; i++) {
                            timeValues.add(i, new DoubleLargeArray(len, std[i][0], true));
                        }
                    } else {
                        for (int i = 0; i < size; i++) {
                            DoubleLargeArray la = new DoubleLargeArray(len * veclen, false);
                            for (int j = 0; j < len; j++) {
                                for (int v = 0; v < veclen; v++) {
                                    la.setDouble(j * veclen + v, std[i][v]);
                                }
                            }
                            timeValues.add(i, la);
                        }
                    }
                    return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                } else {
                    ArrayList<LargeArray> timeValues = new ArrayList<>(size);
                    if (veclen == 1) {
                        for (int i = 0; i < size; i++) {
                            timeValues.add(i, new FloatLargeArray(len, (float) std[i][0], true));
                        }
                    } else {
                        for (int i = 0; i < size; i++) {
                            FloatLargeArray la = new FloatLargeArray(len * veclen, false);
                            for (int j = 0; j < len; j++) {
                                for (int v = 0; v < veclen; v++) {
                                    la.setFloat(j * veclen + v, (float) std[i][v]);
                                }
                            }
                            timeValues.add(i, la);
                        }
                    }
                    return DataArray.create(new TimeData(timeSeries, timeValues, timeSeries.get(0)), veclen, "result");
                }
            }

            //</editor-fold>
        },
        RAND

        {
            //<editor-fold defaultstate="collapsed" desc=" RAND ">
            @Override
            public String toString()
            {
                return "rand";
            }

            @Override
            public int getNArgumetns()
            {
                return 0;
            }

            @Override
            public int getPrecedence()
            {
                return 15;
            }

            @Override
            public DataArray evaluate(long length, boolean doublePrecision, boolean ignoreUnits, DataArray[] args)
            {
                LargeArray da;
                if (doublePrecision) {
                    da = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, length);
                } else {
                    da = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, length);
                }
                return DataArray.create(da, 1, "result");
            }

            //</editor-fold>
        };

        //defaults set for functions, need to be Overriden in othe operators
        public int getPrecedence()
        {
            return 15;
        }

        public int getAssociativity()
        {
            return LEFT_ASSOC;
        }

        public int getNArgumetns()
        {
            return 1;
        }

        public abstract DataArray evaluate(long length, boolean doublePresion, boolean ignoreUnits, DataArray[] args);

    }

    public static boolean isOperator(String token)
    {
        return (getOperator(token) != null);
    }

    public static Operator getOperator(String token)
    {
        Operator[] operators = Operator.values();
        for (int i = 0; i < operators.length; i++) {
            if (operators[i].toString().equals(token)) {
                return operators[i];
            }
        }
        return null;
    }

    public static boolean isAssociative(String token, int type)
    {
        Operator op = getOperator(token);
        if (op == null) {
            throw new IllegalArgumentException("Invalid token: " + token);
        }

        return isAssociative(op, type);
    }

    public static boolean isAssociative(Operator op, int type)
    {
        return (op.getAssociativity() == type);
    }

    public static int cmpPrecedence(String token1, String token2)
    {
        Operator op1 = getOperator(token1);
        Operator op2 = getOperator(token2);
        if (op1 == null || op2 == null) {
            throw new IllegalArgumentException("Invalid tokens: " + token1 + " " + token2);
        }
        return cmpPrecedence(op1, op2);
    }

    public static int cmpPrecedence(Operator op1, Operator op2)
    {
        return op1.getPrecedence() - op2.getPrecedence();
    }

    private static boolean compareComplex(float[] z1, float[] z2)
    {
        return z1[0] == z2[0] && z1[1] == z2[1];
    }

}
