//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.sort;

import pl.edu.icm.jlargearrays.LongLargeArray;



/**
 * The class provides static methods sorting an array of integers according
 * to the ordering relation defined by a comparator submitted as an argument. 
 * It can be used to generate an array of sorted indices to a 
 * collection according to a given ordering
 * 
 * A large version is give sorting a LongLargeArray
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class IndirectSort
{
    
    private IndirectComparator comp;
    private int[] indices;
    private int len;
    private int n;
    private int left;
    private int right;
    private int largest;
    
    private IndirectSort()
    {
    }
    
    private IndirectSort(int[] indices, IndirectComparator comp)
    {
        this.indices = indices;
        n = len = indices.length;
        this.comp = comp;
    }

    private void buildheap()
    {
        n = len - 1;
        for (int i = n / 2; i >= 0; i--)
            maxheap(i);
    }

    private void maxheap(int i)
    {
        left = 2 * i;
        right = 2 * i + 1;
        if (left <= n && comp.compare(indices[left], indices[i]) < 0)
            largest = left;
        else
            largest = i;

        if (right <= n && comp.compare(indices[right], indices[largest]) < 0)
            largest = right;
        if (largest != i) {
            exchange(i, largest);
            maxheap(largest);
        }
    }

    private void exchange(int i, int j)
    {
        int u = indices[i];
        indices[i] = indices[j];
        indices[j] = u;
    }

    private void sort()
    {
        buildheap();

        for (int i = n; i > 0; i--) {
            exchange(0, i);
            n -= 1;
            maxheap(0);
        }
    }
    
/**
 * Sorts a given set of n comparable items returning an array of indices to the sorted items.
 * Does not change the sorted items.
 * @param indices indices of sorted items
 * @param comp    an IndirectComparator instance comparing sorted items pointed to by the arguments;
 * @param initialize if true, indices are initialized to the sequence 0,1,2,...
 * must be able to compare items at indices i and j between 0 and n;
 */
    public static final void indirectSort(int[] indices, IndirectComparator comp, boolean initialize)
    {
        if (initialize)
            for (int i = 0; i < indices.length; i++)
                indices[i] = i;
        new IndirectSort(indices, comp).sort();
    }
    
/**
 * Sorts a given set of n comparable items returning an array of indices to the sorted items.
 * Does not change the sorted items.
 * @param n     number of sorted items
 * @param comp  an IndirectComparator instance "comparing" integers according to user given rule;
 * must be able to compare items at indices i and j between 0 and n;
 * typically comp will return result of comparison of two elements of an array
 * @return      an int array containing indices to the sorted items ordered increasingly with respect to the comparator
 */
    public static final int[] indirectSort(int n, IndirectComparator comp)
    {
        int[] ind = new int[n];
        indirectSort(ind, comp, true);
        return ind;
    }
    
    private IndirectLongComparator lComp;
    private LongLargeArray lIndices;
    private long lLen;
    private long lN;
    private long lLeft;
    private long lRight;
    private long lLargest;
    
    
    private IndirectSort(LongLargeArray indices, IndirectLongComparator comp)
    {
        this.lIndices = indices;
        lN = lLen = lIndices.length();
        this.lComp = comp;
    }

    private void buildLongHeap()
    {
        lN = lLen - 1;
        for (long i = lN / 2; i >= 0; i--)
            maxLongHeap(i);
    }

    private void maxLongHeap(long i)
    {
        lLeft = 2 * i;
        lRight = 2 * i + 1;
        if (lLeft <= lN && lComp.compare(lIndices.getLong(lLeft), lIndices.getLong(i)) < 0)
            lLargest = lLeft;
        else
            lLargest = i;

        if (lRight <= lN && lComp.compare(lIndices.getLong(lRight), lIndices.getLong(lLargest)) < 0)
            lLargest = lRight;
        if (lLargest != i) {
            exchange(i, lLargest);
            maxLongHeap(lLargest);
        }
    }

    private void exchange(long i, long j)
    {
        long u = lIndices.getLong(i);
        lIndices.setLong(i, lIndices.getLong(j));
        lIndices.setLong(j, u);
    }

    private void lSort()
    {
        buildLongHeap();

        for (long i = lN; i > 0; i--) {
            exchange(0, i);
            lN -= 1;
            maxLongHeap(0);
        }
    }
    
/**
 * Sorts a given set of lN comparable items returning an array of indices to the sorted items.
 * Does not change the sorted items.
 * @param indices indices of sorted items
 * @param comp    an IndirectComparator instance comparing sorted items polonged to by the arguments;
 * @param initialize if true, indices are initialized to the sequence 0,1,2,...
 * must be able to compare items at indices i and j between 0 and lN;
 */
    public static final void indirectSort(LongLargeArray indices, IndirectLongComparator comp, boolean initialize)
    {
        if (initialize)
            for (long i = 0; i < indices.length(); i++)
                indices.setLong(i, i);
        new IndirectSort(indices, comp).sort();
    }
    
/**
 * Sorts a given set of n comparable abstract items returning an array of indices to the sorted items.
 * Does not change the sorted items.
 * @param n     number of sorted items
 * @param comp  an IndirectComparator instance "comparing" longs according to user given rule;
 * must be able to compare items at indices i and j between 0 and n;
 * typically comp will return result of comparison of two elements of a large array
 * @return      an long array containing indices to the sorted items ordered increasingly with respect to the comparator
 */
    public static final LongLargeArray indirectSort(long n, IndirectLongComparator comp)
    {
        LongLargeArray ind = new LongLargeArray(n);
        indirectSort(ind, comp, true);
        return ind;
    }
    
    public static final void main(String[] args)
    {
        IndirectComparator cmp = new IndirectComparator()
        {
            @Override
            public int compare(int i, int j)
            {
                while (i % 3 == 0 && j % 3 == 0) {
                    i /= 3;
                    j /= 3;
                }
                return (i % 3 > 0 && j % 3 > 0) ? (i < j ? 1 : -1) : (i % 3 != 0 ? 1 : -1);
            }
        };
        int[] s = new int[100];
        for (int i = 0; i < s.length; i++)
            s[i] = i + 1;
                indirectSort(s, cmp, false);
        for (int i = 0; i < s.length; i++) 
            System.out.println(""+s[i]);
    }
}
