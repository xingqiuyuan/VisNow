//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.probeInterfaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.media.j3d.J3DGraphics2D;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.lib.utils.field.ValueRanges;
import pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public abstract class MultiDisplay
{
    protected LocalToWindow ltw;
    protected boolean xSort = false;
    protected boolean pointerLine = true;
    protected Position probesPosition = RIGHT;
    
    protected int windowWidth = 500, windowHeight = 500;
    
    protected ArrayList<ProbeDisplay> displays = new ArrayList<>();
    protected float[] minima, maxima;
    protected float[] mMinima, mMaxima;
    protected float[] pMinima, physMinima, coeffs;
    protected int[] exponents;
    protected float startTime = 0, endTime = 0;
    protected float time = 0;
    protected String[] names;
    protected int initMargin = 10;
    protected int graphAreaWidth  = 180;
    protected int graphAreaHeight = 150;
    protected int gap = 10;

    abstract public void addDisplay(IrregularField field);
    
    public void updateTimeRange()
    {
        
    }
    
    public void removeDisplay(ProbeDisplay pickedGraph)
    {
        if (displays.isEmpty())
            return;
        if (pickedGraph == null)
            pickedGraph = displays.get(displays.size() - 1);
        displays.remove(pickedGraph);
    }
    
    public void clearDisplays()
    {
        displays.clear();
        minima = null;
        maxima = null;
        mMinima = null;
        mMaxima = null;
        names = null;
        pMinima = null;
        physMinima = null;
        coeffs = null;
        exponents = null;
    }
    
    public ProbeDisplay getSelection()
    {
        for (ProbeDisplay display : displays) 
            if (display.isSelected())
                return display;
        return null;
    }
    
    public void sort()
    {
        Collections.sort(displays);
    }

    public void setProbesPosition(Position probePosition)
    {
        this.probesPosition = probePosition;
        for (ProbeDisplay display : displays) 
            display.setProbesPosition(probesPosition);
    }
    
    public void setPointerLine(boolean pointerLine) {
        this.pointerLine = pointerLine;
        for (ProbeDisplay display : displays) 
            display.setPointerLine(pointerLine);
    }
    
    public ArrayList<ProbeDisplay> getDisplays()
    {
        return displays;
    }
    
    protected void initValueRanges(Field fld)
    {
        int nCmp = fld.getNComponents();
        minima = new float[nCmp];
        maxima = new float[nCmp];
        Arrays.fill(minima, Float.MAX_VALUE);
        Arrays.fill(maxima, -Float.MAX_VALUE);
        mMinima = new float[nCmp];
        mMaxima = new float[nCmp];
        Arrays.fill(mMinima, Float.MAX_VALUE);
        Arrays.fill(mMaxima, -Float.MAX_VALUE);
        pMinima = new float[nCmp];
        physMinima = new float[nCmp];
        coeffs = new float[nCmp];
        exponents = new int[nCmp];
        names = new String[nCmp];
        for (int i = 0; i < nCmp; i++) {
            DataArray da = fld.getComponent(i);
            names[i] = da.getName();
        }
    }
            
    protected void updateValueRanges()
    {
        for (int m = 0; m < displays.size(); m++) {
            Field fld = displays.get(m).getField();
            if (m == 0) 
                initValueRanges(fld);
            ValueRanges.updateValueRanges(fld, minima, maxima, mMinima, mMaxima,
                                               pMinima, physMinima, coeffs, exponents);
        }
        ValueRanges.updateExponentRanges(minima, maxima, mMinima, mMaxima, 
                                         pMinima, physMinima, coeffs, exponents);
        updateTimeRange();
    }
    
    public int[] maxGraphCount()
    {
        return new int[] {
            (windowWidth  - initMargin) / (graphAreaWidth  + gap),
            (windowHeight - initMargin) / (graphAreaHeight + gap),
        };
    }    

    public void setInitMargin(int initMargin)
    {
        this.initMargin = initMargin;
    }
    
    public void setGap(int gap)
    {
        this.gap = gap;
    }
    
    public void clearSelection()
    {
        for (ProbeDisplay display : displays) 
            display.setSelected(false);
    }
    
    abstract public void draw2D(J3DGraphics2D gr, LocalToWindow ltw, int width, int height);

    public float getStartTime()
    {
        return startTime;
    }

    public float getEndTime()
    {
        return endTime;
    }
     
}
