//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.lineProbe;

import pl.edu.icm.visnow.lib.utils.probeInterfaces.Probe;
import pl.edu.icm.visnow.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.LineAttributes;
import static javax.media.j3d.LineAttributes.PATTERN_SOLID;
import javax.media.j3d.LineStripArray;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.visnow.geometries.objects.generics.OpenAppearance;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.objects.generics.OpenShape3D;
import pl.edu.icm.visnow.geometries.parameters.ComponentColorMap;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.utils.ColorMapper;
import pl.edu.icm.visnow.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceGUI;
import pl.edu.icm.visnow.lib.utils.field.RegularFieldToIrregularField;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.*;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class IndexSlice1D extends Probe
{
    
    protected DataMappingParams mapParams;
    protected RegularField inField = null;
    protected RegularField regularSlice = null;
    protected int[] inDims;
    protected IndexSliceParams params = new IndexSliceParams();
    protected float[] sliceCrds;
    protected int nOutNodes = 0;
    protected FloatLargeArray outCoords;
    protected byte[] colors;
    
    protected int[] startstep = new int[2];
    protected int[] off;
    protected float[] w;

    
    protected int lastNOutNodes = -1;
    protected ComponentColorMap componentColorMap;
    protected IrregularField slice = null;
    // glyph used for interactive display (continuously updated when position slider is adjusting)
    protected OpenShape3D lineShape = new OpenShape3D();
    protected OpenBranchGroup  glyphGroup = new OpenBranchGroup();
    protected LineStripArray glyphLines;
    protected OpenBranchGroup parent = new OpenBranchGroup();
    
    protected IndexSliceGUI gui = new IndexSliceGUI();

    public IndexSlice1D()
    {
        gui.setParams(params);
        params.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateCurrentProbeGeometry();
                fireStateChanged(params.isAdjusting());
            }
        });
        glyphGroup.setName("probeGroup");
        parent.setName("probeParentGroup");
    }
    
    public final void setInData(Field field, DataMappingParams mapParams)
    {
        if (!(field instanceof RegularField))
            return;
        this.componentColorMap = mapParams.getColorMap0();
        inField = (RegularField)field;
        inDims = inField.getDims();
        if (params.getAxis() >= this.inField.getDimNum() || params.getAxis() < 0) 
            params.setAxis(0);
        params.setActive(false);
        sliceCrds = params.getPosition();
        for (int i = 0; i < inField.getDimNum(); i++) 
            sliceCrds[i] = inField.getDims()[i] / 2.f;
        params.setActive(true);
        updateCurrentProbeGeometry();
        gui.update(this.inField.getDims());
        slice();
    }
    
    private static void slice(LargeArray in, LargeArray out, long n, long start, long step,
                              int[] off, float[] w, int veclen)
    {
        switch (off.length) {
        case 0:
            for (long i = 0, j = start; i < n; i++, j += step) 
                for (int k = 0; k < veclen; k++) 
                    out.set(i * veclen + k, in.getFloat(j * veclen + k));
            break;
        case 1:
            int of = off[0];
            float t = w[0];
            for (long i = 0, j = start; i < n; i++, j += step) 
                for (int k = 0; k < veclen; k++) 
                    out.set(i * veclen + k, (float)((1 - t) * in.getFloat( j       * veclen + k) +
                                                         t  * in.getFloat((j + of) * veclen + k)));
            break;
        case 2:
            int of0 = off[0];
            float t0 = w[0];
            int of1 = off[1];
            float t1 = w[1];
            for (long i = 0, j = start; i < n; i++, j += step) 
                for (int k = 0; k < veclen; k++) 
                    out.set(i * veclen + k, 
                            (float)((1 - t0) * (1 - t1) * in.getFloat( j              * veclen + k) +
                                         t0  * (1 - t1) * in.getFloat((j + of0)       * veclen + k) +
                                    (1 - t0) *      t1  * in.getFloat((j + of1)       * veclen + k) +
                                         t0  *      t1  * in.getFloat((j + of0 + of1) * veclen + k)));
            break;
        }
        
    }

    private static class SliceArray implements Runnable
    {
        private final int iThread;
        private final int nThreads;
        private final long n;
        private final long start;
        private final long step;
        private final int[] off;
        private final float[] w;
        private final LargeArray[] inData;
        private final LargeArray[] outData;
        private final int[] vlens;
                
        public SliceArray(int iThread, int nThreads, 
                          long n, long start, long step,
                          int[] off, float[] w,
                          LargeArray[] inData, LargeArray[] outData,            
                          int[] vlens) 
        {
            this.iThread  = iThread;
            this.nThreads = nThreads;
            this.n        = n;
            this.start    = start;
            this.step     = step;
            this.off      = off;
            this.w        = w;
            this.inData   = inData;
            this.outData  = outData;
            this.vlens    = vlens;
        }
        
        @Override
        public void run() 
        {
            int nData = inData.length;
            for (int idata =  (iThread * nData) / nThreads; 
                     idata < ((iThread + 1) * nData) / nThreads; 
                     idata++) {
                int veclen = vlens[idata];
                slice(inData[idata], outData[idata], n, start, step, off, w, veclen);
            }
        }
    }
    
    private static void computeIndices(int[] dims, int axis, float[] sliceCrds, int[] toff, float[] tt, int[] startstep)
    {
        int k0, k1, k2;
        if (dims.length == 3)
            switch (axis) {
            case 0:
               k1 = (int)Math.floor(sliceCrds[1]);
               toff[0] = 0;
               if (k1 < 0) {
                   k1 = 0; tt[0] = 0;
               }
               else if (k1 >= dims[1] - 1) {
                   k1 = dims[1] - 1; tt[0] = 0;
               }
               else {
                   tt[0] = sliceCrds[1] - k1;
                   toff[0] = dims[0];
               }
               k2 = (int)Math.floor(sliceCrds[2]);
               toff[0] = 0;
               if (k2 < 0) {
                   k2 = 0; tt[1] = 0;
               }
               else if (k2 >= dims[2] - 1) {
                   k2 = dims[2] - 1; tt[1] = 0;
               }
               else {
                   tt[1] = sliceCrds[2] - k2;
                   toff[1] = dims[0] * dims[1];
               }
               startstep[0] = (k2 * dims[1] + k1) * dims[0];
               startstep[1] = 1;
               break;
            case 1:
               k0 = (int)Math.floor(sliceCrds[0]);
               toff[0] = 0;
               if (k0 < 0) {
                   k0 = 0; tt[0] = 0;
               }
               else if (k0 >= dims[0] - 1) {
                   k0 = dims[0] - 1; tt[0] = 0;
               }
               else {
                   tt[0] = sliceCrds[0] - k0;
                   toff[0] = 1;
               }
               k2 = (int)Math.floor(sliceCrds[2]);
               toff[1] = 0;
               if (k2 < 0) {
                   k2 = 0; tt[1] = 0;
               }
               else if (k2 >= dims[2] - 1) {
                   k2 = dims[2] - 1; tt[1] = 0;
               }
               else {
                   tt[1] = sliceCrds[2] - k2;
                   toff[1] = dims[0] * dims[1];
               }
               startstep[0] = k2 * dims[1] * dims[0] + k0;
               startstep[1] = dims[0];
               break;
            case 2:
               k0 = (int)Math.floor(sliceCrds[0]);
               toff[0] = 0;
               if (k0 < 0) {
                   k0 = 0; tt[0] = 0;
               }
               else if (k0 >= dims[0] - 1) {
                   k0 = dims[0] - 1; tt[0] = 0;
               }
               else {
                   tt[0] = sliceCrds[0] - k0;
                   toff[0] = 1;
               }
               k1 = (int)Math.floor(sliceCrds[1]);
               toff[1] = 0;
               if (k1 < 0) {
                   k1 = 0; tt[1] = 0;
               }
               else if (k1 >= dims[1] - 1) {
                   k1 = dims[1] - 1; tt[1] = 0;
               }
               else {
                   tt[1] = sliceCrds[1] - k1;
                   toff[1] = dims[0];
               }
               startstep[0] = k1 * dims[0] + k0;
               startstep[1] = dims[0] * dims[1];
               break;
            }
        else if (dims.length == 2)
            switch (axis) {
            case 0:
               k1 = (int)Math.floor(sliceCrds[1]);
               toff[0] = 0;
               if (k1 < 0) {
                   k1 = 0; tt[0] = 0;
               }
               else if (k1 >= dims[1] - 1) {
                   k1 = dims[1] - 1; tt[0] = 0;
               }
               else {
                   tt[0] = sliceCrds[1] - k1;
                   toff[0] = dims[0];
               }
               startstep[0] = + k1 * dims[0];
               startstep[1] = 1;
               break;
            case 1:
               k0 = (int)Math.floor(sliceCrds[0]);
               toff[0] = 0;
               if (k0 < 0) {
                   k0 = 0; tt[0] = 0;
               }
               else if (k0 >= dims[0] - 1) {
                   k0 = dims[0] - 1; tt[0] = 0;
               }
               else {
                   tt[0] = sliceCrds[0] - k0;
                   toff[0] = 1;
               }
               startstep[0] = k0;
               startstep[1] = dims[0];
               break;
            }
    }
    
    public void slice() 
    {
        int axis = params.getAxis();
        sliceCrds = params.getPosition();
        int n = inDims[axis];
        
        int nData = inField.getNComponents();
        DataArray[] outDataArrs = new DataArray[nData];
        int nDataItems = 0;
        for (int i =  0, idata =  0 ; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            TimeData timeData = data.getTimeData();
            nDataItems += timeData.getNSteps();
            outDataArrs[idata] = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, n, data.getVectorLength(), 
                                                  data.getName(), data.getUnit(), 
                                                  data.getUserData());
            idata += 1;
        }
        if (inField.hasCoords())
            nDataItems += inField.getCoords().getNSteps();
        LargeArray[] inData  = new LargeArray[nDataItems];
        LargeArray[] outData = new LargeArray[nDataItems];
        int[] vlens          = new int[nDataItems];
        int iDataItem = 0;
        for (int idata =  0;  idata < nData; idata++) {
            DataArray data = inField.getComponent(idata);
            if (data == null || !data.isNumeric())
                continue;
            int vlen = data.getVectorLength();
            TimeData timeData = data.getTimeData();
            for (int i = 0; i < timeData.getNSteps(); i++) {
                vlens[iDataItem]  = vlen;
                inData[iDataItem] = timeData.getValue(i);
                outData[iDataItem] = new FloatLargeArray(vlen * n);
                iDataItem += 1;
            }
        }
        if (inField.hasCoords()) {
            TimeData timeData = inField.getCoords();
            for (int i = 0; i < timeData.getNSteps(); i++) {
                vlens[iDataItem]  = 3;
                inData[iDataItem] = timeData.getValue(i);
                outData[iDataItem] = new FloatLargeArray(3 * n);
                iDataItem += 1;
            }
        }
        int[] startstep = new int[2];
        int[] toff = new int[inDims.length - 1];
        float[] tt = new float[inDims.length - 1];
        
        computeIndices(inDims, axis, sliceCrds, toff, tt, startstep);
        int start = startstep[0], step = startstep[1];  // a crude method to return a tuple
        
        int noff = 0;
        for (int i = 0; i < toff.length; i++)
            if (toff[i] > 0)
                noff += 1;
        int[] off = new int[noff];
        float[] t = new float[noff];
        for (int i = 0, l = 0; i < toff.length; i++)
            if (toff[i] > 0) {
                off[l] = toff[i];
                t[l]   = tt[i];
            }
        
        int nThreads = 
               pl.edu.icm.visnow.system.main.VisNow.availableProcessors();
        if (nThreads > inField.getNComponents())
            nThreads = inField.getNComponents();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] =  new Thread(new SliceArray(iThread, nThreads, n, start, step,
                                                              off, t, inData, outData, vlens));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        nOutNodes = inField.getDims()[axis];
        regularSlice = new RegularField(new int[] {nOutNodes});
        int idata = 0;
        for (int i =  0, iOutdata =  0; i < nData; i++) {
            DataArray data = inField.getComponent(i);
            if (data == null || !data.isNumeric())
                continue;
            TimeData timeData = data.getTimeData();
            int nTimeSteps = timeData.getNSteps();
            for (int j = 0; j < nTimeSteps; j++, idata++) 
                outDataArrs[iOutdata].getTimeData().setValue(outData[idata], data.getTime(j));
            outDataArrs[iOutdata].setPreferredRanges(data.getPreferredMinValue(), data.getPreferredMaxValue(), 
                                                     data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
            regularSlice.addComponent(outDataArrs[iOutdata]);
            iOutdata += 1;
        }
        if (inField.hasCoords()) {
            TimeData outTimeCoords = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
            for (int i = 0; i < inField.getCoords().getNSteps(); i++, idata++) {
                outTimeCoords.setValue(outData[idata], inField.getCoords().getTime(i));
                regularSlice.setCoords(outTimeCoords);
            }
        }
        else {
            float[][] affine = inField.getAffine();
            float[][] sliceAffine = new float[4][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) 
                    sliceAffine[j][i] = 0;
                sliceAffine[3][i] = affine[3][i];
                for (int j = 0; j < 3; j++) 
                    if (j == axis) 
                        sliceAffine[0][i] = affine[j][i];
                    else
                        sliceAffine[3][i] += sliceCrds[j] * affine[j][i];
            }
            regularSlice.setAffine(sliceAffine);
            regularSlice.setCoords(regularSlice.getCoordsFromAffine(), 0);
        }
        float[] outT = new float[nOutNodes];
        float[] outC = regularSlice.getCoords(0).getFloatData();
        outT[0] = 0;
        for (int i = 1; i < nOutNodes; i++) 
            outT[i] = (float)Math.sqrt((outC[3 * i]     - outC[3 * i - 3]) * (outC[3 * i]     - outC[3 * i - 3]) +
                                       (outC[3 * i + 1] - outC[3 * i - 2]) * (outC[3 * i + 1] - outC[3 * i - 2]) +
                                       (outC[3 * i + 2] - outC[3 * i - 1]) * (outC[3 * i + 2] - outC[3 * i - 1])) +
                                        outT[i - 1];
        regularSlice.addComponent(DataArray.create(outT, 1, LINE_SLICE_COORDS));
        int nDim = inField.getDims().length;
        float[] indexCoords = new float[nDim * nOutNodes];
        for (int i = 0, l = 0; i < nOutNodes; i++) 
            for (int j = 0; j < nDim; j++, l++) 
                indexCoords[l] = (j == axis ? i : sliceCrds[j]);
        regularSlice.addComponent(DataArray.create(indexCoords, nDim, INDEX_COORDS));
        slice = RegularFieldToIrregularField.createIrregular(regularSlice);
    }
    
    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return parent;
    }
    
    protected void updateCoords()
    {
        int[] dims = inField.getDims();
        int axis = params.getAxis();
        float[] sliceCrds = params.getPosition();
        nOutNodes = dims[axis];
        if (outCoords == null || outCoords.length() != 3 * nOutNodes)
            outCoords = new FloatLargeArray(3 * nOutNodes);
        if (inField.hasCoords()) 
            slice(inField.getCurrentCoords(), outCoords, nOutNodes, startstep[0], startstep[1], off, w, 3);
        else {
            float[][] affine = inField.getAffine();
            float[][] sliceAffine = new float[4][3];
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 3; j++) {
                    sliceAffine[i][j] = 0;
                }
            }
            for (int i = 0; i < 3; i++) {
                sliceAffine[0][i] = affine[axis][i];
                sliceAffine[3][i] = affine[3][i];
                for (int j = 0; j < dims.length; j++) 
                    if (j != axis) 
                        sliceAffine[3][i] += sliceCrds[j] * affine[j][i];
            }
            for (int i = 0; i < nOutNodes; i++)
                for (int j = 0; j < 3; j++) 
                    outCoords.setFloat(3 * i + j, sliceAffine[3][j] + i * sliceAffine[0][j]);
        }
    }
    
    protected void updateColors()
    {
        if (colors == null || colors.length != 3 * nOutNodes)
            colors = new byte[3 * nOutNodes];
        DataArray da =  inField.getComponent(componentColorMap.getDataComponentIndex());
        if (da != null && da.isNumeric()) {
            FloatLargeArray outDa = new FloatLargeArray(da.getVectorLength() * nOutNodes);
            slice(da.getRawArray(), outDa, nOutNodes, startstep[0], startstep[1], off, w, da.getVectorLength());
            float[] fData = outDa.getData();
            int vlen = da.getVectorLength();
            float[] v = new float[vlen];
            for (int i = 0; i < nOutNodes; i++) {
                System.arraycopy(fData, i * vlen, v, 0, vlen);
                byte[] pointColor = ColorMapper.mapByteColor(v, componentColorMap);
                for (int j = 0; j < 3; j++) 
                    pointColor[j] = (byte)(0xff & (int)(.8 * (pointColor[j] & 0xff)));
                System.arraycopy(pointColor, 0, colors, 3 * i, 3);
            }
        }
        else {
            float[] outT = new float[nOutNodes];
            float[] outC = outCoords.getData();
            outT[0] = 0;
            for (int i = 1; i < nOutNodes; i++) 
                outT[i] = (float)Math.sqrt((outC[3 * i]     - outC[3 * i - 3]) * (outC[3 * i]     - outC[3 * i - 3]) +
                                           (outC[3 * i + 1] - outC[3 * i - 2]) * (outC[3 * i + 1] - outC[3 * i - 2]) +
                                           (outC[3 * i + 2] - outC[3 * i - 1]) * (outC[3 * i + 2] - outC[3 * i - 1])) +
                          outT[i - 1];
            float tMax = outT[nOutNodes - 1];
            for (int i = 0; i < nOutNodes; i++) {
                colors[3 * i]     = (byte)(0xff & (int)((255. * outT[i]) / tMax));
                colors[3 * i + 1] = (byte)(0xff & 255);
                colors[3 * i + 2] = (byte)(0xff & 255);
            }
            ColorMapper.hsvtorgb(colors);
        }
    }
    
    public void updateGlyphData()
    {
        updateCoords();
        updateColors();
        glyphLines.setCoordinates(0, outCoords.getData());
        glyphLines.setColors(0, colors);
    }
    
    int lastAxis = -1;
    
    public void updateCurrentProbeGeometry()
    {
        int[] dims = inField.getDims();
        int axis = params.getAxis();
        nOutNodes = dims[axis];
        float[] sliceCrds = params.getPosition();
       
        int[] toff = new int[dims.length - 1];
        float[] tt = new float[dims.length - 1];

        computeIndices(dims, axis, sliceCrds, toff, tt, startstep);

        int noff = 0;
        for (int i = 0; i < toff.length; i++)
            if (toff[i] > 0)
                noff += 1;
        off = new int[noff];
        w = new float[noff];
        for (int i = 0, l = 0; i < toff.length; i++)
            if (toff[i] > 0) {
                off[l] = toff[i];
                w[l]   = tt[i];
            }
        if ((nOutNodes != lastNOutNodes || lastAxis != axis) && glyphGroup != null) {
            glyphGroup.detach();
            glyphGroup.removeAllChildren();
            glyphLines = new LineStripArray(nOutNodes,  GeometryArray.COORDINATES | GeometryArray.COLOR_3, new int[]{nOutNodes});
            glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_READ);
            glyphLines.setCapability(GeometryArray.ALLOW_COORDINATE_WRITE);
            glyphLines.setCapability(GeometryArray.ALLOW_COLOR_READ);
            glyphLines.setCapability(GeometryArray.ALLOW_COLOR_WRITE);
            updateGlyphData();
            OpenAppearance app = new OpenAppearance();
            app.setLineAttributes(new LineAttributes(4, PATTERN_SOLID, true));
            lineShape.removeAllGeometries();
            lineShape.setAppearance(app);
            lineShape.addGeometry(glyphLines);
            glyphGroup.addChild(lineShape);
            parent.addChild(glyphGroup);
        }
        else if (glyphGroup != null)
            updateGlyphData();
        else {
            glyphGroup.detach();
            glyphGroup.removeAllChildren();
            parent.addChild(glyphGroup);
        }
        lastNOutNodes = nOutNodes;
        lastAxis = axis;
    }
    
    public float[] getPlaneCenter()
    {
        float[] startPoint = new float[3];
        System.arraycopy(outCoords.getData(), 0, startPoint, 0, 3);
        return startPoint;
    }
    
    public void hide()
    {
        if (glyphGroup != null) 
                glyphGroup.detach();
    }
    
    public void show()
    {
        if (glyphGroup.getParent() == null)
            parent.addChild(glyphGroup);
    }
    @Override
    public JPanel getGlyphGUI()
    {
        return gui;
    }

    @Override
    public IrregularField getSliceField()
    {
        slice();
        return slice;
    }

    @Override
    public RegularField getRegularSliceField()
    {
        return regularSlice;
    }
}
