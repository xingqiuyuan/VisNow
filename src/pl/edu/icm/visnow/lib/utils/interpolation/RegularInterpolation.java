//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.interpolation;

import java.util.Arrays;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import static org.apache.commons.math3.util.FastMath.max;
import static org.apache.commons.math3.util.FastMath.min;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 * @author know
 */
public class RegularInterpolation
{
    public static final int COMPUTE_OUTPUT = 1;
    public static final int COMPUTE_POSITIONS = 2;
    private final Field inField;
    private float[] boxVerts;
    private final int trueDim;
    private int[] outDims;
    private float[][] outAffine = new float[4][3];
    private float[][] outInvAffine;
    private float[][] transformedAffine = new float[4][3];
    private RegularField outRegularField;
    protected float[] inCoords = null;
    protected float[] recomputedCoords = null;
    
    protected boolean globalResolution = true;
    protected int resolution = 100;
    protected int resolution0 = 100;
    protected int resolution1 = 100;
    protected int resolution2 = 100;
    protected int computeExtent = COMPUTE_OUTPUT;
    protected LogicLargeArray valid;
    protected FieldPosition[] result;
    private final int nThreads = VisNow.availableProcessors();

    public RegularInterpolation(Field inField)
    {
        this.inField = inField;
        trueDim = inField.getTrueNSpace();
        if (inField.hasCoords()) {
            inCoords = inField.getCoords(0).getData();
            recomputedCoords = new float[trueDim * (int)inField.getNNodes()];
        }
        float[][] inExt = inField.getExtents();
        boxVerts = new float[] {
            inExt[0][0], inExt[0][1], inExt[0][2], 
            inExt[1][0], inExt[0][1], inExt[0][2], 
            inExt[0][0], inExt[1][1], inExt[0][2], 
            inExt[1][0], inExt[1][1], inExt[0][2], 
            inExt[0][0], inExt[0][1], inExt[1][2], 
            inExt[1][0], inExt[0][1], inExt[1][2], 
            inExt[0][0], inExt[1][1], inExt[1][2], 
            inExt[1][0], inExt[1][1], inExt[1][2], 
        };
    }
    
    
    public RegularInterpolation(Field inField, int computeExtent)
    {
        this(inField);
        this.computeExtent = computeExtent;
    }

    public void setBoxVerts(float[] boxVerts)
    {
        this.boxVerts = boxVerts;
    }

    public void setResolution(int resolution)
    {
        this.resolution = resolution;
        globalResolution = true;
    }
    
    public void setResolution(int[] resolution)
    {
        resolution0 = resolution[0];
        if (resolution.length > 1)
            resolution1 = resolution[1];
        if (resolution.length > 2)
            resolution2 = resolution[2];
        globalResolution = false;
    }
    
    /**
     * The original field coordinates are recomputed to the index coordinates of the outfield
     */
    private class CoordinateRecomputation implements Runnable
    {

        int nThreads = 1;
        int iThread = 0;
        int nInNodes;

        public CoordinateRecomputation(int nThr, int iThr)
        {
            this.nThreads = nThr;
            this.iThread = iThr;
            nInNodes = (int) inField.getNNodes();
        }

        @Override
        public void run()
        {
            float[] v = new float[3];
            int dk = nInNodes / nThreads;
            int kstart = iThread * dk + min(iThread, nInNodes % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nInNodes % nThreads);
            for (int k = kstart; k < kend; k++) {
                for (int i = 0; i < 3; i++)
                    v[i] = inCoords[3 * k + i] - outAffine[3][i];
                for (int i = 0; i < trueDim; i++) {
                    recomputedCoords[trueDim * k + i] = 0;
                    for (int j = 0; j < trueDim; j++)
                        recomputedCoords[trueDim * k + i] += outInvAffine[j][i] * v[j];
                }
            }
        }
    }

    public void prepareInterpolation()
    {
        outDims = new int[trueDim];
        float[][] ext = inField.getExtents();
        float[] invDelta = new float[trueDim];
        for (float[] outAffine1 : outAffine)
            Arrays.fill(outAffine1, 0.f);
        for (int i = 0; i < trueDim; i++) {
            int k;
            for (int j = 0; j < trueDim; j++) {
                switch (j) {
                case 0: 
                    k = 3;
                    break;
                case 1:
                    k = 6;
                    break;
                default:
                    k = 12;
                    break;
                }
                outAffine[j][i] = boxVerts[i + k] - boxVerts[i];
            }
            outAffine[3][i] = boxVerts[i];
        }
        float maxExt = 0;
        float[] boxEdges = new float[trueDim];
        Arrays.fill(boxEdges, 0);
        for (int i = 0; i < boxEdges.length; i++) {
            for (int j = 0; j < boxEdges.length; j++) 
                boxEdges[i] += outAffine[i][j] * outAffine[i][j];
            boxEdges[i] = (float)Math.sqrt(boxEdges[i]);
            if (maxExt < boxEdges[i])
                maxExt = boxEdges[i];
        }
        int outNNodes = 1;
        if (globalResolution) {
            float delta = maxExt / (resolution - 1);
            for (int i = 0; i < trueDim; i++)
                outDims[i] = max(2, (int) (boxEdges[i] / delta));
        }
        else {
            invDelta[0] = resolution0 / (ext[1][0] - ext[0][0]);
            invDelta[1] = resolution1 / (ext[1][1] - ext[0][1]);
            if (trueDim > 2)
                invDelta[2] = resolution2 / (ext[1][2] - ext[0][2]);
            for (int i = 0; i < trueDim; i++) {
                outDims[i] = max(2, (int) (invDelta[i] * (ext[1][i] - ext[0][i])));
            }
        }
        for (int i = 0; i < outDims.length; i++)
            outNNodes *= outDims[i];
        for (int i = 0; i < trueDim; i++)
            for (int j = 0; j < 3; j++)
                outAffine[i][j] /= (outDims[i] - 1);
        outRegularField = new RegularField(outDims);
        outRegularField.setAffine(outAffine);
        if ((computeExtent & COMPUTE_POSITIONS) != 0) {
            result = new FieldPosition[outNNodes];
            for (int i = 0; i < result.length; i++)
                result[i] = null;
        }
        outInvAffine = outRegularField.getInvAffine();

        if (inField.hasCoords()) {
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new CoordinateRecomputation(nThreads, iThread));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        valid = new LogicLargeArray(outRegularField.getNNodes(), false);
    }
    
    protected ProcessSimplex processor;
    class UpdateRegularFieldPart implements Runnable
    {

        int from;
        int to;
        RegularField inField;

        public UpdateRegularFieldPart(long n, int nThreads, int iThread, RegularField inField)
        {
            this.inField = inField;
            from = (int)(n * iThread / nThreads);
            to = (int)(n * (iThread + 1) / nThreads);
        }

        @Override
        public void run()
        {
            if (trueDim == 3)
                for (int i = from; i < to; i++) {
                    int[] tetras = inField.getTetras(i);
                    if (tetras == null)
                        continue;
                    for (int tet = 0; tet < 5; tet++) {
                        int[] tetraVerts = new int[4];
                        System.arraycopy(tetras, 4 * tet, tetraVerts, 0, 4);
                        processor.processTetra(tetraVerts, 0);
                    }
                }
            else
                for (int i = from; i < to; i++) {
                    int[] triangles = inField.getTriangles(i);
                    if (triangles == null)
                        continue;
                    for (int tri = 0; tri < 2; tri++) {
                        int[] triVerts = new int[3];
                        System.arraycopy(triangles, 3 * tri, triVerts, 0, 3);
                        processor.processTriangle(triVerts, 0);
                    }
                }
        }
    }

    private void update(RegularField inField)
    {
        int[] inDims = inField.getDims();
        long nOut = 1, nIn = 1;
        for (int i = 0; i < outDims.length; i++) {
            nIn *= (inDims[i] - 1);
            nOut *= outDims[i];
        }
        int nNumDArrays = 0;
        LargeArray[][] inData = null;
        FloatLargeArray[][] outData = null;
        int[] vlens = null;
        int[] indices = null;
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric())
                    nNumDArrays += 1;
            inData = new LargeArray[nNumDArrays][];
            outData = new FloatLargeArray[nNumDArrays][];
            indices = new int[nNumDArrays];
            vlens = new int[nNumDArrays]; 
            int jComponent = 0;
            for (int iComponent = 0; iComponent < inField.getNComponents(); iComponent++)
                if (inField.getComponent(iComponent).isNumeric()) {
                    DataArray da = inField.getComponent(iComponent);
                    indices[iComponent] = iComponent;
                    vlens[iComponent] = da.getVectorLength();
                    inData[iComponent] = new LargeArray[da.getNFrames()];
                    outData[iComponent] = new FloatLargeArray[da.getNFrames()];
                    for (int iTimeStep = 0; iTimeStep < da.getNFrames(); iTimeStep++) {
                        inData[jComponent][iTimeStep] = da.getTimeData().getValues().get(iTimeStep);
                        outData[jComponent][iTimeStep] = new FloatLargeArray(vlens[iComponent] * nOut);
                    }
                    jComponent += 1;
                }
        }
        if (!inField.hasCoords()) {
            int nSpace = inField.getDimNum();
            float[][] inAffine  = inField.getAffine();
            float[][] invAffine = inField.getInvAffine();
            float[][] trAffine  = new float[nSpace][nSpace];
            float[] t = new float[nSpace];
            float[] origInd = new float[nSpace];
            Arrays.fill(origInd, 0);
            for (int i = 0; i < nSpace; i++) {
                t[i] = outAffine[3][i] - inAffine[3][i];
                Arrays.fill(trAffine[i], 0);
            }
            for (int i = 0; i < nSpace; i++) {
                for (int j = 0; j < nSpace; j++) 
                    origInd[i] += invAffine[j][i] * t[j];
                for (int k = 0; k < nSpace; k++) 
                    for (int j = 0; j < nSpace; j++)
                        trAffine[j][i] += invAffine[j][i] * outAffine[k][i];
            }
            int[] ind = new int[nSpace];
            int[] inInd = new int[nSpace];
            for (long i = 0; i < nOut; i++) {   // loop over outField nodes
                valid.set(i, true);
                for (int j = 0; j < nSpace; j++) {
                    ind[j] = (int)(i % outDims[j]);
                    i /= outDims[j];
                }                               // ind = indices of i-th node in outfield
                System.arraycopy(origInd, 0, t, 0, nSpace);
                for (int j = 0; j < nSpace; j++) {
                    for (int k = 0; k < nSpace; k++)
                        t[j] += ind[k] * trAffine[k][j];
                    if (t[j] < 0 || t[j] > inDims[j]) {
                        valid.setBoolean(i, false);
                        break;
                    }                           // t[j] is the j-th coordinate in index coord system of inField
                    inInd[j] = (int)t[j];
                    t[j] -= inInd[j];
                }
                if (!valid.getBoolean(i))
                    continue;
                switch (nSpace) {
                    case 3:
                        for (int iDataArray = 0; iDataArray < nNumDArrays; iDataArray++)  {
                                int vlen = vlens[iDataArray];
                                for (int iTimeStep = 0; iTimeStep < outData[iDataArray].length; iTimeStep++) 
                                    for (int j = 0; j < vlen; j++) {
                                            float val = 0;
//                                            for (int k = 0; k < 4; k++)
//                                                val += baryCoords[k] * inData[iDataArray][iTimeStep].
//                                                                         getFloat(vlen * nodes[k] + j);
//                                            outData[iDataArray][iTimeStep].set(vlen * iData + j, val);
                                        }
                            }
                        break;
                    case 2:
                        for (int iDataArray = 0; iDataArray < nNumDArrays; iDataArray++)  {
                                int vlen = vlens[iDataArray];
                                for (int iTimeStep = 0; iTimeStep < outData[iDataArray].length; iTimeStep++) 
                                    for (int j = 0; j < vlen; j++) {
                                            float val = 0;
//                                            for (int k = 0; k < 4; k++)
//                                                val += baryCoords[k] * inData[iDataArray][iTimeStep].
//                                                                         getFloat(vlen * nodes[k] + j);
//                                            outData[iDataArray][iTimeStep].set(vlen * iData + j, val);
                                        }
                            }
                        break;
                }
            }
        }
        else {
            processor = new ProcessSimplex(outDims, recomputedCoords, nNumDArrays, 
                                           vlens, inData, outData, 
                                           valid, result, computeExtent);
            Thread[] workThreads = new Thread[nThreads];
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread] = new Thread(new UpdateRegularFieldPart(nIn, nThreads, iThread, inField));
            for (int iThread = 0; iThread < nThreads; iThread++)
                workThreads[iThread].start();
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int iDataArray = 0; iDataArray < nNumDArrays; iDataArray++) {
                DataArray data = inField.getComponent(indices[iDataArray]);
                int veclen = data.getVectorLength();
                TimeData outTimeData = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                DataArray outDa = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, nOut, veclen, data.getName(), 
                                                   data.getUnit(), data.getUserData());
                for (int iStep = 0; iStep < data.getNFrames(); iStep++) 
                    outTimeData.setValue(outData[iDataArray][iStep], data.getTime(iStep));
                outDa.setPreferredRanges(data.getPreferredMinValue(),     data.getPreferredMaxValue(), 
                                         data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                outDa.setTimeData(outTimeData);
                outRegularField.addComponent(outDa);
            }
            outRegularField.setMask(valid, 0);
            outRegularField.setCurrentTime(inField.getCurrentTime());
        }
    }

    class UpdateIrregularFieldPart implements Runnable
    {
        int from;
        int to;
        int[] nodes;
        int[] indices;

        public UpdateIrregularFieldPart(int n, int nThreads, int iThread, int[] nodes, int[] indices)
        {
            this.nodes = nodes;
            this.indices = indices;
            from = n * iThread / nThreads;
            to = n * (iThread + 1) / nThreads;
        }

        @Override
        public void run()
        {
            if (trueDim == 3)
                for (int tet = from; tet < to; tet++) {
                    int[] tetraVerts = new int[4];
                    System.arraycopy(nodes, 4 * tet, tetraVerts, 0, 4);
                    if (indices != null)
                        processor.processTetra(tetraVerts, indices[tet]);
                    else
                        processor.processTetra(tetraVerts, 0);
                }
            else
                for (int tri = from; tri < to; tri++) {
                    int[] triVerts = new int[4];
                    System.arraycopy(nodes, 3 * tri, triVerts, 0, 3);
                    if (indices != null)
                        processor.processTriangle(triVerts, indices[tri]);
                    else
                        processor.processTriangle(triVerts, 0);
                }
        }
    }

    private void update(IrregularField inField)
    {
        int nNumDArrays = 0;
        int nNumNodeDataArrays = 0;
        nNumDArrays = 0;
        long nOut = 1;
        for (int i = 0; i < outDims.length; i++) 
            nOut *= outDims[i];
        LargeArray[][] inData = null;
        FloatLargeArray[][] outData = null;
        int[] vlens = null;
        int[] indices = null;
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric())
                    nNumDArrays += 1;
            nNumNodeDataArrays = nNumDArrays;
            boolean cellDataInterpolable = inField.getNCellSets() == 1 && inField.getCellSet(0).getNComponents() > 0;
            if (cellDataInterpolable) {
                CellSet cs = inField.getCellSet(0);
                for (int i = 0; i < cs.getNComponents(); i++)
                    if (cs.getComponent(i).isNumeric())
                        nNumDArrays += 1;
            }
            inData = new LargeArray[nNumDArrays][];
            outData = new FloatLargeArray[nNumDArrays][];
            indices = new int[nNumDArrays];
            vlens = new int[nNumDArrays];
            int iOutData = 0;
            for (int iComponent = 0; iComponent < inField.getNComponents(); iComponent++)
                if (inField.getComponent(iComponent).isNumeric()) {
                    DataArray da = inField.getComponent(iComponent);
                    indices[iOutData] = iComponent;
                    vlens[iOutData] = da.getVectorLength();
                    inData[iOutData] = new LargeArray[da.getNFrames()];
                    outData[iOutData] = new FloatLargeArray[da.getNFrames()];
                    for (int iTimeStep = 0; iTimeStep < da.getNFrames(); iTimeStep++) {
                        inData[iOutData][iTimeStep] = da.getTimeData().getValues().get(iTimeStep);
                        outData[iOutData][iTimeStep] = new FloatLargeArray(vlens[iOutData] * nOut);
                    }
                    iOutData += 1;
                }
            if (cellDataInterpolable) {
                CellSet cs = inField.getCellSet(0);
                for (int iComponent = 0; iComponent < cs.getNComponents(); iComponent++)
                    if (cs.getComponent(iComponent).isNumeric()) {
                        DataArray da = cs.getComponent(iComponent);
                        indices[iOutData] = iComponent;
                        vlens[iOutData] = da.getVectorLength();
                        inData[iOutData] = new LargeArray[da.getNFrames()];
                        outData[iOutData] = new FloatLargeArray[da.getNFrames()];
                        for (int iTimeStep = 0; iTimeStep < da.getNFrames(); iTimeStep++) {
                            inData[iOutData][iTimeStep] = da.getTimeData().getValues().get(iTimeStep);
                            outData[iOutData][iTimeStep] = new FloatLargeArray(vlens[iOutData] * nOut);
                            iOutData += 1;
                        }
                    }
            }
        }
        processor = new ProcessSimplex(outDims, recomputedCoords, nNumNodeDataArrays, 
                                       vlens, inData, outData, 
                                       valid, result, computeExtent);
        
        for (CellSet inCellSet : inField.getCellSets()) {
            if (trueDim == 3)
                for (int i = CellType.TETRA.getValue(); i <= CellType.HEXAHEDRON.getValue(); i++) {
                    if (inCellSet.getCellArray(CellType.getType(i)) == null)
                        continue;
                    CellArray inCellArray = inCellSet.getCellArray(CellType.getType(i)).getTriangulated();
                    int n = inCellArray.getNCells();
                    Thread[] workThreads = new Thread[nThreads];
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread] = new Thread(new UpdateIrregularFieldPart(n, nThreads, iThread, inCellArray.getNodes(), inCellArray.getDataIndices()));
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread].start();
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (InterruptedException e) {
                        }
                }
            else if (trueDim == 2)
                for (int i = CellType.TRIANGLE.getValue(); i <= CellType.QUAD.getValue(); i++) {
                    if (inCellSet.getCellArray(CellType.getType(i)) == null)
                        continue;
                    CellArray inCellArray = inCellSet.getCellArray(CellType.getType(i)).getTriangulated();
                    int n = inCellArray.getNCells();
                    Thread[] workThreads = new Thread[nThreads];
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread] = new Thread(new UpdateIrregularFieldPart(n, nThreads, iThread, inCellArray.getNodes(), inCellArray.getDataIndices()));
                    for (int iThread = 0; iThread < nThreads; iThread++)
                        workThreads[iThread].start();
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (InterruptedException e) {
                        }
                }
        }
        if ((computeExtent & COMPUTE_OUTPUT) != 0) {
            for (int iDataArray = 0; iDataArray < nNumDArrays; iDataArray++) {
                DataArray data = iDataArray < nNumNodeDataArrays ?
                        inField.getComponent(indices[iDataArray]) :
                        inField.getCellSet(0).getComponent(indices[iDataArray]);
                int veclen = data.getVectorLength();
                TimeData outTimeData = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
                DataArray outDa = DataArray.create(DataArrayType.FIELD_DATA_FLOAT, nOut, veclen, data.getName(), 
                                                   data.getUnit(), data.getUserData());
                for (int iStep = 0; iStep < data.getNFrames(); iStep++) 
                    outTimeData.setValue(outData[iDataArray][iStep], data.getTime(iStep));
                outDa.setPreferredRanges(data.getPreferredMinValue(),     data.getPreferredMaxValue(), 
                                         data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                outDa.setTimeData(outTimeData);
                outRegularField.addComponent(outDa);
            }
            outRegularField.setMask(valid, 0);
        }
    }
    
    public void update()
    {
        if (inField == null)
            return;
        if (inField instanceof RegularField)
            update((RegularField)inField);
        else
            update((IrregularField)inField);
    }

    public RegularField getOutRegularField()
    {
        return outRegularField;
    }

    public LogicLargeArray getValid()
    {
        return valid;
    }

    public FieldPosition[] getResult()
    {
        return result;
    }

    public int[] getOutDims() {
        return outDims;
    }
    
}
