//</editor-fold>

package pl.edu.icm.visnow.lib.utils.interpolation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import pl.edu.icm.jlargearrays.ByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataObjectInterface;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class FieldPosition implements DataObjectInterface, Comparable, Serializable
{
    
    public int index;
    public final float v;
    public final int[] p;
    public final long[] lp;
    public final float[] t;

    public FieldPosition(int index, float v, int[] p, float[] t) {
        this.index = index;
        this.v = v;
        this.p = p;
        this.t = t;
        lp = null;
    }
    
    public FieldPosition(int index, float v, long[] lp, float[] t) {
        this.index = index;
        this.v = v;
        this.lp = lp;
        this.t = t;
        p = null;
    }
    
    public boolean isValid(byte[] mask)
    {
        if (p == null || p.length < 1)
            return false;
        for (int i : p) 
            if (mask[i] == 0)
                return false;
        return true;
    }
    
    public boolean isValid(LogicLargeArray mask)
    {
        
        if (lp == null || lp.length < 1) {
            if (p == null || p.length < 1)
                return false;
            for (int i : p) 
                if (!mask.getBoolean(i))
                    return false;
            return true;
        }
        for (long i : lp) 
            if (!mask.getBoolean(i))
                return false;
        return true;
    }
    
    @Override
    public float toFloat()
    {
        return v;
    }
    
    public int getHash()
    {
        if (p != null)
            return Arrays.hashCode(p);
        else
            return Arrays.hashCode(lp);
    }
    
    @Override
    public int compareTo(Object o) 
    {
        return ((FieldPosition)o).v < v ? 1 : (((FieldPosition)o).v == v ? 0 : -1);
    }
    
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder(String.format("%6d %6.3f   ", index, v));
        if (p != null)
            for (int i = 0; i < p.length; i++) 
                s.append(String.format("%6d %6.3f  ",p[i], t[i]));
        else
            for (int i = 0; i < p.length; i++) 
                s.append(String.format("%10d %6.3f  ",lp[i], t[i]));
        return s.toString();
    }
    
    public static FieldPosition combine(int ind, float val, float[] weights, FieldPosition[] positions)
    {
        if (positions == null || weights == null || positions.length != weights.length)
            throw new IllegalArgumentException("Both argument arrays must be of the same legth");
        float s = 0;
        for (float w : weights) 
            s += w;
        if (s < .99999 || s > 1.00001)
            throw new IllegalArgumentException("weights must sum to 1");
        int n = 0;
        boolean intPositions = true;
        if (positions[0].p == null)
            intPositions = false;
        for (FieldPosition pos : positions) 
            n += pos.t.length;
        if (intPositions) {
            int[] pp = new int[n];
            float[] pt = new float[n];
            for (int i = 0, l = 0; i < positions.length; i++) {
                FieldPosition pos = positions[i];
                for (int j = 0; j < pos.p.length; j++, l++) {
                    pp[l] = pos.p[j];
                    pt[l] = weights[i] * pos.t[j];
                }
            } 
            return new FieldPosition(ind, val, pp, pt);
        }
        else {
            long[] lpp = new long[n];
            float[] pt = new float[n];
            for (int i = 0, l = 0; i < positions.length; i++) {
                FieldPosition pos = positions[i];
                for (int j = 0; j < pos.p.length; j++, l++) {
                    lpp[l] = pos.lp[j];
                    pt[l] = weights[i] * pos.t[j];
                }
            } 
            return new FieldPosition(ind, val, lpp, pt);
        }
    }
    
    public float[] getInterpolatedVal(byte[] in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < p.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * (0xff & in[vlen * p[i] + j]);
        return val;
    }
    
    public float[] getInterpolatedVal(short[] in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < p.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in[vlen * p[i] + j];
        return val;
    }
    
    public float[] getInterpolatedVal(int[] in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < p.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in[vlen * p[i] + j];
        return val;
    }
    
    public float[] getInterpolatedVal(float[] in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < p.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in[vlen * p[i] + j];
        return val;
    }
    
    public float getInterpolatedVal(byte[] in)
    {
        float val = 0;
        for (int i = 0; i < p.length; i++) 
            val += t[i] * (0xff & in[p[i]]);
        return val;
    }
    
    public float getInterpolatedVal(short[] in)
    {
        float val = 0;
        for (int i = 0; i < p.length; i++) 
            val += t[i] * in[p[i]];
        return val;
    }
    
    public float getInterpolatedVal(int[] in)
    {
        float val = 0;
        for (int i = 0; i < p.length; i++) 
            val += t[i] * in[p[i]];
        return val;
    }
    
    public float getInterpolatedVal(float[] in)
    {
        float val = 0;
        for (int i = 0; i < p.length; i++) 
            val += t[i] * in[p[i]];
        return val;
    }
    
    public float[] getInterpolatedVal(ByteLargeArray in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < lp.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * (0xff & in.getByte(vlen * lp[i] + j));
        return val;
    }
    
    public float[] getInterpolatedVal(ShortLargeArray in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < lp.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in.getShort(vlen * lp[i] + j);
        return val;
    }
    
    public float[] getInterpolatedVal(IntLargeArray in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < lp.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in.getInt(vlen * lp[i] + j);
        return val;
    }
    
    public float[] getInterpolatedVal(FloatLargeArray in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < lp.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += t[i] * in.getFloat(vlen * lp[i] + j);
        return val;
    }
    
    public float[] getInterpolatedVal(DoubleLargeArray in, int vlen)
    {
        float[] val = new float[vlen];
        for (int i = 0; i < lp.length; i++) 
            for (int j = 0; j < vlen; j++)
               val[j] += (float)(t[i] * in.getDouble(vlen * lp[i] + j));
        return val;
    }
    
    public float getInterpolatedVal(ByteLargeArray in)
    {
        float val = 0;
        for (int i = 0; i < lp.length; i++) 
            val += t[i] * (0xff & in.getByte(lp[i]));
        return val;
    }
    
    public float getInterpolatedVal(ShortLargeArray in)
    {
        float val = 0;
        for (int i = 0; i < lp.length; i++) 
            val += t[i] * in.getShort(p[i]);
        return val;
    }
    
    public float getInterpolatedVal(IntLargeArray in)
    {
        float val = 0;
        for (int i = 0; i < lp.length; i++) 
            val += t[i] * in.getInt(p[i]);
        return val;
    }
    
    public float getInterpolatedVal(FloatLargeArray in)
    {
        float val = 0;
        for (int i = 0; i < lp.length; i++) 
            val += t[i] * in.getFloat(p[i]);
        return val;
    }
    
    public float getInterpolatedVal(DoubleLargeArray in)
    {
        float val = 0;
        for (int i = 0; i < p.length; i++) 
            val += (float)(t[i] * in.getDouble(p[i]));
        return val;
    }
    
    private static class ComputeInterpolation implements Runnable
    {
        private final FieldPosition[] nodes;
        private final LargeArray inArray;
        private final LargeArray outArray;
        private final ByteLargeArray mask;
        private final int vlen;
        private final int from;
        private final int to;

        public ComputeInterpolation(FieldPosition[] nodes,
                                    LargeArray inArray, LargeArray outArray, ByteLargeArray mask, 
                                    int vlen, int from, int to) {
            this.nodes = nodes;
            this.inArray = inArray;
            this.outArray = outArray;
            if (mask != null && inArray.length() == vlen * mask.length())
                this.mask = mask;
            else
                this.mask = null;
            this.vlen = vlen;
            this.from = from;
            this.to = to;
        }

        @Override
        public void run() {
            switch (inArray.getType()) {
            case UNSIGNED_BYTE:
                for (int i = from, k = vlen * from; i < to; i++)
                    if (nodes[i] != null)
                        for (int j = 0; j < vlen; j++, k++) { 
                            float v = 0;
                            float[] t = nodes[i].t;
                            int[] p = nodes[i].p;
                            long[] lp  = nodes[i].lp;
                            if (p != null)
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * p[l] + j);
                            else
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                            outArray.setByte(k, (byte)(0xff & (int)v));
                            if (mask != null)
                                mask.setByte(i, (byte)1);
                        }
                    else {
                        if (mask != null)
                            mask.setByte(i, (byte)0);
                        k += vlen;
                    }
                break;
            case BYTE:
                for (int i = from, k = vlen * from; i < to; i++)
                    if (nodes[i] != null)
                        for (int j = 0; j < vlen; j++, k++) { 
                            float v = 0;
                            float[] t = nodes[i].t;
                            int[] p = nodes[i].p;
                            long[] lp  = nodes[i].lp;
                            if (p != null)
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * p[l] + j);
                            else
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                            if (mask != null)
                                mask.setByte(i, (byte)1);
                        }
                    else {
                        if (mask != null)
                            mask.setByte(i, (byte)0);
                        k += vlen;
                    }
                break;
            case INT:
                for (int i = from, k = vlen * from; i < to; i++)
                    if (nodes[i] != null)
                        for (int j = 0; j < vlen; j++, k++) { 
                            float v = 0;
                            float[] t = nodes[i].t;
                            int[] p = nodes[i].p;
                            long[] lp  = nodes[i].lp;
                            if (p != null)
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * p[l] + j);
                            else
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                            outArray.setInt(k, (int)v);
                            if (mask != null)
                                mask.setByte(i, (byte)1);
                        }
                    else {
                        if (mask != null)
                            mask.setByte(i, (byte)0);
                        k += vlen;
                    }
                break;
            case SHORT:
                for (int i = from, k = vlen * from; i < to; i++)
                    if (nodes[i] != null)
                        for (int j = 0; j < vlen; j++, k++) { 
                            float v = 0;
                            float[] t = nodes[i].t;
                            int[] p = nodes[i].p;
                            long[] lp  = nodes[i].lp;
                            if (p != null)
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * p[l] + j);
                            else
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                            outArray.setShort(k, (short)v);
                            if (mask != null)
                                mask.setByte(i, (byte)1);
                        }
                    else {
                        if (mask != null)
                            mask.setByte(i, (byte)0);
                        k += vlen;
                    }
                break;
            case FLOAT:
                for (int i = from, k = vlen * from; i < to; i++)
                    if (nodes[i] != null)
                        for (int j = 0; j < vlen; j++, k++) { 
                            float v = 0;
                            float[] t = nodes[i].t;
                            int[] p = nodes[i].p;
                            long[] lp  = nodes[i].lp;
                            if (p != null)
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * p[l] + j);
                            else
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                            outArray.setFloat(k, v);
                            if (mask != null)
                                mask.setByte(i, (byte)1);
                        }
                    else {
                        if (mask != null)
                            mask.setByte(i, (byte)0);
                        k += vlen;
                    }
                break;
            case DOUBLE:
                for (int i = from, k = vlen * from; i < to; i++)
                    if (nodes[i] != null)
                        for (int j = 0; j < vlen; j++, k++) { 
                            float v = 0;
                            float[] t = nodes[i].t;
                            int[] p = nodes[i].p;
                            long[] lp  = nodes[i].lp;
                            if (p != null)
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * p[l] + j);
                            else
                                for (int l = 0; l < t.length; l++) 
                                    v += t[l] * inArray.getFloat(vlen * lp[l] + j);
                            outArray.setDouble(k, v);
                            if (mask != null)
                                mask.setByte(i, (byte)1);
                        }
                    else {
                        if (mask != null)
                            mask.setByte(i, (byte)0);
                        k += vlen;
                    }
                break;
            }
        }
    }
    
    public static LargeArray interpolate(FieldPosition[] nodes, LargeArray inArray, int vlen)
    {
        if (nodes == null || nodes.length == 0)
            return null;
        int nNodes = nodes.length;
        LargeArray outArray = LargeArrayUtils.create(inArray.getType(), vlen * nNodes);
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = 
                    new Thread(new ComputeInterpolation(nodes, inArray, outArray, null, vlen, 
                                                        (iThread * nNodes) / nThreads,
                                                        ((iThread + 1) * nNodes) / nThreads));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return outArray;
    }
    
    public static LargeArray interpolate(FieldPosition[] nodes, LargeArray inArray, ByteLargeArray mask, int vlen)
    {
        if (nodes == null || nodes.length == 0)
            return null;
        int nNodes = nodes.length;
        LargeArray outArray = LargeArrayUtils.create(inArray.getType(), vlen * nNodes);
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = 
                    new Thread(new ComputeInterpolation(nodes, inArray, outArray, mask, vlen, 
                                                        (iThread * nNodes) / nThreads,
                                                        ((iThread + 1) * nNodes) / nThreads));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        return outArray;
    }
    
    public static TimeData interpolate(FieldPosition[] nodes, TimeData inData, int vlen)
    {
        float[] inTimes = inData.getTimesAsArray();
        ArrayList<Float> outTimeSeries = new ArrayList<>();
        ArrayList<LargeArray> outDataSeries = new ArrayList<>();
        for (int i = 0; i < inTimes.length; i++) {
            outTimeSeries.add(inTimes[i]);
            outDataSeries.add(interpolate(nodes, inData.getValues().get(i), vlen));
        }        
        return new TimeData(outTimeSeries, outDataSeries, vlen);
    }
    
    public static DataArray interpolate(FieldPosition[] nodes, DataArray inData)
    {
        TimeData outTimeData = interpolate(nodes, inData.getTimeData(), inData.getVectorLength());
        return DataArray.create(outTimeData, inData.getVectorLength(), inData.getName(), inData.getUnit(), inData.getUserData()).
                                preferredRanges(inData.getPreferredMinValue(),     inData.getPreferredMaxValue(), 
                                                inData.getPreferredPhysMinValue(), inData.getPreferredPhysMaxValue());
    }
    
    public static float[] interpolateToIndices(FieldPosition[] nodes, int[] dims)
    {
        int nDims = dims.length;
        float[] ndx = new float[nDims];
        float[] indices = new float[nDims * nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            float[] t = nodes[i].t;
            int[] p = nodes[i].p;
            long[] lp  = nodes[i].lp;
            Arrays.fill(ndx, 0);
            for (int j = 0; j < t.length; j++) {
                long k = p != null ? p[j] : lp[j];
                for (int m = 0; m < nDims; m++) {
                    ndx[m] += t[j] * (k % dims[m]);
                    k /= dims[m];
                }
            }
            System.arraycopy(ndx, 0, indices, nDims * i, nDims);
        }
        return indices;
    }
}
