//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.isosurface;

/*
 * RegularFieldIsosurfaceV1.java
 *
 * Created on August 14, 2004, 2:06 PM
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
import java.util.ArrayList;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.cells.CellType;
import static pl.edu.icm.visnow.lib.utils.isosurface.IsosurfaceEngine.CELLSET_NAME;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class RegularFieldIsosurface extends IsosurfaceEngine
{
    
    protected static final int CHUNK_SIZE = 4096;
    protected RegularField inField = null;
    protected static int[][][][] ISOSURFACE_TRIANGLES = null;
    protected static int[][][][] ISOSURFACE_EDGES = null;
    protected SliceRemap sliceRemap;

    /**
     * Creates a new instance of RegularFieldIsosurfaceV1
     * @param in inpur regular field
     */
    public RegularFieldIsosurface(RegularField in)
    {
        ISOSURFACE_TRIANGLES = LookUpTables.getISOSURFACE_TRIANGLES();
        ISOSURFACE_EDGES = LookUpTables.getISOSURFACE_EDGES();
        this.inField = in;
    }

    /**
     * finding isosurface  nodes and node normals on lines parallel to x axis
     * @param reducedDims  dimensions of downsized and cropped dataset
     * @param low          first crop indices
     * @param dind         downsize values
     * @param currentLayer downsized and cropped data from the current slab
     * @param slice        0 or 1 for lower and upper slab plane
     * @param zIndex       index of slab
     * @param p            current index of isosurface point 
     * @param pointsVect   vector (ArrayList) of point coordinates
     * @param vertexNormalsVect vector (ArrayList) of normals
     * @param hexEdgeIndices array of hexahedra edges in a slab: -1 indicates edge is not cut, otherwise - cutting node number
                             hex edges are ordered according to the hexTriangulationsEdges table in createLUT
     * @return last used index of isosurface point 
     */
    protected static int computeXLinesIntersections(int[] reducedDims, int[] low, int[] dind, 
                                                    float[][] currentLayer, int slice, int zIndex, int p,
                                                    ArrayList<float[]> pointsVect, ArrayList<float[]> vertexNormalsVect,
                                                    int[][][] hexEdgeIndices)
    {
        int yzSliceSize = reducedDims[0];
        int z = low[2] + (zIndex + slice) * dind[2];
        float[] pt;
        float[] nv;
        for (int yIndex = 0; yIndex < reducedDims[1]; yIndex++) 
        {
            int y = low[1] + yIndex * dind[1];
            for (int xIndex = 0; xIndex < reducedDims[0] - 1; xIndex++) {
                int pointIndex = yIndex * reducedDims[0] + xIndex;
                int x = low[0] + xIndex * dind[0];
                // x, y are indices of first edge node
                float u = currentLayer[slice][pointIndex];
                float v = currentLayer[slice][pointIndex + 1];
                if (u * v < 0) {   // isosurface cuts the edge
                    int k = p % CHUNK_SIZE;
                    if (k == 0) {
                        pt = new float[3 * CHUNK_SIZE];
                        pointsVect.add(pt);
                        nv = new float[3 * CHUNK_SIZE];
                        vertexNormalsVect.add(nv);
                    }
                    else {
                        pt = pointsVect.get(pointsVect.size() - 1);
                        nv = vertexNormalsVect.get(vertexNormalsVect.size() - 1);
                    }
                    pt[3 * k] = x + u / (u - v) * dind[0];
                    pt[3 * k + 1] = y;
                    pt[3 * k + 2] = z;
                    nv[3 * k] = (v - u) / dind[0];
                    if (yIndex > 0 && yIndex < reducedDims[1] - 1)
                        nv[3 * k + 1] = .5f * (currentLayer[0][pointIndex + yzSliceSize] - currentLayer[0][pointIndex - yzSliceSize]) / dind[1];
                    else {
                        if (yIndex > 0)
                            nv[3 * k + 1] = (currentLayer[0][pointIndex] - currentLayer[0][pointIndex - yzSliceSize]) / dind[1];
                        else
                            nv[3 * k + 1] = (currentLayer[0][pointIndex + yzSliceSize] - currentLayer[0][pointIndex]) / dind[1];
                    }
                    nv[3 * k + 2] = (currentLayer[1][pointIndex] - currentLayer[0][pointIndex]) / dind[2];
                    if (slice == 0) {
                        if (yIndex < reducedDims[1] - 1)
                            hexEdgeIndices[yIndex][xIndex][0] = p;
                        if (yIndex > 0)
                            hexEdgeIndices[yIndex - 1][xIndex][5] = p;
                    } else
                    {
                        if (yIndex < reducedDims[1] - 1)
                            hexEdgeIndices[yIndex][xIndex][8] = p;
                        if (yIndex > 0)
                            hexEdgeIndices[yIndex - 1][xIndex][11] = p;    
                    }
                    p += 1;
                }
            }
        }
        return p;
    }
    
    
    /**
     * finding isosurface  nodes and node normals on lines parallel to y axis
     * @param reducedDims  dimensions of downsized and cropped dataset
     * @param low          first crop indices
     * @param dind         downsize values
     * @param currentLayer downsized and cropped data from the current slab
     * @param slice        0 or 1 for lower and upper slab plane
     * @param zIndex       index of slab
     * @param p            current index of isosurface point 
     * @param pointsVect   vector (ArrayList) of point coordinates
     * @param vertexNormalsVect vector (ArrayList) of normals
     * @param hexEdgeIndices array of hexahedra edges in a slab: -1 indicates edge is not cut, otherwise - cutting node number
                             hex edges are ordered according to the hexTriangulationsEdges table in createLUT
     * @return last used index of isosurface point 
     */
    protected static int computeYLinesIntersections(int[] reducedDims, int[] low, int[] dind, 
                                                    float[][] currentLayer, int slice, int zIndex, int p,
                                                    ArrayList<float[]> pointsVect, ArrayList<float[]> vertexNormalsVect,
                                                    int[][][] hexEdgeIndices)
    {
        int yzSliceSize = reducedDims[0];
        float[] pt;
        float[] nv;
        int z = low[2] + (zIndex + slice) * dind[2];
        for (int j = 0; j < reducedDims[1] - 1; j++) //finding points in y lines
        {
            int y = low[1] + j * dind[1];
            for (int i = 0; i < reducedDims[0]; i++) {
                int ip = j * reducedDims[0] + i;
                int x = low[0] + i * dind[0];
                float u = currentLayer[slice][ip];
                float v = currentLayer[slice][ip + yzSliceSize];
                if (u * v < 0) {
                    int k = p % CHUNK_SIZE;
                    if (k == 0) {
                        pt = new float[3 * CHUNK_SIZE];
                        pointsVect.add(pt);
                        nv = new float[3 * CHUNK_SIZE];
                        vertexNormalsVect.add(nv);
                    }
                    else {
                        pt = pointsVect.get(pointsVect.size() - 1);
                        nv = vertexNormalsVect.get(vertexNormalsVect.size() - 1);
                    }
                    pt[3 * k] = x;
                    pt[3 * k + 1] = y + u / (u - v) * dind[1];
                    pt[3 * k + 2] = z;
                    if (i > 0 && i < reducedDims[0] - 1)
                        nv[3 * k] = .5f * (currentLayer[0][ip + 1] - currentLayer[0][ip - 1]) / dind[0];
                    else {
                        if (i > 0)
                            nv[3 * k] = (currentLayer[0][ip] - currentLayer[0][ip - 1]) / dind[0];
                        else
                            nv[3 * k] = (currentLayer[0][ip + 1] - currentLayer[0][ip]) / dind[0];
                    }
                    nv[3 * k + 1] = (v - u) / dind[1];
                    nv[3 * k + 2] = (currentLayer[1][ip] - currentLayer[0][ip]) / dind[2];
                    if (slice == 0) {
                        if (i < reducedDims[0] - 1)
                            hexEdgeIndices[j][i][1] = p;
                        if (i > 0)
                            hexEdgeIndices[j][i - 1][3] = p;
                    }
                    else {
                        if (i < reducedDims[0] - 1)
                            hexEdgeIndices[j][i][9] = p;
                        if (i > 0)
                            hexEdgeIndices[j][i - 1][10] = p;
                    }
                    p += 1;
                }
            }
        }
        return p;
    }
    
    
    /**
     * finding isosurface  nodes and node normals on lines parallel to y axis
     * @param reducedDims  dimensions of downsized and cropped dataset
     * @param low          first crop indices
     * @param dind         downsize values
     * @param currentLayer downsized and cropped data from the current slab
     * @param zIndex       index of slab
     * @param p            current index of isosurface point 
     * @param pointsVect   vector (ArrayList) of point coordinates
     * @param vertexNormalsVect vector (ArrayList) of normals
     * @param hexEdgeIndices array of hexahedra edges in a slab: -1 indicates edge is not cut, otherwise - cutting node number
                             hex edges are ordered according to the hexTriangulationsEdges table in createLUT
     * @return last used index of isosurface point 
     */
    protected static int computeZLinesIntersections(int[] reducedDims, int[] low, int[] dind, 
                                                    float[][] currentLayer, int zIndex, int p,
                                                    ArrayList<float[]> pointsVect, ArrayList<float[]> vertexNormalsVect,
                                                    int[][][] hexEdgeIndices)
    {
        int yzSliceSize = reducedDims[0];
        float[] pt;
        float[] nv;
        int z = low[2] + zIndex * dind[2];
        for (int j = 0; j < reducedDims[1]; j++) //finding points in z lines
        {
            float y = low[1] + j * dind[1];
            for (int i = 0; i < reducedDims[0]; i++) {
                int ip = j * reducedDims[0] + i;
                float x = low[0] + i * dind[0];
                float u = currentLayer[0][ip];
                float v = currentLayer[1][ip];
                if (u * v < 0) {
                    int k = p % CHUNK_SIZE;
                    if (k == 0) {
                        pt = new float[3 * CHUNK_SIZE];
                        pointsVect.add(pt);
                        nv = new float[3 * CHUNK_SIZE];
                        vertexNormalsVect.add(nv);
                    } else {
                        pt = pointsVect.get(pointsVect.size() - 1);
                        nv = vertexNormalsVect.get(vertexNormalsVect.size() - 1);
                    }
                    pt[3 * k] = x;
                    pt[3 * k + 1] = y;
                    pt[3 * k + 2] = z + u / (u - v) * dind[2];
                    if (i > 0 && i < reducedDims[0] - 1)
                        nv[3 * k] = .5f * (currentLayer[0][ip + 1] - currentLayer[0][ip - 1]) / dind[0];
                    else {
                        if (i > 0)
                            nv[3 * k] = (currentLayer[0][ip] - currentLayer[0][ip - 1]) / dind[0];
                        else
                            nv[3 * k] = (currentLayer[0][ip + 1] - currentLayer[0][ip]) / dind[0];
                    }
                    if (j > 0 && j < reducedDims[1] - 1)
                        nv[3 * k + 1] = .5f * (currentLayer[0][ip + yzSliceSize] - currentLayer[0][ip - yzSliceSize]) / dind[1];
                    else {
                        if (j > 0)
                            nv[3 * k + 1] = (currentLayer[0][ip] - currentLayer[0][ip - yzSliceSize]) / dind[1];
                        else
                            nv[3 * k + 1] = (currentLayer[0][ip + yzSliceSize] - currentLayer[0][ip]) / dind[1];
                    }
                    nv[3 * k + 2] = (v - u) / dind[2];
                    if (j < reducedDims[1] - 1 && i < reducedDims[0] - 1)
                        hexEdgeIndices[j][i][2] = p;
                    if (j > 0 && i < reducedDims[0] - 1)
                        hexEdgeIndices[j - 1][i][6] = p;
                    if (j < reducedDims[1] - 1 && i > 0)
                        hexEdgeIndices[j][i - 1][4] = p;
                    if (j > 0 && i > 0)
                        hexEdgeIndices[j - 1][i - 1][7] = p;
                    p += 1;
                }
            }
        }
        return p;
    }

    public void setSliceRemap(SliceRemap sliceRemap) {
        this.sliceRemap = sliceRemap;
    }
    
    /**
     * Creates isosurface of a data component of a RegularField in at threshold value threshold
     * and interpolates selected data components on the surface/line mesh
     * Isosurface is generated from the input field cropped and downsized according to the parameters
     * <p>
     * @param params Parameters of isosurface
     * <p>
     * @param threshold
     * @return a TriangulatedField2D containing surface representation and optimized line representation of the
     *         isosurface with outData components interpolated
     */
    @Override
    public IrregularField makeIsosurface(IsosurfaceEngineParams params, float threshold)
    {
        int[] edgeAxis = {0, 1, 2, 1, 2, 0, 2, 2, 0, 1, 1, 0};
        String[] axNames = {"x", "y", "z"};
        int comp = params.getIsoComponent();
        int[] low = params.getLow();
        int[] up = params.getUp();
        int[] d = params.getDownsize();
        int[] outComp = null;
        if (inField == null ||
            inField.getLDims().length != 3 ||
            comp < 0 ||
            comp >= inField.getNComponents() ||
            threshold < inField.getComponent(comp).getPreferredMinValue() ||
            threshold > inField.getComponent(comp).getPreferredMaxValue() ||
            low[0] < 0 || low[1] < 0 || low[2] < 0 ||
            up[0] > inField.getLDims()[0] ||
            up[1] > inField.getLDims()[1] ||
            up[2] > inField.getLDims()[2] ||
            (up[0] - low[0]) / d[0] < 2 ||
            (up[1] - low[1]) / d[1] < 2 ||
            (up[2] - low[2]) / d[2] < 2)
            return null;

        int[] inDims = inField.getDims();
        DataArray data = inField.getComponent(comp);

        boolean isValidity = inField.hasMask();
        LogicLargeArray valid = isValidity ? inField.getCurrentMask() : null;

        ArrayList<float[]> pointsVect = new ArrayList<>();
        ArrayList<float[]> vertexNormalsVect = new ArrayList<>();
        ArrayList<int[]> trianglesVect = new ArrayList<>();
        ArrayList<int[]> edgesVect = new ArrayList<>();
        ArrayList<int[]> orVect = new ArrayList<>();

        float[] pt, nv;
        int[] tr = null;
        int[] ed = null;
        int[] or = null;
        int nIsosurfaceNodes = 0;
        int nIsosurfaceEdges = 0;
        int nIsosurfaceTriangles = 0;
        
        float[] a = new float[3];
        float[] b = new float[3];
        float[] originalDataSlice;
        int[] reducedDims = new int[3];
        int[] dind = {1, 1, 1};
        
        outComp = new int[inField.getNComponents()];
        
        // array of indices of components mapped to the isosurface 
        int nMappedComponents = 1;
        outComp[0] = comp;
        for (int i = 0; i < inField.getNComponents(); i++)
            if (i != comp && inField.getComponent(i).isNumeric()) {
                outComp[nMappedComponents] = i;
                nMappedComponents += 1;
            }
        
        // finding dimensions of downsized data
        for (int i = 0; i < 3; i++) {
            reducedDims[i] = (up[i] - low[i] + d[i] - 1) / d[i];
            dind[i] *= d[i];
        }
        
        int xyzSliceSize = 1;
        int yzSliceSize = reducedDims[0];
        int zSliceSize = reducedDims[0] * reducedDims[1];
        
        // "cut" edge in regular mesh means that values at the end of the edge are on thew opposite sides of threshold

        // array of hexahedra edges in a slab: -1 indicates edge is not cut, otherwise - cutting node number
        // hex edges are ordered according to the hexTriangulationsEdges table in createLUT
        int[][][] hexEdgeIndices = new int[reducedDims[1] - 1][reducedDims[0] - 1][12];
        
        // single thick layer of isosurfaced data values
        float[][] currentLayer = new float[2][zSliceSize];

        //zrównoleglić?
        for (int zIndex = 0; zIndex < reducedDims[2] - 1; zIndex++) {
            fireStatusChanged((.4f * zIndex) / (reducedDims[2] - 1));
            
//            // safety fill by -1 to detect errorneous triangle node at uncut edge
//            for (int[][] lPt : hexEdgeIndices) 
//                for (int[] is : lPt) 
//                    Arrays.fill(is, -1);
            // layer index in original data
            int l = low[2] + zIndex * d[2];
            if (zIndex == 0) {
                if (data.getVectorLength() > 1)
                    originalDataSlice = inField.getCurrent2DNormSlice(comp, 2, l).getData();
                else
                    originalDataSlice = inField.getCurrent2DFloatSlice(comp, 2, l).getData();
                // downsizing lowest original slice to obtain 0-th slice of 0-th slab
                for (int yIndex = 0, k = 0; yIndex < reducedDims[1]; yIndex++)
                    for (int xIndex = 0, m = (int) ((yIndex * d[1] + low[1]) * inDims[0] + low[0]); 
                             xIndex < reducedDims[0]; 
                             xIndex++, k++, m += d[0])
                        currentLayer[0][k] = originalDataSlice[m];
                sliceRemap.remap(currentLayer[0], threshold);
            } else
                System.arraycopy(currentLayer[1], 0, currentLayer[0], 0, zSliceSize);
            
            if (data.getVectorLength() > 1)
                originalDataSlice = inField.getCurrent2DNormSlice(comp, 2, l + d[2]).getData();
            else
                originalDataSlice = inField.getCurrent2DFloatSlice(comp, 2, l + d[2]).getData();
            for (int yIndex = 0, k = 0; yIndex < reducedDims[1]; yIndex++)
                for (int xIndex = 0, m = (int) ((yIndex * d[1] + low[1]) * inDims[0] + low[0]); 
                         xIndex < reducedDims[0]; 
                         xIndex++, k++, m += d[0])
                        currentLayer[1][k] = originalDataSlice[m];
            sliceRemap.remap(currentLayer[1], threshold);
            
            if (zIndex == 0) {
                nIsosurfaceNodes = computeXLinesIntersections(reducedDims, low, dind, 
                                                              currentLayer, 0, zIndex, 
                                                              nIsosurfaceNodes,
                                                              pointsVect, vertexNormalsVect, hexEdgeIndices);
                nIsosurfaceNodes = computeYLinesIntersections(reducedDims, low, dind, 
                                                              currentLayer, 0, zIndex, 
                                                              nIsosurfaceNodes,
                                                              pointsVect, vertexNormalsVect, hexEdgeIndices);
            } else {
                for (int yIndex = 0; yIndex < reducedDims[1] - 1; yIndex++)
                    for (int xIndex = 0; xIndex < reducedDims[0] - 1; xIndex++) {
                        hexEdgeIndices[yIndex][xIndex][0] = hexEdgeIndices[yIndex][xIndex][8];
                        hexEdgeIndices[yIndex][xIndex][1] = hexEdgeIndices[yIndex][xIndex][9];
                        hexEdgeIndices[yIndex][xIndex][3] = hexEdgeIndices[yIndex][xIndex][10];
                        hexEdgeIndices[yIndex][xIndex][5] = hexEdgeIndices[yIndex][xIndex][11];
                    }
            }
            nIsosurfaceNodes = computeXLinesIntersections(reducedDims, low, dind, 
                                                          currentLayer, 1, zIndex, 
                                                          nIsosurfaceNodes,
                                                          pointsVect, vertexNormalsVect, hexEdgeIndices);
            nIsosurfaceNodes = computeYLinesIntersections(reducedDims, low, dind, 
                                                          currentLayer, 1, zIndex, 
                                                          nIsosurfaceNodes,
                                                          pointsVect, vertexNormalsVect, hexEdgeIndices);
            nIsosurfaceNodes = computeZLinesIntersections(reducedDims, low, dind, 
                                                          currentLayer, zIndex, nIsosurfaceNodes,
                                                          pointsVect, vertexNormalsVect, hexEdgeIndices);
            
            for (int yIndex = 0; yIndex < reducedDims[1] - 1; yIndex++)
                //finding triangles
                for (int xIndex = 0; xIndex < reducedDims[0] - 1; xIndex++) {
                    int kk = (int) (((zIndex * d[2] + low[2]) * inDims[1] + 
                                      yIndex * d[1] + low[1]) * inDims[0] + 
                                      xIndex * d[0] + low[0]);
                    int kx = d[0];
                    int ky = (int) (d[1] * inDims[0]);
                    int kz = (int) (d[2] * inDims[1] * inDims[0]);
                    int k = yIndex * reducedDims[0] + xIndex;
                    if (isValidity && valid != null &&
                        !(valid.getBoolean(kk)           && valid.getBoolean(kk + kx) && 
                          valid.getBoolean(kk +      ky) && valid.getBoolean(kk + kx + ky) &&
                          valid.getBoolean(kk +      kz) && valid.getBoolean(kk + kx +      kz) && 
                          valid.getBoolean(kk + ky + kz) && valid.getBoolean(kk + kx + ky + kz)))
                        continue;
                    if (Float.isNaN(currentLayer[0][k]) ||
                        Float.isNaN(currentLayer[0][k +               xyzSliceSize]) ||
                        Float.isNaN(currentLayer[0][k + yzSliceSize])                ||
                        Float.isNaN(currentLayer[0][k + yzSliceSize + xyzSliceSize]) ||
                        Float.isNaN(currentLayer[1][k]) ||
                        Float.isNaN(currentLayer[1][k +               xyzSliceSize]) ||
                        Float.isNaN(currentLayer[1][k + yzSliceSize])                ||
                        Float.isNaN(currentLayer[1][k + yzSliceSize + xyzSliceSize]))
                        continue;
                    int sign = 0;
                    if (currentLayer[0][k] > 0)
                        sign |= 1;
                    if (currentLayer[0][k + xyzSliceSize] > 0)
                        sign |= 1 << 1;
                    if (currentLayer[0][k + yzSliceSize] > 0)
                        sign |= 1 << 2;
                    if (currentLayer[0][k + yzSliceSize + xyzSliceSize] > 0)
                        sign |= 1 << 3;
                    if (currentLayer[1][k] > 0)
                        sign |= 1 << 4;
                    if (currentLayer[1][k + xyzSliceSize] > 0)
                        sign |= 1 << 5;
                    if (currentLayer[1][k + yzSliceSize] > 0)
                        sign |= 1 << 6;
                    if (currentLayer[1][k + yzSliceSize + xyzSliceSize] > 0)
                        sign |= 1 << 7;
                    
                    int parity = (xIndex + yIndex + zIndex) % 2;
                    
                    int[][] trIn = ISOSURFACE_TRIANGLES[parity][sign];
                    for (int it = 0; it < trIn.length; it++) {
                        int m = nIsosurfaceTriangles % CHUNK_SIZE;
                        if (m == 0) {
                            tr = new int[3 * CHUNK_SIZE];
                            trianglesVect.add(tr);
                            or = new int[CHUNK_SIZE];
                            orVect.add(or);
                        }
                        for (int jt = 0; jt < 3; jt++)
                            if (hexEdgeIndices[yIndex][xIndex][trIn[it][jt]] > -1)
                                tr[3 * m + jt] = hexEdgeIndices[yIndex][xIndex][trIn[it][jt]];
                            else {
                                System.out.printf("%3d  %3d %3d %3d     %2d %2d %2d    %2d%1s      ", 256 * parity + sign, 
                                                  xIndex, yIndex, zIndex, 
                                                  trIn[it][0], trIn[it][1], trIn[it][2], trIn[it][jt], axNames[edgeAxis[trIn[it][jt]]]);
                                for (int n = 0; n < 12; n++) 
                                    System.out.printf("%5d ",hexEdgeIndices[yIndex][xIndex][n]); 
                                System.out.println("");
                            }
                        or[m] = (((xIndex + yIndex + l) % 2) << 16) | (sign << 8) | it;
                        nIsosurfaceTriangles += 1;
                    }
                    
                    int[][] edIn = ISOSURFACE_EDGES[parity][sign];
                    
                    for (int it = 0; it < edIn.length; it++) {
                        int m = nIsosurfaceEdges % CHUNK_SIZE;
                        if (m == 0) {
                            ed = new int[2 * CHUNK_SIZE];
                            edgesVect.add(ed);
                        }
                        for (int jt = 0; jt < 2; jt++)
                            ed[2 * m + jt] = hexEdgeIndices[yIndex][xIndex][edIn[it][jt]];
                        nIsosurfaceEdges += 1;
                    }
                }
        }
        if (nIsosurfaceNodes <= 0)
            return null;
        IrregularField out = new IrregularField(nIsosurfaceNodes);
        CellSet isosurfaceCellSet = new CellSet(CELLSET_NAME);
        out.addCellSet(isosurfaceCellSet);
        if (nIsosurfaceNodes == 0 || nIsosurfaceTriangles == 0)
            return null;

        int outNNodes = nIsosurfaceNodes;
        float[] coords = new float[3 * outNNodes];
        float[] normals = new float[3 * outNNodes];
        float[] uncert = null;
        boolean isUncert = params.isUncertainty();
        if (isUncert)
            uncert = new float[3 * outNNodes];
        float uu = 1;
    pointloop:
        for (int i = 0, l = 0; i < pointsVect.size(); i++) {
            fireStatusChanged(.4f + (.15f * (i + 1)) / (pointsVect.size()));
            pt = pointsVect.get(i);
            nv = vertexNormalsVect.get(i);
            for (int j = 0; j < CHUNK_SIZE; j++) {
                uu = nv[3 * j] * nv[3 * j] + nv[3 * j + 1] * nv[3 * j + 1] + nv[3 * j + 2] * nv[3 * j + 2];
                float u = (float) sqrt(uu);
                if (uu == 0)
                    uu = u = 1;
                for (int ip = 0; ip < 3; ip++, l++) {
                    coords[l] = pt[3 * j + ip];
                    normals[l] = nv[3 * j + ip] / u;
                    if (isUncert)
                        uncert[l] = nv[3 * j + ip] / uu;
                }
                if (l >= 3 * nIsosurfaceNodes)
                    break pointloop;
            }
        }
        int[] cells = new int[3 * nIsosurfaceTriangles];
        byte[] orientations = new byte[nIsosurfaceTriangles];
        int[] dataIndices = new int[nIsosurfaceTriangles];
        for (int i = 0; i < nIsosurfaceTriangles; i++) {
            dataIndices[i] = i;
            orientations[i] = 1;
        }
        
    triangleloop:
        for (int i = 0, l = 0; i < trianglesVect.size(); i++) {
            fireStatusChanged(.55f + (.15f * (i + 1)) / (trianglesVect.size()));
            tr = trianglesVect.get(i);
            for (int j = 0; j < tr.length; j += 3, l += 3) {
                if (l >= 3 * nIsosurfaceTriangles)
                    break triangleloop;
                int k0 = cells[l] = tr[j];
                int k1 = cells[l + 1] = tr[j + 1];
                int k2 = cells[l + 2] = tr[j + 2];
                for (int ij = 0; ij < 3; ij++) {
                    a[ij] = coords[3 * k1 + ij] - coords[3 * k0 + ij];
                    b[ij] = coords[3 * k2 + ij] - coords[3 * k0 + ij];
                }
                if ((a[1] * b[2] - a[2] * b[1]) * normals[3 * k0] + 
                    (a[2] * b[0] - a[0] * b[2]) * normals[3 * k0 + 1] + 
                    (a[0] * b[1] - a[1] * b[0]) * normals[3 * k0 + 2] < 0) {
                    cells[l + 1] = k2;
                    cells[l + 2] = k1;
                }
            }
        }

        CellArray triangleArray = new CellArray(CellType.TRIANGLE, cells, orientations, dataIndices);
        isosurfaceCellSet.setCellArray(triangleArray);
        isosurfaceCellSet.setBoundaryCellArray(triangleArray);

        int[] edges = new int[2 * nIsosurfaceEdges];
        byte[] edgeOrientations = new byte[nIsosurfaceEdges];
        int[] edgeDataIndices = new int[nIsosurfaceEdges];
    edgeloop:
        for (int i = 0, l = 0; i < edgesVect.size(); i++) {
            ed = edgesVect.get(i);
            for (int j = 0; j < ed.length; j++, l++) {
                if (l >= 2 * nIsosurfaceEdges) {
                    break edgeloop;
                }
                edges[l] = ed[j];
            }
        }
        java.util.Arrays.fill(orientations, (byte)1);
        CellArray edgeArray = new CellArray(CellType.SEGMENT, edges, edgeOrientations, edgeDataIndices);
        isosurfaceCellSet.setCellArray(edgeArray);

        int[] points = new int[outNNodes];
        for (int n = 0; n < points.length; n++)
            points[n] = n;

        CellArray pointArray = new CellArray(CellType.POINT, points, null, null);
        isosurfaceCellSet.setBoundaryCellArray(pointArray);
        if (data.getVectorLength() == 1) {
            float[] fda = new float[outNNodes];
            for (int i = 0; i < nIsosurfaceNodes; i++)
                fda[i] = threshold;
            try {
                DataArray da = DataArray.create(fda, 1, data.getName() + "(threshold)").unit(data.getUnit()).userData(data.getUserData());
                da.setPreferredRanges(data.getPreferredMinValue(), data.getPreferredMaxValue(), data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                out.addComponent(da);
            } catch (Exception e) {
                System.out.println("could not create threshold data array");
            }
        }
        for (int n = 0; n < outComp.length; n++) {
            fireStatusChanged(.8f + (.1f * (n + 1)) / outComp.length);
            int k = outComp[n];
            DataArray da = inField.getComponent(k);
            int vlen = da.getVectorLength();
            if (da == data && data.getVectorLength() == 1)
                continue;
            try {
                switch (da.getType()) {
                    case FIELD_DATA_LOGIC:
                        byte[] outda = new byte[outNNodes * vlen];
                        for (int i = 0, l = 0; i < (int) out.getNNodes(); i++) {
                            byte[] bi = inField.getInterpolatedData(da.getRawByteArray(), coords[3 * i], coords[3 * i + 1], coords[3 * i + 2]);
                            for (int j = 0; j < bi.length; j++, l++)
                                outda[l] = bi[j];
                        }
                        DataArray dal = DataArray.create(new LogicLargeArray(outda), da.getVectorLength(), da.getName()).unit(da.getUnit()).userData(da.getUserData());
                        dal.setPreferredRanges(da.getPreferredMinValue(), da.getPreferredMaxValue(), da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
                        out.addComponent(dal);
                        break;
                    case FIELD_DATA_BYTE:
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        LargeArray dda = LargeArrayUtils.create(da.getRawArray().getType(), outNNodes * vlen, false);
                        for (int i = 0; i < (int) out.getNNodes(); i++) {
                            Object di = inField.getInterpolatedData(da.getRawArray(), coords[3 * i], coords[3 * i + 1], coords[3 * i + 2]);
                            LargeArrayUtils.arraycopy(di, 0, dda, vlen * i, vlen);
                        }
                        DataArray da2 = DataArray.create(dda, da.getVectorLength(), da.getName()).unit(da.getUnit()).userData(da.getUserData());
                        da2.setPreferredRanges(da.getPreferredMinValue(), da.getPreferredMaxValue(), da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
                        out.addComponent(da2);
                        break;
                    default:
                        throw new IllegalArgumentException("Unsupported data type");
                }
            } catch (Exception e) {
                System.out.println("could not interpolate " + da.getName());
            }
        }
        if (isUncert)
            out.addComponent(DataArray.create(uncert, 3, data.getName() + "(uncertainty)"));
        for (int j = 0; j < outNNodes; j++) {
            float[] cc = inField.getGridCoords(coords[3 * j], coords[3 * j + 1], coords[3 * j + 2]);
            for (int l = 0; l < 3; l++)
                coords[3 * j + l] = cc[l];
        }
        out.setCoords(new FloatLargeArray(coords), 0);
        out.setNormals(new FloatLargeArray(normals));
        return out;
    }
}
