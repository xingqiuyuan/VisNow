//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.isosurface;

import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.ByteDataArray;
import pl.edu.icm.jscic.dataarrays.DoubleDataArray;
import pl.edu.icm.jscic.dataarrays.FloatDataArray;
import pl.edu.icm.jscic.dataarrays.IntDataArray;
import pl.edu.icm.jscic.dataarrays.ShortDataArray;

public class IrregularFieldIsosurface extends IsosurfaceEngine
{

    private final static int[][] cellEdg = new int[][]{{0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}};
    private final static int[][] cellInd = new int[][]{{},
                                                       {0, 1, 2},
                                                       {0, 3, 4},
                                                       {1, 2, 4, 3},
                                                       {1, 3, 5},
                                                       {0, 2, 5, 3},
                                                       {0, 1, 5, 4},
                                                       {2, 4, 5},
                                                       {2, 4, 5},
                                                       {0, 1, 5, 4},
                                                       {0, 2, 5, 3},
                                                       {1, 3, 5},
                                                       {1, 2, 4, 3},
                                                       {0, 3, 4},
                                                       {0, 1, 2},
                                                       {}};
    private int[][] buckets = new int[CellCache.BUCKETS_NUMBER][];
    private float[] bucketsCeilingFun = new float[CellCache.BUCKETS_NUMBER];
    private float dataMin, dataMax;
    private int componentNumber;
    private IrregularField inField;
    private DataArray data;
    private float[] isoData;
    private IsosurfaceEngineParams params;
    private long lastRecompute = -1;
    private CellCache cellCache;
    private IrregularField out;

    /**
     * Instantiates isosurface.
     * 
     * @param inField Input field to compute isosurface on
     */
    public IrregularFieldIsosurface(IrregularField inField)
    {
        if (inField == null) {
            throw new IllegalArgumentException("Incoming field cannot be null");
        }
        this.inField = inField;
    }

    private void interpolateDataToSurface(int nData, int nIsoNodes, int[] newNodeInds, float[] newNodeRatios) throws Exception
    {
        DataArray cData = inField.getComponent(nData);
        int vlen = cData.getVectorLength();
        switch (cData.getType()) {
            case FIELD_DATA_BYTE:
                byte[] bData = ((ByteDataArray)cData).getRawArray().getData();
                byte[] outbData = new byte[vlen * nIsoNodes];
                for (int i = 0; i < nIsoNodes; i++) {
                    int e0 = newNodeInds[2 * i];
                    int e1 = newNodeInds[2 * i + 1];
                    float r = newNodeRatios[i];
                    for (int j = 0; j < vlen; j++)
                        outbData[vlen * i + j] = (byte) (0xff & (int) (r * (0xff & bData[vlen * e1 + j]) +
                            (1 - r) * (0xff & bData[vlen * e0 + j])));
                }
                DataArray da = DataArray.create(outbData, vlen, cData.getName()).unit(cData.getUnit()).userData(cData.getUserData());
                da.setPreferredRanges(cData.getPreferredMinValue(), cData.getPreferredMaxValue(), cData.getPreferredPhysMinValue(), cData.getPreferredPhysMaxValue());
                out.addComponent(da);
                break;
            case FIELD_DATA_SHORT:
                short[] sData = ((ShortDataArray)cData).getRawArray().getData();
                short[] outsData = new short[vlen * nIsoNodes];
                for (int i = 0; i < nIsoNodes; i++) {
                    int e0 = newNodeInds[2 * i];
                    int e1 = newNodeInds[2 * i + 1];
                    float r = newNodeRatios[i];
                    for (int j = 0; j < vlen; j++)
                        outsData[vlen * i + j] = (short) (r * sData[vlen * e1 + j] + (1 - r) * sData[vlen * e0 + j]);
                }
                DataArray das = DataArray.create(outsData, vlen, cData.getName()).unit(cData.getUnit()).userData(cData.getUserData());
                das.setPreferredRanges(cData.getPreferredMinValue(), cData.getPreferredMaxValue(), cData.getPreferredPhysMinValue(), cData.getPreferredPhysMaxValue());
                out.addComponent(das);
                break;
            case FIELD_DATA_INT:
                int[] iData = ((IntDataArray)cData).getRawArray().getData();
                int[] outiData = new int[vlen * nIsoNodes];
                for (int i = 0; i < nIsoNodes; i++) {
                    int e0 = newNodeInds[2 * i];
                    int e1 = newNodeInds[2 * i + 1];
                    float r = newNodeRatios[i];
                    for (int j = 0; j < vlen; j++)
                        outiData[vlen * i + j] = (int) (r * iData[vlen * e1 + j] + (1 - r) * iData[vlen * e0 + j]);
                }
                DataArray dai = DataArray.create(outiData, vlen, cData.getName()).unit(cData.getUnit()).userData(cData.getUserData());
                dai.setPreferredRanges(cData.getPreferredMinValue(), cData.getPreferredMaxValue(), cData.getPreferredPhysMinValue(), cData.getPreferredPhysMaxValue());
                out.addComponent(dai);
                break;
            case FIELD_DATA_FLOAT:
                float[] fData = ((FloatDataArray)cData).getRawArray().getData();
                float[] outfData = new float[vlen * nIsoNodes];
                for (int i = 0; i < nIsoNodes; i++) {
                    int e0 = newNodeInds[2 * i];
                    int e1 = newNodeInds[2 * i + 1];
                    float r = newNodeRatios[i];
                    for (int j = 0; j < vlen; j++)
                        outfData[vlen * i + j] = r * fData[vlen * e1 + j] + (1 - r) * fData[vlen * e0 + j];
                }
                DataArray daf = DataArray.create(outfData, vlen, cData.getName()).unit(cData.getUnit()).userData(cData.getUserData());
                daf.setPreferredRanges(cData.getPreferredMinValue(), cData.getPreferredMaxValue(), cData.getPreferredPhysMinValue(), cData.getPreferredPhysMaxValue());
                out.addComponent(daf);
                break;
            case FIELD_DATA_DOUBLE:
                double[] dData = ((DoubleDataArray)cData).getRawArray().getData();
                double[] outdData = new double[vlen * nIsoNodes];
                for (int i = 0; i < nIsoNodes; i++) {
                    int e0 = newNodeInds[2 * i];
                    int e1 = newNodeInds[2 * i + 1];
                    float r = newNodeRatios[i];
                    for (int j = 0; j < vlen; j++)
                        outdData[vlen * i + j] = r * dData[vlen * e1 + j] + (1 - r) * dData[vlen * e0 + j];
                }
                DataArray dad = DataArray.create(outdData, vlen, cData.getName()).unit(cData.getUnit()).userData(cData.getUserData());
                dad.setPreferredRanges(cData.getPreferredMinValue(), cData.getPreferredMaxValue(), cData.getPreferredPhysMinValue(), cData.getPreferredPhysMaxValue());
                out.addComponent(dad);
                break;
        }
    }

    private void createAndAddTriangle(int v0, int v1, int v2,
                                      int n0, int n1, FloatLargeArray outCoords, FloatLargeArray coords,
                                      int[] nodes, byte[] orientations, int[] l)
    {
        if (v0 < 0 || v1 < 0 || v2 < 0)
            return;
        int i;
        // sorting vertex numbers
        if (v0 > v1) {
            i = v1;
            v1 = v0;
            v0 = i;
        }
        if (v1 > v2) {
            i = v1;
            v1 = v2;
            v2 = i;
        }
        if (v0 > v1) {
            i = v1;
            v1 = v0;
            v0 = i;
        }
        //first edge vector
        float[] a = new float[]{outCoords.getFloat(3 * v1) - outCoords.getFloat(3 * v0),
                                outCoords.getFloat(3 * v1 + 1) - outCoords.getFloat(3 * v0 + 1),
                                outCoords.getFloat(3 * v1 + 2) - outCoords.getFloat(3 * v0 + 2)};
        //second edge vector
        float[] b = new float[]{outCoords.getFloat(3 * v2) - outCoords.getFloat(3 * v0),
                                outCoords.getFloat(3 * v2 + 1) - outCoords.getFloat(3 * v0 + 1),
                                outCoords.getFloat(3 * v2 + 2) - outCoords.getFloat(3 * v0 + 2)};
        //increasing isosurface component direction vector 
        float[] c = new float[]{coords.getFloat(3 * n1) - coords.getFloat(3 * n0),
                                coords.getFloat(3 * n1 + 1) - coords.getFloat(3 * n0 + 1),
                                coords.getFloat(3 * n1 + 2) - coords.getFloat(3 * n0 + 2)};

        float det = a[0] * b[1] * c[2] + a[1] * b[2] * c[0] + a[2] * b[0] * c[1] -
            (a[0] * b[2] * c[1] + a[1] * b[0] * c[2] + a[2] * b[1] * c[0]);
        int k = l[0];
        nodes[3 * k] = v0;
        nodes[3 * k + 1] = v1;
        nodes[3 * k + 2] = v2;
        l[0] += 1;
        orientations[k] = det > 0 ? 1 : (byte)0;
    }

    /**
     * Makes isosurface.
     * 
     * @param p Isosurface core parameters
     * @param threshold Threshold
     * 
     * @return Computed isosurface as IrregularField
     */
    @Override
    public IrregularField makeIsosurface(IsosurfaceEngineParams p, float threshold)
    {
        fireStatusChanged(0);
        if (inField.getComponent(p.getIsoComponent()).getType() == DataArrayType.FIELD_DATA_BYTE)
            threshold = (int) threshold + .5f;
        boolean newData = false;
        if (params == null || params.getIsoComponent() != componentNumber) {
            params = p;
            componentNumber = params.getIsoComponent();
            newData = true;
        }

        if (componentNumber > inField.getNComponents() || componentNumber < 0 ||
            !inField.getComponent(componentNumber).isNumeric() ||
            threshold < inField.getComponent(componentNumber).getPreferredMinValue() ||
            threshold > inField.getComponent(componentNumber).getPreferredMaxValue())
            return null;
        data = inField.getComponent(componentNumber);
        if (data.getVectorLength() == 1)
            isoData = data.getRawFloatArray().getData();
        else
            isoData = data.getVectorNorms().getData();
        dataMax = (float)data.getPreferredMaxValue();
        dataMin = (float)data.getPreferredMinValue();
        if (newData || inField.getComponent(componentNumber).changedSince(lastRecompute))
            lastRecompute = new CellCache(inField.getComponent(componentNumber), inField.getCellSet(0), bucketsCeilingFun, buckets, statusListener).computeCache();

        EdgesCut edgesCut = new EdgesCut(isoData, threshold, inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData());
        int nTriangles = 0;

        //search through buckets
        float bucketFactor = CellCache.BUCKETS_NUMBER / (dataMax - dataMin);

        FloatLargeArray coords = inField.getCurrentCoords();

        for (int bucketInd = min((int) ((threshold - dataMin) * bucketFactor), CellCache.BUCKETS_NUMBER - 1); bucketInd >= 0; --bucketInd) {
            if (bucketsCeilingFun[bucketInd] < threshold)
                break; //buckets with smaller indexes will certainly contain cells below threshold

            //search for cells in current bucket
            int[] nodes = buckets[bucketInd];
            for (int j = 0; j < nodes.length; j += 4) {
                int nOverT = 0;
                for (int i = 0; i < 4; i++)
                    if (isoData[nodes[j + i]] >= threshold)
                        nOverT += 1;
                if (nOverT == 0 || nOverT == 4)
                    continue; // all tetra vertices above or below threshold
                if (nOverT == 2)
                    nTriangles += 2;
                else
                    nTriangles += 1;
                for (int i = j; i < j + 3; i++)
                    for (int ii = i; ii < j + 4; ii++)
                        edgesCut.insertEdge(nodes[i], nodes[ii]);
            }
        }

        int nIsoNodes = edgesCut.getnEdges();
        if (nIsoNodes < 3)
            return null;

        int[] newNodeInds = new int[2 * nIsoNodes];
        float[] newNodeRatios = new float[nIsoNodes];
        for (Long l : edgesCut.getKeys()) {
            EdgesCut.EdgeDesc d = edgesCut.getData(l);
            newNodeInds[2 * d.index] = (int) (l & 0xffffffff);
            newNodeInds[2 * d.index + 1] = (int) ((l >> 32) & 0xffffffff);
            newNodeRatios[d.index] = d.ratio;
        }

        out = new IrregularField(nIsoNodes);

        FloatLargeArray outCoords = new FloatLargeArray(3 * (long)nIsoNodes, false);
        for (int i = 0; i < nIsoNodes; i++) {
            long e0 = newNodeInds[2 * i];
            long e1 = newNodeInds[2 * i + 1];
            float r = newNodeRatios[i];
            for (int j = 0; j < 3; j++)
                outCoords.setFloat(3 * (long)i + j, r * coords.getFloat(3 * e1 + j) + (1 - r) * coords.getFloat(3 * e0 + j));
        }
        out.setCoords(outCoords, 0);
        if (data.getVectorLength() == 1) {
            float[] thrData = new float[nIsoNodes];
            for (int i = 0; i < thrData.length; i++)
                thrData[i] = threshold;
            try {
                DataArray da = DataArray.create(thrData, 1, data.getName() + "(thr)").unit(data.getUnit()).userData(data.getUserData());
                da.setPreferredRanges(data.getPreferredMinValue(), data.getPreferredMaxValue(), data.getPreferredPhysMinValue(), data.getPreferredPhysMaxValue());
                out.addComponent(da);
            } catch (Exception e) {
                System.out.println("could not create threshold data array");
            }
        } else
            try {
                interpolateDataToSurface(componentNumber, nIsoNodes, newNodeInds, newNodeRatios);
            } catch (Exception e) {
                System.out.println("could not create threshold data array");
            }
        for (int nData = 0; nData < inField.getNComponents(); nData++)
            if (nData != componentNumber && inField.getComponent(nData).isNumeric())
                try {
                    interpolateDataToSurface(nData, nIsoNodes, newNodeInds, newNodeRatios);
                } catch (Exception e) {
                    System.out.println("could not interpolate " + inField.getComponent(nData).getName());
                }

        newNodeRatios = null; // no more used - freed 

        int[] nodes = new int[3 * nTriangles];
        byte[] orientations = new byte[nTriangles];

        int[] currentTri = new int[]{0}; // holds number of triangles created - incremented in createAndAddTriangle method

        for (int bucketInd = min((int) ((threshold - dataMin) * bucketFactor), CellCache.BUCKETS_NUMBER - 1); bucketInd >= 0; --bucketInd) {
            if (bucketsCeilingFun[bucketInd] < threshold)
                break;
            //search for cells in current bucket
            int[] bNodes = buckets[bucketInd];
            int e0, e1;
            for (int j = 0; j < bNodes.length; j += 4) {
                int code = 0;
                for (int i = 0; i < 4; i++) {
                    float f = isoData[bNodes[j + i]];
                    if (f == threshold)
                        f += .000001f;
                    if (f > threshold)
                        code |= 1 << i;
                }
                if (cellInd[code].length == 0)
                    continue;
                int[] edges = cellInd[code];
                int[] pts = new int[cellInd[code].length];
                for (int i = 0; i < edges.length; i++)
                    pts[i] = edgesCut.getIndex(bNodes[j + cellEdg[edges[i]][0]],
                                               bNodes[j + cellEdg[edges[i]][1]]);

                if (pts.length == 3 && pts[0] >= 0 && pts[1] >= 0 && pts[2] >= 0) // single triangle
                {
                    e0 = newNodeInds[2 * pts[0]];
                    e1 = newNodeInds[2 * pts[0] + 1];
                    if (isoData[e1] < isoData[e0]) {
                        int k = e0;
                        e0 = e1;
                        e1 = k;
                    }
                    createAndAddTriangle(pts[0], pts[1], pts[2], e0, e1,
                                         outCoords, coords,
                                         nodes, orientations, currentTri);
                } else if (pts.length == 4 && pts[0] >= 0 && pts[1] >= 0 && pts[2] >= 0 && pts[3] >= 0) // quadrangle - cut along shorter diagonal
                {
                    float d0 = 0, d1 = 0;
                    for (int i = 0; i < 3; i++) {
                        float r = outCoords.getFloat(3 * (long)pts[0] + i) - outCoords.getFloat(3 * (long)pts[2] + i);
                        d0 += r * r;
                        r = outCoords.getFloat(3 * (long)pts[1] + i) - outCoords.getFloat(3 * (long)pts[3] + i);
                        d1 += r * r;
                    }
                    if (d0 > d1) {
                        e0 = newNodeInds[2 * pts[1]];
                        e1 = newNodeInds[2 * pts[1] + 1];
                        if (isoData[e1] < isoData[e0]) {
                            int k = e0;
                            e0 = e1;
                            e1 = k;
                        }
                        createAndAddTriangle(pts[0], pts[1], pts[3], e0, e1,
                                             outCoords, coords,
                                             nodes, orientations, currentTri);
                        createAndAddTriangle(pts[1], pts[2], pts[3], e0, e1,
                                             outCoords, coords,
                                             nodes, orientations, currentTri);
                    } else {
                        e0 = newNodeInds[2 * pts[0]];
                        e1 = newNodeInds[2 * pts[0] + 1];
                        if (isoData[e1] < isoData[e0]) {
                            int k = e0;
                            e0 = e1;
                            e1 = k;
                        }
                        createAndAddTriangle(pts[0], pts[1], pts[2], e0, e1,
                                             outCoords, coords,
                                             nodes, orientations, currentTri);
                        createAndAddTriangle(pts[0], pts[2], pts[3], e0, e1,
                                             outCoords, coords,
                                             nodes, orientations, currentTri);
                    }
                }
            }
        }
        for (int i = 3 * currentTri[0]; i < nodes.length; i++)
            nodes[i] = 0;

        newNodeInds = null; // no more used - freed    

        CellSet cs = new CellSet(CELLSET_NAME);
        CellArray triArray = new CellArray(CellType.TRIANGLE, nodes, orientations, null);
        cs.addCells(triArray);
        cs.generateDisplayData(outCoords);
        out.addCellSet(cs);

        return out;
    }

}
