/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class StringUtils
{

    public static boolean isEqualStringArrays(String[] arr1, String[] arr2)
    {
        if (arr1 == null || arr2 == null)
            return false;

        if (arr1.length != arr2.length)
            return false;

        for (int i = 0; i < arr2.length; i++) {
            if(arr1[i] == null || arr2[i] == null)
                return false;
            
            if (!arr1[i].equals(arr2[i]))
                return false;
        }

        return true;
    }

    /**
     * Sorts filenames using numerical sort.
     * Example:
     * input: {f1.txt, f10.txt, f2.txt, f9.txt}
     * output: {f1.txt, f2.txt, f9.txt, f10.txt}
     * <p>
     * @param files list of files
     * <p>
     * @return numerically sorted list of filenames
     */
    public static String[] sortFilenameList(File[] files)
    {
        String[] paths = new String[files.length];
        for (int i = 0; i < paths.length; i++) {
            paths[i] = files[i].getAbsolutePath();
        }
        sortFilenameList(paths);
        return paths;
    }

    /**
     * Sorts filenames using numerical sort.
     * Example:
     * input: {f1.txt, f10.txt, f2.txt, f9.txt}
     * output: {f1.txt, f2.txt, f9.txt, f10.txt}
     * <p>
     * @param filenames list of filenames
     */
    public static void sortFilenameList(String[] filenames)
    {
        Comparator<String> comparator = new Comparator<String>()
        {
            @Override
            public int compare(String o1, String o2)
            {
                return getFileId(o1).compareTo(getFileId(o2));
            }

            private Long getFileId(String filePath)
            {
                String[] token = filePath.split("\\.");
                if (token.length == 0) {
                    return -1l;
                }
                String fileName = token[0];
                StringBuilder fileId = new StringBuilder();

                for (int i = fileName.length() - 1; i >= 0; i--) {
                    if (Character.isDigit(fileName.charAt(i))) {
                        fileId.append(fileName.charAt(i));
                    }
                }
                if (fileId.length() == 0) {
                    return -1l;
                }
                long res;
                try {
                    res = Long.parseLong(fileId.reverse().toString());
                } catch (NumberFormatException ex) {
                    res = -1;
                }
                return res;
            }
        };

        Arrays.sort(filenames, comparator);
    }

    public static String join(String[] strings, String separator)
    {
        StringBuffer joined = new StringBuffer();
        for (int i = 0; i < strings.length; i++) {
            if (i != 0) joined.append(separator);
            joined.append(strings[i]);
        }
        return joined.toString();
    }

    /**
     * Invokes toString on every element of input array and returns String array.
     */
    public static String[] toString(Object[] array)
    {
        String[] toStrings = new String[array.length];
        for (int i = 0; i < array.length; i++) toStrings[i] = array[i].toString();
        return toStrings;
    }

    /**
     * @return first element with toString().equals(toStringValue) == true
     *         null if element is not found.
     * <p>
     * @throws NullPointerException if any null array element is found before method exits.
     */
    public static <T> T findByToString(T[] array, String toStringValue)
    {
        for (T element : array) if (element.toString().equals(toStringValue)) return element;

        return null;
    }
    
    /**
     * Replaces all chars special to .vnf syntax by underscores
     * @param s modified string
     * @return replacement result
     */
    public static String replaceSpecCharsBy_(String s)
    {
        return s.replaceAll("\\s", "_").replaceAll(",", "_").replaceAll("\\.", "_").replaceAll("=", "_").replaceAll(":", "_");
    }
    
    public static final String[] findBlock(String[] src, int start)
    {
        if (src.length - start <2)
            return null;
        int depth = 0;
        for (int i = start + 1, j = 0; i < src.length; i++, j++) {
            String s = src[i];
            if (s.contains("{"))
                depth += 1;
            else if (s.contains("}")) {
                depth -= 1;
                if (depth < 0) {
                    if (j == 0)
                        return null;
                    String[] block = new String[j];
                    System.arraycopy(src, start + 1, block, 0, j);
                    return block;
                }
            }
        }
        return null;
    }

    private StringUtils()
    {
    }
}
