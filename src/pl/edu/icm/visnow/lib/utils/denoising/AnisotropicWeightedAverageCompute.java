//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.denoising;

import org.apache.log4j.Logger;

import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.visnow.system.main.VisNow;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

import pl.edu.icm.visnow.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiser;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class AnisotropicWeightedAverageCompute extends AbstractAnisotropicWeightedAverageCompute
{
    private static final Logger LOGGER = Logger.getLogger(AnisotropicDenoiser.class);

    private int[] dims;
    private int radius;
    private float slope, slope1;
    private float[] anisotropy;

    private int anisotropyVlen;

    private ProgressAgent progressAgent;

    @Override
    public synchronized RegularField compute(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params)
    {
        return compute(inField, anisotropyField, params, ProgressAgent.getDummyAgent());
    }

    @Override
    public synchronized RegularField compute(RegularField inField, RegularField anisotropyField, AnisotropicDenoisingParams params, ProgressAgent progressAgent)
    {
        if (inField == null)
            return null;

        this.progressAgent = progressAgent;

        radius = params.getRadius();
        slope = params.getSlope();
        slope *= slope;
        slope1 = params.getSlope1();
        slope1 *= slope1;
        dims = inField.getDims();
        long outNdata = inField.getNNodes();
        int vlen;
        int nThreads = VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        RegularField outField = new RegularField(dims);
        outField.setAffine(inField.getAffine());
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        for (int nComponent = 0; nComponent < params.getComponentsNumber(); nComponent++) {
            int component = params.getComponent(nComponent);
            int anisotropyComponent = params.getAnisotropyComponent(nComponent);
            DataArray dataArr = inField.getComponent(component);
            LOGGER.info("averaging component " + component + "(" + dataArr.getName() + ")");
            if (anisotropyComponent < 0 || anisotropyField == null || anisotropyField.getComponent(anisotropyComponent) == null) {
                anisotropy = null;
            } else {
                DataArray anisotropyArr = anisotropyField.getComponent(anisotropyComponent);
                anisotropyVlen = anisotropyArr.getVectorLength();
                if (anisotropyVlen != 1 && anisotropyVlen != dims.length)
                    return null;
                if (anisotropyArr.getType() == DataArrayType.FIELD_DATA_FLOAT)
                    anisotropy = (float[]) anisotropyArr.getRawArray().getData();
                else
                    anisotropy = anisotropyArr.getRawFloatArray().getData(); //copy of data
                LOGGER.info(" with anisotropy component " + anisotropyComponent + "(" + anisotropyArr.getName() + ")");
            }
            vlen = dataArr.getVectorLength();

            DataArray typedOutput = null;
            LargeArray inData = dataArr.getRawArray();
            LargeArray outData = LargeArrayUtils.create(dataArr.getType().toLargeArrayType(), (long) outNdata * vlen);
            for (int i = 0; i < workThreads.length; i++) {
                workThreads[i] = new Thread(new FilterArray(vlen, nThreads, i, inData, outData));
                workThreads[i].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
            typedOutput = DataArray.create(outData, vlen, dataArr.getName());
            typedOutput.setPreferredRanges(dataArr.getPreferredMinValue(),
                                           dataArr.getPreferredMaxValue(),
                                           dataArr.getPreferredPhysMinValue(),
                                           dataArr.getPreferredPhysMaxValue());
            outField.addComponent(typedOutput);
        }

        if (outField.getNComponents() > 0)
            return outField;
        else
            return null;
    }

    class FilterArray implements Runnable
    {

        LargeArrayType type;
        int vlen;
        int nThreads;
        int iThread;
        LargeArray inData, outData;


        public FilterArray(int vlen, int nThreads, int iThread,
                           LargeArray inData, LargeArray outData)
        {
            type = inData.getType();
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.vlen = vlen;
            this.inData = inData;
            this.outData = outData;
        }

        @Override
        public void run()
        {
            int rr, kl, ku, jl, ju, il, iu;
            double r, w, sw;
            int dk, kstart, kend;
            for (int v = 0; v < vlen; v++)
                switch (dims.length) {
                    case 3:
                        dk = dims[2] / nThreads;
                        kstart = iThread * dk + min(iThread, dims[2] % nThreads);
                        kend = (iThread + 1) * dk + min(iThread + 1, dims[2] % nThreads);
                        for (int k0 = kstart; k0 < kend; k0++) {
                            progressAgent.increase();
                            int m = k0 * dims[0] * dims[1] * vlen + v;
                            kl = -radius;
                            if (k0 < radius)
                                kl = -k0;
                            ku = radius;
                            if (k0 >= dims[2] - radius)
                                ku = dims[2] - k0 - 1;
                            for (int j0 = 0; j0 < dims[1]; j0++) {
                                jl = -radius;
                                if (j0 < radius)
                                    jl = -j0;
                                ju = radius;
                                if (j0 >= dims[1] - radius)
                                    ju = dims[1] - j0 - 1;
                                for (int i0 = 0, n0 = (dims[1] * k0 + j0) * dims[0]; i0 < dims[0]; i0++, m += vlen, n0++) {
                                    il = -radius;
                                    if (i0 < radius)
                                        il = -i0;
                                    iu = radius;
                                    if (i0 >= dims[0] - radius)
                                        iu = dims[0] - i0 - 1;
                                    r = sw = 0.f;
                                    for (int k = kl; k <= ku; k++)
                                        for (int j = jl; j <= ju; j++)
                                            for (int i = il,
                                                    n = ((k0 + k) * dims[1] + j + j0) * dims[0] + i0 + il,
                                                    p = k * k + j * j;
                                                    i <= iu;
                                                    i++, n++) {
                                                w = 0;
                                                if (anisotropy != null) {
                                                    if (anisotropyVlen > 1)
                                                        w = k * anisotropy[n0 * anisotropyVlen + 2] +
                                                                j * anisotropy[n0 * anisotropyVlen + 1] +
                                                                i * anisotropy[n0 * anisotropyVlen];
                                                    else
                                                        w = anisotropy[n0] - anisotropy[n];
                                                }
                                                w = (float) (exp(-w * w / slope1 - (p + i * i) / slope));
                                                r += inData.getFloat(n * vlen + v) * w;
                                                sw += w;
                                            }
                                    outData.setFloat(m, (float) (r / sw));
                                }
                            }
                        }
                        break;
                    case 2:
                        dk = dims[1] / nThreads;
                        kstart = iThread * dk + min(iThread, dims[1] % nThreads);
                        kend = (iThread + 1) * dk + min(iThread + 1, dims[1] % nThreads);
                        for (int j0 = kstart; j0 < kend; j0++) {
                            progressAgent.increase();
                            jl = -radius;
                            if (j0 < radius)
                                jl = -j0;
                            ju = radius;
                            if (j0 >= dims[1] - radius)
                                ju = dims[1] - j0 - 1;
                            for (int i0 = 0, n0 = j0 * dims[0], m = j0 * dims[0] * vlen + v; i0 < dims[0]; i0++, m += vlen, n0++) {
                                il = -radius;
                                if (i0 < radius)
                                    il = -i0;
                                iu = radius;
                                if (i0 >= dims[0] - radius)
                                    iu = dims[0] - i0 - 1;
                                // r = sw = s = 0.f;
                                r = sw = 0.f;
                                for (int j = jl; j <= ju; j++)
                                    for (int i = il,
                                            n = (j + j0) * dims[0] + i0 + il,
                                            p = j * j;
                                            i <= iu;
                                            i++, n++) {
                                        w = 0;
                                        if (anisotropy != null) {
                                            if (anisotropyVlen > 1)
                                                w = j * anisotropy[n0 * anisotropyVlen + 1] +
                                                        i * anisotropy[n0 * anisotropyVlen];
                                            else
                                                w = anisotropy[n0] - anisotropy[n];
                                        }
                                        w = (float) (exp(-w * w / slope1 - (p + i * i) / slope));
                                        // s += inFData[n * vlen + v] * inFData[n * vlen + v] * w;
                                        r += inData.getFloat(n * vlen + v) * w;
                                        sw += w;
                                    }
                                outData.setFloat(m, (float) (r / sw));//                                
                            }
                        }
                        break;

                    case 1:
                        dk = dims[0] / nThreads;
                        kstart = iThread * dk + min(iThread, dims[0] % nThreads);
                        kend = (iThread + 1) * dk + min(iThread + 1, dims[0] % nThreads);
                        for (int i0 = kstart; i0 < kend; i0++) {
                            progressAgent.increase();
                            int m = i0;
                            il = i0 - radius;
                            if (il < 0)
                                il = 0;
                            iu = i0 + radius;
                            if (i0 >= dims[0])
                                iu = dims[0] - 1;
                            // r = sw = s = 0.f;
                            r = sw = 0.f;
                            for (int i = il; i <= iu; i++) {
                                w = 0;
                                if (anisotropy != null)
                                    w = anisotropy[i] - anisotropy[i0];
                                w = (float) (exp(w * w / slope1 - (i - i0) * (i - i0) / slope));
                                // s += inFData[i * vlen + v] * inFData[i * vlen + v] * w;
                                r += inData.getFloat(i * vlen + v) * w;
                                sw += w;
                            }
                            outData.setFloat(m, (float) (r / sw));
                        }
                        break;
                }
        }

    }
}
