//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.mappers.Isolines;

import java.util.Arrays;
import java.util.ArrayList;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularFieldIsolines
{

    private IsolinesParams params;
    private RegularField inField = null;
    private int inComponent;
    private int[] dims;
    private float[] fData;
    private float[] thresholds;
    private float threshold;
    private int npoint, midpoint;
    private ArrayList<float[]>[] lines = null;
    private float[] t_coords;
    private boolean[] done;
    private boolean isMask = false;
    private LogicLargeArray mask = null;
    private int width;
    private int height;
    private IrregularField outField = null;
    private int nNodes = 0;
    private int nPolys = 0;
    private float[] coords = null;
    private DataArray inData = null;
    private DataArray[] outData = null;

    @SuppressWarnings("unchecked")
    public RegularFieldIsolines(RegularField inField, IsolinesParams params)
    {
        inComponent = params.getComponent();
        thresholds = params.getThresholds();
        if (inField == null || inField.getDims() == null || inField.getDims().length != 2 ||
            thresholds == null || thresholds.length < 1)
            return;
        this.inField = inField;
        this.params = params;
        isMask = inField.hasMask();
        if (isMask)
            mask = inField.getCurrentMask();
        dims = inField.getDims();
        width = dims[0];
        height = dims[1];
        inData = inField.getComponent(inComponent);
        if (inData.getVectorLength() == 1)
            fData = inField.getComponent(inComponent).getRawFloatArray().getData();
        else
            fData = inField.getComponent(inComponent).getVectorNorms().getData();
        float u, v0, v1, v2;
        int endln;
        int i, ii, j;
        i = ii = j = 0;
        done = new boolean[height * width * 2];
        lines = (ArrayList<float[]>[]) new ArrayList[thresholds.length];
        for (int iv = 0; iv < thresholds.length; iv++) {
            threshold = thresholds[iv];
            lines[iv] = new ArrayList<>();
            for (int iy = 0; iy < done.length; iy++)
                done[iy] = false;

            t_coords = new float[2 * width * height];

            for (int iy = 0; iy < height - 1; iy++) {
                for (int ix = 0, l = width * iy; ix < width - 1; ix++, l++) {
                    if (done[2 * l])
                        continue;
                    done[2 * l] = true;
                    v0 = fData[iy * width + ix] - threshold;
                    v1 = fData[iy * width + ix + 1] - threshold;
                    v2 = fData[(iy + 1) * width + ix] - threshold;
                    if (v0 == 0.)
                        v0 = 1.e-10f;
                    if (v1 == 0.)
                        v1 = 1.e-10f;
                    if (v2 == 0.)
                        v2 = 1.e-10f;
                    npoint = midpoint = 0;

                    if (v0 * v1 < 0) {
                        u = 1.f / (v1 - v0);
                        t_coords[2 * npoint] = ix - u * v0;
                        t_coords[2 * npoint + 1] = iy;
                        npoint += 1;
                        Triangle tstart = new Triangle(ix, iy, 0, 2);
                        if (tstart.isValid()) {
                            endln = 0;
                            while (endln == 0)
                                endln = tstart.traverse();
                            midpoint = npoint;
                            if (endln == 2 && iy > 0) {
                                tstart = new Triangle(ix, iy - 1, 1, 2);
                                while (tstart.traverse() == 0)
                                    continue;
                            }
                        }
                    } else if (v0 * v2 < 0) {
                        u = 1.f / (v2 - v0);
                        t_coords[2 * npoint] = ix;
                        t_coords[2 * npoint + 1] = iy - u * v0;
                        npoint += 1;
                        Triangle tstart = new Triangle(ix, iy, 0, 1);
                        if (tstart.isValid()) {
                            endln = 0;
                            while (endln == 0)
                                endln = tstart.traverse();
                            midpoint = npoint;
                            if (endln == 2 && ix > 0) {
                                tstart = new Triangle(ix - 1, iy, 1, 1);
                                while (tstart.traverse() == 0)
                                    continue;
                            }
                        }
                    }
                    if (npoint > 2)
                        try {
                            float[] iCoords = new float[2 * npoint];
                            for (ii = midpoint - 1, i = 0; ii >= 0; ii--, i++)
                                for (j = 0; j < 2; j++)
                                    iCoords[2 * i + j] = t_coords[2 * ii + j];
                            for (ii = midpoint; ii < npoint; ii++, i++)
                                for (j = 0; j < 2; j++)
                                    iCoords[2 * i + j] = t_coords[2 * ii + j];
                            lines[iv].add(iCoords);
                        } catch (Exception e) {
                            System.out.println("i=" + i + " ii=" + ii + " j=" + j);
                        }
                }
            }
        }
        //now remove all unnecessary links:
        fData = null;
        done = null;
        t_coords = null;
        createOutField();
    }

    public ArrayList<float[]>[] getLines()
    {
        return (lines);
    }

    // TRIANGLE *****************************************
    private class Triangle
    {

        private int ix, iy;
        private int is_up;
        private float[][] coords;
        private float[] vals;
        private int edge_in;
        private boolean valid = true;

        public Triangle(int inx, int iny, int inIs_up, int inEdge_in)
        {
            if (inx < 0 || inx >= width || iny < 0 || iny >= height) {
                ix = iy = -999;
                throw new IllegalArgumentException("Triangle out of range.");
            }
            ix = inx;
            iy = iny;
            is_up = inIs_up;
            coords = new float[3][2];
            vals = new float[3];
            loadValues();
            edge_in = inEdge_in;
        }

        private void loadValues()
        {
            if (is_up == 1) {
                coords[0][0] = ix + 1;
                coords[0][1] = iy + 1;
                vals[0] = fData[(iy + 1) * width + ix + 1] - threshold;
                coords[1][0] = ix;
                coords[1][1] = iy + 1;
                vals[1] = fData[(iy + 1) * width + ix] - threshold;
                coords[2][0] = ix + 1;
                coords[2][1] = iy;
                vals[2] = fData[iy * width + ix + 1] - threshold;
                if (isMask)
                    valid = mask.getBoolean((iy + 1) * width + ix + 1) &&
                        mask.getBoolean((iy + 1) * width + ix) &&
                        mask.getBoolean(iy * width + ix + 1);
            } else {
                coords[0][0] = ix;
                coords[0][1] = iy;
                vals[0] = fData[iy * width + ix] - threshold;
                coords[1][0] = ix + 1;
                coords[1][1] = iy;
                vals[1] = fData[iy * width + ix + 1] - threshold;
                coords[2][0] = ix;
                coords[2][1] = iy + 1;
                vals[2] = fData[(iy + 1) * width + ix] - threshold;
                if (isMask)
                    valid = mask.getBoolean(iy * width + ix) &&
                        mask.getBoolean(iy * width + ix + 1) &&
                        mask.getBoolean((iy + 1) * width + ix);
            }
            for (int i = 0; i < 3; i++)
                if (vals[i] == 0.)
                    vals[i] = 1.e-10f;
        }

        public int traverse()
        {
            int edge_out = -1;
            int is_up_out = 1 - is_up;
            int dirx, diry;
            done[2 * (iy * width + ix) + is_up] = true;
            float u;
            dirx = diry = 0;
            switch (edge_in) {
                case 0:
                    if (vals[0] * vals[1] >= 0.)
                        edge_out = 1;
                    else
                        edge_out = 2;
                    break;
                case 1:
                    if (vals[1] * vals[2] >= 0.)
                        edge_out = 2;
                    else
                        edge_out = 0;
                    break;
                case 2:
                    if (vals[2] * vals[0] >= 0.)
                        edge_out = 0;
                    else
                        edge_out = 1;
                    break;
            }
            switch (edge_out) {
                case 0:
                    break;
                case 1:
                    u = 1.f / (vals[2] - vals[0]);
                    for (int i = 0; i < 2; i++)
                        t_coords[2 * npoint + i] = u * (vals[2] * coords[0][i] - vals[0] * coords[2][i]);
                    npoint++;
                    if (is_up == 1)
                        dirx = 1;
                    else
                        dirx = -1;
                    if (ix + dirx < 0 || ix + dirx >= width - 1)
                        return 2;
                    if (done[2 * (iy * width + ix + dirx) + is_up_out])
                        return (1);
                    break;
                case 2:
                    u = 1.f / (vals[0] - vals[1]);
                    for (int i = 0; i < 2; i++)
                        t_coords[2 * npoint + i] = u * (vals[0] * coords[1][i] - vals[1] * coords[0][i]);
                    npoint++;
                    if (is_up == 1)
                        diry = 1;
                    else
                        diry = -1;
                    if (iy + diry < 0 || iy + diry >= height - 1)
                        return 2;
                    if (done[2 * ((iy + diry) * width + ix) + is_up_out])
                        return (1);
                    break;
            }
            ix += dirx;
            iy += diry;
            is_up = is_up_out;
            edge_in = edge_out;
            loadValues();
            if (!valid)
                return 2;
            return 0;
        }

        public boolean isValid()
        {
            return valid;
        }
    }

    private void createOutField()
    {
        boolean[] mappedComponents = params.getMappedCompoents();
        for (ArrayList<float[]> line1 : lines) {
            nPolys += line1.size();
            for (float[] line : line1)
                nNodes += line.length / 2;
        }
        if (nNodes < 2 || nPolys < 1)
            return;
        coords = new float[3 * nNodes];
        int[] polys = new int[nPolys];
        int nSegs = 0;
        int nOutData = 0;
        for (int i = 0; i < inField.getNComponents(); i++)
            if (mappedComponents[i] && i != inComponent)
                nOutData += 1;
        outData = new DataArray[nOutData];
        for (int i = 0, j = 0; i < inField.getNComponents(); i++)
            if (mappedComponents[i] && i != inComponent) {
                DataArray inDA = inField.getComponent(i);
                outData[j] = DataArray.create(inDA.getType(), nNodes, inDA.getVectorLength(), 
                                              inDA.getName(), inDA.getUnit(), inDA.getUserData());
                j += 1;
            }
        for (int n = 0, k = 0, l = 0; n < lines.length; n++) {
            for (float[] line : lines[n]) {
                polys[k] = line.length / 2;
                nSegs += polys[k] - 1;
                k += 1;
                for (int i = 0; i < line.length; i += 2, l++) {
                    float[] c = inField.getGridCoords(line[i], line[i + 1]);
                    System.arraycopy(c, 0, coords, 3 * l, c.length);
                    for (int j = c.length; j < 3; j++)
                        coords[3 * l + j] = 0;
                    if (nOutData > 0) {
                        for (int id = 0, iod = 0; id < inField.getNComponents(); id++) {
                            if (mappedComponents[id] && id != inComponent) {
                                DataArray da = inField.getComponent(id);
                                int vl = da.getVectorLength();
                                LargeArrayUtils.arraycopy(inField.getInterpolatedData(da.getRawArray(), line[i], line[i + 1], 0.f), 0, 
                                                                                outData[iod].produceData(0), l * vl, vl);
                                outData[iod].setPreferredRange(da.getPreferredMinValue(), da.getPreferredMaxValue());
                                iod += 1;
                            }
                        }
                    }
                }
            }
        }
        int[] edges = new int[2 * nSegs];
        for (int i = 0, k = 0, l = 0; i < polys.length; i++) {
            for (int j = 0; j < polys[i] - 1; j++) {
                edges[l] = k + j;
                edges[l + 1] = k + j + 1;
                l += 2;
            }
            k += polys[i];
        }

        outField = new IrregularField(nNodes);
        float[] outThr = new float[nNodes];
        for (int i = 0, k = 0; i < lines.length; i++) {
            int l = 0;
            for (float[] line : lines[i])
                l += line.length / 2;
            Arrays.fill(outThr, k, k + l, thresholds[i]);
            k += l;
        }
        DataArray da = inField.getComponent(inComponent);
        DataArray outDA = DataArray.create(outThr, 1, da.getName()).
                                           preferredRanges(da.getPreferredMinValue(), da.getPreferredMaxValue(), 
                                           da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
        outField.addComponent(outDA);
        for (DataArray outData1 : outData) {
            outData1.recomputeStatistics();
            outField.addComponent(outData1);
        }
        outField.setCoords(new FloatLargeArray(coords), 0);
        CellSet cellSet = new CellSet("Isolines");
        byte[] orientations = new byte[edges.length / 2];
        for (int i = 0; i < orientations.length; i++)
            orientations[i] = 1;
        CellArray skeletonSegments = new CellArray(CellType.SEGMENT, edges, orientations, null);
        cellSet.setBoundaryCellArray(skeletonSegments);
        cellSet.setCellArray(skeletonSegments);
        outField.addCellSet(cellSet);
    }

    public IrregularField getOutField()
    {
        return outField;
    }
}
