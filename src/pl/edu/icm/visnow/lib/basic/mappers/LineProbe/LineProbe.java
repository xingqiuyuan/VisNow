
//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.mappers.LineProbe;

import java.util.ArrayList;
import java.util.Arrays;
import javax.media.j3d.J3DGraphics2D;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.events.ColorEvent;
import pl.edu.icm.visnow.geometries.events.ColorListener;
import pl.edu.icm.visnow.geometries.events.ResizeEvent;
import pl.edu.icm.visnow.geometries.events.ResizeListener;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph;
import pl.edu.icm.visnow.geometries.objects.Geometry2D;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickListener;
import static pl.edu.icm.visnow.lib.basic.mappers.LineProbe.LineProbeShared.*;
import static pl.edu.icm.visnow.lib.basic.mappers.LineProbe.LineProbeShared.ProbeType.*;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.events.MouseRestingEvent;
import pl.edu.icm.visnow.lib.utils.events.MouseRestingListener;
import pl.edu.icm.visnow.lib.utils.field.MergeIrregularField;
import pl.edu.icm.visnow.lib.utils.lineProbe.BasicLineProbe;
import pl.edu.icm.visnow.lib.utils.graphing.GraphDisplay;
import pl.edu.icm.visnow.lib.utils.graphing.MultiGraphs;
import pl.edu.icm.visnow.lib.utils.lineProbe.IndexSlice1D;
import pl.edu.icm.visnow.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.graphing.GraphParams;
import static pl.edu.icm.visnow.lib.utils.graphing.GraphParams.*;
import pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay;
import pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay.Position;
import static pl.edu.icm.visnow.lib.utils.probeInterfaces.ProbeDisplay.Position.*;
import pl.edu.icm.visnow.lib.utils.probeInterfaces.Probe;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class LineProbe extends OutFieldVisualizationModule
{

    
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected Field inField = null;
    
    protected IrregularField probeField = null;
    protected RegularField regularProbeField = null;
    
    protected GUI computeUI = null;
    
    protected boolean newGraphedFieldSet = false;

    protected Probe currentProbe = null;
    protected BasicLineProbe geometricProbe;
    protected InteractiveGlyph geometricProbeGlyph;
    
    protected IndexSlice1D indexProbe;
    protected OpenBranchGroup indexProbeGlyph;
    protected IndexSliceParams indexProbeParams;
    
    protected OpenBranchGroup currentProbeGlyph;
    
    protected MultiGraphs graphs = new MultiGraphs();
    protected GraphParams graphParams = graphs.getGraphParams();
    protected GraphDisplay pickedGraph = null;
    protected int windowWidth  = 500;
    protected int windowHeight = 500;
    protected boolean fromParams = false;
    protected boolean probeReady = false;
    protected int maxProbesOnMargin = 6;
    
    
    Geometry2D graphsGeometry = new Geometry2D()
        {
            @Override
            public void draw2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int w, int h)
            {
                
                graphs.draw2D(vGraphics, ltw, w, h);
            }
        };
    
    protected MouseRestingListener toolTipListener = new MouseRestingListener() {
        @Override
        public void mouseResting(MouseRestingEvent e)
        {
            graphParams.setTooltipPosition(new int[]{e.getX(), e.getY()});
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
        }
    };
    
    protected ResizeListener resizeListener = new ResizeListener() {
        @Override
        public void resized(ResizeEvent e)
        {
            updateAddCapability();
        }
    };

    @Override
    public MouseRestingListener getMouseRestingListener()
    {
        return toolTipListener;
    }
    
    PickListener graphPickingListener = new PickListener()
    {
        @Override
        public void pickChanged(PickEvent e)
        {
            pickedGraph = graphs.pickedDisplay(e.getEvt().getX(), e.getEvt().getY());
            if (pickedGraph != null) {
                for (ProbeDisplay display : graphs.getDisplays()) 
                    display.setSelected(false);
                pickedGraph.setSelected(true);
                computeUI.enableRemoveGraph(true);
                outObj.getRenderingWindow().refresh();
            }
        }
    };
    
    private boolean canAddProbe = parameters.get(GRAPHS_POSITION) == AT_POINT ||
                                  graphs.getDisplays().size() < maxProbesOnMargin;
    
    ChangeListener probeListener = new ChangeListener()
    {
        @Override
        public void stateChanged(ChangeEvent e)
        {
            probeField = currentProbe.getSliceField();
            if (currentProbe instanceof IndexSlice1D)
                regularProbeField = ((IndexSlice1D)currentProbe).getRegularSliceField();
            else
                regularProbeField = null;
            probeReady = true;
            startAction();
            updateAddCapability();
        }
    };
    
    private final void updateAddCapability()
    {
        Position probesPosition = parameters.get(GRAPHS_POSITION);
        int nGraphs = graphs.getDisplays().size();
        int[] maxGraphs = graphs.maxGraphCount();
        canAddProbe = 
            (probesPosition != TOP && probesPosition != BOTTOM || nGraphs < maxGraphs[0]) &&
            (probesPosition != LEFT && probesPosition != RIGHT || nGraphs < maxGraphs[1]);
        computeUI.enableAddGraph(probeReady && canAddProbe);
    }

    public LineProbe()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) 
            {
                graphParams.setWindowBgrColor(e.getSelectedColor());
            }
        };
        
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name == null)
                    return;
                fromParams = true;
                switch (name) {
                case ADD_SLICE_STRING:
                    probeField = currentProbe.getSliceField();
                    if (probeField == null)
                        return;
                    for (ProbeDisplay display : graphs.getDisplays()) 
                        display.setSelected(false);
                    graphs.addDisplay(probeField, currentProbe.getPlaneCenter());
                    pickedGraph = (GraphDisplay)graphs.getDisplays().get(graphs.getDisplays().size() - 1);
                    pickedGraph.setSelected(true);
                    computeUI.enableRemoveGraph(true);
                    computeUI.enableAddGraph(probeReady && canAddProbe);
                    probeReady = false;
                    updateAddCapability();
                    startAction();
                    break;
                case DEL_SLICE_STRING:
                    graphs.removeDisplay(pickedGraph);
                    updateAddCapability();
                    pickedGraph = null;
                    startAction();
                    break;
                case CLEAR_SLICES_STRING:
                    outField = null;
                    graphs.clearDisplays();
                    updateAddCapability();
                    pickedGraph = null;
                    startAction();
                    break;   
                case GRAPH_POSITION_STRING:
                case INIT_MARGIN_STRING:
                case GAP_STRING:
                    graphs.setProbesPosition(parameters.get(GRAPHS_POSITION));
                    graphs.setGap(parameters.get(GAP));
                    computeUI.enableAddGraph(probeReady && canAddProbe);
                    graphs.setRelMargin(parameters.get(INIT_MARGIN) / (float)200);
                    if (outObj.getRenderingWindow() != null)
                        outObj.getRenderingWindow().refresh();
                    break;
                case POINTER_LINE_STRING:
                    graphs.setPointerLine(parameters.get(POINTER_LINE));
                    if (outObj.getRenderingWindow() != null)
                        outObj.getRenderingWindow().refresh();
                case PROBE_TYPE_STRING:
                    if (geometricProbe != null && parameters.get(PROBE_TYPE) == GEOMETRIC) 
                        currentProbe = geometricProbe;
                    else 
                        currentProbe = indexProbe;
                    currentProbeGlyph = currentProbe.getGlyphGeometry();
                    doOutput();
                    break;
                default:
                }
                computeUI.enableRemoveGraph(pickedGraph != null);
            }
        });
        
        graphParams.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                switch (name) {
                case GRAPH_WIDTH:
                case GRAPH_HEIGHT:
                    graphs.setProbesPosition(parameters.get(GRAPHS_POSITION));
                    break;
                }
                if (inField != null && !graphs.isPacking() &&
                    outObj.getRenderingWindow() != null)
                    outObj.getRenderingWindow().refresh();
                computeUI.enableAddGraph(probeReady && canAddProbe);
            }
        });
        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                computeUI.setGraphParams(graphParams);
                JPanel probeGraphPanel = computeUI.getProbeGraphsPanel();
                computeUI.remove(probeGraphPanel);
                ui.getPresentationGUI().hideTransformPanel();
                ui.addComputeGUI(computeUI, "Probe");
                ui.insertAdditionalGUI(probeGraphPanel, "Graphs");
                setPanel(ui);
            }
        });
        
        outObj.addPickListener(graphPickingListener);
    }   

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ADD_SLICE, false),
            new Parameter<>(DEL_SLICE, false),
            new Parameter<>(CLEAR_SLICES, false),
            new Parameter<>(SHOW_ACCUMULATED, false),
            new Parameter<>(GRAPHS_POSITION, RIGHT),
            new Parameter<>(POINTER_LINE, true),
            new Parameter<>(INIT_MARGIN, 5),
            new Parameter<>(GAP, 10),
            new Parameter<>(PROBE_TYPE, GEOMETRIC)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.set(ADD_SLICE, false);
            parameters.set(DEL_SLICE, false);
            parameters.set(CLEAR_SLICES, false);
            parameters.set(SHOW_ACCUMULATED, false);
            parameters.set(GRAPHS_POSITION, RIGHT);
            parameters.set(POINTER_LINE, true);
            parameters.set(INIT_MARGIN, 5);
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }
    
    protected void doOutput()
    {
        if (probeField == null && regularProbeField == null) 
        {
            setOutputValue("currentOutField", null);
            setOutputValue("currentRegularOutField", null);
        }
        else
        if (regularProbeField != null) {
            setOutputValue("currentRegularOutField", new VNRegularField(regularProbeField));
            setOutputValue("currentOutField", null);
        }
        else {
            setOutputValue("currentRegularOutField", null);
            setOutputValue("currentOutField", new VNIrregularField(probeField));
        }
        if (graphs.getDisplays().isEmpty())
            setOutputValue("accumulatedOutField", null);
        else {
            ArrayList<ProbeDisplay> shownGraphs = graphs.getDisplays();
            IrregularField outIrregularField  = null;
            for (int i = 0; i < shownGraphs.size(); i++)
                outIrregularField = MergeIrregularField.merge(outIrregularField, 
                                                      shownGraphs.get(i).getField(), i, false, true);
            outIrregularField.updatePreferredExtents();
            outField = outIrregularField;
            setOutputValue("accumulatedOutField", new VNIrregularField((IrregularField)outField));
        }
        
        prepareOutputGeometry();
        presentationParams.getRenderingParams().setDisplayMode(RenderingParams.EDGES);
        show();
        if (currentProbeGlyph.getParent() == null)
            outObj.addNode(currentProbeGlyph);
        outObj.addGeometry2D(graphsGeometry);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            outObj.clearAllGeometry();
            outObj.clearGeometries2D();
            return;
        }
        if (fromParams) 
            fromParams = false;
        else {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = newField != inField;
            inField = newField;
            validateParamsAndSetSmart(isDifferentField && !isFromVNA());
            notifyGUIs(parameters, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (isNewField) {
                graphs.clearDisplays();
                probeField = null;
                regularProbeField = null;
                outField = null;
                setOutputValue("currentRegularOutField", null);
                setOutputValue("currentOutField", null);
                setOutputValue("accumulatedOutField", null);
                if (geometricProbe != null)
                    geometricProbe.clearChangeListeners();
                geometricProbe = null;
                if (indexProbe != null)
                    indexProbe.clearChangeListeners();
                indexProbe = null;
                if (inField instanceof IrregularField) 
                    for (CellSet cs : ((IrregularField)inField).getCellSets()) 
                        cs.addGeometryData(inField.getCoords(0));
                validateParamsAndSetSmart(true);
                
                if (inField instanceof RegularField) {
                    indexProbe = new IndexSlice1D();
                    indexProbe.setInData(inField, dataMappingParams);
                    indexProbe.addChangeListener(probeListener);
                    indexProbeGlyph = indexProbe.getGlyphGeometry();
                    currentProbe = indexProbe;
                }
                if (inField.getTrueNSpace() >= 2) {
                    geometricProbe = new BasicLineProbe();
                    geometricProbe.setInData(inField, dataMappingParams);
                    geometricProbeGlyph = geometricProbe.getGlyph();
                    geometricProbe.addChangeListener(probeListener);
                    currentProbe = geometricProbe;
                }
                currentProbeGlyph = currentProbe.getGlyphGeometry();
                probeField = currentProbe.getSliceField();
                computeUI.setProbePanels(geometricProbe != null ? geometricProbe.getGlyphGUI() : null,
                                         indexProbe != null ?     indexProbe.getGlyphGUI() :     null);
                
                outField = null;
                graphs.clearDisplays();
                newGraphedFieldSet = false;
                probeReady = canAddProbe = true;
                computeUI.enableAddGraph(probeReady && canAddProbe);
            }
            if (!newGraphedFieldSet && probeField != null) {
                computeUI.setGraphedField(probeField);
                newGraphedFieldSet = true;
            }
        }
        doOutput();
    }
}
