//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.TextGlyphs;

import java.awt.Color;
import java.util.IllegalFormatException;
import javax.media.j3d.*;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.dataarrays.StringDataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.geometries.objects.DataMappedGeometryObject;
import pl.edu.icm.visnow.geometries.objects.GeometryObject;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.parameters.FontParams;
import pl.edu.icm.visnow.geometries.utils.Texts2D;
import pl.edu.icm.visnow.geometries.utils.Texts3D;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.geometries.viewer3d.Display3DPanel;
import pl.edu.icm.visnow.lib.utils.geometry2D.CXYZString;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessage;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.dataarrays.ObjectDataArray;

/**
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GlyphsObject extends DataMappedGeometryObject
{

    private static final Logger LOGGER = Logger.getLogger(GlyphsObject.class);

    private CXYZString[] glyphs = null;
    private Params params;
    private FontParams fontParams;
    private Field inField = null;
    private OpenBranchGroup outGroup = null;
    private DataArray glyphDaraArray, thresholdDataArray;
    private float thr = -Float.MAX_VALUE;
    private float[] baseCoords = null;
    private long[] glyphIn = null;
    private int nGlyphs = 0;
    private int fontSize = 12;
    private TextGlyphs parent = null;

    //TODO: this should be accessible by sth like params.getFormat().getDefaultValue() or parameters.getDefaultValue("format");
    private String defaultTextFormat;

    public GlyphsObject()
    {
        name = "text glyphs";
    }

    private boolean isValid(long i)
    {
        double d = 0, t = 0;
        if (thresholdDataArray == null)
            return true;
        if (thresholdDataArray.getVectorLength() == 1)
            t = thresholdDataArray.getFloatElement(i)[0];
        else {
            float[] v = thresholdDataArray.getFloatElement(i);
            t = 0;
            for (int j = 0; j < v.length; j++)
                t += v[j] * v[j];
            t = sqrt(t);
        }
        return t > thr;
    }

    @Override
    public void drawLocal2D(J3DGraphics2D vGraphics, LocalToWindow ltw, int width, int height)
    {
        if (ltw == null || params == null)
            return;
        if (glyphs != null) {
            for (int i = 0; i < glyphs.length; i++) {
                if (glyphs[i] != null) {
                    glyphs[i].update(ltw);
                    glyphs[i].draw(vGraphics, width, height);
                }
            }
        }
    }

    private void updateCoords()
    {
        if (nGlyphs <= 0)
            return;
        if (baseCoords == null ||
            baseCoords.length != 3 * nGlyphs)
            baseCoords = new float[3 * nGlyphs];
        for (int i = 0; i < baseCoords.length; i++)
            baseCoords[i] = 0;
        if (inField.getCurrentCoords() != null) {
            FloatLargeArray inCoords = inField.getCurrentCoords();
            float[] p = new float[3];
            for (int i = 0; i < nGlyphs; i++)
                System.arraycopy(inCoords.getFloatData(p, 3 * glyphIn[i], 3 * glyphIn[i] + 3, 1), 0, baseCoords, 3 * i, 3);
            //            for (int j = 0; j < nSp; j++)
            //               baseCoords[3 * i + j] = fldCoords[nSp * glyphIn[i] + j];
        } else if (inField instanceof RegularField) {
            float[][] inAff = ((RegularField) inField).getAffine();
            int[] dims = ((RegularField) inField).getDims();
            long i0 = 0, i1 = 0, i2 = 0;
            for (int i = 0; i < nGlyphs; i++) {
                long j = glyphIn[i];
                i0 = j % dims[0];
                if (dims.length > 1) {
                    j /= dims[0];
                    i1 = j % dims[1];
                    if (dims.length > 2)
                        i2 = j / dims[1];
                }
                for (int k = 0; k < 3; k++)
                    baseCoords[3 * i + k] = inAff[3][k] + i0 * inAff[0][k] + i1 * inAff[1][k] + i2 * inAff[2][k];
            }
        }

    }

    private void prepareGlyphCount()
    {
        nGlyphs = 0;
        glyphDaraArray = inField.getComponent(params.getComponent());
        if (glyphDaraArray == null || (glyphDaraArray.getVectorLength() != 3 && glyphDaraArray.getVectorLength() != 1))
            return;
        thr = params.getThr();
        thresholdDataArray = null;
        if (params.getThrComponent() >= 0)
            thresholdDataArray = inField.getComponent(params.getThrComponent());
        int maxNGlyphs = 1;
        if (inField instanceof RegularField) {
            int[] low = params.getLowCrop();
            int[] up = params.getUpCrop();
            int[] down = params.getDown();
            for (int i = 0; i < ((RegularField) inField).getDimNum(); i++)
                maxNGlyphs *= (up[i] - low[i]) / down[i] + 1;
        } else
            maxNGlyphs = (int) (inField.getNNodes() / params.getDownsize() + 1);
        long[] gl = new long[maxNGlyphs];
        for (int i = 0; i < gl.length; i++)
            gl[i] = -1;
        nGlyphs = 0;
        if (inField instanceof RegularField) {
            RegularField inRegularField = (RegularField) inField;
            if (inRegularField.getDims() == null)
                return;
            long[] dims = inRegularField.getLDims();
            int[] low = params.getLowCrop();
            int[] up = params.getUpCrop();
            int[] down = params.getDown();
            long l;
            switch (dims.length) {
                case 3:
                    for (long i = low[2]; i < up[2]; i += down[2])
                        for (long j = low[1]; j < up[1]; j += down[1]) {
                            l = (dims[1] * i + j) * dims[0] + low[0];
                            for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                                if (isValid(l)) {
                                    gl[nGlyphs] = l;
                                    nGlyphs += 1;
                                }
                        }
                    break;
                case 2:
                    for (long j = low[1]; j < up[1]; j += down[1]) {
                        l = j * dims[0] + low[0];
                        for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                            if (isValid(l)) {
                                gl[nGlyphs] = l;
                                nGlyphs += 1;
                            }
                    }
                    break;
                case 1:
                    l = low[0];
                    for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                        if (isValid(l)) {
                            gl[nGlyphs] = l;
                            nGlyphs += 1;
                        }
                    break;
            }
        } else {
            int downsize = params.getDownsize();
            for (long i = 0; i < inField.getNNodes(); i += downsize)
                if (isValid(i)) {
                    gl[nGlyphs] = i;
                    nGlyphs += 1;
                }
        }
        if (nGlyphs > 10000)
        {
            VisNow.get().userMessageSend(parent, "object size", "Could not render " + nGlyphs + " text glyphs", Level.WARNING);
            nGlyphs = -1;
            return;
        }
        glyphIn = new long[nGlyphs];
        System.arraycopy(gl, 0, glyphIn, 0, nGlyphs);
    }

    public void prepareGlyphs()
    {
        if (localToWindow == null)
            localToWindow = getCurrentViewer().getLocToWin();
        if (getCurrentViewer() == null || localToWindow == null || fontParams == null)
            return;
        fontParams.createFontMetrics(localToWindow,
                                     getCurrentViewer().getWidth(),
                                     getCurrentViewer().getHeight());
        fontSize = fontParams.getFontSize();
        glyphDaraArray = inField.getComponent(params.getComponent());
        if (glyphDaraArray == null || glyphDaraArray.getVectorLength() != 1 || nGlyphs < 1)
            return;
        String format = params.getFormat();
        Color textColor2d = fontParams.getColor();
        if (outGroup != null)
            outGroup.detach();
        outGroup = null;
        String[] texts = new String[nGlyphs];
        if (glyphDaraArray.getType() == DataArrayType.FIELD_DATA_STRING) {
            String[] sData = ((StringDataArray) glyphDaraArray).getRawArray().getData();
            if (sData == null)
                return;
            for (int i = 0; i < texts.length; i++)
                texts[i] = sData[(int) glyphIn[i]];
        } else if (glyphDaraArray.getType() == DataArrayType.FIELD_DATA_OBJECT) {
            Object[] sData = ((ObjectDataArray) glyphDaraArray).getRawArray().getData();
            if (sData == null)
                return;
            for (int i = 0; i < texts.length; i++)
                texts[i] = sData[(int) glyphIn[i]].toString();
        } else {
            texts = new String[nGlyphs];
            try {
                for (int i = 0; i < texts.length; i++)
                    texts[i] = String.format(format, glyphDaraArray.getFloatElement(glyphIn[i])[0]);
            } catch (IllegalFormatException ex) { //if incorrect format then drop to default format
                //TODO: how to resolve problem with no access to current application? Maybe some VisNow.getCurrentApplication()
                VisNow.get().userMessageSend(new UserMessage("", "text glyphs", "Incorrect text format", "Specified format is incorrect: " + format + "<br>" + "Dropped to default one: " + defaultTextFormat, Level.WARNING));
                for (int i = 0; i < texts.length; i++)
                    texts[i] = String.format(defaultTextFormat, glyphDaraArray.getFloatElement(glyphIn[i])[0]);
            }
        }
        if (fontParams.isThreeDimensional()) {
            glyphs = null;
            outGroup = new Texts3D(baseCoords, texts, fontParams);
            addNode(outGroup);
            setExtents(inField.getPreferredExtents());
        } else {
            Texts2D texts2D = new Texts2D(baseCoords, texts, fontParams);
            glyphs = texts2D.getGlyphs();
            geometryObj.getCurrentViewer().refresh();
        }
    }

    public void setParent(TextGlyphs parent)
    {
        this.parent = parent;
    }

    public void update(Field inField, Params params)
    {
        if (inField == null || params == null)
            return;
        this.inField = inField;
        this.params = params;
        if (defaultTextFormat == null)
            defaultTextFormat = params.getFormat();
        fontParams = params.getFontParams();
        if (params.getChange() == Params.COUNT_CHANGED)
            prepareGlyphCount();
        if (nGlyphs < 1)
            return;
        if (params.getChange() >= Params.GLYPHS_CHANGED) {
            updateCoords();
            prepareGlyphs();
        }
        params.setChange(0);
    }

    @Override
    public void setCurrentViewer(Display3DPanel panel)
    {
        this.myViewer = panel;
        outObj.setCurrentViewer(panel);

        for (GeometryObject child : geomChildren) {
            child.setCurrentViewer(panel);
        }
        prepareGlyphs();
    }

}
