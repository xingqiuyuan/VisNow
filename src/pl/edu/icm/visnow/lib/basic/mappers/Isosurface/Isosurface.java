//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Isosurface;

import pl.edu.icm.visnow.lib.utils.isosurface.IrregularFieldIsosurface;
import pl.edu.icm.visnow.lib.utils.isosurface.IsosurfaceEngine;
import pl.edu.icm.visnow.lib.utils.isosurface.IsosurfaceEngineParams;
import pl.edu.icm.visnow.lib.utils.isosurface.RegularFieldIsosurface;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArraySchema;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.ParameterProxy;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;
import static pl.edu.icm.visnow.lib.basic.mappers.Isosurface.IsosurfaceShared.*;
import pl.edu.icm.visnow.lib.gui.DownsizeUIShared;
import pl.edu.icm.visnow.lib.gui.cropUI.CropUIShared;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.field.MergeIrregularField;
import pl.edu.icm.visnow.lib.utils.field.SmoothTriangulation;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Isosurface extends OutFieldVisualizationModule
{
    
    class SliceRemap implements pl.edu.icm.visnow.lib.utils.isosurface.SliceRemap
    {
        @Override
        public void remap(float[] slice, float val) 
        {
            for (int i = 0; i < slice.length; i++) {
                slice[i] -= val;
                if (slice[i] == 0)
                    slice[i] += .001f;
            }
        }
        
    }
    
    private static final Logger LOGGER = Logger.getLogger(Isosurface.class);
    /**
     *
     * inField - a 3D field to create isosurface;
     * at least one scalar data component must be present.
     * <p>
     * outField - isosurface field will be created by update method -
     * can be void, can contain no node data (geometry only)
     *
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected Field inField;
    protected IsosurfaceGUI computeUI = null;
    protected SmoothTriangulation smoother = new SmoothTriangulation();
    protected IsosurfaceEngine isosurfaceEngine;
    protected int stateStatus = 0;

    private int runQueue = 0;

    /**
     * Instantiates Isosurface module.
     */
    public Isosurface()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();

            }
        });

        renderingParams.setDisplayMode(RenderingParams.SURFACE);
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new IsosurfaceGUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        outObj.setName("isosurface");
        smoother.addFloatValueModificationListener(
                new FloatValueModificationListener()
                {
                    @Override
                    public void floatValueChanged(FloatValueModificationEvent e)
                    {
                        setProgress(e.getVal() / 10 + .9f);
                    }
                });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        List<Parameter> defaultParameters = createDefaultParametersAsList();
        defaultParameters.addAll(CropUIShared.createDefaultParametersAsList());
        defaultParameters.addAll(DownsizeUIShared.createDefaultParametersAsList());
        return defaultParameters.toArray(new Parameter[defaultParameters.size()]);
    }

    //TODO: remove this (may be problem on many threads)
    protected FloatValueModificationListener progressListener = new FloatValueModificationListener()
    {

        @Override
        public void floatValueChanged(FloatValueModificationEvent e)
        {
            setProgress((stateStatus + e.getVal()) / parameters.get(THRESHOLDS).length);
        }
    };

    /**
     * Updates module state.
     * 
     * @param parameterProxy ParameterProxy object
     * @param updateRenderingParams true if rendering parameters should be updated
     */
    public void update(ParameterProxy parameterProxy, boolean updateRenderingParams)
    {
        ParameterProxy p = parameterProxy;
        float dataCurrentTime = 0;
        if (inField.isTimeDependant() && p.get(TIME_FRAME) != inField.getCurrentTime()) {
            dataCurrentTime = inField.getCurrentTime();
            inField.setCurrentTime(p.get(TIME_FRAME));
        }
        IrregularField tmpField = null;
        float[] thresholds = p.get(THRESHOLDS);
        DataArraySchema componentSchema = inField.getComponent(p.get(META_COMPONENT_NAMES)[p.get(SELECTED_COMPONENT)]).getSchema();

        int[] up = p.get(CropUIShared.HIGH);
        for (int j = 0; j < up.length; j++) up[j]++;

        IsosurfaceEngineParams engineParams
                = new IsosurfaceEngineParams(p.get(SELECTED_COMPONENT), p.get(DownsizeUIShared.DOWNSIZE),
                                             p.get(CropUIShared.LOW), up, p.get(SMOOTHING_STEP_COUNT), p.get(SMOOTHING_STEP_COUNT) > 0,
                                             p.get(UNCERTAINTY_COMPONENT_PRESENT), p.get(CELLSET_SEPARATION), p.get(TIME_FRAME));

        stateStatus = 0;
        for (int i = 0; i < thresholds.length; i++, stateStatus++) {

            IrregularField currentField
                    = isosurfaceEngine.makeIsosurface(engineParams, (float) componentSchema.dataPhysToRaw(p.get(THRESHOLDS)[i]));
            if (currentField != null)
                tmpField = MergeIrregularField.merge(tmpField, currentField, i, p.get(CELLSET_SEPARATION));
        }
        if (tmpField == null) {
            outField = null;
            outIrregularField = null;
            setOutputValue("isosurfaceField", null);
            show();
        } else {
            if (p.get(SMOOTHING_STEP_COUNT) > 0) {
                smoother.setInField(tmpField);
                outField = tmpField.cloneShallow();
                outIrregularField = (IrregularField) outField;
                outIrregularField.setCoords(new FloatLargeArray(smoother.smoothCoords(p.get(SMOOTHING_STEP_COUNT), .5f)), 0);
                if (tmpField.getNormals() != null)
                    outIrregularField.setNormals(new FloatLargeArray(smoother.smoothNormals(p.get(SMOOTHING_STEP_COUNT), .5f)));
            } else {
                outField = tmpField;
                outIrregularField = (IrregularField) outField;
            }
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            outIrregularField.setName(inField.getName());
            prepareOutputGeometry();
            if (updateRenderingParams) {
                renderingParams.setDisplayMode(RenderingParams.SURFACE);
//                if (presentationParams.getChildrenParams() != null)
//                    for (PresentationParams pp : presentationParams.getChildrenParams()) 
//                        pp.getRenderingParams().setDisplayMode(RenderingParams.SURFACE);
            }
            show();
            setOutputValue("isosurfaceField", new VNIrregularField(outIrregularField));
            if (inField.getCurrentTime() != dataCurrentTime)
                inField.setCurrentTime(dataCurrentTime);
        }
    }

    private void validateParamsAndSetSmart(boolean resetFully, boolean differentRegularDims)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        String previousComponentName = parameters.get(META_COMPONENT_NAMES)[parameters.get(SELECTED_COMPONENT)];

        DataContainer.ComponentSimpleView componentView = inField.getComponentSimpleView(false, false);
        parameters.set(META_COMPONENT_NAMES, componentView.componentNames);
        parameters.set(META_MINMAX_RANGES, componentView.preferredPhysMinMaxRanges);
        parameters.set(META_TIME_RANGES, componentView.timeRanges);

        //validate/smart selected component
        //(find component with the same name) assuming there are no two compoenents with the same name        
        int newComponentIndex = Arrays.asList(componentView.componentNames).indexOf(previousComponentName);
        boolean newComponent = newComponentIndex == -1;
        newComponentIndex = Math.max(0, newComponentIndex);
        parameters.set(SELECTED_COMPONENT, newComponentIndex);

        //validate/smart thresholds
        double[] minMax = componentView.preferredPhysMinMaxRanges[newComponentIndex];
        float[] thresholds = parameters.get(THRESHOLDS);
        //set valid single threshold if needed
        if (resetFully || newComponent)
            thresholds = new float[]{(float) (minMax[1] + minMax[0]) / 2};
        else //otherwise thresholds need to be validated against current components (may be same name but different dims and data)
            for (int i = 0; i < thresholds.length; i++)
                thresholds[i] = (float) Math.min(minMax[1], Math.max(thresholds[i], minMax[0]));
        parameters.set(THRESHOLDS, thresholds);

        boolean isRegular = inField instanceof RegularField;
        parameters.set(META_IS_REGULAR, isRegular);

        if (isRegular) {
            //validate/smart crop
            int[] previousDims = parameters.get(CropUIShared.META_FIELD_DIMENSION_LENGTHS);
            int[] dims = ((RegularField) inField).getDims();
            parameters.set(CropUIShared.META_FIELD_DIMENSION_LENGTHS, dims);
            if (resetFully)
                CropUIShared.resetLowHighRange(parameters);
            else if (differentRegularDims) {
                CropUIShared.rescaleAll(parameters, previousDims);
            }

            //validate/smart downsize
            if (resetFully || differentRegularDims) {
                int[] low = parameters.get(CropUIShared.LOW);
                int[] high = parameters.get(CropUIShared.HIGH);
                int[] croppedDims = new int[low.length];
                for (int i = 0; i < croppedDims.length; i++)
                    croppedDims[i] = high[i] - low[i] + 1;

                int[] smartDownsize = DownsizeUIShared.getSmartDefault(croppedDims);
                parameters.set(DownsizeUIShared.META_DEFAULT_DOWNSIZE, smartDownsize,
                               DownsizeUIShared.DOWNSIZE, smartDownsize);
            }
        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            //1. get new field
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            //1a. set distinction flags
            boolean isNewField = !isFromVNA() && newInField != inField;
            boolean isNonCompatibleField = !isFromVNA() &&
                    (inField == null ||
                    inField instanceof IrregularField && newInField instanceof RegularField ||
                    inField instanceof RegularField && newInField instanceof IrregularField);
            boolean isNewIrregularField = !isFromVNA() &&
                    inField instanceof IrregularField && newInField instanceof IrregularField && newInField != inField;  
            boolean isDifferentRegularDims = !isFromVNA() &&
                    inField != null && inField instanceof RegularField && newInField instanceof RegularField &&
                    !Arrays.toString(((RegularField) inField).getDims()).equals(Arrays.toString(((RegularField) newInField).getDims()));
            inField = newInField;

            LOGGER.debug(String.format("Flags isNewField: %b, isNonCompatibleField: %b, isNewIrregularField: %b, isDifferentRegularDims: %b", 
                                       isNewField, isNonCompatibleField, isNewIrregularField, isDifferentRegularDims));
            
            //2. validate params             
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isNonCompatibleField || isNewIrregularField, isDifferentRegularDims);
                //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }
            //3. update gui 
            //TODO: switch to ParameterProxy
            notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);

            //TODO: handle & test: inField.getNComponents() < 1
            //4. run computation and propagate
            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                if (inField instanceof RegularField) {
                    isosurfaceEngine = new RegularFieldIsosurface((RegularField) inField);
                    ((RegularFieldIsosurface)isosurfaceEngine).setSliceRemap(new SliceRemap());
                    isosurfaceEngine.addFloatValueModificationListener(progressListener);
                } else {
                    isosurfaceEngine = new IrregularFieldIsosurface((IrregularField) inField);
                    isosurfaceEngine.addFloatValueModificationListener(progressListener);
                }
                update(p, isNewField);
            }
        }
    }

}
