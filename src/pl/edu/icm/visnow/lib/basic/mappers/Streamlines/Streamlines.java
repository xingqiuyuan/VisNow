//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Streamlines;


import org.apache.log4j.Logger;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Arrays;
import java.util.List;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;

import pl.edu.icm.jscic.dataarrays.DataArray;

import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;

import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import static pl.edu.icm.jscic.utils.CropDownUtils.*;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.geometry2D.GeometryObject2D;

import pl.edu.icm.jscic.cells.CellType;

import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.ParameterProxy;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;

import static pl.edu.icm.visnow.lib.basic.mappers.Streamlines.StreamlinesShared.*;
import pl.edu.icm.visnow.lib.gui.DownsizeUIShared;

import pl.edu.icm.visnow.lib.types.VNField;

import pl.edu.icm.visnow.lib.utils.field.FieldInterpolateToMesh;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 * <p>
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Streamlines extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(Streamlines.class);

    public static OutputEgg[] outputEggs = null;
    public static InputEgg[] inputEggs = null;

    private IrregularField inIrregularField;
    private RegularField inRegularField;

    GUI gui = null;

    private Field inField = null;
    private Field flowField;

    private Field startPointsField;
    private int[] startPointsFieldDims = null;

    private Field downsizedStartPointsField;

    private ComputeStreamlines streamlines;

    private static final int PREFERRED_NUMBER_OF_START_POINTS = 3000;

    private int runQueue = 0;

    ProgressAgent progressAgent = ProgressAgent.getDummyAgent();

    public Streamlines()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                try {
                    gui = new GUI();
                    gui.setParameterProxy(parameters);
                    ui.addComputeGUI(gui);
                    setPanel(ui);
                } catch (Exception e) {
                    LOGGER.error("Cant create GUI", e);
                }
            }
        });
        outObj.setName("Streamlines");
    }

    class Streamlines2D extends GeometryObject2D implements Cloneable
    {

        private int[] lines = null;
        private float[] coords = null;
        private float[] colors = null;

        public Streamlines2D(String name, int nvert, int[] lines, float[] c, float[] colors)
        {
            super(name);
            this.lines = lines;
            coords = new float[2 * nvert];
            for (int i = 0, k = 0, l = 0; i < nvert; i++, k += 1)
                for (int j = 0; j < 2; j++, k++, l++)
                    coords[l] = c[k];
            this.colors = colors;
        }

        @Override
        public void drawLocal2D(Graphics2D g, AffineTransform at)
        {
            g.translate(at.getTranslateX(), at.getTranslateY());
            if (coords == null) {
                g.translate(-at.getTranslateX(), -at.getTranslateY());
                return;
            }
            for (int n = 0, k = 0; n < lines.length; n++, k += 1)
                for (int i = 0; i < lines[n] - 1; i++) {
                    g.setColor(new Color(colors[3 * k], colors[3 * k + 1], colors[3 * k + 2]));
                    g.drawLine((int) (coords[2 * k] * at.getScaleX()), (int) ((height - coords[2 * k + 1]) * at.getScaleY()),
                               (int) (coords[2 * k + 2] * at.getScaleX()), (int) ((height - coords[2 * k + 3]) * at.getScaleY()));
                    k += 1;
                }
            g.translate(-at.getTranslateX(), -at.getTranslateY());
        }
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        List<Parameter> defaultParameters = DownsizeUIShared.createDefaultParametersAsList();
        defaultParameters.addAll(Arrays.asList(new Parameter[]{
            new Parameter<>(COMPONENT, "--------"),
            new Parameter<>(DOWNSIZE_IRREGULAR, 1),
            new Parameter<>(STEP, 0.002f),
            new Parameter<>(NUM_STEPS_FORWARD, 100),
            new Parameter<>(NUM_STEPS_BACKWARD, 10),
            new Parameter<>(RUNNING_MESSAGE, RUN_DYNAMICALLY),
            new Parameter<>(META_IS_START_POINT_FIELD_REGULAR, true),
            new Parameter<>(META_VECTOR_COMPONENT_NAMES, new String[]{"--------"}),
            new Parameter<>(META_PREFERRED_STEPS, new float[]{1})
        }));
        return defaultParameters.toArray(new Parameter[]{});
    }

    protected void validateParametersAndSetSmart(boolean resetAll, boolean recalculateDownsize)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_IS_START_POINT_FIELD_REGULAR, startPointsField instanceof RegularField);
        String[] vectorComponentNames = inField.getVectorComponentNames();
        parameters.set(META_VECTOR_COMPONENT_NAMES, vectorComponentNames);

        if (resetAll) {
            parameters.set(COMPONENT, vectorComponentNames[0]);
            //calculate preferred steps
            double fieldExtent = 0;
            float[][] ext = inField.getPreferredExtents(); //powinno być z phys extents?
            for (int i = 0; i < 3; i++)
                fieldExtent += (ext[0][i] - ext[1][i]) * (ext[0][i] - ext[1][i]);

            float[] preferredSteps = new float[vectorComponentNames.length];
            int stepIndex = 0;
            for (String componentName : vectorComponentNames) {
                double vNorm = inField.getComponent(componentName).getMeanSquaredValue();
                if (vNorm == 0.0) vNorm = 1.0; //this is to overcome divide by 0 for constant=0 fields
                preferredSteps[stepIndex++] = (float) (.001 * Math.sqrt(fieldExtent) / vNorm);
            }

            parameters.set(META_PREFERRED_STEPS, preferredSteps);
            parameters.set(STEP, preferredSteps[0]);
            parameters.set(NUM_STEPS_FORWARD, 100);
            parameters.set(NUM_STEPS_BACKWARD, 10);
        }
            
        if (resetAll || recalculateDownsize) {
            if (startPointsField instanceof RegularField) {
                int[] smartDownsize = DownsizeUIShared.getSmartDefault(((RegularField) startPointsField).getDims(), PREFERRED_NUMBER_OF_START_POINTS);
                parameters.set(DownsizeUIShared.META_DEFAULT_DOWNSIZE, smartDownsize,
                               DownsizeUIShared.DOWNSIZE, smartDownsize);
            } else
                parameters.set(DOWNSIZE_IRREGULAR, (int) (startPointsField.getNNodes() / PREFERRED_NUMBER_OF_START_POINTS));
        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        gui.update(clonedParameterProxy, setRunButtonPending);
    }

    private void checkAndDownsizeStartPointsField(Parameters p)
    {
        int[] down = new int[3];
        Arrays.fill(down, 1);
        System.arraycopy(p.get(DownsizeUIShared.DOWNSIZE), 0, down, 0, p.get(DownsizeUIShared.DOWNSIZE).length);

        if (startPointsField instanceof IrregularField) {
            int downSize = p.get(DOWNSIZE_IRREGULAR);
            if (downSize < 1)
                downSize = 1;
            if (downSize == 1) {
                downsizedStartPointsField = startPointsField.cloneShallow();
                for (CellSet cs : ((IrregularField) downsizedStartPointsField).getCellSets())
                    cs.generateExternFaces();
            } else {
                downsizedStartPointsField = new IrregularField((int) startPointsField.getNNodes() / downSize);
                int[] nodes = new int[(int) startPointsField.getNNodes() / downSize];
                for (int i = 0; i < nodes.length; i++)
                    nodes[i] = i;
                CellArray nodesArray = new CellArray(CellType.POINT, nodes, null, nodes);
                CellSet ptSet = new CellSet();
                ptSet.setCellArray(nodesArray);
                ptSet.setBoundaryCellArray(nodesArray);
                ((IrregularField) downsizedStartPointsField).addCellSet(ptSet);
                downsizedStartPointsField.addComponent(DataArray.create(nodes, 1, "dummy"));
                byte[] startMask = null;
                byte[] downsizedStartMask = null;
                if (startPointsField.hasMask()) {
                    startMask = startPointsField.getCurrentMask().getData();
                    downsizedStartMask = new byte[(int)downsizedStartPointsField.getNNodes()];
                }
                float[] ptsCoords = startPointsField.getCurrentCoords() == null ? null : startPointsField.getCurrentCoords().getData();
                float[] downsizedCoords = new float[3 * (int) downsizedStartPointsField.getNNodes()];
                for (int i = 0, j = 0, k = 0, m = 0; 
                     i < downsizedStartPointsField.getNNodes(); 
                     i++, m += downSize, k += downSize * 3) {
                    for (int l = 0; l < 3; l++, j++)
                        downsizedCoords[j] = ptsCoords[k + l];
                    if (startMask != null)
                        downsizedStartMask[i] = startMask[m];
                }
                downsizedStartPointsField.setCurrentCoords(new FloatLargeArray(downsizedCoords));
                if (startMask != null)
                    downsizedStartPointsField.setCurrentMask(new LogicLargeArray(downsizedStartMask));
            }
        } else {
            boolean downsized = false;
            startPointsFieldDims = ((RegularField) startPointsField).getDims();
            for (int i = 0; i < startPointsFieldDims.length; i++)
                if (down[i] > 1) {
                    downsized = true;
                    break;
                }
            if (!downsized)
                downsizedStartPointsField = startPointsField.cloneShallow();
            else {
                int[] downsizedDims = new int[startPointsFieldDims.length];
                for (int i = 0; i < downsizedDims.length; i++)
                    downsizedDims[i] = (startPointsFieldDims[i] - 1) / down[i] + 1;
                downsizedStartPointsField = new RegularField(downsizedDims);
                int[] nodes = new int[(int) downsizedStartPointsField.getNNodes()];
                for (int i = 0; i < nodes.length; i++)
                    nodes[i] = i;
                downsizedStartPointsField.addComponent(DataArray.create(nodes, 1, "dummy"));
                if (startPointsField.hasMask())
                    downsizedStartPointsField.setCurrentMask((LogicLargeArray)downArray(startPointsField.getCurrentMask(), 1, startPointsFieldDims, down));
                if (null == startPointsField.getCurrentCoords()) {
                    float[][] ptsAffine = ((RegularField) startPointsField).getAffine();
                    float[][] downsizedPtsAffine = new float[4][3];
                    for (int i = 0; i < 3; i++) {
                        downsizedPtsAffine[3][i] = ptsAffine[3][i];
                        for (int j = 0; j < 3; j++)
                            downsizedPtsAffine[j][i] = down[j] * ptsAffine[j][i];
                    }
                    ((RegularField) downsizedStartPointsField).setAffine(downsizedPtsAffine);
                } else 
                    downsizedStartPointsField.setCurrentCoords((FloatLargeArray)downArray(startPointsField.getCurrentCoords(), 3, startPointsFieldDims, down));
            }
        }
    }

    private void computeFlowField(Parameters p)
    {
        float[] outCoords = outIrregularField.getCurrentCoords() == null ? null : outIrregularField.getCurrentCoords().getData();
        flowField = downsizedStartPointsField.cloneShallow();
        flowField.setCurrentCoords(new FloatLargeArray(outCoords));
        int[] validStepRange = new int[2 * (int) flowField.getNNodes()];
        int[] tmpfrom = streamlines.getFromSteps();
        int[] tmpto = streamlines.getToSteps();
        if (flowField instanceof RegularField) {
            RegularField rfField = (RegularField) flowField;
            int[] dims = rfField.getDims();
            switch (dims.length) {
                case 3:
                    for (int k = 0, m = k * dims[0] * dims[1]; k < dims[2]; k++) {
                        int k0 = k - 1;
                        if (k0 < 0)
                            k0 = 0;
                        int k1 = k + 2;
                        if (k1 > dims[2])
                            k1 = dims[2];
                        for (int j = 0; j < dims[1]; j++) {
                            int j0 = j - 1;
                            if (j0 < 0)
                                j0 = 0;
                            int j1 = j + 2;
                            if (j1 > dims[1])
                                j1 = dims[1];
                            for (int i = 0; i < dims[0]; i++, m++) {
                                int t = Integer.MAX_VALUE;
                                int f = Integer.MIN_VALUE;
                                int i0 = i - 1;
                                if (i0 < 0)
                                    i0 = 0;
                                int i1 = i + 2;
                                if (i1 > dims[0])
                                    i1 = dims[0];
                                for (int kk = k0, ks = k0 - k + 1; kk < k1; kk++, ks++)
                                    for (int jj = j0, js = j0 - j + 1; jj < j1; jj++, js++)
                                        for (int ii = i0, is = i0 - i + 1, l = (kk * dims[1] + jj) * dims[0] + i0; ii < i1; ii++, is++, l++) {
                                            if (f < tmpfrom[l])
                                                f = tmpfrom[l];
                                            if (t > tmpto[l])
                                                t = tmpto[l];
                                        }
                                validStepRange[2 * m] = f;
                                validStepRange[2 * m + 1] = t;
                            }
                        }
                    }
                    break;
                case 2:
                    for (int j = 0, m = j * dims[0]; j < dims[1]; j++) {
                        int j0 = j - 1;
                        if (j0 < 0)
                            j0 = 0;
                        int j1 = j + 2;
                        if (j1 > dims[1])
                            j1 = dims[1];
                        for (int i = 0; i < dims[0]; i++, m++) {
                            int t = Integer.MAX_VALUE;
                            int f = Integer.MIN_VALUE;
                            int i0 = i - 1;
                            if (i0 < 0)
                                i0 = 0;
                            int i1 = i + 2;
                            if (i1 > dims[0])
                                i1 = dims[0];
                            for (int jj = j0, js = j0 - j + 1; jj < j1; jj++, js++)
                                for (int ii = i0, is = i0 - i + 1, l = jj * dims[0] + i0; ii < i1; ii++, is++, l++) {
                                    if (f < tmpfrom[l])
                                        f = tmpfrom[l];
                                    if (t > tmpto[l])
                                        t = tmpto[l];
                                }
                            validStepRange[2 * m] = f;
                            validStepRange[2 * m + 1] = t;
                        }
                    }
                    break;
                case 1:
                    for (int i = 0; i < dims[0]; i++) {
                        int t = Integer.MAX_VALUE;
                        int f = Integer.MIN_VALUE;
                        int i0 = i - 1;
                        if (i0 < 0)
                            i0 = 0;
                        int i1 = i + 2;
                        if (i1 > dims[0])
                            i1 = dims[0];
                        for (int ii = i0, is = i0 - i + 1, l = i0; ii < i1; ii++, is++, l++) {
                            if (f < tmpfrom[l])
                                f = tmpfrom[l];
                            if (t > tmpto[l])
                                t = tmpto[l];
                        }
                        validStepRange[2 * i] = f;
                        validStepRange[2 * i + 1] = t;
                    }
                    break;
            }
        } else {
            int n = (int) flowField.getNNodes();
            IrregularField irfField = (IrregularField) flowField;
            for (int i = 0; i < n; i++) {
                validStepRange[2 * i] = Integer.MAX_VALUE;
                validStepRange[2 * i + 1] = Integer.MIN_VALUE;
            }
            for (CellSet cs : irfField.getCellSets())
                for (CellArray ca : cs.getCellArrays())
                    if (ca != null) {
                        int k = ca.getNCellNodes();
                        int[] nodes = ca.getNodes();
                        for (int i = 0; i < nodes.length; i += k) {
                            int t = Integer.MAX_VALUE;
                            int f = Integer.MIN_VALUE;
                            for (int j = 0; j < k; j++) {
                                int l = nodes[i + j];
                                if (f < tmpfrom[l])
                                    f = tmpfrom[l];
                                if (t > tmpto[l])
                                    t = tmpto[l];
                            }
                            for (int j = 0; j < k; j++) {
                                int l = nodes[i + j];
                                if (f > validStepRange[2 * l])
                                    validStepRange[2 * l] = f;
                                if (t < validStepRange[2 * l + 1])
                                    validStepRange[2 * l] = t;
                            }
                        }
                    }

        }
        DataArray da = DataArray.create(validStepRange, 2, "valid time range");
        da.setUserData(new String[]{"valid time range"});
        flowField.addComponent(da);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("FromVNA: " + isFromVNA() + " inField " + (getInputFirstValue("inField") != null) + " startPoints " + (getInputFirstValue("startPoints") != null));

        if (getInputFirstValue("inField") != null) {
            this.switchPanelToGUI();

            // 1. Get new fields from input ports.
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            Field newStartPointsField = null;
            if (getInputFirstValue("startPoints") != null)
                newStartPointsField = ((VNField) getInputFirstValue("startPoints")).getField();
            if (newStartPointsField == null)
                newStartPointsField = newInField;

            // 1a. Set "Different Field" flags.
//            boolean hasDifferentDims = false;
            boolean isDifferentField = !isFromVNA() && (newInField != inField);//hasDifferentDims);

            boolean isAnyFieldDifferent = !isFromVNA() && (newInField != inField || newStartPointsField != startPointsField);

            inField = newInField;
            startPointsField = newStartPointsField;

            // 2. Validate parameters.             
            Parameters p;
            synchronized (parameters) {
                //XXX: this is not very "smart" - now downsize will be reset every time startpointfield is changed - as while manipulating regularfieldslice
                //should be considering startpointfield type (regular/irregular) and dimensions (if they are the same as previously then downsize should stay the same)
                validateParametersAndSetSmart(isDifferentField, isAnyFieldDifferent);
                // 2b. Clone parameters (local read-only copy).
                p = parameters.getReadOnlyClone();
            }

            // 3. Update GUI (GUI doesn't change parameters! Assuming correct set of parameters).
            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isAnyFieldDifferent);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {

                // 4. Run computation and propagate.
                checkAndDownsizeStartPointsField(p);
                progressAgent = getProgressAgent(downsizedStartPointsField.getNNodes());
                if (inField instanceof RegularField) {
                    inIrregularField = null;
                    inRegularField = (RegularField) inField;
                    streamlines = new ComputeRegularFieldStreamlines(inRegularField, p, progressAgent);
                } else {
                    inRegularField = null;
                    inIrregularField = (IrregularField) inField;
                    streamlines = new ComputeIrregularFieldStreamlines(inIrregularField, p, progressAgent);
                }
                streamlines.setStartPoints(downsizedStartPointsField);
                streamlines.updateStreamlines();
                if (inField instanceof RegularField)
                    outIrregularField = (IrregularField)streamlines.getOutField();
                else
                    outIrregularField = (IrregularField) new FieldInterpolateToMesh(inField, streamlines.getOutField()).interpolate();
                if (outIrregularField != null) {
                    outIrregularField.setUserData(new String[] {"streamlines field: " +
                                                                downsizedStartPointsField.getNNodes() + " streamlines" });
                    outIrregularField.getComponent("steps").setAutoResetMapRange(true);
                    computeFlowField(p); 
                    setOutputValue("streamlinesField", new VNIrregularField(outIrregularField));
                } else {
                    setOutputValue("streamlinesField", null);
                }
                if (flowField != null && flowField instanceof RegularField) {
                    setOutputValue("regularFlowField", new VNRegularField((RegularField) flowField));
                    setOutputValue("irregularFlowField", null);
                } else if (flowField != null && flowField instanceof IrregularField) {
                    setOutputValue("regularFlowField", null);
                    setOutputValue("irregularFlowField", new VNIrregularField((IrregularField) flowField));
                } else {
                    setOutputValue("regularFlowField", null);
                    setOutputValue("irregularFlowField", null);
                }
                outField = outIrregularField;
                prepareOutputGeometry();
                show();
            }
        } else
            this.switchPanelToDummy();
    }
}
