//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.PlanarSlice;

import java.util.Arrays;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.numeric.IrregularFieldSplitter;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.mappers.PlanarSlice.PlanarSliceShared.*;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class PlanarSlice extends OutFieldVisualizationModule
{

    protected static final int TRANSFORM = 0;
    protected static final int AXS = 1;
    protected static final int DIMS = 2;
    protected int change = TRANSFORM;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected IrregularField slicedField = null;
    protected GUI computeUI = null;
    protected Field inField = null;
    protected int lastAxis = 2;
    protected FloatLargeArray coords;
    protected float[] center
        = {
            0, 0, 0
        }; // geometric center of the box surrounding the input field used as origin 
    // of the coordinate system for computing slices
    protected float[] coeffs
        = {
            0, 0, 1
        }; // normalized coefficients of the slice plane equation
    protected float fieldCenterRHS = 0; // rhs for the equation of the slice plane passing through the center point
    protected float rhs = 0;

    public PlanarSlice()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (parameters.get(ADJUSTING)) {
                    if (inField == null)
                        return;
                    transformSlice();
                } else {
                    change = TRANSFORM;
                    if (parameters.get(AXIS) != lastAxis)
                        change = AXS;
                    startAction();
                }
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(TYPE, 0),
            new Parameter<>(AXIS, 2),
            new Parameter<>(RIGHT_SIDE, 0f),
            new Parameter<>(ORIGIN_MIN_MAX, new float[]{-1, 1}),
            new Parameter<>(ROTATION_VALUES, new float[3]),
            new Parameter<>(COEFFS, new float[] {0, 0, 1}),
            new Parameter<>(ADJUSTING, false)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        if (resetParameters) {
            parameters.set(TYPE, 0);
            parameters.set(AXIS, 2);
            parameters.set(ORIGIN_MIN_MAX, new float[]{-1, 1});
            parameters.set(ADJUSTING, false);
            parameters.set(ROTATION_VALUES, new float[3]);
            parameters.set(COEFFS, new float[] {0, 0, 1});
            parameters.set(RIGHT_SIDE, 0f);
        
            float[] crds = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
            if (crds != null) {
                float[] c = parameters.get(COEFFS);
                float rmin = Float.MAX_VALUE;
                float rmax = -Float.MAX_VALUE;
                for (int i = 0; i < (int) inField.getNNodes(); i++) {
                    float f = c[0] * crds[3 * i] +
                              c[1] * crds[3 * i + 1] +
                              c[2] * crds[3 * i + 2];
                    if (f < rmin)
                        rmin = f;
                    if (f > rmax)
                        rmax = f;
                }
                parameters.set(ORIGIN_MIN_MAX, new float[]{rmin, rmax});
                parameters.set(RIGHT_SIDE, (rmin + rmax) / 2);
            }
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    private void transformSlice()
    {
        coeffs = parameters.get(COEFFS);
        fieldCenterRHS = 0;
        for (int i = 0; i < 3; i++)
            fieldCenterRHS += coeffs[i] * center[i];
    }

    private void updateInitSlicePosition()
    {

        for (int i = 0; i < 3; i++)
            coeffs[i] = 0;
        coeffs[parameters.get(AXIS)] = 1;
        lastAxis = parameters.get(AXIS);
        float[] crds = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        if (crds != null) {
            float[] c = parameters.get(COEFFS);
            float rmin = Float.MAX_VALUE;
            float rmax = -Float.MAX_VALUE;
            for (int i = 0; i < (int) inField.getNNodes(); i++) {
                float f = c[0] * crds[3 * i] +
                          c[1] * crds[3 * i + 1] +
                          c[2] * crds[3 * i + 2];
                if (f < rmin)
                    rmin = f;
                if (f > rmax)
                    rmax = f;
            }
            parameters.set(ORIGIN_MIN_MAX, new float[]{rmin, rmax});
            if (parameters.get(RIGHT_SIDE) < rmin || parameters.get(RIGHT_SIDE) > rmax)
                parameters.set(RIGHT_SIDE, (rmin + rmax) / 2);
        }
    }

    static public IrregularField sliceField(IrregularField field, int type, final float[] coeffs, final float rhs)
    {
        for (CellSet cs : field.getCellSets())
            for (CellArray ca : cs.getCellArrays())
                if (ca != null && ca.getCellRadii() == null) {
                    cs.addGeometryData(field.getCurrentCoords());
                    if (ca.getCellRadii() == null)
                        throw new RuntimeException("CellSet has not GeometryData. run addGeometryData() method");
                }

        IrregularFieldSplitter splitter = new IrregularFieldSplitter(field, type);
        float[] coords = field.getCurrentCoords() == null ? null : field.getCurrentCoords().getData();

        for (int nSet = 0; nSet < field.getNCellSets(); nSet++) {
            CellSet trCS = field.getCellSet(nSet);
            splitter.initCellSetSplit(trCS);
            for (int iCellArray = 0; iCellArray < trCS.getCellArrays().length; iCellArray++) {
                if (trCS.getCellArray(CellType.getType(iCellArray)) == null)
                    continue;

                CellArray ca = trCS.getCellArray(CellType.getType(iCellArray));
                boolean isTriangulated = ca.isSimplicesArray();

                splitter.initCellArraySplit(ca);
                int nCellNodes = ca.getNCellNodes();
                float[] vals = new float[nCellNodes];
                int[] indices = ca.getDataIndices();
                float[] cellCenters = ca.getCellCenters();
                float[] cellRadii = ca.getCellRadii();
                int[] nodes = ca.getNodes();
                int[] cellNodes = new int[ca.getNCellNodes()];
                int m;
                for (int iCell = 0; iCell < ca.getNCells(); iCell++) {
                    int index = -1;
                    if (indices != null)
                        index = indices[iCell];

                    float t = coeffs[0] * cellCenters[3 * iCell] +
                              coeffs[1] * cellCenters[3 * iCell + 1] +
                              coeffs[2] * cellCenters[3 * iCell + 2] - rhs;
                    if (abs(t) <= cellRadii[iCell]) {
                        for (int l = 0; l < nCellNodes; l++) {
                            cellNodes[l] = m = nodes[nCellNodes * iCell + l];
                            vals[l] = (coeffs[0] * coords[3 * m] +
                                coeffs[1] * coords[3 * m + 1] +
                                coeffs[2] * coords[3 * m + 2] - rhs);
                        }
                        if (isTriangulated)
                            splitter.processSimplex(cellNodes, vals, index);
                        else
                            splitter.processCell(cellNodes, vals, index);
                    } else if (type == -1 && t < -cellRadii[iCell] ||
                        type == 1 && t > cellRadii[iCell])
                        if (isTriangulated)
                            splitter.addSimplex(iCell);
                        else
                            splitter.addCellTriangulation(iCell);
                }

            }
        }
        return splitter.createOutField(coeffs);
    }

    FloatValueModificationListener progressListener = new FloatValueModificationListener()
    {
        @Override
        public void floatValueChanged(FloatValueModificationEvent e)
        {
            setProgress(e.getVal());
        }
    };

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = newField != inField;
            inField = newField;

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField && !isFromVNA());
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (isDifferentField) {
                float[][] extents = inField.getPreferredExtents();
                for (int i = 0; i < 3; i++)
                    center[i] = .5f * (extents[0][i] + extents[1][i]);
                if (inField instanceof IrregularField)
                    slicedField = (IrregularField) inField;
                else
                    slicedField = inField.getTriangulated();
                coords = slicedField.getCurrentCoords();
                lastAxis = 2;

                for (CellSet cs : slicedField.getCellSets()) {
                    cs.addGeometryData(coords);
                }
                updateInitSlicePosition();
            }

            if (change == AXS)
                updateInitSlicePosition();
            change = TRANSFORM;
            transformSlice();

            rhs = parameters.get(RIGHT_SIDE);
            coeffs = parameters.get(COEFFS);

            outIrregularField = sliceField(slicedField, parameters.get(TYPE), coeffs, rhs);

            if (outIrregularField != null)
                setOutputValue("outField", new VNIrregularField(outIrregularField));
            else
                setOutputValue("outField", null);
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
    }
}
