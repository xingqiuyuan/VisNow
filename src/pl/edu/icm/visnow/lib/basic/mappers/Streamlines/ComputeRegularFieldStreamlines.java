//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Streamlines;

import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.visnow.lib.utils.numeric.ODE.Deriv;
import pl.edu.icm.visnow.lib.utils.numeric.ODE.RungeKutta;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.MatrixMath;

import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

import static pl.edu.icm.visnow.lib.basic.mappers.Streamlines.StreamlinesShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 *
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ComputeRegularFieldStreamlines extends ComputeStreamlines
{

    private static final Logger LOGGER = Logger.getLogger(ComputeRegularFieldStreamlines.class);

    private RegularField inField = null;
    private int[] dims = null;
    private float[][] affine = null;
    private float[][] invAffine = null;
    private float[] fldCoords = null;
    private float[] pullVects = null;
    private float indexRadius = 1;

    /**
     * Creates a new instance of ComputeRegularFieldStreamlines
     * <p>
     * @param inField    field containing a vector component to be mapped with streamlines
     * @param parameters streamlines module parameters
     */
    public ComputeRegularFieldStreamlines(RegularField inField, Parameters parameters)
    {
        super(inField, parameters);
        this.inField = inField;
        dims = inField.getDims();
        indexRadius = 0;
        for (int i = 0; i < dims.length; i++)
            indexRadius += dims[i] * dims[i];
        indexRadius = (float) sqrt(indexRadius);
        affine = inField.getAffine();
        fldCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        if (fldCoords == null)
            invAffine = inField.getInvAffine();
        else {
            invAffine = new float[4][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++)
                    invAffine[j][i] = 0;
                invAffine[i][i] = 1;
            }
        }
        nThreads = pl.edu.icm.visnow.system.main.VisNow.availableProcessors();
    }

    public ComputeRegularFieldStreamlines(RegularField inField, Parameters parameters, ProgressAgent progressAgent)
    {
        this(inField, parameters);
        this.progressAgent = progressAgent;
    }

    @Override
    public void setStartPoints(Field startPoints)
    {
        float[] st = null;
        nSrc = (int) startPoints.getNNodes();
        if (startPoints.getCurrentCoords() != null)
            st = startPoints.getCurrentCoords().getData();
        else if (startPoints instanceof RegularField)
            st = ((RegularField) startPoints).getCoordsFromAffine().getData();
        if (inField.getCurrentCoords() != null && inField.getDims().length == 3) {
            startCoords = new float[3 * (int) startPoints.getNNodes()];
            if (inField.getGeoTree() == null)
                inField.createGeoTree();
            for (int i = 0; i < (int) startPoints.getNNodes(); i++) {
                float[] p = inField.getFloatIndices(st[3 * i], st[3 * i + 1], st[3 * i + 2]);
                System.arraycopy(p, 0, startCoords, 3 * i, 3);
            }
        } else
            startCoords = st;
        fromSteps = new int[nSrc];
        toSteps = new int[nSrc];
        if (startPoints.hasMask())
            mask = startPoints.getCurrentMask().getData();
    }

    public synchronized void pullVectors()
    {
        vects = inField.getComponent(parameters.get(COMPONENT)).getRawFloatArray().getData();
        int vl = inField.getComponent(parameters.get(COMPONENT)).getVectorLength();
        if (fldCoords == null) {
            pullVects = new float[vects.length];
            float[][] iaf = inField.getInvAffine();
            for (int i = 0; i < vects.length; i += vl)
                for (int k = 0; k < vl; k++) {
                    pullVects[i + k] = 0;
                    for (int l = 0; l < vl; l++)
                        pullVects[i + k] += iaf[k][l] * vects[i + l];
                }
        } else
            pullVects = MatrixMath.pullVectorField(dims, fldCoords, vects);
        avgVector = 0;
        for (int i = 0; i < pullVects.length; i++)
            avgVector += pullVects[i] * pullVects[i];
        avgVector = (float) sqrt(avgVector / inField.getNNodes());
    }

    public static class InterpolateTo3DLines implements Runnable
    {

        int iThread;
        int nThreads;
        int[] dims;
        int nSrc;
        int nBackward;
        int nForward;
        float[][] upCoords;
        float[][] downCoords;
        int nData;
        int[] indices;
        float[][] inData;
        int[] inVlens;
        float[][] outData;

        public InterpolateTo3DLines(int iThread, int nThreads, int[] dims,
                                    int nSrc, int nBackward, int nForward,
                                    float[][] upCoords, float[][] downCoords, int[] indices,
                                    float[][] inData, int[] inVlens, float[][] outData)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.dims = dims;
            this.nSrc = nSrc;
            this.nBackward = nBackward;
            this.nForward = nForward;
            this.upCoords = upCoords;
            this.downCoords = downCoords;
            this.indices = indices;
            this.inData = inData;
            this.inVlens = inVlens;
            this.outData = outData;
            nData = inData.length;
        }

        @Override
        public void run()
        {
            for (int n = (iThread * nSrc) / nThreads; n < ((iThread + 1) * nSrc) / nThreads; n++) {
                int nn = n;
                for (int k = nBackward - 1; k >= 0; k--, nn += nSrc) {
                    computeAtPoint(downCoords[n][3 * k], downCoords[n][3 * k + 1], downCoords[n][3 * k + 2], nn);
                    indices[nn] = -k;
                }
                for (int k = 0; k < nForward; k++, nn += nSrc) {
                    computeAtPoint(upCoords[n][3 * k], upCoords[n][3 * k + 1], upCoords[n][3 * k + 2], nn);
                    indices[nn] = k;
                }
            }
        }

        private void computeAtPoint(float u, float v, float w, int nn)
        {
            int n0 = dims[0];
            int n1 = dims[0] * dims[1];
            int inexact = 0;
            if (u < 0)
                u = 0;
            if (u >= dims[0])
                u = dims[0] - 1;
            if (v < 0)
                v = 0;
            if (v >= dims[1])
                v = dims[1] - 1;
            if (w < 0)
                w = 0;
            if (w >= dims[2])
                w = dims[2] - 1;
            int i = (int) u;
            u -= i;
            if (u != 0 && i < dims[0] - 1)
                inexact += 1;
            int j = (int) v;
            v -= j;
            if (v != 0 && j < dims[1] - 1)
                inexact += 2;
            int k = (int) w;
            w -= k;
            if (w != 0 && k < dims[2] - 1)
                inexact += 4;
            int m = (dims[1] * k + j) * dims[0] + i;
            switch (inexact) {
                case 0:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp] = inData[var][vlen * m + cmp];
                    }
                    break;
                case 1:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = u * inData[var][vlen * (m + 1) + cmp] +
                                (1 - u) * inData[var][vlen * m + cmp];
                    }
                    break;
                case 2:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = v * inData[var][vlen * (m + n0) + cmp] +
                                (1 - v) * inData[var][vlen * m + cmp];
                    }
                    break;
                case 3:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = v * (u * inData[var][vlen * (m + n0 + 1) + cmp] +
                                (1 - u) * inData[var][vlen * (m + n0) + cmp]) +
                                (1 - v) * (u * inData[var][vlen * (m + 1) + cmp] +
                                (1 - u) * inData[var][vlen * m + cmp]);
                    }
                    break;
                case 4:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = w * inData[var][vlen * (m + n1) + cmp] +
                                (1 - w) * inData[var][vlen * m + cmp];
                    }
                    break;
                case 5:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = w * (u * inData[var][vlen * (m + n1 + 1) + cmp] +
                                (1 - u) * inData[var][vlen * (m + n1) + cmp]) +
                                (1 - w) * (u * inData[var][vlen * (m + 1) + cmp] +
                                (1 - u) * inData[var][vlen * m + cmp]);
                    }
                    break;
                case 6:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = w * (v * inData[var][vlen * (m + n1 + n0) + cmp] +
                                (1 - v) * inData[var][vlen * (m + n1) + cmp]) +
                                (1 - w) * (v * inData[var][vlen * (m + n0) + cmp] +
                                (1 - v) * inData[var][vlen * m + cmp]);
                    }
                    break;
                case 7:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = w * (v * (u * inData[var][vlen * (m + n1 + n0 + 1) + cmp] +
                                (1 - u) * inData[var][vlen * (m + n1 + n0) + cmp]) +
                                (1 - v) * (u * inData[var][vlen * (m + n1 + 1) + cmp] +
                                (1 - u) * inData[var][vlen * (m + n1) + cmp])) +
                                (1 - w) * (v * (u * inData[var][vlen * (m + n0 + 1) + cmp] +
                                (1 - u) * inData[var][vlen * (m + n0) + cmp]) +
                                (1 - v) * (u * inData[var][vlen * (m + 1) + cmp] +
                                (1 - u) * inData[var][vlen * m + cmp]));
                    }
                    break;
            }

        }

    }

    private static class InterpolateTo2DLines implements Runnable
    {

        int iThread;
        int nThreads;
        int[] dims;
        int nSrc;
        int nBackward;
        int nForward;
        float[][] upCoords;
        float[][] downCoords;
        int nData;
        int[] indices;
        float[][] inData;
        int[] inVlens;
        float[][] outData;

        public InterpolateTo2DLines(int iThread, int nThreads, int[] dims,
                                    int nSrc, int nBackward, int nForward,
                                    float[][] upCoords, float[][] downCoords, int[] indices,
                                    float[][] inData, int[] inVlens, float[][] outData)
        {
            this.iThread = iThread;
            this.nThreads = nThreads;
            this.dims = dims;
            this.nSrc = nSrc;
            this.nBackward = nBackward;
            this.nForward = nForward;
            this.upCoords = upCoords;
            this.downCoords = downCoords;
            this.indices = indices;
            this.inData = inData;
            this.inVlens = inVlens;
            this.outData = outData;
            nData = inData.length;
        }

        @Override
        public void run()
        {
            for (int n = (iThread * nSrc) / nThreads; n < ((iThread + 1) * nSrc) / nThreads; n++) {
                int nn = n;
                for (int k = nBackward - 1; k >= 0; k--, nn += nSrc) {
                    computeAtPoint(downCoords[n][2 * k], downCoords[n][2 * k + 1], nn);
                    indices[nn] = -k;
                }
                for (int k = 0; k < nForward; k++, nn += nSrc) {
                    computeAtPoint(upCoords[n][2 * k], upCoords[n][2 * k + 1], nn);
                    indices[nn] = k;
                }
            }
        }

        private void computeAtPoint(float u, float v, int nn)
        {
            int n0 = dims[0];
            int inexact = 0;
            if (u < 0)
                u = 0;
            if (u >= dims[0])
                u = dims[0] - 1;
            if (v < 0)
                v = 0;
            if (v >= dims[1])
                v = dims[1] - 1;
            int i = (int) u;
            u -= i;
            if (u != 0 && i < dims[0] - 1)
                inexact += 1;
            int j = (int) v;
            v -= j;
            if (v != 0 && j < dims[1] - 1)
                inexact += 2;
            int m = j * dims[0] + i;
            switch (inexact) {
                case 0:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp] = inData[var][vlen * m + cmp];
                    }
                    break;
                case 1:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = u * inData[var][vlen * (m + 1) + cmp] +
                                (1 - u) * inData[var][vlen * m + cmp];
                    }
                    break;
                case 2:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = v * inData[var][vlen * (m + n0) + cmp] +
                                (1 - v) * inData[var][vlen * m + cmp];
                    }
                    break;
                case 3:
                    for (int var = 0; var < nData; var++) {
                        int vlen = inVlens[var];
                        for (int cmp = 0; cmp < vlen; cmp++)
                            outData[var][vlen * nn + cmp]
                                = v * (u * inData[var][vlen * (m + n0 + 1) + cmp] +
                                (1 - u) * inData[var][vlen * (m + n0) + cmp]) +
                                (1 - v) * (u * inData[var][vlen * (m + 1) + cmp] +
                                (1 - u) * inData[var][vlen * m + cmp]);
                    }
                    break;
            }
        }
    }

    @Override
    public synchronized void updateStreamlines()
    {
        nForward = parameters.get(NUM_STEPS_FORWARD);
        nBackward = parameters.get(NUM_STEPS_BACKWARD);
        pullVectors();
        effectiveScale = parameters.get(STEP) * indexRadius / avgVector;
        LOGGER.info("avgVect = " + avgVector + " r = " + indexRadius + " scale = " + effectiveScale);
        downCoords = new float[nSrc][trueDim * nBackward];
        upCoords = new float[nSrc][trueDim * nForward];
        downVectors = new float[nSrc][trueDim * nBackward];
        upVectors = new float[nSrc][trueDim * nForward];
        Thread[] workThreads = new Thread[nThreads];
        for (int n = 0; n < nThreads; n++) {
            workThreads[n] = new Thread(new Streamline(n));
            workThreads[n].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        nvert = nSrc * (nBackward + nForward);
        lines = new int[2 * nSrc * (nBackward + nForward - 1)];
        coords = new float[3 * nvert];
        indices = new int[nvert];
        int nComp = inField.getNComponents();
        DataArray[] interpolatedComponents = new DataArray[nComp + 1];
        float[][] inData = new float[nComp + 1][];
        int[] inVlens = new int[nComp + 1];
        float[][] outData = new float[nComp + 1][];
        int nInterpolated = 0, nInterpolatedComponents = 0;
        for (int i = 0; i < inField.getNComponents(); i++)
            if (inField.getComponent(i).isNumeric()) {
                interpolatedComponents[nInterpolated] = inField.getComponent(i);
                inVlens[nInterpolated] = inField.getComponent(i).getVectorLength();
                if (inField.getComponent(i).getType() == DataArrayType.FIELD_DATA_FLOAT) {
                    inData[nInterpolated] = (float[]) inField.getComponent(i).getRawArray().getData();
                } else {
                    inData[nInterpolated] = inField.getComponent(i).getRawArray().getFloatData();
                }
                outData[nInterpolated] = new float[inVlens[nInterpolated] * nvert];
                nInterpolated += 1;
            }
        nInterpolatedComponents = nInterpolated;
        vectors = new float[trueDim * nvert];
        for (int i = 0; i < coords.length; i++)
            coords[i] = 0;
        if (fldCoords == null) {
            for (int iSrc = 0; iSrc < nSrc; iSrc++) {
                int j = iSrc;
                for (int k = nBackward - 1; k >= 0; k--, j += nSrc) {
                    float[] v = new float[trueDim];
                    float[] q = new float[trueDim];
                    for (int l = 0; l < trueDim; l++) {
                        coords[3 * j + l] = downCoords[iSrc][trueDim * k + l];
                        v[l] = downCoords[iSrc][trueDim * k + l] - affine[3][l];
                    }
                    for (int l = 0; l < trueDim; l++) {
                        q[l] = 0;
                        for (int m = 0; m < trueDim; m++)
                            q[l] += invAffine[m][l] * v[m];
                    }
                    System.arraycopy(q, 0, downCoords[iSrc], trueDim * k, trueDim);
                    indices[j] = -k;
                }
                for (int k = 0; k < nForward; k++, j += nSrc) {
                    float[] v = new float[trueDim];
                    float[] q = new float[trueDim];
                    for (int l = 0; l < trueDim; l++) {
                        coords[3 * j + l] = upCoords[iSrc][trueDim * k + l];
                        v[l] = upCoords[iSrc][trueDim * k + l] - affine[3][l];
                    }
                    for (int l = 0; l < trueDim; l++) {
                        q[l] = 0;
                        for (int m = 0; m < trueDim; m++)
                            q[l] += invAffine[m][l] * v[m];
                    }
                    System.arraycopy(q, 0, upCoords[iSrc], trueDim * k, trueDim);
                    indices[j] = k;
                }
            }
            workThreads = new Thread[nThreads];
            for (int n = 0; n < nThreads; n++) {
                if (dims.length == 3)
                    workThreads[n] = new Thread(new InterpolateTo3DLines(n, nThreads,
                                                                         dims, nSrc, nBackward, nForward,
                                                                         upCoords, downCoords, indices,
                                                                         inData, inVlens, outData));
                else
                    workThreads[n] = new Thread(new InterpolateTo2DLines(n, nThreads,
                                                                         dims, nSrc, nBackward, nForward,
                                                                         upCoords, downCoords, indices,
                                                                         inData, inVlens, outData));
                workThreads[n].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        } else {
            inVlens[nInterpolated] = 3;
            inData[nInterpolated] = fldCoords;
            outData[nInterpolated] = coords;
            nInterpolated += 1;
            workThreads = new Thread[nThreads];
            for (int n = 0; n < nThreads; n++) {
                if (dims.length == 3)
                    workThreads[n] = new Thread(new InterpolateTo3DLines(n, nThreads,
                                                                         dims, nSrc, nBackward, nForward,
                                                                         upCoords, downCoords, indices,
                                                                         inData, inVlens, outData));
                else
                    workThreads[n] = new Thread(new InterpolateTo2DLines(n, nThreads,
                                                                         dims, nSrc, nBackward, nForward,
                                                                         upCoords, downCoords, indices,
                                                                         inData, inVlens, outData));
                workThreads[n].start();
            }
            for (Thread workThread : workThreads)
                try {
                    workThread.join();
                } catch (InterruptedException e) {
                }
        }
        downCoords = null;
        upCoords = null;
        downVectors = null;
        upVectors = null;
        for (int i = 0, j = 0; i < nSrc; i++)
            for (int k = 0, l = i * (nBackward + nForward); k < nBackward + nForward - 1; k++, j += 2, l++) {
                lines[j] = i + k * nSrc;
                lines[j + 1] = lines[j] + nSrc;
            }
        byte[] edgeOrientations = new byte[lines.length / 2];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        outField = new IrregularField(nvert);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        DataArray da = DataArray.create(indices, 1, "steps");
        da.recomputeStatistics(true);
        outField.addComponent(da);
        CellArray streamLines = new CellArray(CellType.SEGMENT, lines, edgeOrientations, null);
        CellSet cellSet = new CellSet(inField.getName() + " " + inField.getComponent(parameters.get(COMPONENT)).getName() + " streamlines");
        cellSet.setBoundaryCellArray(streamLines);
        cellSet.setCellArray(streamLines);
        outField.addCellSet(cellSet);
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        for (int i = 0; i < nInterpolatedComponents; i++)
            outField.addComponent(DataArray.create(outData[i], inVlens[i],
                                                   interpolatedComponents[i].getName(),
                                                   interpolatedComponents[i].getUnit(),
                                                   interpolatedComponents[i].getUserData()));
    }

    private class Streamline implements Runnable
    {

        private final int nThread;
        private final RegularVectInterpolate vInt = new RegularVectInterpolate();

        public Streamline(int nThread)
        {
            this.nThread = nThread;
        }

        @Override
        public void run()
        {
            float[] fs = new float[trueDim];
            for (int n = nThread; n < nSrc; n += nThreads) {
                progressAgent.increase();
                if (mask != null && mask[n] == 0) {
                    for (int i = 0; i < downCoords[n].length; i += trueDim)
                        System.arraycopy(startCoords, 0, downCoords[n], i, trueDim);
                    for (int i = 0; i < upCoords[n].length; i += trueDim)
                        System.arraycopy(startCoords, 0, upCoords[n], i, trueDim);
                    continue;
                }
                try {
                    System.arraycopy(startCoords, 3 * n, fs, 0, fs.length);
                    fromSteps[n] = RungeKutta.fourthOrderRK(vInt, fs, effectiveScale, nBackward, -1, downCoords[n], downVectors[n]);
                    toSteps[n] = RungeKutta.fourthOrderRK(vInt, fs, effectiveScale, nForward, 1, upCoords[n], upVectors[n]);
                } catch (Exception e) {
                    LOGGER.error("null at " + n + " from " + nSrc);
                }
            }
        }
    }

    private class RegularVectInterpolate implements Deriv
    {

        @Override
        public float[] derivn(float[] y) throws Exception
        {
            float[] p = new float[y.length];
            float[] q = new float[3];
            for (int i = 0; i < y.length; i++)
                p[i] = y[i] - affine[3][i];
            for (int i = 0; i < dims.length; i++) {
                q[i] = 0;
                for (int j = 0; j < p.length; j++)
                    q[i] += invAffine[j][i] * p[j];
                if (q[i] < 0 || q[i] >= dims[i])
                    return null;
            }
            p = inField.getInterpolatedData(new FloatLargeArray(pullVects), q[0], q[1], q[2]);
            return p;
        }
    }

}
