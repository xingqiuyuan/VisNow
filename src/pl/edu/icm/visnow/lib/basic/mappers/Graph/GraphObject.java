//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Graph;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import javax.swing.event.ChangeEvent;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArraySchema;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.events.ColorEvent;
import pl.edu.icm.visnow.geometries.events.ColorListener;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.VisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNGeometryObject;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.events.MouseRestingEvent;
import pl.edu.icm.visnow.lib.utils.events.MouseRestingListener;
import pl.edu.icm.visnow.lib.utils.field.ValueRanges;
import pl.edu.icm.visnow.lib.utils.graphing.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GraphObject extends VisualizationModule

{
    
    protected GraphWorld graphWorld;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    protected GraphParams params;
    protected Field inField = null;
    protected boolean fromInput = true;
    protected boolean fromParams = false;
    protected float startTime = 0, endTime = 0;
    protected float[] minima, maxima;
    protected float[] mMinima, mMaxima;
    protected float[] pMinima, physMinima, coeffs;
    protected int[] exponents;
    
    protected PickListener pickListener = new PickListener(){
        @Override
        public void pickChanged(PickEvent e)
        {
            if (params.updateTop()) {
                params.setTopRightCorner(e.getScreenPickCoordinates()[0] / (float)outObj.getRenderingWindow().getWidth(), 
                                         e.getScreenPickCoordinates()[1] / (float)outObj.getRenderingWindow().getHeight());
                outObj.getRenderingWindow().refresh();
                ui.updatePositionSliders(params.getXPosition(), params.getGraphWidth(), params.getYPosition(), params.getGraphHeight());
                params.setUpdateTop(false);
            }
            else if (params.updateOrigin()) {
                params.setBottomLeftCorner(e.getScreenPickCoordinates()[0] / (float)outObj.getRenderingWindow().getWidth(),
                                           e.getScreenPickCoordinates()[1] / (float)outObj.getRenderingWindow().getHeight());
                outObj.getRenderingWindow().refresh();
                ui.updatePositionSliders(params.getXPosition(), params.getGraphWidth(), params.getYPosition(), params.getGraphHeight());
                params.setUpdateOrigin(false);
            }
        }
    };
    
    protected MouseRestingListener toolTipListener = new MouseRestingListener(){
        @Override
        public void mouseResting(MouseRestingEvent e)
        {
            params.setTooltipPosition(new int[]{e.getX(), e.getY()});
        }

        @Override
        public void stateChanged(ChangeEvent e)
        {
        }
    };

    @Override
    public MouseRestingListener getMouseRestingListener()
    {
        return toolTipListener;
    }
    
    public GraphObject()
    {
        parameters = params = new GraphParams();
        params.setTitleInFrame(true);
        graphWorld = new GraphWorld(params, false, true);
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) 
            {
                params.setWindowBgrColor(e.getSelectedColor());
            }
        };
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (inField == null)
                    return;
                if (outObj.getRenderingWindow() != null)
                    outObj.getRenderingWindow().refresh();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new GUI();
            }
        });
        ui.setParams(params);
        setPanel(ui);
    }
    
    protected void initValueRanges()
    {
        int nCmp = inField.getNComponents();
        minima = new float[nCmp];
        maxima = new float[nCmp];
        Arrays.fill(minima, Float.MAX_VALUE);
        Arrays.fill(maxima, -Float.MAX_VALUE);
        mMinima = new float[nCmp];
        mMaxima = new float[nCmp];
        pMinima = new float[nCmp];
        physMinima = new float[nCmp];
        coeffs = new float[nCmp];
        exponents = new int[nCmp];
    }
    
    public void updateTimeRange()
    {
        if (params.getDisplayedData() == null || params.getDisplayedData().length < 1)
            return;
        startTime =  Float.MAX_VALUE;
        endTime   = -Float.MAX_VALUE;
        DisplayedData[] displayedData = params.getDisplayedData();
        try {        
            for (int i = 0, j = 0; i < displayedData.length; i++) 
            if (displayedData[i].isDisplayed()) {
                int iData = displayedData[i].getIndex();
                DataArray da = inField.getComponent(iData);
                if (da.getStartTime() < startTime)
                    startTime = da.getStartTime();
                if (da.getEndTime() > endTime)
                    endTime = da.getEndTime();
            }
        } catch (ConcurrentModificationException e) {
        }
        if (startTime > endTime)
            startTime = endTime = 0;
    }
    
    protected void update()
    {
        outObj.clearGeometries2D();
        outObj.addGeometry2D(graphWorld);
        if (outObj.getRenderingWindow() != null)
            outObj.getRenderingWindow().refresh();
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj.addGeometry2D(graphWorld);
        outObj.addPickListener(pickListener);
        setOutputValue("outObj", new VNGeometryObject(outObj));
    }

    @Override
    public void onActive()
    {
        if (!fromParams) {
            fromInput = true;
            if (getInputFirstValue("inField") == null) {
                params.setDisplayedData(new DisplayedData[0]);
                return;
            }
            Field in = ((VNField) getInputFirstValue("inField")).getField();
            if (in == null || in.isEmpty())
                return;
            boolean updateUI = inField == null;
            if (!updateUI) {
                float nComp = in.getNComponents();
                if (nComp != inField.getNComponents())  
                    updateUI = true;
                else
                    for (int i = 0; i < nComp; i++) {
                        DataArraySchema s0 = in.getComponent(i).getSchema();
                        DataArraySchema s1 = inField.getComponent(i).getSchema();
                        if (s0.getType() != s1.getType() || !s0.getName().equals(s1.getName()) ||
                            s0.getVectorLength() != s1.getVectorLength()) {
                            updateUI = true;
                            break;
                        }
                    }
            }
            inField = in;
            if (inField instanceof RegularField && ((RegularField)in).getDims().length != 1)
                params.setDrawPointCloud(true);
            if (updateUI || params.getDisplayedData() == null || 
                            params.getDisplayedData().length == 0)
                ui.setInField(inField);
            
            initValueRanges();
            graphWorld.setInField(inField);
            ValueRanges.updateValueRanges(inField, minima, maxima, mMinima, mMaxima,
                                          pMinima, physMinima, coeffs, exponents);
            ValueRanges.updateExponentRanges(minima, maxima, mMinima, mMaxima, 
                                             pMinima, physMinima, coeffs, exponents);
            graphWorld.setInData(minima, maxima, mMinima, mMaxima,
                                 pMinima, physMinima, coeffs, exponents);
            fromInput = false;
        }
        fromParams = false;
        update();
        if (params.getArgument().isEmpty())
            params.setArgument("index");
    }
}
