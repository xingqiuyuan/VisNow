//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Glyphs;

import static org.apache.commons.math3.util.FastMath.*;

import java.awt.Color;
import javax.media.j3d.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Color3f;

import pl.edu.icm.jscic.DataContainerSchema;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;

import pl.edu.icm.visnow.datamaps.ColorMap;

import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;

import pl.edu.icm.visnow.geometries.geometryTemplates.Glyph;
import pl.edu.icm.visnow.geometries.geometryTemplates.ScalarGlyphTemplates;
import pl.edu.icm.visnow.geometries.geometryTemplates.VectorGlyphTemplates;
import pl.edu.icm.visnow.geometries.objects.generics.*;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.geometries.utils.ColorMapper;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEventListener;

import pl.edu.icm.visnow.lib.templates.visualization.modules.VisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;

import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;

import static pl.edu.icm.visnow.lib.basic.mappers.Glyphs.GlyphsShared.*;

/**
 * 
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 * 
 * Revisions above 566 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class Glyphs extends VisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected OpenBranchGroup outGroup = new OpenBranchGroup();
    
    protected Params params;
    
    protected Field inField = null;
    protected float lowv = 0;
    protected float dv = 0;
    protected boolean isValidity = false;
    protected LogicLargeArray valid = null;
    protected DataArray glyphDataArray, thresholdDataArray;
    protected boolean isThrRelative = false;
    protected float thr = -Float.MAX_VALUE;
    protected float[] baseCoords = null;
    protected float[] baseU = null;
    protected float[] baseV = null;
    protected float[] baseW = null;
    protected int nGlyphs, nstrip, nvert, nind, ncol;
    protected boolean isNormals = false;
    protected long[] glyphIn = null;
    protected int[] cIndex = null;
    protected int[] pIndex = null;
    protected int[] strips = null;
    protected float[] verts = null;
    protected float[] normals = null;
    protected byte[] colors = null;
    protected float[] uvData = null;
    
    protected Glyph gt = null;
    
    protected IndexedGeometryStripArray surf = null;
    
    protected OpenShape3D surfaces = new OpenShape3D();
    protected OpenAppearance appearance = new OpenAppearance();
    protected OpenTransparencyAttributes transparencyAttributes = new OpenTransparencyAttributes();
    protected OpenLineAttributes lattr = new OpenLineAttributes(1.f, OpenLineAttributes.PATTERN_SOLID, true);
    
    protected ColorMap colorMap = null;

    protected float lastTime = -Float.MAX_VALUE;
    protected int currentColorMode = -1;
    
    protected Texture2D texture = null;
    
    protected static final boolean[][] resetGeometry
        = {{false, true, true, true, true, true, true, true, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, false, false, false, false, false, false, false, true},
           {true, true, true, true, true, true, true, true, false}
        };

    private boolean fromUI = false;
    private boolean fromIn = false;
    
    protected GUI gui = null;
    
    public Glyphs()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (fromIn)
                    return;
                if (inField != null) {
                    fromUI = true;
                    update();
                    fromUI = false;
                }
            }
        });

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                
            }
        });
        
        appearance.setTransparencyAttributes(transparencyAttributes);
        appearance.getPolygonAttributes().setBackFaceNormalFlip(true);

        surfaces.setCapability(Shape3D.ENABLE_PICK_REPORTING);
        surfaces.setCapability(Shape3D.ALLOW_GEOMETRY_READ);
        surfaces.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
        surfaces.setCapability(Shape3D.ALLOW_APPEARANCE_READ);
        surfaces.setCapability(Shape3D.ALLOW_APPEARANCE_WRITE);
        surfaces.setCapability(Geometry.ALLOW_INTERSECT);
        surfaces.setCapability(Node.ALLOW_LOCAL_TO_VWORLD_READ);
        
        surfaces.setAppearance(appearance);
        
        outGroup.addChild(surfaces);
        
        outObj.addNode(outGroup);

        RenderEventListener renderListener = new RenderEventListener()
        {
            @Override
            public void renderExtentChanged(RenderEvent e)
            {
                if (inField != null) {
                    int extent = e.getUpdateExtent();
                    int cMode = dataMappingParams.getColorMode();
                    if (renderingParams.getDisplayMode() == RenderingParams.BACKGROUND)
                        cMode = DataMappingParams.UNCOLORED;
                    if (currentColorMode < 0) {
                        updateGeometry();
                        currentColorMode = cMode;
                        return;
                    }
                    if (extent == RenderEvent.COLORS || extent == RenderEvent.TRANSPARENCY || extent == RenderEvent.TEXTURE) {
                        if (resetGeometry[currentColorMode][cMode])
                            updateGeometry();
                        else
                            updateColors();
                        currentColorMode = cMode;
                        return;
                    }
                    if (extent == RenderEvent.COORDS)
                        updateCoords();
                    if (extent == RenderEvent.GEOMETRY)
                        updateGeometry();
                    currentColorMode = cMode;
                }
            }
        };
        dataMappingParams.addRenderEventListener(renderListener);
        renderingParams.addRenderEventListener(renderListener);

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                gui = new GUI();
                gui.setParams(params);
                setPanel(gui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(COMPONENT, 0),
            new Parameter<>(TYPE, 0),

            new Parameter<>(CROP_LOW, new int[]{0, 0, 0}),
            new Parameter<>(CROP_UP, new int[]{1, 1, 1}),
            new Parameter<>(DOWN, new int[]{3, 3, 3}),
            new Parameter<>(DOWNSIZE, 10),
            new Parameter<>(THRESHOLD, 0.1f),
            new Parameter<>(THRESHOLD_COMPONENT, -1),
            new Parameter<>(THRESHOLD_RELATIVE, false), 
            new Parameter<>(CONSTANT_DIAMETER, false),
            new Parameter<>(CONSTANT_THICKNESS, false),
            new Parameter<>(USE_ABS, true),
            new Parameter<>(USE_SQRT, false),
            new Parameter<>(SCALE, 0.1f),
            new Parameter<>(THICKNESS, 0.1f),
            new Parameter<>(LINE_THICKNESS, 0.1f),
            new Parameter<>(TRANSPARENCY, 0.0f),
            new Parameter<>(LEVEL_OF_DETAIL, 1)     
        };
    }
    
    private boolean isValid(long i)
    {
        if (isValidity && !valid.getBoolean(i))
            return false;
        double d = 0;
        double t;
        if (thresholdDataArray == null)
            return true;
        if (glyphDataArray != null && isThrRelative)
            if (glyphDataArray.getVectorLength() == 1)
                d = glyphDataArray.getFloatElement(i)[0];
            else {
                float[] v = glyphDataArray.getFloatElement(i);
                d = 0;
                for (int j = 0; j < v.length; j++)
                    d += v[j] * v[j];
                d = sqrt(d);
            }
        if (thresholdDataArray.getVectorLength() == 1)
            t = thresholdDataArray.getFloatElement(i)[0];
        else {
            float[] v = thresholdDataArray.getFloatElement(i);
            t = 0;
            for (int j = 0; j < v.length; j++)
                t += v[j] * v[j];
            t = sqrt(t);
        }
        if (isThrRelative)
            return d > thr * t;
        else
            return t > thr;
    }

    private void prepareLocalCoords()
    {
        if (glyphDataArray == null)
            return;
        float[] um = {0, 0, 0};
        float[] vm = {0, 0, 0};
        float[] wm = {0, 0, 0};

        baseU = new float[3 * nGlyphs];
        baseV = new float[3 * nGlyphs];
        baseW = new float[3 * nGlyphs];
        //float[] p = {0, 0, 0};
        int vlen = glyphDataArray.getVectorLength();
        for (int i = 0; i < nGlyphs; i++) {
            float[] p = glyphDataArray.getFloatElement(glyphIn[i]);
            
            float pn = 0;
            for (int j = 0; j < p.length; j++)
                pn += p[j] * p[j];
            if (vlen == 1 || vlen > 3 || pn < 1e-20) {
                for (int j = 0; j < 3; j++)
                    baseU[3 * i + j] = baseV[3 * i + j] = baseW[3 * i + j] = 0;
                baseU[3 * i] = baseV[3 * i + 1] = baseW[3 * i + 2] = 1;
                continue;
            }
            pn = (float) sqrt(pn);
            for (int j = 0; j < p.length; j++)
                um[j] = p[j] / pn;
            if (abs(um[0]) > abs(um[1]) && abs(um[0]) > abs(um[2])) {
                vm[0] = vm[1] = 0;
                vm[2] = 1;
            } else {
                vm[1] = vm[2] = 0;
                vm[0] = 1;
            }
            wm[0] = um[1] * vm[2] - um[2] * vm[1];
            wm[1] = um[2] * vm[0] - um[0] * vm[2];
            wm[2] = um[0] * vm[1] - um[1] * vm[0];
            pn = (float) sqrt(wm[0] * wm[0] + wm[1] * wm[1] + wm[2] * wm[2]);
            for (int j = 0; j < 3; j++)
                wm[j] /= pn;
            vm[0] = um[2] * wm[1] - um[1] * wm[2];
            vm[1] = um[0] * wm[2] - um[2] * wm[0];
            vm[2] = um[1] * wm[0] - um[0] * wm[1];
            System.arraycopy(um, 0, baseW, 3 * i, 3);
            System.arraycopy(vm, 0, baseV, 3 * i, 3);
            System.arraycopy(wm, 0, baseU, 3 * i, 3);
        }
    }

    private void prepareGlyphCount()
    {
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 3)
            return;
        thr = params.getThr();
        thresholdDataArray = null;
        if (params.getThrComponent() >= 0)
            thresholdDataArray = inField.getComponent(params.getThrComponent());
        int maxNGlyphs = 1;
        if (inField instanceof RegularField) {
            int[] low = params.getLowCrop();
            int[] up = params.getUpCrop();
            int[] down = params.getDown();
            for (int i = 0; i < ((RegularField) inField).getDimNum(); i++)
                maxNGlyphs *= (up[i] - low[i]) / down[i] + 1;
        } else
            maxNGlyphs = (int) (inField.getNNodes() / params.getDownsize() + 1);
        long[] gl = new long[maxNGlyphs];
        for (int i = 0; i < gl.length; i++)
            gl[i] = -1;
        nGlyphs = 0;
        surf = null;
        if (inField instanceof RegularField) {
            RegularField inRegularField = (RegularField) inField;
            if (inRegularField.getLDims() == null)
                return;
            long[] dims = inRegularField.getLDims();
            int[] low = params.getLowCrop();
            int[] up = params.getUpCrop();
            int[] down = params.getDown();
            long l;
            switch (dims.length) {
                case 3:
                    for (long i = low[2]; i < up[2]; i += down[2])
                        for (long j = low[1]; j < up[1]; j += down[1]) {
                            l = (dims[1] * i + j) * dims[0] + low[0];
                            for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                                if (isValid(l)) {
                                    gl[nGlyphs] = l;
                                    nGlyphs += 1;
                                }
                        }
                    break;
                case 2:
                    for (long j = low[1]; j < up[1]; j += down[1]) {
                        l = j * dims[0] + low[0];
                        for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                            if (isValid(l)) {
                                gl[nGlyphs] = l;
                                nGlyphs += 1;
                            }
                    }
                    break;
                case 1:
                    l = low[0];
                    for (long k = low[0]; k < up[0]; k += down[0], l += down[0])
                        if (isValid(l)) {
                            gl[nGlyphs] = l;
                            nGlyphs += 1;
                        }
            }
        } else {
            int downsize = params.getDownsize();
            for (long i = 0; i < inField.getNNodes(); i += downsize)
                if (isValid(i)) {
                    gl[nGlyphs] = i;
                    nGlyphs += 1;
                }
        }
        glyphIn = new long[nGlyphs];
        System.arraycopy(gl, 0, glyphIn, 0, nGlyphs);
        colors = new byte[3 * nGlyphs];
        baseCoords = new float[3 * nGlyphs];
        updateBaseCoords();
    }

    private void updateBaseCoords()
    {
        for (int i = 0; i < baseCoords.length; i++)
            baseCoords[i] = 0;
        if (inField.getCurrentCoords() != null) {
            FloatLargeArray fldCoords = inField.getCurrentCoords();
            for (int i = 0; i < nGlyphs; i++)
                for (int j = 0; j < 3; j++)
                    baseCoords[3 * i + j] = fldCoords.get(3 * glyphIn[i] + j);
        } else if (inField instanceof RegularField) {
            float[][] inAff = ((RegularField) inField).getAffine();
            long[] dims = ((RegularField) inField).getLDims();
            int i0 = 0, i1 = 0, i2 = 0;
            for (int i = 0; i < nGlyphs; i++) {
                long j = glyphIn[i];
                i0 = (int) (j % dims[0]);
                if (dims.length > 1) {
                    j /= dims[0];
                    i1 = (int) (j % dims[1]);
                    if (dims.length > 2)
                        i2 = (int) (j / dims[1]);
                }
                for (int k = 0; k < 3; k++)
                    baseCoords[3 * i + k] = inAff[3][k] + i0 * inAff[0][k] + i1 * inAff[1][k] + i2 * inAff[2][k];
            }
        }
    }

    public void updateColors()
    {
        int[] glyphIndices = new int[glyphIn.length];
        for (int i = 0; i < glyphIndices.length; i++) {
            glyphIndices[i] = (int)glyphIn[i];
        }
        colors = ColorMapper.mapColorsIndexed(inField, dataMappingParams, glyphIndices, renderingParams.getDiffuseColor(), colors);
        if (inField.getComponent(dataMappingParams.getTransparencyParams().getComponentRange().getComponentName()) == null && 
            params.getTransparency() == 0) {
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
        }
        surf.setColors(0, colors);
    }

    public void updateGeometry()
    {
        surfaces.removeAllGeometries();
        surf = null;

        int cMode = dataMappingParams.getColorMode();
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND)
            cMode = DataMappingParams.UNCOLORED;
        generateGlyphs(cMode);
        surfaces.addGeometry(surf);
        outObj.setExtents(inField.getPreferredExtents());

        appearance.setUserData(this);
        appearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());
        if (appearance.getMaterial() != null) {
            appearance.getMaterial().setAmbientColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setDiffuseColor(dataMappingParams.getDefaultColor());
            appearance.getMaterial().setSpecularColor(dataMappingParams.getDefaultColor());
        }

        Color bgrColor2 = renderingParams.getBackgroundColor();
        float[] bgrColorComps = new float[3];
        bgrColor2.getColorComponents(bgrColorComps);
        if (renderingParams.getShadingMode() == RenderingParams.BACKGROUND)
            appearance.getColoringAttributes().setColor(new Color3f(bgrColorComps[0], bgrColorComps[1], bgrColorComps[2]));
        else
            appearance.getColoringAttributes().setColor(renderingParams.getDiffuseColor());

        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
    }

    private void generateGlyphs(int cMode)
    {
        if (nGlyphs < 1)
            return;
        glyphDataArray = inField.getComponent(params.getComponent());
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 3)
            return;
        int type = params.getType();
        int lod = params.getLod();

        if (glyphDataArray == null || glyphDataArray.getVectorLength() == 1)
            gt = ScalarGlyphTemplates.glyph(type, lod);
        else
            gt = VectorGlyphTemplates.glyph(type, lod);
        nstrip = nGlyphs * gt.getNstrips();
        nvert = nGlyphs * gt.getNverts();
        nind = nGlyphs * gt.getNinds();
        ncol = nGlyphs;
        strips = new int[nstrip];
        verts = new float[3 * nvert];
        normals = new float[3 * nvert];
        pIndex = new int[nind];
        cIndex = new int[nind];
        makeIndices();
        if (glyphDataArray != null && glyphDataArray.getVectorLength() > 1)
            prepareLocalCoords();
        if (verts == null || verts.length != 3 * nGlyphs * gt.getNverts())
            verts = new float[3 * nGlyphs * gt.getNverts()];

        int verticesMode = GeometryArray.COORDINATES;
        isNormals = false;
        if (gt.getType() != Glyph.LINE_STRIPS) {
            verticesMode |= GeometryArray.NORMALS;
            isNormals = true;
        }
        if (cMode == DataMappingParams.UVTEXTURED)
            verticesMode |= GeometryArray.TEXTURE_COORDINATE_2;
        else
            verticesMode |= GeometryArray.COLOR_4;

        if (isNormals && (normals == null || normals.length != 3 * nGlyphs * gt.getNverts()))
            normals = new float[3 * nGlyphs * gt.getNverts()];

        switch (gt.getType()) {
            case Glyph.TRIANGLE_STRIPS:
                surf = new IndexedTriangleStripArray(nvert, verticesMode, nind, strips);
                break;
            case Glyph.TRIANGLE_FANS:
                surf = new IndexedTriangleFanArray(nvert, verticesMode, nind, strips);
                break;
            case Glyph.LINE_STRIPS:
                surf = new IndexedLineStripArray(nvert, verticesMode, nind, strips);
                break;
        }
        surf.setCapability(IndexedLineStripArray.ALLOW_COUNT_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_FORMAT_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_INDEX_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_READ);
        surf.setCapability(IndexedLineStripArray.ALLOW_COORDINATE_WRITE);
        if (cMode == DataMappingParams.UVTEXTURED) {
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_WRITE);
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_INDEX_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_TEXCOORD_INDEX_WRITE);
        } else {
            surf.setCapability(IndexedLineStripArray.ALLOW_COLOR_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_COLOR_WRITE);
            surf.setColorIndices(0, cIndex);
        }
        surf.setCoordinates(0, verts);
        surf.setCoordinateIndices(0, pIndex);
        if (isNormals) {
            surf.setCapability(IndexedLineStripArray.ALLOW_NORMAL_READ);
            surf.setCapability(IndexedLineStripArray.ALLOW_NORMAL_WRITE);
            surf.setNormals(0, normals);
            surf.setNormalIndices(0, pIndex);
        }
        OpenMaterial mat = new OpenMaterial();
        mat.setShininess(15.f);
        mat.setColorTarget(OpenMaterial.AMBIENT_AND_DIFFUSE);
        appearance.setMaterial(mat);
        PolygonAttributes pattr = new PolygonAttributes(
            PolygonAttributes.POLYGON_FILL,
            PolygonAttributes.CULL_NONE, 0.f, true);
        appearance.setPolygonAttributes(pattr);
        OpenColoringAttributes colAttrs = new OpenColoringAttributes();
        colAttrs.setColor(renderingParams.getDiffuseColor());
        appearance.setColoringAttributes(colAttrs);
        appearance.setLineAttributes(lattr);
        updateCoords();
    }

    private void updateCoords()
    {
        lattr.setLineWidth(params.getLineThickness());
        float scale = params.getScale();
        float s = 0;
        float st = 0;
        boolean useAbs = params.isUseAbs();
        boolean useSqrt = params.isUseSqrt();
        float[] tVerts = gt.getVerts();
        float[] tNorms = gt.getNormals();
        float[] p = new float[3];
        float[] u = new float[3];
        float[] v = new float[3];
        float[] w = new float[3];
        if (glyphDataArray == null) {
            s = scale;
            for (int i = 0, k = 0; i < nGlyphs; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                if (isNormals)
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++) {
                            verts[k] = p[l] + s * tVerts[m];
                            normals[k] = tNorms[m];
                        }
                else
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++)
                            verts[k] = p[l] + s * tVerts[m];
            }
        } else if (glyphDataArray.getVectorLength() == 1)
            for (int i = 0, k = 0; i < nGlyphs; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                if (params.isConstantDiam())
                    s = scale;
                else {
                    s = glyphDataArray.getFloatElement(glyphIn[i])[0];
                    if (useAbs || useSqrt)
                        s = abs(s);
                    if (useSqrt)
                        s = (float) sqrt(s);
                    s *= scale;
                }
                if (isNormals)
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++) {
                            verts[k] = p[l] + s * tVerts[m];
                            normals[k] = tNorms[m];
                        }
                else
                    for (int j = 0, m = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++, m++)
                            verts[k] = p[l] + s * tVerts[m];
            }
        else
            for (int i = 0, k = 0; i < nGlyphs; i++) {
                System.arraycopy(baseCoords, 3 * i, p, 0, 3);
                System.arraycopy(baseU, 3 * i, u, 0, 3);
                System.arraycopy(baseV, 3 * i, v, 0, 3);
                System.arraycopy(baseW, 3 * i, w, 0, 3);
                float[] vs = glyphDataArray.getFloatElement(glyphIn[i]);
                if (params.isConstantDiam())
                    s = scale;
                else {
                    s = 0;
                    for (int j = 0; j < vs.length; j++)
                        s += vs[j] * vs[j];
                    s = (float) sqrt(s);
                    if (useSqrt)
                        s = (float) sqrt(s);
                    s *= scale;
                }
                if (params.isConstantThickness())
                    st = params.getThickness();
                else
                    st = s;
                if (isNormals)
                    for (int j = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++) {
                            verts[k] = p[l] + st * (tVerts[3 * j] * u[l] + tVerts[3 * j + 1] * v[l]) + s * tVerts[3 * j + 2] * w[l];
                            normals[k] = tNorms[3 * j] * u[l] + tNorms[3 * j + 1] * v[l] + tNorms[3 * j + 2] * w[l];
                        }
                else
                    for (int j = 0; j < tVerts.length / 3; j++)
                        for (int l = 0; l < 3; l++, k++)
                            verts[k] = p[l] + st * (tVerts[3 * j] * u[l] + tVerts[3 * j + 1] * v[l]) + s * tVerts[3 * j + 2] * w[l];
            }
        surf.setCoordinates(0, verts);
        if (isNormals)
            surf.setNormals(0, normals);
    }

    public void update()
    {
        if (params.getChange() == Params.GEOMETRY_CHANGED) {
            prepareGlyphCount();
            currentColorMode = dataMappingParams.getColorMode();
        }
        if (nGlyphs < 1)
            return;
        if (params.getChange() >= Params.GLYPHS_CHANGED)
            updateGeometry();
        if (params.getChange() >= Params.COORDS_CHANGED)
            updateCoords();
        if (dataMappingParams.getColorMode() != DataMappingParams.UVTEXTURED)
            updateColors();
        appearance.getTransparencyAttributes().setTransparency(params.getTransparency());
        if (params.getTransparency() == 0)
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NONE);
        else
            appearance.getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NICEST);
        params.setChange(0);

        outObj.clearAllGeometry();
        outObj.addNode(outGroup);
        outObj.setExtents(inField.getPreferredExtents());
    }

    protected void makeIndices()
    {
        int istrip = 0, iind = 0, ivert = 0, icol = 0;
        for (int n = 0; n < nGlyphs; n++) {
            for (int i = 0; i < gt.getNstrips(); i++, istrip++)
                strips[istrip] = gt.getStrips()[i];
            for (int i = 0; i < gt.getNinds(); i++, iind++) {
                pIndex[iind] = ivert + gt.getPntsIndex()[i];
                cIndex[iind] = icol;
            }
            ivert += gt.getNverts();
            icol += 1;
        }
    }

    @Override
    public void onActive()
    {
        if (!fromUI) {
            fromIn = true;
            if (getInputFirstValue("inField") == null)
                return;
            Field inFld = ((VNField) getInputFirstValue("inField")).getField();
            if (inFld == null)
                return;
            if (inField != inFld) {
                inField = inFld;
                outObj.setExtents(inField.getPreferredExtents());
                lastTime = inField.getCurrentTime();
                dataMappingParams.setInData(inFld, (DataContainerSchema)null);
                gui.setInData(inField, dataMappingParams);
                isValidity = inField.hasMask();
                valid = inField.getCurrentMask();
                params.setChange(Params.GEOMETRY_CHANGED);
            } else {
                lastTime = inField.getCurrentTime();
                isValidity = inField.hasMask();
                valid = inField.getCurrentMask();
                params.setChange(Params.GEOMETRY_CHANGED);
            }
            fromIn = false;
        }
        update();
        fromUI = false;
    }
}
