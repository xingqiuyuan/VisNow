//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.DiffusionStream;

import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.visnow.lib.utils.numeric.ODE.Deriv;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.utils.MatrixMath;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.lib.utils.numeric.ODE.RandomizedRungeKutta;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class ComputeRegularFieldStreamlines extends ComputeStreamlines
{

    private RegularField inField = null;
    private int[] dims = null;
    private float[][] affine = null;
    private float[][] invAffine = null;
    private float[] fldCoords = null;
    private float[] pullVects = null;

    /**
     * Creates a new instance of TextureVolRender
     *
     * @param inField: a field containing a suitable vector component
     * @param params
     */
    public ComputeRegularFieldStreamlines(RegularField inField, Params params)
    {
        super(inField, params);
        nThreads = params.getNThreads();
        this.inField = inField;
        dims = inField.getDims();
        if (dims.length == 2)
            nSp = 2;
        affine = inField.getAffine();
        fldCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        if (fldCoords == null)
            invAffine = inField.getInvAffine();
        else {
            invAffine = new float[4][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 4; j++)
                    invAffine[j][i] = 0;
                invAffine[i][i] = 1;
            }
        }
    }

    @Override
    public void setStartPoints(Field startPoints)
    {
        nSrc = (int) startPoints.getNNodes();
        float[] st = startPoints.getCurrentCoords() == null ? null : startPoints.getCurrentCoords().getData();
        startCoords = st;
        if (inField.getCurrentCoords() != null && inField.getDims().length == 3) {
            if (inField.getGeoTree() == null)
                inField.createGeoTree();
            for (int i = 0; i < (int) startPoints.getNNodes(); i++) {
                float[] p = inField.getFloatIndices(st[3 * i], st[3 * i + 1], st[3 * i + 2]);
                System.arraycopy(p, 0, startCoords, 3 * i, 3);
            }
        }
        toSteps = new int[nSrc];
    }

    public synchronized void pullVectors()
    {
        vects = inField.getComponent(params.getVectorComponent()).getRawFloatArray().getData();
        if (fldCoords == null) {
            pullVects = new float[vects.length];
            float[][] iaf = inField.getInvAffine();
            int vl = inField.getComponent(params.getVectorComponent()).getVectorLength();
            for (int i = 0; i < vects.length; i += vl)
                for (int k = 0; k < vl; k++) {
                    pullVects[i + k] = 0;
                    for (int l = 0; l < vl; l++)
                        pullVects[i + k] += iaf[k][l] * vects[i + l];
                }
        } else
            pullVects = MatrixMath.pullVectorField(dims, fldCoords, vects);
    }

    @Override
    public synchronized void updateStreamlines()
    {
        nForward = params.getNForwardSteps();
        pullVectors();
        streamlineCoords = new float[nSrc][nSp * nForward];
        velocityVectors = new float[nSrc][nSp * nForward];
        nThreads = params.getNThreads();
        Thread[] workThreads = new Thread[nThreads];
        threadProgress = new int[nThreads];
        for (int i = 0; i < nThreads; i++)
            threadProgress[i] = 0;
        for (int n = 0; n < nThreads; n++) {
            workThreads[n] = new Thread(new Streamline(n));
            workThreads[n].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception e) {
            }
        if (params.computeDiffusion())
            return;
        nvert = nSrc * nForward;
        lines = new int[2 * nSrc * (nForward - 1)];
        coords = new float[3 * nvert];
        indices = new int[nvert];
        vectors = new float[3 * nvert];
        for (int i = 0; i < coords.length; i++)
            coords[i] = 0;
        if (fldCoords == null) {
            for (int i = 0; i < nSrc; i++) {
                int j = i;
                for (int k = 0; k < nForward; k++, j += nSrc) {
                    for (int l = 0; l < nSp; l++) {
                        coords[3 * j + l] = streamlineCoords[i][nSp * k + l];
                        vectors[3 * j + l] = velocityVectors[i][nSp * k + l];
                    }
                    indices[j] = k;
                }
            }
        } else {
            float u, v, w;
            int i, j, k, l, m, n0 = 3 * dims[0], n1 = 3 * dims[0] * dims[1];
            int inexact;
            for (int n = 0; n < nSrc; n++) {
                int nn = n;

                for (int kk = 0; kk < nForward; kk++, nn += nSrc) {
                    inexact = 0;
                    u = streamlineCoords[n][3 * kk];
                    if (u < 0)
                        u = 0;
                    if (u >= dims[0])
                        u = dims[0] - 1;
                    v = streamlineCoords[n][3 * kk + 1];
                    if (v < 0)
                        v = 0;
                    if (v >= dims[1])
                        v = dims[1] - 1;
                    w = streamlineCoords[n][3 * kk + 2];
                    if (w < 0)
                        w = 0;
                    if (w >= dims[2])
                        w = dims[2] - 1;
                    i = (int) u;
                    u -= i;
                    if (u != 0 && i < dims[0] - 1)
                        inexact += 1;
                    j = (int) v;
                    v -= j;
                    if (v != 0 && j < dims[1] - 1)
                        inexact += 2;
                    k = (int) w;
                    w -= k;
                    if (w != 0 && k < dims[2] - 1)
                        inexact += 4;
                    m = 3 * ((dims[1] * k + j) * dims[0] + i);
                    switch (inexact) {
                        case 0:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = fldCoords[m + l];
                                vectors[3 * nn + l] = vects[m + l];
                            }
                            break;
                        case 1:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = u * fldCoords[m + l + 3] + (1 - u) * fldCoords[m + l];
                                vectors[3 * nn + l] = u * vects[m + l + 3] + (1 - u) * vects[m + l];
                            }
                            break;
                        case 2:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = v * fldCoords[m + l + n0] + (1 - v) * fldCoords[m + l];
                                vectors[3 * nn + l] = v * vects[m + l + n0] + (1 - v) * vects[m + l];
                            }
                            break;
                        case 3:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = v * (u * fldCoords[m + l + n0 + 3] + (1 - u) * fldCoords[m + l + n0]) +
                                    (1 - v) * (u * fldCoords[m + l + 3] + (1 - u) * fldCoords[m + l]);
                                vectors[3 * nn + l] = v * (u * vects[m + l + n0 + 3] + (1 - u) * vects[m + l + n0]) +
                                    (1 - v) * (u * vects[m + l + 3] + (1 - u) * vects[m + l]);
                            }
                            break;
                        case 4:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = w * fldCoords[m + l + n1] + (1 - w) * fldCoords[m + l];
                                vectors[3 * nn + l] = w * vects[m + l + n1] + (1 - w) * vects[m + l];
                            }
                            break;
                        case 5:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = w * (u * fldCoords[m + l + n1 + 3] + (1 - u) * fldCoords[m + l + n1]) +
                                    (1 - w) * (u * fldCoords[m + l + 3] + (1 - u) * fldCoords[m + l]);
                                vectors[3 * nn + l] = w * (u * vects[m + l + n1 + 3] + (1 - u) * vects[m + l + n1]) +
                                    (1 - w) * (u * vects[m + l + 3] + (1 - u) * vects[m + l]);
                            }
                            break;
                        case 6:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = w * (v * fldCoords[m + l + n1 + n0] + (1 - v) * fldCoords[m + l + n1]) +
                                    (1 - w) * (v * fldCoords[m + l + n0] + (1 - v) * fldCoords[m + l]);
                                vectors[3 * nn + l] = w * (v * vects[m + l + n1 + n0] + (1 - v) * vects[m + l + n1]) +
                                    (1 - w) * (v * vects[m + l + n0] + (1 - v) * vects[m + l]);
                            }
                            break;
                        case 7:
                            for (l = 0; l < 3; l++) {
                                coords[3 * nn + l] = w * (v * (u * fldCoords[m + l + n1 + n0 + 3] + (1 - u) * fldCoords[m + l + n1 + n0]) +
                                    (1 - v) * (u * fldCoords[m + l + n1 + 3] + (1 - u) * fldCoords[m + l + n1])) +
                                    (1 - w) * (v * (u * fldCoords[m + l + n0 + 3] + (1 - u) * fldCoords[m + l + n0]) +
                                    (1 - v) * (u * fldCoords[m + l + 3] + (1 - u) * fldCoords[m + l]));
                                vectors[3 * nn + l] = w * (v * (u * vects[m + l + n1 + n0 + 3] + (1 - u) * vects[m + l + n1 + n0]) +
                                    (1 - v) * (u * vects[m + l + n1 + 3] + (1 - u) * vects[m + l + n1])) +
                                    (1 - w) * (v * (u * vects[m + l + n0 + 3] + (1 - u) * vects[m + l + n0]) +
                                    (1 - v) * (u * vects[m + l + 3] + (1 - u) * vects[m + l]));
                            }
                            break;
                    }
                    indices[nn] = -kk;
                }
            }
        }
        streamlineCoords = null;
        velocityVectors = null;
        for (int i = 0, j = 0; i < nSrc; i++)
            for (int k = 0, l = i * nForward; k < nForward - 1; k++, j += 2, l++) {
                lines[j] = i + k * nSrc;
                lines[j + 1] = lines[j] + nSrc;
            }
        byte[] edgeOrientations = new byte[lines.length / 2];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        outField = new IrregularField(nvert);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        DataArray da = DataArray.create(indices, 1, "steps");
        da.setPreferredRange(0, nForward);
        outField.addComponent(da);
        DataArray dn = DataArray.create(vectors, 3, "vectors");
        outField.addComponent(dn);
        CellArray streamLines = new CellArray(CellType.SEGMENT, lines, edgeOrientations, null);
        CellSet cellSet = new CellSet(inField.getName() + " " + inField.getComponent(params.getVectorComponent()).getName() + " streamlines");
        cellSet.setBoundaryCellArray(streamLines);
        cellSet.setCellArray(streamLines);
        outField.addCellSet(cellSet);
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
    }

    private class Streamline implements Runnable
    {

        private final int nThread;
        private final RegularVectInterpolate vInt = new RegularVectInterpolate();

        public Streamline(int nThread)
        {
            this.nThread = nThread;
        }

        @Override
        public void run()
        {
            float[] fs = new float[nSp];

            for (int n = nThread; n < nSrc; n += nThreads) {
                if (nThread == 0)
                    fireStatusChanged((float) n / nSrc);
                try {
                    System.arraycopy(startCoords, 3 * n, fs, 0, fs.length);
                    toSteps[n] = RandomizedRungeKutta.fourthOrderRK(vInt, fs, params.getDiffCoeff(), params.getStep(), nForward, streamlineCoords[n], velocityVectors[n]);

                } catch (Exception e) {
                    System.out.println("null at " + n + " from " + nSrc);
                }
            }
        }
    }

    private class RegularVectInterpolate implements Deriv
    {

        @Override
        public float[] derivn(float[] y) throws Exception
        {
            float[] p = new float[y.length];
            float[] q = new float[3];
            for (int i = 0; i < y.length; i++)
                p[i] = y[i] - affine[3][i];
            for (int i = 0; i < dims.length; i++) {
                q[i] = 0;
                for (int j = 0; j < p.length; j++)
                    q[i] += invAffine[j][i] * p[j];
                if (q[i] < 0 || q[i] >= dims[i])
                    return null;
            }
            p = inField.getInterpolatedData(new FloatLargeArray(pullVects), q[0], q[1], q[2]);
            return p;
        }
    }

}
