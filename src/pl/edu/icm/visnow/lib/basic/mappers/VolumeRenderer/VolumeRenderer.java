//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.VolumeRenderer;

import pl.edu.icm.visnow.geometries.utils.TextureVolumeRenderer;
import java.util.ArrayList;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Vector3f;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.DataContainerSchema;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.LinkFace;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.geometries.events.ColorListener;
import pl.edu.icm.visnow.geometries.events.ProjectionEvent;
import pl.edu.icm.visnow.geometries.events.ProjectionListener;
import pl.edu.icm.visnow.geometries.objects.ColormapLegend;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickType;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.FrameRenderedListener;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import pl.edu.icm.visnow.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickEvent;
import pl.edu.icm.visnow.lib.basic.mappers.VolumeRenderer.FieldPick.FieldPickListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.VisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import static pl.edu.icm.jscic.utils.CropDownUtils.*;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.events.LightDirectionEvent;
import pl.edu.icm.visnow.geometries.events.LightDirectionListener;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.parameters.VolumeShadingParams;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class VolumeRenderer extends VisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(VolumeRenderer.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    public static String helpTopicID = null;
    protected TextureVolumeRenderer volRender;
    protected GUI ui = null;
    protected Params params = new Params();
    private VolumeShadingParams shadingParams = params.getShadingParams();
    protected RegularField inField;

    protected int[] inDims = null;
    protected float[][] af = null;
    protected float[][] ia = null;

    protected float[][] phys_exts = null;
    protected ColormapLegend colormapLegend = outObj.getColormapLegend();
    protected RegularField outField = null;
    protected boolean outCroppedField = false;
    protected boolean needPowerOf2Textures = true;
    protected LocalToWindow locToWin = null;
    private boolean ignoreUI = false;

    protected float[] picked_world_coords = new float[3];
    protected int[] picked_index = null;
    protected float[] picked_physical_coords = new float[3];

    public VolumeRenderer()
    {
        this(true);
    }

    public VolumeRenderer(boolean lendUI)
    {
        super();
        parameters = params;
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new GUI();
            }
        });
        dataMappingParams.getTransparencyParams().getComponentRange().setAddNull(false);
        params.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                outCroppedField = true;
                startAction();
            }
        });

        ui.setParams(params);
        ui.setDataMappingParams(dataMappingParams);
        ui.getCropUI().addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (ignoreUI)
                    return;
                outObj.detach();
                if (volRender != null)
                    volRender.setCrop(ui.getCropUI().getLow(), ui.getCropUI().getUp());
                outObj.attach();
            }
        });

        if (lendUI)
            setPanel(ui);

        outObj.setName("VolRender");
        outObj.getRenderingParams().getAppearance().getTransparencyAttributes().setTransparencyMode(TransparencyAttributes.NICEST);
        outObj.setCreator(this);
        colormapLegend.setParams(dataMappingParams.getColormapLegendParameters());
        outObj.addGeometry2D(colormapLegend);
        needPowerOf2Textures = VisNow.get().getMainConfig().isPower2TextureNeeded();

        projectionListener = new ProjectionListener()
        {
            @Override
            public void projectionChanged(ProjectionEvent e)
            {
                locToWin = e.getLocalToWindow();
                if (volRender == null || locToWin == null)
                    return;

                int d;

                if (volRender.getNormalVectors() != null) {
                    Transform3D t3d = new Transform3D();
                    volRender.getNormalVectors().getTransform(t3d);
                    d = locToWin.getDir(t3d);
                } else
                    d = locToWin.getDir();

                if (d != lastDir ||
                    (d > 0 && volRender.getDir() != lastDir - 1) ||
                    (d <= 0 && volRender.getDir() != -lastDir - 1)) {
                    if (d > 0)
                        volRender.setDir(d - 1, false);
                    else
                        volRender.setDir(-d - 1, true);
                    lastDir = d;
                }
            }
        };

        lightDirectionListener = new LightDirectionListener()
        {
            @Override
            public void lightChanged(LightDirectionEvent e)
            {
                Vector3f ld = new Vector3f(), tld = new Vector3f();
                Transform3D ltr = new Transform3D();
                e.getLight().getLocalToVworld(ltr);
                e.getLight().getDirection(tld);
                ltr.transform(tld, ld);
                ld.get(lightDir);
                boolean newLightDir = false;
                float ldNorm = 0;
                for (int i = 0; i < 3; i++) {
                    if (lastLightDir[i] != lightDir[i])
                        newLightDir = true;
                    lastLightDir[i] = lightDir[i];
                    ldNorm += lightDir[i] * lightDir[i];
                }
                ldNorm = 1 / (float) Math.sqrt(ldNorm);
                for (int i = 0; i < 3; i++)
                    lightDir[i] *= ldNorm;
                if (newLightDir && volRender != null) {
                    volRender.setLightDir(lightDir);
                }
            }
        };
    }

    public GUI getVolRenderUI()
    {
        return ui;
    }

    public Params getParams()
    {
        return params;
    }

    public void setInField(RegularField in)
    {
        clearAll();
        if (in == null) {
            ui.setInField(null, dataMappingParams);
            update();
            return;
        }
        int[] inD = in.getDims();
        if (inD.length != 3 || inD[0] < 2 || inD[1] < 2 || inD[2] < 2 || in.getNComponents() < 1) {
            ui.setInField(null, dataMappingParams);
            update();
            return;
        }
        inField = in;
        inDims = inD;
        af = inField.getAffine();
        ia = inField.getInvAffine();
        phys_exts = inField.getPreferredPhysicalExtents();
        dataMappingParams.setInData(inField, (DataContainerSchema) null);
        ui.setInField(this.inField, dataMappingParams);
        volRender = new TextureVolumeRenderer(inField, dataMappingParams, params.getShadingParams(), needPowerOf2Textures);
        outObj.addNode(volRender);
        outObj.setExtents(inField.getPreferredExtents());
        update();
    }

    public void setInData(RegularField in, int mapType, float low, float up)
    {
        clearAll();
        if (in == null) {
            ui.setInField(null, dataMappingParams);
            update();
            return;
        }
        int[] inD = in.getDims();
        if (inD.length != 3 || inD[0] < 2 || inD[1] < 2 || inD[2] < 2 || in.getNComponents() < 1) {
            ui.setInField(null, dataMappingParams);
            update();
            return;
        }
        inField = in;
        inDims = inD;
        af = inField.getAffine();
        ia = inField.getInvAffine();
        phys_exts = inField.getPreferredPhysicalExtents();
        dataMappingParams.setInData(inField, (DataContainerSchema) null);
        dataMappingParams.getColorMap0().setMapIndex(mapType);
        dataMappingParams.getColorMap0().getComponentRange().setLowUp(low, up);
        dataMappingParams.getTransparencyParams().getComponentRange().setLowUp(low, up);
        ui.setInField(this.inField, dataMappingParams);
        volRender = new TextureVolumeRenderer(inField, dataMappingParams, params.getShadingParams(), needPowerOf2Textures);
        outObj.addNode(volRender);
        outObj.setExtents(inField.getPreferredExtents());
        update();
    }

    public void update()
    {
        boolean det = outObj.detach();
        if (volRender != null) {
            volRender.updateTexture();
            volRender.updateMesh();
        } else
            outObj.clearAllGeometry();

        if (inField != null) {
            outObj.setExtents(inField.getPreferredExtents());
            if (det)
                outObj.attach();
        }
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            clearAll();
            return;
        }

        if (outCroppedField) {
            outCroppedField = false;
            if (inField == null) {
                clearAll();
                return;
            }
            int[] outDims = new int[inDims.length];
            int[] low = ui.getCropUI().getLow();
            int[] up = ui.getCropUI().getUp();
            int[] down = new int[]{
                1, 1, 1
            };
            for (int i = 0; i < outDims.length; i++) {
                if (low[i] < 0 || up[i] > inDims[i])
                    return;
                outDims[i] = up[i] - low[i];
            }
            outField = new RegularField(outDims);
            if (inField.getCurrentCoords() != null)
                outField.setCurrentCoords((FloatLargeArray) cropDownArray(inField.getCurrentCoords(), 3, inDims, low, up, down));
            else {
                float[][] outAffine = new float[4][3];
                float[][] affine = inField.getAffine();
                System.arraycopy(affine[3], 0, outAffine[3], 0, 3);
                for (int i = 0; i < outDims.length; i++)
                    for (int j = 0; j < 3; j++) {
                        outAffine[3][j] += low[i] * affine[i][j];
                        outAffine[i][j] = affine[i][j];
                    }
                outField.setAffine(outAffine);
            }

            for (int i = 0; i < inField.getNComponents(); i++) {
                DataArray dta = inField.getComponent(i);
                Object data = cropDownArray(dta.getRawArray(), dta.getVectorLength(), inDims, low, up, down).getData();
                outField.addComponent(DataArray.create(data, dta.getVectorLength(), dta.getName(), dta.getUnit(), dta.getUserData()));
            }
            setOutputValue("croppedField", new VNRegularField(outField));
            return;
        }
        RegularField in = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (in == null) {
            clearAll();
            return;
        }
        if (in.getDims().length != 3 || in.hasCoords()) { 
            clearAll();
            return;
        }

        if (in != inField) {
            ignoreUI = true;
            clearAll();
            int[] inD = in.getDims();
            if (inD.length != 3 ||
                inD[0] < 2 || inD[1] < 2 || inD[2] < 2 ||
                in.getNComponents() < 1)
                return;

            if (in.isLarge()) {
                int[] down = new int[]{1, 1, 1};
                boolean downsample = false;
                for (int i = 0; i < inD.length; i++) {
                    if (inD[i] > 1000) {
                        down[i] = (int) Math.ceil(inD[i] / 1000.);
                        if (down[i] > 1) {
                            downsample = true;
                        }
                    }
                }
                if (downsample) {
                    this.inField = in.downsample(down);
                    inD = this.inField.getDims();
                } else {
                    this.inField = in;
                }
            } else {
                this.inField = in;
            }

            if (this.inField.isLarge()) {
                this.inField = null;
                volRender = null;
                return;
            }

            //outObj.clearAllGeometry();
            float cTime = this.inField.getCurrentTime();
            inDims = inD;
            af = inField.getAffine();
            ia = inField.getInvAffine();
            phys_exts = inField.getPreferredPhysicalExtents();
            inField.setCurrentTime(cTime);
            dataMappingParams.setInData(inField, (DataContainerSchema) null);
            dataMappingParams.setParentObjectSize((int) inField.getNNodes());
            volRender = new TextureVolumeRenderer(inField, dataMappingParams, params.getShadingParams(), needPowerOf2Textures);
            volRender.addStatusListener(
                new FloatValueModificationListener()
            {
                @Override
                public void floatValueChanged(FloatValueModificationEvent e)
                {
                    setProgress(e.getVal());
                }
            });
            ui.setInField(this.inField, dataMappingParams);
            outObj.addNode(volRender);
            ignoreUI = false;
        }
        update();
    }

    private void clearAll()
    {
        if (volRender != null) {
            volRender.cleanOldListeners();
            volRender.clearStatusListener();
            volRender = null;
        }
        inField = null;
        if(outObj != null) {
            outObj.clearAllGeometry();
        }
    }

    
    @Override
    public void onInputDetach(LinkFace link)
    {
        clearAll();
    }

    @Override
    public FrameRenderedListener getFrameRenderedListener()
    {
        return null;
    }

    protected int lastDir = 0;

    protected void updateProjection(ProjectionEvent e)
    {
        if (e == null)
            return;
        locToWin = e.getLocalToWindow();
        if (volRender == null || locToWin == null)
            return;

        int d;

        if (volRender.getNormalVectors() != null) {
            Transform3D t3d = new Transform3D();
            volRender.getNormalVectors().getTransform(t3d);
            d = locToWin.getDir(t3d);
        } else
            d = locToWin.getDir();

        if (d != lastDir ||
            (d > 0 && volRender.getDir() != lastDir - 1) ||
            (d <= 0 && volRender.getDir() != -lastDir - 1)) {
            if (d > 0)
                volRender.setDir(d - 1, false);
            else
                volRender.setDir(-d - 1, true);
            lastDir = d;
        }
    }

    public DataMappingParams getDataMappingParams()
    {
        return dataMappingParams;
    }

    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            if (inField == null)
                return;
            float[] x = e.getPoint();
            if (x == null)
                return;
            float[] y = new float[3];
            for (int i = 0; i < y.length; i++)
                y[i] = x[i] - af[3][i];
            for (int i = 0; i < 3; i++) {
                picked_world_coords[i] = 0;
                for (int j = 0; j < picked_world_coords.length; j++)
                    picked_world_coords[j] += ia[i][j] * y[j];
            }
            picked_index = new int[]{(int) picked_world_coords[0], (int) picked_world_coords[1], (int) picked_world_coords[2]};
            if (picked_index[0] < 0 || picked_index[0] >= inDims[0] ||
                picked_index[1] < 0 || picked_index[1] >= inDims[1] ||
                picked_index[2] < 0 || picked_index[2] >= inDims[2])
                System.out.println("picked point outside of field area");
            else {
                for (int i = 0; i < 3; i++)
                    picked_physical_coords[i] = phys_exts[0][i] + picked_world_coords[i] / (inDims[i] - 1) * (phys_exts[1][i] - phys_exts[0][i]);
                System.out.println("picked index = [" + picked_index[0] + "," + picked_index[1] + "," + picked_index[2] + "]");
                System.out.println("picked point = [" + picked_physical_coords[0] + "," + picked_physical_coords[1] + "," + picked_physical_coords[2] + "]");
                fireFieldPickChanged();
            }
        }
    };

    @Override
    public Pick3DListener getPick3DListener()
    {
        return pick3DListener;
    }

    @Override
    public ProjectionListener getProjectionListener()
    {
        return projectionListener;
    }

    @Override
    public ColorListener getBackgroundColorListener()
    {
        return ui.getBackgroundColorListener();
    }

    //
    ///* ========== FieldPickListeners - listening ====== */
    //
    /**
     * Utility field holding list of FieldPickListeners.
     */
    protected transient ArrayList<FieldPickListener> fieldPickListenerList = new ArrayList<>();

    /**
     * Registers FieldPickListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addPick3DListener(FieldPickListener listener)
    {
        fieldPickListenerList.add(listener);
    }

    /**
     * Removes FieldPickListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removePick3DListener(FieldPickListener listener)
    {
        fieldPickListenerList.remove(listener);
    }

    /**
     * Notifies all registered FieldPickListeners about the event (picking a
     * point in the volume renderer).
     * <p>
     */
    public void fireFieldPickChanged()
    {
        FieldPickEvent e = new FieldPickEvent(this, picked_world_coords, picked_index, picked_physical_coords);
        for (FieldPickListener listener : fieldPickListenerList)
            listener.pick3DChanged(e);
    }
}
