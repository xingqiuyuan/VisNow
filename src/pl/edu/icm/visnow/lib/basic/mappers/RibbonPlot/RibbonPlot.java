//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.RibbonPlot;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.lib.basic.mappers.Axes3D.Axes3DObject;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static java.lang.Math.*;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class RibbonPlot extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected RegularField inField;
    protected GUI computeUI = null;
    protected boolean ignoreUI = false;
    protected boolean fromUI = false;
    protected Params params = null;
    protected int[] inDims = null;
    protected float[] coords = null;
    protected int axis = -1;
    protected int[] cells = null;
    protected byte[] orientations = null;
    protected int[] edges = null;
    protected byte[] edgeOrientations = null;
    protected boolean ribbon = true;
    protected RegularField outBox = new RegularField(new int[]{2, 2, 2});
    protected pl.edu.icm.visnow.lib.basic.mappers.Axes3D.Axes3DObject axesObj = null;
    protected pl.edu.icm.visnow.lib.basic.mappers.Axes3D.Axes3DParams axesParams
        = new pl.edu.icm.visnow.lib.basic.mappers.Axes3D.Axes3DParams();

    public RibbonPlot()
    {
        parameters = params = new Params();
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            public void run()
            {
                computeUI = new GUI();
            }
        });
        computeUI.setParams(params);
        computeUI.getAxesGUI().setParams(axesParams);
        ui.addComputeGUI(computeUI);
        outObj.setName("ribbon plot");
        axesObj = new Axes3DObject();
        axesObj.setName("axes3D");
        outObj.addChild(axesObj);
        renderingParams.setDisplayMode(RenderingParams.SURFACE | RenderingParams.SEGMENT_CELLS);

        params.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                fromUI = true;
                if (ignoreUI)
                    return;
                if (computeUI.isAdjusting()) {
                    updateCoords();
                    irregularFieldGeometry.updateCoords();
                } else {
                    axis   = params.getAxis();
                    ribbon = params.isRibbon();
                    startAction();
                }
            }
        });
        axesParams.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent evt)
            {
                if (params.showAxes())
                    axesObj.update(outBox, axesParams);
            }
        });
        setPanel(ui);
    }

    void updateCells()
    {
        edges = new int[4 * inDims[1] * inDims[0]];
        edgeOrientations = new byte[2 * inDims[1] * inDims[0]];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        if (axis == 0) {
            orientations = new byte[inDims[1] * (inDims[0] - 1)];
            for (int i = 0; i < orientations.length; i++)
                orientations[i] = 1;
            cells = new int[4 * inDims[1] * (inDims[0] - 1)];
            for (int i = 0, k = 0, l = 0; i < inDims[1]; i++, l += 2)
                for (int j = 0; j < inDims[0] - 1; j++, l += 2, k += 4) {
                    cells[k] = l;
                    cells[k + 1] = l + 1;
                    cells[k + 2] = l + 3;
                    cells[k + 3] = l + 2;
                }
            for (int i = 0, k = 0, l = 0; i < inDims[1]; i++, l += 2) {
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
                for (int j = 0; j < inDims[0] - 1; j++, l += 2, k += 4) {
                    edges[k] = l;
                    edges[k + 1] = l + 2;
                    edges[k + 2] = l + 1;
                    edges[k + 3] = l + 3;
                }
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
            }
        } else {
            orientations = new byte[inDims[0] * (inDims[1] - 1)];
            for (int i = 0; i < orientations.length; i++)
                orientations[i] = 1;
            cells = new int[4 * inDims[0] * (inDims[1] - 1)];
            for (int i = 0, k = 0; i < inDims[0]; i++)
                for (int j = 0, l = 2 * i; j < inDims[1] - 1; j++, l += 2 * inDims[0], k += 4) {
                    cells[k] = l;
                    cells[k + 1] = l + 1;
                    cells[k + 2] = l + 2 * inDims[0] + 1;
                    cells[k + 3] = l + 2 * inDims[0];
                }
            for (int i = 0, k = 0; i < inDims[0]; i++) {
                int l = 2 * i;
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
                for (int j = 0; j < inDims[1] - 1; j++, l += 2 * inDims[0], k += 4) {
                    edges[k] = l;
                    edges[k + 1] = l + 2 * inDims[0];
                    edges[k + 2] = l + 1;
                    edges[k + 3] = l + 2 * inDims[0] + 1;
                }
                edges[k] = l;
                edges[k + 1] = l + 1;
                k += 2;
            }
        }
    }

    void updateCoords()
    {
        float[] dataRow;
        float[] tCoords = new float[3 * inDims[0]];
        float[] baseCoords, uCoords = null;
        float base = 0;
        if (ribbon)
            uCoords = new float[3 * inDims[0]];
        if (!params.isZeroBased())
            base = (float) inField.getComponent(params.getComponent()).getPreferredMinValue();
        if (inField.getCurrentCoords() == null) {
            baseCoords = new float[3 * (int) inField.getNNodes()];
            float[] normalVector = new float[3];     // a versor orthogonal to the field plane
            float[][] affine = inField.getAffine();
            normalVector[0] = affine[0][1] * affine[1][2] - affine[0][2] * affine[1][1];
            normalVector[1] = affine[0][2] * affine[1][0] - affine[0][0] * affine[1][2];
            normalVector[2] = affine[0][0] * affine[1][1] - affine[0][1] * affine[1][0];
            float r = (float) (sqrt(normalVector[0] * normalVector[0] + 
                                    normalVector[1] * normalVector[1] + 
                                    normalVector[2] * normalVector[2]));
            for (int i = 0; i < normalVector.length; i++)
                normalVector[i] *= params.getScale() / r;
            float[] transverseDir;
            if (axis == 0)
                transverseDir = affine[1];
            else
                transverseDir = affine[0];
            
            for (int i = 0, m = 0; i < inDims[1]; i++) {
                dataRow = inField.getComponent(params.getComponent()).getCurrent1DFloatSlice(i * inDims[0], inDims[0], 1).getData();
                for (int j = 0, k = 0; j < inDims[0]; j++)
                    for (int l = 0; l < 3; l++, k++) {
                        baseCoords[k] = affine[3][l] + i * affine[1][l] + j * affine[0][l];
                        tCoords[k] = baseCoords[k] + normalVector[l] * (dataRow[j] - base);
                    }
                if (ribbon) 
                    for (int j = 0, k = 0; j < inDims[0]; j++, m += 6) 
                        for (int l = 0; l < 3; l++, k++) {
                            coords[m +     l] = tCoords[k] - .5f * transverseDir[l];
                            coords[m + 3 + l] = tCoords[k] + .5f * transverseDir[l];
                    }
                else 
                    for (int j = 0, l = 0; j < inDims[0]; j++, m += 6, l += 3) {
                        System.arraycopy(baseCoords, l, coords, m,     3);
                        System.arraycopy(tCoords,    l, coords, m + 3, 3);
                    }
            }
        } else {
            baseCoords = inField.getCurrentCoords().getData();
            float[] normals = inField.getNormals().getData();
            int plusOff = 0, minusOff = 0;
            float scale = 1;
            for (int i = 0, k = 0, m = 0; i < inDims[1]; i++) {
                if (axis == 0) {
                    plusOff  =  3 * inDims[0];
                    minusOff = -3 * inDims[0];
                    scale = .5f;
                    if (i == 0) {
                        minusOff = 0;
                        scale = 1;
                    }
                    if (i == inDims[1] - 1) {
                        plusOff = 0;
                        scale = 1;
                    }
                }
                dataRow = inField.getComponent(params.getComponent()).getCurrent1DFloatSlice(i * inDims[0], inDims[0], 1).
                                  getData();
                for (int j = 0; j < inDims[0]; j++) {
                    if (axis == 1) {
                        plusOff = 3;
                        minusOff = -3;
                        scale = .5f;
                        if (j == 0) {
                            minusOff = 0;
                            scale = 1;
                        }
                        if (j == inDims[0] - 1) {
                            plusOff = 0;
                            scale = 1;
                        }
                    }
                    float s = params.getScale() * (dataRow[j] - base);
                    for (int l = 0; l < 3; l++, k++) {
                        tCoords[3 * j + l] = baseCoords[k] + s * normals[k];
                        if (ribbon)
                            uCoords[3 * j + l] = scale * (baseCoords[k + plusOff] - baseCoords[k + minusOff] + 
                                                     s *    (normals[k + plusOff] -    normals[k + minusOff]));
                    }
                }
                if (ribbon) {
                    for (int j = 0; j < 3 * inDims[0]; m += 6) 
                        for (int l = 0; l < 3; l++, j++) {
                            coords[m +     l] = tCoords[j] - .5f * uCoords[j];
                            coords[m + 3 + l] = tCoords[j] + .5f * uCoords[j];
                        }
                } else 
                    for (int j = 0, l = 0; j < inDims[0]; j++,  m += 6, l += 3) 
                        System.arraycopy(tCoords, l, coords, m + 3, 3);
                    
            }
            if (!ribbon)
                for (int i = 0; i < baseCoords.length; i += 3)
                    System.arraycopy(baseCoords, i, coords, 2 * i, 3);
        }
    }

    @Override
    public void onActive()
    {
        fromUI = false;
        if (!params.isActive())
            return;
        VNRegularField inFld = (VNRegularField) getInputFirstValue("inField");
        if (inFld == null)
            return;
        RegularField in = inFld.getField();
        if (in == null || in.getNComponents() < 1 ||
            in.getDims().length != 2 || in.getDims()[0] < 2 || in.getDims()[1] < 2)
            return;
        if (in != inField || axis != params.getAxis()) {
            if (!in.isDataCompatibleWith(inField)) {
                ignoreUI = true;
                computeUI.setInField(in);
                ignoreUI = false;
            }
            inField = in;
            inDims = inField.getDims();
        }
        axis = params.getAxis();
        outIrregularField = new IrregularField(2 * (int) inField.getNNodes());
        coords = new float[3 * (int) outIrregularField.getNNodes()];
        updateCoords();
        FloatLargeArray lCoords = new FloatLargeArray(coords);
        outIrregularField.setCurrentCoords(lCoords);
        updateCells();
        CellArray ribbons = new CellArray(CellType.QUAD, cells, orientations, null);
        CellArray ribbonEdges = new CellArray(CellType.SEGMENT, edges, edgeOrientations, null);
        CellSet cs = new CellSet();
        cs.setCellArray(ribbons);
        cs.setCellArray(ribbonEdges);
        cs.generateDisplayData(lCoords);
        outIrregularField.addCellSet(cs);
        for (DataArray dta : inField.getComponents()) {
            if (dta.isNumeric() && dta.getVectorLength() == 1) {
                LargeArray inDD = dta.getRawArray();
                LargeArray outDD = LargeArrayUtils.create(inDD.getType(), 2 * dta.getNElements(), false);
                for (long i = 0, j = 0; i < inDD.length(); i++, j += 2) {
                    outDD.set(j, inDD.get(i));
                    outDD.set(j + 1, inDD.get(i));
                }
                outIrregularField.addComponent(DataArray.create(outDD, 1, dta.getName()));
            }
        }
        outField = outIrregularField;
        float[][] phExts = new float[2][3];
        float[][] exts = outField.getPreferredExtents();
        float[][] boxAffine = new float[4][3];
        for (int i = 0; i < 3; i++) {
            boxAffine[3][i] = exts[0][i];
            for (int j = 0; j < 3; j++) {
                boxAffine[j][i] = 0;
            boxAffine[i][i] = exts[1][i] - exts[0][i];
            }
        }
        outBox.setAffine(boxAffine);
        
        for (int i = 0; i < 2; i++)
            System.arraycopy(exts[i], 0, phExts[i], 0, 2);
        phExts[0][2] = (float) inField.getComponent(params.getComponent()).getPreferredPhysMinValue();
        phExts[1][2] = (float) inField.getComponent(params.getComponent()).getPreferredPhysMaxValue();
        outBox.setPreferredExtents(outBox.getPreferredExtents(), phExts);
        if (outBox.getNComponents() == 0) {
            outBox.addComponent(DataArray.create(new int[]{0, 1, 2, 3, 4, 5, 6, 7}, 1, "dummy"));
        }
        setOutputValue("outField", new VNRegularField(outBox));
        axesObj.update(outBox, axesParams);
        computeUI.getAxesGUI().setInfield(outBox);
        prepareOutputGeometry();
        renderingParams.setShadingMode(params.isRibbon() ? RenderingParams.GOURAUD_SHADED : RenderingParams.BACKGROUND);
        show();
    }

}
