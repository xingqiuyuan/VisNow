//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Isolines;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.gui.events.BooleanChangeListener;
import pl.edu.icm.visnow.gui.events.BooleanEvent;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class IsolinesGUI extends JPanel
{
    private Field inField = null;
    private IsolinesParams params = new IsolinesParams();
    private String oldComponentName = "";
    private boolean[] aCS = null;
    private float vMin = 0, vMax = 255, 
                  oldMin = Float.NEGATIVE_INFINITY, oldMax = Float.POSITIVE_INFINITY;
    private float physMin = 0, physMax = 255,
                  oldPhysMin = Float.NEGATIVE_INFINITY, oldPhysMax = Float.POSITIVE_INFINITY;
    private boolean syncing = false;

    /**
     * Creates new form IsolinesUI
     */
    public IsolinesGUI()
    {
        initComponents();
        isoComponentSelector.setTitle("isoline component");
        thresholdsEditor.setMaxRangeCount(2);
        thresholdsEditor.setStartSingle(false);
        thresholdsEditor.setValuePreferred(false);
        thresholdsEditor.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                params.setThresholds(thresholdsEditor.getThresholds());
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        isoComponentSelector = new pl.edu.icm.visnow.lib.gui.DataComponentSelector();
        cellSetScrollPane = new javax.swing.JScrollPane();
        cellSetList = new javax.swing.JList();
        thresholdsEditor = new pl.edu.icm.visnow.lib.gui.FloatArrayEditor();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        dataComponentList = new pl.edu.icm.visnow.lib.gui.DataComponentList();
        estimateQualityBox = new javax.swing.JCheckBox();

        setLayout(new java.awt.GridBagLayout());

        isoComponentSelector.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        isoComponentSelector.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                isoComponentSelectorStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(isoComponentSelector, gridBagConstraints);

        cellSetScrollPane.setMinimumSize(new java.awt.Dimension(50, 60));
        cellSetScrollPane.setPreferredSize(new java.awt.Dimension(50, 100));

        cellSetList.setEnabled(false);
        cellSetList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                cellSetListValueChanged(evt);
            }
        });
        cellSetScrollPane.setViewportView(cellSetList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(cellSetScrollPane, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        add(thresholdsEditor, gridBagConstraints);

        jLabel1.setText("interpolate components to isolines");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(6, 7, 3, 7);
        add(jLabel1, gridBagConstraints);

        jPanel1.setLayout(new java.awt.BorderLayout());

        dataComponentList.setPreferredSize(new java.awt.Dimension(10, 100));
        dataComponentList.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                dataComponentListStateChanged(evt);
            }
        });
        jPanel1.add(dataComponentList, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(jPanel1, gridBagConstraints);

        estimateQualityBox.setText("estimate isoline quality");
        estimateQualityBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estimateQualityBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(estimateQualityBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void cellSetListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_cellSetListValueChanged
    {//GEN-HEADEREND:event_cellSetListValueChanged
        if (aCS != null)
            for (int i = 0; i < aCS.length; i++)
                aCS[i] = cellSetList.isSelectedIndex(i);
        params.setActiveCellSets(aCS);
    }//GEN-LAST:event_cellSetListValueChanged

    private void isoComponentSelectorStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_isoComponentSelectorStateChanged
        if (syncing)
            return;
        syncing = true;
        boolean oldActive = params.isActive();
        params.setActive(false);
        int k = isoComponentSelector.getComponent();
        params.setComponent(k);
        if (k != -1)
            setDataComponent(k);
        dataComponentList.setComponent(k);
        params.setActive(oldActive);
        syncing = false;
    }//GEN-LAST:event_isoComponentSelectorStateChanged

    private void dataComponentListStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_dataComponentListStateChanged
    {//GEN-HEADEREND:event_dataComponentListStateChanged
        params.setMappedComponents(dataComponentList.getComponentSelections());
    }//GEN-LAST:event_dataComponentListStateChanged

    private void estimateQualityBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estimateQualityBoxActionPerformed
        params.setEstimateQuality(estimateQualityBox.isSelected());
    }//GEN-LAST:event_estimateQualityBoxActionPerformed

    private void setDataComponent(int k)
    {
        DataArray da = inField.getComponent(k);
        vMin = (float)da.getPreferredMinValue();
        vMax = (float)da.getPreferredMaxValue();
        physMin = (float)da.getPreferredPhysMinValue();
        physMax = (float)da.getPreferredPhysMaxValue();
        if (da.getName().equals(oldComponentName) &&
            vMin    == oldMin     && vMax    == oldMax &&
            physMin == oldPhysMin && physMax == oldPhysMax)
            return;
        oldMin = vMin;
        oldMax = vMax;
        oldPhysMin = physMin;
        oldPhysMax = physMax;
        oldComponentName = da.getName();
        thresholdsEditor.setMinMax(vMin, vMax, physMin, physMax);
    }

    /**
     * Setter for property inField.
     *
     * @param in New value of property inField.
     */
    public void setInField(VNField in)
    {
        params.setActive(false);
        Field inFld = in.getField();
        estimateQualityBox.setEnabled(inFld instanceof RegularField);
        if (inFld instanceof RegularField &&
            (((RegularField) inFld).getDims() == null ||
            ((RegularField) inFld).getDims().length != 2))
            return;

        if (inField == null || !inField.isDataCompatibleWith(inFld)) {
            inField = inFld;
            isoComponentSelector.setDataSchema(inFld.getSchema());
            int k = -1;
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).isNumeric() && inField.getComponent(i).getVectorLength() == 1) {
                    k = i;
                    break;
                }
            isoComponentSelector.setSelectedIndex(0);
        }
        if (inField instanceof IrregularField) {
            String[] cellSetNames = new String[((IrregularField) inField).getNCellSets()];
            for (int i = 0; i < cellSetNames.length; i++)
                cellSetNames[i] = ((IrregularField) inField).getCellSet(i).getName();
            cellSetList.setListData(cellSetNames);
            cellSetList.setEnabled(true);
            aCS = new boolean[((IrregularField) inField).getNCellSets()];
            for (int i = 0; i < aCS.length; i++)
                aCS[i] = true;
            params.setActiveCellSets(aCS);
        } else
            cellSetList.setEnabled(false);
        dataComponentList.setInField(inField);
        setDataComponent(isoComponentSelector.getComponent());
        params.setActive(true);
    }

    public void setParams(IsolinesParams params)
    {
        this.params = params;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JList cellSetList;
    public javax.swing.JScrollPane cellSetScrollPane;
    public pl.edu.icm.visnow.lib.gui.DataComponentList dataComponentList;
    public javax.swing.JCheckBox estimateQualityBox;
    public pl.edu.icm.visnow.lib.gui.DataComponentSelector isoComponentSelector;
    public javax.swing.JLabel jLabel1;
    public javax.swing.JPanel jPanel1;
    public pl.edu.icm.visnow.lib.gui.FloatArrayEditor thresholdsEditor;
    // End of variables declaration//GEN-END:variables
}
