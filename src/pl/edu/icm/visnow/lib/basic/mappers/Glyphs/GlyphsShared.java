//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Glyphs;

import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */

public class GlyphsShared
{
    // Parameter names + specification. SPECIFICATION CONSTRAINTS ARE NOT TESTED IN LOGIC!
    // Specification of a parameter above declaration
    
    // A number of existing component in input field
    static final ParameterName<Integer> COMPONENT = new ParameterName<>("Component");
    // Type of glyphs (one of two models)
    static final ParameterName<Integer> TYPE = new ParameterName<>("Type");
    
    // CROP/DOWNSIZE PARAMETERS:
    // An int array of size equals to field dimension (dims.length)...
    static final ParameterName<int[]> CROP_LOW = new ParameterName<>("Crop low indices");
    static final ParameterName<int[]> CROP_UP = new ParameterName<>("Crop high indices");
    static final ParameterName<int[]> DOWN = new ParameterName<>("Down");
    static final ParameterName<Integer> DOWNSIZE = new ParameterName<>("Downsize");
    static final ParameterName<Float> THRESHOLD = new ParameterName<>("Threshold");
    // A number of existing component in input field, or -1, if the won't be used
    static final ParameterName<Integer> THRESHOLD_COMPONENT = new ParameterName<>("Threshold component");
    static final ParameterName<Boolean> THRESHOLD_RELATIVE = new ParameterName<>("Threshold relative");

    // Scaling...? Dependent of type?
    static final ParameterName<Boolean> CONSTANT_DIAMETER = new ParameterName<>("Constant diameter");
    static final ParameterName<Boolean> CONSTANT_THICKNESS = new ParameterName<>("Constant thickness");
    static final ParameterName<Boolean> USE_ABS = new ParameterName<>("Use abs");
    static final ParameterName<Boolean> USE_SQRT = new ParameterName<>("Use sqrt");
    static final ParameterName<Float> SCALE = new ParameterName<>("Scale");
    static final ParameterName<Float> THICKNESS = new ParameterName<>("Thickness");
    static final ParameterName<Float> LINE_THICKNESS = new ParameterName<>("Line thickness");
    static final ParameterName<Float> TRANSPARENCY = new ParameterName<>("Transparency");
    static final ParameterName<Integer> LEVEL_OF_DETAIL = new ParameterName<>("Level of detail (LOD)");
}