//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Streamlines;

import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;

import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

import static pl.edu.icm.visnow.lib.basic.mappers.Streamlines.StreamlinesShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 * 
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
abstract public class ComputeStreamlines
{
    protected IrregularField outField = null;
    protected int trueDim = 3;
    protected float[] startCoords = null;
    protected float[] coords = null;
    protected int[] fromSteps = null;
    protected int[] toSteps = null;
    protected float[] vectors = null;
    protected int nvert = 0;
    protected int[] lines = null;
    protected Parameters parameters = null;
    protected int nForward = 0;
    protected int nBackward = 0;
    protected int nSrc;
    protected float[] vects = null;
    protected int vlen = 3;
    protected float[][] downCoords = null;
    protected float[][] upCoords = null;
    protected float[][] downVectors = null;
    protected float[][] upVectors = null;
    protected int nThreads;
    protected int[] indices = null;
    protected float effectiveScale = .001f, avgVector = 1;
    protected byte[] mask = null;

    protected ProgressAgent progressAgent = null;
    
    public ComputeStreamlines(Field inField, Parameters parameters)
    {
        this.parameters = parameters;
        nForward = parameters.get(NUM_STEPS_FORWARD);
        nBackward = parameters.get(NUM_STEPS_BACKWARD);
        trueDim = inField.getTrueNSpace();
        vects = inField.getComponent(parameters.get(COMPONENT)).getRawFloatArray().getData();
        vlen = inField.getComponent(parameters.get(COMPONENT)).getVectorLength();
    }
    
    public ComputeStreamlines(Field inField, Parameters parameters, ProgressAgent progressAgent)
    {
        this(inField, parameters);
        this.progressAgent = progressAgent;
    }

    abstract void updateStreamlines();

    public void setStartPoints(Field startPoints)
    {
        nSrc = (int) startPoints.getNNodes();
        if (startPoints.getCurrentCoords() != null)
            startCoords = startPoints.getCurrentCoords().getData();
        else if (startPoints instanceof RegularField)
            startCoords = ((RegularField) startPoints).getCoordsFromAffine().getData();
        fromSteps = new int[nSrc];
        toSteps = new int[nSrc];
        if (startPoints.hasMask())
            mask = startPoints.getCurrentMask().getData();
    }

    public float[] getvNorms()
    {
        return vectors;
    }

    public IrregularField getOutField()
    {
        return outField;
    }

    public float[] getCoords()
    {
        return coords;
    }

    public int[] getFromSteps()
    {
        return fromSteps;
    }

    public int[] getToSteps()
    {
        return toSteps;
    }

    public int[] getLines()
    {
        return lines;
    }

    public int getNvert()
    {
        return nvert;
    }
}
