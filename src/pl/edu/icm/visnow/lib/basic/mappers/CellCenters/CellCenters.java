//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.CellCenters;

import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class CellCenters extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected RegularField inRegularField = null;
    protected IrregularField inField = null;

    public CellCenters()
    {
        setPanel(ui);
        outObj.setName("cell centers");
    }
    
    private int makeCellSetCentersData(CellSet inCS, int veclen, float[] in, float[] out, int iout)
    {
        float[] u = new float[veclen];
        for (int j = 0; j < Cell.getNProperCellTypes(); j++)
            if (inCS.getCellArray(CellType.getType(j)) != null) {
                CellArray inCA = inCS.getCellArray(CellType.getType(j));
                int[] nodes = inCA.getNodes();
                int nv = CellType.getType(j).getNVertices();
                for (int k = 0; k < inCA.getNCells(); k++, iout++) {
                    for (int l = 0; l < veclen; l++) {
                        u[l] = 0;
                        for (int p = 0; p < nv; p++) {
                            int ip = nodes[k * nv + p];
                            u[l] += in[veclen * ip + l];
                        }
                        u[l] /= nv;
                    }
                    System.arraycopy(u, 0, out, veclen * iout, veclen);
                }
            }
        return iout;
    }
    
    private int makeCellSetCentersData(CellSet inCS, int veclen, double[] in, double[] out, int iout)
    {
        double[] u = new double[veclen];
        for (int j = 0; j < Cell.getNProperCellTypes(); j++)
            if (inCS.getCellArray(CellType.getType(j)) != null) {
                CellArray inCA = inCS.getCellArray(CellType.getType(j));
                int[] nodes = inCA.getNodes();
                int nv = CellType.getType(j).getNVertices();
                for (int k = 0; k < inCA.getNCells(); k++, iout++) {
                    for (int l = 0; l < veclen; l++) {
                        u[l] = 0;
                        for (int p = 0; p < nv; p++) {
                            int ip = nodes[k * nv + p];
                            u[l] += in[veclen * ip + l];
                        }
                        u[l] /= nv;
                    }
                    System.arraycopy(u, 0, out, veclen * iout, veclen);
                }
            }
        return iout;
    }
    
    private int makeCellSetCentersData(CellSet inCS, int veclen, int[] in, int[] out, int iout)
    {
        int[] u = new int[veclen];
        for (int j = 0; j < Cell.getNProperCellTypes(); j++)
            if (inCS.getCellArray(CellType.getType(j)) != null) {
                CellArray inCA = inCS.getCellArray(CellType.getType(j));
                int[] nodes = inCA.getNodes();
                int nv = CellType.getType(j).getNVertices();
                for (int k = 0; k < inCA.getNCells(); k++, iout++) {
                    for (int l = 0; l < veclen; l++) {
                        u[l] = 0;
                        for (int p = 0; p < nv; p++) {
                            int ip = nodes[k * nv + p];
                            u[l] += in[veclen * ip + l];
                        }
                        u[l] = (int)Math.round(u[l] / (double)nv);
                    }
                    System.arraycopy(u, 0, out, veclen * iout, veclen);
                }
            }
        return iout;
    }
    
    private int makeCellSetCentersData(CellSet inCS, int veclen, short[] in, short[] out, int iout)
    {
        int[] u = new int[veclen];
        short[] v = new short[veclen];
        for (int j = 0; j < Cell.getNProperCellTypes(); j++)
            if (inCS.getCellArray(CellType.getType(j)) != null) {
                CellArray inCA = inCS.getCellArray(CellType.getType(j));
                int[] nodes = inCA.getNodes();
                int nv = CellType.getType(j).getNVertices();
                for (int k = 0; k < inCA.getNCells(); k++, iout++) {
                    for (int l = 0; l < veclen; l++) {
                        u[l] = 0;
                        for (int p = 0; p < nv; p++) {
                            int ip = nodes[k * nv + p];
                            u[l] += in[veclen * ip + l];
                        }
                        v[l] = (short)Math.round(u[l] / (double)nv);
                    }
                    System.arraycopy(v, 0, out, veclen * iout, veclen);
                }
            }
        return iout;
    }

    private int makeCellSetCentersData(CellSet inCS, int veclen, byte[] in, byte[] out, int iout)
    {
        int[] u = new int[veclen];
        byte[] v = new byte[veclen];
        for (int j = 0; j < Cell.getNProperCellTypes(); j++)
            if (inCS.getCellArray(CellType.getType(j)) != null) {
                CellArray inCA = inCS.getCellArray(CellType.getType(j));
                int[] nodes = inCA.getNodes();
                int nv = CellType.getType(j).getNVertices();
                for (int k = 0; k < inCA.getNCells(); k++, iout++) {
                    for (int l = 0; l < veclen; l++) {
                        u[l] = 0;
                        for (int p = 0; p < nv; p++) {
                            int ip = nodes[k * nv + p];
                            u[l] += in[veclen * ip + l] & 0xff;
                        }
                        v[l] = (byte)((int)Math.round(u[l] / (double)nv) & 0xff);
                    }
                    System.arraycopy(v, 0, out, veclen * iout, veclen);
                }
            }
        return iout;
    }

    private void createOutField()
    {
        if (inField == null)
            return;
        int nNodes = 0;
        int[] nSetNodes = new int[inField.getNCellSets()];
        for (int i = 0; i < nSetNodes.length; i++)
            nSetNodes[i] = 0;
        for (int i = 0; i < inField.getNCellSets(); i++) {
            CellSet cs = inField.getCellSet(i);
            for (int j = 0; j < Cell.getNProperCellTypes(); j++)
                if (cs.getCellArray(CellType.getType(j)) != null) {
                    nNodes += cs.getCellArray(CellType.getType(j)).getNCells();
                    nSetNodes[i] += cs.getCellArray(CellType.getType(j)).getNCells();
                }
        }
        outIrregularField = new IrregularField(nNodes);

        float[] inCoords = inField.getCurrentCoords() == null ? null : inField.getCurrentCoords().getData();
        float[] coords = new float[3 * nNodes];
        float[] u = new float[3];
        for (int i = 0, iout = 0; i < inField.getNCellSets(); i++) {
            CellSet inCS = inField.getCellSet(i);
            CellSet outCS = new CellSet(inCS.getName());
            int[] outIndices = new int[nSetNodes[i]];
            byte[] orientations = new byte[nSetNodes[i]];
            int[] outNodes = new int[nSetNodes[i]];
            for (int j = 0, csCell = 0, iCAout = 0; j < Cell.getNProperCellTypes(); j++)
                if (inCS.getCellArray(CellType.getType(j)) != null) {
                    CellArray inCA = inCS.getCellArray(CellType.getType(j));
                    int[] nodes = inCA.getNodes();
                    int nv = CellType.getType(j).getNVertices();
                    for (int k = 0; k < inCA.getNCells(); k++, iout++, csCell++) {
                        for (int l = 0; l < 3; l++) {
                            u[l] = 0;
                            for (int p = 0; p < nv; p++) {
                                int ip = nodes[k * nv + p];
                                u[l] += inCoords[3 * ip + l];
                            }
                            u[l] /= nv;
                        }
                        System.arraycopy(u, 0, coords, 3 * iout, 3);
                        outNodes[csCell] = iout;
                        orientations[csCell] = 1;
                    }
                    if (inCA.getDataIndices() != null) {
                        int[] inIndices = inCA.getDataIndices();
                        for (int k = 0; k < inCA.getNCells(); k++, iCAout++)
                            outIndices[iCAout] = inIndices[k];
                    }
                }
            CellArray outCA = new CellArray(CellType.POINT, outNodes, orientations, outIndices);
            outCS.setCellArray(outCA);
            for (int j = 0; j < inCS.getNComponents(); j++)
                outCS.addComponent(inCS.getComponent(j).cloneShallow());
            if (inField.getNCellSets() == 1)
                for (int j = 0; j < inCS.getNComponents(); j++)
                    outIrregularField.addComponent(inCS.getComponent(j).cloneShallow());
            outIrregularField.addCellSet(outCS);
        }        
        outIrregularField.setCurrentCoords(new FloatLargeArray(coords));
        for (int iData = 0; iData < inField.getNComponents(); iData++) {
            DataArray inDA = inField.getComponent(iData);
            if (inDA.isNumeric()) {
                int vl = inDA.getVectorLength();
                String name = inDA.getName();
                switch (inDA.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] outBA = new byte[nNodes * vl];
                    for (int iSet = 0, iout = 0; iSet < inField.getNCellSets(); iSet++) {
                        CellSet inCS = inField.getCellSet(iSet);
                        iout = makeCellSetCentersData(inCS, vl, (byte[])inDA.getRawArray().getData(), outBA, iout);
                    }
                    outIrregularField.addComponent(DataArray.create(new UnsignedByteLargeArray(outBA), vl, name));
                    break;
                case FIELD_DATA_SHORT:
                    short[] outSA = new short[nNodes * vl];
                    for (int iSet = 0, iout = 0; iSet < inField.getNCellSets(); iSet++) {
                        CellSet inCS = inField.getCellSet(iSet);
                        iout = makeCellSetCentersData(inCS, vl, (short[])inDA.getRawArray().getData(), outSA, iout);
                    }
                    outIrregularField.addComponent(DataArray.create(new ShortLargeArray(outSA), vl, name));
                    break;
                case FIELD_DATA_INT:
                    int[] outIA = new int[nNodes * vl];
                    for (int iSet = 0, iout = 0; iSet < inField.getNCellSets(); iSet++) {
                        CellSet inCS = inField.getCellSet(iSet);
                        iout = makeCellSetCentersData(inCS, vl, (int[])inDA.getRawArray().getData(), outIA, iout);
                    }
                    outIrregularField.addComponent(DataArray.create(new IntLargeArray(outIA), vl, name));
                    break;
                case FIELD_DATA_FLOAT:
                    float[] outFA = new float[nNodes * vl];
                    for (int iSet = 0, iout = 0; iSet < inField.getNCellSets(); iSet++) {
                        CellSet inCS = inField.getCellSet(iSet);
                        iout = makeCellSetCentersData(inCS, vl, (float[])inDA.getRawArray().getData(), outFA, iout);
                    }
                    outIrregularField.addComponent(DataArray.create(new FloatLargeArray(outFA), vl, name));
                    break;
                case FIELD_DATA_DOUBLE:
                    double[] outDA = new double[nNodes * vl];
                    for (int iSet = 0, iout = 0; iSet < inField.getNCellSets(); iSet++) {
                        CellSet inCS = inField.getCellSet(iSet);
                        iout = makeCellSetCentersData(inCS, vl, (double[])inDA.getRawArray().getData(), outDA, iout);
                    }
                    outIrregularField.addComponent(DataArray.create(new DoubleLargeArray(outDA), vl, name));
                    break;
                }
            }
        }
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        VNField input = ((VNField) getInputFirstValue("inField"));
        Field newInField = input.getField();
        if (newInField != null && inField != newInField) {
            inField = (IrregularField) newInField;
            outObj.clearAllGeometry();
            outGroup = null;
            outObj.setName(inField.getName());
            createOutField();
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            outField = outIrregularField;
            prepareOutputGeometry();
            show();
        }
        setOutputValue("outField", new VNIrregularField(outIrregularField));
    }
}
