//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.mappers.Streamlines;

import static org.apache.commons.math3.util.FastMath.*;
import org.apache.log4j.Logger;

import pl.edu.icm.visnow.lib.utils.numeric.ODE.Deriv;
import pl.edu.icm.visnow.lib.utils.numeric.ODE.RungeKutta;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.SimplexPosition;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.cells.CellType;

import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

import static pl.edu.icm.visnow.lib.basic.mappers.Streamlines.StreamlinesShared.*;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 * 
 * Revisions above 564 modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl),
 * University of Warsaw, Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class ComputeIrregularFieldStreamlines extends ComputeStreamlines
{
    private static final Logger LOGGER = Logger.getLogger(ComputeIrregularFieldStreamlines.class);
    
    private IrregularField inField = null;
    private float radius = 1;

    public ComputeIrregularFieldStreamlines(IrregularField inField, Parameters parameters)
    {
        super(inField, parameters);
        this.inField = inField;
        if (inField.getGeoTree() == null) {
            LOGGER.info("creating cell tree");
            long start = System.currentTimeMillis();
            inField.createGeoTree();
            LOGGER.info("cell tree created in " + ((float) (System.currentTimeMillis() - start)) / 1000 + "seconds");
        }

        avgVector = 0;
        for (int i = 0; i < vects.length; i++)
            avgVector += vects[i] * vects[i];
        avgVector = (float) sqrt(avgVector / inField.getNNodes());
    }
    
    public ComputeIrregularFieldStreamlines(IrregularField inField, Parameters parameters, ProgressAgent progressAgent)
    {
        this(inField, parameters);
        this.progressAgent = progressAgent;
    }

    @Override
    public synchronized void updateStreamlines()
    {
        nForward = parameters.get(NUM_STEPS_FORWARD);
        nBackward = parameters.get(NUM_STEPS_BACKWARD);

        downCoords = new float[nSrc][trueDim * nBackward];
        upCoords = new float[nSrc][trueDim * nForward];
        downVectors = new float[nSrc][trueDim * nBackward];
        upVectors = new float[nSrc][trueDim * nForward];
        float[][] xt = inField.getPreferredExtents();
        radius = 0;
        for (int i = 0; i < inField.getTrueNSpace(); i++)
            radius += (xt[1][i] - xt[0][i]) * (xt[1][i] - xt[0][i]);
        radius = (float) sqrt(radius);
        avgVector = 0;
        for (int i = 0; i < vects.length; i++)
            avgVector += vects[i] * vects[i];
        avgVector = (float) sqrt(avgVector / inField.getNNodes());
        effectiveScale = parameters.get(STEP) * radius / avgVector;
//        LOGGER.info("avgVect = " + avgVector + " r = " + radius + " scale = " + effectiveScale);
        nThreads = pl.edu.icm.visnow.system.main.VisNow.availableProcessors();
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++) {
            workThreads[iThread] = new Thread(new Streamline(iThread));
            workThreads[iThread].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }
        nvert = nSrc * (nBackward + nForward);
        lines = new int[2 * nSrc * (nBackward + nForward - 1)];
        coords = new float[3 * nvert];
        indices = new int[nvert];
        vectors = new float[trueDim * nvert];
        for (int i = 0; i < coords.length; i++)
            coords[i] = 0;
        for (int i = 0; i < nSrc; i++) {
            int j = i;
            for (int k = nBackward - 1; k >= 0; k--, j += nSrc) {
                for (int l = 0; l < trueDim; l++) {
                    coords[3 * j + l] = downCoords[i][trueDim * k + l];
                    vectors[j] = downVectors[i][k];
                }
                indices[j] = -k;
            }
            for (int k = 0; k < nForward; k++, j += nSrc) {
                for (int l = 0; l < trueDim; l++) {
                    coords[3 * j + l] = upCoords[i][trueDim * k + l];
                    vectors[j] = upVectors[i][k];
                }
                indices[j] = k;
            }
        }

        downCoords = null;
        upCoords = null;
        downVectors = null;
        upVectors = null;
        for (int i = 0, j = 0; i < nSrc; i++)
            for (int k = 0; k < nBackward + nForward - 1; k++, j += 2) {
                lines[j] = i + k * nSrc;
                lines[j + 1] = lines[j] + nSrc;
            }
        byte[] edgeOrientations = new byte[lines.length / 2];
        for (int i = 0; i < edgeOrientations.length; i++)
            edgeOrientations[i] = 1;
        outField = new IrregularField(nvert);
        outField.setCurrentCoords(new FloatLargeArray(coords));
        DataArray da = DataArray.create(indices, 1, "steps");
        da.recomputeStatistics(true);
        outField.addComponent(da);
        CellArray streamLines = new CellArray(CellType.SEGMENT, lines, edgeOrientations, null);
        CellSet cellSet = new CellSet(inField.getName() + " " + inField.getComponent(parameters.get(COMPONENT)).getName() + " streamlines");
        cellSet.setBoundaryCellArray(streamLines);
        cellSet.setCellArray(streamLines);
        outField.addCellSet(cellSet);
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
    }

    private class Streamline implements Runnable
    {
        private int iThread;
        private SimplexPosition interp = new SimplexPosition(new int[4], new float[4], null);
        private IrregularVectInterpolate vInt = new IrregularVectInterpolate(inField, interp);

        public Streamline(int iThread)
        {
            this.iThread = iThread;
        }

        public void run()
        {
            float[] fs = new float[trueDim];
            int dk = nSrc / nThreads;
            int kstart = iThread * dk + min(iThread, nSrc % nThreads);
            int kend = (iThread + 1) * dk + min(iThread + 1, nSrc % nThreads);
            for (int n = kstart; n < kend; n++) {
                if(iThread == 0)
                    progressAgent.increase();
                if (mask != null && mask[n] == 0) {
                    for (int i = 0; i < downCoords[n].length; i += trueDim) 
                        System.arraycopy(startCoords, 0, downCoords[n], i, trueDim);
                    for (int i = 0; i < upCoords[n].length; i += trueDim) 
                        System.arraycopy(startCoords, 0, upCoords[n], i, trueDim);
                    continue;
                }
                try {
                    System.arraycopy(startCoords, 3 * n, fs, 0, fs.length);
                    fromSteps[n] = RungeKutta.fourthOrderRK(vInt, fs, effectiveScale, nBackward, -1, downCoords[n], downVectors[n]);
                    toSteps[n] = RungeKutta.fourthOrderRK(vInt, fs, effectiveScale, nForward, 1, upCoords[n], upVectors[n]);
                } catch (Exception e) {
                    LOGGER.error("null at " + n + " from " + nSrc);
                    e.printStackTrace();
                }
            }
        }
    }

    private class IrregularVectInterpolate implements Deriv
    {
        private IrregularField fld;
        private SimplexPosition interp;

        public IrregularVectInterpolate(IrregularField fld, SimplexPosition interp)
        {
            this.fld = fld;
            this.interp = interp;
        }

        @Override
        public float[] derivn(float[] y) throws Exception
        {
            if (fld.getFieldCoords(y, interp)) {
                float[] q = new float[]{0, 0, 0};
                for (int i = 0; i < vlen + 1; i++) {
                    int k = vlen * interp.getVertices()[i];
                    for (int j = 0; j < vlen; j++)
                        q[j] += interp.getCoords()[i] * vects[k + j];
                }
                return q;
            } else
                return null;
        }
    }
}
