//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.utilities.Annotations;

import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.LinkFace;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.geometries.parameters.FontParamsShared;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickType;
import static pl.edu.icm.visnow.lib.basic.utilities.Annotations.AnnotationsShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.VisualizationModule;
import pl.edu.icm.visnow.lib.types.VNGeometryObject;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Annotations extends VisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(Annotations.class);
    /**
     * Creates a new instance of CreateGrid
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI ui = null;
    private AnnotationsObject glyphObj = new AnnotationsObject();

    public Annotations()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener() {

            @Override
            public void parameterChanged(String name) {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new GUI();
                setPanel(ui);
                ui.setParameters(parameters);
            }
        });
    }

    @Override
    public void onInitFinishedLocal()
    {
        outObj = glyphObj;
        outObj.setCreator(this);
        outObj.getGeometryObj().setUserData(new ModuleIdData(this));
        outObj2DStruct.setParentModulePort(this.getName() + ".out.outObj");
        setOutputValue("outObj", new VNGeometryObject(outObj, outObj2DStruct));
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {      
        ArrayList<Parameter> p = new ArrayList<>();
        p.add(new Parameter<>(IS_ACTIVE, true));
        p.add(new Parameter<>(COORDS, new float[0]));
        p.add(new Parameter<>(TEXTS, new String[0]));
        p.addAll(FontParamsShared.createDefaultParametersAsList());
        
        return p.toArray(new Parameter[p.size()]);
    }
    
    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        ui.updateGUI(clonedParameterProxy);
    }
            
    @Override
    public void onActive()
    {
        LOGGER.debug(isFromVNA());
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, isFromVNA(), false);
        // (sometimes viewer does not exist when network is loaded)!!! To do - networ flow change. 

        glyphObj.update(p); 
    } 
    

    @Override
    public Pick3DListener getPick3DListener() {
        return pick3DListener;
    }
    
    private void addPoint(float[] p)
    {
        float[] oldCoords = parameters.get(COORDS);
        String[] oldTexts = parameters.get(TEXTS);
        String[] texts = new String[oldTexts.length + 1];
        float[] coords = new float[oldCoords.length + 3];
        System.arraycopy(oldTexts, 0, texts, 1, oldTexts.length);
        System.arraycopy(oldCoords, 0, coords, 3, oldCoords.length);
        texts[0] = String.format("[%.3f, %.3f, %.3f]", p[0], p[1], p[2]);
        System.arraycopy(p, 0, coords, 0, 3);
        parameters.set(TEXTS, texts, COORDS, coords);
    }
    
    /**
     * Pick3DListener
     * <p/>
     * No getter is required if
     * <code>parameters</code> store an object of class
     * <code>Params</code> and that object stores this Pick3DListener and overrides
     * <code>getPick3DListener()</code> method.
     */
    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            float[] x;
            x = e.getPoint(); // x will always be not-null
            if (!parameters.get(IS_ACTIVE))
                return;
            
            addPoint(x);
        }
    };

}
