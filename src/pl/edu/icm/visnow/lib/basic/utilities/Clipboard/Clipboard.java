/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.utilities.Clipboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static pl.edu.icm.visnow.lib.basic.utilities.Clipboard.ClipboardShared.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class Clipboard extends OutFieldVisualizationModule
{

    private static final Map<String, Field> clipboard = new HashMap<>();
    private static List<GUI> guis = new ArrayList<>();
    private GUI computeUI = null;
    private String lastSel = null;

    public Clipboard()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                String sel = parameters.get(SELECTED_FIELD_NAME);
                if (name.equals(SELECTED_FIELD_NAME.getName()) && sel != null && !sel.isEmpty() && !sel.equals(lastSel)) {
                    lastSel = sel;
                    startAction();
                } else if (name.equals(REMOVE.getName()) && sel != null && !sel.isEmpty()) {
                    clipboard.remove(sel);
                    if (sel.equals(lastSel)) {
                        lastSel = null;
                    }
                    updateGUIs();
                    startAction();
                } else if (name.equals(CLEAR.getName())) {
                    clipboard.clear();
                    lastSel = null;
                    updateGUIs();
                    startAction();
                } else if (name.equals(CREATE_GEOMETRIES.getName())) {
                    startAction();
                }
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                guis.add(computeUI);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
                computeUI.setClipboard(clipboard);
            }
        });
    }

    @Override
    public void onDelete()
    {
        guis.remove(computeUI);
    }

    private static void updateGUIs()
    {
        for (GUI gui : guis)
            gui.updateList();
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SELECTED_FIELD_NAME, ""),
            new Parameter<>(REMOVE, false),
            new Parameter<>(CLEAR, false),
            new Parameter<>(CREATE_GEOMETRIES, true)};
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    public static final void add(String name, Field field)
    {
        int i = 0;
        String fieldname = name;
        while (clipboard.containsKey(fieldname)) {
            i++;
            fieldname = name + "(" + Integer.toString(i) + ")";
        }
        Field shallowCopy = field.cloneShallow();
        shallowCopy.setName(fieldname);
        clipboard.put(fieldname, shallowCopy);
        updateGUIs();
    }

    public static final void add(String name, Object array, long[] dims, int vlen)
    {
        if (dims == null || dims.length < 1 || dims.length > 3 || vlen < 1) {
            throw new IllegalArgumentException("dims == null || dims.length < 1 || dims.length > 3 || vlen < 1");
        }
        RegularField field = new RegularField(dims);
        DataArray da = DataArray.create(array, vlen, name, "", null);
        field.addComponent(da);
        add(name, field);
    }

    public static final void add(String name, Object array, int vlen)
    {
        long[] dims = {0};
        if (array instanceof byte[])
            dims[0] = ((byte[]) array).length;
        else if (array instanceof short[])
            dims[0] = ((short[]) array).length;
        else if (array instanceof int[])
            dims[0] = ((int[]) array).length;
        else if (array instanceof long[])
            dims[0] = ((long[]) array).length;
        else if (array instanceof float[])
            dims[0] = ((float[]) array).length;
        else if (array instanceof double[])
            dims[0] = ((double[]) array).length;
        else if (array instanceof String[])
            dims[0] = ((String[]) array).length;
        else if (array instanceof LargeArray)
            dims[0] = ((LargeArray) array).length();

        if (dims[0] > 0 && dims[0] % vlen == 0) {
            dims[0] /= vlen;
            add(name, array, dims, vlen);
        } else {
            throw new IllegalArgumentException("Invalid array type.");
        }
    }

    public static final void add(String name, Object array)
    {
        add(name, array, 1);
    }

    public static final void add(String name, Object array, long[] dims)
    {
        add(name, array, dims, 1);
    }

    public static final Field get(String name)
    {
        return clipboard.get(name);
    }

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, false, false);
        Field tmpField = get(p.get(SELECTED_FIELD_NAME));
        outField = null;
        outRegularField = null;
        outIrregularField = null;
        if (tmpField == null || tmpField.getNComponents() < 1) {            
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
        } else {
            if (p.get(CREATE_GEOMETRIES)) {
                outField = tmpField;                
            }
            if (tmpField instanceof RegularField) {
                setOutputValue("outRegularField", new VNRegularField((RegularField) tmpField));
                setOutputValue("outIrregularField", null);
            } else if (tmpField instanceof IrregularField) {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", new VNIrregularField((IrregularField) tmpField));
            }
        }
        prepareOutputGeometry();
        show();
    }

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

}
