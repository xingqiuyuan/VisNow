package pl.edu.icm.visnow.lib.basic.utilities.Annotations;

import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author norkap
 */


public class AnnotationsShared {
    static final ParameterName<Boolean> IS_ACTIVE = new ParameterName("is active");
    
    static final ParameterName<float[]> COORDS = new ParameterName("coords");
    
    static final ParameterName<String[]> TEXTS = new ParameterName("texts");
    
}
