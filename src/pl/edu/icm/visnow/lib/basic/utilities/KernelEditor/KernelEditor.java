/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.utilities.KernelEditor;

import org.apache.log4j.Logger;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.ModuleCore;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.utilities.KernelEditor.KernelEditorShared.*;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 *
 * @author Michal Lyczek (lyczek@icm.edu.pl)
 * @author Piotr Wendykier (piotrw@icm.edu.pl)
 */
public class KernelEditor extends ModuleCore
{
    private static final Logger LOGGER = Logger.getLogger(KernelEditor.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;
    private Core core = new Core();

    public KernelEditor()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                setPanel(computeUI);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(KERNEL_TYPE, KernelType.CONSTANT),
            new Parameter<>(RANK, 2),
            new Parameter<>(RADIUS, 1),
            new Parameter<>(NORMALIZE_KERNEL, false),
            new Parameter<>(GAUSSIAN_SIGMA, 0.5f),
            new Parameter<>(KERNEL, new float[]{1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f})
        };
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        Parameters p=parameters.getReadOnlyClone();
        

        notifyGUIs(p, isFromVNA(), false);

        core.update(p.get(RADIUS), p.get(RANK), p.get(GAUSSIAN_SIGMA), p.get(NORMALIZE_KERNEL), p.get(KERNEL_TYPE), p.get(KERNEL));

        synchronized (parameters) {
            parameters.setParameterActive(false);
            if (parameters.get(KERNEL_TYPE) != KernelType.CUSTOM)
                parameters.set(KERNEL, core.getOutField().getComponent(0).getRawFloatArray().getData());
            parameters.setParameterActive(true);

            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, isFromVNA(), false);

        setOutputValue("outField", new VNRegularField(core.getOutField()));
    }
}
