//<editor-fold defaultstate="collapsed" desc=" License ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.testdata.TestField;

import pl.edu.icm.visnow.engine.core.ProgressAgent;
import java.util.ArrayList;
import java.util.List;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jscic.TimeData;
import static pl.edu.icm.visnow.lib.basic.testdata.TestField.TestFieldShared.*;

/**
 * 3D data components computational core.
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class TestField3DCore
{
    private final ProgressAgent progressAgent;

    private UnsignedByteLargeArray dataGaussians;
    private FloatLargeArray dataTrig, dataSemielipsoid, dataHopf, dataConst, dataVortex, 
            dataTime0, dataTime1, dataTime2, dataTime3, 
            dataPipe;
    private boolean gaussiansQ;
    private boolean trigFunctionQ;
    private boolean semielipsoidQ;
    private boolean hopfQ;
    private boolean timeVectorFieldQ;
    private boolean constQ;
    private boolean vortexQ;
    private boolean timeFieldQ;
    private boolean pipeQ;

    private TestField3DCore(ProgressAgent progressAgent)
    {
        this.progressAgent = progressAgent;
    }

    private class Compute implements Runnable
    {
        int nThreads;
        int[] dims;
        int iThread;

        public Compute(int nThreads, int[] dims, int iThread)
        {
            this.nThreads = nThreads;
            this.dims = dims;
            this.iThread = iThread;
        }

        @Override
        public void run()
        {
            for (long k = iThread; k < dims[2]; k += nThreads) {
                progressAgent.increase();

                long l = k * dims[0] * dims[1];
                double w = (2. * k - dims[2]) / dims[1];
                for (long j = 0; j < dims[1]; j++) {
                    double v = (2. * j - dims[1]) / dims[1];
                    for (long i = 0; i < dims[0]; i++, l++) {
                        double u = (2. * i - dims[0]) / dims[1];
                        if (gaussiansQ) {
                            dataGaussians.setByte(l, (byte) (0xff & (int) (64 * (3 + (exp(-1.6 * (2 * (u - .3) * (u - .3) + (v - .6) * (v - .6) + (w - .6) * (w - .6))) -
                                                  2 * exp(-.5 * ((u + .6) * (u + .6) + 2 * (v + .4) * (v + .4) + (w - .4) * (w - .6))) -
                                                  exp(-.8 * ((u + .6) * (u + .6) + (v - .6) * (v - .6) + 2 * (w + .2) * (w + .2))) +
                                                  exp(-1.4 * ((u - .3) * (u - .6) + (v + .6) * (v + .6) + (w + .6) * (w + .6))))))));
                        }
                        if (trigFunctionQ) {
                            dataTrig.setFloat(l, (float) (cos(2 * u - 1) * sin(4 * v * w) * cos(3 * u * v * w) + (u * u + v * v + w) / 2));
                        }
                        if (vortexQ) {
                            double r = sqrt(u * u + v * v);
                            double s = 1 / (.1 + (r - .5f) * (r - .5f) + w * w);
                            dataVortex.setFloat(3 * l, (float) (-v * s - u / 2 - w / 4));
                            dataVortex.setFloat(3 * l + 1, (float) (u * s - v / 2));
                            dataVortex.setFloat(3 * l + 2, -(float) (w + u / 5 + .2 / (.05 + u * u + v * v)));
                        }
                        if (semielipsoidQ) {
                            if (u * u + v * v + (w + .5f) * (w + .5f) / 4 < .5 && w + .5 > 0)
                                dataSemielipsoid.setFloat(l, (float) w + .5f);
                        }
                        if (timeFieldQ) {
                            double r0 = .1234567;
                            double r000 = sqrt((u - r0) * (u - r0) + (v - r0) * (v - r0) + (w - r0) * (w - r0));
                            double r001 = sqrt((u - r0) * (u - r0) + (v - r0) * (v - r0) + (w + r0) * (w + r0));
                            double r010 = sqrt((u - r0) * (u - r0) + (v + r0) * (v + r0) + (w - r0) * (w - r0));
                            double r011 = sqrt((u - r0) * (u - r0) + (v + r0) * (v + r0) + (w + r0) * (w + r0));
                            double r100 = sqrt((u + r0) * (u + r0) + (v - r0) * (v - r0) + (w - r0) * (w - r0));
                            double r101 = sqrt((u + r0) * (u + r0) + (v - r0) * (v - r0) + (w + r0) * (w + r0));
                            double r110 = sqrt((u + r0) * (u + r0) + (v + r0) * (v + r0) + (w - r0) * (w - r0));
                            double r111 = sqrt((u + r0) * (u + r0) + (v + r0) * (v + r0) + (w + r0) * (w + r0));
                            dataTime0.setFloat(l, (float) (1 / (4 + u * u + v * v + w * w)));
                            dataTime1.setFloat(l, (float) (1 / (2 + r000) - 1 / (2 + r001)));
                            dataTime2.setFloat(l, (float) (1 / (1 + r000) - 1 / (1+ r001) - 1 / (1 + r010) + 1 / (1 + r011)));
                            dataTime3.setFloat(l, (float) (1 / (.5 + r000) - 1 / (.5 + r001) - 1 / (.5 + r010) + 1 / (.5 + r011) -
                                               1 / (.5 + r100) + 1 / (.5 + r101) + 1 / (.5 + r110) - 1 / (.5 + r111)));
                        }
                        if (hopfQ) {
                            float x1 = 3 * (float)u;
                            float x2 = 3 * (float)v;
                            float x3 = 3 * (float)w;
                            dataHopf.setFloat(3 * l, -x2 - x1 * x3);
                            dataHopf.setFloat(3 * l + 1, x1 - x2 * x3);
                            dataHopf.setFloat(3 * l + 2, .5f * (x1 * x1 + x2 * x2 - x3 * x3 - 1));
                        }
                        if (constQ) {
                            dataConst.setFloat(3 * l, 1);
                        }
                        if (pipeQ) {
                            dataPipe.setFloat(l, (float) (u * u + 2 * v * v + 3 * w * w));
                        }
                    }
                }
            }
        }
    }

    static List<DataArray> createDataArrays(int nThreads, int[] dims, int[] components, ProgressAgent progressAgent) {
        return new TestField3DCore(progressAgent).createDataArrays(nThreads, dims, components);
    }
    
    private List<DataArray> createDataArrays(int nThreads, int[] dims, int[] components)
    {
        // 10/150
        progressAgent.increase((int) dims[2] / 10);
        long n = (long)dims[0] * (long)dims[1] * (long)dims[2];
        for (int i = 0; i < components.length; i++) {
            String componentName = FIELD_NAMES_3D[components[i]];
            if (componentName.equals(GAUSSIANS)) gaussiansQ = true;
            else if (componentName.equals(TRIG_FUNCTION)) trigFunctionQ = true;
            else if (componentName.equals(SEMIELIPSOID)) semielipsoidQ = true;
            else if (componentName.equals(HOPF)) hopfQ = true;
            else if (componentName.equals(CONST)) constQ = true;
            else if (componentName.equals(VORTEX)) vortexQ = true;
            else if (componentName.equals(TIME_VECTOR_FIELD)) timeVectorFieldQ = true;
            else if (componentName.equals(TIME_FIELD)) timeFieldQ = true;
            else if (componentName.equals(PIPE)) pipeQ = true;
            else throw new IllegalStateException("Incorrect field component: " + componentName);
        }
        if (gaussiansQ) dataGaussians = new UnsignedByteLargeArray(n, false);
        if (trigFunctionQ) dataTrig = new FloatLargeArray(n, false);
        if (semielipsoidQ) dataSemielipsoid = new FloatLargeArray(n, false);
        if (hopfQ) dataHopf = new FloatLargeArray(3 * n, false);
        if (constQ) dataConst = new FloatLargeArray(3 * n, false);
        if (vortexQ) dataVortex = new FloatLargeArray(3 * n, false);
        if (timeFieldQ) {
            dataTime0 = new FloatLargeArray(n, false);
            dataTime1 = new FloatLargeArray(n, false);
            dataTime2 = new FloatLargeArray(n, false);
            dataTime3 = new FloatLargeArray(n, false);
        }
        if (pipeQ) dataPipe = new FloatLargeArray(n, false);

        // 20/150
        progressAgent.increase((int) dims[2] / 10);

        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < workThreads.length; i++) {
            workThreads[i] = new Thread(new Compute(nThreads, dims, i));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }

        List<DataArray> dataArrays = new ArrayList<>();

        if (trigFunctionQ) dataArrays.add(DataArray.create(dataTrig, 1, TRIG_FUNCTION));

        if (gaussiansQ) {
            DataArray da = DataArray.create(dataGaussians, 1, GAUSSIANS);
            da.setPreferredRanges(0, 255, -3, 4);
            dataArrays.add(da);
        }
        if (timeFieldQ) {
            DataArray da = DataArray.create(dataTime3, 1, TIME_FIELD);
            da.addRawArray(dataTime2, 1);
            da.addRawArray(dataTime1, 2);
            da.addRawArray(dataTime0, 3);
            dataArrays.add(da);
        }
        if (semielipsoidQ) dataArrays.add(DataArray.create(dataSemielipsoid, 1, SEMIELIPSOID));
        if (vortexQ) dataArrays.add(DataArray.create(dataVortex, 3, VORTEX));
        if (hopfQ) dataArrays.add(DataArray.create(dataHopf, 3, HOPF));

        if (timeVectorFieldQ) {
            ArrayList<LargeArray> vData = new ArrayList<>();
            ArrayList<Float> tSeries = new ArrayList<>();
            for (int m = 0; m < 100; m++) {
                double phi = Math.PI * m / 30.;
                float c = (float)Math.cos(phi), s = (float)Math.sin(phi);
                FloatLargeArray vm = new FloatLargeArray(3 * n);
                for (int i = 0; i < 3 * n; i += 3) {
                    vm.setFloat(i, .01f * c);
                    vm.setFloat(i + 1, .01f * s);
                    vm.setFloat(i + 2, (float)(.001f * phi));
                }
                vData.add(vm);
            }
            for (int i = 0; i < vData.size(); i++)
                tSeries.add(1.f * i);
            TimeData tData = new TimeData(tSeries, vData, 0);
            DataArray da = DataArray.create(tData, 3, "nonstationary_vector_field","", null);
            dataArrays.add(da);
        }
        if (pipeQ) dataArrays.add(DataArray.create(dataPipe, 1, PIPE));
        if (constQ) dataArrays.add(DataArray.create(dataConst, 3, CONST));

        return dataArrays;
    }
    
    
    public static float[] createCurvilinearCoords(int resolution, int[] dims)
    {
        float[] crds = new float[3 * dims[0] * dims[1] * dims[2]];
            for (int k = 0, l = 0; k < dims[2]; k ++) {
                float s0 = (float)Math.sin(PI * k / (2 * resolution) - PI / 4);
                float c0 = (float)Math.cos(PI * k / (2 * resolution) - PI / 4);
                for (int i = 0; i < dims[1]; i++) {
                    float s = (float)Math.sin(PI * i / (2 * resolution));
                    float c = (float)Math.cos(PI * i / (2 * resolution));
                    for (int j = 0; j < dims[0]; j++, l += 3) {
                        float r = (j * .7f) / (resolution - 1.f) + .3f;
                        crds[l]     = c0 * c * r;
                        crds[l + 1] = c0 * s * r;
                        crds[l + 2] = s0 * r;
                    }
                }
            }
            return crds;
        }
}
