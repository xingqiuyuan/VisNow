//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.readers.ReadFluent;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;
import javax.imageio.stream.FileImageInputStream;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw
 * Interdisciplinary Centre for Mathematical and Computational Modelling *
 */
public class FluentReader
{

    private static org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(FluentReader.class);

    private String casFilePath = null;
    private String datFilePath = null;
    private FloatLargeArray coords = null;
    private int nNodes = 0;
    private FluentCell[] cells;
    private int nCells = 0;
    private FluentFace[] faces;
    private int nFaces = 0;
    private String caseBuffer;
    private String dataBuffer;
    private HashMap<Integer, String> variableNames = null;
    private ArrayList<Integer> cellZones = new ArrayList<>();
    private ArrayList<ScalarDataChunk> scalarDataChunks = new ArrayList<>();
    private ArrayList<VectorDataChunk> vectorDataChunks = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> subSectionZones = new ArrayList<>();
    private ArrayList<Integer> subSectionIds = new ArrayList<>();
    private ArrayList<Integer> subSectionSize = new ArrayList<>();
    private ArrayList<Integer> scalarSubSectionIds = new ArrayList<>();
    private ArrayList<Integer> vectorSubSectionIds = new ArrayList<>();
    private FileImageInputStream fluentCaseFile;
    private FileImageInputStream fluentDataFile;
    private ArrayList<String> cellDataArraySelection = new ArrayList<>();
    private int nSpace;
    private int nScalarData = 0;
    private int nVectorData = 0;
    private int nData = 0;

    public FluentReader(String casFilePath, String datFilePath)
    {
        this.casFilePath = casFilePath;
        this.datFilePath = datFilePath;
    }

    public boolean requestInformation()
    {
        if (this.casFilePath == null) {
            System.err.println("CAS FileName has to be specified!");
            return false;
        }

        boolean casFileOpened = this.openCaseFile(casFilePath);
        if (!casFileOpened) {
            System.err.println("Unable to open cas file.");
            return false;
        }

        boolean datFileOpened = false;
        if (datFilePath != null)
            datFileOpened = this.openDataFile(datFilePath);

        fireProgressChanged(0.0f);
        this.variableNames = VariableNames.loadVariableNames();
        try {
            this.parseCaseFile();
            this.cleanCells();
            this.populateCellNodes();
            this.getNumberOfCellZones();
            this.nScalarData = 0;
            this.nVectorData = 0;
            fireProgressChanged(0.5f);
            if (datFileOpened) {
                this.parseDataFile();
                this.closeDataFile();
            }
            if (casFileOpened) {
                this.closeCaseFile();
            }
        } catch (IOException ex) {
            System.err.println("Error reading fluent files.");
            return false;
        }
        this.nData = this.nScalarData + this.nVectorData;

        int ssId, ssVeclen;
        for (int i = 0; i < this.subSectionIds.size(); i++) {
            ssId = this.subSectionIds.get(i);
            ssVeclen = this.subSectionSize.get(i);
            if (ssVeclen == 1) {
                this.cellDataArraySelection.add(this.variableNames.get(ssId));
                this.scalarSubSectionIds.add(ssId);
            } else {
                this.cellDataArraySelection.add(this.variableNames.get(ssId));
                this.vectorSubSectionIds.add(ssId);
            }
        }
        this.nCells = this.cells.length;
        fireProgressChanged(1.0f);
        return true;
    }

    public IrregularField requestData() {
        return requestData(1.0f);
    }
    
    public IrregularField requestData(float coordsRescale)
    {
        if (this.casFilePath == null) {
            System.err.println("FileName has to be specified!");
            return null;
        }
        fireProgressChanged(0.0f);

        IrregularField outField = new IrregularField(nNodes);
        
        if(coordsRescale > 0.0f && coordsRescale != 1.0f) {
            for (int i = 0; i < coords.length(); i++) {
                coords.setFloat(i, coords.get(i)*coordsRescale);            
            }
        }        
        outField.setCurrentCoords(coords);

        CellSet[] cellSets = new CellSet[this.cellZones.size()];
        int zone = 0;
        for (int i = 0; i < this.cellZones.size(); i++) {
            zone = this.cellZones.get(i);
            cellSets[i] = new CellSet("zone_" + zone);
            int[][] csnodeIndices = new int[Cell.getNProperCellTypes()][];
            byte[][] csorientations = new byte[Cell.getNProperCellTypes()][];
            int[][] csdataIndices = new int[Cell.getNProperCellTypes()][];
            CellArray[] cellArrays = new CellArray[Cell.getNProperCellTypes()];
            int[] ncellsoftype = new int[Cell.getNProperCellTypes()];
            for (int j = 0; j < ncellsoftype.length; j++) {
                ncellsoftype[j] = 0;
            }
            FluentCell c;
            boolean error = false;

            //count cell types
            for (int j = 0; j < cells.length; j++) {
                c = cells[j];
                if (c.getZone() == zone) {
                    if (c.getType() == FluentCell.ELEMENT_TYPE_POLYHEDRAL) {
                        //TODO ??
                        error = true;
                        LOGGER.error("Cell zone contains not supported (POLYHEDRAL) cells - skipping");
                        System.err.println("Cell zone contains not supported (POLYHEDRAL) cells - skipping");
                        break;
                    } else if (c.getType() == FluentCell.ELEMENT_TYPE_MIXED) {
                        error = true;
                        //this should not happen!!
                        System.err.println("Cell zone contains not supported (MIXED) cells - skipping");
                        break;
                    }
                    ncellsoftype[FluentCell.mapTypeToVisNow.get(c.getType()).getValue()]++;
                }
            }
            if (error) {
                cellSets[i] = null;
                continue;
            }

            int nCellsInSet = 0;
            for (int j = 0; j < ncellsoftype.length; j++) {
                nCellsInSet += ncellsoftype[j];
            }
            cellSets[i].setNCells(nCellsInSet);

            //allocate arrays
            for (int j = 0; j < Cell.getNProperCellTypes(); j++) {
                if (ncellsoftype[j] > 0) {
                    csnodeIndices[j] = new int[CellType.getType(j).getNVertices() * ncellsoftype[j]];
                    csorientations[j] = new byte[ncellsoftype[j]];
                    csdataIndices[j] = new int[ncellsoftype[j]];
                }
            }

            //fill arrays
            CellType cellType;
            int[] cellNodes;
            int[] count = new int[Cell.getNProperCellTypes()];
            int[] ccount = new int[Cell.getNProperCellTypes()];
            for (int j = 0; j < Cell.getNProperCellTypes(); j++) {
                count[j] = 0;
                ccount[j] = 0;
            }
            for (int j = 0; j < cells.length; j++) {
                c = cells[j];
                if (c.getZone() == zone) {
                    cellType = FluentCell.mapTypeToVisNow.get(c.getType());
                    if (ncellsoftype[cellType.getValue()] > 0) {
                        try {
                            cellNodes = c.getNodes();
                            for (int k = 0; k < cellNodes.length; k++, ccount[cellType.getValue()]++) {
                                csnodeIndices[cellType.getValue()][ccount[cellType.getValue()]] = cellNodes[k];
                                csorientations[cellType.getValue()][count[cellType.getValue()]] = 1;
                                csdataIndices[cellType.getValue()][count[cellType.getValue()]] = j;
                            }
                            count[cellType.getValue()]++;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }

            //create cellarrays
            for (int j = 0; j < Cell.getNProperCellTypes(); j++) {
                if (ncellsoftype[j] > 0) {
                    cellArrays[j] = new CellArray(CellType.getType(j), csnodeIndices[j], csorientations[j], csdataIndices[j]);
                    cellSets[i].addCells(cellArrays[j]);
                }
            }

            //add cell data
            int ssId;
            for (int j = 0; j < scalarDataChunks.size(); j++) {
                if (scalarDataChunks.get(j).getZoneId() == zone) {
                    ssId = scalarDataChunks.get(j).getSubsectionId();
                    String name = this.variableNames.get(ssId);
                    if (name == null)
                        name = "UNKNOWN_" + ssId;
                    cellSets[i].addComponent(DataArray.create(scalarDataChunks.get(j).getScalarData(), 1, name));
                }
            }
            for (int j = 0; j < vectorDataChunks.size(); j++) {
                if (vectorDataChunks.get(j).getZoneId() == zone) {
                    ssId = vectorDataChunks.get(j).getSubsectionId();
                    String name = this.variableNames.get(ssId);
                    if (name == null)
                        name = "UNKNOWN_" + ssId;
                    cellSets[i].addComponent(DataArray.create(vectorDataChunks.get(j).getVectorData(), vectorDataChunks.get(j).getVectorLength(), name));
                }
            }

            fireProgressChanged((float) (i + 1) / (float) this.cellZones.size());
        }

        for (int i = 0; i < cellSets.length; i++) {
            if (cellSets[i] != null) {
                cellSets[i].generateDisplayData(coords);
                outField.addCellSet(cellSets[i]);
            }
        }
        fireProgressChanged(1.0f);
        return outField;
    }

    private boolean openCaseFile(String filename)
    {
        if (filename == null) {
            return false;
        }

        try {
            this.fluentCaseFile = new FileImageInputStream(new File(filename));
            this.fluentCaseFile.setByteOrder(ByteOrder.LITTLE_ENDIAN);
            return (this.fluentCaseFile != null);
        } catch (IOException ex) {
            System.err.println("ERROR: cannot open case file " + filename);
            this.fluentCaseFile = null;
            return false;
        }
    }

    private boolean openDataFile(String filename)
    {
        if (filename == null) {
            return false;
        }
        try {
            this.fluentDataFile = new FileImageInputStream(new File(filename));
            this.fluentDataFile.setByteOrder(ByteOrder.LITTLE_ENDIAN);
            return true;
        } catch (IOException ex) {
            System.err.println("ERROR: cannot open data file " + filename);
            fluentDataFile = null;
            return false;
        }
    }

    private void closeDataFile()
    {
        if (this.fluentDataFile == null) {
            return;
        }

        try {
            this.fluentDataFile.close();
        } catch (IOException ex) {
        }
    }

//    private boolean openDataFiles(String[] filenames)
//    {
//        if (filenames == null) {
//            return false;
//        }
//        fluentDataFiles = new FileImageInputStream[filenames.length];
//        for (int i = 0; i < filenames.length; i++) {
//            try {
//                this.fluentDataFiles[i] = new FileImageInputStream(new File(filenames[i]));
//                this.fluentDataFiles[i].setByteOrder(ByteOrder.LITTLE_ENDIAN);
//            } catch (IOException ex) {
//                fluentDataFiles = null;
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private void closeDataFiles()
//    {
//        for (int i = 0; i < datFilePaths.length; i++) {
//            closeDataFile(i);            
//        }        
//    }    
//    
//    private void closeDataFile(int iFile)
//    {
//        if (this.fluentDataFiles == null) {
//            return;
//        }
//
//        try {
//            this.fluentDataFiles[iFile].close();
//        } catch (IOException ex) {
//        }
//    }
    private void closeCaseFile()
    {
        if (this.fluentCaseFile == null) {
            return;
        }

        try {
            this.fluentCaseFile.close();
        } catch (IOException ex) {
        }
    }

    private boolean getCaseChunk()
    {
        try {
            this.caseBuffer = "";
            SteppedStringBuilder s = new SteppedStringBuilder(100 * 1024 * 1024, 50 * 1024 * 1024);

            int v;
            char c;

            // Find beginning of chunk
            while (true) {
                v = this.fluentCaseFile.read();
                if (v == -1) { //EOF                    
                    return false;
                }
                c = (char) v;
                if (c == '(') {
                    s.append(c);
                    break;
                }
            }

            // Gather index
            String index = "";
            while (true) {
                v = this.fluentCaseFile.read();
                if (v == -1) { //EOF
                    return false;
                }
                c = (char) v;
                s.append(c);
                if (c == ' ') {
                    break;
                }
                index += c;
            }

            // If the index is 3 digits or more, then binary, otherwise ascii.
            if (index.length() > 2) { // Binary Chunk
                String end = "End of Binary Section   " + index + ")";
                int len = end.length();

                // Load the case buffer enough to start comparing to the end.
                while (s.length() < len) {
                    v = this.fluentCaseFile.read();
                    if (v == -1) { //EOF
                        return false;
                    }
                    c = (char) v;
                    s.append(c);
                }

                while (!end.equals(s.substring(s.length() - len))) {
                    v = this.fluentCaseFile.read();
                    if (v == -1) { //EOF
                        return false;
                    }
                    c = (char) v;
                    s.append(c);
                }
            } else { // Ascii Chunk
                int level = 0;
                while (true) {
                    v = this.fluentCaseFile.read();
                    if (v == -1) { //EOF
                        return false;
                    }
                    c = (char) v;
                    s.append(c);
                    if (c == ')' && level == 0) {
                        break;
                    }
                    if (c == '(') {
                        level++;
                    }
                    if (c == ')') {
                        level--;
                    }
                }
            }
            this.caseBuffer = s.toString();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    private boolean getDataChunk()
    {
        try {
            this.dataBuffer = "";
            //StringBuilder s = new StringBuilder(100*1024*1024);            //initial allocated size of sb 100M
            SteppedStringBuilder s = new SteppedStringBuilder(100 * 1024 * 1024, 50 * 1024 * 1024);
            int v;
            char c;

            // Find beginning of chunk
            while (true) {
                v = this.fluentDataFile.read();
                if (v == -1) { //EOF
                    return false;
                }
                c = (char) v;
                if (c == '(') {
                    s.append(c);
                    break;
                }
            }

            // Gather index
            String index = "";
            while (true) {
                v = this.fluentDataFile.read();
                if (v == -1) { //EOF
                    return false;
                }
                c = (char) v;
                s.append(c);

                if (c == ' ') {
                    break;
                }
                index += c;
            }

            // If the index is 3 digits or more, then binary, otherwise ascii.
            if (index.length() > 2) { // Binary Chunk
                String end = "End of Binary Section   " + index + ")";
                int len = end.length();

                // Load the data buffer enough to start comparing to the end std::string.
                while (s.length() < len) {
                    v = this.fluentDataFile.read();
                    if (v == -1) { //EOF
                        return false;
                    }
                    c = (char) v;
                    s.append(c);
                }

                while (!end.equals(s.substring(s.length() - len))) {
                    v = this.fluentDataFile.read();
                    if (v == -1) { //EOF
                        return false;
                    }
                    c = (char) v;
                    s.append(c);
                }
            } else { // Ascii Chunk
                int level = 0;
                while (true) {
                    v = this.fluentDataFile.read();
                    if (v == -1) { //EOF
                        return false;
                    }
                    c = (char) v;
                    s.append(c);
                    if (c == ')' && level == 0) {
                        break;
                    }
                    if (c == '(') {
                        level++;
                    }
                    if (c == ')') {
                        level--;
                    }
                }
            }
            this.dataBuffer = s.toString();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    private int getCaseIndex()
    {
        String sindex = "";
        int i = 1;
        while (this.caseBuffer.charAt(i) != ' ') {
            sindex += this.caseBuffer.charAt(i++);
        }
        return Integer.parseInt(sindex);
    }

    private int getDataIndex()
    {
        String sindex = "";
        int i = 1;
        while (this.dataBuffer.charAt(i) != ' ') {
            sindex += this.dataBuffer.charAt(i++);
        }
        return Integer.parseInt(sindex);
    }

    private int getNSpace()
    {
        int start = this.caseBuffer.indexOf("(", 1);
        String info = this.caseBuffer.substring(start + 4, start + 5);
        return Integer.parseInt(info);
    }

    private void getCasLittleEndianFlag()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        String[] infos = info.split(" ");
        int flag = Integer.parseInt(infos[0]);
        if (flag == 60) {
            fluentCaseFile.setByteOrder(ByteOrder.LITTLE_ENDIAN);
        } else {
            fluentCaseFile.setByteOrder(ByteOrder.BIG_ENDIAN);
        }
    }

//    private void getDatLittleEndianFlag(int iFile)
//    {
//        int start = this.dataBuffer.indexOf('(', 1);
//        int end = this.dataBuffer.indexOf(')', 1);
//        String info = this.dataBuffer.substring(start + 1, end);
//        String[] infos = info.split(" ");
//        int flag = Integer.parseInt(infos[0]);
//        if (flag == 60) {
//            fluentDataFiles[iFile].setByteOrder(ByteOrder.LITTLE_ENDIAN);
//        } else {
//            fluentDataFiles[iFile].setByteOrder(ByteOrder.BIG_ENDIAN);
//        }
//    }
    private void getDatLittleEndianFlag()
    {
        int start = this.dataBuffer.indexOf('(', 1);
        int end = this.dataBuffer.indexOf(')', 1);
        String info = this.dataBuffer.substring(start + 1, end);
        String[] infos = info.split(" ");
        int flag = Integer.parseInt(infos[0]);
        if (flag == 60) {
            fluentDataFile.setByteOrder(ByteOrder.LITTLE_ENDIAN);
        } else {
            fluentDataFile.setByteOrder(ByteOrder.BIG_ENDIAN);
        }
    }

    private int getCaseBufferInt(int off, boolean swap)
    {
        byte[] b = new byte[4];
        for (int j = 0; j < 4; j++) {
            if (swap) {
                b[3 - j] = (byte) this.caseBuffer.charAt(off + j);
            } else {
                b[j] = (byte) this.caseBuffer.charAt(off + j);
            }
        }
        ByteBuffer buf = ByteBuffer.wrap(b);
        return buf.getInt();
    }

    private int getDataBufferInt(int off, boolean swap)
    {
        byte[] b = new byte[4];
        for (int j = 0; j < 4; j++) {
            if (swap) {
                b[3 - j] = (byte) this.dataBuffer.charAt(off + j);
            } else {
                b[j] = (byte) this.dataBuffer.charAt(off + j);
            }
        }
        ByteBuffer buf = ByteBuffer.wrap(b);
        return buf.getInt();
    }

    private float getCaseBufferFloat(int off, boolean swap)
    {
        byte[] b = new byte[4];
        for (int j = 0; j < 4; j++) {
            if (swap) {
                b[3 - j] = (byte) this.caseBuffer.charAt(off + j);
            } else {
                b[j] = (byte) this.caseBuffer.charAt(off + j);
            }
        }
        ByteBuffer buf = ByteBuffer.wrap(b);
        return buf.getFloat();
    }

    private float getDataBufferFloat(int off, boolean swap)
    {
        byte[] b = new byte[4];
        for (int j = 0; j < 4; j++) {
            if (swap) {
                b[3 - j] = (byte) this.dataBuffer.charAt(off + j);
            } else {
                b[j] = (byte) this.dataBuffer.charAt(off + j);
            }
        }
        ByteBuffer buf = ByteBuffer.wrap(b);
        return buf.getFloat();
    }

    private double getCaseBufferDouble(int off, boolean swap)
    {
        byte[] b = new byte[8];
        for (int j = 0; j < 8; j++) {
            if (swap) {
                b[7 - j] = (byte) this.caseBuffer.charAt(off + j);
            } else {
                b[j] = (byte) this.caseBuffer.charAt(off + j);
            }
        }
        ByteBuffer buf = ByteBuffer.wrap(b);
        return buf.getDouble();
    }

    private double getDataBufferDouble(int off, boolean swap)
    {
        byte[] b = new byte[8];
        for (int j = 0; j < 8; j++) {
            if (swap) {
                b[7 - j] = (byte) this.dataBuffer.charAt(off + j);
            } else {
                b[j] = (byte) this.dataBuffer.charAt(off + j);
            }
        }
        ByteBuffer buf = ByteBuffer.wrap(b);
        return buf.getDouble();
    }

    private void getCasNodesAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, type, nd;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        type = Integer.parseInt(infos[3]);
        nd = Integer.parseInt(infos[4]);

        if (zoneId == 0) {
            this.nNodes = lastIndex;
            //this.nodes = new float[nNodes * nSpace];
            this.coords = new FloatLargeArray((long) nNodes * 3);
        } else {
            int dstart = this.caseBuffer.indexOf('(', 5);
            int dend = this.caseBuffer.indexOf(')', dstart + 1);
            String pdata = this.caseBuffer.substring(dstart + 1, dend);
            StringTokenizer st = new StringTokenizer(pdata, " \n", false);
            double x, y, z;
            if (this.nSpace == 3) {
                for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                    x = Double.parseDouble(st.nextToken());
                    y = Double.parseDouble(st.nextToken());
                    z = Double.parseDouble(st.nextToken());
                    this.coords.setFloat(i * 3, (float) x);
                    this.coords.setFloat(i * 3 + 1, (float) y);
                    this.coords.setFloat(i * 3 + 2, (float) z);
                }
            } else {
                for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                    x = Double.parseDouble(st.nextToken());
                    y = Double.parseDouble(st.nextToken());
                    //force 3D field
                    z = 0.0;
                    this.coords.setFloat(i * 3, (float) x);
                    this.coords.setFloat(i * 3 + 1, (float) y);
                    this.coords.setFloat(i * 3 + 2, (float) z);
                }
            }
        }
    }

    private void getCasCellTreeAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int cellId0, cellId1, parentZoneId, childZoneId;
        String[] infos = info.split(" ");
        cellId0 = Integer.parseInt(infos[0], 16);
        cellId1 = Integer.parseInt(infos[1], 16);
        parentZoneId = Integer.parseInt(infos[2], 16);
        childZoneId = Integer.parseInt(infos[3], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int dend = this.caseBuffer.indexOf(')', dstart + 1);
        String pdata = this.caseBuffer.substring(dstart + 1, dend);
        StringTokenizer st = new StringTokenizer(pdata, " \n", false);
        int numberOfKids, kid;
        for (int i = cellId0 - 1; i <= cellId1 - 1; i++) {
            this.cells[i].setParent(1);
            numberOfKids = Integer.parseInt(st.nextToken(), 32);
            for (int j = 0; j < numberOfKids; j++) {
                kid = Integer.parseInt(st.nextToken(), 32) - 1;
                this.cells[kid].setChild(1);
            }
        }
    }

    private void getCasCellTreeBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int cellId0, cellId1, parentZoneId, childZoneId;
        String[] infos = info.split(" ");
        cellId0 = Integer.parseInt(infos[0], 16);
        cellId1 = Integer.parseInt(infos[1], 16);
        parentZoneId = Integer.parseInt(infos[2], 16);
        childZoneId = Integer.parseInt(infos[3], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        int numberOfKids, kid;
        for (int i = cellId0 - 1; i <= cellId1 - 1; i++) {
            this.cells[i].setParent(1);
            numberOfKids = this.getCaseBufferInt(off, swap);
            off = off + 4;
            for (int j = 0; j < numberOfKids; j++) {
                kid = this.getCaseBufferInt(off, swap) - 1;
                off = off + 4;
                this.cells[kid].setChild(1);
            }
        }
    }

    private void getCasCellsAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, type, elementType;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        type = Integer.parseInt(infos[3]);

        if (zoneId == 0) { // Cell Info
            this.nCells = lastIndex;
            cells = new FluentCell[this.nCells];
            for (int i = 0; i < nCells; i++) {
                cells[i] = new FluentCell();
            }
        } else { // Cell Definitions
            elementType = Integer.parseInt(infos[4]);
            if (elementType == FluentCell.ELEMENT_TYPE_MIXED) {
                int dstart = this.caseBuffer.indexOf('(', 5);
                int dend = this.caseBuffer.indexOf(')', dstart + 1);
                String pdata = this.caseBuffer.substring(dstart + 1, dend);
                StringTokenizer st = new StringTokenizer(pdata, " \n", false);
                for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                    FluentCell c = this.cells[i];
                    c.setType(Integer.parseInt(st.nextToken()));
                    c.setZone(zoneId);
                }
            } else {
                for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                    FluentCell c = this.cells[i];
                    c.setType(elementType);
                    c.setZone(zoneId);
                }
            }
        }
    }

    private void getCasCellsBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, type, elementType;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        type = Integer.parseInt(infos[3], 16);
        elementType = Integer.parseInt(infos[4], 16);
        if (elementType == FluentCell.ELEMENT_TYPE_MIXED) {
            int dstart = this.caseBuffer.indexOf('(', 7);
            int off = dstart + 1;
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                FluentCell c = this.cells[i];
                c.setType(this.getCaseBufferInt(off, swap));
                off = off + 4;
                c.setZone(zoneId);
            }
        } else {
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                FluentCell c = this.cells[i];
                c.setType(elementType);
                c.setZone(zoneId);
            }
        }
    }

    private void getCasFacesAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, bcType, faceType;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        bcType = Integer.parseInt(infos[3], 16);

        if (zoneId == 0) { // Face Info
            this.nFaces = lastIndex;
            faces = new FluentFace[nFaces];
            for (int i = 0; i < nFaces; i++) {
                faces[i] = new FluentFace();
            }
        } else { // Face Definitions
            faceType = Integer.parseInt(infos[4], 16);
            int dstart = this.caseBuffer.indexOf('(', 7);
            int dend = this.caseBuffer.indexOf(')', dstart + 1);
            String pdata = this.caseBuffer.substring(dstart + 1, dend);
            StringTokenizer st = new StringTokenizer(pdata, " \n", false);
            int numberOfNodesInFace;
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                if (faceType == FluentFace.FACE_TYPE_MIXED || faceType == FluentFace.FACE_TYPE_POLYGONAL) {
                    numberOfNodesInFace = Integer.parseInt(st.nextToken());
                } else {
                    numberOfNodesInFace = faceType;
                }
                int[] fnodes = new int[numberOfNodesInFace];
                for (int j = 0; j < numberOfNodesInFace; j++) {
                    fnodes[j] = Integer.parseInt(st.nextToken(), 32) - 1;
                }
                FluentFace f = this.faces[i];
                f.setType(faceType);
                f.setNodes(fnodes);
                f.setC0(Integer.parseInt(st.nextToken(), 32) - 1);
                f.setC1(Integer.parseInt(st.nextToken(), 32) - 1);
                f.setZone(zoneId);
                if (f.getC0() >= 0) {
                    this.cells[f.getC0()].addFace(i);
                }
                if (f.getC1() >= 0) {
                    this.cells[f.getC1()].addFace(i);
                }
            }
        }
    }

    private void getCasFacesBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, bcType, faceType, faceType2, c0, c1;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        bcType = Integer.parseInt(infos[3], 16);
        faceType = Integer.parseInt(infos[4], 16);
        if (faceType == FluentFace.FACE_TYPE_POLYGONAL) {
            System.err.println("ERROR: polygonal faces not supported! Skipping.");
            return;
        }
        int dstart = this.caseBuffer.indexOf('(', 7);
        int numberOfNodesInFace = 0;
        int off = dstart + 1;
        for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
            if (faceType == FluentFace.FACE_TYPE_MIXED) {
                numberOfNodesInFace = this.getCaseBufferInt(off, swap);
                faceType2 = numberOfNodesInFace;
                off = off + 4;
            } else {
                numberOfNodesInFace = faceType;
                faceType2 = faceType;
            }
            FluentFace f = this.faces[i];
            int[] fnodes = new int[numberOfNodesInFace];
            for (int k = 0; k < numberOfNodesInFace; k++) {
                fnodes[k] = this.getCaseBufferInt(off, swap) - 1;
                off = off + 4;
            }
            c0 = this.getCaseBufferInt(off, swap) - 1;
            off = off + 4;
            c1 = this.getCaseBufferInt(off, swap) - 1;
            off = off + 4;

            f.setType(faceType2);
            f.setZone(zoneId);
            f.setNodes(fnodes);
            f.setC0(c0);
            f.setC1(c1);
            if (c0 >= 0) {
                this.cells[c0].addFace(i);
            }
            if (c1 >= 0) {
                this.cells[c1].addFace(i);
            }
        }
    }

    private void getCasPeriodicShadowFacesAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int firstIndex, lastIndex, periodicZone, shadowZone;
        String[] infos = info.split(" ");
        firstIndex = Integer.parseInt(infos[0], 16);
        lastIndex = Integer.parseInt(infos[1], 16);
        periodicZone = Integer.parseInt(infos[2], 16);
        shadowZone = Integer.parseInt(infos[3], 16);

        int dstart = this.caseBuffer.indexOf('(', 7);
        int dend = this.caseBuffer.indexOf(')', dstart + 1);
        String pdata = this.caseBuffer.substring(dstart + 1, dend);
        StringTokenizer st = new StringTokenizer(pdata, " \n", false);

        int faceIndex1, faceIndex2;
        for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
            faceIndex1 = Integer.parseInt(st.nextToken(), 32);
            faceIndex2 = Integer.parseInt(st.nextToken(), 32);
            this.faces[faceIndex1].setPeriodicShadow(1);
            ///a po co jest faceindex2 ???
        }
    }

    private void getCasSpeciesVariableNames()
    {
        //Locate the "(species (names" entry
        String variables = this.caseBuffer;
        int startPos = variables.indexOf("(species (names (");
        if (startPos != -1) {
            startPos += 17;
            variables = variables.substring(startPos);
            int endPos = variables.indexOf(")");
            variables = variables.substring(0, endPos);
            StringTokenizer st = new StringTokenizer(variables, " \n", false);
            int iterator = 0;
            while (st.hasMoreTokens()) {
                String temp = st.nextToken();

                this.variableNames.put(200 + iterator, temp);
                this.variableNames.put(250 + iterator, "M1_" + temp);
                this.variableNames.put(300 + iterator, "M2_" + temp);
                this.variableNames.put(450 + iterator, "DPMS_" + temp);
                this.variableNames.put(850 + iterator, "DPMS_DS_" + temp);
                this.variableNames.put(1000 + iterator, "MEAN_" + temp);
                this.variableNames.put(1050 + iterator, "RMS_" + temp);
                this.variableNames.put(1250 + iterator, "CREV_" + temp);
                iterator++;
            }
        }
    }

    private void getCasFaceTreeAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int faceId0, faceId1, parentZoneId, childZoneId;
        String[] infos = info.split(" ");
        faceId0 = Integer.parseInt(infos[0], 16);
        faceId1 = Integer.parseInt(infos[1], 16);
        parentZoneId = Integer.parseInt(infos[2], 16);
        childZoneId = Integer.parseInt(infos[3], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int dend = this.caseBuffer.indexOf(')', dstart + 1);
        String pdata = this.caseBuffer.substring(dstart + 1, dend);
        StringTokenizer st = new StringTokenizer(pdata, " \n", false);
        int numberOfKids, kid;
        for (int i = faceId0 - 1; i <= faceId1 - 1; i++) {
            this.faces[i].setParent(1);
            numberOfKids = Integer.parseInt(st.nextToken(), 32);
            for (int j = 0; j < numberOfKids; j++) {
                kid = Integer.parseInt(st.nextToken(), 32) - 1;
                this.faces[kid].setChild(1);
            }
        }
    }

    private void getCasFaceTreeBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int faceId0, faceId1, parentZoneId, childZoneId;
        String[] infos = info.split(" ");
        faceId0 = Integer.parseInt(infos[0], 16);
        faceId1 = Integer.parseInt(infos[1], 16);
        parentZoneId = Integer.parseInt(infos[2], 16);
        childZoneId = Integer.parseInt(infos[3], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        int numberOfKids, kid;
        for (int i = faceId0 - 1; i <= faceId1 - 1; i++) {
            this.faces[i].setParent(1);
            numberOfKids = this.getCaseBufferInt(off, swap);
            off = off + 4;
            for (int j = 0; j < numberOfKids; j++) {
                kid = this.getCaseBufferInt(off, swap) - 1;
                off = off + 4;
                this.faces[kid].setChild(1);
            }
        }
    }

    private void getCasInterfaceFaceParentsAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int faceId0, faceId1;
        String[] infos = info.split(" ");
        faceId0 = Integer.parseInt(infos[0], 16);
        faceId1 = Integer.parseInt(infos[1], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int dend = this.caseBuffer.indexOf(')', dstart + 1);
        String pdata = this.caseBuffer.substring(dstart + 1, dend);
        StringTokenizer st = new StringTokenizer(pdata, " \n", false);
        int parentId0, parentId1;
        for (int i = faceId0 - 1; i <= faceId1 - 1; i++) {
            parentId0 = Integer.parseInt(st.nextToken(), 32) - 1;
            parentId1 = Integer.parseInt(st.nextToken(), 32) - 1;
            this.faces[parentId0].setInterfaceFaceParent(1);
            this.faces[parentId1].setInterfaceFaceParent(1);
            this.faces[i].setInterfaceFaceChild(1);
        }
    }

    private void getCasInterfaceFaceParentsBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int faceId0, faceId1;
        String[] infos = info.split(" ");
        faceId0 = Integer.parseInt(infos[0], 16);
        faceId1 = Integer.parseInt(infos[1], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        int parentId0, parentId1;
        for (int i = faceId0 - 1; i <= faceId1 - 1; i++) {
            parentId0 = this.getCaseBufferInt(off, swap) - 1;
            off = off + 4;
            parentId1 = this.getCaseBufferInt(off, swap) - 1;
            off = off + 4;
            this.faces[parentId0].setInterfaceFaceParent(1);
            this.faces[parentId1].setInterfaceFaceParent(1);
            this.faces[i].setInterfaceFaceChild(1);
        }
    }

    private void getCasNonconformalGridInterfaceFaceInformationAscii()
    {
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int kidId, parentId, numberOfFaces;
        String[] infos = info.split(" ");
        kidId = Integer.parseInt(infos[0]);
        parentId = Integer.parseInt(infos[1]);
        numberOfFaces = Integer.parseInt(infos[2]);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int dend = this.caseBuffer.indexOf(')', dstart + 1);
        String pdata = this.caseBuffer.substring(dstart + 1, dend);
        StringTokenizer st = new StringTokenizer(pdata, " \n", false);
        int child, parent;
        for (int i = 0; i < numberOfFaces; i++) {
            child = Integer.parseInt(st.nextToken(), 32) - 1;
            parent = Integer.parseInt(st.nextToken(), 32) - 1;
            this.faces[child].setNcgChild(1);
            this.faces[parent].setNcgParent(1);
        }
    }

    private void getCasNonconformalGridInterfaceFaceInformationBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int kidId, parentId, numberOfFaces;
        String[] infos = info.split(" ");
        kidId = Integer.parseInt(infos[0]);
        parentId = Integer.parseInt(infos[1]);
        numberOfFaces = Integer.parseInt(infos[2]);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        int child, parent;
        for (int i = 0; i < numberOfFaces; i++) {
            child = this.getCaseBufferInt(off, swap) - 1;
            off = off + 4;
            parent = this.getCaseBufferInt(off, swap) - 1;
            off = off + 4;
            this.faces[child].setNcgChild(1);
            this.faces[parent].setNcgParent(1);
        }
    }

    private void getCasNodesSinglePrecision()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, type;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        type = Integer.parseInt(infos[3]);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        double x, y, z;
        if (this.nSpace == 3) {
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                x = this.getCaseBufferFloat(off, swap);
                off = off + 4;

                y = this.getCaseBufferFloat(off, swap);
                off = off + 4;

                z = this.getCaseBufferFloat(off, swap);
                off = off + 4;
                this.coords.setFloat(i * 3, (float) x);
                this.coords.setFloat(i * 3 + 1, (float) y);
                this.coords.setFloat(i * 3 + 2, (float) z);
            }
        } else {
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                x = this.getCaseBufferFloat(off, swap);
                off = off + 4;

                y = this.getCaseBufferFloat(off, swap);
                off = off + 4;
                //forced 3D field
                z = 0.0;
                this.coords.setFloat(i * 3, (float) x);
                this.coords.setFloat(i * 3 + 1, (float) y);
                this.coords.setFloat(i * 3 + 2, (float) z);
            }
        }
    }

    private void getCasNodesDoublePrecision()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int zoneId, firstIndex, lastIndex, type;
        String[] infos = info.split(" ");
        zoneId = Integer.parseInt(infos[0], 16);
        firstIndex = Integer.parseInt(infos[1], 16);
        lastIndex = Integer.parseInt(infos[2], 16);
        type = Integer.parseInt(infos[3]);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        double x, y, z;
        if (this.nSpace == 3) {
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                x = this.getCaseBufferDouble(off, swap);
                off = off + 8;

                y = this.getCaseBufferDouble(off, swap);
                off = off + 8;

                z = this.getCaseBufferDouble(off, swap);
                off = off + 8;
                this.coords.setFloat(i * 3, (float) x);
                this.coords.setFloat(i * 3 + 1, (float) y);
                this.coords.setFloat(i * 3 + 2, (float) z);
            }
        } else {
            for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
                x = this.getCaseBufferDouble(off, swap);
                off = off + 8;

                y = this.getCaseBufferDouble(off, swap);
                off = off + 8;
                //forced 3D field
                z = 0.0;
                this.coords.setFloat(i * 3, (float) x);
                this.coords.setFloat(i * 3 + 1, (float) y);
                this.coords.setFloat(i * 3 + 2, (float) z);
            }
        }
    }

    private void getCasPeriodicShadowFacesBinary()
    {
        boolean swap = (fluentCaseFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.caseBuffer.indexOf('(', 1);
        int end = this.caseBuffer.indexOf(')', 1);
        String info = this.caseBuffer.substring(start + 1, end);
        int firstIndex, lastIndex, periodicZone, shadowZone;
        String[] infos = info.split(" ");
        firstIndex = Integer.parseInt(infos[0], 16);
        lastIndex = Integer.parseInt(infos[1], 16);
        periodicZone = Integer.parseInt(infos[2], 16);
        shadowZone = Integer.parseInt(infos[3], 16);
        int dstart = this.caseBuffer.indexOf('(', 7);
        int off = dstart + 1;
        int faceIndex1, faceIndex2;
        for (int i = firstIndex - 1; i <= lastIndex - 1; i++) {
            faceIndex1 = this.getCaseBufferInt(off, swap);
            off = off + 4;
            faceIndex2 = this.getCaseBufferInt(off, swap);
            off = off + 4;
            this.faces[faceIndex1].setPeriodicShadow(1);
            ///a po co jest faceindex2 ???
        }
    }

    private void parseCaseFile() throws IOException
    {
        this.fluentCaseFile.reset();
        this.fluentCaseFile.seek(0);

        while (this.getCaseChunk()) {
            int index = this.getCaseIndex();
            switch (index) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    this.nSpace = this.getNSpace();
                    break;
                case 4:
                    this.getCasLittleEndianFlag();
                    break;
                case 10:
                    this.getCasNodesAscii();
                    break;
                case 12:
                    this.getCasCellsAscii();
                    break;
                case 13:
                    this.getCasFacesAscii();
                    break;
                case 18:
                    this.getCasPeriodicShadowFacesAscii();
                    break;
                case 37:
                    this.getCasSpeciesVariableNames();
                    break;
                case 38:
                    break;
                case 39:
                    break;
                case 40:
                    break;
                case 41:
                    break;
                case 45:
                    break;
                case 58:
                    this.getCasCellTreeAscii();
                    break;
                case 59:
                    this.getCasFaceTreeAscii();
                    break;
                case 61:
                    this.getCasInterfaceFaceParentsAscii();
                    break;
                case 62:
                    this.getCasNonconformalGridInterfaceFaceInformationAscii();
                    break;
                case 63:
                    break;
                case 64:
                    break;
                case 2010:
                    this.getCasNodesSinglePrecision();
                    break;
                case 3010:
                    this.getCasNodesDoublePrecision();
                    break;
                case 2012:
                case 3012:
                    this.getCasCellsBinary();
                    break;
                case 2013:
                case 3013:
                    this.getCasFacesBinary();
                    break;
                case 2018:
                case 3018:
                    this.getCasPeriodicShadowFacesBinary();
                    break;
                case 2040:
                case 3040:
                    break;
                case 2041:
                case 3041:
                    break;
                case 2058:
                case 3058:
                    this.getCasCellTreeBinary();
                    break;
                case 2059:
                case 3059:
                    this.getCasFaceTreeBinary();
                    break;
                case 2061:
                case 3061:
                    this.getCasInterfaceFaceParentsBinary();
                    break;
                case 2062:
                case 3062:
                    this.getCasNonconformalGridInterfaceFaceInformationBinary();
                    break;
                case 2063:
                case 3063:
                    break;
                default:
                    break;
            }
        }
    }

    private void cleanCells()
    {
        ArrayList<Integer> t = new ArrayList<>();
        for (int i = 0; i < cells.length; i++) {
            if (((this.cells[i].getType() == FluentCell.ELEMENT_TYPE_TRIANGULAR) && (this.cells[i].getFaces().size() != 3)) ||
                ((this.cells[i].getType() == FluentCell.ELEMENT_TYPE_TETRAHEDRAL) && (this.cells[i].getFaces().size() != 4)) ||
                ((this.cells[i].getType() == FluentCell.ELEMENT_TYPE_QUADRILATERAL) && (this.cells[i].getFaces().size() != 4)) ||
                ((this.cells[i].getType() == FluentCell.ELEMENT_TYPE_HEXAHEDRAL) && (this.cells[i].getFaces().size() != 6)) ||
                ((this.cells[i].getType() == FluentCell.ELEMENT_TYPE_PYRAMID) && (this.cells[i].getFaces().size() != 5)) ||
                ((this.cells[i].getType() == FluentCell.ELEMENT_TYPE_WEDGE) && (this.cells[i].getFaces().size() != 5))) {
                // Copy faces
                t.clear();
                for (int j = 0; j < (int) this.cells[i].getFaces().size(); j++) {
                    t.add(this.cells[i].getFaces().get(j));
                }

                // Clear Faces
                this.cells[i].getFaces().clear();

                // Copy the faces that are not flagged back into the cell
                for (int j = 0; j < (int) t.size(); j++) {
                    if ((this.faces[t.get(j)].getChild() == 0) &&
                        (this.faces[t.get(j)].getNcgChild() == 0) &&
                        (this.faces[t.get(j)].getInterfaceFaceChild() == 0)) {
                        this.cells[i].addFace(t.get(j));
                    }
                }
            }
        }
    }

    private void populateTriangleCell(int i)
    {
        FluentCell c = this.cells[i];
        int[] cnodes = new int[3];
        if (faces[c.getFaces().get(0)].getC0() == i) {
            cnodes[0] = faces[c.getFaces().get(0)].getNode(0);
            cnodes[1] = faces[c.getFaces().get(0)].getNode(1);
        } else {
            cnodes[1] = faces[c.getFaces().get(0)].getNode(0);
            cnodes[0] = faces[c.getFaces().get(0)].getNode(1);
        }

        if (faces[c.getFaces().get(1)].getNode(0) != cnodes[0] && faces[c.getFaces().get(1)].getNode(0) != cnodes[1]) {
            cnodes[2] = faces[c.getFaces().get(1)].getNode(0);
        } else {
            cnodes[2] = faces[c.getFaces().get(1)].getNode(1);
        }
        c.setNodes(cnodes);
    }

    private void populateTetraCell(int i)
    {
        FluentCell c = this.cells[i];
        int[] cnodes = new int[4];

        if (faces[c.getFaces().get(0)].getC0() == i) {
            cnodes[0] = faces[c.getFaces().get(0)].getNode(0);
            cnodes[1] = faces[c.getFaces().get(0)].getNode(1);
            cnodes[2] = faces[c.getFaces().get(0)].getNode(2);
        } else {
            cnodes[2] = faces[c.getFaces().get(0)].getNode(0);
            cnodes[1] = faces[c.getFaces().get(0)].getNode(1);
            cnodes[0] = faces[c.getFaces().get(0)].getNode(2);
        }

        if (faces[c.getFaces().get(1)].getNode(0) != cnodes[0] &&
            faces[c.getFaces().get(1)].getNode(0) != cnodes[1] &&
            faces[c.getFaces().get(1)].getNode(0) != cnodes[2]) {
            cnodes[3] = faces[c.getFaces().get(1)].getNode(0);
        } else if (faces[c.getFaces().get(1)].getNode(1) != cnodes[0] &&
            faces[c.getFaces().get(1)].getNode(1) != cnodes[1] &&
            faces[c.getFaces().get(1)].getNode(1) != cnodes[2]) {
            cnodes[3] = faces[c.getFaces().get(1)].getNode(1);
        } else {
            cnodes[3] = faces[c.getFaces().get(1)].getNode(2);
        }
        c.setNodes(cnodes);
    }

    private void populateQuadCell(int i)
    {
        FluentCell c = this.cells[i];
        int[] cnodes = new int[4];

        if (faces[c.getFaces().get(0)].getC0() == i) {
            cnodes[0] = faces[c.getFaces().get(0)].getNode(0);
            cnodes[1] = faces[c.getFaces().get(0)].getNode(1);
        } else {
            cnodes[1] = faces[c.getFaces().get(0)].getNode(0);
            cnodes[0] = faces[c.getFaces().get(0)].getNode(1);
        }

        if ((faces[c.getFaces().get(1)].getNode(0) != cnodes[0] &&
            faces[c.getFaces().get(1)].getNode(0) != cnodes[1]) &&
            (faces[c.getFaces().get(1)].getNode(1) != cnodes[0] &&
            faces[c.getFaces().get(1)].getNode(1) != cnodes[1])) {
            if (faces[c.getFaces().get(1)].getC0() == i) {
                cnodes[2] = faces[c.getFaces().get(1)].getNode(0);
                cnodes[3] = faces[c.getFaces().get(1)].getNode(1);
            } else {
                cnodes[3] = faces[c.getFaces().get(1)].getNode(0);
                cnodes[2] = faces[c.getFaces().get(1)].getNode(1);
            }
        } else if ((faces[c.getFaces().get(2)].getNode(0) != cnodes[0] &&
            faces[c.getFaces().get(2)].getNode(0) != cnodes[1]) &&
            (faces[c.getFaces().get(2)].getNode(1) != cnodes[0] &&
            faces[c.getFaces().get(2)].getNode(1) != cnodes[1])) {
            if (faces[c.getFaces().get(2)].getC0() == i) {
                cnodes[2] = faces[c.getFaces().get(2)].getNode(0);
                cnodes[3] = faces[c.getFaces().get(2)].getNode(1);
            } else {
                cnodes[3] = faces[c.getFaces().get(2)].getNode(0);
                cnodes[2] = faces[c.getFaces().get(2)].getNode(1);
            }
        } else {
            if (faces[c.getFaces().get(3)].getC0() == i) {
                cnodes[2] = faces[c.getFaces().get(3)].getNode(0);
                cnodes[3] = faces[c.getFaces().get(3)].getNode(1);
            } else {
                cnodes[3] = faces[c.getFaces().get(3)].getNode(0);
                cnodes[2] = faces[c.getFaces().get(3)].getNode(1);
            }
        }
        c.setNodes(cnodes);
    }

    private void populateHexahedronCell(int i)
    {
        FluentCell c = this.cells[i];
        int[] cnodes = new int[8];

        if (faces[c.getFaces().get(0)].getC0() == i) {
            for (int j = 0; j < 4; j++) {
                cnodes[j] = faces[c.getFaces().get(0)].getNode(j);
            }
        } else {
            for (int j = 3; j >= 0; j--) {
                cnodes[3 - j] = faces[c.getFaces().get(0)].getNode(j);
            }
        }

        //  Look for opposite face of hexahedron
        for (int j = 1; j < 6; j++) {
            int flag = 0;
            for (int k = 0; k < 4; k++) {
                if ((cnodes[0] == faces[c.getFaces().get(j)].getNode(k)) ||
                    (cnodes[1] == faces[c.getFaces().get(j)].getNode(k)) ||
                    (cnodes[2] == faces[c.getFaces().get(j)].getNode(k)) ||
                    (cnodes[3] == faces[c.getFaces().get(j)].getNode(k))) {
                    flag = 1;
                }
            }
            if (flag == 0) {
                if (faces[c.getFaces().get(j)].getC1() == i) {
                    for (int k = 4; k < 8; k++) {
                        cnodes[k] = faces[c.getFaces().get(j)].getNode(k - 4);
                    }
                } else {
                    for (int k = 7; k >= 4; k--) {
                        cnodes[k] = faces[c.getFaces().get(j)].getNode(7 - k);
                    }
                }
            }
        }

        //  Find the face with points 0 and 1 in them.
        int[] f01 = new int[]{-1, -1, -1, -1};
        for (int j = 1; j < 6; j++) {
            int flag0 = 0;
            int flag1 = 0;
            for (int k = 0; k < 4; k++) {
                if (cnodes[0] == faces[c.getFaces().get(j)].getNode(k)) {
                    flag0 = 1;
                }
                if (cnodes[1] == faces[c.getFaces().get(j)].getNode(k)) {
                    flag1 = 1;
                }
            }
            if ((flag0 == 1) && (flag1 == 1)) {
                if (faces[c.getFaces().get(j)].getC0() == i) {
                    for (int k = 0; k < 4; k++) {
                        f01[k] = faces[c.getFaces().get(j)].getNode(k);
                    }
                } else {
                    for (int k = 3; k >= 0; k--) {
                        f01[k] = faces[c.getFaces().get(j)].getNode(k);
                    }
                }
            }
        }

        //  Find the face with points 0 and 3 in them.
        int[] f03 = new int[]{-1, -1, -1, -1};
        for (int j = 1; j < 6; j++) {
            int flag0 = 0;
            int flag1 = 0;
            for (int k = 0; k < 4; k++) {
                if (cnodes[0] == faces[c.getFaces().get(j)].getNode(k)) {
                    flag0 = 1;
                }
                if (cnodes[3] == faces[c.getFaces().get(j)].getNode(k)) {
                    flag1 = 1;
                }
            }

            if ((flag0 == 1) && (flag1 == 1)) {
                if (faces[c.getFaces().get(j)].getC0() == i) {
                    for (int k = 0; k < 4; k++) {
                        f03[k] = faces[c.getFaces().get(j)].getNode(k);
                    }
                } else {
                    for (int k = 3; k >= 0; k--) {
                        f03[k] = faces[c.getFaces().get(j)].getNode(k);
                    }
                }
            }
        }

        // What point is in f01 and f03 besides 0 ... this is point 4
        int p4 = 0;
        for (int k = 0; k < 4; k++) {
            if (f01[k] != cnodes[0]) {
                for (int n = 0; n < 4; n++) {
                    if (f01[k] == f03[n]) {
                        p4 = f01[k];
                    }
                }
            }
        }

        // Since we know point 4 now we check to see if points
        //  4, 5, 6, and 7 are in the correct positions.
        int[] t = new int[8];
        t[4] = cnodes[4];
        t[5] = cnodes[5];
        t[6] = cnodes[6];
        t[7] = cnodes[7];
        if (p4 == cnodes[5]) {
            cnodes[5] = t[6];
            cnodes[6] = t[7];
            cnodes[7] = t[4];
            cnodes[4] = t[5];
        } else if (p4 == cnodes[6]) {
            cnodes[5] = t[7];
            cnodes[6] = t[4];
            cnodes[7] = t[5];
            cnodes[4] = t[6];
        } else if (p4 == cnodes[7]) {
            cnodes[5] = t[4];
            cnodes[6] = t[5];
            cnodes[7] = t[6];
            cnodes[4] = t[7];
        }
        // else point 4 was lined up so everything was correct.
        c.setNodes(cnodes);
    }

    private void populatePyramidCell(int i)
    {
        FluentCell c = this.cells[i];
        int[] cnodes = new int[5];
        //  The quad face will be the base of the pyramid
        for (int j = 0; j < c.getFaces().size(); j++) {
            if (faces[c.getFaces().get(j)].getNodes().length == 4) {
                if (faces[c.getFaces().get(j)].getC0() == i) {
                    for (int k = 0; k < 4; k++) {
                        cnodes[k] = faces[c.getFaces().get(j)].getNode(k);
                    }
                } else {
                    for (int k = 0; k < 4; k++) {
                        cnodes[3 - k] = faces[c.getFaces().get(j)].getNode(k);
                    }
                }
            }
        }

        // Just need to find point 4
        for (int j = 0; j < c.getFaces().size(); j++) {
            if (faces[c.getFaces().get(j)].getNodes().length == 3) {
                for (int k = 0; k < 3; k++) {
                    if ((faces[c.getFaces().get(j)].getNode(k) != cnodes[0]) &&
                        (faces[c.getFaces().get(j)].getNode(k) != cnodes[1]) &&
                        (faces[c.getFaces().get(j)].getNode(k) != cnodes[2]) &&
                        (faces[c.getFaces().get(j)].getNode(k) != cnodes[3])) {
                        cnodes[4] = faces[c.getFaces().get(j)].getNode(k);
                    }
                }
            }
        }
        c.setNodes(cnodes);
    }

    private void populateWedgeCell(int i)
    {
        FluentCell c = this.cells[i];
        int[] cnodes = new int[6];

        //  Find the first triangle face and make it the base.
        FluentFace base = faces[0];
        int first = 0;
        for (int j = 0; j < c.getFaces().size(); j++) {
            if ((faces[c.getFaces().get(j)].getType() == FluentFace.FACE_TYPE_TRIANGULAR) && (first == 0)) {
                base = faces[c.getFaces().get(j)];
                first = 1;
            }
        }

        //  Find the second triangle face and make it the top.
        FluentFace top = faces[0];
        int second = 0;
        for (int j = 0; j < c.getFaces().size(); j++) {
            if ((faces[c.getFaces().get(j)].getType() == FluentFace.FACE_TYPE_TRIANGULAR) && (second == 0) && (faces[c.getFaces().get(j)] != base)) {
                top = faces[c.getFaces().get(j)];
                second = 1;
            }
        }

        // Load Base nodes into the nodes std::vector
        if (base.getC0() == i) {
            for (int j = 0; j < 3; j++) {
                cnodes[j] = base.getNode(j);
            }
        } else {
            for (int j = 2; j >= 0; j--) {
                cnodes[2 - j] = base.getNode(j);
            }
        }
        // Load Top nodes into the nodes std::vector
        if (top.getC1() == i) {
            for (int j = 3; j < 6; j++) {
                cnodes[j] = top.getNode(j - 3);
            }
        } else {
            for (int j = 3; j < 6; j++) {
                cnodes[j] = top.getNode(5 - j);
            }
        }
        //  Find the quad face with points 0 and 1 in them.
        int[] w01 = new int[4];
        for (int j = 0; j < c.getFaces().size(); j++) {
            if (faces[c.getFaces().get(j)] != base && faces[c.getFaces().get(j)] != top) {
                int wf0 = 0;
                int wf1 = 0;
                for (int k = 0; k < 4; k++) {
                    if (cnodes[0] == faces[c.getFaces().get(j)].getNode(k)) {
                        wf0 = 1;
                    }
                    if (cnodes[1] == faces[c.getFaces().get(j)].getNode(k)) {
                        wf1 = 1;
                    }
                    if ((wf0 == 1) && (wf1 == 1)) {
                        for (int n = 0; n < 4; n++) {
                            w01[n] = faces[c.getFaces().get(j)].getNode(n);
                        }
                    }
                }
            }
        }
        //  Find the quad face with points 0 and 2 in them.
        int[] w02 = new int[]{-1, -1, -1, -1};
        for (int j = 0; j < c.getFaces().size(); j++) {
            if (faces[c.getFaces().get(j)] != base && faces[c.getFaces().get(j)] != top) {
                int wf0 = 0;
                int wf2 = 0;
                for (int k = 0; k < 4; k++) {
                    if (cnodes[0] == faces[c.getFaces().get(j)].getNode(k)) {
                        wf0 = 1;
                    }
                    if (cnodes[2] == faces[c.getFaces().get(j)].getNode(k)) {
                        wf2 = 1;
                    }
                    if ((wf0 == 1) && (wf2 == 1)) {
                        for (int n = 0; n < 4; n++) {
                            w02[n] = faces[c.getFaces().get(j)].getNode(n);
                        }
                    }
                }
            }
        }
        // Point 3 is the point that is in both w01 and w02
        // What point is in f01 and f02 besides 0 ... this is point 3
        int p3 = 0;
        for (int k = 0; k < 4; k++) {
            if (w01[k] != cnodes[0]) {
                for (int n = 0; n < 4; n++) {
                    if (w01[k] == w02[n]) {
                        p3 = w01[k];
                    }
                }
            }
        }
        // Since we know point 3 now we check to see if points
        //  3, 4, and 5 are in the correct positions.
        int[] t = new int[6];
        t[3] = cnodes[3];
        t[4] = cnodes[4];
        t[5] = cnodes[5];
        if (p3 == cnodes[4]) {
            cnodes[3] = t[4];
            cnodes[4] = t[5];
            cnodes[5] = t[3];
        } else if (p3 == cnodes[5]) {
            cnodes[3] = t[5];
            cnodes[4] = t[3];
            cnodes[5] = t[4];
        }
        // else point 3 was lined up so everything was correct.

        c.setNodes(cnodes);
    }

    //    private void populatePolyhedronCell(int i) {
    //        FluentCell c = this.cells[i];
    //        ArrayList<Integer> cnodes = new ArrayList<>();
    //        for (int j = 0; j < c.getFaces().size(); j++) {
    //            int k;
    //            for (k = 0; k < (int) faces[c.getFaces().get(j)].getNodes().length; k++) {
    //                int flag;
    //                flag = 0;
    //                // Is the node already in the cell?
    //                for (int n = 0; n < cnodes.size(); n++) {
    //                    if (cnodes.get(n) == faces[c.getFaces().get(j)].getNode(k)) {
    //                        flag = 1;
    //                    }
    //                }
    //                if (flag == 0) {
    //                    //No match - insert node into cell.
    //                    cnodes.add(faces[c.getFaces().get(j)].getNode(k));
    //                }
    //            }
    //        }
    //
    //        int[] cnodesArr = new int[cnodes.size()];
    //        for (int j = 0; j < cnodesArr.length; j++) {
    //            cnodesArr[j] = cnodes.get(j);
    //        }
    //        c.setNodes(cnodesArr);
    //    }
    private void populateCellNodes()
    {
        for (int i = 0; i < this.cells.length; i++) {
            switch (this.cells[i].getType()) {
                case FluentCell.ELEMENT_TYPE_TRIANGULAR:
                    this.populateTriangleCell(i);
                    break;
                case FluentCell.ELEMENT_TYPE_TETRAHEDRAL:
                    this.populateTetraCell(i);
                    break;
                case FluentCell.ELEMENT_TYPE_QUADRILATERAL:
                    this.populateQuadCell(i);
                    break;
                case FluentCell.ELEMENT_TYPE_HEXAHEDRAL:
                    this.populateHexahedronCell(i);
                    break;
                case FluentCell.ELEMENT_TYPE_PYRAMID:
                    this.populatePyramidCell(i);
                    break;
                case FluentCell.ELEMENT_TYPE_WEDGE:
                    this.populateWedgeCell(i);
                    break;
                case FluentCell.ELEMENT_TYPE_POLYHEDRAL:
                    //this.populatePolyhedronCell(i);
                    System.err.println("POLYHEDRAL cells not supported!");
                    break;
            }
        }
    }

    private void getNumberOfCellZones()
    {
        int match;
        for (int i = 0; i < this.cells.length; i++) {
            if (this.cellZones.isEmpty()) {
                this.cellZones.add(this.cells[i].getZone());
            } else {
                match = 0;
                for (int j = 0; j < this.cellZones.size(); j++) {
                    if (this.cellZones.get(j) == this.cells[i].getZone()) {
                        match = 1;
                    }
                }
                if (match == 0) {
                    this.cellZones.add(this.cells[i].getZone());
                }
            }
        }
    }

    private void getDataASCII()
    {
        int start = this.dataBuffer.indexOf('(', 1);
        int end = this.dataBuffer.indexOf(')', 1);
        String info = this.dataBuffer.substring(start + 1, end);
        int subSectionId, zoneId, size, nTimeLevels, nPhases, firstId, lastId;
        String[] infos = info.split(" ");
        subSectionId = Integer.parseInt(infos[0]);
        zoneId = Integer.parseInt(infos[1]);
        size = Integer.parseInt(infos[2]);
        nTimeLevels = Integer.parseInt(infos[3]);
        nPhases = Integer.parseInt(infos[4]);
        firstId = Integer.parseInt(infos[5]);
        lastId = Integer.parseInt(infos[6]);

        boolean zmatch = false;
        for (int i = 0; i < this.cellZones.size(); i++) {
            if (this.cellZones.get(i) == zoneId) {
                zmatch = true;
                break;
            }
        }

        if (zmatch) {
            int dstart = this.dataBuffer.indexOf('(', 7);
            int dend = this.dataBuffer.indexOf(')', dstart + 1);
            String pdata = this.dataBuffer.substring(dstart + 1, dend);
            StringTokenizer st = new StringTokenizer(pdata, " \n", false);
            boolean match = false;
            for (int i = 0; i < this.subSectionIds.size(); i++) {
                if (subSectionId == this.subSectionIds.get(i)) {
                    match = true;
                    break;
                }
            }

            if (!match && (size < 4)) {
                this.subSectionIds.add(subSectionId);
                this.subSectionSize.add(size);
                this.subSectionZones.add(new ArrayList<Integer>());
                this.subSectionZones.get(this.subSectionZones.size() - 1).add(zoneId);
            }

            if (size == 1) {
                this.nScalarData++;
                ScalarDataChunk sdc = new ScalarDataChunk();
                sdc.setSubsectionId(subSectionId);
                sdc.setZoneId(zoneId);
                double[] data = new double[lastId - firstId + 1];
                for (int i = 0; i < data.length; i++) {
                    data[i] = Double.parseDouble(st.nextToken());
                }
                sdc.setScalarData(data);
                this.scalarDataChunks.add(sdc);
            } else {
                this.nVectorData++;
                VectorDataChunk vdc = new VectorDataChunk();
                vdc.setSubsectionId(subSectionId);
                vdc.setZoneId(zoneId);
                int N = (lastId - firstId + 1);
                double[] data = new double[size * N];
                for (int i = 0; i < N; i++) {
                    for (int v = 0; v < size; v++) {
                        data[size * i + v] = Double.parseDouble(st.nextToken());
                    }
                }
                vdc.setVectorData(data);
                vdc.setVeclen(size);
                vectorDataChunks.add(vdc);
            }
        }
    }

    private void getDataFloat()
    {
        boolean swap = (fluentDataFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.dataBuffer.indexOf('(', 1);
        int end = this.dataBuffer.indexOf(')', 1);
        String info = this.dataBuffer.substring(start + 1, end);
        int subSectionId, zoneId, size, nTimeLevels, nPhases, firstId, lastId;
        String[] infos = info.split(" ");
        subSectionId = Integer.parseInt(infos[0]);
        zoneId = Integer.parseInt(infos[1]);
        size = Integer.parseInt(infos[2]);
        nTimeLevels = Integer.parseInt(infos[3]);
        nPhases = Integer.parseInt(infos[4]);
        firstId = Integer.parseInt(infos[5]);
        lastId = Integer.parseInt(infos[6]);

        boolean zmatch = false;
        for (int i = 0; i < this.cellZones.size(); i++) {
            if (this.cellZones.get(i) == zoneId) {
                zmatch = true;
                break;
            }
        }

        if (zmatch) {
            int dstart = this.dataBuffer.indexOf('(', 7);
            int off = dstart + 1;

            boolean match = false;
            for (int i = 0; i < this.subSectionIds.size(); i++) {
                if (subSectionId == this.subSectionIds.get(i)) {
                    match = true;
                    break;
                }
            }

            if (!match && (size < 4)) {
                this.subSectionIds.add(subSectionId);
                this.subSectionSize.add(size);
                this.subSectionZones.add(new ArrayList<Integer>());
                this.subSectionZones.get(this.subSectionZones.size() - 1).add(zoneId);
            }

            if (size == 1) {
                this.nScalarData++;
                ScalarDataChunk sdc = new ScalarDataChunk();
                sdc.setSubsectionId(subSectionId);
                sdc.setZoneId(zoneId);
                double[] data = new double[lastId - firstId + 1];
                for (int i = 0; i < data.length; i++) {
                    data[i] = this.getDataBufferFloat(off, swap);
                    off = off + 4;
                }
                sdc.setScalarData(data);
                this.scalarDataChunks.add(sdc);
            } else {
                this.nVectorData++;
                VectorDataChunk vdc = new VectorDataChunk();
                vdc.setSubsectionId(subSectionId);
                vdc.setZoneId(zoneId);
                int N = (lastId - firstId + 1);
                double[] data = new double[size * N];
                for (int i = 0; i < N; i++) {
                    for (int v = 0; v < size; v++) {
                        data[size * i + v] = this.getDataBufferFloat(off, swap);
                        off = off + 4;
                    }
                }
                vdc.setVectorData(data);
                vdc.setVeclen(size);
                vectorDataChunks.add(vdc);
            }
        }
    }

    private void getDataDouble()
    {
        boolean swap = (fluentDataFile.getByteOrder() == ByteOrder.LITTLE_ENDIAN);
        int start = this.dataBuffer.indexOf('(', 1);
        int end = this.dataBuffer.indexOf(')', 1);
        String info = this.dataBuffer.substring(start + 1, end);
        int subSectionId, zoneId, size, nTimeLevels, nPhases, firstId, lastId;
        String[] infos = info.split(" ");
        subSectionId = Integer.parseInt(infos[0]);
        zoneId = Integer.parseInt(infos[1]);
        size = Integer.parseInt(infos[2]);
        nTimeLevels = Integer.parseInt(infos[3]);
        nPhases = Integer.parseInt(infos[4]);
        firstId = Integer.parseInt(infos[5]);
        lastId = Integer.parseInt(infos[6]);

        boolean zmatch = false;
        for (int i = 0; i < this.cellZones.size(); i++) {
            if (this.cellZones.get(i) == zoneId) {
                zmatch = true;
                break;
            }
        }

        if (zmatch) {
            int dstart = this.dataBuffer.indexOf('(', 7);
            int off = dstart + 1;

            boolean match = false;
            for (int i = 0; i < this.subSectionIds.size(); i++) {
                if (subSectionId == this.subSectionIds.get(i)) {
                    match = true;
                    break;
                }
            }

            if (!match && (size < 4)) {
                this.subSectionIds.add(subSectionId);
                this.subSectionSize.add(size);
                this.subSectionZones.add(new ArrayList<Integer>());
                this.subSectionZones.get(this.subSectionZones.size() - 1).add(zoneId);
            }

            if (size == 1) {
                this.nScalarData++;
                ScalarDataChunk sdc = new ScalarDataChunk();
                sdc.setSubsectionId(subSectionId);
                sdc.setZoneId(zoneId);
                double[] data = new double[lastId - firstId + 1];
                for (int i = 0; i < data.length; i++) {
                    data[i] = this.getDataBufferDouble(off, swap);
                    off = off + 8;
                }
                sdc.setScalarData(data);
                this.scalarDataChunks.add(sdc);
            } else {
                this.nVectorData++;
                VectorDataChunk vdc = new VectorDataChunk();
                vdc.setSubsectionId(subSectionId);
                vdc.setZoneId(zoneId);
                int N = (lastId - firstId + 1);
                double[] data = new double[size * N];
                for (int i = 0; i < N; i++) {
                    for (int v = 0; v < size; v++) {
                        data[size * i + v] = this.getDataBufferDouble(off, swap);
                        off = off + 8;
                    }
                }
                vdc.setVectorData(data);
                vdc.setVeclen(size);
                vectorDataChunks.add(vdc);
            }
        }
    }

    private void parseDataFile() throws IOException
    {
        if (fluentDataFile == null)
            return;

        this.fluentDataFile.reset();
        this.fluentDataFile.seek(0);
        while (this.getDataChunk()) {
            int index = this.getDataIndex();
            switch (index) {
                case 0:
                    break;
                case 4:
                    getDatLittleEndianFlag();
                    break;
                case 33:
                    break;
                case 37:
                    break;
                case 300:
                    getDataASCII();
                    break;
                case 301:
                    break;
                case 302:
                    break;
                case 2300:
                    getDataFloat();
                    break;
                case 2301:
                    break;
                case 2302:
                    break;
                case 3300:
                    getDataDouble();
                    break;
                case 3301:
                    break;
                case 3302:
                    break;
                default:
                    break;
            }
        }
    }

    protected transient FloatValueModificationListener progressListener = null;

    public void setFloatValueModificationListener(FloatValueModificationListener listener)
    {
        this.progressListener = listener;
    }

    public void clearFloatValueModificationListener()
    {
        progressListener = null;
    }

    protected void fireProgressChanged(float progress)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, progress, true);
        if (progressListener != null)
            progressListener.floatValueChanged(e);
    }

    private class SteppedStringBuilder
    {

        private char[] data;
        private int count = 0;
        private int step;

        public SteppedStringBuilder()
        {
            this(256);
        }

        public SteppedStringBuilder(int size)
        {
            this(size, 256);
        }

        public SteppedStringBuilder(int size, int step)
        {
            this.step = step;
            data = new char[size];
        }

        public void append(char c)
        {
            if (count + 1 > data.length) {
                expandCapacity();
            }
            data[count++] = c;
        }

        public void expandCapacity()
        {
            data = Arrays.copyOf(data, data.length + step);
        }

        public int length()
        {
            return count;
        }

        @Override
        public String toString()
        {
            return new String(data, 0, count);
        }

        public String substring(int start)
        {
            return substring(start, count);
        }

        public String substring(int start, int end)
        {
            if (start < 0)
                throw new StringIndexOutOfBoundsException(start);
            if (end > count)
                throw new StringIndexOutOfBoundsException(end);
            if (start > end)
                throw new StringIndexOutOfBoundsException(end - start);
            return new String(data, start, end - start);
        }

    }

}
