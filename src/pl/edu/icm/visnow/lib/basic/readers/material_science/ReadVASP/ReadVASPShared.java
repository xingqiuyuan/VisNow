package pl.edu.icm.visnow.lib.basic.readers.material_science.ReadVASP;

import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author norkap
 */


public class ReadVASPShared {
    
    static final ParameterName<String> FILENAME = new ParameterName("File name");
    
    static final ParameterName<Integer> TYPE = new ParameterName("Type");
    
    static final ParameterName<int[]> MULT = new ParameterName("Multiplicity");
}
