/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.readers.ReadSTL;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.j3d.loaders.stl.STLFileReader;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.utils.VectorMath;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import static pl.edu.icm.visnow.lib.basic.readers.ReadSTL.ReadSTLShared.PATH;
import pl.edu.icm.visnow.lib.grid.gridftp.GridFTPException;
import pl.edu.icm.visnow.lib.grid.gridftp.GridFTPLocalUtils;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;

/**
 * @author theki
 */
public class ReadSTL extends OutFieldVisualizationModule {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadSTL.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;

    /**
     * Creates a new instance of STLReader
     */
    public ReadSTL() {
        parameters.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable() {
            @Override
            public void run() {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    public void onActive() {
        Parameters parametersClone = parameters.getReadOnlyClone();
        notifyGUIs(parametersClone, false, false);

        String path = parametersClone.get(PATH);
        if (path.isEmpty()) {
            outIrregularField = null;
        } else {
            try {
                ProgressAgent progressAgent = getProgressAgent(140); //20 for download from gsiftp, 100 for read, 20 for geometry
                File f = GridFTPLocalUtils.getFileOrLocalGSIFTPCopy(path, progressAgent.getSubAgent(20));
                outIrregularField = readSTL(f, progressAgent);
                if (outIrregularField != null)
                    VisNow.get().userMessageSend(this, "Successfully read: " + path, "", Level.INFO);
            } catch (IOException | GridFTPException ex) {
                outIrregularField = null;
                LOGGER.error("", ex);
                VisNow.get().userMessageSend(this, "Error reading file: " + path, "See log for details", Level.ERROR);
            }
        }
        LOGGER.debug("Preparing output geometry");
        outField = outIrregularField;
        if (outIrregularField != null) {
            setOutputValue("outField", new VNIrregularField(outIrregularField));
        } else {
            setOutputValue("outField", null);
        }
        prepareOutputGeometry();
        show();
    }

    @Override
    protected Parameter[] createDefaultParameters() {
        return new Parameter[]{
            new Parameter<>(PATH, new String()),};
    }

    @Override
    protected void notifySwingGUIs(final pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending) {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onInitFinishedLocal() {
        if (isForceFlag()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    computeUI.activateOpenDialog();
                }
            });
        }
    }

    @Override
    public boolean isGenerator() {
        return true;
    }

    public static IrregularField readSTL(File f, ProgressAgent progressAgent) throws IOException {
        IrregularField out = null;
        STLFileReader reader;
        try {
            reader = new STLFileReader(f);
        } catch (Exception ex) {
            LOGGER.error(ex);
            return null;

        }
        double[] normal = new double[3];
        double[][] vertices = new double[3][3];

        int nObjects = reader.getNumOfObjects();
        String[] objectNames = reader.getObjectNames();
        int[] nFacets = reader.getNumOfFacets();
        int[][] cellsIndices = new int[nObjects][];
        HashMap<Node, Integer> vertexMap = new HashMap<Node, Integer>();

        int progressTotalSteps = VectorMath.vectorSum(nFacets);
        int progressStep = 0;

        int progressMod = progressTotalSteps / 100;
        if (progressMod == 0) {
            progressMod = 1;
        }

        int nodeCounter = 0;
        for (int n = 0; n < nObjects; n++) {
            cellsIndices[n] = new int[3 * nFacets[n]];
            for (int cellIndex = 0; cellIndex < nFacets[n]; cellIndex++) {
                if (!reader.getNextFacet(normal, vertices)) {
                    throw new IllegalStateException("ERROR: not enough facets in STL file.");
                }
                for (int i = 0; i < 3; i++) {
                    float[] p = new float[3];
                    for (int j = 0; j < p.length; j++) {
                        p[j] = (float) vertices[i][j];
                    }
                    Node node = new Node(p);
                    if (!vertexMap.containsKey(node)) {
                        vertexMap.put(node, nodeCounter);
                        cellsIndices[n][3 * cellIndex + i] = nodeCounter;
                        nodeCounter++;
                    } else {
                        cellsIndices[n][3 * cellIndex + i] = vertexMap.get(node);
                    }
                }
                if (progressStep++ % progressMod == 0) {
                    progressAgent.setProgressStep(20 + 100 * progressStep / progressTotalSteps);
                }
            }
        }
        reader.close();

        LOGGER.debug("File read finished");

        Set<Node> nodeSet = vertexMap.keySet();
        int nNodes = vertexMap.size();
        FloatLargeArray coords = new FloatLargeArray(3 * (long) nNodes, false);
        Iterator<Node> nodeIterator = nodeSet.iterator();
        int i;
        while (nodeIterator.hasNext()) {
            Node n = nodeIterator.next();
            i = vertexMap.get(n);
            LargeArrayUtils.arraycopy(n.getCoords(), 0, coords, 3 * i, 3);
        }
        vertexMap.clear();

        vertexMap = null;
        Runtime.getRuntime().gc();

        out = new IrregularField(nNodes);
        out.setCurrentCoords(coords);
        byte[] dummy = new byte[nNodes];
        out.addComponent(DataArray.create(dummy, 1, "dummy"));
        for (int n = 0; n < nObjects; n++) {
            CellArray ca = new CellArray(CellType.TRIANGLE, cellsIndices[n], null, null);
            CellSet cs = new CellSet(objectNames[n] != null ? objectNames[n] : "NoName");
            cs.setCellArray(ca);
            out.addCellSet(cs);
            cs.generateDisplayData(coords);
        }
        return out;
    }

    private static class Node {

        float[] p;

        public Node(float[] p) {
            this.p = p;
        }

        public float[] getCoords() {
            return p;
        }

        @Override
        public int hashCode() {
            return java.util.Arrays.hashCode(p);
        }

        @Override
        public boolean equals(Object o) {
            if (o == null || p == null) {
                return false;
            }

            if (!(o instanceof Node)) {
                return false;
            }

            Node on = (Node) o;
            float[] op = on.getCoords();
            if (p.length != op.length) {
                return false;
            }

            for (int i = 0; i < p.length; i++) {
                if (p[i] != op[i]) {
                    return false;
                }
            }

            return true;
        }

    }
}
