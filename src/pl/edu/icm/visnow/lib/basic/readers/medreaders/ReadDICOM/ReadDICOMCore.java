//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.readers.medreaders.ReadDICOM;

import com.pixelmed.dicom.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import pl.edu.icm.visnow.lib.basic.readers.medreaders.ReadDICOM.DicomReaderCore.DICOMSortingEntry;
import pl.edu.icm.visnow.lib.gui.HistoArea;
import pl.edu.icm.visnow.lib.utils.denoising.*;
import static pl.edu.icm.visnow.lib.utils.denoising.AnisotropicDenoisingParams.*;
import pl.edu.icm.visnow.lib.utils.field.FieldSmoothDown;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.system.main.VisNow;
import static pl.edu.icm.visnow.lib.basic.readers.medreaders.ReadDICOM.ReadDICOMShared.*;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ReadDICOMCore
{

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ReadDICOMCore.class);
    private RegularField outField = null;
    private Parameters params = null;
    private float progress = 0.0f;
    private HistoArea histoArea = null;

    
    public final static String OUT_RFIELD_MAIN = "outField";
    
    public ReadDICOMCore()
    {
    }

    public ReadDICOMCore(Parameters params)
    {
        this.params = params;
    }

    public void setHistoArea(HistoArea histoArea)
    {
        this.histoArea = histoArea;
    }

    /**
     * @param params the params to set
     */
    public void setParameters(Parameters params)
    {
        this.params = params;
    }

    //TODO: make it consistent (create proper interface)
    public void recalculate() throws ReadDICOMException
    {
        update();
    }

    public void update() throws ReadDICOMException
    {
        log.debug("running ReadDICOM core update");
        
        outField = readDicomFromFileList();

        if (params.get(SLICE_DENOISING_LEVEL) > 0) {

            int level = params.get(SLICE_DENOISING_LEVEL);
            int radiusPresmooth = 3 * level;
            int radius = level;
            float slope = level;
            float slope1 = 2 * level;         
            int[][] components = new int[][]{{0, 0}}; // Default value. 
            
            RegularField anisotropyField = FieldSmoothDown.smoothDownToFloat(outField, 1, (float) radiusPresmooth, VisNow.availableProcessors());
            
            AbstractAnisotropicWeightedAverageCompute averageCompute = new AnisotropicWeightedAverageCompute();
            
            AnisotropicDenoisingParams currentParams = new AnisotropicDenoisingParams (
                                AVERAGE,
                                radius,
                                slope,
                                slope1,
                                components
                            );

            RegularField tmpField = averageCompute.compute(outField, anisotropyField, currentParams);
            tmpField.getComponent(0).setPreferredRanges(outField.getComponent(0).getPreferredMinValue(), outField.getComponent(0).getPreferredMaxValue(), outField.getComponent(0).getPreferredPhysMinValue(), outField.getComponent(0).getPreferredPhysMaxValue());
            outField = tmpField;
        }

        if (params.get(INTERPOLATE_DATA) && outField != null) {
            log.debug("running ReadDICOM core data interpolation");
            progress = 0.5f;
            fireStatusChanged(progress);
            Regularizator r = new Regularizator();
            r.addFloatValueModificationListener(new FloatValueModificationListener()
            {

                public void floatValueChanged(FloatValueModificationEvent e)
                {
                    progress = 0.5f + (e.getVal()) / 2.0f;
                    if (progress >= 1.0f) {
                        progress = 0.999f;
                    }
                    fireStatusChanged(progress);
                }
            });

            float voxelSize = 1.0f;
            float[][] affine = outField.getAffine();
            switch (params.get(INTERPOLATE_DATA_VOXEL_SIZE_FROM)) {
                case ReadDICOMShared.VOXELSIZE_FROM_MANUALVALUE:
                    voxelSize = params.get(INTERPOLATE_DATA_VOXEL_SIZE_MANUAL_VALUE);
                    break;
                case ReadDICOMShared.VOXELSIZE_FROM_PIXELSIZE:
                    voxelSize = 0;
                    for (int i = 0; i < 3; i++) {
                        voxelSize += affine[0][i] * affine[0][i];
                    }
                    voxelSize = (float) sqrt(voxelSize);
                    break;
                case ReadDICOMShared.VOXELSIZE_FROM_SLICESDISTANCE:
                    voxelSize = 0;
                    for (int i = 0; i < 3; i++) {
                        voxelSize += affine[2][i] * affine[2][i];
                    }
                    voxelSize = (float) sqrt(voxelSize);
                    break;
            }
            System.out.println("voxel size =" + voxelSize);
            log.debug("voxel size =" + voxelSize);
            outField = r.regularize(outField, pl.edu.icm.visnow.system.main.VisNow.availableProcessors(), voxelSize);
        }
        progress = 1.0f;
        fireStatusChanged(progress);
    }

    /**
     * @return the outField
     */
    public RegularField getOutField()
    {
        return outField;
    }
    
    public Field getOutField(String name)
    {
        if (name.equals(OUT_RFIELD_MAIN))
            return outField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }
    
    public RegularField readDicomFromFileList() throws ReadDICOMException
    {
        RegularField field = null;
        String[] files = params.get(FILE_LIST);
        ArrayList<String> fileList;
        if (files == null || files.length < 1) fileList = null;
        else fileList = new ArrayList<>(Arrays.asList(files));
            
        if (fileList == null || fileList.size() < 1) {
            throw new ReadDICOMException("ERROR: no files to read!");
        }

        progress = 0.0f;
        fireStatusChanged(progress);

        ArrayList<AttributeList> atls = new ArrayList<AttributeList>();
        ArrayList<String> fileListFiltered = new ArrayList<String>();

        log.debug("number of files in list: " + fileList.size());
        log.debug("Filter DICOM file list");
        File file;
        Attribute att;

        for (int i = 0; i < fileList.size(); i++) {
            progress = i * 0.25f / fileList.size();
            if (params.get(INTERPOLATE_DATA)) {
                progress = progress / 2.0f;
            }
            fireStatusChanged(progress);

            String fileName = fileList.get(i);
            file = new File(fileName);
            AttributeList atl = new AttributeList();

            try {
                if(!file.canRead())
                    throw new IOException();
                
                atl.read(file, TagFromName.PixelData);
            } catch (IOException ex) {
                System.out.println("WARNING! Cannot read file: " + fileName);
                continue;
            } catch (DicomException ex) {
                System.out.println("WARNING! File is not a DICOM file: " + fileName);
                continue;
            }

            //----------------check Rows TAG--------------------
            att = atl.get(TagFromName.Rows);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'Rows' DICOM TAG: " + fileName);
                continue;
            }
            int rows = att.getSingleIntegerValueOrDefault(0);

            //----------------check Columns TAG--------------------
            att = atl.get(TagFromName.Columns);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'Columns' DICOM TAG: " + fileName);
                continue;
            }
            int cols = att.getSingleIntegerValueOrDefault(0);

            //----------------check rows/cols size--------------------
            if (rows == 0 || cols == 0) {
                System.out.println("WARNING! File image data of zero size: " + fileName);
                continue;
            }

            //----------------check PhotometricInterpretation TAG--------------------
            att = atl.get(TagFromName.PhotometricInterpretation);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'PhotometricInterpretation' DICOM TAG: " + fileName);
                continue;
            }
            String photometricInterpretation = att.getSingleStringValueOrDefault("");

            if (!(photometricInterpretation.equals("MONOCHROME1") ||
                    photometricInterpretation.equals("MONOCHROME2") ||
                    photometricInterpretation.equals("PALETTE COLOR") || photometricInterpretation.equals("RGB"))) {
                System.out.println("WARNING! Unknown PhotometricInterpretation '" + photometricInterpretation + "' in file: " + fileName);
                continue;
            }

            if (fileList.size() == 1) {
                atls.add(atl);
                fileListFiltered.add(fileName);
                break;
            }

            //----------------check PixelSpacing TAG--------------------
            att = atl.get(TagFromName.PixelSpacing);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'PixelSpacing' DICOM TAG: " + fileName);
                continue;
            }

            //----------------check SeriesNumber TAG--------------------
            att = atl.get(TagFromName.SeriesNumber);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'SeriesNumber' DICOM TAG: " + fileName);
                continue;
            }

            //----------------check ImagePositionPatient TAG--------------------
            att = atl.get(TagFromName.ImagePositionPatient);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'ImagePositionPatient' DICOM TAG: " + fileName);
                continue;
            }

            //----------------check ImageOrientationPatient TAG--------------------
            att = atl.get(TagFromName.ImageOrientationPatient);
            if (att == null) {
                System.out.println("WARNING! File does not contain 'ImageOrientationPatient' DICOM TAG: " + fileName);
                continue;
            }

            atls.add(atl);
            fileListFiltered.add(fileName);

        }

        int numberOfFrames = 1;
        int N = atls.size();
        if (N == 0) {
            throw new ReadDICOMException("ERROR: no files to read after DICOM tagging check!");
        }
        log.debug("number of files after filtering tags: " + N);

        //check coherency
        log.debug("Check coherency of DICOM file list");
        try {
            int cols = 0, rows = 0, tmpcols, tmprows, seriesNo = -1, tmpseriesNo;

            double[] pixelSpacingTmp, pixelSpacing = {1.0, 1.0};

            double[] imageOrientation = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
            double[] imageOrientationTmp;

            String photometricInterpretation = "", tmpphotometricInterpretation;

            att = atls.get(0).get(TagFromName.Columns);
            if (att != null)
                cols = att.getSingleIntegerValueOrDefault(0);
            att = atls.get(0).get(TagFromName.Rows);
            if (att != null)
                rows = att.getSingleIntegerValueOrDefault(0);
            att = atls.get(0).get(TagFromName.SeriesNumber);
            if (att != null)
                seriesNo = att.getSingleIntegerValueOrDefault(-1);
            att = atls.get(0).get(TagFromName.PhotometricInterpretation);
            if (att != null)
                photometricInterpretation = att.getSingleStringValueOrDefault("");

            att = atls.get(0).get(TagFromName.NumberOfFrames);
            if (att != null)
                numberOfFrames = att.getSingleIntegerValueOrDefault(1);

            att = atls.get(0).get(TagFromName.ImageOrientationPatient);
            if (!params.get(IGNORE_ORIENTATION) && att != null)
                imageOrientation = att.getDoubleValues();

            att = atls.get(0).get(TagFromName.PixelSpacing);
            if (att != null)
                pixelSpacing = att.getDoubleValues();

            for (int i = 1; i < atls.size(); i++) {
                att = atls.get(i).get(TagFromName.Columns);
                tmpcols = att.getSingleIntegerValueOrDefault(0);
                if (!(tmpcols == cols && tmpcols > 0)) {
                    System.out.println("WARNING! File incoherent in DICOM TAG 'Columns': " + fileListFiltered.get(i));
                    atls.remove(i);
                    fileListFiltered.remove(i);
                    i--;
                    continue;
                }

                att = atls.get(i).get(TagFromName.Rows);
                tmprows = att.getSingleIntegerValueOrDefault(0);
                if (!(tmprows == rows && tmprows > 0)) {
                    System.out.println("WARNING! File incoherent in DICOM TAG 'Rows': " + fileListFiltered.get(i));
                    atls.remove(i);
                    fileListFiltered.remove(i);
                    i--;
                    continue;
                }

                att = atls.get(i).get(TagFromName.SeriesNumber);
                tmpseriesNo = att.getSingleIntegerValueOrDefault(-1);
                //if (!(tmpseriesNo == seriesNo && seriesNo >= 0)) {
                if (tmpseriesNo != seriesNo) {
                    System.out.println("WARNING! File incoherent in DICOM TAG 'SeriesNumber': " + fileListFiltered.get(i));
                    atls.remove(i);
                    fileListFiltered.remove(i);
                    i--;
                    continue;
                }

                att = atls.get(i).get(TagFromName.PhotometricInterpretation);
                tmpphotometricInterpretation = att.getSingleStringValueOrDefault("");
                if (!(tmpphotometricInterpretation.equals(photometricInterpretation))) {
                    System.out.println("WARNING! File incoherent in DICOM TAG 'PhotometricInterpretation': " + fileListFiltered.get(i));
                    atls.remove(i);
                    fileListFiltered.remove(i);
                    i--;
                    continue;
                }

                double orientationEps = 0.000001;
                att = atls.get(i).get(TagFromName.ImageOrientationPatient);
                if (att != null) {
                    imageOrientationTmp = att.getDoubleValues();
                } else {
                    imageOrientationTmp = imageOrientation;
                }
                if (!params.get(IGNORE_ORIENTATION) && !(imageOrientationTmp != null &&
                        imageOrientationTmp.length == 6 &&
                        abs(imageOrientationTmp[0] - imageOrientation[0]) < orientationEps &&
                        abs(imageOrientationTmp[1] - imageOrientation[1]) < orientationEps &&
                        abs(imageOrientationTmp[2] - imageOrientation[2]) < orientationEps &&
                        abs(imageOrientationTmp[3] - imageOrientation[3]) < orientationEps &&
                        abs(imageOrientationTmp[4] - imageOrientation[4]) < orientationEps &&
                        abs(imageOrientationTmp[5] - imageOrientation[5]) < orientationEps)) {
                    System.out.println("WARNING! File incoherent in DICOM TAG 'ImageOrientationPatient': " + fileListFiltered.get(i));
                    atls.remove(i);
                    fileListFiltered.remove(i);
                    i--;
                    continue;
                }

                att = atls.get(i).get(TagFromName.PixelSpacing);
                pixelSpacingTmp = att.getDoubleValues();
                if (!(pixelSpacingTmp != null && pixelSpacingTmp.length == 2 && pixelSpacingTmp[0] == pixelSpacing[0] && pixelSpacingTmp[1] == pixelSpacing[1])) {
                    System.out.println("WARNING! File incoherent in DICOM TAG 'PixelSpacing': " + fileListFiltered.get(i));
                    atls.remove(i);
                    fileListFiltered.remove(i);
                    i--;
                    continue;
                }

                att = atls.get(i).get(TagFromName.NumberOfFrames);
                if (att != null) {
                    int tmpNumberOfFrames = att.getSingleIntegerValueOrDefault(1);
                    if (tmpNumberOfFrames != numberOfFrames) {
                        System.out.println("WARNING! File incoherent in DICOM TAG 'NumberOfFrames': " + fileListFiltered.get(i));
                        atls.remove(i);
                        fileListFiltered.remove(i);
                        i--;
                        continue;
                    }
                }
            }

            N = atls.size();
            if (N == 0) {
                throw new ReadDICOMException("ERROR: no files to read after coherency check!");
            }
            log.debug("number of files after coherency filtering: " + N);

            //list is DICOM checked and coherent for reading
            log.debug("DICOM file list filtered and coherency-checked");

            String patientName = "patient";
            att = atls.get(0).get(TagFromName.PatientName);
            if (att != null) {
                patientName = att.getSingleStringValueOrDefault("dicom_data");
            }
            patientName = patientName.replaceAll("^", "");
            if (patientName.startsWith("0") ||
                    patientName.startsWith("1") ||
                    patientName.startsWith("2") ||
                    patientName.startsWith("3") ||
                    patientName.startsWith("4") ||
                    patientName.startsWith("5") ||
                    patientName.startsWith("6") ||
                    patientName.startsWith("7") ||
                    patientName.startsWith("8") ||
                    patientName.startsWith("9")) {
                patientName = "_" + patientName;
            }
            params.setParameterActive(false);
            params.set(PATIENT_NAME, patientName);
            params.setParameterActive(false);
            //filelist ready
            if (N == 1 && numberOfFrames == 1) {
                field = readSingleDicomFile(atls.get(0), fileListFiltered.get(0));
            } else if (N == 1 && numberOfFrames > 1) {
                field = readSingleDicomFramesFile(atls.get(0), fileListFiltered.get(0));
            } else {
                field = readMultipleDicomFiles(atls, fileListFiltered);
            }
        } catch (DicomException ex) {
            throw new ReadDICOMException("ERROR: DICOM exception occured: " + ex.getMessage());
        } catch (IOException ex) {
            throw new ReadDICOMException("ERROR: IO exception occured: " + ex.getMessage());
        }

        atls = null;
        System.gc();
        return field;
    }

    public RegularField readSingleDicomFile(AttributeList atl, String filePath) throws DicomException, IOException
    {
        ArrayList<DICOMSortingEntry> entries = new ArrayList<DICOMSortingEntry>();
        entries.add(new DICOMSortingEntry(filePath, atl, new double[]{0}));

        Attribute att;
        int cols, rows;
        att = atl.get(TagFromName.Columns);
        if (att == null) {
            return null;
        }
        cols = att.getSingleIntegerValueOrDefault(0);
        att = atl.get(TagFromName.Rows);
        if (att == null) {
            return null;
        }
        rows = att.getSingleIntegerValueOrDefault(0);

        if (rows == 0 || cols == 0) {
            return null;
        }

        att = atl.get(TagFromName.PhotometricInterpretation);
        if (att == null) {
            return null;
        }
        String photometricInterpretation = att.getSingleStringValueOrNull();
        if (photometricInterpretation == null) {
            return null;
        }

        if (!(photometricInterpretation.equals("MONOCHROME1") ||
                photometricInterpretation.equals("MONOCHROME2") ||
                photometricInterpretation.equals("PALETTE COLOR") || photometricInterpretation.equals("RGB"))) {
            System.err.println("ERROR: DICOM photometric interpretation '" + photometricInterpretation + "' not supported!");
            return null;
        }

        int[] downsize = params.get(DOWNSIZE);
        int[] dims = new int[2];
        dims[0] = (int) ceil((double) cols / (double) downsize[0]);
        dims[1] = (int) ceil((double) rows / (double) downsize[1]);
        RegularField field = new RegularField(dims);
        DataArray[] outDataArray = null;

        DicomReaderCore drc = null;

        if (photometricInterpretation.equals("MONOCHROME1") || photometricInterpretation.equals("MONOCHROME2")) {
            drc = new DicomReaderCoreMonochrome(histoArea);
        } else if (photometricInterpretation.equals("PALETTE COLOR")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCorePaletteColor();
        } else if (photometricInterpretation.equals("RGB")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCoreRGB();
        }
        if (drc == null)
            return null;

        drc.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                ReadDICOMCore.this.progress = e.getVal();
                fireStatusChanged(progress);
            }
        });
        outDataArray = drc.readDicomDataArray(entries, params.get(READ_AS), params.get(LOW), params.get(HIGH), dims, downsize, false, false, 1.0f);

        if (outDataArray != null) {
            for (int j = 0; j < outDataArray.length; j++) {
                if (outDataArray[j] != null) {
                    if (params.get(READ_AS) == ReadDICOMShared.READ_AS_BYTES) {
                        outDataArray[j].setPreferredRanges(0, 255, params.get(LOW), params.get(HIGH));
                    }
                    field.addComponent(outDataArray[j]);
                }
            }
        }

        int N = entries.size();
        double w = cols, h = rows;

        double[] pixelSpacing = {1.0, 1.0};
        att = atl.get(TagFromName.PixelSpacing);
        if (att != null) {
            pixelSpacing = att.getDoubleValues();
        }

        double[] imagePositionFirst = null;
        att = atl.get(TagFromName.ImagePositionPatient);
        if (att != null) {
            imagePositionFirst = att.getDoubleValues();
        } else {
            imagePositionFirst = new double[3];
            imagePositionFirst[0] = 0;
            imagePositionFirst[1] = 0;
            imagePositionFirst[2] = 0;
        }

        double[] imageOrientation = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
        att = atl.get(TagFromName.ImageOrientationPatient);
        if (!params.get(IGNORE_ORIENTATION) && att != null) {
            imageOrientation = att.getDoubleValues();
        }

        double[][] daffine = new double[4][3];
        daffine[3][0] = imagePositionFirst[0];
        daffine[3][1] = imagePositionFirst[1];
        daffine[3][2] = imagePositionFirst[2];

        daffine[0][0] = imageOrientation[0] * (double) pixelSpacing[0];
        daffine[0][1] = imageOrientation[1] * (double) pixelSpacing[0];
        daffine[0][2] = imageOrientation[2] * (double) pixelSpacing[0];
        daffine[1][0] = imageOrientation[3] * (double) pixelSpacing[1];
        daffine[1][1] = imageOrientation[4] * (double) pixelSpacing[1];
        daffine[1][2] = imageOrientation[5] * (double) pixelSpacing[1];

        daffine[2][0] = daffine[0][1] * daffine[1][2] - daffine[0][2] * daffine[1][1];
        daffine[2][1] = daffine[0][2] * daffine[1][0] - daffine[0][0] * daffine[1][2];
        daffine[2][2] = daffine[0][0] * daffine[1][1] - daffine[0][1] * daffine[1][0];
        
        float[][] extents = new float[2][3];
        extents[0][0] = (float)daffine[3][0];
        extents[0][1] = (float)daffine[3][1];
        extents[0][2] = (float)daffine[3][2];
        for (int i = 0; i < 3; i++) {
            extents[1][i] = (float)(extents[0][i] + daffine[0][i] * (w - 1) + daffine[1][i] * (h - 1) + daffine[2][i] * (N - 1));
        }

        switch (params.get(POSITION)) {
            case ReadDICOMShared.POSITION_CENTER:
                //center affine
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] -= (extents[1][i] + extents[0][i]) / 2.0;
                }
                break;
            case ReadDICOMShared.POSITION_ZERO:
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] = 0.0;
                }
                break;
        }

        //set affine to field
        float[][] affine = new float[4][3];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = (float) daffine[i][j];
            }
        }
        
        switch (params.get(LENGTH_UNIT)) {
            case ReadDICOMShared.UNIT_M:
                //normalize affine to m
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 1000.0;
                break;
            case ReadDICOMShared.UNIT_CM:
                //normalize affine to cm
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 10.0;
                break;
            default:
        }
        
        field.setAffine(affine);

        float[][] physExtents = new float[2][3];
        for (int i = 0; i < 3; i++) {
            physExtents[0][i] = (float) (extents[0][i]);
            physExtents[1][i] = (float) (extents[1][i]);
        }
        field.setPreferredExtents(field.getPreferredExtents(), physExtents);

        return field;
    }

    public RegularField readSingleDicomFramesFile(AttributeList atl, String filePath) throws DicomException, IOException
    {
        ArrayList<DICOMSortingEntry> entries = new ArrayList<DICOMSortingEntry>();
        entries.add(new DICOMSortingEntry(filePath, atl, new double[]{0}));

        Attribute att;
        int cols, rows;
        att = atl.get(TagFromName.Columns);
        if (att == null) {
            return null;
        }
        cols = att.getSingleIntegerValueOrDefault(0);
        att = atl.get(TagFromName.Rows);
        if (att == null) {
            return null;
        }
        rows = att.getSingleIntegerValueOrDefault(0);

        if (rows == 0 || cols == 0) {
            return null;
        }

        int numberOfFrames = 1;
        att = atl.get(TagFromName.NumberOfFrames);
        if (att != null)
            numberOfFrames = att.getSingleIntegerValueOrDefault(1);

        if (numberOfFrames == 1)
            return readSingleDicomFile(atl, filePath);
        else {
            if (params.get(FRAMES_AS_TIME)) {
                return readSingleDicomFramesFileAsTime(atl, filePath);
            } else
                return readSingleDicomFramesFileAsDim(atl, filePath);
        }
    }

    public RegularField readSingleDicomFramesFileAsDim(AttributeList atl, String filePath) throws DicomException, IOException
    {
        DICOMSortingEntry entry = new DICOMSortingEntry(filePath, atl, new double[]{0});

        Attribute att;
        int cols, rows;
        att = atl.get(TagFromName.Columns);
        if (att == null) {
            return null;
        }
        cols = att.getSingleIntegerValueOrDefault(0);
        att = atl.get(TagFromName.Rows);
        if (att == null) {
            return null;
        }
        rows = att.getSingleIntegerValueOrDefault(0);

        if (rows == 0 || cols == 0) {
            return null;
        }

        int numberOfFrames = 1;
        att = atl.get(TagFromName.NumberOfFrames);
        if (att != null)
            numberOfFrames = att.getSingleIntegerValueOrDefault(1);

        if (numberOfFrames == 1)
            return readSingleDicomFile(atl, filePath);

        att = atl.get(TagFromName.PhotometricInterpretation);
        if (att == null) {
            return null;
        }
        String photometricInterpretation = att.getSingleStringValueOrNull();
        if (photometricInterpretation == null) {
            return null;
        }

        if (!(photometricInterpretation.equals("MONOCHROME1") ||
                photometricInterpretation.equals("MONOCHROME2") || photometricInterpretation.equals("PALETTE COLOR"))) {
            System.err.println("ERROR: DICOM photometric interpretation '" +
                    photometricInterpretation + "' not supported!");
            return null;
        }

        int[] downsize = params.get(DOWNSIZE);
        int[] dims = new int[3];
        dims[0] = (int) ceil((double) cols / (double) downsize[0]);
        dims[1] = (int) ceil((double) rows / (double) downsize[1]);
        dims[2] = (int) ceil((double) numberOfFrames / (double) downsize[2]);
        RegularField field = new RegularField(dims);
        DataArray[] outDataArray = null;
        DicomReaderCore drc = null;

        if (photometricInterpretation.equals("MONOCHROME1") ||
                photometricInterpretation.equals("MONOCHROME2")) {
            drc = new DicomReaderCoreMonochrome(histoArea);
        } else if (photometricInterpretation.equals("PALETTE COLOR")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCorePaletteColor();
        } else if (photometricInterpretation.equals("RGB")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCoreRGB();
        }
        if (drc == null)
            return null;

        drc.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                ReadDICOMCore.this.progress = e.getVal();
                fireStatusChanged(progress);
            }
        });
        outDataArray = drc.readDicomDataArrayFrames(entry, params.get(READ_AS),
                                                    params.get(LOW), params.get(HIGH),
                                                    dims, downsize, false, 1.0f, true, 0, 0);

        if (outDataArray != null) {
            for (int j = 0; j < outDataArray.length; j++) {
                if (outDataArray[j] != null) {
                    if (params.get(READ_AS) == ReadDICOMShared.READ_AS_BYTES) {
                        outDataArray[j].setPreferredRanges(0, 255, params.get(LOW), params.get(HIGH));
                    }
                    field.addComponent(outDataArray[j]);
                }
            }
        }

        double w = cols, h = rows, d = numberOfFrames;

        Attribute attPixelSpacing, attImagePositionPatient, attImageOrientationPatient, attSpacingBetweenSlices;
        att = null;

        double[] pixelSpacing = {1.0, 1.0};
        double sliceThickness = 1.0;
        double[][] daffine = new double[4][3];
        for (int i = 0; i < 3; i++) {
            daffine[i][i] = 1.0;
        }

        attPixelSpacing = atl.get(TagFromName.PixelSpacing);
        attImagePositionPatient = atl.get(TagFromName.ImagePositionPatient);
        attImageOrientationPatient = atl.get(TagFromName.ImageOrientationPatient);
        attSpacingBetweenSlices = atl.get(TagFromName.SpacingBetweenSlices);
        att = atl.get(TagFromName.SharedFunctionalGroupsSequence);
        AttributeList seqAtl = null;
        if (att != null && ((SequenceAttribute) att).getItem(0) != null) {
            seqAtl = ((SequenceAttribute) att).getItem(0).getAttributeList();
        }

        if (attPixelSpacing != null && attImageOrientationPatient != null && attImagePositionPatient != null) {
            pixelSpacing = attPixelSpacing.getDoubleValues();
            double[] imagePositionFirst = attImagePositionPatient.getDoubleValues();
            double[] imageOrientation = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
            if (!params.get(IGNORE_ORIENTATION) && attImageOrientationPatient != null) {
                imageOrientation = attImageOrientationPatient.getDoubleValues();
            }
            if (pixelSpacing != null || imagePositionFirst != null || imageOrientation != null) {
                daffine[3][0] = imagePositionFirst[0];
                daffine[3][1] = imagePositionFirst[1];
                daffine[3][2] = imagePositionFirst[2];

                daffine[0][0] = imageOrientation[0] * pixelSpacing[0];
                daffine[0][1] = imageOrientation[1] * pixelSpacing[0];
                daffine[0][2] = imageOrientation[2] * pixelSpacing[0];
                daffine[1][0] = imageOrientation[3] * pixelSpacing[1];
                daffine[1][1] = imageOrientation[4] * pixelSpacing[1];
                daffine[1][2] = imageOrientation[5] * pixelSpacing[1];

                daffine[2][0] = daffine[0][1] * daffine[1][2] - daffine[0][2] * daffine[1][1];
                daffine[2][1] = daffine[0][2] * daffine[1][0] - daffine[0][0] * daffine[1][2];
                daffine[2][2] = daffine[0][0] * daffine[1][1] - daffine[0][1] * daffine[1][0];
                if (attSpacingBetweenSlices != null) {
                    double norm = sqrt(daffine[2][0] * daffine[2][0] + daffine[2][1] * daffine[2][1] + daffine[2][2] * daffine[2][2]);
                    double spacing = attSpacingBetweenSlices.getSingleDoubleValueOrDefault(1.0f);
                    daffine[2][0] = spacing * daffine[2][0] / norm;
                    daffine[2][1] = spacing * daffine[2][1] / norm;
                    daffine[2][2] = spacing * daffine[2][2] / norm;
                }

                //TODO downsize
            }
        } else if (attPixelSpacing != null) {
            pixelSpacing = attPixelSpacing.getDoubleValues();
            att = atl.get(TagFromName.SliceThickness);
            if (att != null) {
                sliceThickness = att.getSingleDoubleValueOrDefault(0);
                if (sliceThickness > 0) {
                    daffine[0][0] = pixelSpacing[0];
                    daffine[1][1] = pixelSpacing[1];
                    daffine[2][2] = sliceThickness;

                    daffine[3][0] = -(dims[0] * pixelSpacing[0]) / 2.0;
                    daffine[3][1] = -(dims[1] * pixelSpacing[1]) / 2.0;
                    daffine[3][2] = -(dims[2] * sliceThickness) / 2.0;
                }
            }
        } else if (seqAtl != null) {
            Attribute mps = seqAtl.get(TagFromName.PixelMeasuresSequence);
            AttributeList mpsAtl = ((SequenceAttribute) mps).getItem(0).getAttributeList();
            attPixelSpacing = mpsAtl.get(TagFromName.PixelSpacing);
            att = mpsAtl.get(TagFromName.SliceThickness);
            if (attPixelSpacing != null && att != null) {
                pixelSpacing = attPixelSpacing.getDoubleValues();
                sliceThickness = att.getSingleDoubleValueOrDefault(0);
                if (pixelSpacing != null && sliceThickness > 0) {
                    daffine[0][0] = pixelSpacing[0] * downsize[0];
                    daffine[1][1] = pixelSpacing[1] * downsize[1];
                    daffine[2][2] = sliceThickness * downsize[2];

                    daffine[3][0] = -(w * pixelSpacing[0]) / 2.0;
                    daffine[3][1] = -(h * pixelSpacing[1]) / 2.0;
                    daffine[3][2] = -(d * sliceThickness) / 2.0;
                }
            }
        } else {
            daffine[0][0] = pixelSpacing[0] * downsize[0];
            daffine[1][1] = pixelSpacing[1] * downsize[1];
            daffine[2][2] = sliceThickness * downsize[2];

            daffine[3][0] = -(w * pixelSpacing[0]) / 2.0;
            daffine[3][1] = -(h * pixelSpacing[1]) / 2.0;
            daffine[3][2] = -(d * sliceThickness) / 2.0;
        }

//        switch (params.getLengthUnit())
//        {
//        case ReadDICOMShared.UNIT_M:
//            //normalize affine to m
//            for (int i = 0; i < 4; i++)
//                for (int j = 0; j < 3; j++)
//                    daffine[i][j] /= 1000.0;
//                break;
//        case ReadDICOMShared.UNIT_CM:
//            //normalize affine to cm
//            for (int i = 0; i < 4; i++) 
//                for (int j = 0; j < 3; j++) 
//                    daffine[i][j] /= 10.0;
//                break;
//        case ReadDICOMShared.UNIT_MM:
//        default:
//        }
        
        float[][] extents = new float[2][3];
        extents[0][0] = (float)daffine[3][0];
        extents[0][1] = (float)daffine[3][1];
        extents[0][2] = (float)daffine[3][2];
        for (int i = 0; i < 3; i++) {
            extents[1][i] = (float)(extents[0][i] + daffine[0][i] * (w - 1) + daffine[1][i] * (h - 1) + daffine[2][i] * (d - 1));
        }

        switch (params.get(POSITION)) {
            case ReadDICOMShared.POSITION_CENTER:
                //center affine
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] -= (extents[1][i] + extents[0][i]) / 2.0;
                }
                break;
            case ReadDICOMShared.POSITION_ZERO:
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] = 0.0;
                }
                break;
        }

        //set affine to field
        float[][] affine = new float[4][3];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = (float) daffine[i][j];
            }
        }
        
        switch (params.get(LENGTH_UNIT)) {
            case ReadDICOMShared.UNIT_M:
                //normalize affine to m
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 1000.0;
                break;
            case ReadDICOMShared.UNIT_CM:
                //normalize affine to cm
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 10.0;
                break;
            default:
        }
        
        field.setAffine(affine);

        float[][] physExtents = new float[2][3];
        for (int i = 0; i < 3; i++) {
            physExtents[0][i] = (float) (extents[0][i]);
            physExtents[1][i] = (float) (extents[1][i]);
        }
        field.setPreferredExtents(field.getPreferredExtents(), physExtents);

        return field;
    }

    public RegularField readSingleDicomFramesFileAsTime(AttributeList atl, String filePath) throws DicomException, IOException
    {
        DICOMSortingEntry entry = new DICOMSortingEntry(filePath, atl, new double[]{0});

        Attribute att;
        int cols, rows;
        att = atl.get(TagFromName.Columns);
        if (att == null) {
            return null;
        }
        cols = att.getSingleIntegerValueOrDefault(0);
        att = atl.get(TagFromName.Rows);
        if (att == null) {
            return null;
        }
        rows = att.getSingleIntegerValueOrDefault(0);

        if (rows == 0 || cols == 0) {
            return null;
        }

        int numberOfFrames = 1;
        att = atl.get(TagFromName.NumberOfFrames);
        if (att != null)
            numberOfFrames = att.getSingleIntegerValueOrDefault(1);

        if (numberOfFrames == 1)
            return readSingleDicomFile(atl, filePath);

        att = atl.get(TagFromName.PhotometricInterpretation);
        if (att == null) {
            return null;
        }
        String photometricInterpretation = att.getSingleStringValueOrNull();
        if (photometricInterpretation == null) {
            return null;
        }

        if (!(photometricInterpretation.equals("MONOCHROME1") ||
                photometricInterpretation.equals("MONOCHROME2") || photometricInterpretation.equals("PALETTE COLOR"))) {
            System.err.println("ERROR: DICOM photometric interpretation '" +
                    photometricInterpretation + "' not supported!");
            return null;
        }

        int[] downsize = params.get(DOWNSIZE);
        int[] dims = new int[2];
        dims[0] = (int) ceil((double) cols / (double) downsize[0]);
        dims[1] = (int) ceil((double) rows / (double) downsize[1]);
        RegularField field = new RegularField(dims);
        DataArray[] outDataArray = null;
        DicomReaderCore drc = null;

        int framesRangeLow = 0, framesRangeUp = 0;

        if (params.get(FRAMES_RANGE) != null) {
            framesRangeLow = params.get(FRAMES_RANGE)[0];
            if (framesRangeLow < 0)
                framesRangeLow = 0;
            if (framesRangeLow >= numberOfFrames)
                framesRangeLow = numberOfFrames - 1;
            framesRangeUp = params.get(FRAMES_RANGE)[1];
            if (framesRangeUp < 0)
                framesRangeUp = 0;
            if (framesRangeUp >= numberOfFrames)
                framesRangeUp = numberOfFrames - 1;
            if (framesRangeUp < framesRangeLow)
                framesRangeUp = framesRangeLow;
        }

        if (photometricInterpretation.equals("MONOCHROME1") ||
                photometricInterpretation.equals("MONOCHROME2")) {
            drc = new DicomReaderCoreMonochrome(histoArea);
        } else if (photometricInterpretation.equals("PALETTE COLOR")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCorePaletteColor();
        } else if (photometricInterpretation.equals("RGB")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCoreRGB();
        }
        if (drc == null)
            return null;

        drc.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                ReadDICOMCore.this.progress = e.getVal();
                fireStatusChanged(progress);
            }
        });
        outDataArray = drc.readDicomDataArrayFrames(entry, params.get(READ_AS),
                                                    params.get(LOW), params.get(HIGH),
                                                    dims, downsize, false, 1.0f, false, framesRangeLow, framesRangeUp);

        if (outDataArray != null) {
            for (int j = 0; j < outDataArray.length; j++) {
                if (outDataArray[j] != null) {
                    if (params.get(READ_AS) == ReadDICOMShared.READ_AS_BYTES) {
                        outDataArray[j].setPreferredRanges(0, 255, params.get(LOW), params.get(HIGH));
                    }
                    field.addComponent(outDataArray[j]);
                }
            }
        }

        double w = cols, h = rows;

        Attribute attPixelSpacing, attImagePositionPatient, attImageOrientationPatient;
        att = null;

        double[] pixelSpacing = {1.0, 1.0};
        double[][] daffine = new double[4][3];
        for (int i = 0; i < 3; i++) {
            daffine[i][i] = 1.0;
        }

        attPixelSpacing = atl.get(TagFromName.PixelSpacing);
        attImagePositionPatient = atl.get(TagFromName.ImagePositionPatient);
        attImageOrientationPatient = atl.get(TagFromName.ImageOrientationPatient);
        att = atl.get(TagFromName.SharedFunctionalGroupsSequence);
        AttributeList seqAtl = null;
        if (att != null && ((SequenceAttribute) att).getItem(0) != null) {
            seqAtl = ((SequenceAttribute) att).getItem(0).getAttributeList();
        }

        if (attPixelSpacing != null && attImageOrientationPatient != null && attImagePositionPatient != null) {
            pixelSpacing = attPixelSpacing.getDoubleValues();
            double[] imagePositionFirst = attImagePositionPatient.getDoubleValues();
            double[] imageOrientation = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
            if (!params.get(IGNORE_ORIENTATION)) {
                imageOrientation = attImageOrientationPatient.getDoubleValues();
            }
            if (pixelSpacing != null || imagePositionFirst != null || imageOrientation != null) {
                daffine[3][0] = imagePositionFirst[0];
                daffine[3][1] = imagePositionFirst[1];
                daffine[3][2] = imagePositionFirst[2];

                daffine[0][0] = imageOrientation[0] * pixelSpacing[0];
                daffine[0][1] = imageOrientation[1] * pixelSpacing[0];
                daffine[0][2] = imageOrientation[2] * pixelSpacing[0];
                daffine[1][0] = imageOrientation[3] * pixelSpacing[1];
                daffine[1][1] = imageOrientation[4] * pixelSpacing[1];
                daffine[1][2] = imageOrientation[5] * pixelSpacing[1];

                //TODO downsize
            }
        } else if (attPixelSpacing != null) {
            pixelSpacing = attPixelSpacing.getDoubleValues();
            daffine[0][0] = pixelSpacing[0];
            daffine[1][1] = pixelSpacing[1];

            daffine[3][0] = -(dims[0] * pixelSpacing[0]) / 2.0;
            daffine[3][1] = -(dims[1] * pixelSpacing[1]) / 2.0;
            daffine[3][2] = 0.0f;
        } else if (seqAtl != null) {
            Attribute mps = seqAtl.get(TagFromName.PixelMeasuresSequence);
            AttributeList mpsAtl = ((SequenceAttribute) mps).getItem(0).getAttributeList();
            attPixelSpacing = mpsAtl.get(TagFromName.PixelSpacing);
            if (attPixelSpacing != null) {
                pixelSpacing = attPixelSpacing.getDoubleValues();
                if (pixelSpacing != null) {
                    daffine[0][0] = pixelSpacing[0] * downsize[0];
                    daffine[1][1] = pixelSpacing[1] * downsize[1];

                    daffine[3][0] = -(w * pixelSpacing[0]) / 2.0;
                    daffine[3][1] = -(h * pixelSpacing[1]) / 2.0;
                    daffine[3][2] = 0.0f;
                }
            }
        } else {
            daffine[0][0] = pixelSpacing[0] * downsize[0];
            daffine[1][1] = pixelSpacing[1] * downsize[1];

            daffine[3][0] = -(w * pixelSpacing[0]) / 2.0;
            daffine[3][1] = -(h * pixelSpacing[1]) / 2.0;
            daffine[3][2] = 0.0f;
        }
//
//        switch (params.getLengthUnit())
//        {
//        case ReadDICOMShared.UNIT_M:
//            //normalize affine to m
//            for (int i = 0; i < 4; i++)
//                for (int j = 0; j < 3; j++)
//                    daffine[i][j] /= 1000.0;
//                break;
//        case ReadDICOMShared.UNIT_CM:
//            //normalize affine to cm
//            for (int i = 0; i < 4; i++) 
//                for (int j = 0; j < 3; j++) 
//                    daffine[i][j] /= 10.0;
//                break;
//        default:
//        }

        float[][] extents = new float[2][3];
        extents[0][0] = (float)daffine[3][0];
        extents[0][1] = (float)daffine[3][1];
        extents[0][2] = (float)daffine[3][2];
        for (int i = 0; i < 3; i++) {
            extents[1][i] = (float)(extents[0][i] + daffine[0][i] * (w - 1) + daffine[1][i] * (h - 1));
        }

        switch (params.get(POSITION)) {
            case ReadDICOMShared.POSITION_CENTER:
                //center affine
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] -= (extents[1][i] + extents[0][i]) / 2.0;
                }
                break;
            case ReadDICOMShared.POSITION_ZERO:
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] = 0.0;
                }
                break;
        }

        //set affine to field
        float[][] affine = new float[4][3];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = (float) daffine[i][j];
            }
        }
        
        switch (params.get(LENGTH_UNIT)) {
            case ReadDICOMShared.UNIT_M:
                //normalize affine to m
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 1000.0;
                break;
            case ReadDICOMShared.UNIT_CM:
                //normalize affine to cm
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 10.0;
                break;
            default:
        }
        
        field.setAffine(affine);

        float[][] physExtents = new float[2][3];
        for (int i = 0; i < 3; i++) {
            physExtents[0][i] = (float) (extents[0][i]);
            physExtents[1][i] = (float) (extents[1][i]);
        }
        field.setPreferredExtents(field.getPreferredExtents(), physExtents);

        return field;
    }

    private RegularField readMultipleDicomFiles(ArrayList<AttributeList> atls, ArrayList<String> fileListFiltered) throws IOException, ReadDICOMException, DicomException
    {
        //analyze time frames in list
        log.debug("Analyze DICOM file list for time steps");
        int[][] framesLists = DicomReaderCore.analyzeDicomTimeSteps(atls);

        if (framesLists == null) {
            log.debug("DICOM file list does not contain time steps");
            DICOMSortingEntry[][] sortedEntries = DicomReaderCore.sortDicomFiles(atls, fileListFiltered, true, params.get(IGNORE_ORIENTATION));
            if (sortedEntries == null) {
                throw new ReadDICOMException("ERROR: cannot reconstruct volume due to sorting error!");
            }
            return readDicomAsVolumes(sortedEntries);
        } else {
            log.debug("DICOM file list contains " + framesLists.length + " time steps");

            //generate separate sorted lists
            DICOMSortingEntry[][][] sortedEntriesTimeSteps = DicomReaderCore.sortDicomFilesTimeSteps(atls, fileListFiltered, framesLists, true, params.get(IGNORE_ORIENTATION));
            if (sortedEntriesTimeSteps == null) {
                throw new ReadDICOMException("ERROR: cannot reconstruct volume due to sorting error!");
            }
            return readDicomAsVolumesTimeSteps(sortedEntriesTimeSteps);
        }
    }

    private RegularField readDicomAsVolumes(DICOMSortingEntry[][] sortedEntriesV) throws IOException, DicomException
    {
        if (sortedEntriesV == null || sortedEntriesV[0].length < 2) {
            return null;
        }

        AttributeList atl = sortedEntriesV[0][0].getHeader();
        AttributeList atlLast = sortedEntriesV[0][sortedEntriesV[0].length - 1].getHeader();

        Attribute att;
        int cols, rows, slices, nVolumes;
        att = atl.get(TagFromName.Columns);
        cols = att.getSingleIntegerValueOrDefault(0);
        att = atl.get(TagFromName.Rows);
        rows = att.getSingleIntegerValueOrDefault(0);
        slices = sortedEntriesV[0].length;
        nVolumes = sortedEntriesV.length;

        att = atl.get(TagFromName.PhotometricInterpretation);
        String photometricInterpretation = att.getSingleStringValueOrNull();
        if (photometricInterpretation == null) {
            return null;
        }

        if (!(photometricInterpretation.equals("MONOCHROME1") || photometricInterpretation.equals("MONOCHROME2") || photometricInterpretation.equals("PALETTE COLOR"))) {
            System.err.println("ERROR: DICOM photometric interpretation '" + photometricInterpretation + "' not supported!");
            return null;
        }

        if (rows == 0 || cols == 0 || slices == 0) {
            return null;
        }

        int[] dims;
        int[] downsize = params.get(DOWNSIZE);
        if (slices == 1) {
            dims = new int[2];
            dims[0] = (int) ceil((double) cols / (double) downsize[0]);
            dims[1] = (int) ceil((double) rows / (double) downsize[1]);
        } else {
            dims = new int[3];
            dims[0] = (int) ceil((double) cols / (double) downsize[0]);
            dims[1] = (int) ceil((double) rows / (double) downsize[1]);
            dims[2] = (int) ceil((double) slices / (double) downsize[2]);
        }

        RegularField field = new RegularField(dims);
        DicomReaderCore drc = null;

        float progressModifier = 1.0f;
        if (params.get(INTERPOLATE_DATA)) {
            progressModifier = 0.5f;
        }
        progressModifier /= (float)nVolumes;

        if (photometricInterpretation.equals("MONOCHROME1") || photometricInterpretation.equals("MONOCHROME2")) {
            drc = new DicomReaderCoreMonochrome(histoArea);
        } else if (photometricInterpretation.equals("PALETTE COLOR")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCorePaletteColor();
        } else if (photometricInterpretation.equals("RGB")) {
            if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                params.setParameterActive(false);
                params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                params.setParameterActive(true);
            }
            drc = new DicomReaderCoreRGB();
        }
        drc.addFloatValueModificationListener(new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                ReadDICOMCore.this.progress = e.getVal();
                fireStatusChanged(progress);
            }
        });
        for (int nv = 0; nv < nVolumes; nv++) {
            ArrayList<DICOMSortingEntry> tmpEntries = new ArrayList<>();
            for (int i = 0; i < slices; i++) {
                tmpEntries.add(sortedEntriesV[nv][i]);
            }            
            DataArray[] tmpDataArrays = drc.readDicomDataArray(tmpEntries, params.get(READ_AS),
                                                   params.get(LOW), params.get(HIGH),
                                                   dims, downsize, params.get(INPAINT_MISSING_SLICES),
                                                   true, progressModifier);
            if (tmpDataArrays == null) {
                return null;
            }

            for (int i = 0; i < tmpDataArrays.length; i++) {
                if (params.get(READ_AS) == ReadDICOMShared.READ_AS_BYTES) {
                    tmpDataArrays[i].setPreferredRanges(0, 255, params.get(LOW), params.get(HIGH));
                }
                if(nVolumes > 1)
                    tmpDataArrays[i].setName(tmpDataArrays[i].getName()+"_"+nv);
                field.addComponent(tmpDataArrays[i]);
            }
        }

        double w = dims[0], h = dims[1], d = dims[2];

        double[] pixelSpacing = {1.0, 1.0};
        att = atl.get(TagFromName.PixelSpacing);
        if (att != null) {
            pixelSpacing = att.getDoubleValues();
        }

        pixelSpacing[0] = pixelSpacing[0] * downsize[0];
        pixelSpacing[1] = pixelSpacing[1] * downsize[1];

        double[] imagePositionFirst = null;
        att = atl.get(TagFromName.ImagePositionPatient);
        if (att != null) {
            imagePositionFirst = att.getDoubleValues();
        } else {
            imagePositionFirst = new double[3];
            imagePositionFirst[0] = 0;
            imagePositionFirst[1] = 0;
            imagePositionFirst[2] = 0;
        }

        double[] imagePositionLast = null;
        att = atlLast.get(TagFromName.ImagePositionPatient);
        if (att != null) {
            imagePositionLast = att.getDoubleValues();
        } else {
            imagePositionLast = new double[3];
            imagePositionLast[0] = 0;
            imagePositionLast[1] = 0;
            imagePositionLast[2] = slices - 1;
        }

        double[] imageOrientation = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
        att = atl.get(TagFromName.ImageOrientationPatient);
        if (!params.get(IGNORE_ORIENTATION) && att != null) {
            imageOrientation = att.getDoubleValues();
        }

        double[][] daffine = new double[4][3];
        daffine[3][0] = imagePositionFirst[0];
        daffine[3][1] = imagePositionFirst[1];
        daffine[3][2] = imagePositionFirst[2];

        daffine[0][0] = imageOrientation[0] * (double) pixelSpacing[0];
        daffine[0][1] = imageOrientation[1] * (double) pixelSpacing[0];
        daffine[0][2] = imageOrientation[2] * (double) pixelSpacing[0];
        daffine[1][0] = imageOrientation[3] * (double) pixelSpacing[1];
        daffine[1][1] = imageOrientation[4] * (double) pixelSpacing[1];
        daffine[1][2] = imageOrientation[5] * (double) pixelSpacing[1];

        daffine[2][0] = (imagePositionLast[0] - imagePositionFirst[0]) / (double) (dims[2] - 1);
        daffine[2][1] = (imagePositionLast[1] - imagePositionFirst[1]) / (double) (dims[2] - 1);
        daffine[2][2] = (imagePositionLast[2] - imagePositionFirst[2]) / (double) (dims[2] - 1);

        float[][] extents = new float[2][3];
        extents[0][0] = (float)daffine[3][0];
        extents[0][1] = (float)daffine[3][1];
        extents[0][2] = (float)daffine[3][2];
        for (int i = 0; i < 3; i++) {
            extents[1][i] = (float)(extents[0][i] + daffine[0][i] * (w - 1) + daffine[1][i] * (h - 1) + daffine[2][i] * (d - 1));
            if(extents[0][i] > extents[1][i]) {
                float tmp = extents[1][i];
                extents[1][i] = extents[0][i];
                extents[0][i] = tmp;
            }
        }                

        switch (params.get(POSITION)) {
            case ReadDICOMShared.POSITION_CENTER:
                //center affine
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] -= (extents[1][i] + extents[0][i]) / 2.0;
                }
                break;
            case ReadDICOMShared.POSITION_ZERO:
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] = 0.0;
                }
                break;
        }

        //set affine to field
        float[][] affine = new float[4][3];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = (float) daffine[i][j];
            }
        }
        
        switch (params.get(LENGTH_UNIT)) {
            case ReadDICOMShared.UNIT_M:
                //normalize affine to m
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 1000.0;
                break;
            case ReadDICOMShared.UNIT_CM:
                //normalize affine to cm
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 10.0;
                break;
            default:
        }
        
        field.setAffine(affine);

        float[][] physExtents = new float[2][3];
        for (int i = 0; i < 3; i++) {
            physExtents[0][i] = (float) (extents[0][i]);
            physExtents[1][i] = (float) (extents[1][i]);
        }
        field.setPreferredExtents(field.getPreferredExtents(), physExtents);

        return field;
    }

    private RegularField readDicomAsVolumesTimeSteps(DICOMSortingEntry[][][] entriesFramesV) throws IOException, DicomException
    {
        if (entriesFramesV == null || entriesFramesV.length < 1) {
            return null;
        }

        DICOMSortingEntry[][] entriesV = entriesFramesV[0];

        DicomInputStream dis = new DicomInputStream(new File(entriesV[0][0].getFilePath()));
        AttributeList atl = new AttributeList();
        atl.read(dis, TagFromName.PixelData);
        dis.close();

        dis = new DicomInputStream(new File(entriesV[0][entriesV[0].length - 1].getFilePath()));
        AttributeList atlLast = new AttributeList();
        atlLast.read(dis, TagFromName.PixelData);
        dis.close();

        Attribute att;
        int cols, rows, slices, nVolumes;
        att = atl.get(TagFromName.Columns);
        cols = att.getSingleIntegerValueOrDefault(0);
        att = atl.get(TagFromName.Rows);
        rows = att.getSingleIntegerValueOrDefault(0);
        slices = entriesV[0].length;
        nVolumes = entriesV.length;

        att = atl.get(TagFromName.PhotometricInterpretation);
        String photometricInterpretation = att.getSingleStringValueOrNull();
        if (photometricInterpretation == null) {
            return null;
        }

        if (!(photometricInterpretation.equals("MONOCHROME1") || photometricInterpretation.equals("MONOCHROME2") || photometricInterpretation.equals("PALETTE COLOR"))) {
            System.err.println("ERROR: DICOM photometric interpretation '" + photometricInterpretation + "' not supported!");
            return null;
        }

        if (rows == 0 || cols == 0 || slices == 0) {
            return null;
        }

        int[] dims;
        int[] downsize = params.get(DOWNSIZE);
        if (slices == 1) {
            dims = new int[2];
            dims[0] = (int) ceil((double) cols / (double) downsize[0]);
            dims[1] = (int) ceil((double) rows / (double) downsize[1]);
        } else {
            dims = new int[3];
            dims[0] = (int) ceil((double) cols / (double) downsize[0]);
            dims[1] = (int) ceil((double) rows / (double) downsize[1]);
            dims[2] = (int) ceil((double) slices / (double) downsize[2]);
        }
        RegularField field = new RegularField(dims);
        DicomReaderCore drc = null;

        float progressModifier = 1.0f;
        if (params.get(INTERPOLATE_DATA)) {
            progressModifier = 0.5f;
        }

        for (int i = 0; i < entriesFramesV.length; i++) {
            if (photometricInterpretation.equals("MONOCHROME1") || photometricInterpretation.equals("MONOCHROME2")) {
                drc = new DicomReaderCoreMonochrome(histoArea);
            } else if (photometricInterpretation.equals("PALETTE COLOR")) {
                if (params.get(READ_AS) != ReadDICOMShared.READ_AS_AUTO) {
                    System.out.println("WARNING! Color DICOM data detected. Switching read type to AUTO!");
                    params.setParameterActive(false);
                    params.set(READ_AS, ReadDICOMShared.READ_AS_AUTO);
                    params.setParameterActive(true);
                }
                drc = new DicomReaderCorePaletteColor();
            }

            if (drc == null)
                return null;

            drc.addFloatValueModificationListener(new FloatValueModificationListener()
            {
                @Override
                public void floatValueChanged(FloatValueModificationEvent e)
                {
                    ReadDICOMCore.this.progress = e.getVal();
                    fireStatusChanged(progress);
                }
            });
            
            for (int nv = 0; nv < nVolumes; nv++) {
                ArrayList<DICOMSortingEntry> tmpEntries = new ArrayList<>();
                for (int j = 0; j < slices; j++) {
                    tmpEntries.add(entriesFramesV[i][nv][j]);
                }                        
                DataArray[] tmpDataArray = drc.readDicomDataArray(tmpEntries, params.get(READ_AS), params.get(LOW), params.get(HIGH), dims, downsize, params.get(INPAINT_MISSING_SLICES), true, progressModifier);

                if (tmpDataArray != null) {
                    for (int j = 0; j < tmpDataArray.length; j++) {
                        if (tmpDataArray[j] != null) {
                            tmpDataArray[j].setName("frame" + i + "_" + tmpDataArray[j].getName());
                            if (params.get(READ_AS) == ReadDICOMShared.READ_AS_BYTES) {
                                   tmpDataArray[i].setPreferredRanges(0, 255, params.get(LOW), params.get(HIGH));
                            }
                            if(nVolumes > 1)
                                tmpDataArray[j].setName(tmpDataArray[j].getName()+"_"+nv);                                
                            field.addComponent(tmpDataArray[j]);
                        }
                    }
                }
            }
        }

        int N = slices;
        double w = cols, h = rows;

        double[] pixelSpacing = {1.0, 1.0};
        att = atl.get(TagFromName.PixelSpacing);
        if (att != null) {
            pixelSpacing = att.getDoubleValues();
        }

        double[] imagePositionFirst = null;
        att = atl.get(TagFromName.ImagePositionPatient);
        if (att != null) {
            imagePositionFirst = att.getDoubleValues();
        } else {
            imagePositionFirst = new double[3];
            imagePositionFirst[0] = 0;
            imagePositionFirst[1] = 0;
            imagePositionFirst[2] = 0;
        }

        double[] imagePositionLast = null;
        att = atlLast.get(TagFromName.ImagePositionPatient);
        if (att != null) {
            imagePositionLast = att.getDoubleValues();
        } else {
            imagePositionLast = new double[3];
            imagePositionLast[0] = 0;
            imagePositionLast[1] = 0;
            imagePositionLast[2] = N - 1;
        }

        double[] imageOrientation = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0};
        att = atl.get(TagFromName.ImageOrientationPatient);
        if (!params.get(IGNORE_ORIENTATION) && att != null) {
            imageOrientation = att.getDoubleValues();
        }

        double[][] daffine = new double[4][3];
        daffine[3][0] = imagePositionFirst[0];
        daffine[3][1] = imagePositionFirst[1];
        daffine[3][2] = imagePositionFirst[2];

        daffine[0][0] = imageOrientation[0] * (double) pixelSpacing[0];
        daffine[0][1] = imageOrientation[1] * (double) pixelSpacing[0];
        daffine[0][2] = imageOrientation[2] * (double) pixelSpacing[0];
        daffine[1][0] = imageOrientation[3] * (double) pixelSpacing[1];
        daffine[1][1] = imageOrientation[4] * (double) pixelSpacing[1];
        daffine[1][2] = imageOrientation[5] * (double) pixelSpacing[1];

        daffine[2][0] = (imagePositionLast[0] - imagePositionFirst[0]) / (double) (N - 1);
        daffine[2][1] = (imagePositionLast[1] - imagePositionFirst[1]) / (double) (N - 1);
        daffine[2][2] = (imagePositionLast[2] - imagePositionFirst[2]) / (double) (N - 1);
//
//        switch (params.getLengthUnit()) {
//            case ReadDICOMShared.UNIT_M:
//                //normalize affine to m
//                for (int i = 0; i < 4; i++)
//                    for (int j = 0; j < 3; j++)
//                        daffine[i][j] /= 1000.0;
//                break;
//            case ReadDICOMShared.UNIT_CM:
//                //normalize affine to cm
//                for (int i = 0; i < 4; i++)
//                    for (int j = 0; j < 3; j++)
//                        daffine[i][j] /= 10.0;
//                break;
//            default:
//        }

        float[][] extents = new float[2][3];
        extents[0][0] = (float)daffine[3][0];
        extents[0][1] = (float)daffine[3][1];
        extents[0][2] = (float)daffine[3][2];
        for (int i = 0; i < 3; i++) {
            extents[1][i] = (float)(extents[0][i] + daffine[0][i] * (w - 1) + daffine[1][i] * (h - 1) + daffine[2][i] * (N - 1));
        }

        switch (params.get(POSITION)) {
            case ReadDICOMShared.POSITION_CENTER:
                //center affine
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] -= (extents[1][i] + extents[0][i]) / 2.0;
                }
                break;
            case ReadDICOMShared.POSITION_ZERO:
                for (int i = 0; i < 3; i++) {
                    daffine[3][i] = 0.0;
                }
                break;
        }

        //set affine to field
        float[][] affine = new float[4][3];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                affine[i][j] = (float) daffine[i][j];
            }
        }
        
        switch (params.get(LENGTH_UNIT)) {
            case ReadDICOMShared.UNIT_M:
                //normalize affine to m
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 1000.0;
                break;
            case ReadDICOMShared.UNIT_CM:
                //normalize affine to cm
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 3; j++)
                        affine[i][j] /= 10.0;
                break;
            default:
        }
        
        field.setAffine(affine);

        float[][] physExtents = new float[2][3];
        for (int i = 0; i < 3; i++) {
            physExtents[0][i] = (float) (extents[0][i]);
            physExtents[1][i] = (float) (extents[1][i]);
        }
        field.setPreferredExtents(field.getPreferredExtents(), physExtents);

        return field;
    }

    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null) {
            this.statusListener = listener;
        } else {
            System.out.println("" + this + ": only one status listener can be added");
        }
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null) {
            statusListener.floatValueChanged(e);
        }
    }
    
    public static String[] getInFieldNames()
    {
        return new String[]{};
    }

    public static String[] getOutFieldNames()
    {
        return new String[]{OUT_RFIELD_MAIN};
    }
    
}
