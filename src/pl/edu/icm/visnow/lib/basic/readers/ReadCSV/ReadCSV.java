/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.readers.ReadCSV;

import java.io.File;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import static pl.edu.icm.visnow.lib.basic.readers.ReadCSV.ReadCSVShared.*;
import pl.edu.icm.visnow.lib.grid.gridftp.GridFTPException;
import pl.edu.icm.visnow.lib.grid.gridftp.GridFTPLocalUtils;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 19 July 2013
 */
public class ReadCSV extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(ReadCSV.class);
    public static OutputEgg[] outputEggs = null;
    private ReadCSVGUI computeUI = null;

    public ReadCSV()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ReadCSVGUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(FIELD_SEPARATOR, ";"),
            new Parameter<>(HAS_HEADER_LINE, true)
        };
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, isFromVNA(), false);

        if (p.get(FILENAME).isEmpty()) outRegularField = null;
        else {
            ProgressAgent progressAgent = getProgressAgent(140); //20 for download from gsiftp, 100 for read, 20 for geometry
            File f;
            try {
                f = GridFTPLocalUtils.getFileOrLocalGSIFTPCopy(p.get(ReadCSVShared.FILENAME), progressAgent.getSubAgent(20));
                outRegularField = CsvFieldReader.readCsvField1D(f, p.get(FIELD_SEPARATOR), p.get(HAS_HEADER_LINE));
            } catch (GridFTPException ex) {
                LOGGER.error("Error while reading file " + p.get(ReadCSVShared.FILENAME), ex);
                VisNow.get().userMessageSend(this, "Error reading file: " + p.get(ReadCSVShared.FILENAME), "See log for details", Level.ERROR);
            }
        }

        outField = outRegularField;
        prepareOutputGeometry();
        show();
        if (outRegularField == null) setOutputValue("outField", null);
        else setOutputValue("outField", new VNRegularField(outRegularField));
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag()) {
            SwingUtilities.invokeLater(new Runnable()
            {

                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
        }
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
