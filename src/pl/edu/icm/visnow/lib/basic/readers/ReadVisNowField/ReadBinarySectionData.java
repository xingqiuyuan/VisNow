//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField;

import java.io.IOException;
import javax.imageio.stream.ImageInputStream;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import static pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.SectionModel.typeLengths;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ReadBinarySectionData
{

    public static int readSectionData(SectionModel model, ImageInputStream inStream, String filePath)
    {
        int nComps = model.nItems;
        long nNodes = model.nData;
        int[] offsets = model.offsets;
        DataArrayType[] types = model.types;
        int[] vlens = model.vlens;
        long[] ind = model.ind;
        LogicLargeArray[] boolArrs = model.boolArrs;
        UnsignedByteLargeArray[] byteArrs = model.byteArrs;
        ShortLargeArray[] shortArrs = model.shortArrs;
        IntLargeArray[] intArrs = model.intArrs;
        FloatLargeArray[] floatArrs = model.floatArrs;
        DoubleLargeArray[] dblArrs = model.dblArrs;
        FileSectionSchema schema = model.sectionSchema;
        try {
            if (schema.getComponents().isEmpty())
                return 0;
            if (schema.getNComponents() == 1) {
                switch (types[0]) {
                    case FIELD_DATA_LOGIC:
                    {
                        long bufferlen = (int)Math.min(boolArrs[0].length(), 1 << 27);
                        byte[] buffer = new byte[(int)bufferlen];
                        int iters = boolArrs[0].length() <= bufferlen ? 1 : (int)Math.ceil((double)boolArrs[0].length() / (double)bufferlen); 
                        long length = bufferlen;
                        for (int i = 0; i < iters; i++) {
                            if(i == iters - 1) {
                                length = boolArrs[0].length() - i*bufferlen;
                            }
                            inStream.readFully(buffer, 0, (int)length);
                            int k = 0;
                            for (long j = i*bufferlen; j < i*bufferlen + length; j++) {
                                boolArrs[0].setByte(j, buffer[k++]);
                            }
                        }
                    }
                    break;
                    case FIELD_DATA_BYTE:
                    {
                        long bufferlen = (int)Math.min(byteArrs[0].length(), 1 << 27);
                        byte[] buffer = new byte[(int)bufferlen];
                        int iters = byteArrs[0].length() <= bufferlen ? 1 : (int)Math.ceil((double)byteArrs[0].length() / (double)bufferlen); 
                        long length = bufferlen;
                        for (int i = 0; i < iters; i++) {
                            if(i == iters - 1) {
                                length = byteArrs[0].length() - i*bufferlen;
                            }
                            inStream.readFully(buffer, 0, (int)length);
                            LargeArrayUtils.arraycopy(buffer, 0, byteArrs[0], i*bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_SHORT:
                    {
                        long bufferlen = (int)Math.min(shortArrs[0].length(), 1 << 27);
                        short[] buffer = new short[(int)bufferlen];
                        int iters = shortArrs[0].length() <= bufferlen ? 1 : (int)Math.ceil((double)shortArrs[0].length() / (double)bufferlen); 
                        long length = bufferlen;
                        for (int i = 0; i < iters; i++) {
                            if(i == iters - 1) {
                                length = shortArrs[0].length() - i*bufferlen;
                            }
                            inStream.readFully(buffer, 0, (int)length);
                            LargeArrayUtils.arraycopy(buffer, 0, shortArrs[0], i*bufferlen, length);
                        }
                    }    
                    break;
                    case FIELD_DATA_INT:
                    {
                        long bufferlen = (int)Math.min(intArrs[0].length(), 1 << 27);
                        int[] buffer = new int[(int)bufferlen];
                        int iters = intArrs[0].length() <= bufferlen ? 1 : (int)Math.ceil((double)intArrs[0].length() / (double)bufferlen); 
                        long length = bufferlen;
                        for (int i = 0; i < iters; i++) {
                            if(i == iters - 1) {
                                length = intArrs[0].length() - i*bufferlen;
                            }
                            inStream.readFully(buffer, 0, (int)length);
                            LargeArrayUtils.arraycopy(buffer, 0, intArrs[0], i*bufferlen, length);
                        }
                    }    
                    break;
                    case FIELD_DATA_FLOAT:
                    {
                        long bufferlen = (int)Math.min(floatArrs[0].length(), 1 << 27);
                        float[] buffer = new float[(int)bufferlen];
                        int iters = floatArrs[0].length() <= bufferlen ? 1 : (int)Math.ceil((double)floatArrs[0].length() / (double)bufferlen); 
                        long length = bufferlen;
                        for (int i = 0; i < iters; i++) {
                            if(i == iters - 1) {
                                length = floatArrs[0].length() - i*bufferlen;
                            }
                            inStream.readFully(buffer, 0, (int)length);
                            LargeArrayUtils.arraycopy(buffer, 0, floatArrs[0], i*bufferlen, length);
                        }
                    }
                    break;
                    case FIELD_DATA_DOUBLE:
                    {
                        long bufferlen = (int)Math.min(dblArrs[0].length(), 1 << 27);
                        double[] buffer = new double[(int)bufferlen];
                        int iters = dblArrs[0].length() <= bufferlen ? 1 : (int)Math.ceil((double)dblArrs[0].length() / (double)bufferlen); 
                        long length = bufferlen;
                        for (int i = 0; i < iters; i++) {
                            if(i == iters - 1) {
                                length = dblArrs[0].length() - i*bufferlen;
                            }
                            inStream.readFully(buffer, 0, (int)length);
                            LargeArrayUtils.arraycopy(buffer, 0, dblArrs[0], i*bufferlen, length);
                        }
                    }
                    break;
                }
                return 0;
            } else
                for (int k = 0; k < nNodes; k++) {
                    int cPos = 0;
                    for (int l = 0; l < nComps; l++) {
                        int ll = offsets[l] - cPos;
                        cPos = offsets[l] + typeLengths[types[l].getValue()];
                        inStream.skipBytes(ll);
                        switch (types[l]) {
                            case FIELD_DATA_LOGIC:
                                boolArrs[l].setByte(ind[l], inStream.readByte());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_BYTE:
                                byteArrs[l].setByte(ind[l], inStream.readByte());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_SHORT:
                                shortArrs[l].setShort(ind[l], inStream.readShort());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_INT:
                                intArrs[l].setInt(ind[l], inStream.readInt());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_FLOAT:
                                floatArrs[l].setFloat(ind[l], inStream.readFloat());
                                ind[l] += vlens[l];
                                break;
                            case FIELD_DATA_DOUBLE:
                                dblArrs[l].setDouble(ind[l], inStream.readDouble());
                                ind[l] += vlens[l];
                                break;
                        }
                    }
                }
        } catch (IOException e) {
            System.err.println("ERROR: inStream data file");
            e.printStackTrace();
            return 1;
        }
        return 0;
    }
}
