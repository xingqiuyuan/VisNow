/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.readers.material_science.ReadVASP;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University
 * Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class AtomTemplate
{

    /**
     * atomic number (0 for default unknown atom)
     */
    private int number;

    /**
     * atomic symbol (important - use proper upper/lowercase letter - Ca and not CA)
     */
    private String symbol;

    /**
     * full name
     */
    private String name;

    /**
     * atomic mass used in centering routines
     */
    private float mass;

    /**
     * standard radius for ball presentation
     */
    private float vdWRadius;

    /**
     *
     */
    private float ionRadius;

    /**
     * Creates a new instance of AtomTemplate
     */
    public AtomTemplate(int number, float mass, String symbol, String name,
                        float vdWRadius, float ionRadius)
    {
        this.number = number;
        this.mass = mass;
        this.symbol = symbol;
        this.name = name;
        this.mass = 1;
        this.vdWRadius = vdWRadius;
        this.ionRadius = ionRadius;
    }

    public AtomTemplate(int number, String symbol, String name,
                        float vdWRadius, float ionRadius)
    {
        this(number, 1, symbol, name, vdWRadius, ionRadius);
    }

    public AtomTemplate()
    {
        this(0, 1, "du", "dummy", .1f, .1f);
    }

    public String getSymbol()
    {
        return symbol;
    }

    /**
     * Setter for property symbol.
     * <p>
     * @param symbol New value of property symbol.
     *
     */
    public void setSymbol(java.lang.String symbol)
    {
        this.symbol = symbol;
    }

    /**
     * Getter for property number.
     * <p>
     * @return Value of property number.
     *
     */
    public int getNumber()
    {
        return number;
    }

    /**
     * Setter for property number.
     * <p>
     * @param number New value of property number.
     *
     */
    public void setNumber(int number)
    {
        this.number = number;
    }

    /**
     * Getter for property mass.
     * <p>
     * @return Value of property mass.
     *
     */
    public float getMass()
    {
        return mass;
    }

    /**
     * Setter for property mass.
     * <p>
     * @param mass New value of property mass.
     *
     */
    public void setMass(float mass)
    {
        this.mass = mass;
    }

    public float getVdWRadius()
    {
        return (float) vdWRadius;
    }

    public void setVdWRadius(float r)
    {
        vdWRadius = r;
    }

    public float getIonRadius()
    {
        return (float) ionRadius;
    }

    public void setIonRadius(float r)
    {
        ionRadius = r;
    }

}
