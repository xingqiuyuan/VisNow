//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.readers.material_science.ReadVASP;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.ObjectDataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import static pl.edu.icm.visnow.lib.basic.readers.material_science.ReadVASP.ReadVASPShared.*;
import pl.edu.icm.visnow.lib.grid.gridftp.GridFTPException;
import pl.edu.icm.visnow.lib.grid.gridftp.GridFTPLocalUtils;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.io.InputSource;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University, Interdisciplinary Centre for
 * Mathematical and Computational Modelling
 */
public class ReadVASP extends OutFieldVisualizationModule
{
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadVASP.class);
    protected GUI computeUI = null;
    protected boolean fromGUI = false;
    protected Scanner scanner = null;
    protected Object input = null;
    protected String filePath = "";
    protected URLConnection urlConnection = null;
    protected URL url = null;
    protected VisualAtomTemplate[] templates = AtomGeometryTemplates.stdAtomTemplates;
    protected IrregularField atomicStructure = null;

    /**
     * Creates a new instance of CreateGrid
     */
    public ReadVASP()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(FILENAME, ""),
            new Parameter<>(TYPE, InputSource.FILE),
            new Parameter<>(MULT, new int[]{1, 1, 1})
        };
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public static OutputEgg[] outputEggs = null;

    private int readVASP(String filePath, Parameters parametrsClone)
    {
        String[] atomNames = null;
        try {
            if (parametrsClone.get(TYPE) == InputSource.URL) {
                url = new URL(filePath);
                urlConnection = url.openConnection();
                scanner = new Scanner(new InputStreamReader(urlConnection.getInputStream()));
            } else
                scanner = new Scanner(new FileReader(filePath));
            String name, line;
            String[] tokens;
            int[] mult = parametrsClone.get(MULT);
            int gMult = mult[0] * mult[1] * mult[2];
            name = scanner.nextLine().trim();
            float scale = scanner.nextFloat();
            float[][] affine = new float[4][3];
            for (int i = 0; i < 3; i++) {
                affine[3][i] = 0;
                for (int j = 0; j < 3; j++)
                    affine[i][j] = scanner.nextFloat() * scale;
            }
            line = scanner.nextLine().trim();
            while (line.isEmpty())
                line = scanner.nextLine().trim();
            tokens = line.split(" +");
            try {
                int k = Integer.parseInt(tokens[0]);
            } catch (Exception e) {
                atomNames = new String[tokens.length];
                for (int i = 0; i < tokens.length; i++)
                    atomNames[i] = tokens[i].trim();
                line = scanner.nextLine().trim();
                tokens = line.split(" +");
            }
            int nAtoms = 0;
            int[] nAtomsOfType = new int[tokens.length];
            for (int i = 0; i < nAtomsOfType.length; i++) {
                nAtomsOfType[i] = Integer.parseInt(tokens[i]);
                nAtoms += nAtomsOfType[i];
            }
            line = scanner.nextLine().trim();
            while (line.isEmpty())
                line = scanner.nextLine().trim();
            float[] coordsInCell = new float[3 * nAtoms];
            for (int i = 0; i < coordsInCell.length; i++)
                coordsInCell[i] = scanner.nextFloat();
            if (!line.equalsIgnoreCase("cartesian")) {
                float[] c = new float[3];
                for (int i = 0; i < nAtoms; i++) {
                    System.arraycopy(coordsInCell, 3 * i, c, 0, 3);
                    for (int j = 0; j < 3; j++) {
                        coordsInCell[3 * i + j] = 0;
                        for (int k = 0; k < 3; k++)
                            coordsInCell[3 * i + j] += c[k] * affine[k][j];
                    }
                }
            }

            atomicStructure = new IrregularField(gMult * nAtoms);

            int[] typesInCell = new int[nAtoms];
            if (atomNames == null || atomNames.length != nAtomsOfType.length)
                for (int i = 0, k = 0; i < nAtomsOfType.length; i++)
                    for (int j = 0; j < nAtomsOfType[i]; j++, k++)
                        typesInCell[k] = i + 1;
            else
                for (int i = 0, k = 0; i < nAtomsOfType.length; i++)
                    for (int j = 0; j < nAtomsOfType[i]; j++, k++)
                        typesInCell[k] = AtomGeometryTemplates.getTemplateIndex(atomNames[i]);
            FloatLargeArray coords = new FloatLargeArray(3 * (long)gMult * nAtoms, false);
            int[] types = new int[gMult * nAtoms];
            VisualAtomTemplate[] atoms = new VisualAtomTemplate[gMult * nAtoms];
            for (int i = 0, n = 0, na = 0; i < mult[2]; i++)
                for (int j = 0; j < mult[1]; j++)
                    for (int k = 0; k < mult[0]; k++)
                        for (int l = 0, p = 0, pa = 0; l < nAtoms; l++, pa++, na++) {
                            types[na] = typesInCell[pa];
                            atoms[na] = templates[types[na]];
                            for (int m = 0; m < 3; m++, n++, p++)
                                coords.setFloat(n, coordsInCell[p] + i * affine[2][m] + j * affine[1][m] + k * affine[0][m]);
                        }
            atomicStructure.setCurrentCoords(coords);
            ObjectDataArray atomDataArray = (ObjectDataArray) DataArray.create(atoms, 1, "atoms");
            atomicStructure.addComponent(DataArray.create(types, 1, "types"));
            atomicStructure.addComponent(atomDataArray);
            int[] pts = new int[gMult * nAtoms];
            for (int i = 0; i < gMult * nAtoms; i++)
                pts[i] = i;
            CellArray ca = new CellArray(CellType.POINT, pts, null, pts);
            boolean[][] bonded = new boolean[gMult * nAtoms][gMult * nAtoms];
            int nBonds = 0;
            for (int i = 0; i < gMult * nAtoms; i++)
                for (int j = i + 1; j < gMult * nAtoms; j++) {
                    float d = 0;
                    for (int k = 0; k < 3; k++)
                        d += (coords.getFloat(3 * i + k) - coords.getFloat(3 * j + k)) * (coords.getFloat(3 * i + k) - coords.getFloat(3 * j + k));
                    float d0 = atoms[i].getRadius() + atoms[j].getRadius();
                    bonded[i][j] = d < .5f * d0 * d0;
                    if (bonded[i][j])
                        nBonds += 1;
                }
            int[] bonds = new int[2 * nBonds];
            for (int i = 0, k = 0; i < gMult * nAtoms; i++)
                for (int j = i + 1; j < gMult * nAtoms; j++)
                    if (bonded[i][j]) {
                        bonds[k] = i;
                        bonds[k + 1] = j;
                        k += 2;
                    }
            CellArray bondCells = new CellArray(CellType.SEGMENT, bonds, null, bonds);
            CellSet cs = new CellSet();
            cs.addCells(ca);
            cs.addCells(bondCells);
            ObjectDataArray bondDataArray = (ObjectDataArray) DataArray.create(atoms, 1, "atoms");
            cs.addComponent(bondDataArray);
            cs.generateDisplayData(coords);
            atomicStructure.addCellSet(cs);

            int[] dimsOfCell = new int[3];
            int[] dims = new int[3];
            int nNodes = 1;
            for (int i = 0; i < 3; i++) {
                dimsOfCell[i] = scanner.nextInt();
                dims[i] = mult[i] * dimsOfCell[i];
                nNodes *= dims[i];
            }
            outRegularField = new RegularField(dims);
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    affine[i][j] /= dimsOfCell[i];
            outRegularField.setName(name);
            outRegularField.setAffine(affine);
            int nOfCell = dimsOfCell[0] * dimsOfCell[1] * dimsOfCell[2];
            float[] densityInCell = new float[nOfCell];
            for (int i = 0; i < densityInCell.length; i++)
                densityInCell[i] = scanner.nextFloat();
            float[] density = new float[nNodes];
            for (int i = 0, ii = 0; i < mult[2]; i++, ii += dimsOfCell[2])
                for (int j = 0, jj = 0; j < mult[1]; j++, jj += dimsOfCell[1])
                    for (int k = 0, kk = 0; k < mult[0]; k++, kk += dimsOfCell[1])
                        for (int l = 0, iin = 0; l < dimsOfCell[2]; l++)
                            for (int m = 0; m < dimsOfCell[1]; m++)
                                for (int n = 0, iout = ((ii + l) * dims[1] + jj + m) * dims[0] + kk; n < dimsOfCell[0]; n++, iin++, iout++)
                                    density[iout] = densityInCell[iin];

            outRegularField.addComponent(DataArray.create(density, 1, "density"));
            return 0;
        } catch (Exception e) {
            outputError("error in data file " + filePath, filePath, 0, null);
            e.printStackTrace();
            return 2;
        }

    }

    static void outputError(String text, String fname, int lineNumber, Exception e)
    {
        System.err.println("ERROR: " + text + "; in function " + fname + " line " + lineNumber);
        //e.printStackTrace();   
    }

    private VisualAtomTemplate[] getAtoms(File potCarFile)
    {
        Vector<String> tmp = new Vector<String>();
        VisualAtomTemplate[] templates = AtomGeometryTemplates.stdAtomTemplates;
        try {
            LineNumberReader potCarReader = new LineNumberReader(new FileReader(potCarFile));
            String line = "  ";
            do {
                line = line.trim();
                if (line.startsWith("TITEL"))
                    tmp.add(line.split(" +")[3]);
                line = potCarReader.readLine();
            } while (line != null);
            if (tmp.isEmpty())
                return null;
            VisualAtomTemplate[] out = new VisualAtomTemplate[tmp.size()];
            for (int i = 0; i < out.length; i++) {
                out[i] = templates[0];
                for (int j = 0; j < templates.length; j++)
                    if (tmp.get(i).startsWith(templates[j].getSymbol())) {
                        out[i] = templates[j];
                        break;
                    }
            }
            return out;
        } catch (Exception ex) {
            LOGGER.error("Error while getting atoms ", ex);
        }
        return null;
    }
    
    @Override
    protected void notifySwingGUIs(final pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();
        notifyGUIs(p, isFromVNA(), false);
        
        if (p.get(FILENAME) == null || p.get(FILENAME).isEmpty())
            return;
        ProgressAgent progressAgent = getProgressAgent(140); //20 for download from gsiftp, 100 for read, 20 for geometry
        String userFileName = p.get(FILENAME);
        try {
            if (!GridFTPLocalUtils.isGSIUri(userFileName));
            {
                File potCarFile = new File(new File(userFileName).getParentFile().getCanonicalPath() + File.separator + "POTCAR");
                if (potCarFile.canRead())
                    templates = getAtoms(potCarFile);
            }
            File densityFile = GridFTPLocalUtils.getFileOrLocalGSIFTPCopy(userFileName, progressAgent.getSubAgent(20));
            readVASP(densityFile.getAbsolutePath(), p);
        } catch (GridFTPException | IOException ex) {
            LOGGER.error("Error while reading file " + userFileName, ex);
            VisNow.get().userMessageSend(this, "Error reading file: " + userFileName, "See log for details", Level.ERROR);
        }

        if (outRegularField == null)
            return;
        setOutputValue("volume", new VNRegularField(outRegularField));
        setOutputValue("atoms", new VNIrregularField(atomicStructure));
        outField = outRegularField;
        //if (!params.isShow())
        //    return;
        prepareOutputGeometry();
        show();
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            computeUI.activateOpenDialog();
    }

}
