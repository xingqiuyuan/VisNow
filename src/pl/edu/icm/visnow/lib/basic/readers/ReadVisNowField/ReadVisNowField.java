//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * VisNow field (VNF) format handling.
 *
 * @author Krzysztof Nowinski, University of Warsaw, ICM
 */
public class ReadVisNowField extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ReadVisNowField.class);

    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI = null;
    protected Parser headerParser = null;
    protected FieldIOSchema schema = null;
    protected String fileName = null;
    protected int nCurrentThreads;
    protected int continuousColorAdjustingLimit
        = Integer.parseInt(VisNow.get().getMainConfig().getProperty("visnow.continuousColorAdjustingLimit"));

    /**
     * Creates a new instance of CreateGrid
     */
    public ReadVisNowField()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {

            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameters(parameters);
                ui.addComputeGUI(computeUI);
            }
        });
        setPanel(ui);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(ReadVisNowFieldShared.FILENAME, "")};
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    private void outputNullField()
    {
        setOutputValue("regularOutField", null);
        setOutputValue("irregularOutField", null);
        prepareOutputGeometry();
        show();
    }

    @Override
    protected void notifySwingGUIs(final pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        long startTime = System.nanoTime();

        LOGGER.debug("isFromVNA: " + isFromVNA());
        Parameters parametersClone = parameters.getReadOnlyClone();
        notifyGUIs(parametersClone, false, false);

        fileName = parametersClone.get(ReadVisNowFieldShared.FILENAME);
        if (!fileName.isEmpty()) {
            boolean isSerializedField;
            try (FileInputStream in = new FileInputStream(fileName)) {
                byte[] magic = new byte[2];
                int count = in.read(magic);
                if (count < 2) {
                    isSerializedField = false;
                } else {
                    isSerializedField = (magic[0] == -84 && magic[1] == -19);
                }
                in.close();
            } catch (IOException ex) {
                return;
            }
            if (isSerializedField) {
                try (BufferedInputStream bin = new BufferedInputStream(new FileInputStream(fileName)); ObjectInputStream in = new ObjectInputStream(bin)) {
                    outField = (Field) in.readObject();
                    in.close();
                } catch (IOException | ClassNotFoundException ex) {
                    return;
                }
            } else {
                try {
                    headerParser = new Parser(fileName, false, null);
                    schema = headerParser.parseFieldHeader();
                    if (schema == null) {
                        outputNullField();
                        return;
                    }
                    outField = schema.getField();
                    if (outField == null) {
                        outputNullField();
                        return;
                    }
                } catch (IOException e) {
                    outputNullField();
                    return;
                }
                ProgressAgent progressAgent = getProgressAgent(100 * schema.getFileSchemas().size());

                for (int i = 0; i < schema.getFileSchemas().size(); i++) {
                    ReadFile rf = new ReadFile(outField, schema, i, schema.getFileSchemas().size(), false, null, progressAgent);
                    if (!rf.isCorrect())
                        return;
                    rf.run();
                    setProgress((float) i / schema.getFileSchemas().size());
                }
                outField.setCurrentTime(0);
                if (outField.hasCoords())
                    outField.updatePreferredExtents();

                if (outField.hasMask())
                    for (DataArray da : outField.getComponents())
                        da.recomputeStatistics(outField.getMask(), false);
                else
                    for (DataArray da : outField.getComponents())
                        da.recomputeStatistics(false);
                if (outField instanceof IrregularField) {
                    outIrregularField = (IrregularField) outField;
                    for (CellSet cs : outIrregularField.getCellSets())
                        for (DataArray da : cs.getComponents())
                            da.recomputeStatistics();
                }

            }
        }
        if (outField != null) {
            if (outField instanceof RegularField) {
                outRegularField = (RegularField) outField;
                outIrregularField = null;
                setOutputValue("regularOutField", new VNRegularField(outRegularField));
                setOutputValue("irregularOutField", null);
            } else {
                outRegularField = null;
                outIrregularField = (IrregularField) outField;
                setOutputValue("regularOutField", null);
                setOutputValue("irregularOutField", new VNIrregularField((outIrregularField)));
            }
            prepareOutputGeometry();
            show();
        }

        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        LOGGER.info("duration (s): " + (double) duration / 1000000000.0);
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }

    /**
     * Compute interface getter.
     *
     * @return Compute UI
     */
    public GUI getGUI()
    {
        return computeUI;
    }

    /**
     * Reads a file in VisNow field format from given URL.
     *
     * @param url URL to file
     *
     * @return Field stored in a given file
     */
    public Field readVnfFromURL(String url)
    {
        FieldIOSchema schema;
        Field outField;
        Parser headerParser;
        try {
            headerParser = new Parser(url, true, null);
            schema = headerParser.parseFieldHeader();
            if (schema == null) {
                return null;
            }
            outField = schema.getField();
            if (outField == null) {
                return null;
            }
        } catch (IOException e) {
            return null;
        }
        ProgressAgent progressAgent = getProgressAgent(100 * schema.getFileSchemas().size());
        for (int i = 0; i < schema.getFileSchemas().size(); i++) {
            ReadFile rf = new ReadFile(outField, schema, i, schema.getFileSchemas().size(), true, null, progressAgent);
            if (!rf.isCorrect())
                return null;
            rf.run();
        }
        outField.setCurrentTime(0);
        if (outField.getCurrentCoords() != null)
            outField.updatePreferredExtents();

        for (DataArray da : outField.getComponents())
            da.recomputeStatistics();

        return outField;
    }

    /**
     * Reads a file in VisNow field format.
     *
     * @param filePath path to file
     *
     * @return Field stored in a given file
     */
    public Field readVnf(String filePath)
    {
        ProgressAgent progressAgent = getProgressAgent(100 * schema.getFileSchemas().size());
        return readVnf(filePath, progressAgent);
    }

    /**
     * Reads a file in VisNow field format.
     *
     * @param filePath      path to file
     * @param progressAgent progress agent to monitor read progress
     *
     * @return Field stored in a given file
     */
    public static Field readVnf(String filePath, ProgressAgent progressAgent)
    {
        FieldIOSchema schema;
        Field outField;
        Parser headerParser;

        if (filePath == null)
            return null;

        File f = new File(filePath);
        if (!f.exists() || !f.canRead())
            return null;

        try {
            headerParser = new Parser(filePath, false, null);
            schema = headerParser.parseFieldHeader();
            if (schema == null)
                return null;
            outField = schema.getField();
        } catch (IOException e) {
            return null;
        }
        for (int i = 0; i < schema.getFileSchemas().size(); i++) {
            ReadFile rf = new ReadFile(outField, schema, i, schema.getFileSchemas().size(), false, null, progressAgent);
            if (!rf.isCorrect())
                return null;
            rf.run();
        }
        outField.setCurrentTime(0);
        if (outField.getCurrentCoords() != null)
            outField.updatePreferredExtents();

        for (DataArray da : outField.getComponents())
            da.recomputeStatistics();

        if (outField instanceof IrregularField) {
            IrregularField outIrregularField = (IrregularField) outField;
            for (CellSet cs : outIrregularField.getCellSets())
                for (DataArray da : cs.getComponents())
                    da.recomputeStatistics();
        }
        return outField;
    }
}
