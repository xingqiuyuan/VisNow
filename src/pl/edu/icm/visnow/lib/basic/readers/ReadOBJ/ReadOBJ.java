/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.readers.ReadOBJ;

import com.sun.j3d.loaders.IncorrectFormatException;
import com.sun.j3d.loaders.ParsingErrorException;
import com.sun.j3d.loaders.Scene;
import com.sun.j3d.loaders.objectfile.ObjectFile;
import com.sun.j3d.utils.geometry.GeometryInfo;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedTriangleArray;
import javax.media.j3d.Shape3D;
import javax.swing.SwingUtilities;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import static pl.edu.icm.visnow.lib.basic.readers.ReadOBJ.ReadOBJShared.READOBJ_PATH;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * @author theki
 */
public class ReadOBJ extends OutFieldVisualizationModule
{
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ReadOBJ.class);
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;

    /**
     * Creates a new instance of Convolution
     */
    public ReadOBJ()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {

            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadOBJShared.createDefaultParametersAsList().toArray(new Parameter[]{});
    }

    @Override
    protected void notifySwingGUIs(final pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {

        Parameters p = parameters.getReadOnlyClone();
        outIrregularField = createOBJGeometry(p.get(READOBJ_PATH));

        if (outIrregularField != null)
            setOutputValue("outField", new VNIrregularField(outIrregularField));
        else
            setOutputValue("outField", null);
        outField = outIrregularField;
        prepareOutputGeometry();
        irregularFieldGeometry.getFieldDisplayParams().getRenderingParams().setShadingMode(RenderingParams.FLAT_SHADED);
        show();

    }

    public IrregularField createOBJGeometry(String path)
    {
        if (path == null) {
            LOGGER.info("path == null");
            return null;
        }

        ObjectFile a = new ObjectFile();
        a.setFlags(ObjectFile.RESIZE | ObjectFile.TRIANGULATE | ObjectFile.STRIPIFY);

        Scene s = null;

        try {
            s = a.load(path);
        } catch (FileNotFoundException ex) {
            LOGGER.info("File " + path + " not found");
            VisNow.get().userMessageSend(this, "Error loading obj", "File " + path + " does not exist.", pl.edu.icm.visnow.system.utils.usermessage.Level.ERROR);
            return null;
        } catch (IncorrectFormatException ex) {
            LOGGER.info("Incorrect format of obj");
            VisNow.get().userMessageSend(this, "Incorrect format of obj", "Change format of: " + path + " ", pl.edu.icm.visnow.system.utils.usermessage.Level.ERROR);
            return null;
        } catch (ParsingErrorException ex) {
            LOGGER.info("Incorrect parsing format of obj");
            VisNow.get().userMessageSend(this, "Incorrect parsing format of obj", "Change format of: " + path + " ", pl.edu.icm.visnow.system.utils.usermessage.Level.ERROR);
            return null;
        }

        if (s == null) {
            VisNow.get().userMessageSend(this, "Incorrect format of obj", "Change format of: " + path + " ", pl.edu.icm.visnow.system.utils.usermessage.Level.ERROR);
            return null;
        }

        GeometryInfo geomInfo;
        try {
            geomInfo = new GeometryInfo((GeometryArray) ((Shape3D) s.getSceneGroup().getChild(0)).getGeometry());
        } catch (Exception ex) {
            LOGGER.info("Incorrect parsing format of obj");
            VisNow.get().userMessageSend(this, "Incorrect parsing format of obj", "Change format of: " + path + " ", pl.edu.icm.visnow.system.utils.usermessage.Level.ERROR);
            return null;
        }
        geomInfo.convertToIndexedTriangles();
        IndexedTriangleArray ita = (IndexedTriangleArray) geomInfo.getIndexedGeometryArray();
        //      ita.setCapability(IndexedTriangleArray.ALLOW_COORDINATE_INDEX_READ);
        //      ita.setCapability(IndexedTriangleArray.ALLOW_COORDINATE_READ);

        int n = ita.getVertexCount();
        IrregularField outFld = new IrregularField(n);
        int count = ita.getIndexCount();
        int[] cells = new int[count];
        float[] coord = new float[n * 3];
        byte[] orientations = new byte[count / 3];
        for (int i = 0; i < orientations.length; i++)
            orientations[i] = 1;
        ita.getCoordinates(0, coord);
        ita.getCoordinateIndices(0, cells);
        outFld.setCurrentCoords(new FloatLargeArray(coord));
        CellArray ca = new CellArray(CellType.TRIANGLE, cells, orientations, null);
        CellSet cs = new CellSet("cells");
        cs.setCellArray(ca);
        //cs.generateExternFaces();
        cs.generateDisplayData(new FloatLargeArray(coord));
        outFld.addCellSet(cs);
        float[] temp1 = new float[n];
        for (int i = 0; i < n; i++)
            temp1[i] = i;
        outFld.addComponent(DataArray.create(temp1, 1, "obj"));
        VisNow.get().userMessageSend(this, "File successfully loaded", "", pl.edu.icm.visnow.system.utils.usermessage.Level.INFO);
        return outFld;
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag())
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    computeUI.activateOpenDialog();
                }
            });
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

}
