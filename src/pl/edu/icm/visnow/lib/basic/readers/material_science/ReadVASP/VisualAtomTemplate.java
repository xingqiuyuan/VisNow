/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.readers.material_science.ReadVASP;

import com.sun.j3d.utils.geometry.*;
import javax.media.j3d.*;
import javax.vecmath.*;
import java.awt.*;
import pl.edu.icm.visnow.geometries.visualObjects.VisualObject;
import pl.edu.icm.visnow.geometries.geometryTemplates.Glyph;
import pl.edu.icm.visnow.geometries.geometryTemplates.ScalarTemplates;

/**
 *
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University
 * Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class VisualAtomTemplate implements VisualObject
{

    private AtomTemplate atomTemplate = null;
    /**
     * user definer radius for ball presentation overriding vdWRadius
     */
    private float userRadius;

    private Material m;

    private Appearance a = new Appearance();

    private float r = 1.f, g = 1.f, b = 1.f;

    /**
     * Colors used for 3d presentation
     */
    /**
     * Colors used for 3d presentation
     */
    private Color color2D = null;

    /**
     * Creates a new instance of AtomTemplate
     */
    public VisualAtomTemplate(AtomTemplate at, float userRadius,
                              float r, float g, float b)
    {
        atomTemplate = at;
        this.userRadius = userRadius;
        this.r = r;
        this.g = g;
        this.b = b;
        color2D = new Color(r, g, b);
        m = new Material(new Color3f(r, g, b), new Color3f(r * 0.2f, g * 0.2f, b * 0.2f),
                         new Color3f(r, g, b), new Color3f(1.0f, 1.0f, 1.0f), 5.f);
        a.setMaterial(m);
        a.setColoringAttributes(new ColoringAttributes(r, g, b, 0));
        a.setCapability(Appearance.ALLOW_MATERIAL_READ);
        a.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
    }

    public VisualAtomTemplate(int number, double mass, String symbol, String name,
                              double vdWRadius, double ionRadius, double userRadius,
                              double r, double g, double b)
    {
        this(new AtomTemplate(number, (float) mass, symbol, name, (float) vdWRadius, (float) ionRadius),
             (float) userRadius, (float) r, (float) g, (float) b);
    }

    public VisualAtomTemplate(int number, float mass, String symbol, String name,
                              float vdWRadius, float ionRadius, float userRadius,
                              float r, float g, float b)
    {
        this(new AtomTemplate(number, mass, symbol, name, vdWRadius, ionRadius),
             userRadius, r, g, b);
    }

    public VisualAtomTemplate(int number, String symbol, String name,
                              float vdWRadius, float ionRadius, float userRadius,
                              float r, float g, float b)
    {
        this(number, 1, symbol, name, vdWRadius, ionRadius, userRadius, r, g, b);
    }

    public VisualAtomTemplate()
    {
        this(new AtomTemplate(), .1f, .2f, .2f, .2f);
    }

    public Node getModel(int mode)
    {
        Node s;
        if (mode == 1)
            s = new Sphere(atomTemplate.getVdWRadius(), Sphere.GENERATE_NORMALS, 40, a);
        else if (mode == 0)
            s = new Sphere(0.16f * atomTemplate.getVdWRadius(), Sphere.GENERATE_NORMALS, 10, a);
        else
            s = new Sphere(0.1f, Sphere.GENERATE_NORMALS, 5, a);
        s.setCapability(Sphere.GEOMETRY_NOT_SHARED);
        s.setCapability(Sphere.ENABLE_APPEARANCE_MODIFY);
        return s;
    }

    public Glyph getGlyph(int detail)
    {
        return new ScalarTemplates.SphereTemplate(detail);
    }

    public Glyph getGlyph()
    {
        return new ScalarTemplates.SphereTemplate();
    }

    public String getSymbol()
    {
        return atomTemplate.getSymbol();
    }

    /**
     * Getter for property a.
     * <p>
     * @return Value of property a.
     *
     */
    public javax.media.j3d.Appearance getAppearance()
    {
        return a;
    }

    public float getRadius()
    {
        return atomTemplate.getVdWRadius();
    }

    public Color3f getColor()
    {
        return new Color3f(r, g, b);
    }

    /**
     * Getter for property number.
     * <p>
     * @return Value of property number.
     *
     */
    public int getNumber()
    {
        return atomTemplate.getNumber();
    }
    
    public String toString() {
        return atomTemplate.getSymbol();
    }

    /**
     * Getter for property color2D.
     * <p>
     * @return Value of property color2D.
     *
     */
    public java.awt.Color getColor2D()
    {
        return color2D;
    }

    /**
     * Getter for property mass.
     * <p>
     * @return Value of property mass.
     *
     */
    public float getMass()
    {
        return atomTemplate.getMass();
    }

    public void setMass(float m)
    {
        atomTemplate.setMass(m);
    }

    public void setVdWRadius(float r)
    {
        atomTemplate.setVdWRadius(r);
    }

    public void setIonRadius(float r)
    {
        atomTemplate.setIonRadius(r);
    }

    public void setUserRadius(float r)
    {
        userRadius = r;
    }

    public float toFloat()
    {
        return getRadius();
    }

    public void setColor(float r, float g, float b)
    {
        this.color2D = new Color(r, g, b);
        m = new Material(new Color3f(r, g, b), new Color3f(r * 0.2f, g * 0.2f, b * 0.2f),
                         new Color3f(r, g, b), new Color3f(1.0f, 1.0f, 1.0f), 5.f);
        a.setMaterial(m);
    }

    public byte[] getARGB()
    {
        byte[] argb = new byte[4];
        argb[0] = (byte) (0xff & 255);
        argb[1] = (byte) (0xff & 255);
        argb[2] = (byte) (0xff & 255);
        argb[3] = (byte) (0xff & 255);
        return argb;
    }

}
