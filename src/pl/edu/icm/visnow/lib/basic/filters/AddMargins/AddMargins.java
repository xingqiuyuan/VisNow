//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.AddMargins;

import java.util.ArrayList;
import java.util.Arrays;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;
import static pl.edu.icm.visnow.lib.basic.filters.AddMargins.AddMarginsShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.PaddingType;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static pl.edu.icm.visnow.lib.utils.field.ExtendMargins.*;
import pl.edu.icm.jscic.utils.VectorMath;
import pl.edu.icm.jlargearrays.LargeArray;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class AddMargins extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(AddMargins.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI = null;
    protected RegularField inField = null;
    private int runQueue = 0;

    public AddMargins()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(MARGINS, new long[][]{{10, 10}, {0, 0}, {0, 0}}),
            new Parameter<>(PADDING_TYPE, PaddingType.ZERO),
            new Parameter<>(REFLECT_VECTORS, false),
            new Parameter<>(SELECTED_COMPONENTS, new int[]{}),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{})
        };
    }

    private static long[] computeOutDims(long[] inDims, long[][] margins)
    {
        long[] out
                = {
                    1, 1, 1
                };
        int space = inDims.length;
        for (int i = 0; i < 3; i++) {
            long[] is = margins[i];
            if (is[0] > 0 || is[1] > 0) {
                if (i < inDims.length) {
                    out[i] = inDims[i] + is[0] + is[1];
                } else {
                    out[i] = is[0] + is[1] + 1;
                }
            } else if (i < inDims.length) {
                out[i] = inDims[i];
            }
        }
        for (int i = 2; i >= 0; i--) {
            if (out[i] > 1) {
                space = i + 1;
                break;
            }
        }
        long[] outDims = new long[space];
        System.arraycopy(out, 0, outDims, 0, space);
        return outDims;
    }

    protected void extendedField(Parameters p)
    {
        long[][] margins = p.get(MARGINS);
        int[] selectedComponents = p.get(SELECTED_COMPONENTS);
        if (selectedComponents.length == 0) {
            outRegularField = null;
            return;
        }
        long[] dims = inField.getLDims();
        long[] outDims = computeOutDims(dims, margins);
        inField.checkTrueNSpace();
        if (inField.getCurrentCoords() != null) {
            return;
        }
        outRegularField = new RegularField(outDims);

        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < inAffine.length; i++) {
            System.arraycopy(inAffine[i], 0, outAffine[i], 0, 3);
        }

        if (dims.length == 1 && outDims.length > 1) {
            outAffine[1] = VectorMath.vectorPerpendicular(inAffine[0], false);
            outAffine[1] = VectorMath.vectorMultiplyScalar(VectorMath.vectorNormalize(outAffine[1], false), VectorMath.vectorNorm(outAffine[0]), false);
        }
        
        if (dims.length <= 2 && outDims.length == 3) {
            outAffine[2] = VectorMath.crossProduct(outAffine[0], outAffine[1], false);
            outAffine[2] = VectorMath.vectorMultiplyScalar(VectorMath.vectorNormalize(outAffine[2], false), VectorMath.vectorNorm(outAffine[0]), false);
        }
        
        for (int vectorIndex = 0; vectorIndex < 3; vectorIndex++)
            for (int i = 0; i < 3; i++)
                outAffine[3][i] -= margins[vectorIndex][0] * outAffine[vectorIndex][i];

        outRegularField.setAffine(outAffine);

        for (int n = 0; n < selectedComponents.length; n++) {
            DataArray da = inField.getComponent(selectedComponents[n]);
            int frames = da.getNFrames();
            if (frames == 1) {
                outRegularField.addComponent(DataArray.create(extendMargins(dims, da.getVectorLength(), outDims, margins, da.getRawArray(), parameters.get(PADDING_TYPE), parameters.get(REFLECT_VECTORS)), da.getVectorLength(), da.getName()));
            } else {
                ArrayList<Float> times = da.getTimeSeries();
                ArrayList<LargeArray> timeValues = new ArrayList<>(frames);
                for (Float time : times) {
                    timeValues.add(extendMargins(dims, da.getVectorLength(), outDims, margins, da.getRawArray(time), parameters.get(PADDING_TYPE), parameters.get(REFLECT_VECTORS)));
                }
                TimeData timeData = new TimeData(times, timeValues, 0);
                outRegularField.addComponent(DataArray.create(timeData, da.getVectorLength(), da.getName()));
            }
        }
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_COMPONENT_NAMES, inField.getComponentNames());
        if (resetParameters && inField.getNComponents() > 0)
            parameters.set(SELECTED_COMPONENTS, new int[]{0});
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("isFromVNA = " + isFromVNA());

        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getComponentNames(), newField.getComponentNames()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p;

            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                extendedField(p);
                if (outRegularField == null) {
                    setOutputValue("outField", null);
                } else {
                    setOutputValue("outField", new VNRegularField(outRegularField));
                }
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
