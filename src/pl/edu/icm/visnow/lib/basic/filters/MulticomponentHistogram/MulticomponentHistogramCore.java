/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.filters.MulticomponentHistogram;

import java.util.ArrayList;
import java.util.HashMap;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jscic.utils.MatrixMath;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class MulticomponentHistogramCore
{
    private Field inField;
    private RegularField outField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    public final static String IN_FIELD_MAIN = "inField";
    public final static String OUT_RFIELD_MAIN = "outField";

    public MulticomponentHistogramCore()
    {
    }

    public void update(MulticomponentHistogramCoreParams params)
    {
        if (inField == null) {
            return;
        }

        outField = null;

        HistogramOperation[] tmpOps = params.getHistogramOperations();
        HistogramOperation[] ops = null;
        int nOps = 0;
        if (tmpOps != null)
            nOps = tmpOps.length;

        nOps++;
        ops = new HistogramOperation[nOps];
        ops[0] = new HistogramOperation(params.isCountLogScale(), params.getLogConstant(), params.isCountDropBackground());
        for (int i = 0; i < nOps - 1; i++) {
            ops[i + 1] = tmpOps[i];
        }

        int[] tmp = params.getDims();
        int nDims = params.getNDims();
        int[] histDims = new int[nDims];
        for (int i = 0; i < nDims; i++) {
            histDims[i] = tmp[i];
        }

        float[][] affine = new float[4][3];
        float[][] ext = new float[2][3];
        float[][] physExt = new float[2][3];
        ArrayList<DataArray> outData = new ArrayList<DataArray>();
        boolean roundByteDimsTo32 = params.isRoundByteDimsTo32();

        switch (params.getBinning()) {
            case MulticomponentHistogramCoreParams.BINNING_BY_COMPONENTS:
                DataArray[] data = new DataArray[nDims];
                int[] sel = params.getSelectedComponents();
                for (int i = 0; i < nDims; i++) {
                    data[i] = inField.getComponent(sel[i]);
                }

                for (int i = 0; i < ops.length; i++) {
                    DataArray hist = null;
                    if (ops[i].getComponent() == null || ops[i].getComponent().getVectorLength() == 1) {
                        hist = HistogramBuilder.buildDataHistogram(histDims, roundByteDimsTo32, data, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                    } else {
                        hist = HistogramBuilder.buildVectorDataHistogram(histDims, roundByteDimsTo32, data, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                    }
                    if (hist != null) {
                        outData.add(hist);
                    }
                    fireStatusChanged((float) (i + 1) / (float) ops.length);
                }

                outField = new RegularField(histDims);
                for (int i = 0; i < outData.size(); i++) {
                    outField.addComponent(outData.get(i));
                }
                
                if (!params.isOutGeometryToData()) {
                    for (int i = 0; i < histDims.length; i++) {
                        affine[i][i] = 1.0f;
                    }
                    outField.setAffine(affine);
                    ext = outField.getExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        if (roundByteDimsTo32 && data[i].getType() == DataArrayType.FIELD_DATA_BYTE) {
                            physExt[0][i] = 0.0f;
                            physExt[1][i] = 255.0f;
                        } else {
                            physExt[0][i] = (float) data[i].getPreferredPhysMinValue();
                            physExt[1][i] = (float) data[i].getPreferredPhysMaxValue();
                        }
                    }
                    outField.setPreferredExtents(ext, physExt);
                } else {                        
                    for (int i = 0; i < histDims.length; i++) {
                        ext[0][i] = (float)data[i].getPreferredMinValue();
                        ext[1][i] = (float)data[i].getPreferredMaxValue();
                        physExt[0][i] = (float) data[i].getPreferredPhysMinValue();
                        physExt[1][i] = (float) data[i].getPreferredPhysMaxValue();
                    }
                    affine = MatrixMath.computeAffineFromExtents(histDims, ext); 
                    outField.setAffine(affine);
                    outField.setPreferredExtents(ext, physExt);       
                }
                
                String[] axesNames = new String[]{"","",""};
                for (int i = 0; i < nDims; i++) {
                    axesNames[i] = data[i].getName();
                }
                outField.setAxesNames(axesNames);

                break;
            case MulticomponentHistogramCoreParams.BINNING_BY_COORDINATES:

                int[] histCoords = params.getSelectedCoords();

                for (int i = 0; i < ops.length; i++) {
                    float[] hist = null;

                    if (ops[i].getComponent() == null || ops[i].getComponent().getVectorLength() == 1) {
                        hist = HistogramBuilder.buildCoordsHistogram(histDims, histCoords, inField, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                        if (hist != null) {
                            outData.add(DataArray.create(hist, 1, ops[i].toString()));
                        }
                    } else {
                        hist = HistogramBuilder.buildVectorCoordsHistogram(histDims, histCoords, inField, ops[i], params.getFilterConditions(), params.getFilterConditionsLogic());
                        if (hist != null) {
                            if (ops[i].getOperation() == HistogramOperation.Operation.VSTD) {
                                int veclen = ops[i].getComponent().getVectorLength();
                                int nData = hist.length / (veclen * veclen);
                                for (int v = 0; v < veclen; v++) {
                                    float[] tmpHist = new float[nData * veclen];
                                    for (int m = 0; m < nData; m++) {
                                        for (int l = 0; l < veclen; l++) {
                                            //tmpHist[m*veclen + l] = hist[m*veclen*veclen + v*veclen + l];
                                            tmpHist[m * veclen + l] = hist[m * veclen * veclen + l * veclen + v];
                                        }
                                    }
                                    outData.add(DataArray.create(tmpHist, veclen, ops[i].toString() + "_" + v));
                                }
                            } else {
                                outData.add(DataArray.create(hist, ops[i].getComponent().getVectorLength(), ops[i].toString()));
                            }
                        }
                    }

                    fireStatusChanged((float) (i + 1) / (float) ops.length);
                }

                outField = new RegularField(histDims);
                for (int i = 0; i < outData.size(); i++) {
                    outField.addComponent(outData.get(i));
                }
           
                if (!params.isOutGeometryToData()) {
                    float[][] inPhysExt = inField.getPreferredPhysicalExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        affine[i][i] = 1.0f;
                    }
                    outField.setAffine(affine);
                    ext = outField.getExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        physExt[0][i] = inPhysExt[0][histCoords[i]];
                        physExt[1][i] = inPhysExt[1][histCoords[i]];
                    }
                    outField.setPreferredExtents(ext, physExt);
                } else {  
                    float[][] inExt = inField.getPreferredExtents();
                    float[][] inPhysExt = inField.getPreferredPhysicalExtents();
                    for (int i = 0; i < histDims.length; i++) {
                        ext[0][i] = inExt[0][histCoords[i]];
                        ext[1][i] = inExt[1][histCoords[i]];
                        physExt[0][i] = inPhysExt[0][histCoords[i]];
                        physExt[1][i] = inPhysExt[1][histCoords[i]];
                    }
                    affine = MatrixMath.computeAffineFromExtents(histDims, ext); 
                    outField.setAffine(affine);
                    outField.setPreferredExtents(ext, physExt);
                }
                
                break;
        }

        if (outField.getNComponents() == 0)
            outField = null;

        fireStatusChanged(1.0f);
    }

    public Field getOutField(String name)
    {
        if (name.equals(OUT_RFIELD_MAIN))
            return outField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }

    public void setInField(String name, Field inField)
    {
        if (name.equals(IN_FIELD_MAIN))
            this.inField = inField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }

    //    private abstract class HistogramOperationThread extends Thread{
    //        protected float[] hist = null;
    //        
    //        public float[] getHistogram() {
    //            return hist;
    //        }
    //    }
    //    
    //    private class DataScalarHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;
    //        private DataArray[] data; 
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public DataScalarHistogramOperationThread(int[] histDims, DataArray[] data, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.data = data;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildDataHistogram(histDims, data, op, filterConditions, filterConditionsLogic);                                
    //        }
    //    }
    //
    //    private class DataVectorHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;
    //        private DataArray[] data; 
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public DataVectorHistogramOperationThread(int[] histDims, DataArray[] data, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.data = data;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildVectorDataHistogram(histDims, data, op, filterConditions, filterConditionsLogic);                        
    //        }
    //    }
    //
    //    private class CoordsScalarHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;        
    //        private int[] histCoords;        
    //        private Field field;
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public CoordsScalarHistogramOperationThread(int[] histDims, int[] histCoords, Field field, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.histCoords = histCoords;
    //            this.field = field;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildCoordsHistogram(histDims, histCoords, field, op, filterConditions, filterConditionsLogic);                    
    //        }
    //    }
    //    
    //    private class CoordsVectorHistogramOperationThread extends HistogramOperationThread {
    //        private int[] histDims;        
    //        private int[] histCoords;        
    //        private Field field;
    //        private HistogramOperation op;
    //        private Condition[] filterConditions; 
    //        private Condition.Logic[] filterConditionsLogic;       
    //        
    //        public CoordsVectorHistogramOperationThread(int[] histDims, int[] histCoords, Field field, HistogramOperation op, Condition[] filterConditions, Condition.Logic[] filterConditionsLogic) {
    //            this.histDims = histDims;
    //            this.histCoords = histCoords;
    //            this.field = field;
    //            this.op = op;    
    //            this.filterConditions = filterConditions;
    //            this.filterConditionsLogic = filterConditionsLogic;
    //        }
    //        
    //        @Override
    //        public void run() {
    //            hist = HistogramBuilder.buildVectorCoordsHistogram(histDims, histCoords, field, op, filterConditions, filterConditionsLogic);                    
    //        }
    //    }
    //    
    private transient FloatValueModificationListener statusListener = null;

    public void addFloatValueModificationListener(FloatValueModificationListener listener)
    {
        if (statusListener == null)
            this.statusListener = listener;
        else
            System.out.println("" + this + ": only one status listener can be added");
    }

    private void fireStatusChanged(float status)
    {
        FloatValueModificationEvent e = new FloatValueModificationEvent(this, status, true);
        if (statusListener != null)
            statusListener.floatValueChanged(e);
    }

}
