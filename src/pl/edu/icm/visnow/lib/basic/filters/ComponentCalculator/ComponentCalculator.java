/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.filters.ComponentCalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.LongLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.NO_RUN;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.RUN_ONCE;
import static pl.edu.icm.visnow.lib.basic.filters.ComponentCalculator.ComponentCalculatorShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.StringUtils;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.expressions.ArrayExpressionParser;
import pl.edu.icm.visnow.lib.utils.expressions.ExpressionOperators;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;

/**
 *
 ** @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class ComponentCalculator extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ComponentCalculator.class);

    private GUI computeUI = null;
    private Field inField = null;

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private int runQueue = 0;

    public ComponentCalculator()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParameterProxy(parameters);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(EXPRESSION_LINES, new String[]{}),
            new Parameter<>(PRECISION, Precision.SINGLE),
            new Parameter<>(META_IS_GENERATOR, true),
            //
            new Parameter<>(INPUT_ALIASES, new String[]{}),
            new Parameter<>(IGNORE_UNITS, true),
            new Parameter<>(RETAIN, false),
            //            new Parameter<>(META_FIELD_DIMENSION_LENGTHS, new int[]{2, 2}),
            new Parameter<>(META_COMPONENT_NAMES, new String[]{}),
            new Parameter<>(META_COMPONENT_VECLEN, new int[]{}),
            //
            new Parameter<>(GENERATOR_DIMENSION_LENGTHS, new int[]{50, 50, 50}),
            new Parameter<>(GENERATOR_EXTENTS, new float[][]{{-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f, 0.5f}}),
            //
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_IS_GENERATOR, inField == null);

        if (parameters.get(META_IS_GENERATOR)) {
            //validate extents
            float[][] extents = parameters.get(GENERATOR_EXTENTS);
            for (int i = 0; i < extents[0].length; i++)
                if (extents[0][i] + 10000 * Math.ulp(extents[0][i]) > extents[1][i]) extents[1][i] = extents[0][i] + 10000 * Math.ulp(extents[0][i]);
            parameters.set(GENERATOR_EXTENTS, extents);
        } else {
            parameters.set(META_COMPONENT_NAMES, inField.getComponentNames());
            parameters.set(META_COMPONENT_VECLEN, inField.getComponentVectorLengths());
        }

        if (!parameters.get(META_IS_GENERATOR)) {
            String[] aliasesToValidate;

            if (resetParameters) //create new aliases
                aliasesToValidate = inField.getComponentNames();
            else //validate aliases
                aliasesToValidate = parameters.get(INPUT_ALIASES);

            Set<String> reservedAliases = new HashSet<String>();
            for (ExpressionOperators.Operator op : ExpressionOperators.Operator.values())
                reservedAliases.add(op.toString());

            for (int i = 0; i < aliasesToValidate.length; i++) {
                if (resetParameters || !isAliasValid(aliasesToValidate[i]) || reservedAliases.contains(aliasesToValidate[i])) {
                    String alias = findSmartAlias(reservedAliases, aliasesToValidate[i]);
                    aliasesToValidate[i] = alias;
                }
                reservedAliases.add(aliasesToValidate[i]);
            }

            parameters.set(INPUT_ALIASES, aliasesToValidate);
        }

        parameters.setParameterActive(true);
    }

    private boolean isAliasValid(String name)
    {
        return name.matches("[a-zA-Z][a-zA-Z0-9]*");
    }

    private String findSmartAlias(Set<String> reservedAliases, String name)
    {
        String[] wordsTrimmed3 = name.replaceAll("[^a-zA-Z0-9 ]", " ").split(" ");
        for (int i = 0; i < wordsTrimmed3.length; i++)
            wordsTrimmed3[i] = wordsTrimmed3[i].substring(0, Math.min(3, wordsTrimmed3[i].length()));

        String newName = StringUtils.join(wordsTrimmed3, "").replaceAll("^\\d+", "");
        if (newName.length() == 0) newName = "x";

        String name3 = newName.substring(0, Math.min(3, newName.length()));
        String name4 = newName.substring(0, Math.min(4, newName.length()));
        String name5 = newName.substring(0, Math.min(5, newName.length()));

        if (!reservedAliases.contains(name3)) return name3;
        else if (!reservedAliases.contains(name4)) return name4;
        else if (!reservedAliases.contains(name5)) return name5;
        else {
            int i = 2;
            while (reservedAliases.contains(name3 + i)) i++;
            return name3 + i;
        }
    }

    @Override
    public void onActive()
    {
        //FIXME: there is a problem because here we don't know if input port is empty or incorrect (empty) field is connected - which makes difference for
        //ComponentCalculator as it works in two modes as a calculator and as a generator

        LOGGER.debug(isFromVNA() + " inField: " + getInputFirstValue("inField"));

        boolean isDifferentField = false;
        boolean isNewField = false;
        if (getInputFirstValue("inField") != null) {
            Field newInField = ((VNField) getInputFirstValue("inField")).getField();
            isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(newInField.getComponentNames(), inField.getComponentNames()));
            isNewField = !isFromVNA() && (inField == null || newInField != inField);
            inField = newInField;
        } else
            inField = null;

        Parameters p;
        synchronized (parameters) {
            validateParamsAndSetSmart(isDifferentField);
            p = parameters.getReadOnlyClone();
        }

        notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

        if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
            runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
            try {
                String[] tmpexpr = p.get(EXPRESSION_LINES);
                ArrayList<String> expressions = new ArrayList<>();
                for (String tmp : tmpexpr) {
                    tmp = tmp.trim().replaceAll(";$", "");
                    if (!tmp.isEmpty() && tmp.contains("="))
                        expressions.add(tmp);
                }

                outField = null;
                outRegularField = null;
                outIrregularField = null;

//                if (!expressions.isEmpty())
                if (inField == null) outField = generateOutField(expressions, p);
                else outField = calculateOutField(expressions, p);

                if (outField != null && outField instanceof RegularField) outRegularField = (RegularField) outField;
                else if (outField != null && outField instanceof IrregularField) outIrregularField = (IrregularField) outField;

                setOutputValue("outRegularField", outRegularField == null ? null : new VNRegularField(outRegularField));
                setOutputValue("outIrregularField", outIrregularField == null ? null : new VNIrregularField(outIrregularField));

                if (outField == null) VisNow.get().userMessageSend(this, "No data calculated", "", Level.INFO);
                else VisNow.get().userMessageSend(this, "Calculated " + outField.getNComponents() + " component(s)", "", Level.INFO);
            } catch (Exception ex) {
                LOGGER.warn("Exception while generating out field", ex);
                VisNow.get().userMessageSend(this, "Error in expression", "" + ex.getMessage(), Level.ERROR);
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
            }

            prepareOutputGeometry();

            show();
        }
    }

    private Field generateOutField(ArrayList<String> expressions, Parameters p) throws Exception
    {
        if (expressions.isEmpty())
            return null;

        int[] dims = p.get(GENERATOR_DIMENSION_LENGTHS);
        if (dims == null) {
            return null;
        }

        int dim = dims.length;
        float[][] extents = p.get(GENERATOR_EXTENTS);
        if (extents == null) {
            return null;
        }

        RegularField out = new RegularField(dims);
        out.setName("Generated field");

        float[][] affine = new float[4][3];
        for (int i = 0; i < dim; i++) {
            affine[3][i] = extents[0][i];
            affine[i][i] = (extents[1][i] - extents[0][i]) / (dims[i] - 1);
        }
        out.setAffine(affine);
        int nNodes = (int) out.getNNodes();

        String[] exprNames = new String[expressions.size()];
        String[] exprExpr = new String[expressions.size()];
        for (int i = 0; i < expressions.size(); i++) {
//            String[] tmpsplit = expressions.get(i).split("=");
//            exprNames[i] = tmpsplit[0].trim();
//            exprExpr[i] = tmpsplit[1].trim();
            String tmp = expressions.get(i);
            int eqpos = tmp.indexOf("=");
            exprNames[i] = tmp.substring(0, eqpos).trim();
            exprExpr[i] = tmp.substring(eqpos + 1).trim();
        }

        String[] fieldVariables = listAllFieldVariables(out, null, p.get(IGNORE_UNITS));
        String[] variablesInUse = ArrayExpressionParser.listVariablesInUse(fieldVariables, exprExpr);
        Map<String, DataArray> vars = prepareVariables(out, variablesInUse, p);

        for (int i = 0; i < expressions.size(); i++) {
            if (exprExpr[i] == null || exprNames[i] == null) {
                continue;
            }

            String name = exprNames[i];
            String expr = exprExpr[i];

            if (out.getComponent(name) != null) {
                throw new Exception("ERROR: wrong result variable name!");
            }
            ArrayExpressionParser parser = new ArrayExpressionParser(nNodes, p.get(PRECISION) == Precision.DOUBLE, p.get(IGNORE_UNITS), vars);

            DataArray result = parser.evaluateExpr(expr);
            if (result != null) {
                result.setName(name);
                out.addComponent(result);
                vars.put(name, result);
                if (result.getVectorLength() > 1) {
                    for (int j = 0; j < result.getVectorLength(); j++) {
                        vars.put(name + "." + j, result);
                    }
                }
            }
        }
        return out;
    }

    private Field calculateOutField(ArrayList<String> expressions, Parameters p) throws Exception
    {
        if (inField == null || expressions.isEmpty() && (inField.getNComponents() == 0 || !p.get(RETAIN)))
            return null;
        else {
            Field out = inField.cloneShallow();
            if (!p.get(RETAIN)) {
                out.removeComponents();
            } else if (p.get(IGNORE_UNITS)) {
                for (int i = 0; i < out.getNComponents(); i++) {
                    out.getComponent(i).setUnit("1");
                }
                out.setCoordsUnit("1");
                out.setTimeUnit("1");
            }

            String[] exprNames = new String[expressions.size()];
            String[] exprExpr = new String[expressions.size()];
            for (int i = 0; i < expressions.size(); i++) {
//            String[] tmpsplit = expressions.get(i).split("=");
//            exprNames[i] = tmpsplit[0].trim();
//            exprExpr[i] = tmpsplit[1].trim();
                String tmp = expressions.get(i);
                int eqpos = tmp.indexOf("=");
                exprNames[i] = tmp.substring(0, eqpos).trim();
                exprExpr[i] = tmp.substring(eqpos + 1).trim();
            }

            int nNodes = (int) inField.getNNodes();
            String[] fieldVariables = listAllFieldVariables(inField, p.get(INPUT_ALIASES), p.get(IGNORE_UNITS));
            String[] variablesInUse = ArrayExpressionParser.listVariablesInUse(fieldVariables, exprExpr);
            Map<String, DataArray> vars = prepareVariables(inField, variablesInUse, p);

            for (int i = 0; i < expressions.size(); i++) {
                if (exprExpr[i] == null || exprNames[i] == null) {
                    continue;
                }

                String name = exprNames[i];
                String expr = exprExpr[i];

                if (inField.getComponent(name) != null) {
                    throw new Exception("ERROR: wrong result variable name!");
                }
                ArrayExpressionParser parser = new ArrayExpressionParser(nNodes, p.get(PRECISION) == Precision.DOUBLE, p.get(IGNORE_UNITS), vars);
                DataArray da = parser.evaluateExpr(expr);
                if (da != null) {
                    if (da.hasParent()) {
                        da = da.cloneShallow();
                    }
                    da.setName(name);
                    out.addComponent(da);
                    vars.put(name, da);
                    if (da.getVectorLength() > 1) {
                        for (int j = 0; j < da.getVectorLength(); j++) {
                            vars.put(name + "." + j, da);
                        }
                    }
                }
            }

            return out;
        }
    }

    private boolean isVariableInUse(String var, String[] variablesInUse)
    {
        for (int i = 0; i < variablesInUse.length; i++) {
            if (variablesInUse[i].equals(var) ||
                (variablesInUse[i].contains(".") && variablesInUse[i].subSequence(0, variablesInUse[i].indexOf(".")).equals(var))) {
                return true;
            }
        }
        return false;
    }

    private Map<String, DataArray> prepareVariables(Field field, String[] variablesInUse, Parameters p)
    {
        Map<String, DataArray> vars = new HashMap<>();
        String alias;
        int veclen;
        for (int i = 0; i < field.getNComponents(); i++) {
            alias = p.get(INPUT_ALIASES)[i];
            veclen = field.getComponent(i).getVectorLength();
            if (isVariableInUse(alias, variablesInUse) && !vars.containsKey(alias)) {
                vars.put(alias, field.getComponent(i).cloneShallow());
                if (veclen > 1) {
                    for (int j = 0; j < veclen; j++) {
                        vars.put(alias + "." + j, vars.get(alias));
                    }
                }

            }
        }

        if (field.hasMask()) {
            if (isVariableInUse("mask", variablesInUse) && !vars.containsKey("mask")) {
                DataArray mask = DataArray.create(field.getCurrentMask(), 1, "mask");
                vars.put("mask", mask);
            }
        }

        if (field instanceof RegularField) {

            //indices
            if (isVariableInUse("index", variablesInUse) && !vars.containsKey("index")) {
                int[] dims = ((RegularField) field).getDims();
                LongLargeArray indices = new LongLargeArray(field.getNNodes() * dims.length);
                for (int i = 0; i < dims.length; i++) {
                    LongLargeArray ind = ((RegularField) field).getIndices(i);
                    for (long j = 0; j < field.getNNodes(); j++) {
                        indices.setLong(dims.length * j + i, ind.getLong(j));
                    }
                }
                vars.put("index", DataArray.create(indices, dims.length, "index"));

            }
            if (isVariableInUse("index.0", variablesInUse) && !vars.containsKey("index.0")) {
                if (vars.containsKey("i"))
                    vars.put("index.0", vars.get("i"));
                else
                    vars.put("index.0", DataArray.create(field.getIndices(0), 1, "index.0"));

            }
            if (isVariableInUse("i", variablesInUse) && !vars.containsKey("i")) {
                if (vars.containsKey("index.0"))
                    vars.put("i", vars.get("index.0"));
                else
                    vars.put("i", DataArray.create(field.getIndices(0), 1, "i"));
            }

            if (isVariableInUse("index.1", variablesInUse) && !vars.containsKey("index.1")) {
                if (vars.containsKey("j"))
                    vars.put("index.1", vars.get("j"));
                else
                    vars.put("index.1", DataArray.create(field.getIndices(1), 1, "index.1"));
            }

            if (isVariableInUse("j", variablesInUse) && !vars.containsKey("j")) {
                if (vars.containsKey("index.1"))
                    vars.put("j", vars.get("index.1"));
                else
                    vars.put("j", DataArray.create(field.getIndices(1), 1, "j"));
            }

            if (isVariableInUse("index.2", variablesInUse) && !vars.containsKey("index.2")) {
                if (vars.containsKey("k"))
                    vars.put("index.2", vars.get("k"));
                else
                    vars.put("index.2", DataArray.create(field.getIndices(2), 1, "index.2"));
            }

            if (isVariableInUse("k", variablesInUse) && !vars.containsKey("k")) {
                if (vars.containsKey("index.2"))
                    vars.put("k", vars.get("index.2"));
                else
                    vars.put("k", DataArray.create(field.getIndices(2), 1, "k"));
            }

            //coords
            if ((isVariableInUse("coords", variablesInUse) ||
                isVariableInUse("coords.0", variablesInUse) ||
                isVariableInUse("coords.1", variablesInUse) ||
                isVariableInUse("coords.2", variablesInUse)) ||
                isVariableInUse("x", variablesInUse) ||
                isVariableInUse("y", variablesInUse) ||
                isVariableInUse("z", variablesInUse)) {
                if (field.getCoords() == null) {
                    float[][] affine = ((RegularField) field).getAffine();
                    long[] dims = ((RegularField) field).getLDims();

                    if (isVariableInUse("coords", variablesInUse) && !vars.containsKey("coords")) {
                        vars.put("coords", DataArray.create(((RegularField) field).getCoordsFromAffine(), 3, "coords", field.getCoordsUnit(0), null));
                    }

                    if (isVariableInUse("coords.0", variablesInUse) && !vars.containsKey("coords.0")) {
                        if (vars.containsKey("x")) {
                            vars.put("coords.0", vars.get("x"));
                        } else {
                            FloatLargeArray coordsx = new FloatLargeArray(field.getNNodes());
                            switch (dims.length) {
                                case 1:
                                    for (long x = 0; x < dims[0]; x++) {
                                        coordsx.setFloat(x, affine[3][0] + x * affine[0][0]);
                                    }
                                    break;
                                case 2:
                                    for (long y = 0, l = 0; y < dims[1]; y++) {
                                        for (long x = 0; x < dims[0]; x++, l++) {
                                            coordsx.setFloat(l, affine[3][0] + x * affine[0][0] + y * affine[1][0]);
                                        }
                                    }
                                    break;
                                case 3:
                                    for (long z = 0, l = 0; z < dims[2]; z++) {
                                        for (long y = 0; y < dims[1]; y++) {
                                            for (long x = 0; x < dims[0]; x++, l++) {
                                                coordsx.setFloat(l, affine[3][0] + x * affine[0][0] + y * affine[1][0] + z * affine[2][0]);
                                            }
                                        }
                                    }
                                    break;
                            }
                            vars.put("coords.0", DataArray.create(coordsx, 1, "coords.0", field.getCoordsUnits()[0], null));
                        }
                    }

                    if (isVariableInUse("x", variablesInUse) && !vars.containsKey("x")) {
                        if (vars.containsKey("coords.0")) {
                            vars.put("x", vars.get("coords.0"));
                        } else {
                            FloatLargeArray coordsx = new FloatLargeArray(field.getNNodes());
                            switch (dims.length) {
                                case 1:
                                    for (long x = 0; x < dims[0]; x++) {
                                        coordsx.setFloat(x, affine[3][0] + x * affine[0][0]);
                                    }
                                    break;
                                case 2:
                                    for (long y = 0, l = 0; y < dims[1]; y++) {
                                        for (long x = 0; x < dims[0]; x++, l++) {
                                            coordsx.setFloat(l, affine[3][0] + x * affine[0][0] + y * affine[1][0]);
                                        }
                                    }
                                    break;
                                case 3:
                                    for (long z = 0, l = 0; z < dims[2]; z++) {
                                        for (long y = 0; y < dims[1]; y++) {
                                            for (long x = 0; x < dims[0]; x++, l++) {
                                                coordsx.setFloat(l, affine[3][0] + x * affine[0][0] + y * affine[1][0] + z * affine[2][0]);
                                            }
                                        }
                                    }
                                    break;
                            }
                            vars.put("x", DataArray.create(coordsx, 1, "x", field.getCoordsUnit(0), null));
                        }
                    }

                    if (isVariableInUse("coords.1", variablesInUse) && !vars.containsKey("coords.1")) {
                        if (vars.containsKey("y")) {
                            vars.put("coords.1", vars.get("y"));
                        } else {
                            FloatLargeArray coordsy = new FloatLargeArray(field.getNNodes());
                            switch (dims.length) {
                                case 1:
                                    for (long x = 0; x < dims[0]; x++) {
                                        coordsy.setFloat(x, affine[3][1] + x * affine[0][1]);
                                    }
                                    break;
                                case 2:
                                    for (long y = 0, l = 0; y < dims[1]; y++) {
                                        for (long x = 0; x < dims[0]; x++, l++) {
                                            coordsy.setFloat(l, affine[3][1] + x * affine[0][1] + y * affine[1][1]);
                                        }
                                    }
                                    break;
                                case 3:
                                    for (long z = 0, l = 0; z < dims[2]; z++) {
                                        for (long y = 0; y < dims[1]; y++) {
                                            for (long x = 0; x < dims[0]; x++, l++) {
                                                coordsy.set(l, affine[3][1] + x * affine[0][1] + y * affine[1][1] + z * affine[2][1]);
                                            }
                                        }
                                    }
                                    break;
                            }
                            vars.put("coords.1", DataArray.create(coordsy, 1, "coords.1", field.getCoordsUnit(1), null));
                        }
                    }

                    if (isVariableInUse("y", variablesInUse) && !vars.containsKey("y")) {
                        if (vars.containsKey("coords.1")) {
                            vars.put("y", vars.get("coords.1"));
                        } else {
                            FloatLargeArray coordsy = new FloatLargeArray(field.getNNodes());
                            switch (dims.length) {
                                case 1:
                                    for (long x = 0; x < dims[0]; x++) {
                                        coordsy.setFloat(x, affine[3][1] + x * affine[0][1]);
                                    }
                                    break;
                                case 2:
                                    for (long y = 0, l = 0; y < dims[1]; y++) {
                                        for (long x = 0; x < dims[0]; x++, l++) {
                                            coordsy.setFloat(l, affine[3][1] + x * affine[0][1] + y * affine[1][1]);
                                        }
                                    }
                                    break;
                                case 3:
                                    for (long z = 0, l = 0; z < dims[2]; z++) {
                                        for (long y = 0; y < dims[1]; y++) {
                                            for (long x = 0; x < dims[0]; x++, l++) {
                                                coordsy.setFloat(l, affine[3][1] + x * affine[0][1] + y * affine[1][1] + z * affine[2][1]);
                                            }
                                        }
                                    }
                                    break;
                            }
                            vars.put("y", DataArray.create(coordsy, 1, "y", field.getCoordsUnit(1), null));
                        }
                    }

                    if (isVariableInUse("coords.2", variablesInUse) && !vars.containsKey("coords.2")) {
                        if (vars.containsKey("z")) {
                            vars.put("coords.2", vars.get("z"));
                        } else {
                            FloatLargeArray coordsz = new FloatLargeArray(field.getNNodes());
                            switch (dims.length) {
                                case 1:
                                    for (long x = 0; x < dims[0]; x++) {
                                        coordsz.setFloat(x, affine[3][2] + x * affine[0][2]);
                                    }
                                    break;
                                case 2:
                                    for (long y = 0, l = 0; y < dims[1]; y++) {
                                        for (long x = 0; x < dims[0]; x++, l++) {
                                            coordsz.setFloat(l, affine[3][2] + x * affine[0][2] + y * affine[1][2]);
                                        }
                                    }
                                    break;
                                case 3:
                                    for (long z = 0, l = 0; z < dims[2]; z++) {
                                        for (long y = 0; y < dims[1]; y++) {
                                            for (long x = 0; x < dims[0]; x++, l++) {
                                                coordsz.setFloat(l, affine[3][2] + x * affine[0][2] + y * affine[1][2] + z * affine[2][2]);
                                            }
                                        }
                                    }
                                    break;
                            }
                            vars.put("coords.2", DataArray.create(coordsz, 1, "coords.2", field.getCoordsUnit(2), null));
                        }
                    }

                    if (isVariableInUse("z", variablesInUse) && !vars.containsKey("z")) {
                        if (vars.containsKey("coords.2")) {
                            vars.put("z", vars.get("coords.2"));
                        } else {
                            FloatLargeArray coordsz = new FloatLargeArray(field.getNNodes());
                            switch (dims.length) {
                                case 1:
                                    for (long x = 0; x < dims[0]; x++) {
                                        coordsz.setFloat(x, affine[3][2] + x * affine[0][2]);
                                    }
                                    break;
                                case 2:
                                    for (long y = 0, l = 0; y < dims[1]; y++) {
                                        for (long x = 0; x < dims[0]; x++, l++) {
                                            coordsz.setFloat(l, affine[3][2] + x * affine[0][2] + y * affine[1][2]);
                                        }
                                    }
                                    break;
                                case 3:
                                    for (long z = 0, l = 0; z < dims[2]; z++) {
                                        for (long y = 0; y < dims[1]; y++) {
                                            for (long x = 0; x < dims[0]; x++, l++) {
                                                coordsz.setFloat(l, affine[3][2] + x * affine[0][2] + y * affine[1][2] + z * affine[2][2]);
                                            }
                                        }
                                    }
                                    break;
                            }
                            vars.put("z", DataArray.create(coordsz, 1, "z", field.getCoordsUnit(2), null));
                        }
                    }

                } else {
                    if (isVariableInUse("coords", variablesInUse) && !vars.containsKey("coords"))
                        vars.put("coords", DataArray.create(((RegularField) field).getCoords(), 3, "coords"));
                    if (isVariableInUse("coords.0", variablesInUse) && !vars.containsKey("coords.0"))
                        vars.put("coords.0", DataArray.create(field.getCoords(), 3, "coords.0"));
                    if (isVariableInUse("x", variablesInUse) && !vars.containsKey("x"))
                        vars.put("x", DataArray.create(field.getCoords(), 3, "x"));
                    if (isVariableInUse("coords.1", variablesInUse) && !vars.containsKey("coords.1"))
                        vars.put("coords.1", DataArray.create(field.getCoords(), 3, "coords.1"));
                    if (isVariableInUse("y", variablesInUse) && !vars.containsKey("y"))
                        vars.put("y", DataArray.create(field.getCoords(), 3, "y"));
                    if (isVariableInUse("coords.2", variablesInUse) && !vars.containsKey("coords.2"))
                        vars.put("coords.2", DataArray.create(field.getCoords(), 3, "coords.2"));
                    if (isVariableInUse("z", variablesInUse) && !vars.containsKey("z"))
                        vars.put("z", DataArray.create(field.getCoords(), 3, "z"));
                }
            }

        } //end regular field

        if (field instanceof IrregularField) {
            if (isVariableInUse("index", variablesInUse) && !vars.containsKey("index")) {
                if (vars.containsKey("i"))
                    vars.put("index", vars.get("i"));
                else
                    vars.put("index", DataArray.create(field.getIndices(0), 1, "index"));
            }

            if (isVariableInUse("i", variablesInUse) && !vars.containsKey("i")) {
                if (vars.containsKey("index"))
                    vars.put("i", vars.get("index"));
                else
                    vars.put("i", DataArray.create(field.getIndices(0), 1, "i"));
            }
            
            //coords
            if (isVariableInUse("coords", variablesInUse) && !vars.containsKey("coords")) {
                vars.put("coords", DataArray.create(field.getCoords(), 3, "coords"));
            }

            if (isVariableInUse("coords.0", variablesInUse) && !vars.containsKey("coords.0")) {
                vars.put("coords.0", DataArray.create(field.getCoords(), 3, "coords.0"));
            }

            if (isVariableInUse("x", variablesInUse) && !vars.containsKey("x")) {
                vars.put("x", DataArray.create(field.getCoords(), 3, "x"));
            }

            if (isVariableInUse("coords.1", variablesInUse) && !vars.containsKey("coords.1")) {
                vars.put("coords.1", DataArray.create(field.getCoords(), 3, "coords.1"));
            }

            if (isVariableInUse("y", variablesInUse) && !vars.containsKey("y")) {
                vars.put("y", DataArray.create(field.getCoords(), 3, "y"));
            }

            if (isVariableInUse("coords.2", variablesInUse) && !vars.containsKey("coords.2")) {
                vars.put("coords.2", DataArray.create(field.getCoords(), 3, "coords.2"));
            }

            if (isVariableInUse("z", variablesInUse) && !vars.containsKey("z")) {
                vars.put("z", DataArray.create(field.getCoords(), 3, "z"));
            }
        } //end irregular field

        return vars;
    }

    private static String[] listAllFieldVariables(Field field, String[] aliases, boolean ignoreUnits)
    {
        if (field == null)
            return null;

        ArrayList<String> list = new ArrayList<String>();
        if (field instanceof RegularField) {
            switch (((RegularField) field).getDims().length) {
                case 3:
                    list.add("index.2");
                    list.add("k");
                case 2:
                    list.add("index.1");
                    list.add("j");
                case 1:
                    list.add("index.0");
                    list.add("i");
                    list.add("index");
            }
        } else if (field instanceof IrregularField) {
            list.add("index");
            list.add("i");
        }

        if (ignoreUnits || field.hasCoordsUnitsEqual()) {
            list.add("coords.2");
            list.add("z");
            list.add("coords.1");
            list.add("y");
            list.add("coords.0");
            list.add("x");
            list.add("coords");
        } else {
            list.add("z");
            list.add("y");
            list.add("x");
        }
        if (field.hasMask()) {
            list.add("mask");
        }

        for (int i = 0; i < field.getNComponents(); i++) {
            int veclen = field.getComponent(i).getVectorLength();
            String alias = aliases[i];
            list.add(alias);
            switch (veclen) {
                case 3:
                    list.add(alias + ".2");
                case 2:
                    list.add(alias + ".1");
                    list.add(alias + ".0");
            }
        }

        String[] out = new String[list.size()];
        for (int i = 0; i < out.length; i++) {
            out[i] = list.get(i);
        }
        return out;
    }
}
