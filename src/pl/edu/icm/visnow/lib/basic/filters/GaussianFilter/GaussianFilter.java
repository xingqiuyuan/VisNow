///<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.GaussianFilter;

import static java.lang.Math.*;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.utils.VectorMath;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;
import static pl.edu.icm.visnow.lib.basic.filters.GaussianFilter.GaussianFilterShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.field.DirectionalFieldSmoothing;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class GaussianFilter extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected GUI computeUI = null;
    protected RegularField inField = null;
    protected int radius, radius1;
    private int runQueue = 0;
    protected FloatValueModificationListener presmoothingProgressListener = new FloatValueModificationListener()
    {

        public void floatValueChanged(FloatValueModificationEvent e)
        {
            setProgress(e.getVal());
        }
    };

    public GaussianFilter()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();

            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SIGMA, 1.5f),
            new Parameter<>(SIGMA1, 1f),
            new Parameter<>(HIGH_PASS, false),
            new Parameter<>(HIGH_ABS, false),
            new Parameter<>(BAND_PASS, false),
            new Parameter<>(BAND_ABS, false),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    public void update(Parameters p)
    {
        float[] outData = null;
        float[] outData1 = null;
        float[] kernel;
        float[] kernel1 = null;
        float[] kernelDistribution;
        float[] kernel1Distribution = null;
        int[] dims = inField.getDims();

        radius = (int) Math.ceil(3 * p.get(SIGMA));
        radius = Math.min(VectorMath.vectorMin(dims) - 1, radius * 2) / 2;
        outRegularField = new RegularField(dims);
        outRegularField.setAffine(inField.getAffine());
        if (inField.getCoords() != null)
            outRegularField.setCoords(inField.getCoords());
        kernel = new float[2 * radius + 1];
        float r = 1.0f / (2.0f * p.get(SIGMA) * p.get(SIGMA));
        for (int i = 0; i <= radius; i++) kernel[radius + i] = kernel[radius - i] = (float) exp(-i * i * r);
        VectorMath.vectorMultiplyScalar(kernel, 1.0f / VectorMath.vectorSum(kernel), false);
        kernelDistribution = VectorMath.vectorCumulativeSum(kernel, false);

        if (p.get(BAND_PASS)) {
            radius1 = Math.max(1, (int) (3 * p.get(SIGMA1)));
            radius1 = Math.min(VectorMath.vectorMin(dims) - 1, radius1 * 2) / 2;
            kernel1 = new float[2 * radius1 + 1];
            r = 1.0f / (2.0f * p.get(SIGMA1) * p.get(SIGMA1));
            for (int i = 0; i <= radius1; i++)
                kernel1[radius1 + i] = kernel1[radius1 - i] = (float) exp(-i * i * r);
            VectorMath.vectorMultiplyScalar(kernel1, 1.0f / VectorMath.vectorSum(kernel1), false);
            kernel1Distribution = VectorMath.vectorCumulativeSum(kernel1, false);
        }

        for (int component = 0; component < inField.getNComponents(); component++) {
            if (inField.getComponent(component).getVectorLength() == 1) {
                outData = inField.getComponent(component).getRawFloatArray().getData();
                for (int direction = 0; direction < dims.length; direction++) {
                    int nThreads = VisNow.availableProcessors();
                    Thread[] workThreads = new Thread[nThreads];
                    DirectionalFieldSmoothing[] smoothers = new DirectionalFieldSmoothing[nThreads];
                    for (int i = 0; i < workThreads.length; i++) {
                        smoothers[i] = new DirectionalFieldSmoothing(direction, inField.getDims(), outData, kernel, kernelDistribution, nThreads, i);
                        if (i == 0)
                            smoothers[i].addFloatValueModificationListener(presmoothingProgressListener);
                        workThreads[i] = new Thread(smoothers[i]);
                        workThreads[i].start();
                    }
                    for (Thread workThread : workThreads)
                        try {
                            workThread.join();
                        } catch (Exception e) {
                        }
                }
                if (p.get(BAND_PASS)) {
                    outData1 = inField.getComponent(component).getRawFloatArray().getData();
                    for (int direction = 0; direction < dims.length; direction++) {
                        int nThreads = VisNow.availableProcessors();
                        Thread[] workThreads = new Thread[nThreads];
                        DirectionalFieldSmoothing[] smoothers = new DirectionalFieldSmoothing[nThreads];
                        for (int i = 0; i < workThreads.length; i++) {
                            smoothers[i] = new DirectionalFieldSmoothing(direction, inField.getDims(), outData1, kernel1, kernel1Distribution, nThreads, i);
                            if (i == 0)
                                smoothers[i].addFloatValueModificationListener(presmoothingProgressListener);
                            workThreads[i] = new Thread(smoothers[i]);
                            workThreads[i].start();
                        }
                        for (int i = 0; i < workThreads.length; i++)
                            try {
                                workThreads[i].join();
                            } catch (Exception e) {
                            }
                    }
                }

                switch (inField.getComponent(component).getType()) {
                    case FIELD_DATA_BYTE:
                        UnsignedByteLargeArray outBData = new UnsignedByteLargeArray(outData.length);
                        for (int i = 0; i < outData.length; i++)
                            outBData.setByte(i, (byte) (0xff & (int) outData[i]));
                        outRegularField.addComponent(DataArray.create(outBData, inField.getComponent(component).getVectorLength(), "smoothed " + inField.getComponent(component).getName()));
                        if (p.get(HIGH_PASS)) {
                            UnsignedByteLargeArray inBData = (UnsignedByteLargeArray)inField.getComponent(component).getRawArray();
                            ShortLargeArray outSHData = new ShortLargeArray(outData.length);
                            if (p.get(HIGH_ABS))
                                for (long i = 0; i < outData.length; i++)
                                    outSHData.setShort(i, (short) abs((0xff & inBData.getByte(i)) - (0xff & outBData.getByte(i))));
                            else
                                for (int i = 0; i < outData.length; i++)
                                    outSHData.setShort(i, (short) ((0xff & inBData.getByte(i)) - (0xff & outBData.getByte(i))));
                            outRegularField.addComponent(DataArray.create(outSHData, inField.getComponent(component).getVectorLength(), "hi_pass " + inField.getComponent(component).getName()));
                        }
                        if (p.get(BAND_PASS)) {
                            short[] outSBData = new short[outData.length];
                            if (p.get(BAND_ABS))
                                for (int i = 0; i < outData.length; i++)
                                    outSBData[i] = (short) abs(outData1[i] - outData[i]);
                            else
                                for (int i = 0; i < outData.length; i++)
                                    outSBData[i] = (short) (outData1[i] - outData[i]);
                            outRegularField.addComponent(DataArray.create(outSBData, inField.getComponent(component).getVectorLength(), "band_pass " + inField.getComponent(component).getName()));
                        }
                        break;
                    case FIELD_DATA_SHORT:
                    case FIELD_DATA_INT:
                    case FIELD_DATA_FLOAT:
                    case FIELD_DATA_DOUBLE:
                        outRegularField.addComponent(DataArray.create(outData, inField.getComponent(component).getVectorLength(), "smoothed " + inField.getComponent(component).getName()));
                        LargeArray inDData = inField.getComponent(component).getRawArray();
                        if (p.get(HIGH_PASS)) {
                            LargeArray outFHData = LargeArrayUtils.create(inDData.getType(), outData.length, false);
                            if (p.get(HIGH_ABS))
                                for (int i = 0; i < outData.length; i++)
                                    outFHData.setDouble(i, abs(inDData.getDouble(i) - outData[i]));
                            else
                                for (int i = 0; i < outData.length; i++)
                                    outFHData.setDouble(i, (inDData.getDouble(i) - outData[i]));
                            outRegularField.addComponent(DataArray.create(outFHData, inField.getComponent(component).getVectorLength(), "hi_pass " + inField.getComponent(component).getName()));
                        }
                        if (p.get(BAND_PASS)) {
                            LargeArray outFBData = LargeArrayUtils.create(inDData.getType(), outData.length, false);
                            if (p.get(BAND_ABS))
                                for (int i = 0; i < outData.length; i++)
                                    outFBData.setDouble(i, abs(outData1[i] - outData[i]));
                            else
                                for (int i = 0; i < outData.length; i++)
                                    outFBData.setDouble(i, outData1[i] - outData[i]);
                            outRegularField.addComponent(DataArray.create(outFBData, inField.getComponent(component).getVectorLength(), "hi_pass " + inField.getComponent(component).getName()));
                        }
                        break;
                }
            }
        }
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    //TODO: should work for 1d as well, but apparently doesn't
    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getTimestamp() != newField.getTimestamp());
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p = parameters.getReadOnlyClone();

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); //can be run (-> decreased) in run dynamically mode on input attach or new inField data
                update(p);
                if (outRegularField == null) {
                    setOutputValue("outField", null);
                } else {
                    setOutputValue("outField", new VNRegularField(outRegularField));
                }
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
