/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.filters.ComponentCalculator;

import java.awt.BorderLayout;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.engine.core.ParameterProxy;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.gui.widgets.RunButton;
import static pl.edu.icm.visnow.lib.basic.filters.ComponentCalculator.ComponentCalculatorShared.*;
import pl.edu.icm.visnow.lib.utils.StringUtils;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.swing.UIStyle;

/**
 *
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class GUI extends javax.swing.JPanel
{
    private static final Logger LOGGER = Logger.getLogger(GUI.class);

    private ParameterProxy parameterProxy;

    //unfortunatelly this flag is necessary to distinguish user action on jTable from setter actions.
    private boolean tableActive = true;
    //this is necessary to avoid firing textArea event after a delay (when parameters are active again)
    private boolean textAreaActive = true;

    //arrays for restore user values (in generator mode, when dim num changed)
    int[] generatorFullDims = new int[]{2, 2, 2};
    float[][] generatorFullExtents = new float[][]{{-0.5f, -0.5f, -0.5f}, {0.5f, 0.5f, 0.5f}};

    /**
     * Creates new form GUI
     */
    public GUI()
    {
        initComponents();

        generatorTablePanel.add(generatorTable.getTableHeader(), BorderLayout.NORTH);
        generatorTable.getTableHeader().setReorderingAllowed(false);
        componentTable.getTableHeader().setReorderingAllowed(false);

        componentTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        componentTable.getColumnModel().getColumn(1).setPreferredWidth(40);
        componentTable.getColumnModel().getColumn(2).setPreferredWidth(60);

        initControlListeners();
    }

    private void initControlListeners()
    {
        generatorTable.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (tableActive) {
                    int dimNum = generatorTable.getRowCount();
                    int dims[] = new int[dimNum];
                    float[][] extents = new float[2][dimNum];
                    for (int i = 0; i < dimNum; i++) {
                        if ((Integer) generatorTable.getValueAt(i, 1) < 2) generatorTable.setValueAt(2, i, 1);
                        dims[i] = (Integer) generatorTable.getValueAt(i, 1);
                        extents[0][i] = (Float) generatorTable.getValueAt(i, 2);
                        extents[1][i] = (Float) generatorTable.getValueAt(i, 3);
                        if (extents[1][i] <= extents[0][i]) extents[1][i] = extents[0][i] + Math.ulp(extents[0][i]);
                    }

                    updateGeneratorInternalArrays(dims, extents);

                    parameterProxy.set(GENERATOR_DIMENSION_LENGTHS, dims,
                                   GENERATOR_EXTENTS, extents);
                }
            }
        });

        componentTable.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                if (tableActive) {
                    String[] aliases = new String[componentTable.getRowCount()];
                    for (int i = 0; i < componentTable.getRowCount(); i++)
                        aliases[i] = (String) componentTable.getValueAt(i, 2);

                    parameterProxy.set(INPUT_ALIASES, aliases);
                }
            }
        });

        expressionsTA.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void insertUpdate(DocumentEvent e)
            {
                processEvent();
            }

            @Override
            public void removeUpdate(DocumentEvent e)
            {
                processEvent();
            }

            @Override
            public void changedUpdate(DocumentEvent e)
            {
                processEvent();
            }

            private long lastModifyTime = 0;
            private SwingWorker worker = null;

            synchronized private void processEvent()
            {
                if (textAreaActive) {
                    lastModifyTime = System.currentTimeMillis();

                    if (worker == null) {
                        worker = new SwingWorker()
                        {
                            @Override
                            protected Object doInBackground()
                            {
                                try {
                                    while (System.currentTimeMillis() - lastModifyTime < 1000) Thread.sleep(10);
                                } catch (InterruptedException ex) {
                                    LOGGER.warn(ex);
                                }
                                return null;
                            }

                            @Override
                            protected void done()
                            {
                                worker = null;
                                parameterProxy.set(EXPRESSION_LINES, expressionsTA.getText().split("\\n", -1));
                            }

                        };
                        worker.execute();
                    }
                }
            }
        });
    }

    public void setParameterProxy(ParameterProxy targetParameterProxy)
    {
        //putting run button between logic and gui
        parameterProxy = runButton.getPlainProxy(targetParameterProxy, RUNNING_MESSAGE);
    }

    public void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {
        LOGGER.debug("");

        generatorPanel.setVisible(p.get(META_IS_GENERATOR));
        calculatorPanel.setVisible(!p.get(META_IS_GENERATOR));

        String expressionLines = StringUtils.join(p.get(EXPRESSION_LINES), System.lineSeparator());

        textAreaActive = false;
        if (!expressionsTA.getText().equals(expressionLines)) expressionsTA.setText(expressionLines);
        textAreaActive = true;

        singlePrecisionRB.setSelected(p.get(PRECISION) == Precision.SINGLE);
        doublePrecisionRB.setSelected(p.get(PRECISION) == Precision.DOUBLE);

        dim1RB.setSelected(p.get(GENERATOR_DIMENSION_LENGTHS).length == 1);
        dim2RB.setSelected(p.get(GENERATOR_DIMENSION_LENGTHS).length == 2);
        dim3RB.setSelected(p.get(GENERATOR_DIMENSION_LENGTHS).length == 3);

        retainCB.setSelected(p.get(RETAIN));

        updateGeneratorInternalArrays(p.get(GENERATOR_DIMENSION_LENGTHS), p.get(GENERATOR_EXTENTS));

        updateGeneratorTable(p.get(GENERATOR_DIMENSION_LENGTHS), p.get(GENERATOR_EXTENTS));

        updateComponentTable(p.get(META_COMPONENT_NAMES), p.get(META_COMPONENT_VECLEN), p.get(INPUT_ALIASES));

        runButton.updateAutoState(p.get(RUNNING_MESSAGE));
        runButton.updatePendingState(setRunButtonPending);
    }

    private void updateGeneratorInternalArrays(int[] generatorDims, float[][] generatorExtents)
    {
        System.arraycopy(generatorDims, 0, generatorFullDims, 0, generatorDims.length);
        System.arraycopy(generatorExtents[0], 0, generatorFullExtents[0], 0, generatorExtents[0].length);
        System.arraycopy(generatorExtents[1], 0, generatorFullExtents[1], 0, generatorExtents[1].length);
    }

    private void updateGeneratorTable(int[] generatorDims, float[][] generatorExtents)
    {
        tableActive = false;
        DefaultTableModel generatorTableModel = (DefaultTableModel) generatorTable.getModel();

        while (generatorTableModel.getRowCount() > 0) generatorTableModel.removeRow(0);

        for (int i = 0; i < generatorDims.length; i++)
            generatorTableModel.addRow(new Object[]{new String[]{"x", "y", "z"}[i],
                                                    generatorDims[i],
                                                    generatorExtents[0][i],
                                                    generatorExtents[1][i]});
        tableActive = true;
    }

    private void updateComponentTable(String[] componentNames, int[] componentVecLens, String[] componentAliases)
    {
        tableActive = false;
        DefaultTableModel componentTableModel = (DefaultTableModel) componentTable.getModel();

        while (componentTableModel.getRowCount() > 0) componentTableModel.removeRow(0);

        for (int i = 0; i < componentNames.length; i++)
            componentTableModel.addRow(new Object[]{componentNames[i], componentVecLens[i], componentAliases[i]});

        tableActive = true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        precisionRBG = new javax.swing.ButtonGroup();
        generatorRBG = new javax.swing.ButtonGroup();
        generatorPanel = new javax.swing.JPanel();
        generatorLabel = new javax.swing.JLabel();
        dimPanel = new javax.swing.JPanel();
        dim1RB = new pl.edu.icm.visnow.gui.swingwrappers.RadioButton();
        dim2RB = new pl.edu.icm.visnow.gui.swingwrappers.RadioButton();
        dim3RB = new pl.edu.icm.visnow.gui.swingwrappers.RadioButton();
        generatorTablePanel = new javax.swing.JPanel();
        generatorTable = new javax.swing.JTable();
        calculatorPanel = new javax.swing.JPanel();
        calculatorLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        componentTable = new javax.swing.JTable();
        retainCB = new javax.swing.JCheckBox();
        exprPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        expressionsTA = new javax.swing.JTextArea();
        precisionPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        singlePrecisionRB = new pl.edu.icm.visnow.gui.swingwrappers.RadioButton();
        doublePrecisionRB = new pl.edu.icm.visnow.gui.swingwrappers.RadioButton();
        ignoreUnitsCB = new javax.swing.JCheckBox();
        runButton = new pl.edu.icm.visnow.gui.widgets.RunButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        setLayout(new java.awt.GridBagLayout());

        generatorPanel.setLayout(new java.awt.GridBagLayout());

        generatorLabel.setText("Generate field:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        generatorPanel.add(generatorLabel, gridBagConstraints);

        dimPanel.setLayout(new java.awt.GridBagLayout());

        generatorRBG.add(dim1RB);
        dim1RB.setText("1D");
        dim1RB.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                generatorDimRBUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        dimPanel.add(dim1RB, gridBagConstraints);

        generatorRBG.add(dim2RB);
        dim2RB.setSelected(true);
        dim2RB.setText("2D");
        dim2RB.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                generatorDimRBUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        dimPanel.add(dim2RB, gridBagConstraints);

        generatorRBG.add(dim3RB);
        dim3RB.setText("3D");
        dim3RB.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                generatorDimRBUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        dimPanel.add(dim3RB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        generatorPanel.add(dimPanel, gridBagConstraints);

        generatorTablePanel.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 0, 0, javax.swing.UIManager.getDefaults().getColor("ScrollBar.darkShadow")));
        generatorTablePanel.setLayout(new java.awt.BorderLayout());

        generatorTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null,  new Integer(50),  new Float(-0.5),  new Float(0.5)}
            },
            new String []
            {
                "dim", "size", "min", "max"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean []
            {
                false, true, true, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        generatorTable.setRowSelectionAllowed(false);
        generatorTablePanel.add(generatorTable, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        generatorPanel.add(generatorTablePanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        add(generatorPanel, gridBagConstraints);

        calculatorPanel.setPreferredSize(new java.awt.Dimension(50, 50));
        calculatorPanel.setLayout(new java.awt.GridBagLayout());

        calculatorLabel.setText("Input field components:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        calculatorPanel.add(calculatorLabel, gridBagConstraints);

        componentTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Name", "Veclen", "Variable"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        componentTable.setRowSelectionAllowed(false);
        jScrollPane1.setViewportView(componentTable);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        calculatorPanel.add(jScrollPane1, gridBagConstraints);

        retainCB.setText("retain input components");
        retainCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                retainCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        calculatorPanel.add(retainCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(calculatorPanel, gridBagConstraints);

        exprPanel.setPreferredSize(new java.awt.Dimension(50, 50));
        exprPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Expressions:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 4, 0);
        exprPanel.add(jLabel1, gridBagConstraints);

        expressionsTA.setText("result = expr");
        jScrollPane3.setViewportView(expressionsTA);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        exprPanel.add(jScrollPane3, gridBagConstraints);

        precisionPanel.setLayout(new java.awt.GridBagLayout());

        jLabel3.setText("Precision:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        precisionPanel.add(jLabel3, gridBagConstraints);

        precisionRBG.add(singlePrecisionRB);
        singlePrecisionRB.setSelected(true);
        singlePrecisionRB.setText("single");
        singlePrecisionRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                singlePrecisionRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        precisionPanel.add(singlePrecisionRB, gridBagConstraints);

        precisionRBG.add(doublePrecisionRB);
        doublePrecisionRB.setText("double");
        doublePrecisionRB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                doublePrecisionRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        precisionPanel.add(doublePrecisionRB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        exprPanel.add(precisionPanel, gridBagConstraints);

        ignoreUnitsCB.setSelected(true);
        ignoreUnitsCB.setText("ignore units");
        ignoreUnitsCB.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                ignoreUnitsCBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        exprPanel.add(ignoreUnitsCB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(8, 0, 0, 0);
        add(exprPanel, gridBagConstraints);

        runButton.addUserActionListener(new pl.edu.icm.visnow.gui.swingwrappers.UserActionListener()
        {
            public void userAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
            }
            public void userChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)
            {
                runButtonUserChangeAction(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        add(runButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        add(filler1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    private void singlePrecisionRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_singlePrecisionRBActionPerformed
        parameterProxy.set(PRECISION, Precision.SINGLE);
    }//GEN-LAST:event_singlePrecisionRBActionPerformed

    private void doublePrecisionRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doublePrecisionRBActionPerformed
        parameterProxy.set(PRECISION, Precision.DOUBLE);
    }//GEN-LAST:event_doublePrecisionRBActionPerformed

    private void retainCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retainCBActionPerformed
        parameterProxy.set(RETAIN, retainCB.isSelected());
    }//GEN-LAST:event_retainCBActionPerformed

    private void runButtonUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_runButtonUserChangeAction
    {//GEN-HEADEREND:event_runButtonUserChangeAction
        parameterProxy.set(RUNNING_MESSAGE, (RunButton.RunState) evt.getEventData());
    }//GEN-LAST:event_runButtonUserChangeAction

    private void generatorDimRBUserChangeAction(pl.edu.icm.visnow.gui.swingwrappers.UserEvent evt)//GEN-FIRST:event_generatorDimRBUserChangeAction
    {//GEN-HEADEREND:event_generatorDimRBUserChangeAction
        int dimNum = dim1RB.isSelected() ? 1 : dim2RB.isSelected() ? 2 : 3;

        int[] dims = Arrays.copyOf(generatorFullDims, dimNum);
        float[][] extents = new float[][]{Arrays.copyOf(generatorFullExtents[0], dimNum), Arrays.copyOf(generatorFullExtents[1], dimNum)};

        updateGeneratorTable(dims, extents);

        parameterProxy.set(GENERATOR_DIMENSION_LENGTHS, dims,
                       GENERATOR_EXTENTS, extents);
    }//GEN-LAST:event_generatorDimRBUserChangeAction

    private void ignoreUnitsCBActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_ignoreUnitsCBActionPerformed
    {//GEN-HEADEREND:event_ignoreUnitsCBActionPerformed
         parameterProxy.set(IGNORE_UNITS, ignoreUnitsCB.isSelected());
    }//GEN-LAST:event_ignoreUnitsCBActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel calculatorLabel;
    private javax.swing.JPanel calculatorPanel;
    private javax.swing.JTable componentTable;
    private pl.edu.icm.visnow.gui.swingwrappers.RadioButton dim1RB;
    private pl.edu.icm.visnow.gui.swingwrappers.RadioButton dim2RB;
    private pl.edu.icm.visnow.gui.swingwrappers.RadioButton dim3RB;
    private javax.swing.JPanel dimPanel;
    private pl.edu.icm.visnow.gui.swingwrappers.RadioButton doublePrecisionRB;
    private javax.swing.JPanel exprPanel;
    private javax.swing.JTextArea expressionsTA;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel generatorLabel;
    private javax.swing.JPanel generatorPanel;
    private javax.swing.ButtonGroup generatorRBG;
    private javax.swing.JTable generatorTable;
    private javax.swing.JPanel generatorTablePanel;
    private javax.swing.JCheckBox ignoreUnitsCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel precisionPanel;
    private javax.swing.ButtonGroup precisionRBG;
    private javax.swing.JCheckBox retainCB;
    private pl.edu.icm.visnow.gui.widgets.RunButton runButton;
    private pl.edu.icm.visnow.gui.swingwrappers.RadioButton singlePrecisionRB;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        UIStyle.initStyle();

        SwingUtilities.invokeLater(new Runnable()
        {

            @Override
            public void run()
            {
                JFrame f = new JFrame();
                f.add(new GUI());
                f.setSize(260, 500);
                f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                f.setLocationRelativeTo(null);
                f.setVisible(true);
            }
        });
    }
}
