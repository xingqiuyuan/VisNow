//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.ClearMaskedOutNodes;


import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class ClearMaskedOutNodes extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    public ClearMaskedOutNodes()
    {
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null)
            return;
        IrregularField inField = ((VNIrregularField)getInputFirstValue("inField")).getField();
        if (!inField.hasMask()) 
             outIrregularField = inField;
        else {
            int nNodes = (int)inField.getNNodes();
            int nOutNodes = 0;
            int[] reindex = new int[nNodes];
            byte[] mask = inField.getCurrentMask().getData();
            for (int i = 0; i < mask.length; i++) {
                if (mask[i] == 0)
                    reindex[i] = -1;
                else {
                    reindex[i] = nOutNodes;
                    nOutNodes += 1;
                }
            }
            outIrregularField = new IrregularField(nOutNodes);
            float[] coords = inField.getCurrentCoords().getData();
            float[] outCoords = new float[3 * nOutNodes];
            for (int i = 0; i < nNodes; i++) 
                if (reindex[i] >= 0)
                    System.arraycopy(coords, 3 * i, outCoords, 3 * reindex[i], 3);
            outIrregularField.setCurrentCoords(new FloatLargeArray(outCoords));
            for (DataArray inDa : inField.getComponents()) {
                int vlen = inDa.getVectorLength();
                switch (inDa.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] inBD = (byte[])inDa.getRawArray().getData();
                    byte[] outBD = new byte[vlen * nOutNodes];
                    for (int i = 0; i < nNodes; i++) 
                        if (reindex[i] >= 0)
                            System.arraycopy(inBD, vlen * i, outBD, vlen * reindex[i], vlen);
                    outIrregularField.addComponent(DataArray.create(new UnsignedByteLargeArray(outBD), vlen, inDa.getName()));
                    break;
                case FIELD_DATA_SHORT:
                    short[] inSD = (short[])inDa.getRawArray().getData();
                    short[] outSD = new short[vlen * nOutNodes];
                    for (int i = 0; i < nNodes; i++) 
                        if (reindex[i] >= 0)
                            System.arraycopy(inSD, vlen * i, outSD, vlen * reindex[i], vlen);
                    outIrregularField.addComponent(DataArray.create(new ShortLargeArray(outSD), vlen, inDa.getName()));
                    break;
                case FIELD_DATA_INT:
                    int[] inID = (int[])inDa.getRawArray().getData();
                    int[] outID = new int[vlen * nOutNodes];
                    for (int i = 0; i < nNodes; i++) 
                        if (reindex[i] >= 0)
                            System.arraycopy(inID, vlen * i, outID, vlen * reindex[i], vlen);
                    outIrregularField.addComponent(DataArray.create(new IntLargeArray(outID), vlen, inDa.getName()));
                    break;
                case FIELD_DATA_FLOAT:
                    float[] inFD = (float[])inDa.getRawArray().getData();
                    float[] outFD = new float[vlen * nOutNodes];
                    for (int i = 0; i < nNodes; i++) 
                        if (reindex[i] >= 0)
                            System.arraycopy(inFD, vlen * i, outFD, vlen * reindex[i], vlen);
                    outIrregularField.addComponent(DataArray.create(new FloatLargeArray(outFD), vlen, inDa.getName()));
                    break;
                }
            }
            for (CellSet cs : inField.getCellSets()) {
                CellSet outCs = new CellSet(cs.getName());
                for (CellArray ca : cs.getCellArrays())
                    if (ca != null) {
                        int cellNNodes = ca.getNCellNodes();
                        int nCells = ca.getNCells();
                        int[] nodes = ca.getNodes();
                        byte[] orient = ca.getOrientations();
                        int nOutCells = 0;
                        for (int i = 0; i < nCells; i++) {
                            boolean valid = true;
                            for (int j = 0; j < cellNNodes; j++)
                                if (mask[nodes[cellNNodes * i + j]] == 0)
                                    valid = false;
                            if (valid)
                                nOutCells += 1;
                        }
                        int[] outNodes = new int[cellNNodes * nOutCells];
                        byte[] outOrient = new byte[nOutCells];
                        for (int i = 0, out = 0; i < nCells; i++) {
                            boolean valid = true;
                            for (int j = 0; j < cellNNodes; j++)
                                if (mask[nodes[cellNNodes * i + j]] == 0)
                                    valid = false;
                            if (valid) {
                                for (int j = 0; j < cellNNodes; j++)
                                    outNodes[cellNNodes * out + j] = reindex[nodes[cellNNodes * i + j]];
                                outOrient[out] = orient[i];
                                out += 1;
                            }
                        }
                        outCs.addCells(new CellArray(ca.getType(), outNodes, outOrient, null));
                    }
                for (CellArray ca : cs.getBoundaryCellArrays())
                    if (ca != null) {
                        int cellNNodes = ca.getNCellNodes();
                        int nCells = ca.getNCells();
                        int[] nodes = ca.getNodes();
                        byte[] orient = ca.getOrientations();
                        int nOutCells = 0;
                        for (int i = 0; i < nCells; i++) {
                            boolean valid = true;
                            for (int j = 0; j < cellNNodes; j++)
                                if (mask[nodes[cellNNodes * i + j]] == 0)
                                    valid = false;
                            if (valid)
                                nOutCells += 1;
                        }
                        int[] outNodes = new int[cellNNodes * nOutCells];
                        byte[] outOrient = new byte[nOutCells];
                        for (int i = 0, out = 0; i < nCells; i++) {
                            boolean valid = true;
                            for (int j = 0; j < cellNNodes; j++)
                                if (mask[nodes[cellNNodes * i + j]] == 0)
                                    valid = false;
                            if (valid) {
                                for (int j = 0; j < cellNNodes; j++)
                                    outNodes[cellNNodes * out + j] = reindex[nodes[cellNNodes * i + j]];
                                outOrient[out] = orient[i];
                                out += 1;
                            }
                        }
                        outCs.setBoundaryCellArray(new CellArray(ca.getType(), outNodes, outOrient, null));
                    }
                outIrregularField.addCellSet(outCs);
            }
        }
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();
    }
}
