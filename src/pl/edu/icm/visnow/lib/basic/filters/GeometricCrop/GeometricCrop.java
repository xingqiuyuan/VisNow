//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.basic.filters.GeometricCrop;

import java.awt.Color;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.events.ColorEvent;
import pl.edu.icm.visnow.geometries.events.ColorListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams;
import pl.edu.icm.visnow.geometries.parameters.PresentationParams;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.NO_RUN;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.RUN_ONCE;
import static pl.edu.icm.visnow.lib.basic.filters.GeometricCrop.GeometricCropShared.*;
import pl.edu.icm.visnow.lib.gui.ComponentBasedUI.range.ComponentSubrange;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.field.subsetting.FieldSubset;
import pl.edu.icm.visnow.lib.utils.field.subsetting.GeometricSubsetParams;
import static pl.edu.icm.visnow.lib.utils.field.subsetting.GeometricSubsetParams.Depth.*;
import static pl.edu.icm.visnow.lib.utils.field.subsetting.GeometricSubsetParams.Position.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class GeometricCrop extends OutFieldVisualizationModule
{
    public static InputEgg[]  inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    
    protected Field inField = null;
    protected FloatLargeArray fieldCoords = null;
    
    protected int mergeCounter = 0;
    protected int oldTrueNSpace = -1;
    protected GlyphType type;
    protected InteractiveGlyph glyph;
    protected InteractiveGlyphParams glyphParams;
    protected InteractiveGlyphGUI glyphUI;
    protected ComponentSubrange subrange = new ComponentSubrange();
    protected GUI computeUI = new GUI();
    
    
    public GeometricCrop()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) {
                Color bgr = e.getSelectedColor();
                float[] bgrF = new float[4];
                bgr.getColorComponents(bgrF);
                if (glyph != null)
                    glyph.setColors(bgrF[0] + bgrF[1] + bgrF[2] > 1.5);
            }
        };
        type        = BOX;
        glyph       = new InteractiveGlyph(type);
        glyphParams = glyph.getParams();
        glyphUI     = glyph.getComputeUI();
        glyphUI.setParams(glyphParams);
        subrange.setNumericsOnly(true);
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && 
                                    parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    startAction();
                    return;
                }
                switch (name) {
                case GEOMETRIC_STRING:
                    glyphParams.setShow(parameters.get(GEOMETRIC));
                    if (outObj.getCurrentViewer() != null)
                        outObj.getCurrentViewer().refresh();
                    break;
                case TYPE_STRING:
                    type        = parameters.get(TYPE);
                    glyph.setType(type);
                    break;
                }
                if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });
        glyphParams.addParameterChangelistener(new ParameterChangeListener() {
            @Override
            public void parameterChanged(String name) {
                if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
                else    
                    computeUI.armRunButton();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI.setParameters(parameters);
                computeUI.setRange(subrange);
                computeUI.addUI(glyphUI);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
            }
        });
        outObj.addNode(glyph);
    }   

    @Override
    public Pick3DListener getPick3DListener() {
        return glyph.getPick3DListener();
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(GEOMETRIC, true),
            new Parameter<>(TYPE, BOX),
            new Parameter<>(POSITION, FULLY_IN),
            new Parameter<>(DEPTH, CELLS),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }
    
    
    @Override
    public void onActive()
    {   
        if (getInputFirstValue("inField") != null) {
            Field newField = ((VNField) getInputFirstValue("inField")).getField();
            if (newField != inField) {
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
                outField = null;
                if (newField == null)
                    return;
                subrange.setContainer(newField);
                inField = newField;
                int trueNSpace = inField.getTrueNSpace();
                if (trueNSpace != oldTrueNSpace) {
                    parameters.set(TYPE, trueNSpace == 2 ? RECTANGLE : BOX);
                    computeUI.setInFieldTrueNSpace(trueNSpace);
                }
                oldTrueNSpace = trueNSpace;
                if (inField.hasCoords())
                    glyphParams.setCoords(inField.getCoords(0));
                else
                {
                    float[][] xt = inField.getExtents();
                    float[] coords = new float[24];
                    for (int i = 0; i < 8; i++) {
                        int l = i;
                        for (int j = 0; j < 3; j++) {
                            coords[3 * i + j] = xt[l%2][j];
                            l /= 2;
                        }
                    }
                    glyphParams.setCoords(new FloatLargeArray(coords));
                }
            }
            else {
                outField = FieldSubset.createSubset(inField, 
                        new GeometricSubsetParams(parameters.get(GEOMETRIC), glyphParams, subrange,
                                                  parameters.get(POSITION), parameters.get(DEPTH)));
                computeUI.displayWarning(outField == null);
                setOutputValue("outRegularField", null);
                setOutputValue("outIrregularField", null);
                if (outField instanceof RegularField) 
                    setOutputValue("outRegularField", new VNRegularField((RegularField)outField));
                if (outField instanceof IrregularField) 
                    setOutputValue("outIrregularField", new VNIrregularField((IrregularField)outField));
            }
            computeUI.disarmRunButton();
            prepareOutputGeometry();
            ui.getPresentationGUI().getRenderingGUI().setCullMode(0);
            ui.getPresentationGUI().getRenderingGUI().setShadingMode(1);
            show();
            outObj.addNode(glyph);
        }
    }
    
}
