package pl.edu.icm.visnow.lib.basic.filters.CropDown;

import java.util.ArrayList;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import static pl.edu.icm.jscic.utils.CropDownUtils.*;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;

//TODO: proper javadoc here
/**
 *
 * @author szpak
 */
public class CropDownCore
{

    private RegularField inField;
    private RegularField outField;

    //FIXME: no field schema testing ... modules may be incorrectly connected now (in Unicore wizard).
    public final static String IN_RFIELD_MAIN = "inField";
    public final static String OUT_RFIELD_MAIN = "outField";

    public CropDownCore()
    {
    }

    public static CropDownCore createCore()
    {
        return new CropDownCore();
    }


    //TODO: put somwhere recalculation of auto smart values (now it's implemented in GUI (sic!))    
    /**
     * Crops field including low index and excluding up index.
     */
    public void recalculate(int[] low, int[] up, int[] down)
    {
        //reset out field
        outField = null;

        if (inField != null) {

            int[] dims = inField.getDims();
            int[] outDims = new int[dims.length];
            for (int i = 0; i < outDims.length; i++) {
                if (low[i] < 0 || up[i] > dims[i] || down[i] < 1 || up[i] < low[i] + 2 * down[i]) {
                    return;
                }
                outDims[i] = (up[i] - low[i] - 1) / down[i] + 1;
            }
            outField = new RegularField(outDims);

            if (inField.getCoords() != null) {
                ArrayList<LargeArray> oldTimeCoordsSeries = inField.getCoords().getValues();
                float[] oldTimeCoordsTimes = inField.getCoords().getTimesAsArray();
                for (int i = 0; i < oldTimeCoordsSeries.size(); i++) {
                    outField.setCoords((FloatLargeArray)cropDownArray(oldTimeCoordsSeries.get(i), 3, dims, low, up, down),
                                       oldTimeCoordsTimes[i]);
                }
                outField.setCurrentCoords((FloatLargeArray)cropDownArray(inField.getCurrentCoords(), 3, dims, low, up, down));
            } else {
                float[][] outAffine = new float[4][3];
                float[][] affine = inField.getAffine();
                System.arraycopy(affine[3], 0, outAffine[3], 0, 3);
                for (int i = 0; i < outDims.length; i++) {
                    for (int j = 0; j < 3; j++) {
                        outAffine[3][j] += low[i] * affine[i][j];
                        outAffine[i][j] = affine[i][j] * down[i];
                    }
                }
                outField.setAffine(outAffine);
            }
            if (inField.hasMask()) {
                outField.setCurrentMask((LogicLargeArray)cropDownArray(inField.getCurrentMask(), 1, dims, low, up, down));
            }

            for (int i = 0; i < inField.getNComponents(); i++) {
                DataArray dta = inField.getComponent(i);
                if (!dta.isNumeric()) {
                    continue;
                }
                int outNData = (int) outField.getNNodes();
                DataArray outDta = DataArray.create(dta.getType(), outNData, dta.getVectorLength(), dta.getName(), dta.getUnit(), dta.getUserData());
                outDta.setPreferredRanges(dta.getPreferredMinValue(), dta.getPreferredMaxValue(), dta.getPreferredPhysMinValue(),dta.getPreferredPhysMaxValue());
                outDta.setTimeData(dta.getTimeData().cropDown(dta.getVectorLength(), dims, low, up, down));
                outField.addComponent(outDta);
            }
        }
    }

    public static String[] getInFieldNames()
    {
        return new String[]{IN_RFIELD_MAIN};
    }

    public static String[] getOutFieldNames()
    {
        return new String[]{OUT_RFIELD_MAIN};
    }

    public Field getOutField(String name)
    {
        if (name.equals(OUT_RFIELD_MAIN))
            return outField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }

    public void setInField(String name, Field inField)
    {
        if (name.equals(IN_RFIELD_MAIN))
            this.inField = (RegularField) inField;
        else
            throw new IllegalArgumentException("Incorrect field name: " + name);
    }
}
