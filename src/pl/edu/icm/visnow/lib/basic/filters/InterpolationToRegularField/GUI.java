//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.InterpolationToRegularField;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.visnow.engine.core.ParameterProxy;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.geometries.parameters.TransformParams;
import static pl.edu.icm.visnow.lib.basic.filters.InterpolationToRegularField.InterpolationToRegularFieldShared.*;

public class GUI extends javax.swing.JPanel
{

    private int trueDim;
    private Parameters parameters;
    private final ChangeListener transformParams3DListener;
    private final ChangeListener transformParams2DListener;
    private final TransformParams transformParams2D;
    private final TransformParams transformParams3D;

    /**
     * Creates new form EmptyVisnowModuleGUI
     */
    public GUI()
    {
        initComponents();
        transformPanel.exportButtonVisible(false);

        transformParams3DListener = new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                parameters.set(ADJUSTING, transformPanel.isAdjusting(),
                               TRANSFORM_MATRIX, transformPanel.getTransformMatrix(),
                               ROTATION_VALUES, transformPanel.getRotationValues(),
                               TRANSLATION_VALUES, transformPanel.getTranslationValues(),
                               SCALE_VALUES, transformPanel.getScaleValues(),
                               SCALE, transformPanel.getScale());
            }
        };

        transformParams2DListener = new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                parameters.set(ADJUSTING, transform2DPanel.isAdjusting(),
                               TRANSFORM_MATRIX, transform2DPanel.getTransformMatrix(),
                               ROTATION_VALUES, new float[]{transform2DPanel.getRotation()},
                               TRANSLATION_VALUES, transform2DPanel.getTranslationValues(),
                               SCALE_VALUES, transform2DPanel.getScaleValues(),
                               SCALE, transform2DPanel.getScale());

            }
        };

        transformParams3D = new TransformParams();
        transformParams3D.addChangeListener(transformParams3DListener);
        transformParams2D = new TransformParams();
        transformParams2D.addChangeListener(transformParams2DListener);
        startRotatedBox.setVisible(false);
    }

    void setParameters(Parameters parameters)
    {
        this.parameters = parameters;
    }

    void updateGUI(ParameterProxy p, boolean resetFully, boolean setRunButtonPending)
    {

        startRotatedBox.setSelected(p.get(START_ROTATED));
        trueDim = p.get(META_TRUE_NSPACE);
        if (p.get(META_TRUE_NSPACE) == 3) {
            transformParams3D.setTransform(p.get(TRANSFORM_MATRIX));
            transformPanel.setTransformParams(transformParams3D);
            transformPanel.updateGUI(p.get(ROTATION_VALUES), p.get(TRANSLATION_VALUES), p.get(SCALE_VALUES), p.get(SCALE));
            transformPanel.setTransSensitivity(p.get(META_DIAMETER) / 500);
        } else {
            transformParams2D.setTransform(p.get(TRANSFORM_MATRIX));
            transform2DPanel.setTransformParams(transformParams2D);
            transform2DPanel.updateGUI(p.get(ROTATION_VALUES)[0], p.get(TRANSLATION_VALUES), p.get(SCALE_VALUES), p.get(SCALE));
            transform2DPanel.setTransSensitivity(p.get(META_DIAMETER) / 500);
        }
        transformPanel.setVisible(trueDim == 3);
        transform2DPanel.setVisible(trueDim == 2);
        resSlider0.setVal(p.get(RESOLUTION0));
        resSlider1.setVal(p.get(RESOLUTION1));
        resSlider2.setVal(p.get(RESOLUTION2));
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        resSlider0 = new pl.edu.icm.visnow.gui.widgets.EnhancedIntSlider();
        resSlider1 = new pl.edu.icm.visnow.gui.widgets.EnhancedIntSlider();
        resSlider2 = new pl.edu.icm.visnow.gui.widgets.EnhancedIntSlider();
        jButton1 = new javax.swing.JButton();
        transformPanel = new pl.edu.icm.visnow.geometries.gui.TransformPanel();
        transform2DPanel = new pl.edu.icm.visnow.geometries.gui.Transform2DPanel();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        startRotatedBox = new javax.swing.JCheckBox();

        setLayout(new java.awt.GridBagLayout());

        resSlider0.setBorder(javax.swing.BorderFactory.createTitledBorder("x resolution")); // NOI18N
        resSlider0.setMax(500);
        resSlider0.setMin(50);
        resSlider0.setName(""); // NOI18N
        resSlider0.setVal(100);
        resSlider0.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                resSlider0StateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(resSlider0, gridBagConstraints);

        resSlider1.setBorder(javax.swing.BorderFactory.createTitledBorder("y resolution")); // NOI18N
        resSlider1.setMax(500);
        resSlider1.setMin(50);
        resSlider1.setName(""); // NOI18N
        resSlider1.setVal(100);
        resSlider1.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                resSlider1StateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(resSlider1, gridBagConstraints);

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pl/edu/icm/visnow/lib/basic/filters/InterpolationToRegularField/Bundle"); // NOI18N
        resSlider2.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("GUI.resSlider2.border.title"))); // NOI18N
        resSlider2.setMax(500);
        resSlider2.setMin(50);
        resSlider2.setName(""); // NOI18N
        resSlider2.setVal(100);
        resSlider2.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                resSlider2StateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(resSlider2, gridBagConstraints);

        jButton1.setText("Output interpolated field");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(jButton1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        add(transformPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        add(transform2DPanel, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.weighty = 1.0;
        add(filler2, gridBagConstraints);

        startRotatedBox.setText("rotate box to principal axes"); // NOI18N
        startRotatedBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                startRotatedBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 0);
        add(startRotatedBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        parameters.set(IS_OUTPUT, true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void resSlider0StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_resSlider0StateChanged
        if (resSlider0.isAdjusting())
            return;
        parameters.set(RESOLUTION0, resSlider0.getVal());
    }//GEN-LAST:event_resSlider0StateChanged

    private void startRotatedBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_startRotatedBoxActionPerformed
    {//GEN-HEADEREND:event_startRotatedBoxActionPerformed
        parameters.set(START_ROTATED, startRotatedBox.isSelected());
    }//GEN-LAST:event_startRotatedBoxActionPerformed

    private void resSlider1StateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_resSlider1StateChanged
    {//GEN-HEADEREND:event_resSlider1StateChanged
        if (resSlider1.isAdjusting())
            return;
        parameters.set(RESOLUTION1, resSlider1.getVal());
    }//GEN-LAST:event_resSlider1StateChanged

    private void resSlider2StateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_resSlider2StateChanged
    {//GEN-HEADEREND:event_resSlider2StateChanged
        if (resSlider2.isAdjusting())
            return;
        parameters.set(RESOLUTION2, resSlider2.getVal());// TODO add your handling code here:
    }//GEN-LAST:event_resSlider2StateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler2;
    private javax.swing.JButton jButton1;
    private pl.edu.icm.visnow.gui.widgets.EnhancedIntSlider resSlider0;
    private pl.edu.icm.visnow.gui.widgets.EnhancedIntSlider resSlider1;
    private pl.edu.icm.visnow.gui.widgets.EnhancedIntSlider resSlider2;
    private javax.swing.JCheckBox startRotatedBox;
    private pl.edu.icm.visnow.geometries.gui.Transform2DPanel transform2DPanel;
    private pl.edu.icm.visnow.geometries.gui.TransformPanel transformPanel;
    // End of variables declaration//GEN-END:variables

    public void resetTransform()
    {
        transformPanel.resetTransform();
        transform2DPanel.resetTransform();
    }

}
