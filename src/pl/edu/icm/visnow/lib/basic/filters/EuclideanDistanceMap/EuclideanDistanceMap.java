//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.EuclideanDistanceMap;

import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import static pl.edu.icm.visnow.lib.utils.numeric.EuclideanDistanceMap.*;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class EuclideanDistanceMap extends OutFieldVisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected RegularField inRegularField = null;
    protected Field inMesh = null;
    protected int trueDim;
    protected int nThreads = 1;
    protected int nMeshNodes = 0;
    protected float[] meshCoords;

    public EuclideanDistanceMap()
    {
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || getInputFirstValue("inMesh") == null)
            return;
        RegularField field = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (field == null)
            return;
        if (inRegularField != field) {
            inRegularField = field;
            if (inRegularField.getTrueNSpace() != inRegularField.getDimNum() || inRegularField.hasCoords())
                return;
            
        }
        Field mesh = ((VNField) getInputFirstValue("inMesh")).getField();
        if (mesh == null || trueDim < mesh.getTrueNSpace())
            return;
        if (inMesh != mesh)
            inMesh = mesh;
        nMeshNodes = (int)inMesh.getNNodes();
        outField = inMesh.cloneShallow();
        if (inMesh.hasCoords())
            meshCoords = inMesh.getCurrentCoords().getData();
        else if (inMesh instanceof RegularField)
            meshCoords = ((RegularField)inMesh).getCoordsFromAffine().getData();
        else
           return;
        int[] dims = inRegularField.getDims();
        float[] setCoords; 
        if (dims.length == 2) {
        
        }
        float[] map = createMap2D(inRegularField.getDims(), meshCoords);

        if (outField instanceof RegularField)
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));

        if (outField != null){
            if (outField instanceof RegularField) {
            setOutputValue("outRegularField", new VNRegularField((RegularField) outField));
            setOutputValue("outIrregularField", null);
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", new VNIrregularField((IrregularField) outField));
        }
        } else {
            setOutputValue("outRegularField", null);
            setOutputValue("outIrregularField", null);
        }
        prepareOutputGeometry();
        show();

    }
}
