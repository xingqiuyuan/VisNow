
package pl.edu.icm.visnow.lib.basic.filters.InterpolateToMesh;

import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;

/**
 *
 * @author know
 */


public class TestLargeArrs
{
    public static void main(String args[])
    {
        byte[] ta = new byte[1000000000];
        for (int i = 0; i < ta.length; i++) 
            ta[i] = (byte)(0xff & (i%255));
        UnsignedByteLargeArray t = new UnsignedByteLargeArray(ta);
        float f = 0;
        long l = System.currentTimeMillis();
        for (int i = 0; i < ta.length; i++) 
            f += 0xff & ta[i];
        System.out.println("array access "+(System.currentTimeMillis()-l) + " " + f);
        l = System.currentTimeMillis();
        f = 0;
        for (int i = 0; i < ta.length; i++) 
            f += 0xff & t.getByte(i);
        System.out.println("byte access  "+(System.currentTimeMillis()-l) + " " + f);
        l = System.currentTimeMillis();
        f = 0;
        for (int i = 0; i < ta.length; i++) 
            f += t.getFloat(i);
        System.out.println("byte access  "+(System.currentTimeMillis()-l) + " " + f);
    }
    
}
