//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.AnisotropicDenoiser;

import java.util.ArrayList;
import static org.apache.commons.math3.util.FastMath.*;

import java.util.Arrays;

import org.apache.log4j.Logger;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;

import pl.edu.icm.visnow.system.main.VisNow;

import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;

import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.ProgressAgent;

import pl.edu.icm.visnow.lib.types.VNRegularField;

import pl.edu.icm.visnow.lib.utils.field.FieldSmoothDown;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.denoising.*;

import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;

import static pl.edu.icm.visnow.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiserShared.*;
import static pl.edu.icm.visnow.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiserShared.Method.*;
import static pl.edu.icm.visnow.lib.basic.filters.AnisotropicDenoiser.AnisotropicDenoiserShared.Resource.*;

import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;
import pl.edu.icm.visnow.lib.utils.field.SliceRegularField;

import pl.edu.icm.visnow.system.utils.usermessage.Level;
import pl.edu.icm.visnow.system.utils.usermessage.UserMessage;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl) Warsaw University,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 *
 * Modified by Szymon Jaranowski (s.jaranowski@icm.edu.pl), University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling,
 * revisions above 245.
 */
public class AnisotropicDenoiser extends OutFieldVisualizationModule
{
    private static final Logger LOGGER = Logger.getLogger(AnisotropicDenoiser.class);

    /**
     * Creates a new instance of AnisotropicDenoiser
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected GUI gui = null;

    protected RegularField inField = null;
    protected RegularField anisotropyField = null;
    //FIXME: temporal solution to properly set run button in pending state
    protected RegularField anisotropyInputField = null;

    protected AnisotropicDenoisingParams currentParameters;

    protected AbstractAnisotropicWeightedMedianCompute medianCompute = new AnisotropicWeightedMedianCompute();
    protected AbstractAnisotropicWeightedAverageCompute averageCompute = new AnisotropicWeightedAverageCompute();
    protected AbstractAnisotropicWeightedAverageCompute averageComputeWithCuda = new AnisotropicWeightedAverageComputeWithCuda();

    protected FloatLargeArray aniso = null;

    private int runQueue = 0;

    public AnisotropicDenoiser()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name != null && name.equals(RUNNING_MESSAGE.getName()) && parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startAction();
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                gui = new GUI();
                gui.setParameterProxy(parameters);
                ui.addComputeGUI(gui);
                setPanel(ui);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(BY_SLICE, false),
            new Parameter<>(RESOURCE, VisNow.hasCudaDevice() ? GPU_CUDA : CPU),
            new Parameter<>(METHOD, AVERAGE),
            new Parameter<>(PRESMOOTH, false),
            new Parameter<>(PRESMOOTH_METHOD, AVERAGE),
            new Parameter<>(PRESMOOTH_RADIUS, 5),
            new Parameter<>(RADIUS, 3),
            new Parameter<>(SLOPE, 3.0f),
            new Parameter<>(SLOPE1, 1.0f),
            new Parameter<>(ITERATIONS, 1),
            new Parameter<>(COMPONENTS, new int[][]{{0, 0}}),
            new Parameter<>(COMPUTE_WEIGHTS, false),
            new Parameter<>(COMPUTE_ONLY_WEIGHTS, false),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN),
            new Parameter<>(META_INPUT_FIELD_COMPONENT_NAMES, new String[]{"component1"}),
            new Parameter<>(META_ANISOTROPY_FIELD_COMPONENT_NAMES, new String[]{"anisotropy1"}),
            new Parameter<>(META_INPUT_FIELD_SCALAR_COMPONENT_NAMES, new String[]{"component1"}),
            new Parameter<>(META_ANISOTROPY_FIELD_SCALAR_COMPONENT_NAMES, new String[]{"anisotropy1"}),
            new Parameter<>(META_COMPONENT_EXTENTS, new double[][]{{1.0, 0.0}})
        };
    }

    /**
     * Find maximum value in a given column in 2D array.
     *
     * @param arr Given array.
     * @param col Given column, bounds are not checked!
     *
     * @return Maximum value in the column.
     */
    private int findMaxColumn(int[][] arr, int col)
    {
        int max = 0;
        for (int i = 0; i < arr.length; ++i)
            if (arr[i][col] > max)
                max = arr[i][col];
        return max;
    }

    private void resetParameters()
    {
        // Be sure parameters are not active!

        parameters.set(PRESMOOTH, getInputFirstValue("anisotropyField") == null);

        // Smart initialize "components" array.
        int inputFirstScalarComponent = inField != null ? getFirstScalarComponent(inField) : 0;
        int anisotropyFirstScalarComponent;
        if (anisotropyField != null) {
            anisotropyFirstScalarComponent = getFirstScalarComponent(anisotropyField);
        } else {
            anisotropyFirstScalarComponent = getFirstScalarComponent(inField);
        }

        parameters.set(BY_SLICE, false);

        parameters.set(RESOURCE, VisNow.hasCudaDevice() ? GPU_CUDA : CPU);
        parameters.set(METHOD, AVERAGE);

        parameters.set(PRESMOOTH, true);
        parameters.set(PRESMOOTH_RADIUS, 5);
        parameters.set(PRESMOOTH_METHOD, AVERAGE);

        parameters.set(RADIUS, 3);
        parameters.set(SLOPE, 3.0f);
        parameters.set(ITERATIONS, 1);

        parameters.set(COMPUTE_WEIGHTS, false);
        parameters.set(COMPUTE_ONLY_WEIGHTS, false);

        parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(META_INPUT_FIELD_COMPONENT_NAMES, Arrays.copyOf(inField.getComponentNames(), inField.getComponentNames().length));
        parameters.set(META_INPUT_FIELD_SCALAR_COMPONENT_NAMES, Arrays.copyOf(inField.getScalarComponentNames(), inField.getScalarComponentNames().length));
        if (anisotropyField != null) {
            parameters.set(META_ANISOTROPY_FIELD_COMPONENT_NAMES, Arrays.copyOf(anisotropyField.getComponentNames(), anisotropyField.getComponentNames().length));
            parameters.set(META_ANISOTROPY_FIELD_SCALAR_COMPONENT_NAMES, Arrays.copyOf(anisotropyField.getScalarComponentNames(), anisotropyField.getScalarComponentNames().length));
        } else {
            parameters.set(META_ANISOTROPY_FIELD_COMPONENT_NAMES, Arrays.copyOf(inField.getComponentNames(), inField.getComponentNames().length));
            parameters.set(META_ANISOTROPY_FIELD_SCALAR_COMPONENT_NAMES, Arrays.copyOf(inField.getScalarComponentNames(), inField.getScalarComponentNames().length));
        }
        parameters.set(COMPONENTS, new int[][]{{inputFirstScalarComponent, anisotropyFirstScalarComponent}});
        // Set smart SLOPE1 parameter, based on input field first scalar l.
        int[][] components = parameters.get(COMPONENTS);
        parameters.set(SLOPE1, inField != null ? (float) (inField.getComponent(components[0][0]).getPreferredMaxValue() - inField.getComponent(components[0][1]).getPreferredMinValue()) : 1.0f);

    }

    private void validateParametersAndSetSmart(boolean resetParameters, boolean inFieldIsNew)
    {
        parameters.setParameterActive(false);

        if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) parameters.set(RUNNING_MESSAGE, NO_RUN);

        parameters.set(PRESMOOTH, getInputFirstValue("anisotropyField") == null);

        // Smart initialize "components" array.
        int inputFirstScalarComponent = inField != null ? getFirstScalarComponent(inField) : 0;
        int anisotropyFirstScalarComponent;
        if (anisotropyField != null) {
            anisotropyFirstScalarComponent = getFirstScalarComponent(anisotropyField);
        } else {
            anisotropyFirstScalarComponent = getFirstScalarComponent(inField);
        }

        if (resetParameters)
            resetParameters();
        else {
            int[][] components = parameters.get(COMPONENTS);

            boolean resetAnyway = false;

            if (anisotropyField == null)
                parameters.set(PRESMOOTH, true);
            else // Check, if anisotropy components can be applied to new field.
                if (findMaxColumn(components, 1) >= anisotropyField.getNComponents()) {
                    resetAnyway = true;
                    parameters.set(COMPONENTS, new int[][]{{inputFirstScalarComponent, anisotropyFirstScalarComponent}}); // Actual reset.
                    components = parameters.get(COMPONENTS);
                    parameters.set(SLOPE1, inField != null ? (float) (inField.getComponent(components[0][0]).getPreferredMaxValue() - inField.getComponent(components[0][1]).getPreferredMinValue()) : 1.0f);

                }

            // Check, if input field components can be applied to new field.
            if (findMaxColumn(components, 0) >= inField.getNComponents()) {
                resetAnyway = true;
                parameters.set(COMPONENTS, new int[][]{{inputFirstScalarComponent, anisotropyFirstScalarComponent}}); // Actual reset.
                components = parameters.get(COMPONENTS);
                parameters.set(SLOPE1, inField != null ? (float) (inField.getComponent(components[0][0]).getPreferredMaxValue() - inField.getComponent(components[0][1]).getPreferredMinValue()) : 1.0f);
            }

            if (resetAnyway)
                resetParameters();
        }

        if (inFieldIsNew) {
            int[][] components = parameters.get(COMPONENTS);
            parameters.set(SLOPE1, inField != null ? (float) (inField.getComponent(components[0][0]).getPreferredMaxValue() - inField.getComponent(components[0][1]).getPreferredMinValue()) : 1.0f);
        }

        // Set up meta parameters. Always needed.
        parameters.set(META_INPUT_FIELD_COMPONENT_NAMES, inField.getComponentNames());
        parameters.set(META_INPUT_FIELD_SCALAR_COMPONENT_NAMES, inField.getScalarComponentNames());
        String[] anisotropyFieldComponentNames = null;
        if (anisotropyField != null) {
            anisotropyFieldComponentNames = anisotropyField.getScalarComponentNames();
            parameters.set(META_ANISOTROPY_FIELD_COMPONENT_NAMES, anisotropyField.getComponentNames());
        } else {
            anisotropyFieldComponentNames = inField.getScalarComponentNames();
//            for (int i = 0; i < anisotropyFieldComponentNames.length; ++i)
//                anisotropyFieldComponentNames[i] += " (presmoothed)";
            parameters.set(META_ANISOTROPY_FIELD_COMPONENT_NAMES, inField.getComponentNames());
        }

        parameters.set(META_ANISOTROPY_FIELD_SCALAR_COMPONENT_NAMES, anisotropyFieldComponentNames);
        double[][] componentsExtents = new double[inField.getNComponents()][2];
        for (int i = 0; i < inField.getNComponents(); ++i) {
            componentsExtents[i][0] = inField.getComponent(i).getPreferredMaxValue();
            componentsExtents[i][1] = inField.getComponent(i).getPreferredMinValue();
        }
        parameters.set(META_COMPONENT_EXTENTS, componentsExtents);

        parameters.setParameterActive(true);
    }

    private int getFirstScalarComponent(RegularField inField)
    {
        if (inField != null) {
            String firstScalarComponentName = inField.getScalarComponentNames()[0];
            return Arrays.asList(inField.getComponentNames()).indexOf(firstScalarComponentName);
        }
        return 0;
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        gui.update(clonedParameterProxy, resetFully, setRunButtonPending);
    }

    private int nNumericComponents = 0;
    int[][] presmoothedComponents = {{0, 0}};

    private RegularField presmooth(Parameters p)
    {
        nNumericComponents = 0;
        for (int component = 0; component < inField.getNComponents(); component++)
            if (inField.getComponent(component).isNumeric())
                nNumericComponents += 1;
        presmoothedComponents = new int[nNumericComponents][2];
        for (int component = 0, l = 0; component < inField.getNComponents(); component++)
            if (inField.getComponent(component).isNumeric()) {
                presmoothedComponents[l][0] = component;
                presmoothedComponents[l][1] = -1;
                l += 1;
            }
        float r = p.get(PRESMOOTH_RADIUS);
        FloatLargeArray[] outArrays = new FloatLargeArray[nNumericComponents];
        for (int l = 0; l < inField.getNComponents(); l++)
            outArrays[l] = new FloatLargeArray(inField.getComponent(presmoothedComponents[l][0]).getRawArray().length());
        AnisotropicDenoisingParams pp = new AnisotropicDenoisingParams(AnisotropicDenoisingParams.MEDIAN, (int) (1.5 * r), r, 1, presmoothedComponents);

        RegularField presmoothedField = null;
        if (p.get(BY_SLICE)) {
            int[] dims = inField.getDims();
            presmoothedField = new RegularField(dims);
            int nSlices = dims[dims.length - 1];
            for (int sl = 0; sl < nSlices; sl++) {
                RegularField slice = SliceRegularField.sliceField(inField, dims.length - 1, sl);
                RegularField presmoothedSlice
                        = p.get(PRESMOOTH_METHOD) == MEDIAN
                        ? medianCompute.computePresmoothing(slice, pp)
                        : FieldSmoothDown.smoothDownToFloat(slice, 1, r, VisNow.availableProcessors());
                for (int l = 0; l < nNumericComponents; l++) {
                    long len = presmoothedSlice.getComponent(l).getRawArray().length();
                    LargeArrayUtils.arraycopy(presmoothedSlice.getComponent(l).getRawFloatArray(), 0, outArrays[l], sl * len, len);
                }
            }
            for (int l = 0; l < inField.getNComponents(); l++) {
                int component = presmoothedComponents[l][0];
                presmoothedField.addComponent(DataArray.create(outArrays[l],
                                                               inField.getComponent(component).getVectorLength(),
                                                               inField.getComponent(component).getName()));
            }
        } else if (p.get(PRESMOOTH_METHOD) == MEDIAN)
            presmoothedField = medianCompute.computePresmoothing(inField, pp);
        else
            presmoothedField = FieldSmoothDown.smoothDownToFloat(inField, 1, (float) parameters.get(PRESMOOTH_RADIUS), VisNow.availableProcessors());
        return presmoothedField;
    }

    private RegularField anisotropicallySmooth(Parameters clonedParameters, RegularField inField, RegularField anisotropyField, ProgressAgent progressAgent)
    {
        RegularField outField = null;
        RegularField tmpField = null;
        RegularField tmpAnisotropy = anisotropyField;
        switch (clonedParameters.get(METHOD)) {
            case AVERAGE:
                if (clonedParameters.get(RESOURCE) == CPU || !VisNow.hasCudaDevice()) {
                    for (int i = 0; i < clonedParameters.get(ITERATIONS); i++) {
                        tmpField = averageCompute.compute(inField, tmpAnisotropy, currentParameters, progressAgent);
                        tmpAnisotropy = tmpField;
                    }
                    outField = tmpField;
                } else
                    /* Number of iterations is passed via "currentParams" here. */
                    outField = averageComputeWithCuda.compute(inField, anisotropyField, currentParameters, progressAgent);
                break;
            case MEDIAN:
                for (int i = 0; i < clonedParameters.get(ITERATIONS); i++) {
                    tmpField = medianCompute.compute(inField, tmpAnisotropy, currentParameters, progressAgent);
                    tmpAnisotropy = tmpField;
                }
                outField = tmpField;
                break;
            default: 
                LOGGER.info("Smoothing method was not selected");
        }
        return outField;
    }

    private RegularField computeOutField(Parameters clonedParameters)
    {
        RegularField outField = null;

        currentParameters = new AnisotropicDenoisingParams(
                clonedParameters.get(ITERATIONS),
                // TODO Try to find a better solution for following line.
                clonedParameters.get(METHOD) == AVERAGE ? 0 : 1,
                clonedParameters.get(RADIUS),
                clonedParameters.get(SLOPE),
                clonedParameters.get(SLOPE1),
                clonedParameters.get(COMPONENTS)
        );

        // Start the timer.
        long t0 = System.currentTimeMillis();

        long lastDimensionLength = inField.getDims()[inField.getDimNum() - 1];
        ProgressAgent progressAgent = getProgressAgent(lastDimensionLength * clonedParameters.get(ITERATIONS) * currentParameters.getComponentsNumber());

        ArrayList<DataArray> components = new ArrayList<>();
        for (int[] getter : clonedParameters.get(COMPONENTS)) {
            components.add(inField.getComponent(getter[0]));
        }

        if (clonedParameters.get(BY_SLICE)) {
            int[] dims = inField.getDims();
            outField = new RegularField(dims);
            int nSlices = dims[dims.length - 1];
            LargeArray[] outArrays = new LargeArray[components.size()];
            for (int component = 0; component < components.size(); component++)
                if (inField.getComponent(component).isNumeric()) {
                    LargeArray in = inField.getComponent(component).getRawArray();
                    outArrays[component] = LargeArrayUtils.create(in.getType(), inField.getComponent(component).getRawArray().length(), false);
                }
            for (int sl = 0; sl < nSlices; sl++) {
                RegularField slice = SliceRegularField.sliceField(inField, dims.length - 1, sl);
                RegularField anisoSlice = SliceRegularField.sliceField(anisotropyField, dims.length - 1, sl);
                RegularField smoothedSlice = anisotropicallySmooth(clonedParameters, slice, anisoSlice, progressAgent);
                for (int component = 0, cmp = 0; component < components.size(); component++) {
                    if (inField.getComponent(component).isNumeric() && smoothedSlice != null) {
                        long l = smoothedSlice.getComponent(cmp).getRawArray().length();
                        LargeArrayUtils.arraycopy(smoothedSlice.getComponent(cmp).getRawArray(), 0, outArrays[component], sl * l, l);
                        cmp += 1;
                    }
                }
            }
            for (int component = 0; component < components.size(); component++)
                if (inField.getComponent(component).isNumeric())
                    outField.addComponent(DataArray.create(outArrays[component],
                                                           inField.getComponent(component).getVectorLength(),
                                                           inField.getComponent(component).getName()));
        } else
            outField = anisotropicallySmooth(clonedParameters, inField, anisotropyField, progressAgent);

        // Log the time of computation.
        LOGGER.info("Elapsed time " + (System.currentTimeMillis() - t0) / 1000.f);

        if (outField == null)
            return null;

        if (inField.getComponent(0).getUserData() != null)
            outField.getComponent(0).setUserData(inField.getComponent(0).getUserData());

        outField.setAffine(inField.getAffine());

        return outField;
    }

    private RegularField computeWeights(Parameters par)
    {
        if (inField == null)
            return null;

        int radius = par.get(RADIUS);
        float slope = par.get(SLOPE) * par.get(SLOPE);
        float slope1 = par.get(SLOPE1) * par.get(SLOPE1);

        int[] dims = inField.getDims();

        for (int i = 0; i < dims.length; i++)
            if (dims[i] < 2 * radius + 2)
                return null;

        RegularField weights = new RegularField(dims);
        weights.setAffine(inField.getAffine());
        weights.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());

        for (int[] cmp : par.get(COMPONENTS)) {
            int component = cmp[0];
            int anisotropyComponent = cmp[1];
            DataArray dataArr = inField.getComponent(component);
            int anisoVlen = 0;
            if (anisotropyComponent >= 0) {
                DataArray anisoArr = anisotropyField.getComponent(anisotropyComponent);
                anisoVlen = anisoArr.getVectorLength();
                if (anisoVlen == 1 || anisoVlen == dims.length)
                    aniso =  (FloatLargeArray) anisoArr.getRawArray();
            }
            FloatLargeArray data = new FloatLargeArray(inField.getNNodes());
            switch (dims.length) {
                case 3:
                    for (int k0 = radius; k0 < dims[2] - radius; k0 += 2 * radius + 3) {
                        setProgress((1.f * k0) / dims[2]);
                        for (int j0 = radius; j0 < dims[1] - radius; j0 += 2 * radius + 3)
                            for (int i0 = radius; i0 < dims[0] - radius; i0 += 2 * radius + 3) {
                                int n0 = (k0 * dims[1] + j0) * dims[0] + i0;
                                if (aniso != null)
                                    if (anisoVlen > 1)
                                        for (int k = -radius; k <= radius; k++)
                                            for (int j = -radius; j <= radius; j++)
                                                for (int i = -radius, n = ((k + k0) * dims[1] + j + j0) * dims[0] + i0 - radius, p = k * k + j * j; i <= radius; i++, n++) {
                                                    float s = k * aniso.getFloat(n0 * anisoVlen + 2) +
                                                            j * aniso.getFloat(n0 * anisoVlen + 1) +
                                                            i * aniso.getFloat(n0 * anisoVlen);
                                                    data.setFloat(n, (float) (exp(-s * s / slope1 - (p + i * i) / slope)));
                                                }
                                    else
                                        for (int k = -radius; k <= radius; k++)
                                            for (int j = -radius; j <= radius; j++)
                                                for (int i = -radius, n = ((k + k0) * dims[1] + j + j0) * dims[0] + i0 - radius, p = k * k + j * j; i <= radius; i++, n++) {
                                                    float s = aniso.getFloat(n0) - aniso.getFloat(n);
                                                    data.setFloat(n,(float) (exp(-s * s / slope1 - (p + i * i) / slope)));
                                                }
                                else
                                    for (int k = -radius; k <= radius; k++)
                                        for (int j = -radius; j <= radius; j++)
                                            for (int i = -radius, n = ((k + k0) * dims[1] + j + j0) * dims[0] + i0 - radius, p = k * k + j * j; i <= radius; i++, n++)
                                                data.setFloat(n, (float) (exp(-(p + i * i) / slope)));
                            }
                    }
                    break;
                case 2:
                    for (int j0 = radius; j0 < dims[1] - radius; j0 += 2 * radius + 3)
                        for (int i0 = radius; i0 < dims[0] - radius; i0 += 2 * radius + 3) {
                            int n0 = j0 * dims[0] + i0;
                            if (aniso != null)
                                if (anisoVlen > 1)
                                    for (int j = -radius; j <= radius; j++)
                                        for (int i = -radius, n = (j + j0) * dims[0] + i0 - radius, p = j * j; i <= radius; i++, n++) {
                                            float s = j * aniso.getFloat(n0 * anisoVlen + 1) +
                                                    i * aniso.getFloat(n0 * anisoVlen);
                                            data.setFloat(n, (float) (exp(-s * s / slope1 - (p + i * i) / slope)));
                                        }
                                else
                                    for (int j = -radius; j <= radius; j++)
                                        for (int i = -radius, n = (j + j0) * dims[0] + i0 - radius, p = j * j; i <= radius; i++, n++) {
                                            float s = aniso.getFloat(n0) - aniso.getFloat(n);
                                            data.setFloat(n,(float) (exp(-s * s / slope1 - (p + i * i) / slope)));
                                        }
                            else
                                for (int j = -radius; j <= radius; j++)
                                    for (int i = -radius, n = (j + j0) * dims[0] + i0 - radius, p = j * j; i <= radius; i++, n++)
                                        data.setFloat(n,(float) (exp(-(p + i * i) / slope)));
                        }
                    break;
                    default:
                        LOGGER.info("1D data is not supported in Anisotropic Denoiser");
            }
            weights.addComponent(DataArray.create(data, 1, dataArr.getName()));
        }
        return weights;
    }

    @Override
    public void onActive()
    {
        LOGGER.debug("FromVNA: " + isFromVNA());

        if (getInputFirstValue("inField") != null) {
            this.switchPanelToGUI();

            // 1. Get input fields from ports, and check, whether parameters or GUI should reset.
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            RegularField newAnisotropyField = null;

            boolean isDifferentAnisotropyField = false;
            //FIXME: temporal solution to properly set run button in pending state
            boolean isDifferentAnisotropyInputField = false;

            if (getInputFirstValue("anisotropyField") != null) {
                newAnisotropyField = ((VNRegularField) getInputFirstValue("anisotropyField")).getField();

                // Check, whether "inField" and "anisotropyField" are conforming . Should be covered by acceptor mechanism.
                if (!Arrays.equals(newAnisotropyField.getDims(), newInField.getDims())) {
                    VisNow.get().userMessageSend(new UserMessage(this.getApplication().getTitle(), this.getName(), "Input and anisotropy fields are not consistent", "Input and anisotropy fields must have exactly the same dimensions (dims[] array).", Level.ERROR));
                    this.switchPanelToDummy();
                }

                if (anisotropyField != null)
                    isDifferentAnisotropyField = !Arrays.equals(newInField.getDims(), newAnisotropyField.getDims());
            }

            isDifferentAnisotropyInputField = newAnisotropyField != anisotropyInputField;
            anisotropyInputField = newAnisotropyField;

            boolean isDifferentInField = false;

            // Reset due to "dims", if not using "anisotropyField" at all.
            if (inField != null)
                isDifferentInField = !Arrays.equals(inField.getDims(), newInField.getDims());

            // boolean isDifferentField = !isFromVNA() && (inField == null || isDifferentInField || isDifferentAnisotropyField);
            boolean isDifferentField = !isFromVNA() && (isDifferentInField || isDifferentAnisotropyField);

            // Needed by "RunButton" to know, whether to set itself to pending state.
            boolean isNewInputOrAnisotropyField = !isFromVNA() && (newInField != inField || isDifferentAnisotropyField || isDifferentAnisotropyInputField);

            // Needed to reset some parameters.
            boolean isNewInputField = !isFromVNA() && newInField != inField;

            inField = newInField;
            anisotropyField = newAnisotropyField;

            // 2. Validate parameters.
            Parameters p;
            synchronized (parameters) {
                validateParametersAndSetSmart(isDifferentField, isNewInputField);
                // Clone parameters (local read-only copy).
                p = parameters.getReadOnlyClone();
            }

            // 3. Update GUI (GUI doesn't change clonedParameters! Assuming correct set of clonedParameters).
            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewInputOrAnisotropyField);

            RegularField outWeights = null;
            outRegularField = null;

            if (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0); // Can run (-> decreased) in "run dynamically" mode on input attach or new inField data.

                // 4. Run computation and propagate.
                if (p.get(PRESMOOTH))
                    anisotropyField = presmooth(p);

                if (p.get(COMPUTE_WEIGHTS))
                    outWeights = computeWeights(p);

                if (!p.get(COMPUTE_ONLY_WEIGHTS))
                    outRegularField = computeOutField(p);

                // Set both output fields.
                // Main output field.
                if (outRegularField == null)
                    setOutputValue("outField", null);
                else
                    setOutputValue("outField", new VNRegularField(outRegularField));
                // Optional weight field.
                if (outWeights == null)
                    setOutputValue("weights", null);
                else
                    setOutputValue("weights", new VNRegularField(outWeights));

                this.outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        } else
            this.switchPanelToDummy();
    }
}
