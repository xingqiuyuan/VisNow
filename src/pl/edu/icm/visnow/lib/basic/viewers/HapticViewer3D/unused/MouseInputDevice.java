//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.unused;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.media.j3d.InputDevice;
import javax.media.j3d.Sensor;
import javax.media.j3d.Transform3D;
import javax.vecmath.Vector3d;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class MouseInputDevice implements InputDevice
{

    private Sensor sensor;
    private Transform3D transform = new Transform3D();
    private Vector3d transl = new Vector3d();
    private int[] buttons = new int[3];
    private Component parent;
    private MouseListener lm;
    private MouseMotionListener lmm;
    private MouseWheelListener lmw;

    public MouseInputDevice(Component c)
    {
        parent = c;
        c.addMouseListener(lm = new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                setButtons(e);
            }

            @Override
            public void mouseReleased(MouseEvent e)
            {
                setButtons(e);
            }
        });

        c.addMouseMotionListener(lmm = new MouseMotionListener()
        {
            public void mouseDragged(MouseEvent e)
            {
                transl.x = e.getX();
                transl.y = e.getY();
                setButtons(e);
            }

            public void mouseMoved(MouseEvent e)
            {
                transl.x = e.getX();
                transl.y = e.getY();
                setButtons(e);
            }
        });

        c.addMouseWheelListener(lmw = new MouseWheelListener()
        {
            int wheel = 0;

            public void mouseWheelMoved(MouseWheelEvent e)
            {
                wheel += e.getWheelRotation();
                transl.z = wheel;
            }
        });
    }

    private void setButtons(MouseEvent e)
    {
        buttons[0] = ((MouseEvent.BUTTON1_DOWN_MASK & e.getModifiersEx()) != 0) ? 1 : 0;
        buttons[1] = ((MouseEvent.BUTTON2_DOWN_MASK & e.getModifiersEx()) != 0) ? 1 : 0;
        buttons[2] = ((MouseEvent.BUTTON3_DOWN_MASK & e.getModifiersEx()) != 0) ? 1 : 0;
    }

    public boolean initialize()
    {
        sensor = new Sensor(this, 3, 3);

        return true;
    }

    public void setNominalPositionAndOrientation()
    {
    }

    public void pollAndProcessInput()
    {
        transform.setTranslation(transl);
        sensor.setNextSensorRead(BLOCKING, transform, buttons);
    }

    public void processStreamInput()
    {
    }

    public void close()
    {
        parent.removeMouseListener(lm);
        parent.removeMouseMotionListener(lmm);
        parent.removeMouseWheelListener(lmw);
    }

    public int getProcessingMode()
    {
        return InputDevice.DEMAND_DRIVEN;
    }

    public void setProcessingMode(int mode)
    {
    }

    public int getSensorCount()
    {
        return 1;
    }

    public Sensor getSensor(int sensorIndex)
    {
        if (sensorIndex != 0) {
            throw new IndexOutOfBoundsException("Sensor index " + sensorIndex + " too large");
        }
        return sensor;
    }
}
