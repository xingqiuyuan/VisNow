//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui;

import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.Vector;
import javax.media.j3d.Transform3D;
import javax.media.j3d.View;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.LinkFace;
import pl.edu.icm.visnow.engine.core.ModuleCore;
import pl.edu.icm.visnow.engine.core.VNData;
import pl.edu.icm.visnow.geometries.objects.GeometryObject;
import pl.edu.icm.visnow.geometries.objects.RegularField3DOutline;
import pl.edu.icm.visnow.geometries.viewer3d.Display3DFrame;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.ILocalToVworldGetter;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.ILocalToVworldListener;
import pl.edu.icm.visnow.lib.types.VNGeometryObject;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.devices.DeviceName;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.DeviceWarningsHandler;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.controller.InputDevicePointerController;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.InputFields;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.RegularFld_DA;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.haptic_forces.DeviceContext;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.devices.DeviceManager;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
 */
public class HapticViewer3D
    extends ModuleCore
    implements DeviceWarningsHandler, ILocalToVworldGetter
{

    static final org.apache.log4j.Logger LOGGER
        = org.apache.log4j.Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    /**
     * "Emergency brake" for haptic forces. It attaches directly to the
     * <code>KeyboardFocusManager</code>, intercepts all Space and Enter presses typed in any window
     * and disables all forces in all ForceContexts in all devices in all HapticViewers.
     */
    private static KeyEventDispatcher emergencyDispatcher = null;
    /**
     * Counts number of HapticViewers.
     * Use instancesLock to read or modify this variable.
     */
    private static Integer instanceCounter = 0;
    /**
     * Used when accessing instanceCounter, emergencyDispatcher and emergencyCreated.
     */
    private static final Object instancesLock = new Object();
    //
    protected Display3DFrame window;
    private GUI ui;
    /**
     * Helper object managing state of devices and registering/unregistering usage.
     */
    protected ViewerDeviceManager viewerDeviceManager;
    /**
     * Components from data input that could be used to generate haptic forces.
     * HapticViewer3D owns this variable.
     */
    private InputFields fields = new InputFields();
    /**
     * handles ILocalToVworldListener listeners
     */
    LocalToVWorldSupport support = new LocalToVWorldSupport();

    /**
     * Creates a new instance of HapticViewer3D
     */
    public HapticViewer3D()
    {
        synchronized (instancesLock) {
            ++instanceCounter;
        }

        final HapticViewer3D viewer = this;

        viewerDeviceManager = new ViewerDeviceManager(this);

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                window = new Display3DFrame();
                window.setTitle("VisNow HapticViewer3D");

                /**
                 * DeviceManager.initializeDevicesOnce() should be called BEFORE new GUI, because
                 * otherwise panel with buttons for
                 * enabling devices will be created before initializing devices and all devices in
                 * the first created haptic viewer will be sorted in the order of initializing them
                 * instead of order set in Device.getDevicesNamesList()
                 */
                DeviceManager.initializeDevicesOnce(viewer);

                ui = new GUI(viewer);

                viewerDeviceManager.initialize(window);

                ui.addChangeListener(new ChangeListener()
                {
                    @Override
                    public void stateChanged(ChangeEvent evt)
                    {
                        window.setVisible(true);
                        window.setExtendedState(Frame.NORMAL);
                    }
                });

                window.setVisible(true);
            }
        });

        setPanel(ui); // this setPanel can be here, because swingRunAndWait() is synchronous

        initEmergencyForceSwitch();

    }

    @Override
    public boolean isViewer()
    {
        return true;
    }

    /**
     * Adds emergency disabling all haptic forces when user presses Enter or Space.
     * <p/>
     * NOTE: It attaches directly to the KeyboardFocusManager to do that (will intercept every Space
     * or Enter in any Visnow's window).
     * <p/>
     * No matter how many times the method will be called, the handler will be set only once.
     */
    private void initEmergencyForceSwitch()
    {
        if (emergencyDispatcher == null) {

            KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            assert emergencyDispatcher == null;

            emergencyDispatcher = new KeyEventDispatcher()
            {
                final org.apache.log4j.Logger LOGGER
                    = org.apache.log4j.Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());

                @Override
                public boolean dispatchKeyEvent(KeyEvent e)
                {

                    /* Space or Enter pressed - emergency disable all forces */
                    if (e.getID() == KeyEvent.KEY_PRESSED &&
                        (e.getKeyCode() == KeyEvent.VK_SPACE ||
                        e.getKeyCode() == KeyEvent.VK_ENTER)) {
                        LOGGER.error("Pressed emergency key - disabling all forces!");
                        DeviceContext.emergencyDisableForces();
                    }
                    return false;
                }
            };

            manager.addKeyEventDispatcher(emergencyDispatcher);

        }
    }

    /**
     * Prepare lists of scalar and vector components that can be used to generate forces (1D and
     * 3D components of a regular field).
     * <p/>
     * Call {@link #getFields()} to get them.
     * <p/>
     * @param inputFields fields attached to the hapticField data port (blue)
     */
    public void setInputFieldsData(Vector inputFields)
    {
        fields.vectorFields.clear();
        fields.scalarFields.clear();

        Iterator it = inputFields.iterator();
        while (it.hasNext()) {
            VNRegularField f = (VNRegularField) it.next();
            if (f != null) {
                RegularField field = f.getField();
                if (field != null && field.getDims().length == 3) {
                    for (DataArray da : field.getComponents()) {
                        if (da.getVectorLength() == 1) {
                            fields.scalarFields.add(new RegularFld_DA(field, da));
                        } else if (da.getVectorLength() == 3) {
                            fields.vectorFields.add(new RegularFld_DA(field, da));
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns fields that can be used for haptic forces (1D and 3D components)
     */
    public final InputFields getFields()
    {
        return fields;
    }

    public static InputEgg[] getInputEggs()
    {
        return new InputEgg[]{new InputEgg("inObject", VNGeometryObject.class,
                                           InputEgg.TRIGGERING | InputEgg.NECESSARY, 0, -1, new VNGeometryObject()),
                              new InputEgg("hapticField", VNRegularField.class,
                                           InputEgg.TRIGGERING, 0, 1, null)};
    }

    @Override
    public void onDelete()
    {

        /* uninitialize ViewerDeviceManager (unregister all used devices) */
        viewerDeviceManager.onDelete();

        /* uninitialize GUI */
        ui.onDelete();

        window.getDisplayPanel().clearCanvas();
        if (window.getDisplayPanel().getControlsFrame() != null) {
            window.getDisplayPanel().getControlsFrame().dispose();
        }
        if (window.getDisplayPanel().getTransientControlsFrame() != null) {
            window.getDisplayPanel().getTransientControlsFrame().dispose();
        }
        window.dispose();

        synchronized (instancesLock) {
            // if we are the last instance of HapticViewer3D, deactivate emergency key
            if (instanceCounter == 1 && emergencyDispatcher != null) {

                KeyboardFocusManager manager
                    = KeyboardFocusManager.getCurrentKeyboardFocusManager();

                manager.removeKeyEventDispatcher(emergencyDispatcher);
                emergencyDispatcher = null;
            }
            --instanceCounter;
        }

    }

    @Override
    public void onInputDetach(LinkFace link)
    {
        onActive();
    }

    @Override
    public void onInputAttach(LinkFace link)
    {
        onActive();
    }

    /* TODO BUG: when data components change (e.g. after reading a different file in VisNow field reader) 
     * forces that depend on components remain the same (are based on old values).
     * There are two reasons:
     *  1) Component removed: There is no mechanism of notifying ForceContext that a component has disappeared.
     *  2) Component values changed: Each force depending on data component stores a local copy of its data 
     *     (that workaround for FieldInterpolator was suggested by Babor). This means that changes 
     *     in values will not reflect data used by forces.
     */
    @Override
    public void onActive()
    {
        if (!window.isVisible())
            window.setVisible(true);
        window.getDisplayPanel().clearAllGeometry();

        // add haptic outline if applicable
        updateHapticFieldOutline();

        // add geometry objects to be displayed (red links)
        Vector ins = getInputValues("inObject");
        for (Object obj : ins) {
            if ((VNGeometryObject) obj != null &&
                ((VNGeometryObject) obj).getGeometryObject() != null) {
                window.getDisplayPanel().addChild(
                    ((VNGeometryObject) obj).getGeometryObject());
            }
        }

        window.repaint();

        Vector<Object> values = getInputValues("hapticField");
        setInputFieldsData(values);
    }

    public View getView()
    {
        return window.getDisplayPanel().getCanvas().getView();
    }

    // <editor-fold desc=" Adapter methods for handling registering and unregistering devices "> 
    /**
     * Register using a device by this HapticViewer3D. Calls
     * {@link ViewerDeviceManager#registerDevice} which will handle registration.
     */
    void registerDevice(String deviceName)
    {
        viewerDeviceManager.registerDevice(deviceName);
    }

    /**
     * Unregister using a device by this HapticViewer3D. Calls
     * {@link ViewerDeviceManager#registerDevice} which will handle unregistration.
     */
    void unregisterDevice(String deviceName)
    {
        viewerDeviceManager.unregisterDevice(deviceName);
    }

    void createDeviceTab(InputDevicePointerController controller)
    {
        ui.createDeviceTab(controller);
    }

    void removeDeviceTab(InputDevicePointerController controller)
    {
        ui.removeDeviceTab(controller);
    }

    // </editor-fold>  
    void addRegisteredDevicesListener(ViewerDeviceManager.IDeviceListener l)
    {
        viewerDeviceManager.addRegisteredDevicesListener(l);
    }

    void removeRegisteredDevicesListener(ViewerDeviceManager.IDeviceListener l)
    {
        viewerDeviceManager.removeRegisteredDevicesListener(l);
    }

    @Override
    public void handleDeviceWarning(DeviceName deviceRef, String message)
    {
        JOptionPane.showMessageDialog(
            ui, message,
            "Warning while initializing " +
            deviceRef.getFriendlyName() +
            " (" + deviceRef.getName() + ")",
            JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public boolean handleDeviceWarningOkCancel(DeviceName deviceRef, String message)
    {
        String title = "Warning while initializing " +
            deviceRef.getFriendlyName() +
            " (" + deviceRef.getName() + ")";

        int option = JOptionPane.showOptionDialog(
            ui, message, title,
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
            null, null, null);

        if (option == JOptionPane.CLOSED_OPTION || option == JOptionPane.CANCEL_OPTION)
            return false;
        else
            return true;
    }

    // === haptic outline box ===
    /**
     *
     * @author Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of Warsaw, 2013
     */
    public class HapticViewerOutlineGeometryObject extends GeometryObject
    {

        public HapticViewerOutlineGeometryObject(String name)
        {
            super(name);
        }
    }

    HapticViewerOutlineGeometryObject forceOutlineGeom = null;
    /**
     * Outline showing where haptic forces are working (size of the field) and providing
     * localToVWorld transformation matrix
     */
    RegularField3DOutline forceOutline = null;
    int forcesActive = 0;

    /**
     * Set
     * <code>localToVWorld</code> to the transform of the haptic outline object. The object must
     * have been already attached to the Java3D tree.
     * <p/>
     * @param out_localToVWorld local to vworld transform (out param)
     */
    @Override
    public void getLocalToVworld(Transform3D out_localToVWorld) throws ILocalToVworldGetter.NoDataException
    {
        // check whether the force outline is attached to screen (race condition)
        if (!forceOutlineGeom.getGeometryObj().isLive())
            throw new ILocalToVworldGetter.NoDataException();

        forceOutlineGeom.getGeometryObj().getLocalToVworld(out_localToVWorld);
    }

    /**
     * Checks whether haptic outline is being displayed and should disappear or isn't being
     * displayed, but it should, and updates its state.
     */
    protected void updateHapticFieldOutline()
    {

        RegularField hapticField = null;
        VNData data = getInputFirstData("hapticField");
        /* 
         * data == null - unlinked port, 
         * data.getValue() == null - empty data
         */

        if (data != null) {
            VNRegularField vnField = (VNRegularField) data.getValue();
            if (vnField != null)
                hapticField = vnField.getField();
        }

        boolean hapticInputDataAvailable = (hapticField != null);

        /* 1) If haptic outline was created, but data is unavailable (data port was just detached), 
         *      it should now be destroyed -> remove outline from the scene.
         * 2) If haptic outline was created and data is available (port is attached, data is present),
         *      check whether outline is attached to scene. It may be not if the whole geometry has
         *      just been removed in onActive(). We will check forceOutlineGeom.getCurrentViewer() == null
         *      to get to know that. Then attach the outline.
         * 3) If haptic outline wasn't created and data is available, create outline (and attach to the scene).
         */
        if (isHapticFieldOutlineCreated()) {
            if (!hapticInputDataAvailable)
                removeHapticFieldOutline(); // 1)
            else {
                if (forceOutlineGeom.getCurrentViewer() == null) // 2) 
                    attachHapticFieldOutline();
            }
        } else {
            if (hapticInputDataAvailable)
                createHapticFieldOutline(hapticField); // 3)
        }
    }

    protected boolean isHapticFieldOutlineCreated()
    {
        return (forceOutline != null);
    }

    /**
     * Creates a bounding box showing where haptic forces will be generated.
     * Fired when data input was attached.
     */
    protected void createHapticFieldOutline(RegularField hapticField)
    {
        if (forceOutline != null) // rather should not happen, but just in case
            removeHapticFieldOutline();

        //TODO: consider synchronizing forceOutline and forceOutlineGeom somehow
        forceOutline = new RegularField3DOutline();
        forceOutline.setField(hapticField);
        forceOutlineGeom = new HapticViewerOutlineGeometryObject("force outline");
        forceOutlineGeom.addNode(forceOutline);

        attachHapticFieldOutline();
    }

    /**
     * If outline was created, attaches it to the scene and notifies listeners.
     * <p/>
     * Called in {@link #createHapticFieldOutline} and in {@link #onActive} (when redrawing all
     * geometry).
     * <p/>
     */
    protected void attachHapticFieldOutline()
    {
        if (isHapticFieldOutlineCreated()) {
            window.getDisplayPanel().addChild(forceOutlineGeom);
            support.notifyListeners(true);
        }
    }

    /**
     * Removes bounding box from viewer and sets properly all variables linked with it.
     * <p/>
     * NOTE: Should be fired when data input was detached and <i>before clearing all geometry</i>.
     */
    protected void removeHapticFieldOutline()
    {
        window.getDisplayPanel().removeChild(forceOutlineGeom);

        /* Do NOT set forceOutlineGeom to null as it may be just used by another thread in getLocalToVworld 
         * (even after callback to listeners, because by the time the callback was called, the listener 
         * could have already enter the method :] ).  
         * */
        forceOutline = null;

        support.notifyListeners(false);
    }

    // === ILocalToVworldListener support ===
    @Override
    public void addLocalToVworldChangeListener(ILocalToVworldListener listener)
    {
        support.addListener(listener, isLocalToVworldSet());
    }

    @Override
    public void removeLocalToVworldChangeListener(ILocalToVworldListener listener)
    {
        support.removeListener(listener);
    }

    public boolean isLocalToVworldSet()
    {
        return isHapticFieldOutlineCreated();
    }
    
    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (window != null)
            window.setTitle("VisNow HapticViewer3D - " + this.getApplication().getTitle() + " - " + this.getName());
    }
    
}
// revised.
