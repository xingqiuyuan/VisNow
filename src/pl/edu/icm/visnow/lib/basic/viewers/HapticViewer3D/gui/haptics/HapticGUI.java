//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.haptics;

import java.awt.Color;
import java.awt.Window;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.vecmath.Vector3f;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.controller.HapticPointerController;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.controller.InputDevicePointerController;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.CenterableJDialog;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.haptics.forces_panels.AbstractEditPanel;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.haptics.forces_panels.NewForceDialog;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.devices.haptics.HapticException;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.haptic_forces.IForceGetter;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.devices.haptics.IHapticReadOnlyDevice;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForce;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.model.haptic_forces.forces.IForceListModel;
import pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.DeviceGUITab;
import static org.apache.commons.math3.util.FastMath.*;

/**
 *
 * Displays a tab with controls for enabling haptic force, adding variety of forces etc. A similar
 * class should be created for other types of devices (e.g. tablet).
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 * @author modified by Łukasz Czerwiński <czerwinskilukasz1 [#] gmail.com>, ICM, University of
 * Warsaw, 2013
 */
public class HapticGUI
    extends DeviceGUITab
    implements IForceListController
{

    private HapticPointerController controller = null;
    private UpdateHapticTabThread forceMagnitudeThread;
    //
    private boolean updatingFromGUI = false;
    static final org.apache.log4j.Logger LOGGER
        = org.apache.log4j.Logger.getLogger(new Throwable().getStackTrace()[0].getClassName());
    //
    /**
     * Lock for all modifications of GUI.
     */
    private final Object GUILock = new Object();
    //
    /**
     * For read by HapticGUI, for write by UpdateHapticTabThread
     */
    private boolean lastWasForceEnabled;
    /**
     * For read by HapticGUI, for write by UpdateHapticTabThread
     */
    private boolean lastWasForceDataBlocked;
    /**
     * For read by HapticGUI, for write by UpdateHapticTabThread
     */
    private boolean lastWasForceDragBlocked;
    /**
     * For read by HapticGUI, for write by UpdateHapticTabThread
     */
    private boolean lastWasForceClamped;
    //
    private ListDataListener listDataListener = null;

    /**
     * Creates new form HapticGUI. Only for GUI Builder.
     *
     * @deprecated
     */
    public HapticGUI()
    {
        initComponents();
    }

    public HapticGUI(InputDevicePointerController deviceController) throws HapticException
    {

        initComponents();

        HapticPointerController hapticController = (HapticPointerController) deviceController;
        this.controller = hapticController;

        IForceGetter forceGetter = controller.getForceGetter();
        scaleSlider.setVal(forceGetter.getForceScale());
        clampSlider.setVal(forceGetter.getForceClamp());
        clampSlider.setMax((float) hapticController.getDevice().getDeviceNominalMaxContinuousForce());

        // no need to update force button here, 
        // it will be updated in this thread in the UpdateHapticTabThread's constructor
        initForcesList();
        initForceDisplayThread();
    }

    /**
     * Called in constructor.
     */
    private void initForcesList()
    {
        IForceListModel model = controller.getForceListModel();
        forcesList.setModel(model);
        forcesList.initMouseHandling(this);

        listDataListener = new ListDataListener()
        {
            @Override
            public void intervalAdded(ListDataEvent e)
            {
                updateForceButtonsState();
            }

            @Override
            public void intervalRemoved(ListDataEvent e)
            {
                updateForceButtonsState();
            }

            @Override
            public void contentsChanged(ListDataEvent e)
            {
                updateForceButtonsState();
            }
        };
        model.addListDataListener(listDataListener);

        forcesList.addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                updateForceButtonsState();
            }
        });

    }

    /**
     * Create and start {@link UpdateHapticTabThread} - it will update GUI: value of the force and
     * state of ForceContext (it will update the toggle button used to enable/disable force output)
     */
    private void initForceDisplayThread()
    {

        forceOutputTBActionPerformed(null);

        forceMagnitudeThread = new UpdateHapticTabThread(controller.getDevice(),
                                                         controller.getForceGetter());
        forceMagnitudeThread.start();

        /* Now update buttons' state below the force list */
        updateForceButtonsState();
    }

    /**
     * Notifies HapticGUI that forceContext was unset in the controller, so
     * thread showing haptics' position and force should be ended. The thread will end after waking
     * from sleep(100), so not immediately.
     */
    public void endForceDisplayThread()
    {
        if (forceMagnitudeThread != null) {
            forceMagnitudeThread.end();
            forceMagnitudeThread = null;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelForceMagnitude = new javax.swing.JPanel();
        forceMagnitudeBar = new javax.swing.JProgressBar();
        forceMagnitudeLabel = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        forceOutputTB = new javax.swing.JToggleButton();
        jPanelForces = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        editButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        disableButton = new javax.swing.JButton();
        duplicateButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        forcesList = new pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.haptics.ForcesList();
        jPanelForceParams = new javax.swing.JPanel();
        clampSlider = new pl.edu.icm.visnow.gui.widgets.FloatSlider();
        scaleSlider = new pl.edu.icm.visnow.gui.widgets.FloatSlider();
        jPanelUpdateRate = new javax.swing.JPanel();
        hapticPosTitle3 = new javax.swing.JLabel();
        meanUpdateRate = new javax.swing.JLabel();
        jPanelPosVel = new javax.swing.JPanel();
        hapticPosTitle = new javax.swing.JLabel();
        hapticVelTitle = new javax.swing.JLabel();
        hapticPosXLabel = new javax.swing.JLabel();
        hapticVelXLabel = new javax.swing.JLabel();
        hapticPosYLabel = new javax.swing.JLabel();
        hapticVelYLabel = new javax.swing.JLabel();
        hapticPosZLabel = new javax.swing.JLabel();
        hapticVelZLabel = new javax.swing.JLabel();
        jLabelDummy1 = new javax.swing.JLabel();
        hapticVelValue = new javax.swing.JLabel();
        jPanelResizingGap = new javax.swing.JPanel();

        setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        setMinimumSize(new java.awt.Dimension(200, 800));
        setPreferredSize(new java.awt.Dimension(200, 800));
        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.PAGE_AXIS));

        jPanelForceMagnitude.setBorder(javax.swing.BorderFactory.createTitledBorder("Current force magnitude"));
        jPanelForceMagnitude.setMaximumSize(new java.awt.Dimension(250, 250));
        jPanelForceMagnitude.setPreferredSize(new java.awt.Dimension(208, 120));
        jPanelForceMagnitude.setRequestFocusEnabled(false);

        forceMagnitudeLabel.setText("Force in Newtons");

        forceOutputTB.setText("Force output disabled");
        forceOutputTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forceOutputTBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(forceOutputTB)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(forceOutputTB)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

        javax.swing.GroupLayout jPanelForceMagnitudeLayout = new javax.swing.GroupLayout(jPanelForceMagnitude);
        jPanelForceMagnitude.setLayout(jPanelForceMagnitudeLayout);
        jPanelForceMagnitudeLayout.setHorizontalGroup(
                jPanelForceMagnitudeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelForceMagnitudeLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelForceMagnitudeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(forceMagnitudeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(forceMagnitudeBar, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
                );
        jPanelForceMagnitudeLayout.setVerticalGroup(
                jPanelForceMagnitudeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelForceMagnitudeLayout.createSequentialGroup()
                                .addComponent(forceMagnitudeBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(forceMagnitudeLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                );

        add(jPanelForceMagnitude);

        jPanelForces.setBorder(javax.swing.BorderFactory.createTitledBorder("Forces"));
        jPanelForces.setMaximumSize(new java.awt.Dimension(250, 320));
        jPanelForces.setMinimumSize(new java.awt.Dimension(200, 280));
        jPanelForces.setPreferredSize(new java.awt.Dimension(200, 320));

        addButton.setText("Add");
        addButton.setPreferredSize(new java.awt.Dimension(45, 23));
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        editButton.setText("Edit");
        editButton.setMaximumSize(addButton.getMaximumSize());
        editButton.setMinimumSize(addButton.getMinimumSize());
        editButton.setOpaque(false);
        editButton.setPreferredSize(new java.awt.Dimension(45, 23));
        editButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editButtonActionPerformed(evt);
            }
        });

        removeButton.setText("Remove");
        removeButton.setMaximumSize(addButton.getMaximumSize());
        removeButton.setMinimumSize(addButton.getMinimumSize());
        removeButton.setPreferredSize(new java.awt.Dimension(45, 23));
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });

        disableButton.setText("Disable");
        disableButton.setMaximumSize(addButton.getMaximumSize());
        disableButton.setMinimumSize(addButton.getMinimumSize());
        disableButton.setPreferredSize(new java.awt.Dimension(45, 23));
        disableButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                disableButtonActionPerformed(evt);
            }
        });

        duplicateButton.setText("Duplicate");
        duplicateButton.setMaximumSize(addButton.getMaximumSize());
        duplicateButton.setMinimumSize(addButton.getMinimumSize());
        duplicateButton.setOpaque(false);
        duplicateButton.setPreferredSize(new java.awt.Dimension(45, 23));
        duplicateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                duplicateButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(forcesList);

        javax.swing.GroupLayout jPanelForcesLayout = new javax.swing.GroupLayout(jPanelForces);
        jPanelForces.setLayout(jPanelForcesLayout);
        jPanelForcesLayout.setHorizontalGroup(
                jPanelForcesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelForcesLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelForcesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addGroup(jPanelForcesLayout.createSequentialGroup()
                                                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelForcesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(editButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(removeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                                        .addComponent(duplicateButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(disableButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addContainerGap())
                );
        jPanelForcesLayout.setVerticalGroup(
                jPanelForcesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelForcesLayout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(8, 8, 8)
                                .addGroup(jPanelForcesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanelForcesLayout.createSequentialGroup()
                                                .addComponent(editButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(disableButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(removeButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(duplicateButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(2, Short.MAX_VALUE))
                );

        add(jPanelForces);

        jPanelForceParams.setBorder(javax.swing.BorderFactory.createTitledBorder("Forces parameters"));
        jPanelForceParams.setMaximumSize(new java.awt.Dimension(250, 500));
        jPanelForceParams.setPreferredSize(new java.awt.Dimension(150, 200));

        clampSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Clamp"));
        clampSlider.setMax(10.0F);
        clampSlider.setPreferredSize(new java.awt.Dimension(200, 75));
        clampSlider.setVal(2.0F);
        clampSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                clampSliderStateChanged(evt);
            }
        });

        scaleSlider.setBorder(javax.swing.BorderFactory.createTitledBorder("Scale"));
        scaleSlider.setMax(2.0F);
        scaleSlider.setPreferredSize(new java.awt.Dimension(200, 75));
        scaleSlider.setVal(1.0F);
        scaleSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                scaleSliderStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanelForceParamsLayout = new javax.swing.GroupLayout(jPanelForceParams);
        jPanelForceParams.setLayout(jPanelForceParamsLayout);
        jPanelForceParamsLayout.setHorizontalGroup(
                jPanelForceParamsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelForceParamsLayout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addGroup(jPanelForceParamsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(scaleSlider, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                                        .addComponent(clampSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                );
        jPanelForceParamsLayout.setVerticalGroup(
                jPanelForceParamsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelForceParamsLayout.createSequentialGroup()
                                .addComponent(clampSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scaleSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

        add(jPanelForceParams);

        jPanelUpdateRate.setBorder(javax.swing.BorderFactory.createTitledBorder("Update rate"));
        jPanelUpdateRate.setMaximumSize(new java.awt.Dimension(250, 250));
        jPanelUpdateRate.setPreferredSize(new java.awt.Dimension(237, 70));

        hapticPosTitle3.setText("Mean update rate:");
        hapticPosTitle3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticPosTitle3.setMaximumSize(new java.awt.Dimension(144, 16));
        hapticPosTitle3.setMinimumSize(new java.awt.Dimension(144, 16));
        hapticPosTitle3.setPreferredSize(new java.awt.Dimension(144, 16));

        meanUpdateRate.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        meanUpdateRate.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));

        javax.swing.GroupLayout jPanelUpdateRateLayout = new javax.swing.GroupLayout(jPanelUpdateRate);
        jPanelUpdateRate.setLayout(jPanelUpdateRateLayout);
        jPanelUpdateRateLayout.setHorizontalGroup(
                jPanelUpdateRateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelUpdateRateLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(hapticPosTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(meanUpdateRate, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                );
        jPanelUpdateRateLayout.setVerticalGroup(
                jPanelUpdateRateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelUpdateRateLayout.createSequentialGroup()
                                .addGroup(jPanelUpdateRateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(hapticPosTitle3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(meanUpdateRate, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

        add(jPanelUpdateRate);

        jPanelPosVel.setBorder(javax.swing.BorderFactory.createTitledBorder("In device coordinate system (TRACKER)"));
        jPanelPosVel.setMaximumSize(new java.awt.Dimension(250, 250));
        jPanelPosVel.setPreferredSize(new java.awt.Dimension(208, 110));
        jPanelPosVel.setLayout(new java.awt.GridLayout(5, 2));

        hapticPosTitle.setText("Haptic position:");
        hapticPosTitle.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticPosTitle.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticPosTitle);

        hapticVelTitle.setText("Haptic velocity:");
        hapticVelTitle.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticVelTitle.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticVelTitle);

        hapticPosXLabel.setText("pos x");
        hapticPosXLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticPosXLabel.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticPosXLabel.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticPosXLabel);

        hapticVelXLabel.setText("vel x");
        hapticVelXLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticVelXLabel.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticVelXLabel.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticVelXLabel);

        hapticPosYLabel.setText("pos y");
        hapticPosYLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticPosYLabel.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticPosYLabel.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticPosYLabel);

        hapticVelYLabel.setText("vel y");
        hapticVelYLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticVelYLabel.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticVelYLabel.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticVelYLabel);

        hapticPosZLabel.setText("pos z");
        hapticPosZLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticPosZLabel.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticPosZLabel.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticPosZLabel);

        hapticVelZLabel.setText("vel z");
        hapticVelZLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticVelZLabel.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticVelZLabel.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticVelZLabel);

        jLabelDummy1.setMaximumSize(new java.awt.Dimension(50, 14));
        jLabelDummy1.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(jLabelDummy1);

        hapticVelValue.setText("vel value");
        hapticVelValue.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 5, 1, 5));
        hapticVelValue.setMaximumSize(new java.awt.Dimension(50, 14));
        hapticVelValue.setPreferredSize(new java.awt.Dimension(85, 8));
        jPanelPosVel.add(hapticVelValue);

        add(jPanelPosVel);

        jPanelResizingGap.setPreferredSize(new java.awt.Dimension(32767, 32767));

        javax.swing.GroupLayout jPanelResizingGapLayout = new javax.swing.GroupLayout(jPanelResizingGap);
        jPanelResizingGap.setLayout(jPanelResizingGapLayout);
        jPanelResizingGapLayout.setHorizontalGroup(
                jPanelResizingGapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 200, Short.MAX_VALUE)
                );
        jPanelResizingGapLayout.setVerticalGroup(
                jPanelResizingGapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 35, Short.MAX_VALUE)
                );

        add(jPanelResizingGap);
    }// </editor-fold>//GEN-END:initComponents

    // ======== IForceListController ===========
    @Override
    public void showNewForceDialog()
    {

        Window a = (Window) getTopLevelAncestor();
        NewForceDialog nfDlg = new NewForceDialog(a,
                                                  controller.getParams());

        // set dialog's position to be centered on list of forces
        nfDlg.centerOn(forcesList);

        // set visible and wait for force inputed
        nfDlg.setVisible(true); // this call makes the thread to block 
        // and wait for closing the dialog
        IForce force = nfDlg.getForce();
        if (force != null) {
            forcesList.addForce(force);
        }
    }

    @Override
    public void showEditDialog(IForce editedForce)
    {

        if (!editedForce.canBeChanged())
            return;

        IForce newForce = editedForce.clone();

        AbstractEditPanel editPanel;
        try {
            editPanel = AbstractEditPanel.createPanel(newForce, controller.getParams());
        } catch (UnsupportedOperationException ex) {
            LOGGER.error(String.format("Unknown type of force: {0}", newForce.getClass()));
            JOptionPane.showMessageDialog(this,
                                          "Wewnętrzny błąd aplikacji.",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        Window a = (Window) getTopLevelAncestor();
        CenterableJDialog dialog = new CenterableJDialog(a, "Edit force parameters");
        dialog.setModal(true);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

        dialog.setContentPane(editPanel);
        dialog.pack();
        dialog.centerOn(forcesList);

        dialog.setVisible(true); // this blocks the thread and waits for closing the dialog window

        newForce = editPanel.getForce();
        if (newForce != null) {
            forcesList.setForce(editedForce, newForce);
        }
    }

    // ========== end of IForceListController =======================
    //
    // ==== colors for bar showing force value ===
    //
    static final Color magnitudeBarColorActive = new Color(49, 59, 197);
    static final Color magnitudeBarColorInactive = new Color(92, 123, 175);
    static final Color magnitudeBarColorClamped = new Color(255, 100, 0);
    //
    // ==== colors for label showing force value ===
    static final Color magnitudeLabelColorActive = Color.black;
    static final Color magnitudeLabelColorInactive = Color.gray;
    static final Color magnitudeLabelColorClamped = magnitudeBarColorClamped;
    //
    // ==== colors for label showing update rate (should be around 1000Hz) ===
    static final Color meanUpdateRateColorOK = new Color(0, 140, 0);
    static final Color meanUpdateRateColorWarning = new Color(250, 150, 0);
    static final Color meanUpdateRateColorProblem = new Color(255, 0, 0);

    // ==== colors for force output toggle button
    static final Color forceButtonColorForcesEnabled = new Color(0, 140, 0);
    static final Color forceButtonColorForcesDisabled = new Color(250, 150, 0);

    //    static final Color forceButtonColorForcesBlocked = new Color(255, 0, 0);
    /**
     * This thread maintains refreshing values of haptic parameters (force output enabled/disabled,
     * haptic position and velocity, update rate). Refresh is done periodically
     * - there is a wait(100) function call in run().
     */
    private class UpdateHapticTabThread extends Thread
    {

        private boolean running = true;
        private final IHapticReadOnlyDevice hapticDevice;
        private final IForceGetter forceGetter;
        public static final int UPDATE_RATE_OK = 995; // in Phantom Test those values are: 1000 and 950, here intentionally slightly lower
        public static final int UPDATE_RATE_PROBLEM = 950;

        private UpdateHapticTabThread(IHapticReadOnlyDevice hapticDevice,
                                      IForceGetter forceGetter)
        {
            if (hapticDevice == null) {
                throw new IllegalArgumentException("IHapticGetPointer cannot be null");
            }
            if (forceGetter == null) {
                throw new IllegalArgumentException("IForceGetter cannot be null");
            }
            setPriority(Thread.MIN_PRIORITY);
            this.hapticDevice = hapticDevice;
            this.forceGetter = forceGetter;

            updateForceDisplayGUI(true);
        }

        private void updateForceDisplayGUI(boolean forceUpdate)
        {
            boolean isForceDataBlocked = forceGetter.isForceOutputDataBlocked();
            boolean isForceDragBlocked = forceGetter.isForceOutputDragBlocked();
            boolean isForceEnabled = forceGetter.isForceOutputEnabled();
            boolean wasForceClamped = forceGetter.wasLastForceClamped();
            boolean update = forceUpdate ? true
                : (isForceEnabled != lastWasForceEnabled ||
                isForceDataBlocked != lastWasForceDataBlocked ||
                isForceDragBlocked != lastWasForceDragBlocked ||
                wasForceClamped != lastWasForceClamped);
            if (update) {
                updateGUIForces(isForceDataBlocked, isForceDragBlocked,
                                isForceEnabled, wasForceClamped);
                lastWasForceEnabled = isForceEnabled;
                lastWasForceDataBlocked = isForceDataBlocked;
                lastWasForceDragBlocked = isForceDragBlocked;
                lastWasForceClamped = wasForceClamped;
            }
        }

        @Override
        public void run()
        {
            while (running) {

                /* --- Get and display force --- */
                Vector3f force = new Vector3f();
                forceGetter.getLastComputedForce(force);

                /* -- round force value to 4 decimal places -- */
                BigDecimal forceValue = new BigDecimal(force.length());
                forceValue = forceValue.setScale(4, RoundingMode.HALF_UP);

                /* -- display force value (text and progress bar) -- */
                forceMagnitudeBar.setValue((int) round(forceValue.doubleValue() / forceGetter.getForceClamp() * 100));
                forceMagnitudeLabel.setText(String.format("Force value: " + forceValue));

                /* --- Update "Force enabled" push button and colors of a force label and force bar --- */
                updateForceDisplayGUI(false);

                /* --- Get and display position --- */
                Vector3f pos = new Vector3f();
                hapticDevice.getPosition(pos);

                hapticPosXLabel.setText(String.format("X: %.5f", pos.getX()));
                hapticPosYLabel.setText(String.format("Y: %.5f", pos.getY()));
                hapticPosZLabel.setText(String.format("Z: %.5f", pos.getZ()));

                /* --- Get and display velocity --- */
                Vector3f vel = new Vector3f();
                hapticDevice.getVelocity(vel);

                hapticVelXLabel.setText(String.format("X: %.5f", vel.getX()));
                hapticVelYLabel.setText(String.format("Y: %.5f", vel.getY()));
                hapticVelZLabel.setText(String.format("Z: %.5f", vel.getZ()));
                hapticVelValue.setText(String.format("value: %.5f", vel.length()));

                /* --- Get and display update rate --- */
                int meanUpdateRateValue = hapticDevice.getPositionMeanUpdateRate();
                meanUpdateRate.setText(String.format("%d Hz", meanUpdateRateValue));
                if (meanUpdateRateValue >= UPDATE_RATE_OK)
                    meanUpdateRate.setForeground(meanUpdateRateColorOK);
                else {
                    if (meanUpdateRateValue > UPDATE_RATE_PROBLEM)
                        meanUpdateRate.setForeground(meanUpdateRateColorWarning);
                    else
                        meanUpdateRate.setForeground(meanUpdateRateColorProblem);
                }

                /* --- wait 100 ms --- */
                synchronized (this) {
                    try {
                        wait(100);
                    } catch (InterruptedException ex) {
                    }
                }

            }
            forceMagnitudeBar.setValue(0);
        }

        private void end()
        {
            running = false;
        }
    }

    /**
     * Enables and disables force output
     *
     * @param evt unused
     */
    private void forceOutputTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forceOutputTBActionPerformed
        boolean enableForces = forceOutputTB.isSelected();
        controller.setForcesEnabled(enableForces);
    }//GEN-LAST:event_forceOutputTBActionPerformed

    /**
     * Updates toggle button, force value label and force value bar.
     * <p/>
     * @param dataBlocked are forces blocked because of lack of needed data
     * @param dragBlocked are forces blocked because of draging the 3D scene
     * @param enabled     are forces enabled
     * @param clamped     were forces clamped
     */
    private void updateGUIForces(boolean dataBlocked, boolean dragBlocked,
                                 boolean enabled, boolean clamped)
    {
        boolean blocked = dataBlocked || dragBlocked;

        synchronized (GUILock) {
            if (enabled && !blocked) {
                forceOutputTB.setSelected(true);
                forceOutputTB.setText("Force output is enabled");
                if (clamped) {
                    forceMagnitudeBar.setForeground(magnitudeBarColorClamped);
                    forceMagnitudeLabel.setForeground(magnitudeLabelColorClamped);
                } else {
                    forceMagnitudeLabel.setForeground(magnitudeLabelColorActive);
                    forceMagnitudeBar.setForeground(magnitudeBarColorActive);
                }
            } else {
                // disabled or blocked force output
                forceOutputTB.setSelected(false);
                if (blocked) {
                    if (dataBlocked)
                        forceOutputTB.setText("Attach blue link and set data");
                    else
                        // <=> dragBlocked == true
                        forceOutputTB.setText("Waiting for finish dragging");
                } else
                    forceOutputTB.setText("Force output is disabled");

                forceMagnitudeLabel.setForeground(magnitudeLabelColorInactive);
                if (clamped)
                    forceMagnitudeBar.setForeground(magnitudeBarColorClamped);
                else
                    forceMagnitudeBar.setForeground(magnitudeBarColorInactive);
            }

            forceOutputTB.setEnabled(!blocked);
            forcesList.setEnabled(!dataBlocked);
            addButton.setEnabled(!dataBlocked);

            updateForceButtonsState();

        }
    }

    private void clampSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_clampSliderStateChanged
        if (!updatingFromGUI) {
            controller.setForceClamp(clampSlider.getVal());
        }
    }//GEN-LAST:event_clampSliderStateChanged

    private void scaleSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_scaleSliderStateChanged
        if (!updatingFromGUI) {
            controller.setForceScale(scaleSlider.getVal());
        }
    }//GEN-LAST:event_scaleSliderStateChanged

    @SuppressWarnings(value = "unchecked")
    private void editButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editButtonActionPerformed
        IForce item = (IForce) forcesList.getSelectedValue();
        if (item != null) {
            showEditDialog(item);
        }
    }//GEN-LAST:event_editButtonActionPerformed

    private void disableButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_disableButtonActionPerformed
        forcesList.swapEnableSelectedForce();
    }//GEN-LAST:event_disableButtonActionPerformed

    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        forcesList.removeSelectedForce();
    }//GEN-LAST:event_removeButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        showNewForceDialog();
    }//GEN-LAST:event_addButtonActionPerformed

    private void duplicateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_duplicateButtonActionPerformed
        forcesList.duplicateSelectedForce();
    }//GEN-LAST:event_duplicateButtonActionPerformed
     // Variables declaration - do not modify//GEN-BEGIN:variables

    private javax.swing.JButton addButton;
    private pl.edu.icm.visnow.gui.widgets.FloatSlider clampSlider;
    private javax.swing.JButton disableButton;
    private javax.swing.JButton duplicateButton;
    private javax.swing.JButton editButton;
    private javax.swing.JProgressBar forceMagnitudeBar;
    private javax.swing.JLabel forceMagnitudeLabel;
    private javax.swing.JToggleButton forceOutputTB;
    private pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.gui.haptics.ForcesList forcesList;
    private javax.swing.JLabel hapticPosTitle;
    private javax.swing.JLabel hapticPosTitle3;
    private javax.swing.JLabel hapticPosXLabel;
    private javax.swing.JLabel hapticPosYLabel;
    private javax.swing.JLabel hapticPosZLabel;
    private javax.swing.JLabel hapticVelTitle;
    private javax.swing.JLabel hapticVelValue;
    private javax.swing.JLabel hapticVelXLabel;
    private javax.swing.JLabel hapticVelYLabel;
    private javax.swing.JLabel hapticVelZLabel;
    private javax.swing.JLabel jLabelDummy1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelForceMagnitude;
    private javax.swing.JPanel jPanelForceParams;
    private javax.swing.JPanel jPanelForces;
    private javax.swing.JPanel jPanelPosVel;
    private javax.swing.JPanel jPanelResizingGap;
    private javax.swing.JPanel jPanelUpdateRate;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel meanUpdateRate;
    private javax.swing.JButton removeButton;
    private pl.edu.icm.visnow.gui.widgets.FloatSlider scaleSlider;

    // End of variables declaration//GEN-END:variables
    /**
     * Enables/disabled buttons: Disable/Enable, Remove, Edit, Duplicate depending mainly on state
     * of a currently selected force.
     */
    private void updateForceButtonsState()
    {
        synchronized (GUILock) {
            if (lastWasForceDataBlocked) {
                disableButton.setEnabled(false);
                removeButton.setEnabled(false);
                editButton.setEnabled(false);
                duplicateButton.setEnabled(false);
            } else {
                IForce sel = (IForce) forcesList.getSelectedValue();
                int index = forcesList.getSelectedIndex();
                updateForceEnabledButtonState(sel, index);
                updateForceRemoveButtonState(sel, index);
                updateForceEditButtonState(sel, index);
                updateForceDuplicateButtonState(sel, index);
            }
        }
    }

    /**
     * Update Enable/Disable button. Change text and set as enabled for modifiable force and as
     * disabled for nonmodifiable one.
     * <p/>
     * @param sel   selected force (can be null)
     * @param index selected index
     */
    private void updateForceEnabledButtonState(IForce sel, int index)
    {
        if (sel == null) {
            disableButton.setEnabled(false);
        } else {
            disableButton.setEnabled(true);
            if (sel.isEnabled()) {
                disableButton.setText("Disable");
            } else {
                disableButton.setText("Enable");
            }
            disableButton.setEnabled(sel.canBeChanged());
        }
    }

    /**
     * Update Remove button. Change text and set as enabled for modifiable force and as
     * disabled for nonmodifiable one.
     * <p/>
     * @param sel   selected force (can be null)
     * @param index selected index
     */
    private void updateForceRemoveButtonState(IForce sel, int index)
    {
        boolean enableButton = (sel != null && sel.canBeChanged());
        removeButton.setEnabled(enableButton);

    }

    /**
     * Update Edit button. Change text and set as enabled for modifiable force and as
     * disabled for nonmodifiable one.
     * <p/>
     * @param sel   selected force (can be null)
     * @param index selected index
     */
    private void updateForceEditButtonState(IForce sel, int index)
    {
        boolean enableButton = (sel != null && sel.canBeChanged());
        editButton.setEnabled(enableButton);
    }

    /**
     * Update Duplicate button. Change text and set as enabled for modifiable force and as
     * disabled for nonmodifiable one.
     * <p/>
     * @param sel   selected force (can be null)
     * @param index selected index
     */
    private void updateForceDuplicateButtonState(IForce sel, int index)
    {
        boolean enableButton = (sel != null && sel.canBeChanged());
        duplicateButton.setEnabled(enableButton);
    }

    /**
     * Call that method when stopping using haptic GUI panel.
     * <p/>
     * It stops updating labels that display haptic parameters.
     */
    @Override
    public void close()
    {
        endForceDisplayThread();

    }
}
//revised.
