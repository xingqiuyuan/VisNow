package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import pl.edu.icm.jscic.dataarrays.DataArray;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.utils.VectorMath;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.visnow.lib.types.VNRegularField;

/**
 *
 * @author norkap
 */
public class Core
{

    public static final Color[] PLOT_COLORS = new Color[]{Color.RED, Color.GREEN, Color.BLUE, Color.CYAN, Color.MAGENTA, Color.YELLOW, Color.BLACK, Color.PINK, Color.ORANGE};

    

    public Core()
    {

    }

    /**
     * Creates XY chart from series collection. Enables panning and paint series.
     * <p>
     * @param series
     *               <p>
     * @return
     */
    public static JFreeChart createPannableXYChart(XYSeriesCollection series)
    {
        String xAxisLabel = "";
        Set<String> xLabels = new HashSet<>();
        String desc;
        for (int i = 0; i < series.getSeriesCount(); i++) {
            desc = series.getSeries(i).getDescription();
            xLabels.add(desc);
        }
        
        String[] xAxisLabelsArray = (String[]) xLabels.toArray(new String[xLabels.size()]);
        for (String label : xAxisLabelsArray) {
            xAxisLabel+=label+", ";
        }
        /*create chart*/
        JFreeChart chart = ChartFactory.createXYLineChart(
            "",
            xAxisLabel,
            "",
            series,
            PlotOrientation.VERTICAL,
            true,
            true,
            false);

        /*default settings*/
        Font font = new Font("Arial", Font.BOLD, 11);
        
        chart.getLegend().setVisible(false);
        chart.getXYPlot().setDomainPannable(true);
        chart.getXYPlot().setRangePannable(true);
        chart.getXYPlot().setBackgroundPaint(Color.WHITE);
        chart.getXYPlot().setRangeGridlinePaint(Color.LIGHT_GRAY);
        chart.getXYPlot().setDomainGridlinePaint(Color.LIGHT_GRAY);
        chart.getXYPlot().getDomainAxis().setLabelFont(font);
        chart.getXYPlot().getRangeAxis().setLabelFont(font);
        

        /*paint*/
        for (int i = 0; i < series.getSeriesCount(); i++) {
            chart.getXYPlot().getRenderer().setSeriesPaint(i, PLOT_COLORS[i % PLOT_COLORS.length]);
        }

        return chart;
    }
    
    public static XYSeriesCollection createXYSeriesCollection(Vector<Object> fields)
    {
        RegularField inFld;
        XYSeriesCollection dataSet = new XYSeriesCollection();
        
        if(fields == null || fields.get(0) == null){
            return dataSet;
        }
        
        for (Object field : fields) {
            inFld = ((VNRegularField) field).getField();

            for (int j = 0; j < inFld.getNComponents(); j++) {
                DataArray da = inFld.getComponent(j);
                if (da.isNumeric() && da.getVectorLength() == 1){
                        dataSet.addSeries(createSeries(da, inFld)); 
                    }
                }
            }
         
        return dataSet;
    }
    
    private static double valRawToPhys(double[] sCoeff, double val)
    {
        return sCoeff[0] * val + sCoeff[1];
    }

    
    public static XYSeries createSeries(DataArray da, RegularField inFld) {
        
        float[] affine1D = inFld.getAffine()[0];
        
        LargeArray data;
                    
        if(da.getRawArray().getType() == LargeArrayType.FLOAT){
            data = da.getRawArray();
        }else{
            data = da.getRawFloatArray();
        }
        
        XYSeries serie = new XYSeries(da.getName());
        float[] physCoeff;
        float[] geomCoeff;
        double[] sCoeff;
        FloatLargeArray coords = inFld.getCoordsFromAffine();
        
        if(inFld.hasCoords()){
            for (long k = 0; k < da.getNElements(); k++) {
                serie.add((double) k, da.getSchema().dataRawToPhys(data.getFloat(k)));
            }
            serie.setDescription("indices");
            
        }else if(affine1D[0]!=0 && affine1D[1]==0 && affine1D[2]==0){
            
            physCoeff = new float[]{inFld.getPreferredPhysicalExtents()[0][0], inFld.getPreferredPhysicalExtents()[1][0]};
            geomCoeff = new float[]{inFld.getPreferredExtents()[0][0], inFld.getPreferredExtents()[1][0]};
            sCoeff = new double[]{(physCoeff[1] - physCoeff[0])/(geomCoeff[1] - geomCoeff[0]), physCoeff[0] - ((physCoeff[1] - physCoeff[0])/(geomCoeff[1] - geomCoeff[0]))*geomCoeff[0]};
                                    
            for (long k = 0; k < da.getNElements(); k++) {
                serie.add(valRawToPhys(sCoeff, coords.get(3*k)), da.getSchema().dataRawToPhys(data.getFloat(k)));
            }
            String desc = "X";
            if(inFld.getAxesNames()!=null && !inFld.getAxesNames()[0].equals("")) 
                desc = inFld.getAxesNames()[0];
                                    
            serie.setDescription(desc);
            
        }else if(affine1D[0]==0 && affine1D[1]!=0 && affine1D[2]==0){
            physCoeff = new float[]{inFld.getPreferredPhysicalExtents()[0][1], inFld.getPreferredPhysicalExtents()[1][1]};
            geomCoeff = new float[]{inFld.getPreferredExtents()[0][1], inFld.getPreferredExtents()[1][1]};
            sCoeff = new double[]{(physCoeff[1] - physCoeff[0])/(geomCoeff[1] - geomCoeff[0]), physCoeff[0] - ((physCoeff[1] - physCoeff[0])/(geomCoeff[1] - geomCoeff[0]))*geomCoeff[0]};
                                    
            for (long k = 0; k < da.getNElements(); k++) {
                serie.add(valRawToPhys(sCoeff, coords.get(3*k + 1)), da.getSchema().dataRawToPhys(data.getFloat(k)));
            }
            String desc = "Y";
            if(inFld.getAxesNames()!=null && !inFld.getAxesNames()[0].equals("")) 
                desc = inFld.getAxesNames()[0];
                                    
            serie.setDescription(desc);
            
        }else if(affine1D[0]==0 && affine1D[1]==0 && affine1D[2]!=0){
            physCoeff = new float[]{inFld.getPreferredPhysicalExtents()[0][2], inFld.getPreferredPhysicalExtents()[1][2]};
            geomCoeff = new float[]{inFld.getPreferredExtents()[0][2], inFld.getPreferredExtents()[1][2]};
            sCoeff = new double[]{(physCoeff[1] - physCoeff[0])/(geomCoeff[1] - geomCoeff[0]), physCoeff[0] - ((physCoeff[1] - physCoeff[0])/(geomCoeff[1] - geomCoeff[0]))*geomCoeff[0]};
                                                  
            for (long k = 0; k < da.getNElements(); k++) {
                serie.add(valRawToPhys(sCoeff, coords.get(3*k + 2)), da.getSchema().dataRawToPhys(data.getFloat(k)));
            }
            String desc = "Z";
            if(inFld.getAxesNames()!=null && !inFld.getAxesNames()[0].equals("")) 
                desc = inFld.getAxesNames()[0];
                                    
            serie.setDescription(desc);
            
        }else{
            float veclen = VectorMath.vectorNorm(affine1D);
            for (long k = 0; k < da.getNElements(); k++) {
                serie.add((double) k*veclen, da.getSchema().dataRawToPhys(data.getFloat(k)));
            }
            serie.setDescription("coordinates");
        }
        
        
        return serie;
    }
    
    
    public static XYSeries createSeries(DataArray da, RegularField inFld, String seriesName) {
        DataArray daClone = da.cloneDeep();
        daClone.setName(seriesName);
        return createSeries(daClone, inFld);
    }

    /**
     * compute series statistics i.e. signal: preferredMinValue, average, preferredMaxValue, SD.
 <p>
     * @param series
     *               <p>
     * @return
     */
    public static float[][] computeStats(XYSeriesCollection series)
    {
        float[][] stats = new float[series.getSeriesCount()][4];

        for (int i = 0; i < stats.length; i++) {
            stats[i] = Arrays.copyOf(computeSerieStats(series.getSeries(i)), 4);
        }

        return stats;
    }

    private static float[] computeSerieStats(XYSeries data)
    {
        float max = 0;
        float min = Float.MAX_VALUE;
        int counter = 0;
        float avg = 0.0f;
        float s = 0.0f;
        float val;

        for (int i = 0; i < data.getItemCount(); i++) {
            val = (float) data.getDataItem(i).getYValue();
            max = max(max, val);
            min = min(min, val);
            avg += val;
            counter++;
            s += val * val;
        }

        avg /= (double) counter;
        if (counter > 1) {
            s = (float) (sqrt(s / (double) counter - avg * avg));
        } else {
            s = 0.0f;
        }

        return new float[]{min, avg, max, s};
    }

}
