package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.ExtendedChart;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.XYSeriesCollection;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.Core;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.utils.ColorEditor;

/**
 *
 * @author norkap
 */
public class ExtendedChart implements SeriesToDisplayChangedListener
{

    private final ChartData data;
    private final ExtendedChartPanel chartGUI;
    private final ChartParams params;

    public ExtendedChart(ChartData data, ExtendedChartPanel chartGUI)
    {
        this.data = data;
        this.chartGUI = chartGUI;
        this.params = new ChartParams();

        chartGUI.addSeriesListener(this);
        chartGUI.addRemoveSeriesListener(new RemoveSeriesListener());
        chartGUI.addSeriesColorChangedListener(new SeriesColorChangedListener());
        chartGUI.addKeyPressedOnSeriesTableListener(new KeyListenerAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_DELETE){
                    removeSeries();
                }
            }
        });
    }

    public ChartParams getChartParams()
    {
        return params;
    }
    
    public ExtendedChartPanel getChartPanel()
    {
        return chartGUI;
    }

    public void setDataSet(XYSeriesCollection chartData)
    {
        data.setDataSet(chartData);
    }

    public void update()
    {
        data.updateSeriesToDisplay();
        chartGUI.setChartPanel(new ChartPanel(Core.createPannableXYChart(data.getDataSetToDisplay())));
        data.initSeriesColorMap(Core.PLOT_COLORS[0]);
        data.setStats(Core.computeStats(data.getDataSet()));
        chartGUI.initGUIComponents(data.getSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
        chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
        chartGUI.showChart();
    }
    
    private void addSeries(String[] names)
    {
        boolean seriesAdded = false;
        if(data.getDataSetNr() > 0){
            for (String name : names) {
                if (data.addSeriesToDisplay(name)) {
                    int idx = data.getDataSetToDisplayNr();
                    Color c = chartGUI.addNewColorToPlot(--idx);
                    String key = data.getDisplayedSeriesName(idx);
                    data.addColorToMap(key, c);
                    seriesAdded = true;
                }
            }
            if (seriesAdded) {
                chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
            }
        }
    }

    private void addSeries()
    {
        if (data.getDataSetNr() > 0 && data.addSeriesToDisplay(chartGUI.getSelectedComponentName())) {
            int idx = data.getDataSetToDisplayNr();
            Color c = chartGUI.addNewColorToPlot(--idx);
            String key = data.getDisplayedSeriesName(idx);
            data.addColorToMap(key, c);
            chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
        }
    }

    private void removeSeries()
    {
        String[] names = chartGUI.getRemovedSeriesNames();
        if (names != null) {
            data.removeSeriesToDisplay(names);
            chartGUI.paintPlot(data.getSeriesColorMap());
            chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
        }
    }

    private void updateSeriesColor(ColorEditor cEditor)
    {
        Color c = chartGUI.getSeriesColor(cEditor);
        chartGUI.setSeriesColor(chartGUI.getSelectedRow(), c);
        data.addColorToMap(chartGUI.getSelectedSeriesName(), c);
        chartGUI.update(data.getDisplayedSeriesNames(), data.getStats(), data.getDisplayedSeriesXLabels());
    }

    @Override
    public void addSeriesToDisplay(String[] names) {
        addSeries(names);
    }

    class AddSeriesActionListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addSeries();
        }

    }

    class RemoveSeriesListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            removeSeries();
        }

    }

    class SeriesColorChangedListener implements CellEditorListener
    {

        @Override
        public void editingStopped(ChangeEvent e)
        {
            if (e.getSource() instanceof ColorEditor)
                updateSeriesColor((ColorEditor) e.getSource());
        }

        @Override
        public void editingCanceled(ChangeEvent e)
        {

        }

    }
}
