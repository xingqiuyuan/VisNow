package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.ExtendedChart;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author Norbert_2
 */


public abstract class KeyListenerAdapter implements KeyListener{

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public abstract void keyPressed(KeyEvent e);

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
    
}
