package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1DNew.ExtendedChart;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author norkap
 */
public class ChartData
{
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ChartData.class);
    private XYSeriesCollection dataSet;
    private XYSeriesCollection dataSetToDisplay;
    private final Map<String, Color> seriesColorMap;
    private float[][] stats;

    public ChartData()
    {
        this.seriesColorMap = new HashMap<>();
        this.dataSet = new XYSeriesCollection();
        this.dataSetToDisplay = new XYSeriesCollection();
    }

    public void updateSeriesToDisplay()
    {
        if (dataSet.getSeriesCount() > 0) {
            if(dataSetToDisplay.getSeriesCount() > 0){
                XYSeriesCollection newSeriesToDisplay = new XYSeriesCollection();
                for (int i = 0; i < dataSetToDisplay.getSeriesCount(); i++) {
                    try{
                        XYSeries series = dataSet.getSeries(dataSetToDisplay.getSeries(i).getKey());
                        newSeriesToDisplay.addSeries(series);
                    }catch(UnknownKeyException ex){
                        LOGGER.info("Series removed", ex);
                    }
                }
                if(newSeriesToDisplay.getSeriesCount()>0){
                    dataSetToDisplay = newSeriesToDisplay;
                }else{
                    dataSetToDisplay.removeAllSeries();
                    dataSetToDisplay.addSeries(dataSet.getSeries(0));
                }
            }else{
                dataSetToDisplay.addSeries(dataSet.getSeries(0));
            }
        }else{ 
            dataSetToDisplay.removeAllSeries();
        }
    }

    public void initSeriesColorMap(Color c)
    {
        if (seriesColorMap.size() > 0) {
            seriesColorMap.clear();
        }
        if (dataSetToDisplay.getSeriesCount() > 0) {
            seriesColorMap.put((String) dataSetToDisplay.getSeriesKey(0), c);
        }
    }

    public XYSeriesCollection getDataSet()
    {
        return dataSet;
    }

    public XYSeriesCollection getDataSetToDisplay()
    {
        return this.dataSetToDisplay;
    }

    public int getDataSetNr()
    {
        return dataSet.getSeriesCount();
    }

    public int getDataSetToDisplayNr()
    {
        return this.dataSetToDisplay.getSeriesCount();
    }

    public String[] getSeriesNames()
    {
        String[] names = new String[dataSet.getSeriesCount()];

        for (int i = 0; i < names.length; i++) {
            names[i] = (String) dataSet.getSeriesKey(i);
        }
        return names;
    }

    public String[] getDisplayedSeriesNames()
    {
        String[] names = new String[dataSetToDisplay.getSeriesCount()];
        for (int i = 0; i < names.length; i++) {
            names[i] = (String) dataSetToDisplay.getSeriesKey(i);
        }
        return names;
    }
    
    public String[] getDisplayedSeriesXLabels()
    {
        Set<String> xLabels = new HashSet<>();
        String desc;
        for (int i = 0; i < dataSetToDisplay.getSeriesCount(); i++) {
            desc = dataSetToDisplay.getSeries(i).getDescription();
            xLabels.add(desc);
        }
        return (String[]) xLabels.toArray(new String[xLabels.size()]);
    }

    public String getDisplayedSeriesName(int idx)
    {
        return (String) dataSetToDisplay.getSeriesKey(idx);
    }

    public Map<String, Color> getSeriesColorMap()
    {
        return seriesColorMap;
    }

    public float[][] getStats()
    {
        return stats;
    }

    public void setStats(float[][] stats)
    {
        this.stats = new float[stats.length][4];

        for (int i = 0; i < stats.length; i++) {
            this.stats[i] = Arrays.copyOf(stats[i], stats[i].length);
        }
    }

    public void setDataSet(XYSeriesCollection dataSet)
    {
        this.dataSet = dataSet;
    }

    public void addColorToMap(String key, Color c)
    {
        seriesColorMap.put(key, c);
    }

    public boolean addSeriesToDisplay(String key)
    {
        if (dataSetToDisplay.getSeriesIndex(key) == -1) {
            dataSetToDisplay.addSeries(dataSet.getSeries(key));
            return true;
        }
        return false;
    }

    public void removeSeriesToDisplay(String[] names)
    {
        for (String name : names) {
            dataSetToDisplay.removeSeries(dataSetToDisplay.getSeries(name));
            seriesColorMap.remove(name);
        }
    }

}
