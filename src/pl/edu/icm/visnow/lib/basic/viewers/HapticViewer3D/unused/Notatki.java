package pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.unused;

//
//import com.sun.j3d.utils.behaviors.mouse.*;
//import com.sun.j3d.utils.behaviors.sensor.*;
//import com.sun.j3d.utils.behaviors.vp.*;
//import com.sun.j3d.utils.picking.*;
//import javax.media.j3d.InputDevice;

/*
 * Behaviors:
 * 
 *   -  KeyNavigatorBehavior
 *   -  setViewPlatformBehavior() allows only one behavior
 *   -  http://stackoverflow.com/questions/9857548/add-multiple-behavior-to-viewingplatform-java3d
 *   -  przykład aplikacji: http://fivedots.coe.psu.ac.th/~ad/jg/ch15/chap15.pdf
 *   -  
 */
/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class Notatki
{

    /**
     * KARTA LPT
     *
     * You will need to enable this for your PCI parallel port card as well. The only PCI card that
     * works is the AXXON 534KB and it works only on Windows XP and XP 64.
     *
     * YES and please note that this card will work only on Windows XP and XP 64-bit OS. This card
     * will not work properly on Vista or Windows 7.
     * <p/>
     * http://www.softio.com/pcieppprinterportuniv.htm
     *
     *
     * Hi,
     * <p/>
     * It would be nice to see in future releases of API some functions for querying the configured
     * devices (names and maybe some additional properties)
     */
    /**
     * Estimating velocity
     *
     * // Get the time delta since the last update.
     * HDdouble instRate;
     * hdGetDoublev(HD_INSTANTANEOUS_UPDATE_RATE, &instRate);
     * HDdouble deltaT = 1.0 / instRate;
     * <p/>
     * // Get the current proxy position from the state cache.
     * // Note that the effect state cache is maintained in workspace coordinates,
     * // so we don't need to do any transformations in using the proxy
     * // position for computing forces.
     * hduVector3Dd proxyPos;
     * hlCacheGetDoublev(cache, HL_PROXY_POSITION, proxyPos);
     * <p/>
     * //Calculate velocity in mm/s
     * static hduVector3Dd proxyVelocity;
     * //Initialize lastCached position to be zero
     * //to get a proper first value for velocity
     * static hduVector3Dd lastCachedProxyPos(0,0,0);
     * proxyVelocity = (proxyPos - lastCachedProxyPos) / deltaT ;
     *
     *
     *
     */
    public void rozneLinki()
    {
        //        com.sun.j3d.utils.behaviors.sensor.SensorEventAgent a0;
        //        com.sun.j3d.utils.behaviors.sensor.Mouse6DPointerBehavior a1;
        //        com.sun.j3d.utils.behaviors.mouse.MouseRotate a2;
        //        com.sun.j3d.utils.behaviors.mouse.MouseTranslate a3;
        //        com.sun.j3d.utils.behaviors.mouse.MouseWheelZoom a4;
        //        com.sun.j3d.utils.behaviors.mouse.MouseBehavior a5;
        //        com.sun.j3d.utils.behaviors.vp.WandViewBehavior a7;
        //
        //        InputDevice id = null;
        //
        //        javax.media.j3d.WakeupOnAWTEvent a6;
    }

    public void logowanie_i_debuggowanie()
    {
        //        Thread.currentThread().getStackTrace();
        //        Thread.currentThread().getName();
        //        Thread.currentThread().toString();
    }

    public void konfiguracja()
    {
        //konfiguracja
        //        java.util.Properties trzymanie_konfiguracji;
        /**
         * Wartości parametru os.name
         * http://lopica.sourceforge.net/os.html
         */
        //obsluga jezykow
        //        java.util.PropertyResourceBundle czytanieRBzPliku;
        //        java.util.ResourceBundle abstrakcyjnaKlasaZasobu;
        /**
         * Większość standardowych tekstów mogłaby być trzymana w
         * NLS.java jakimś głównym (stałe oznaczające kody pól i static metody
         * do ładowania zasobów)
         * Na dodatek w pakietach każdy mógłby dodawać swoje, lub nadpisywać istniejące
         * (tylko lepiej, żeby były one trzymane w jednym katalogu,
         * albo w osobnej strukturze, żeby było łatwiej tym zawiadywać i tłumaczyć)
         */
        /**
         * Dear Sirs,
         * <p/>
         * We are using your Phantom Premium 6DOF device to enhance interaction with our
         * visualisation framework. After working for some time with it, I would like to bring your
         * attention to two topics.
         * <p/>
         * First, is the interface through which the device communicates. Parallel port is a dying
         * solution. It is not supported on current motherboards, forcing end-users to use device
         * with older hardware. We have tried to use LPT PCI card, but could not manage to establish
         * communication through it. Do you plan to change interface technology to serial ports in
         * future? USB seems to be the preferred solution.
         * <p/>
         * Second thing, which concerns us, is the construction of Phantom, particularly the
         * high-force model. While it should have a very solid construction, it has disassembled
         * himself a few times. Main point of weakness is the connection between the higher
         * horizontal arm (to which the cables are strapped) and the vertical part. We find it
         * unacceptable in a high-class expensive solution, to have one part stick into the other
         * without a one screw. To reassemble it, you have to press it inside with brute force,
         * using hand as a hammer. Most likely, it can limit lifetime of this device.
         * <p/>
         * I am looking forward to your reply,
         * Krzysztof Madejski
         */
    }
}
