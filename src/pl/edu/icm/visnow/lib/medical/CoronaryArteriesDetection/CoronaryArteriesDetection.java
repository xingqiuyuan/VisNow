package pl.edu.icm.visnow.lib.medical.CoronaryArteriesDetection;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jlargearrays.LogicLargeArray;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class CoronaryArteriesDetection extends OutFieldVisualizationModule
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI = new GUI();
    protected Params params;
    protected RegularField inField = null;
    protected RegularField glField = null;
    protected boolean fromGUI = false;
    private int nThreads = pl.edu.icm.visnow.system.main.VisNow.availableProcessors();
    private short[] inData;
    private int[] dims = null;
    private int[] r1 = null;
    private int[][] r = null;
    private int margin = 15;
    private short[] od = null;
    private int down = 3;
    private int[][] offsets = new int[3][];

    public CoronaryArteriesDetection()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                fromGUI = true;
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.setParams(params);
            }
        });
        computeUI.setParams(params);
        ui.addComputeGUI(computeUI);
        setPanel(ui);
    }

    private void computeRanges(int radius)
    {
        r1 = new int[2 * radius + 1];
        r = new int[2 * radius + 1][];
        for (int i = -radius, k = 0; i <= radius; i++, k++) {
            r1[k] = (int) (sqrt(radius * radius - i * i));
            r[k] = new int[2 * r1[k] + 1];
            for (int j = -r1[k], l = 0; j <= r1[k]; j++, l++) {
                r[k][l] = (int) (sqrt(radius * radius - i * i - j * j));
            }
        }
    }

    private class Erode implements Runnable
    {

        private int nThreads = 1;
        private int iThread = 0;
        private int[] dims;
        private short[] inData = null;
        private short[] outData = null;
        private int radius;
        private int[] r1 = null;
        private int[][] r = null;

        public Erode(int nThreads, int iThread, int[] dims, short[] inData, short[] outData, int radius, int[] r1, int[][] r)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            this.inData = inData;
            this.outData = outData;
            this.radius = radius;
            this.r1 = r1;
            this.r = r;
        }

        @Override
        public void run()
        {
            int kstart = (iThread * dims[2]) / nThreads;
            int kend = ((iThread + 1) * dims[2]) / nThreads;
            for (int i2 = kstart; i2 < kend; i2++) {
                if (iThread == 0)
                    setProgress(.2f * i2 / (kend - 1.f));

                for (int i1 = 0; i1 < dims[1]; i1++)
                    for (int i0 = 0; i0 < dims[0]; i0++) {
                        int ms = (i2 * dims[1] + i1) * dims[0] + i0;
                        int k2 = max(0, i2 - radius);
                        int l2 = min(dims[2] - 1, i2 + radius);
                        short v0 = Short.MAX_VALUE;
                        innerLoop:
                        for (int j2 = k2, jj2 = radius - i2 + k2; j2 <= l2; j2++, jj2++) {
                            int radius1 = r1[jj2];
                            int k1 = max(0, i1 - radius1);
                            int l1 = min(dims[1] - 1, i1 + radius1);
                            for (int j1 = k1, jj1 = radius1 - i1 + k1; j1 <= l1; j1++, jj1++) {
                                int radius0 = r[jj2][jj1];
                                int k0 = max(0, i0 - radius0);
                                int l0 = min(dims[0] - 1, i0 + radius0);
                                for (int j0 = k0, m0 = (j2 * dims[1] + j1) * dims[0] + k0; j0 < l0; j0++, m0++)
                                    if (inData[m0] < v0)
                                        v0 = inData[m0];
                            }
                        }
                        outData[ms] = v0;
                    }
            }
        }
    }

    private class Dilate implements Runnable
    {

        private int nThreads = 1;
        private int iThread = 0;
        private int[] dims;
        private short[] inData = null;
        private short[] outData = null;
        private int radius;
        private int[] r1 = null;
        private int[][] r = null;

        public Dilate(int nThreads, int iThread, int[] dims, short[] inData, short[] outData, int radius, int[] r1, int[][] r)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            this.inData = inData;
            this.outData = outData;
            this.radius = radius;
            this.r1 = r1;
            this.r = r;
        }

        public void run()
        {
            int kstart = (iThread * dims[2]) / nThreads;
            int kend = ((iThread + 1) * dims[2]) / nThreads;
            for (int i2 = kstart; i2 < kend; i2++) {
                if (iThread == 0)
                    setProgress(.2f + .6f * i2 / (kend - 1.f));
                for (int i1 = 0; i1 < dims[1]; i1++)
                    for (int i0 = 0; i0 < dims[0]; i0++) {
                        int ms = (i2 * dims[1] + i1) * dims[0] + i0;
                        int k2 = max(0, i2 - radius);
                        int l2 = min(dims[2] - 1, i2 + radius);
                        short v0 = Short.MIN_VALUE;
                        innerLoop:
                        for (int j2 = k2, jj2 = radius - i2 + k2; j2 <= l2; j2++, jj2++) {
                            int radius1 = r1[jj2];
                            int k1 = max(0, i1 - radius1);
                            int l1 = min(dims[1] - 1, i1 + radius1);
                            for (int j1 = k1, jj1 = radius1 - i1 + k1; j1 <= l1; j1++, jj1++) {
                                int radius0 = r[jj2][jj1];
                                int k0 = max(0, i0 - radius0);
                                int l0 = min(dims[0] - 1, i0 + radius0);
                                for (int j0 = k0, m0 = (j2 * dims[1] + j1) * dims[0] + k0; j0 < l0; j0++, m0++)
                                    if (inData[m0] > v0)
                                        v0 = inData[m0];
                            }
                        }
                        outData[ms] = v0;
                    }
            }
        }
    }

    private void compute()
    {
        outRegularField = inField.cloneShallow();
        dims = inField.getDims();
        offsets[0] = new int[]{-dims[0] * dims[1] - dims[0] - 1, -dims[0] * dims[1] - dims[0], -dims[0] * dims[1] - dims[0] + 1,
                               -dims[0] * dims[1] - 1, -dims[0] * dims[1], -dims[0] * dims[1] + 1,
                               -dims[0] * dims[1] + dims[0] - 1, -dims[0] * dims[1] + dims[0], -dims[0] * dims[1] + dims[0] + 1,
                               -dims[0] - 1, -dims[0], -dims[0] + 1,
                               -1, 0, +1,
                               dims[0] - 1, dims[0], dims[0] + 1,
                               dims[0] * dims[1] - dims[0] - 1, dims[0] * dims[1] - dims[0], dims[0] * dims[1] - dims[0] + 1,
                               dims[0] * dims[1] - 1, dims[0] * dims[1], dims[0] * dims[1] + 1,
                               dims[0] * dims[1] + dims[0] - 1, dims[0] * dims[1] + dims[0], dims[0] * dims[1] + dims[0] + 1,};
        offsets[1] = new int[]{-dims[0] * dims[1] - dims[0],
                               -dims[0] * dims[1] - 1, -dims[0] * dims[1], -dims[0] * dims[1] + 1,
                               -dims[0] * dims[1] + dims[0],
                               -dims[0] - 1, -dims[0], -dims[0] + 1,
                               -1, 0, +1,
                               dims[0] - 1, dims[0], dims[0] + 1,
                               dims[0] * dims[1] - dims[0],
                               dims[0] * dims[1] - 1, dims[0] * dims[1], dims[0] * dims[1] + 1,
                               dims[0] * dims[1] + dims[0],};
        offsets[2] = new int[]{dims[0] * dims[1],
                               -dims[0],
                               -1, 0, 1,
                               dims[0],
                               dims[0] * dims[1]
        };

        inData = inField.getComponent(params.getComponent()).getRawShortArray().getData();
        int[] tdims = new int[dims.length];
        for (int i = 0; i < tdims.length; i++)
            tdims[i] = (dims[i] + down - 1) / down;
        int nTmp = tdims[0] * tdims[1] * tdims[2];
        short[] td0 = new short[nTmp];
        for (int i = 0, l = 0; i < tdims[2]; i++) {
            int i0 = down * i;
            int i1 = min(down * i + down, dims[2]);
            for (int j = 0; j < tdims[1]; j++) {
                int j0 = down * j;
                int j1 = min(down * j + down, dims[1]);
                for (int k = 0; k < tdims[0]; k++, l++) {
                    int k0 = down * k;
                    int k1 = min(down * k + down, dims[0]);
                    int av = 0;
                    for (int ii = i0; ii < i1; ii++)
                        for (int jj = j0; jj < j1; jj++)
                            for (int kk = k0; kk < k1; kk++)
                                av += inData[(ii * dims[1] + jj) * dims[0] + kk];
                    td0[l] = (short) (av / ((i1 - i0) * (j1 - j0) * (k1 - k0)));
                }
            }

        }
        short[] td1 = new short[nTmp];
        int rErode = (params.getRadius() + down / 2) / down;
        computeRanges(rErode);
        Thread[] workThreads = new Thread[nThreads];
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread] = new Thread(new Erode(nThreads, iThread, tdims, td0, td1,
                                                        rErode, r1, r));
        for (Thread workThread : workThreads)
            workThread.start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception e) {
            }
        int rDilate = (params.getRadius() + margin + down / 2) / down;
        computeRanges(rDilate);
        for (int iThread = 0; iThread < nThreads; iThread++)
            workThreads[iThread] = new Thread(new Dilate(nThreads, iThread, tdims, td1, td0,
                                                         rDilate, r1, r));
        for (Thread workThread : workThreads)
            workThread.start();
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (Exception e) {
            }
        od = new short[(int) (int) inField.getNNodes()];
        for (int i = 0, l = 0; i < tdims[2]; i++) {
            int i0 = down * i;
            int i1 = min(down * i + down, dims[2]);
            for (int j = 0; j < tdims[1]; j++) {
                int j0 = down * j;
                int j1 = min(down * j + down, dims[1]);
                for (int k = 0; k < tdims[0]; k++, l++) {
                    int k0 = down * k;
                    int k1 = min(down * k + down, dims[0]);
                    for (int ii = i0; ii < i1; ii++)
                        for (int jj = j0; jj < j1; jj++)
                            for (int kk = k0; kk < k1; kk++)
                                od[(ii * dims[1] + jj) * dims[0] + kk] = td0[l];
                }
            }

        }

        outRegularField.addComponent(DataArray.create(od, 1, "opened"));
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null ||
            ((VNRegularField) getInputFirstValue("inField")).getField() == null)
            return;
        if (!fromGUI) {
            inField = ((VNRegularField) getInputFirstValue("inField")).getField();
            if (inField == null || inField.getDims() == null || inField.getDims().length < 2)
                return;
            computeUI.setInField(inField);
            dims = inField.getDims();
        } else {
            fromGUI = false;
            if (params.outputMask()) {
                LogicLargeArray mask = new LogicLargeArray(od.length);
                for (int i = 0; i < mask.length(); i++)
                    mask.setBoolean(i, od[i] > params.getVMin());
                outRegularField.setCurrentMask(mask);
                setOutputValue("outField", new VNRegularField(outRegularField));
                params.setOutputMask(false);
            } else {
                compute();
                setOutputValue("outField", new VNRegularField(outRegularField));
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }

    }

}
