//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.medical.ExtendedSkeletonizer;

import pl.edu.icm.visnow.lib.basic.mappers.Skeletonizer.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.geometries.objects.RegularField3DOutline;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.PickType;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.InterpolateToMesh;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.curves.RegularMeshExtrusion;
import pl.edu.icm.visnow.lib.utils.numeric.splines.SplineValueGradient;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.cells.CellType;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Skeletonizer extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of Skeletonizer
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    protected SkeletonCompute skeletonizer = null;
    protected ExtendedSkeletonizerUI computeUI;
    protected RegularField lastInField = null;
    protected RegularField inField;
    protected RegularField straightTubeField;
    protected RegularField tubeField;
    protected IrregularField startPoints;
    protected Params params;
    protected SkeletonizerParams skeletonizerParams;
    protected float[] crds;
    protected float[] fullSegCoords = null;
    protected short[] rData;
    protected int nPolys;
    protected int[] polylines;
    protected int pickedNode = -1;
    protected float dl = 0;
    protected RegularField3DOutline tubeOutline = new RegularField3DOutline();

    protected FloatValueModificationListener progressListener
        = new FloatValueModificationListener()
        {
            @Override
            public void floatValueChanged(FloatValueModificationEvent e)
            {
                setProgress(e.getVal());
            }
        };

    protected Pick3DListener pick3DListener = new Pick3DListener(PickType.POINT)
    {
        @Override
        public void handlePick3D(Pick3DEvent e)
        {
            float[] x = e.getPoint();
            if (x == null || outIrregularField == null || polylines == null)
                return;
            int k = -1;
            float fmin = Float.MAX_VALUE;
            float[] coords = outIrregularField.getCurrentCoords() == null ? null : outIrregularField.getCurrentCoords().getData();
            if (coords == null)
                return;
            for (int i = 0; i < coords.length; i += 3) {
                float f = (coords[i] - x[0]) * (coords[i] - x[0]) +
                    (coords[i + 1] - x[1]) * (coords[i + 1] - x[1]) +
                    (coords[i + 2] - x[2]) * (coords[i + 2] - x[2]);
                if (f < fmin) {
                    fmin = f;
                    k = i / 3;
                }
            }
            if (k >= 0) {
                pickedNode = k;
                skeletonizerParams.setRecompute(SkeletonizerParams.SELECT);
                startAction();
            }
        }
    };

    public Skeletonizer()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                skeletonizerParams.setRecompute(0);
                startAction();
            }
        });
        skeletonizerParams = params.getSkeletonizerParams();
        skeletonizerParams.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                if (skeletonizerParams.isRecompute() > 0)
                    startAction();
                else 
                    show();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new ExtendedSkeletonizerUI();
                computeUI.setParams(params);
                ui.addComputeGUI(computeUI);
            }
        });
        outObj.setName("skeletonizer");
        setPanel(ui);
    }

    @Override
    public Pick3DListener getPick3DListener()
    {
        return pick3DListener;
    }

    private void createTubeField(float[] segCoords)
    {
        int n = segCoords.length / 3;
        float len = 0;
        float[] rlen = new float[n];
        rlen[0] = 0;
        for (int i = 1; i < n; i++) {
            float f = 0;
            for (int j = 0; j < 3; j++)
                f += (segCoords[3 * i + j] - segCoords[3 * i + j - 3]) *
                     (segCoords[3 * i + j] - segCoords[3 * i + j - 3]);
            rlen[i] = len += (float) sqrt(f);
        }
        float vl = len / (n - 1);
        dl = vl * params.getResolution() / 100.f;
        int nx = (int) (len / dl);
        int nr = (int) (params.getDiameter() * 100.f / params.getResolution()) + 1;
        computeUI.setMeshDescription(String.format("interpolating to %dx%dx%d mesh", nx,nr,nr));
        float[] axisCoords = new float[3 * (nx + 1)];
        int k = 0;
        //TODO crashes on path shortening
        for (int i = 0; i <= nx; i++) {
            while (i * dl > rlen[k])
                k += 1;
            if (i * dl == rlen[k])
                System.arraycopy(segCoords, 3 * k, axisCoords, 3 * i, 3);
            else {
                float t = (i * dl - rlen[k - 1]) / (rlen[k] - rlen[k - 1]);
                for (int j = 0; j < 3; j++)
                    axisCoords[3 * i + j] = t * segCoords[3 * k + j] + (1 - t) * segCoords[3 * k + j - 3];
            }
        }
        tubeField = RegularMeshExtrusion.extrude(axisCoords, new float[]{dl, 0, 0},
                                                 nr, params.getSmoothing());
        tubeField.setUserData(new String[]{String.format("dl=%8.6f", dl)});
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        inField = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (inField == null)
            return;
        if (inField != lastInField) {
            lastInField = inField;
            if (skeletonizer != null)
                skeletonizer.clearFloatValueModificationListener();
            if (inField.getDims().length == 2)
                skeletonizer = new Compute2D(inField, skeletonizerParams);
            else
                skeletonizer = new Compute3D(inField, skeletonizerParams);
            skeletonizer.addFloatValueModificationListener(progressListener);
            skeletonizer.addActivityListener(computeUI.getActivityListener());
            computeUI.setInfield(inField);
            skeletonizerParams.setRecompute(SkeletonizerParams.SKELETONIZE);
            return;
        }
        if (inField == null)
            return;

        if (skeletonizerParams.isRecompute() == SkeletonizerParams.SKELETONIZE) {
            skeletonizer.setInitDiameter(params.getInitDiameter());
            skeletonizer.updateSkeleton();
            fullSegCoords = null;
        }

        if (skeletonizerParams.isRecompute() >= SkeletonizerParams.MIN_LENGTH) {
            nPolys = 0;
            int[] activeSets = skeletonizerParams.getSets();
            if (activeSets == null || activeSets.length < 1)
                activeSets = new int[]{2};
            int[][] nNodes = skeletonizer.getNNodes();
            if (nNodes == null)
                return;
            float[][][] coords = skeletonizer.getCoords();
            short[][][] radii = skeletonizer.getRadiiData();
            int[][] endSegments = skeletonizer.getEndSegments();
            int[][] endNodes    = skeletonizer.getEndNodes();
            int[][] startNodes  = new int[endSegments.length][];
            int[][] resEndNodes    = new int[endSegments.length][];
            for (int i = 0; i < resEndNodes.length; i++) {
                startNodes[i]     = new int[endNodes[i].length];
                resEndNodes[i]    = new int[endNodes[i].length];
                for (int j = 0; j < resEndNodes[i].length; j++) 
                    resEndNodes[i][j] = startNodes[i][j] = -1;
            }
            int nVerts = 0, nCells = 0, nMax = 0;
            for (int iSet = 0; iSet < activeSets.length; iSet++) {
                int nSet = activeSets[iSet] - 2;
                if (nSet > nMax)
                    nMax = nSet;
                if (nNodes[nSet] == null || nNodes[nSet].length < 1)
                    continue;
                int cNPolys = nNodes[nSet].length;
                for (int iPoly = 0; iPoly < cNPolys; iPoly++)
                    if (nNodes[nSet][iPoly] >= skeletonizerParams.getMinSegLen()) {
                        if (endSegments[nSet][iPoly] != -1 && startNodes[nSet][endSegments[nSet][iPoly]] != -1) {
                            resEndNodes[nSet][iPoly] = startNodes[nSet][endSegments[nSet][iPoly]] + endNodes[nSet][iPoly];
                            nCells += 1;
                        }
                        nVerts += nNodes[nSet][iPoly];
                        nCells += nNodes[nSet][iPoly] - 1;
                        nPolys += 1;
                    }
            }
            polylines = new int[nPolys + 1];
            polylines[0] = 0;
            if (nVerts == 0)
                return;
            crds = new float[3 * nVerts];
            int[] cells = new int[2 * nCells];
            rData = new short[nVerts];
            for (int iSet = 0, k = 0, l = 0, m = 0, p = 1; iSet < activeSets.length; iSet++) {
                int nSet = activeSets[iSet] - 2;
                if (nNodes[nSet] == null || nNodes[nSet].length < 1)
                    continue;
                int cNPolys = nNodes[nSet].length;
                for (int iPoly = 0; iPoly < cNPolys; iPoly++) {
                    if (nNodes[nSet][iPoly] < skeletonizerParams.getMinSegLen())
                        continue;
                    System.arraycopy(radii[nSet][iPoly], 0, rData, m, nNodes[nSet][iPoly]);
                    for (int i = 0; i < coords[nSet][iPoly].length; i++, k++)
                        crds[k] = coords[nSet][iPoly][i];
                    polylines[p] = polylines[p - 1] + nNodes[nSet][iPoly];
                    p += 1;
                    for (int i = 0; i < nNodes[nSet][iPoly] - 1; i++, l += 2) {
                        cells[l] = m + i;
                        cells[l + 1] = m + i + 1;
                    }
                    if (resEndNodes[nSet][iPoly] != -1)
                    {
                        cells[l] = m + nNodes[nSet][iPoly] - 1;
                        cells[l + 1] = resEndNodes[nSet][iPoly];
                        l += 2;
                        polylines[p] += 1;
                    }
                    m += nNodes[nSet][iPoly];
                }
            }
            byte[] edgeOrientations = new byte[nCells];
            for (int i = 0; i < edgeOrientations.length; i++)
                edgeOrientations[i] = 1;
            outIrregularField = new IrregularField(nVerts);
            outIrregularField.setCurrentCoords(new FloatLargeArray(crds));
            CellSet cellSet = new CellSet(inField.getName() + "skeleton");
            DataArray da = DataArray.create(rData, 1, "radii");
            outIrregularField.addComponent(da);
            CellArray skeletonSegments = new CellArray(CellType.SEGMENT, cells, edgeOrientations, null);
            cellSet.setBoundaryCellArray(skeletonSegments);
            cellSet.setCellArray(skeletonSegments);
            outIrregularField.addCellSet(cellSet);
            outIrregularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
            outField = outIrregularField;
            prepareOutputGeometry();
            outObj.addNode(tubeOutline);
            setOutputValue("outField", new VNIrregularField(outIrregularField));
            IrregularField tree = SkeletonTree.createTree(crds, cells, rData);
            setOutputValue("treeField", new VNIrregularField(tree));
        }

        if (skeletonizerParams.isRecompute() == SkeletonizerParams.SELECT)
            for (int i = 1; i < polylines.length; i++)
                if (pickedNode < polylines[i]) {
                    int low = polylines[i - 1];
                    int up = polylines[i];
                    int n = up - low;
                    fullSegCoords = new float[3 * n];
                    for (int j = 0; j < n; j++) 
                        System.arraycopy(crds, 3 * (low + j), fullSegCoords, 3 * (n - j - 1), 3);
                    computeUI.setSelectedLineLength(n);
                    break;
                }

        if (fullSegCoords != null) {
            int from = params.getRange()[0];
            int to = params.getRange()[1];
            float[] segCoords = new float[3 * (to - from)];
            System.arraycopy(fullSegCoords, 3 * from, segCoords, 0, segCoords.length);
            createTubeField(segCoords);
            if (params.isInterpolate()) {
                params.setInterpolate(false);
                InterpolateToMesh.updateOutField(inField, tubeField);
                tubeField.setMask(null);
                DataArray splData = DataArray.create(SplineValueGradient.getSplineValues(inField.getComponent(0),
                                                                                         inField.getDims(),
                                                                                         inField.getAffine()[3],
                                                                                         inField.getInvAffine(), 1,
                                                                                         tubeField.getCurrentCoords() == null ? null : tubeField.getCurrentCoords().getData()),
                                                     1, "spline interpolated density");
                splData.setPreferredRanges(inField.getComponent(0).getPreferredMinValue(), inField.getComponent(0).getPreferredMaxValue(), inField.getComponent(0).getPreferredPhysMinValue(), inField.getComponent(0).getPreferredPhysMaxValue());
                tubeField.addComponent(splData);
                straightTubeField = tubeField.cloneShallow();
                straightTubeField.removeCoords();
                straightTubeField.setAffine(new float[][]{{dl, 0, 0}, {0, dl, 0}, {0, 0, dl}, {0, 0, 0}});
                tubeField.updatePreferredExtents(true);
                setOutputValue("straightTubeField", new VNRegularField(straightTubeField));
                setOutputValue("tubeField", new VNRegularField(tubeField));
            } else
                tubeOutline.setField(tubeField);
        }

        skeletonizerParams.setRecompute(0);
        show();
    }
}
