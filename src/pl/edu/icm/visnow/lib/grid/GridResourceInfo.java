/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import eu.unicore.hila.Resource;
import eu.unicore.hila.exceptions.HiLAException;
import eu.unicore.hila.grid.File;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

//TODO: load at once: lastModified, size, directory/collection and file 
//TODO: add BaseJob.getTaskName() / meta
//TODO: Unicore6Grid.getProperties
//TODO: Unicore6File.getStagingURI() / getPath()
/**
 * Class that stores long / mid / short description of resource; used in grid browser / grid explorer.
 * Currently based only on Resource.getName(), obj.getClass().getName(), Location.getStringValue(), File.isDirectory(), File.size(), File.lastModified()
 * <p>
 * @author szpak
 */
class GridResourceInfo
{

    //greedy info (set in constructor)
    //general name
    private String name = "";
    //java class name
    private String className = "";
    private String uri = "";
    //
    //lazy info (set on demand)
    //size of the file or -1 for collection 
    private Long size = null;
    //last modified date
    private Date date = null;
    //by default every resource is a collection 
    private Boolean collection = null;
    //file reflects eu.unicore.hila.grid.File (there is no .exists() testing)
    private Boolean file = null;
    //indicates if size/date/collection/file are loaded
    private boolean detailsLoaded = false;

    //info strings
    private String shortInfo = null;
    private String midInfo = null;
    private String longInfo = null;

    //init data
    //let's assume that resource cannot be null (provided in constructor)
    Resource resource = null;

    /**
     * Gets info from resource
     */
    public GridResourceInfo(Resource r)
    {
        resource = r;

        initValues(r.getName(),
                   r.getClass().getName(),
                   r.getLocation().getStringValue().toString());
    }

    private void initValues(String name, String className, String uri)
    {
        this.name = name;
        this.className = className;
        this.uri = uri;
    }

    @Override
    public String toString()
    {
        return getShortInfo();
    }

    //TODO: format common output string for short/mid info (11 chars)
    /**
     * @return short name of resource (without [DIR]/[filesize] prefix)
     */
    public String getShortInfo()
    {
        if (shortInfo == null)
            shortInfo = "           " + getName();
        return shortInfo;
    }

    /**
     * @return name with additional [DIR] prefix for all collections/directories and [filesize] for files
     */
    public String getMidInfo()
    {
        //cache last modified even if it's not used
        getLastModified();

        if (midInfo == null) {
            String sizeString = "" + getFileSize();
            if (sizeString.length() < 11)
                sizeString = String.format("%1$11s", sizeString);

            midInfo = (isCollection() ? "      [DIR] " : sizeString + " ") + name;
        }
        return midInfo;
    }

    /**
     * @return generated info in format: lastModifiedDate [DIR]/filesize name java_class Unicore_URI
     */
    public String getLongInfo()
    {
        if (longInfo == null)
            longInfo = (isFile() ? DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(getLastModified()) + " " : "") +
                getMidInfo() +
                "   CLASS: " + className +
                "   URI: " + uri;
        return longInfo;
    }

    /**
     * Uri returned by this method is not "HiLA compatible" and it doesn't work with Location.locate() for some URI like eg.,
     * unicore6:/storages/...; it needs "manual" traversing in this case
     * <p>
     * @return uri taken from Location.getStringValue() - typically sth like unicore6:/sites/... or unicore6:/storages/...
     */
    public String getURI()
    {
        return uri;
    }

    /**
     * Tests if given resource is a collection and can be traversed (so possibly has some children)
     * <p>
     * @param resource to test
     * <p>
     * @return true if resource is a directory or collection (not a file or "file that does not exist" like e.g. "--files--/exports/imports/transfers" in
     *         storages)
     *         false if resource is a no-directory-file
     */
    public static boolean isCollection(Resource resource)
    {
        try {
            if (!(resource instanceof eu.unicore.hila.grid.File) //looks like it's faster without calling .exists() (HiLAExceptin("File does not exists") appears much less frequently)
                //                    || !((eu.unicore.hila.grid.File) resource).exists()
                ||
                ((eu.unicore.hila.grid.File) resource).isDirectory())
                return true;
            else
                return false;
        } catch (HiLAException ex) {
            //"file that does not exist" like e.g. "files/exports/imports/transfers" in storages
            //by default treated like collection
            return true;
        }
    }

    /**
     * @return true if resource is a collection/directory, @see GridResourceInfo#isCollection(Resource resource) isCollection for details
     */
    public boolean isCollection()
    {
        loadDetails(false);
        return collection;
    }

    /**
     * @return true if provided Resource is instanceof eu.unicore.hila.grid.File
     */
    public boolean isFile()
    {
        loadDetails(false);
        return file;
    }

    /**
     * @return size of the file or -1 for collection/directory
     */
    public long getFileSize()
    {
        loadDetails(false);
        return size;
    }

    /**
     * @return date of last modification for file; null for other resources
     */
    public Date getLastModified()
    {
        loadDetails(false);
        if (date == null) {
            //on exception: fake date returned
            Calendar c = Calendar.getInstance();
            c.set(2000, 0, 1);
            return c.getTime();
        }
        return date;
    }

    /**
     * Indicates if details are loaded, so can be read quickly.
     * <p>
     * @return
     */
    public boolean isDetailsLoaded()
    {
        return detailsLoaded;
    }

    private void setDetailsLoaded(boolean detailsLoaded)
    {
        this.detailsLoaded = detailsLoaded;
    }

    /**
     * Loads details like: date, file, collection, filesize; sets detailsLoaded to true (even on failure)
     * <p>
     * @param force if true than refresh details otherwise skip already loaded details
     */
    public void loadDetails(boolean force)
    {
        //load collection             
        if (collection == null || force)
            collection = isCollection(resource); //assuming that resource cannot be null

        //load file
        if (file == null || force)
            file = (resource instanceof File);

        //load date (depends on file)
        if (file && (date == null || force))
            try {
                date = ((File) resource).lastModified();
            } catch (HiLAException ex) {
                date = null;
            }
        if (!file)
            date = null;

        //load file size (depends on collection)
        if (size == null || force) {
            size = -1L; //default size == -1 (for collection and for unknown file
            if (!collection && resource != null)
                try {
                    if (resource instanceof File)
                        size = ((File) resource).size();
                } catch (HiLAException ex) {
                }
        }

        setDetailsLoaded(true);
    }

    public String getName()
    {
        return name;
    }
}
