//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.grid.gridftp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import static javax.swing.JFileChooser.CANCEL_OPTION;
import static javax.swing.JFileChooser.ERROR_OPTION;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import static javax.swing.JFileChooser.DIRECTORIES_ONLY;
import static javax.swing.JFileChooser.FILES_AND_DIRECTORIES;
import static javax.swing.JFileChooser.FILES_ONLY;

import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.log4j.Logger;
import org.globus.ftp.exception.ClientException;
import org.globus.ftp.exception.ServerException;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * Dialog frame around JFileChooser with standard functionality:
 * - showDialog / showOpenDialog / showSaveDialog methods
 * -
 *
 * And additional functionality:
 * - local file system / gridFTP access
 *
 *
 * @author szpak
 */
public class UniversalFileChooser extends javax.swing.JFrame implements ActionListener
{

    private static final Logger LOGGER = Logger.getLogger(UniversalFileChooser.class);

    private enum SelectedFileSystemView
    {

        LOCAL_FS, GRID_FTP
    }

    private SelectedFileSystemView selectedFileSystemView;
    //gridFTP related components/variables
    private GridFTPFileSystemView gridFTPFileSystemView = null;
    private JFileChooser gridFTPFileChooser = null;
    //host port like: qcg.plgrid.icm.edu.pl:2811
    private static final String gridFTPHostnamePlaceholder = "<host>";
    private static final String gridFTPPortPlaceholder = "<port>";
    //dialog that contains this chooser as an only child in contentPane
    private JDialog dialog = null;
    private int returnValue = ERROR_OPTION; //return status for dialog
    //filefilter/files/directories selection
    private FileFilter fileFilter = null;
    private int fileSelectionMode = FILES_ONLY;
    private boolean multiSelectionEnabled = false;
    private File[] selectedFiles = new File[]{};
    private File selectedFile = null;
    //background color to remember
    private Color textFieldDefaultColor = null;
    private Color textFieldHighlightColor = Color.pink;

    /**
     * Creates new form UniversalFileChooser
     */
    public UniversalFileChooser()
    {
        initComponents();
        initFrame();
    }

    /**
     * Additional frame initialization
     */
    private void initFrame()
    {
        final UniversalFileChooser handle = this;
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                gridFTPConnectionPanel.setVisible(false);
                selectedFileSystemView = SelectedFileSystemView.LOCAL_FS;
                gridFTPConnectionHostTF.setText(gridFTPHostnamePlaceholder);
                gridFTPConnectionPortTF.setText(gridFTPPortPlaceholder);
                toggleConnectionForm(false, null);

                //add listener for approve/cancel action
                localFSFileChooser.addActionListener(handle);

                localFSFileChooser.setFileSelectionMode(fileSelectionMode);
                localFSFileChooser.setMultiSelectionEnabled(multiSelectionEnabled);
                localFSFileChooser.setFileFilter(fileFilter);

                //hack to prevent gridFTPConnectionPanel from jumping when toggle URI label
                // (not needed anymore - when console button is added)
                //                gridFTPURILabel.setPreferredSize(gridFTPConnectionPanel.getSize());
                //                gridFTPURILabel.setMinimumSize(gridFTPConnectionPanel.getSize());
                //remember Look&Feel background color
                textFieldDefaultColor = new Color(gridFTPConnectionHostTF.getBackground().getRGB());

                //hide console
                consoleToggle(false);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getActionCommand().equals(JFileChooser.CANCEL_SELECTION)) {
            LOGGER.debug("Cancel");
            returnValue = CANCEL_OPTION;
        } else {
            JFileChooser sourceFileChooser = (JFileChooser) e.getSource();
            //            FileSystemView sourceFileSystemView = sourceFileChooser.getFileSystemView();
            //            String returnFileName;
            //            if (sourceFileSystemView instanceof GridFTPFileSystemView) {
            //                GridFTPFileSystemView gftpfsv = (GridFTPFileSystemView) sourceFileSystemView;
            //                returnFileName = gftpfsv.getConnectionURI() + sourceFileChooser.getSelectedFile().getAbsolutePath();
            //            } else returnFileName = sourceFileChooser.getSelectedFile().getAbsolutePath();
            LOGGER.debug(sourceFileChooser.getSelectedFile().toURI());
            returnValue = APPROVE_OPTION;
            //TODO: single/multiple selection
            selectedFile = sourceFileChooser.getSelectedFile();
            selectedFiles = sourceFileChooser.getSelectedFiles();
        }
        LOGGER.debug(this);
        //TODO: spradzic czy to wystarczy aby sie okno zamknelo - dodatkowo sprawdzic co z polaczeniami
        dialog.setVisible(false);
        //        setVisible(false);
        //        dispose();
    }

    /**
     * getter for user selected file.
     */
    public File getSelectedFile()
    {
        return selectedFile;
    }

    /**
     * getter for user selected files.
     */
    public File[] getSelectedFiles()
    {
        return selectedFiles;
    }

    /**
     * Sets the
     * <code>UniversalFileChooser</code> to allow the user to just
     * select files, just select
     * directories, or select both files and directories. The default is
     * <code>JFilesChooser.FILES_ONLY</code>.
     */
    public void setFileSelectionMode(int mode)
    {
        if (fileSelectionMode == mode) {
            return;
        }

        if ((mode == FILES_ONLY) || (mode == DIRECTORIES_ONLY) || (mode == FILES_AND_DIRECTORIES)) {
            fileSelectionMode = mode;
            localFSFileChooser.setFileSelectionMode(fileSelectionMode);
            if (gridFTPFileChooser != null)
                gridFTPFileChooser.setFileSelectionMode(fileSelectionMode);
        } else {
            throw new IllegalArgumentException("Incorrect Mode for file selection: " + mode);
        }
    }

    /**
     * Sets the file chooser to allow multiple file selections.
     */
    public void setMultiSelectionEnabled(boolean b)
    {
        if (multiSelectionEnabled == b) {
            return;
        }
        multiSelectionEnabled = b;
        localFSFileChooser.setMultiSelectionEnabled(multiSelectionEnabled);
        if (gridFTPFileChooser != null)
            gridFTPFileChooser.setMultiSelectionEnabled(multiSelectionEnabled);
    }

    /**
     * Sets the current file filter. The file filter is used by the
     * file chooser to filter out files from the user's view.
     */
    public void setFileFilter(FileFilter filter)
    {
        if (filter == null && fileFilter == null)
            return;
        if (filter.equals(fileFilter))
            return;
        fileFilter = filter;
        localFSFileChooser.setFileFilter(fileFilter);
        if (gridFTPFileChooser != null)
            gridFTPFileChooser.setFileFilter(fileFilter);
    }

    private void initGridFTP(String host, int port)
    {
        //kill old component / connection / fsView
        if (gridFTPFileSystemView != null)
            try {
                gridFTPFileSystemView.killConnection();
            } catch (Exception ex) {
                LOGGER.warn("Cannot kill connection", ex);
            }
        if (gridFTPFileChooser != null)
            fileChooserPanel.remove(gridFTPFileChooser);

        consoleAppend("Trying to connect to: " + host + ":" + port + "...", true);

        try {
            //add new component / connection /fsView
            //TODO: handle exception
            //        gridFTPFileSystemView = GridFTPFileSystemView.getFileSystemView(host, port);
            gridFTPFileSystemView = GridFTPFileSystemView.createFileSystemView(host, port);
        } catch (Exception ex) {
            LOGGER.error("", ex);
            consoleAppend("ERROR: " + ex, false);
            consoleToggle(true);
            return;
        }

        consoleAppend("...DONE", false);

        gridFTPFileChooser = new JFileChooser(gridFTPFileSystemView);
        gridFTPFileChooser.setFileSelectionMode(fileSelectionMode);
        gridFTPFileChooser.setMultiSelectionEnabled(multiSelectionEnabled);
        gridFTPFileChooser.setFileFilter(fileFilter);

        gridFTPFileChooser.addActionListener(this);
        //////////////refresh button support////////////////
        final JFileChooser fcToRefresh = gridFTPFileChooser;
        final GridFTPFileSystemView fsvToRefresh = gridFTPFileSystemView;

        JComponent refreshPanel = new JPanel(new FlowLayout());
        JButton refreshButton = new JButton("refresh");

        refreshButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //TODO:temporal solution
                fsvToRefresh.wipeOutCache();
                //TODO: fix bug with changing detailed to list view.
                //reread
                //                setFileSystemView(getFileSystemView());
                //                FileChooserUI ui = ((FileChooserUI) UIManager.getUI(fileChooser));
                //                setUI(ui);

                fcToRefresh.updateUI();

                Action details = fcToRefresh.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);
                //                fcToRefresh.set

                //                FileChooserUI ui = ((FileChooserUI)UIManager.getUI(fcToRefresh));
                //                fcToRefresh.setUI(ui);
                //                fcToRefresh.rescanCurrentDirectory();
                //                gridFTPFileChooser.getUI()
                //                setCurrentDirectory(getCurrentDirectory());
            }
        });
        refreshPanel.add(refreshButton);

        gridFTPFileChooser.setAccessory(refreshPanel);
        //
        //        SwingUtilities.invokeLater(new Runnable() {
        //            @Override
        //            public void run() {
        //                fcToRefresh.addActionListener(new ActionListener() {
        //                    @Override
        //                    public void actionPerformed(ActionEvent e) {
        //                        String cmd = e.getActionCommand();
        //                        if (cmd.equals(APPROVE_SELECTION) || cmd.equals(CANCEL_SELECTION))
        //                            try {
        //                                ((GridFTPFileSystemView) fcToRefresh.getFileSystemView()).killConnection();
        //                            } catch (Exception ex) {
        //                                throw new RuntimeException(ex);
        //                            }
        //
        //                    }
        //                });
        //            }
        //        });       
        //        //////////////[end] ^refresh button support////////////////

        java.awt.GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        fileChooserPanel.add(gridFTPFileChooser, gridBagConstraints);
        //        fileChooserPanel.updateUI();
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        fileSystemViewBG = new javax.swing.ButtonGroup();
        fsvSelectionPanel = new javax.swing.JPanel();
        localFSRB = new javax.swing.JRadioButton();
        gridFTPRB = new javax.swing.JRadioButton();
        gridFTPConnectionPanel = new javax.swing.JPanel();
        gridFTPConnectionHostTF = new javax.swing.JTextField();
        gridFTPConnectButton = new javax.swing.JButton();
        gridFTPConnectionPortTF = new javax.swing.JTextField();
        gridFTPURILabel = new javax.swing.JLabel();
        gridFTPConsoleTB = new javax.swing.JToggleButton();
        consoleScroll = new javax.swing.JScrollPane();
        consoleTP = new javax.swing.JTextPane();
        fileChooserPanel = new javax.swing.JPanel();
        localFSFileChooser = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        fsvSelectionPanel.setLayout(new java.awt.GridBagLayout());

        fileSystemViewBG.add(localFSRB);
        localFSRB.setSelected(true);
        localFSRB.setText("Local FS");
        localFSRB.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        localFSRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                localFSRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 20);
        fsvSelectionPanel.add(localFSRB, gridBagConstraints);

        fileSystemViewBG.add(gridFTPRB);
        gridFTPRB.setText("Grid FTP");
        gridFTPRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gridFTPRBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 20);
        fsvSelectionPanel.add(gridFTPRB, gridBagConstraints);

        gridFTPConnectionPanel.setLayout(new java.awt.GridBagLayout());

        gridFTPConnectionHostTF.setText("<host>");
        gridFTPConnectionHostTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gridFTPConnectionSubmit(evt);
            }
        });
        gridFTPConnectionHostTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                connectionInputFocusGained(evt);
            }

            public void focusLost(java.awt.event.FocusEvent evt) {
                connectionInputFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridFTPConnectionPanel.add(gridFTPConnectionHostTF, gridBagConstraints);

        gridFTPConnectButton.setText("connect");
        gridFTPConnectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gridFTPConnectButtonActionPerformed(evt);
            }
        });
        gridFTPConnectButton.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                connectionInputFocusGained(evt);
            }

            public void focusLost(java.awt.event.FocusEvent evt) {
                connectionInputFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridFTPConnectionPanel.add(gridFTPConnectButton, gridBagConstraints);

        gridFTPConnectionPortTF.setText("<port>");
        gridFTPConnectionPortTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gridFTPConnectionSubmit(evt);
            }
        });
        gridFTPConnectionPortTF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                connectionInputFocusGained(evt);
            }

            public void focusLost(java.awt.event.FocusEvent evt) {
                connectionInputFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 10);
        gridFTPConnectionPanel.add(gridFTPConnectionPortTF, gridBagConstraints);

        gridFTPURILabel.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        gridFTPURILabel.setText("gsiftp://host:port");
        gridFTPURILabel.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        gridFTPURILabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                gridFTPURILabelMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridFTPConnectionPanel.add(gridFTPURILabel, gridBagConstraints);

        gridFTPConsoleTB.setText("{}");
        gridFTPConsoleTB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gridFTPConsoleTBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridFTPConnectionPanel.add(gridFTPConsoleTB, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 0, 10);
        fsvSelectionPanel.add(gridFTPConnectionPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(fsvSelectionPanel, gridBagConstraints);

        consoleScroll.setViewportView(consoleTP);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        getContentPane().add(consoleScroll, gridBagConstraints);

        fileChooserPanel.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                fileChooserPanelComponentResized(evt);
            }
        });
        fileChooserPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        fileChooserPanel.add(localFSFileChooser, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(fileChooserPanel, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void gridFTPRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gridFTPRBActionPerformed
        if (selectedFileSystemView != SelectedFileSystemView.GRID_FTP)
            updateSelectedFileSystemView(SelectedFileSystemView.GRID_FTP);
    }//GEN-LAST:event_gridFTPRBActionPerformed

    private void localFSRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_localFSRBActionPerformed
        if (selectedFileSystemView != SelectedFileSystemView.LOCAL_FS)
            updateSelectedFileSystemView(SelectedFileSystemView.LOCAL_FS);
    }//GEN-LAST:event_localFSRBActionPerformed

    /**
     * Switches selectedFileSystemView to {@code sfsv} and then calls {@link #updateSelectedFileSystemView()}
     *
     * @param sfsv
     */
    private void updateSelectedFileSystemView(SelectedFileSystemView sfsv)
    {
        selectedFileSystemView = sfsv;
        updateSelectedFileSystemView();
    }

    /**
     * Updates frame according to current selected file system view (turns on/off necessary panels).
     *
     * Turns off console.
     * ???Resets connection/closes connection if necessary (or starts with empty frame)
     *
     * @param sfsv
     */
    private void updateSelectedFileSystemView()
    {
        consoleToggle(false);
        switch (selectedFileSystemView) {
            case LOCAL_FS:
                gridFTPConnectionPanel.setVisible(false);
                localFSFileChooser.setVisible(true);
                if (gridFTPFileChooser != null)
                    gridFTPFileChooser.setVisible(false);
                localFSFileChooser.grabFocus();
                break;
            case GRID_FTP:
                gridFTPConnectionPanel.setVisible(true);
                localFSFileChooser.setVisible(false);
                if (gridFTPFileChooser != null) {
                    gridFTPFileChooser.setVisible(true);
                    gridFTPFileChooser.grabFocus();
                }
                break;
        }
    }

    private void gridFTPConnectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gridFTPConnectButtonActionPerformed
        gridFTPConnectionSubmit(evt);
    }//GEN-LAST:event_gridFTPConnectButtonActionPerformed

    private void gridFTPURILabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gridFTPURILabelMouseClicked
        //shows label anyway - probably cannot grab focus for an invisible component
        toggleConnectionForm(false, null);
        gridFTPConnectionHostTF.grabFocus();
    }//GEN-LAST:event_gridFTPURILabelMouseClicked

    /**
     * Hides input and shows label when user stops editing hostname/port/button - loses focus.
     */
    private void connectionInputFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_connectionInputFocusLost
        //TODO: show/update label only if connection exists
        if (gridFTPFileChooser != null)
            toggleConnectionForm(true, null);
    }//GEN-LAST:event_connectionInputFocusLost

    /**
     * Shows input and hides label when user starts editing hostname/port/button - gains focus
     */
    private void connectionInputFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_connectionInputFocusGained
        toggleConnectionForm(false, null);
        if (evt.getSource() == gridFTPConnectionHostTF)
            gridFTPConnectionHostTF.selectAll();
        if (evt.getSource() == gridFTPConnectionPortTF)
            gridFTPConnectionPortTF.selectAll();
    }//GEN-LAST:event_connectionInputFocusGained

    //TODO: remove it / switch frame to JComponent -> (access to protected methods) setUI
    private void fileChooserPanelComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_fileChooserPanelComponentResized
    //        LOGGER.debug("");
        //        localFSFileChooser.revalidate();
        //        localFSFileChooser.invalidate();
        //        localFSFileChooser.getUI().rescanCurrentDirectory(localFSFileChooser);
        //        Action details = localFSFileChooser.getActionMap().get("viewTypeDetails");
        //        details.actionPerformed(null);

        //        if (selectedFileSystemView == SelectedFileSystemView.LOCAL_FS)
        //            localFSFileChooser.updateUI();
        //        else
        //            if (gridFTPFileSystemView != null)
        //                gridFTPFileChooser.updateUI();
        this.invalidate();
        this.pack();
    }//GEN-LAST:event_fileChooserPanelComponentResized

    /**
     * Toggles console.
     *
     * @param evt
     */
    private void gridFTPConsoleTBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gridFTPConsoleTBActionPerformed
        consoleToggle(gridFTPConsoleTB.isSelected());
    }//GEN-LAST:event_gridFTPConsoleTBActionPerformed

    /**
     * Validates input fields and tries to connect to passed host/port
     */
    private void gridFTPConnectionSubmit(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gridFTPConnectionSubmit
        toggleConnectionTFHighlight(false, false);

        String hostString = gridFTPConnectionHostTF.getText().trim();
        String portString = gridFTPConnectionPortTF.getText().trim();

        if (hostString.equals(""))
            gridFTPConnectionHostTF.setText(gridFTPHostnamePlaceholder);
        if (portString.equals(""))
            gridFTPConnectionPortTF.setText(gridFTPPortPlaceholder);

        if (hostString.equals(gridFTPHostnamePlaceholder) || hostString.equals("") ||
            portString.equals(gridFTPPortPlaceholder) || portString.equals(""))
            return;

        try {
            int port = Integer.parseInt(portString);
            initGridFTP(hostString, port);
            //TODO: show/update label only on success
            toggleConnectionForm(true, "> " + gridFTPFileSystemView.getConnectionURI());
        } catch (NumberFormatException exception) {
            LOGGER.warn(exception);
            toggleConnectionTFHighlight(false, true);
            //TODO: handle wrong format.
        }
    }//GEN-LAST:event_gridFTPConnectionSubmit

    /**
     * Toggles URI label (and hostTF/portTF/connectButton).
     *
     * @param labelVisible true if label should be visible (textfields/button hidden)
     * @param text         text to update label text - or null to not change the text
     */
    private void toggleConnectionForm(boolean labelVisible, String text)
    {
        gridFTPConnectionHostTF.setVisible(!labelVisible);
        gridFTPConnectionPortTF.setVisible(!labelVisible);
        gridFTPConnectButton.setVisible(!labelVisible);

        gridFTPURILabel.setVisible(labelVisible);
        gridFTPConsoleTB.setVisible(labelVisible);
        if (text != null)
            gridFTPURILabel.setText(text);

        //fix to avoid filechooser's combo box jump over the console        
        if (!labelVisible)
            consoleToggle(false);
    }

    /**
     * Highlights textFields - this is to show invalid text fields to the user.
     */
    private void toggleConnectionTFHighlight(boolean hostTFHighlight, boolean portTFHighlight)
    {
        gridFTPConnectionHostTF.setBackground(hostTFHighlight ? textFieldHighlightColor : textFieldDefaultColor);
        gridFTPConnectionPortTF.setBackground(portTFHighlight ? textFieldHighlightColor : textFieldDefaultColor);
    }

    /**
     * Shows console (and covers current file chooser); or hides console; Additionally updates console toggle button.
     *
     * @param visible
     */
    private void consoleToggle(boolean visible)
    {
        consoleScroll.setVisible(visible);
        gridFTPConsoleTB.setSelected(visible);
    }

    /**
     * Appends {@code text} to console.
     */
    private void consoleAppend(String text, boolean prependEmptyLine)
    {
        consoleTP.setText((prependEmptyLine ? System.getProperty("line.separator") : "") + consoleTP.getText() + text + System.getProperty("line.separator"));
    }

    /*
     * Show dialog - based on JFileChooser.createDialog
     */
    protected JDialog createDialog(Component parent, String title) throws HeadlessException
    {
        //        String title = getTitle();
        //        putClientProperty(AccessibleContext.ACCESSIBLE_DESCRIPTION_PROPERTY,                  title);

        JDialog dialog;

        if (parent == null)
            dialog = new JDialog((Frame) null, title, true);
        else {
            //get root window (Frame / Dialog)
            Component rootWindow = parent;
            while (!(rootWindow instanceof Frame || rootWindow instanceof Dialog))
                rootWindow = rootWindow.getParent();

            if (rootWindow instanceof Frame) {
                dialog = new JDialog((Frame) rootWindow, title, true);
            } else {
                dialog = new JDialog((Dialog) rootWindow, title, true);
            }
        }
        dialog.setComponentOrientation(this.getComponentOrientation());

        Container dialogContentPane = dialog.getContentPane();
        dialogContentPane.setLayout(new BorderLayout());
        dialogContentPane.add(getRootPane(), BorderLayout.CENTER);

        if (JDialog.isDefaultLookAndFeelDecorated()) {
            boolean supportsWindowDecorations
                = UIManager.getLookAndFeel().getSupportsWindowDecorations();
            if (supportsWindowDecorations) {
                dialog.getRootPane().setWindowDecorationStyle(JRootPane.FILE_CHOOSER_DIALOG);
            }
        }

        //        dialog.getRootPane().setDefaultButton(ui.getDefaultButton(this));
        dialog.pack();
        dialog.setLocationRelativeTo(parent);

        return dialog;
    }

    /*
     * Show dialog - based on JFileChooser.showDialog
     */
    public int showDialog(Component parent, String approveButtonText)
    {
        if (dialog != null) {
            // Prevent to show second instance of dialog if the previous one still exists
            return JFileChooser.ERROR_OPTION;
        }

        dialog = createDialog(parent, approveButtonText);

        dialog.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                returnValue = CANCEL_OPTION;
            }
        });
        returnValue = ERROR_OPTION;
        //        rescanCurrentDirectory();

        //TODO: allow resize
        dialog.setResizable(false);
        dialog.setVisible(true);
        //        firePropertyChange("JFileChooserDialogIsClosingProperty", dialog, null);

        // Remove all components from dialog. The MetalFileChooserUI.installUI() method (and other LAFs)
        // registers AWT listener for dialogs and produces memory leaks. It happens when
        // installUI invoked after the showDialog method.
        LOGGER.debug("dispose");
        dialog.getContentPane().removeAll();
        dialog.dispose();
        dialog = null;
        //TODO: spradzic czy to wystarczy aby sie okno zamknelo - dodatkowo sprawdzic co z polaczeniami
        //jak widac sa dwie wersje - zamkniecie z X, Alt-F4; zamkniecie z open/close dialog boxa
        this.dispose();
        return returnValue;
    }

    /**
     * Opens dialog window with 'Open' string as a button and title
     */
    public int showOpenDialog(Component parent)
    {
        return showDialog(parent, "Open");
    }

    /**
     * Opens dialog window with 'Save' string as a button and title
     */
    public int showSaveDialog(Component parent)
    {
        return showDialog(parent, "Save");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        VisNow.initLogging(true);

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    if (false)
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UniversalFileChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UniversalFileChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UniversalFileChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UniversalFileChooser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //        new JFileChooser().showOpenDialog(null);

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                ////dialog with parent frame
                final UniversalFileChooser fileChooser = new UniversalFileChooser();
                //                final JFileChooser fileChooser = new JFileChooser();
                //                final JFrame jFrame = new JFrame("ramka1");
                //                JButton jButton = new JButton("jakis długi guzik");
                //                jFrame.add(jButton);
                //                jFrame.pack();
                //                jFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
                //                jButton.addActionListener(new ActionListener() {
                //                    @Override
                //                    public void actionPerformed(ActionEvent e) {
                //                        universalFileChooser.showOpenDialog(jFrame);
                //                    }
                //                });
                //                jFrame.setVisible(true);

                fileChooser.setFileSelectionMode(FILES_AND_DIRECTORIES);
                fileChooser.setMultiSelectionEnabled(true);
                fileChooser.setFileFilter(new FileNameExtensionFilter("jpg png txt log", "jpg", "png", "txt", "log"));
                int status = fileChooser.showOpenDialog(null);
                LOGGER.debug(status == ERROR_OPTION ? "ERROR_OPTION" : status == CANCEL_OPTION ? "CANCEL_OPTION" : "APPROVE_OPTION");

                //                JFileChooser chooser = new JFileChooser();
                ////                chooser.setMultiSelectionEnabled(true);
                //                LOGGER.info(chooser.showOpenDialog(null));
                File sf = fileChooser.getSelectedFile();
                LOGGER.info((sf == null) + " " + sf);
                LOGGER.info(sf != null ? sf.toURI() : "");
                LOGGER.info(sf != null ? sf.getAbsolutePath() : "");
                try {
                    LOGGER.info(sf != null ? sf.getCanonicalPath() : "");
                } catch (IOException ex) {
                }
                LOGGER.info(sf != null ? sf.getPath() : "");
                File[] sfs = fileChooser.getSelectedFiles();
                LOGGER.info((sfs == null) + " " + Arrays.toString(sfs));
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane consoleScroll;
    private javax.swing.JTextPane consoleTP;
    private javax.swing.JPanel fileChooserPanel;
    private javax.swing.ButtonGroup fileSystemViewBG;
    private javax.swing.JPanel fsvSelectionPanel;
    private javax.swing.JButton gridFTPConnectButton;
    private javax.swing.JTextField gridFTPConnectionHostTF;
    private javax.swing.JPanel gridFTPConnectionPanel;
    private javax.swing.JTextField gridFTPConnectionPortTF;
    private javax.swing.JToggleButton gridFTPConsoleTB;
    private javax.swing.JRadioButton gridFTPRB;
    private javax.swing.JLabel gridFTPURILabel;
    private javax.swing.JFileChooser localFSFileChooser;
    private javax.swing.JRadioButton localFSRB;
    // End of variables declaration//GEN-END:variables
}
