//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import org.apache.log4j.Logger;
import org.globus.ftp.FileInfo;
import org.globus.ftp.MlsxEntry;
import org.globus.ftp.exception.ClientException;
import org.globus.ftp.exception.ServerException;

/**
 * This is simpler version of java.io.File which assumes.
 *
 *
 * @author szpak
 */
public class GridFTPFile extends File
{

    private static final Logger LOGGER = Logger.getLogger(GridFTPFile.class);
    private GridFTPConnection connection;
    //TODO: currently all data is set in constructor - switch to abstract file (so file with relative path resolved to real file in execution time
    //TODO: so allow support for non existing files
    //paths don't contain trailing slash "/" (with exception of root path / root absolute path / root name)
    private String absolutePath; //absolutePath resolved from path (this is done while constructing GridFTPFile object)
    private String name; //name resolved from absolute path (based on last separator position)
    private String parent = null; //parent resolved from absolute path (based on last separator position)
    private boolean exists = false;
    private boolean isDirectory = false;
    private boolean isFile = false;
    private long length = 0l;
    private long lastModified = 0l;
    //unix-type separator is the only one supported here
    public static final String separator = "/";
    /**
     * root(separator) is a root path, root name and root absolute name,
     */
    public static final String root = separator;
    public static final String currentDir = ".";
    public static final String parentDir = "..";

    //root path and name (consistent with requirement that no path ends with separator)
    //    protected final static String innerRootPath = "";
    //    protected final static String innerRootAbsolutePath = "";
    //    protected final static String innerRootName = "";
    //    //root path/name as visible by external classes
    //    protected final static String rootPath = separator;
    //    protected final static String rootAbsolutePath = separator;
    //    protected final static String rootName = separator;
    public GridFTPFile(GridFTPConnection connection, File parent, String fileName, FileInfo fileInfo, MlsxEntry mlsxEntry) throws IOException, ServerException, ClientException
    {
        this(connection, removeTrailingSeparator(parent.getAbsolutePath()) + separator + trimSeparators(fileName), fileInfo, mlsxEntry);
    }

    //TODO: add support for ".." for "."
    public GridFTPFile(GridFTPConnection connection, String path, FileInfo fileInfo, MlsxEntry mlsxEntry) throws IOException, ServerException, ClientException
    {
        super("");

        if (connection == null)
            throw new IllegalArgumentException("Connection cannot be null");

        if (currentDir.equals(path))
            path = connection.getCurrentDir();
        else if (parentDir.equals(path))
            path = findParent(connection.getCurrentDir());

        //store connection (used in parent file creation)
        this.connection = connection;

        LOGGER.debug("connection: " + connection + " path: " + path + " fileinfo: " + fileInfo + " mlsxEntry: " + mlsxEntry);
        //root path
        if (path.equals(root))
            setAsRootFile();
        //non-root file
        else {
            //remove trailing slash
            path = removeTrailingSeparator(path);

            //resolve to absolute path
            if (path.startsWith(root))
                absolutePath = path;
            else
                absolutePath = connection.getCurrentDir() + separator + trimSeparators(path);

            //find parent
            parent = findParent(absolutePath);

            //find name
            name = findName(absolutePath);

            //if empty string then set as non existing file
            if (name.equals("")) {
                LOGGER.trace("Non existing file!");
                setAsNonExistingFile();
                return;
            }
            //find out file details
            //            if (fileInfo == null && mlsxEntry == null) {
            //TODO: ignore fileInfo ? 
            if (mlsxEntry == null) {
                //                try {
                //TODO: move mlst call to FileSystemView? + remove connection from GridFTPFile
                if ((mlsxEntry = MlsxEntryCached.get(this)) == null) {
                    try {
                        mlsxEntry = connection.mlst(path);
                    } catch (ServerException exception) {
                        LOGGER.debug("Error while listing remote file. Setting as non-existing file. ", exception);
                    } catch (IOException exception) {
                        LOGGER.debug("Error while listing remote file. Setting as non-existing file. ", exception);
                    }
                    if (mlsxEntry != null)
                        MlsxEntryCached.put(this, mlsxEntry);
                }
                LOGGER.debug(mlsxEntry);
                if (mlsxEntry == null) {
                    LOGGER.trace("Mlst returned null. Switching to non existing file");
                    setAsNonExistingFile();
                    return;
                }
                //                } catch (Exception ex) {
                //                    //reset connection if cannot read details about the file
                //                    LOGGER.trace("Cannot call mlst. Switching to non existing file: ", ex);
                //                    setAsNonExistingFile();
                //                    return;
                //                }
            }

            //TODO: ignore file info?
            //            if (fileInfo != null) { //get data from fileInfo 
            //                isDirectory = fileInfo.isDirectory();
            //                isFile = fileInfo.isFile();
            //                length = fileInfo.getSize();
            //                try {
            //                    lastModified = new SimpleDateFormat().parse(fileInfo.getDate() + " " + fileInfo.getTime()).getTime();
            //                } catch (ParseException ex) {
            //                    LOGGER.warn("cannot parse: " + fileInfo.getDate() + " " + fileInfo.getTime());
            //                    lastModified = 0l;
            //                }
            //            } else 
            { //get data from mlsxEntry
                String type = mlsxEntry.get(MlsxEntry.TYPE);
                isDirectory = MlsxEntry.TYPE_DIR.equals(type);
                isFile = MlsxEntry.TYPE_FILE.equals(type);

                try {
                    length = Long.parseLong(mlsxEntry.get(MlsxEntry.SIZE));
                } catch (NumberFormatException ex) {
                    LOGGER.error("file length cannot be determined ", ex);
                    length = 0;
                }
                try {
                    lastModified = new SimpleDateFormat("yyyyMMddHHmmss").parse(mlsxEntry.get(MlsxEntry.MODIFY)).getTime();
                } catch (ParseException ex) {
                    LOGGER.error("modify time cannot be determined ", ex);
                    length = 0;
                }

            }
            exists = true;
        }
    }

    private void setAsRootFile()
    {
        name = root;
        absolutePath = root;
        isDirectory = true;
        isFile = false;
        parent = null;
        length = 0;
        lastModified = 0l;
        exists = true;
    }

    private void setAsNonExistingFile()
    {
        isDirectory = false;
        isFile = true;
        length = 0;
        lastModified = 0;
        exists = false;
    }

    static String removeTrailingSeparator(String path)
    {
        return separator.equals(path.substring(path.length() - 1)) ? path.substring(0, path.length() - 1) : path;
    }

    static String removeLeadingSeparator(String path)
    {
        return separator.equals(path.substring(0, 1)) ? path.substring(1) : path;
    }

    static String trimSeparators(String path)
    {
        return removeLeadingSeparator(removeTrailingSeparator(path));
    }

    /**
     * @return name (without separator) for every file
     *         root (with separator) for root
     */
    @Override
    public String getName()
    {
        LOGGER.debug(name);
        return name;
    }

    @Override
    /**
     * @return parent for regular file/dir
     *         null for root
     */
    public String getParent()
    {
        LOGGER.debug(parent);
        return parent;
    }

    /**
     * Finds parent cutting absolutePath from last separator position. This works only with no-root paths. So if rootPath is passed here
     * than it's considered as empty filename at root level (and root is returned).
     *
     * @return root (with separator) if absolutePath == root or parent is root,
     *         parent (without separator) if (parent != root),
     * <p>
     * @throws IllegalArgumentException if passed absolutePath was incorrect (no separator found)
     */
    private static String findParent(String absolutePath)
    {
        if (absolutePath.equals(root))
            return root;

        int index = absolutePath.lastIndexOf(separator);
        if (index == 0)
            return root;
        else if (index > 0)
            return absolutePath.substring(0, index);
        else
            throw new IllegalArgumentException("Absolute path has to contain starting separator");
    }

    /**
     * Finds name cutting absolutePath up to last separator position. AbsolutePath should not end with separator (otherwise empty name is returned).
     *
     * @return regular name resolved from absolute path (last, longest substring without separator)
     *         empty string if absolutePath ends with separator
     */
    private String findName(String absolutePath)
    {
        int index = absolutePath.lastIndexOf(separator);
        if (index == -1)
            throw new IllegalArgumentException("Absolute path has to contain starting separator");
        return absolutePath.substring(index + 1);
    }

    @Override
    public File getParentFile()
    {
        LOGGER.debug("");
        if (parent == null)
            return null;
        else
            try {
                return new GridFTPFile(connection, parent, null, null);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
    }

    @Override
    public String getPath()
    {
        LOGGER.trace(absolutePath);
        return absolutePath;
    }

    /**
     * Assuming that GridFTPFile is always absolute
     *
     * @return
     */
    @Override
    public boolean isAbsolute()
    {
        LOGGER.debug("");
        return true;
    }

    @Override
    public String getAbsolutePath()
    {
        LOGGER.debug(absolutePath);
        return absolutePath;
    }

    /**
     * Assuming that GridFTPFile is always absolute
     *
     * @return
     */
    @Override
    public File getAbsoluteFile()
    {
        LOGGER.debug("");
        return this;
    }

    /**
     * same as getAbsolutePath
     */
    @Override
    public String getCanonicalPath() throws IOException
    {
        LOGGER.debug("");
        return absolutePath;
    }

    /**
     * same as getAbsoluteFile
     */
    @Override
    public File getCanonicalFile() throws IOException
    {
        LOGGER.debug("");
        return this;
    }

    @Override
    public URL toURL() throws MalformedURLException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public URI toURI()
    {
        LOGGER.debug("absolutePath: " + absolutePath);
        try {
            return new URI(connection.getURI() + absolutePath + (isDirectory && !absolutePath.equals(root) ? separator : ""));
        } catch (URISyntaxException ex) {
            throw new RuntimeException("Incorrect absolute path - cannot create URI", ex);
        }
    }

    /**
     * @return true if file/dir exists
     */
    @Override
    public boolean canRead()
    {
        LOGGER.debug("");
        return exists;
    }

    /**
     * Returns always false.
     *
     * @return false
     */
    @Override
    public boolean canWrite()
    {
        LOGGER.debug("");
        return false;
    }

    @Override
    public boolean exists()
    {
        LOGGER.debug(exists);
        return exists;
    }

    @Override
    public boolean isDirectory()
    {
        LOGGER.debug(isDirectory);
        return isDirectory;
    }

    @Override
    public boolean isFile()
    {
        LOGGER.debug(isFile);
        return isFile;
    }

    @Override
    public boolean isHidden()
    {
        LOGGER.debug("");
        return false;
    }

    @Override
    public long lastModified()
    {
        LOGGER.debug("");
        return lastModified;
    }

    @Override
    public long length()
    {
        LOGGER.debug("");
        return length;
    }

    @Override
    public boolean createNewFile() throws IOException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean delete()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteOnExit()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String[] list()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        throw new 
        //        LOGGER.debug("");
        //        return new String[]{};
        //        return super.list(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] list(FilenameFilter filter)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return new String[]{};
        //        return super.list(filter); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File[] listFiles()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return new File[]{};
        //        return super.listFiles(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File[] listFiles(FilenameFilter filter)
    {
        throw new UnsupportedOperationException("Not supported yet.");

        //        LOGGER.debug("");
        //        return new File[]{};
        //        return super.listFiles(filter); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public File[] listFiles(FileFilter filter)
    {
        throw new UnsupportedOperationException("Not supported yet.");

        //        LOGGER.debug("");
        //        return new File[]{};
        //        return super.listFiles(filter); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean mkdir()
    {
        throw new UnsupportedOperationException("Not supported yet.");

        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean mkdirs()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean renameTo(File dest)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setLastModified(long time)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setReadOnly()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setWritable(boolean writable, boolean ownerOnly)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setWritable(boolean writable)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setReadable(boolean readable, boolean ownerOnly)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setReadable(boolean readable)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setExecutable(boolean executable, boolean ownerOnly)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean setExecutable(boolean executable)
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public boolean canExecute()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return false;
    }

    @Override
    public long getTotalSpace()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return 1000000l;
        //        return super.getTotalSpace(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long getFreeSpace()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return 1000000l;
        //        return super.getFreeSpace(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long getUsableSpace()
    {
        throw new UnsupportedOperationException("Not supported yet.");
        //        LOGGER.debug("");
        //        return 1000000l;
        //        return super.getUsableSpace(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Compares absolute paths lexicographically.
     */
    @Override
    public int compareTo(File pathname)
    {
        return absolutePath.compareTo(pathname.getAbsolutePath());
    }

    @Override
    public boolean equals(Object obj)
    {
        return ((GridFTPFile) obj).absolutePath.equals(absolutePath);
    }

    @Override
    public int hashCode()
    {
        return absolutePath.hashCode() ^ 1234321;
    }

    static void wipeOutCache()
    {
        MlsxEntryCached.wipeOut();
    }

    static class MlsxEntryCached
    {

        private static final HashMap<String, MlsxEntryCached> mlsxCache = new HashMap<String, MlsxEntryCached>();
        private static final long maxTime = 3600000l; //3600 seconds (1 hour cache)
        private MlsxEntry mlsxEntry;
        private long timestamp;

        private MlsxEntryCached(MlsxEntry mlsxEntry)
        {
            this.mlsxEntry = mlsxEntry;
            this.timestamp = System.currentTimeMillis();
        }

        static MlsxEntry get(GridFTPFile file)
        {
            String key = generateKey(file);
            MlsxEntryCached entry = mlsxCache.get(key);
            if (entry == null || entry.timestamp + maxTime < System.currentTimeMillis())
                return null;
            return entry.mlsxEntry;
        }

        static void put(GridFTPFile file, MlsxEntry entry)
        {
            String key = generateKey(file);
            mlsxCache.put(key, new MlsxEntryCached(entry));
        }

        private static String generateKey(GridFTPFile file)
        {
            return file.toURI().getPath();//connection.getURI() + file.getAbsolutePath();
        }

        static void wipeOut()
        {
            mlsxCache.clear();
        }
    }
}
