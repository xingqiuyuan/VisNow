package pl.edu.icm.visnow.lib.grid.gridftp;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.globus.common.CoGProperties;
import org.globus.ftp.DataSink;
import org.globus.ftp.DataSource;
import org.globus.ftp.FileRandomIO;
import org.globus.ftp.GridFTPClient;
import org.globus.ftp.GridFTPSession;
import org.globus.ftp.MarkerListener;
import org.globus.ftp.MlsxEntry;
import org.globus.ftp.Session;
import org.globus.ftp.exception.ClientException;
import org.globus.ftp.exception.ServerException;
import org.globus.gsi.GSIConstants;
import org.gridforum.jgss.ExtendedGSSCredential;
import org.gridforum.jgss.ExtendedGSSManager;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import pl.edu.icm.visnow.system.main.VisNow;

//TODO: timeouts should be added to avoid locks when client doesn't response.
/**
 * This class is kind of singleton factory of connection. It creates only one connection for specified host/port.
 * <p>
 * All methods that uses GridFTPClient are synchronized to avoid interrupting one command with another (this is necessary because they act
 * like FTP commands)
 * <p>
 * Connections are set to be passive ones (server in passive mode). They are binary (image) type connections and run in stream mode.
 * <p>
 * Some commands are transfer type commands and need to be refreshed (reset to passive) before each run. Transfer commands include:
 * put, get, extended get, list.
 *
 * @author szpak
 */
public class GridFTPConnection
{

    private static final Logger LOGGER = Logger.getLogger(GridFTPConnection.class);
    private final static Map<String, GridFTPConnection> connections = new HashMap<String, GridFTPConnection>();
    private static GSSCredential gSSCredential = null;
    private static long gSSCredentialLifeTime = 0;
    private GridFTPClient client;
    private String host;
    private int port;
    private String mapKey;
    //certificate paths
    private static final String userCertEnvVar = "X509_CERT_DIR"; //in Windows has to be path of type:  "C:\\\\Users\\\\user_name\\\\.globus\\\\certificates"

    private static final File userHome = new File(System.getProperty("user.home"));
    private static final File userCertDir = new File(new File(userHome, ".globus"), "certificates");

    //fix path used to access certificates
    static {
        String certDir = userCertDir.getAbsolutePath();
        if (VisNow.getOsType() == VisNow.OsType.OS_WINDOWS)
            //first backslash level for java source, second backslash level for regexp third backslash for escape...
            certDir = certDir.replaceAll("\\\\", "\\\\\\\\");

        LOGGER.info("setting " + userCertEnvVar + " to " + certDir);
    }

    private GridFTPConnection(GridFTPClient client, String host, int port)
    {
        this.client = client;
        this.host = host;
        this.port = port;
    }

    /**
     * Returns connection from the pool or if connection for this host/port doesn't exist than create new one. This includes that
     * there may be only one connection to (host, port) pair.
     *
     */
    public static GridFTPConnection getConnection(String host, int port) throws IOException, ServerException, GSSException, CertificateException, GeneralSecurityException, ClientException
    {
        String mapKey = host + ":" + port;

        GridFTPConnection connection = connections.get(mapKey);

        if (connection == null) {
            connection = new GridFTPConnection(initClient(host, port), host, port);
            connection.mapKey = mapKey;

            connections.put(mapKey, connection);
        }
        ;
        return connection;
    }

    /**
     * Returns URI in format gsiftp://host:port.
     */
    String getURI()
    {
        return "gsiftp://" + host + ":" + port;
    }

    /**
     * Creates and sets up new GridFTPClient (connection) for specified host and port;
     * Connection is of binary (image) type and starts in stream mode.
     */
    private static GridFTPClient initClient(String host, int port) throws IOException, ServerException, ClientException, GSSException, CertificateException, GeneralSecurityException
    {
        GridFTPClient gridFTPClient = connect(host, port);
        gridFTPClient.setType(Session.TYPE_IMAGE);
        gridFTPClient.setMode(GridFTPSession.MODE_STREAM);

        setConnectionMode(gridFTPClient);

        return gridFTPClient;
    }

    /**
     * Sets proper connection mode which is (apparently the most stable mode) one-directional mode:
     * server is in passive state, so doesn't connect to client; client is in active state.
     */
    private static void setConnectionMode(GridFTPClient gridFTPClient) throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        gridFTPClient.setPassive();
        gridFTPClient.setLocalActive();
    }

    /**
     * Refreshes connection. This is necessary in data channel commands like: list, put, get
     *
     * @see http://www.globus.org/cog/jftp/guide.html}
     */
    private void refresh() throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        setConnectionMode(client);
    }

    /**
     * Creates GridFTPClient for specified host and port and authenticates connection.
     */
    private static GridFTPClient connect(String host, int port) throws IOException, ServerException, GSSException, CertificateException, GeneralSecurityException
    {
        LOGGER.info("Trying to connect to: " + host + ":" + port);
        GridFTPClient client = new GridFTPClient(host, port);
        client.authenticate(getDefaultCredential());
        return client;
    }

    /**
     * Get proxy data based on default location of user key and certificate files.
     */
    private static byte[] getProxyBytes() throws IOException, GeneralSecurityException
    {
        CoGProperties properties = CoGProperties.getDefault();
        String keyFile = properties.getUserKeyFile();
        String certFile = properties.getUserCertFile();

        GridFTPProxyInit init = new GridFTPProxyInit();

        int bits = 1024;
        int lifetime = 60 * 60 * 12; // 12 hours proxy
        gSSCredentialLifeTime = System.currentTimeMillis() + lifetime * 1000;

        GSIConstants.CertificateType proxyType = GSIConstants.CertificateType.GSI_3_IMPERSONATION_PROXY;

        return init.createProxy(certFile, keyFile, bits, lifetime, proxyType);
    }

    /**
     * Get user credential from proxy data.
     */
    private static GSSCredential getDefaultCredential() throws IOException, GSSException, CertificateException, GeneralSecurityException
    {
        if (gSSCredential == null || gSSCredentialLifeTime < System.currentTimeMillis()) {
            ExtendedGSSManager manager = (ExtendedGSSManager) ExtendedGSSManager.getInstance();
            gSSCredential = manager.createCredential(getProxyBytes(), ExtendedGSSCredential.IMPEXP_OPAQUE, GSSCredential.DEFAULT_LIFETIME, null, GSSCredential.INITIATE_AND_ACCEPT);
        }
        return gSSCredential;
    }

    /**
     * Closes connection (and removes it from the pool).
     */
    public void close() throws IOException, ServerException
    {
        GridFTPConnection.connections.remove(mapKey);
        client.close();
    }
    
    public void abortCurrentTransfer() throws IOException, ServerException 
    {
        client.abort();
    }

    //<editor-fold defaultstate="expanded" desc="GridFTPClient wrappers">
    //GridFTPClient wrappers are necessary to refresh connection before every transfer (list is also a transfer)
    //see: http://www.globus.org/cog/jftp/guide.html
    /**
     * Synchronized wrapper around GridFTPClient.
     */
    public synchronized String getCurrentDir() throws IOException, ServerException
    {
        LOGGER.debug("");
        return GridFTPFile.removeTrailingSeparator(client.getCurrentDir());
    }

    /**
     * Synchronized wrapper around GridFTPClient.
     */
    public synchronized void goUpDir() throws IOException, ServerException
    {
        LOGGER.debug("");
        client.goUpDir();
    }

    /**
     * Synchronized wrapper around GridFTPClient.
     */
    public synchronized void changeDir(String dir) throws IOException, ServerException
    {
        LOGGER.debug(dir);
        client.changeDir(dir);
    }

    /**
     * Synchronized wrapper around GridFTPClient; Refreshes connection before calling client.list.
     *
     * @return list of files in current directory
     */
    public synchronized Vector list() throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        refresh();
        return client.list();
    }

    /**
     * Synchronized wrapper around GridFTPClient.
     */
    public synchronized MlsxEntry mlst(String fileName) throws IOException, ServerException
    {
        LOGGER.debug(fileName);
        return client.mlst(fileName);
    }

    /**
     * Synchronized wrapper around GridFTPClient; Refreshes connection before calling client.put.
     */
    public synchronized void put(String remoteFileName, DataSource source, MarkerListener markerListener) throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        refresh();
        client.put(remoteFileName, source, markerListener);
    }

    /**
     * Synchronized wrapper around GridFTPClient; Refreshes connection before calling client.get.
     */
    public synchronized void get(String remoteFileName, File localFile) throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        refresh();
        client.get(remoteFileName, localFile);
    }

    /**
     * Synchronized wrapper around GridFTPClient; Refreshes connection before calling client.extendedGet.
     */
    public synchronized void extendedGet(String remoteFileName, long offset, long size, DataSink sink, MarkerListener mListener) throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        refresh();
        client.extendedGet(remoteFileName, offset, size, sink, mListener);
    }

    /**
     * Synchronized wrapper around GridFTPClient; Refreshes connection before calling client.extendedGet.
     */
    public synchronized void extendedGet(String remoteFileName, File localFile, long offset, long size, MarkerListener mListener) throws IOException, ServerException, ClientException
    {
        LOGGER.debug("");
        refresh();
        client.extendedGet(remoteFileName, offset, size, new FileRandomIO(new RandomAccessFile(localFile, "rw")), mListener);
    }

    /**
     * Synchronized wrapper around GridFTPClient.
     */
    public synchronized void deleteFile(String fileName) throws IOException, ServerException
    {
        LOGGER.debug("");
        client.deleteFile(fileName);
    }
    //</editor-fold>    
}
