//<editor-fold defaultstate="collapsed" desc=" License ">

/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.grid.gridftp;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 * @author szpak
 */
public class UniversalFileChooserOldTmp extends JFileChooser
{

    public UniversalFileChooserOldTmp()
    {
        super(GridFTPFileSystemView.getFileSystemView());

        final GridFTPFileSystemView fileSystemView = (GridFTPFileSystemView) getFileSystemView();

        final JFileChooser fileChooser = this;

        JComponent refreshPanel = new JPanel(new FlowLayout());
        JButton refreshButton = new JButton("refresh");

        refreshButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                //TODO:temporal solution
                fileSystemView.wipeOutCache();
                //TODO: fix bug with changing detailed to list view.
                //reread
                //                setFileSystemView(getFileSystemView());
                //                FileChooserUI ui = ((FileChooserUI) UIManager.getUI(fileChooser));
                //                setUI(ui);
                updateUI();
                //                setCurrentDirectory(getCurrentDirectory());
            }
        });
        refreshPanel.add(refreshButton);

        this.setAccessory(refreshPanel);

        //        final JFileChooser tChooser = this;
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {

                //                tChooser.setCurrentDirectory(new File("/"));
                //                tChooser.setCurrentDirectory(view.getHomeDirectory());
                //                tChooser.setFileSystemView(view);
                //                tChooser.updateUI();
                addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        String cmd = e.getActionCommand();
                        if (cmd.equals(APPROVE_SELECTION) || cmd.equals(CANCEL_SELECTION))
                            try {
                                ((GridFTPFileSystemView) getFileSystemView()).killConnection();
                            } catch (Exception ex) {
                                throw new RuntimeException(ex);
                            }

                    }
                });
            }
        });
    }

    public static void main(String[] args)
    {
        VisNow.initLogging(true);
        //        JFrame f = new JFrame();

        //        System.out.println("["+"/".substring(1)+"]"); if (true) return;
        //        System.out.println("["+new File("/asdf/asdf/").getName()+"]"); if (true) return;
        //        JFileChooser chooserA = new JFileChooser();
        //        
        ////        chooserA.setMultiSelectionEnabled(true);
        //        chooserA.setFileSelectionMode(DIRECTORIES_ONLY);            
        //        
        //        int codeA = chooserA.showDialog(null, "Chooser A");
        //        System.out.println(codeA == JFileChooser.APPROVE_OPTION ? "APPROVE_OPTION" : codeA == JFileChooser.CANCEL_OPTION ? "CANCEL_OPTION" : "ERROR_OPTION");
        //        System.out.println("Selected file: " + chooserA.getSelectedFile());
        //        System.out.println("Selected files[" + chooserA.getSelectedFiles().length + "]: " + Arrays.toString(chooserA.getSelectedFiles()));
        //        JFileChooser chooser = new JFileChooser();
        //        chooser.setMultiSelectionEnabled(true);
        //        chooser.setFileSelectionMode(FILES_AND_DIRECTORIES);            
        //        chooser.showDialog(null, "Chooser B");
        //       new UniversalFileChooser().showOpenDialog(null);
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                final JFileChooser uFileChooser = new UniversalFileChooserOldTmp();
                uFileChooser.showDialog(null, "Hurray!");
                System.out.println(uFileChooser.getSelectedFile());
            }
        });
    }
}
