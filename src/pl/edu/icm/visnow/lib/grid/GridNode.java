/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import eu.unicore.hila.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Tree structure for caching grid nodes (resources); This class doesn't operate on Resource(s) but just keeps them in nodes.
 *
 * Apart from standard behaviour like add/remove child/parent it also contains
 * ResourceInfo and "loadedChildren" flag
 * which indicates that node's children have been loaded/read (with possibly no
 * children) - loadedChildren should be used and set manually
 *
 * @author szpak
 */
class GridNode
{

    private List<GridNode> children = new ArrayList<GridNode>();
    private GridNode parent = null;
    //TODO: change to abstract Resource, sth like GridResource
    //TODO: and than keep all values (node/resource, info and loadedChildren) in one class/nodeValue
    private Resource nodeValue = null;
    private GridResourceInfo nodeInfo = null;
    private boolean loadedChildren = false;

    public GridNode(Resource value)
    {
        nodeValue = value;
    }

    public Resource getValue()
    {
        return nodeValue;
    }

    public GridResourceInfo getInfo()
    {
        if (nodeInfo == null && getValue() != null)
            nodeInfo = new GridResourceInfo(getValue());

        return nodeInfo;
    }

    public List<GridNode> getChildren()
    {
        return children;
    }

    public List<Resource> getChildrenValues()
    {
        List<Resource> v = new ArrayList<Resource>();
        for (GridNode child : children)
            v.add(child.getValue());
        return v;
    }

    public int getChildrenNum()
    {
        return children.size();
    }

    /**
     * Means that node's children have been loaded/read (possibly no children) - should be used and set manually.
     */
    public boolean hasLoadedChildren()
    {
        return loadedChildren;
    }

    /**
     * Means that node's children have been loaded/read (possibly no children) - should be used and set manually.
     */
    public void setLoadedChildren(boolean loadedChildren)
    {
        this.loadedChildren = loadedChildren;
    }

    public GridNode getParent()
    {
        return parent;
    }

    public void setParent(GridNode parent)
    {
        this.parent = parent;
    }

    public boolean hasParent()
    {
        return getParent() != null;
    }

    public GridNode getRoot()
    {
        if (hasParent())
            return getParent().getRoot();
        else
            return this;
    }

    public void addChild(GridNode child)
    {
        if (child.hasParent())
            child.getParent().removeChild(this);
        child.setParent(this);
        children.add(child);
    }

    public void removeChild(GridNode child)
    {
        child.parent = null;
        children.remove(child);
    }

    public void removeParent()
    {
        parent.removeChild(this);
    }

    /**
     * Removes all children - used before refresh the tree at current level.
     */
    public void removeChildren()
    {
        while (children.iterator().hasNext())
            removeChild(children.iterator().next());
    }
}
