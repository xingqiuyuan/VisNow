/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.grid;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 * Grid command line explorer (using GridExplorer class).
 * Simple application using GridExplorer class.
 * Allows to explore grid tree and export files using shell-like commands (help, cd, ls, pwd, export, ...).
 * ls works "fast" (up to 25 seconds :D )
 * ls -m / ls -l work slower (up to 6 minutes for 700 files per dir) (sic!)
 * Try "help" command for full list.
 * <p>
 * @author szpak
 */
public class GridCommandLineExplorer
{

    private final static Logger LOGGER = Logger.getLogger(GridCommandLineExplorer.class);

    public static void main(String[] args)
    {
        VisNow.initLogging(true);

        GridExplorer explorer;

        // <editor-fold defaultstate="collapsed" desc="EXAMPLE single-file export">   
        //        GridExportTask tmpExportTask = null;
        //        try {
        //            GridExplorer tmpExplorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/met_single_file.007",false,false);
        //            tmpExportTask = tmpExplorer.exportFile(new GridExportTaskObserver() {
        //                @Override
        //                public void updateStatus(GridExportTask task) {
        //                    System.out.print("Transfer: "+task.getLocalFileSize()+"/"+task.getRemoteFileSize()+" "+task.getStatus() + (char)10);
        //                }
        //            },null);
        //        } catch (GridException ex) {
        //            Logger.getLogger(GridCommandLineExplorer.class.getName()).log(Level.SEVERE, null, ex);
        //        }
        //        System.out.println(tmpExportTask.getLocalDirectory()+"/"+tmpExportTask.getFileName());        
        //        if (true) return;
        // </editor-fold>   
        //Grid URI cases
        // 1. proper Unicore URI eg., unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/imagerotate.png
        // 2. mistaken URI that reflects tree structure though, eg., unicore6:/storages/SHARE-ACK/files
        // 3. mistaken URI at schema level, eg., wrong_schema:/asdfasdf/asdf
        // 4. mistaken URI further on, eg., unicore6:/storages/SHARE-ACK/files/asdfasdfasdf12431234/fasdsfasdf
        //
        //1 and 3 are handled more or less correctly by Location.locate
        //4 is loaded like it existed :(
        //2 is not loaded but it is probably cached improperly by HiLA and cannot be refreshed later on :(
        // <editor-fold defaultstate="collapsed" desc="EXAMPLE URIs (valid and invalid ones)">          
        //            explorer = new GridExplorer(null); //GridException
        //            explorer = new GridExplorer("tmp:/"); //GridException can't read
        //            explorer = new GridExplorer("tmp"); //GridException can't read
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/files/asdfasdfasdf12431234/fasdsfasdf"); //Stops at files WARNING (Location not fully loaded)/  OK!
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasksasdfasdf/"); //Stops at ICM-HYDRA_201301181133 WARNING (Location not fully loaded) + sometimes HiLA exception -> Hila.Log4J/  OK!
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/imagerotate.png"); //leaf skipped OK!
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/");
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files");
        //            explorer = new GridExplorer("unicore6:/sites/");
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/");        
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133");
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/");
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/imagerotate.png");
        //
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/files/src");
        //            
        //            explorer = new GridExplorer("unicore6:/");
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/files/src");
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/files/src/special.h");
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/files/asdfasdfasdf12431234/fasdsfasdf");
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/");
        //            explorer = new GridExplorer("unicore6:/storages/");
        //
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasks/f8de2794-3cb1-4f92-a50b-329e77cf8897/wd/files/tmpdir/");
        //            explorer = new GridExplorer("unicore6:/storages/SHARE-ACK/files/aaa");
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/tasksasdfasdf/");
        //
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/storages/SHARE-ICM/files");
        //            explorer = new GridExplorer("unicore6:/sites/ICM-HYDRA_201301181133/");
        // </editor-fold>  
        try {
            System.out.println("Init. Please wait...");
            explorer = new GridExplorer("unicore6:/sites");
            //            explorer = new GridExplorer("unicore6:");
            System.out.println("Init. Done.");
        } catch (Exception e) {
            LOGGER.info("Exception while loading URI: ", e);
            return;
        }

        String helpMsg = "-----------------------------------------------------------\n" +
            "Grid Explorer. Explore grid tree with shell-like commands:\n" +
            "   help, cd num | name, ls [-m, -mid | -l, -long], pwd, refresh [-f, -force], export, exit\n" +
            "-----------------------------------------------------------";

        char[] buffer = new char[1000];
        InputStreamReader systemReader = new InputStreamReader(System.in);

        System.out.println(helpMsg);

        //command loop
        boolean exitCommand = false;

        while (!exitCommand) {
            System.out.print("[ " + explorer.getCurrentInfo().getName() + " ]$ ");

            //read line from system input and splits into tokens            
            String[] userCommand = new String[0];
            try {
                userCommand = new String(buffer, 0, systemReader.read(buffer, 0, buffer.length)).trim().split("\\s+");
            } catch (IOException exception) {
                LOGGER.warn("Can't read user input", exception);
            }

            if (userCommand.length > 0) {
                long milis1 = System.currentTimeMillis();
                String tmpStr = userCommand[0].trim();
                if (tmpStr.equals("help")) {
                    System.out.println(helpMsg);
                } else if (tmpStr.equals("ls")) {
                    System.out.println("CMD: ls");

                    boolean midList = false;
                    boolean longList = false;
                    if (userCommand.length > 1) {
                        String uc1 = userCommand[1].trim();
                        if (uc1.equals("-l") || uc1.equals("-long")) {
                            longList = true;
                        } else if (uc1.equals("-m") || uc1.equals("-mid")) {
                            midList = true;
                        }
                    }
                    List<GridResourceInfo> childrenInfo = explorer.getChildrenInfo();
                    //align first (index) column in printf according to number of children
                    int numChars = ("" + childrenInfo.size()).length();
                    for (int i = 0; i < childrenInfo.size(); i++) {
                        System.out.printf("%1$" + numChars + "d. %2$s\n", i, (longList ? childrenInfo.get(i).getLongInfo() : midList ? childrenInfo.get(i).getMidInfo() : childrenInfo.get(i).getShortInfo()));
                    }
                } else if (tmpStr.equals("pwd")) {
                    System.out.println(explorer.getCurrentInfo().getURI());
                } else if (tmpStr.equals("refresh")) {
                    boolean forceClean = userCommand.length > 1 && (userCommand[1].trim().equals("-f") || userCommand[1].trim().equals("-force"));
                    explorer.reloadChildren(forceClean);
                } else if (tmpStr.equals("export")) {
                    int selectedFile = getUserCommand(userCommand, 0, explorer.getChildrenNum() - 1);
                    if (selectedFile < 0 || explorer.isCollection(selectedFile)) //no file or collection/directory selected
                    {
                        System.out.println("Please select correct file (can't export directory)");
                    } else {
                        System.out.print("Tries to export (asynchronously) to local temporary dir: \n");
                        try {
                            String tmpPath = System.getProperty("java.io.tmpdir");
                            File tmpDir = new File(tmpPath + File.separator + "VisNowTMP");
                            if (!tmpDir.exists()) {
                                tmpDir.mkdirs();
                            }
                            GridExportTask task = explorer.exportChildFile(selectedFile, new GridExportTaskObserver()
                            {
                                public void updateStatus(GridExportTask task)
                                {
                                    System.out.print("Transfer: " + task.getLocalFileSize() + "/" + task.getRemoteFileSize() + " " + task.getStatus() + (char) 10);
                                }
                            }, tmpDir.getAbsolutePath().toString());
                            System.out.println(task.getLocalDirectory() + "/" + task.getFileName());

                        } catch (GridException e) {
                            LOGGER.warn("Problem while exporting file", e);
                        }
                    }
                } else if (tmpStr.equals("cd")) {
                    System.out.println("CMD: cd");
                    int selectedOption = getUserCommand(userCommand, 0, explorer.getChildrenNum() - 1);
                    switch (selectedOption) {
                        case -4: //wrong number or no argument specified
                            System.out.println("Please select correct option");
                            break;
                        case -3: //string as argument
                            //test if leaf but do not reload children
                            //                                explorer.traverseToRelativePath(userCommand[1],true,false);
                            try {
                                explorer.traverseToPath(userCommand[1], true, false);
                            } catch (GridException e) {
                                LOGGER.warn("Can't go to selected path", e);
                            }
                            break;
                        case -2: //root /
                            explorer.traverseToRoot();
                            break;
                        case -1: //parent ..
                            explorer.traverseToParent();
                            break;
                        default: //valid number
                            if (explorer.isCollection(selectedOption)) {
                                explorer.traverseToChild(selectedOption);
                            } else {
                                System.out.println("Selected resource is not a collection/directory");
                            }
                    }
                } else if (tmpStr.equals("exit")) {
                    exitCommand = true;
                    break;

                }
                System.out.println((System.currentTimeMillis() - milis1) / 1000.0 + " seconds");
            }
        }
        System.out.println("Bye!");
    }

    /**
     * Parse user command (argument no 1) which should be the number or filename or slash for root "/" or parent "..".
     *
     * @return integer in range: 0..maxNum; (which is direct parse from userCommand[1] string);
     *         -1 if user typed ".." (which means "1 level up");
     *         -2 if user typed "/" (which means "go to root");
     *         -3 string command / parse int error
     *         -4 otherwise (other/no parameter);
     */
    private static int getUserCommand(String[] userCommand, int minNum, int maxNum)
    {
        if (userCommand.length <= 1)
            return -4;
        String uc = userCommand[1].trim();
        if (uc.equals("/"))
            return -2;
        if (uc.equals(GridExplorer.gridPathParentSymbol))
            return -1;

        int userOption = -1;
        try {
            userOption = Integer.parseInt(uc);
        } catch (NumberFormatException ex) {
            return -3;
        }

        if (userOption < minNum || userOption > maxNum)
            return -4;
        else
            return userOption;
    }
}
