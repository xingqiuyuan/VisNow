//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.gui.ComponentBasedUI.scale;

import pl.edu.icm.jscic.DataContainerSchema;
import pl.edu.icm.jscic.FieldSchema;
import pl.edu.icm.visnow.gui.events.FloatValueModificationEvent;
import pl.edu.icm.visnow.gui.events.FloatValueModificationListener;
import pl.edu.icm.visnow.lib.gui.ComponentBasedUI.ComponentFeature;

/**
 * This parameter class is designed for automatic scaling of componentSchema values to match (usually)
 * with the field geometry extents - for use in graphing functions, glyph scaling and streamlines.
 * 
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ComponentScale extends ComponentFeature
{
    public static final int ALWAYS_RESET   = 0;
    public static final int KEEP_IF_INSIDE = 1;
    public static final int ALWAYS_KEEP    = 2;
    
    public static final int SCALE_BY_AVG   = 0;
    public static final int SCALE_BY_EXT   = 1;
    public static final int SCALE_BY_RMS   = 2;
    public static final int SCALE_BY_MAX   = 3;
    public static final int SCALE_BY_SIGMA = 4;
    
    protected float value = 1;
    
    /**
     * indicates denominator in default value computation
     */
    protected int   scaleBy = SCALE_BY_RMS;
    /**
     * indicates the ratio of extent produced by the default scale defaultValue * componentExtent = scaleTo * geometryExtent
     */
    protected float scaleTo = .01f;
    /**
     * indicates the logarithmic range of scale range slider in UI:
     * the slider will range from default/scaleRange to default*scaleRange
     */
    protected float scaleRange = 10;
    protected float scaleLow   = 1.f/scaleRange;
    protected float scaleUp    = scaleRange;
    
    protected float componentExtent = 1;
    protected float geometryExtent  = 1;
    protected ComponentScaleUI valUI = null;
    protected float initValue = geometryExtent * scaleTo / componentExtent;
    
    protected FloatValueModificationListener uiValueChangedListener = 
           new FloatValueModificationListener()
           {
              @Override
              public void floatValueChanged(FloatValueModificationEvent e)
              {
                 if (componentSchema == null || e.isAdjusting() && !continuousUpdate)
                    return;
                 value = e.getVal();
                 fireStateChanged();
              }
           };
    
    public ComponentScale()
    {
       super();
       policy = KEEP_IF_INSIDE;
    }

    public void setScaleBy(int scaleBy)
    {
        this.scaleBy = scaleBy;
    }

    public ComponentScale scaleBy(int scaleBy)
    {
        this.scaleBy = scaleBy;
        return this;
    }

    public void setScaleRange(float scaleRange)
    {
        this.scaleRange = scaleRange;
    }
    
    public ComponentScale scaleRange(float scaleRange)
    {
        this.scaleRange = scaleRange;
        return this;
    }

    public void setExtent(float extent)
    {
        this.geometryExtent = extent;
    }

    public void setValue(float value)
    {
        if (componentSchema == null)
            return;
        this.value = value;
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    void setValueFromUI(float value)
    {
        if (componentSchema == null)
            return;
        this.value = value;
        if (continuousUpdate || valUI == null || !valUI.isAdjusting())
            fireStateChanged();
    }

    public float getValue()
    {
        return value;
    }

    public float getScaleTo()
    {
        return scaleTo;
    }

    public float getScaleRange()
    {
        return scaleRange;
    }

    public void setScaleTo(float scaleTo)
    {
        this.scaleTo = scaleTo;
        updateComponentScale();
    }

    public float getInitValue()
    {
        return initValue;
    }

    public void updateComponentScale()
    {
        initValue = geometryExtent * scaleTo / componentExtent;
        if (valUI != null)
            valUI.updateRange();
        scaleLow   = initValue / scaleRange;
        scaleUp    = initValue * scaleRange;
        switch (policy)
        {
        case ALWAYS_RESET:
            value = initValue; 
            break;
        case KEEP_IF_INSIDE: 
            if (value < scaleLow || value > scaleUp)
                value = initValue;
            break;
        case ALWAYS_KEEP:
            break;
        }
        if (valUI != null)
            valUI.updateValue();
        fireStateChanged();
    }

    @Override
    public void setComponentSchema(String componentName)
    {
        if (fireOnUpdate) {
            super.setComponentSchema(componentName);
            if (componentSchema == null)
                return;
            switch (scaleBy) {
            case SCALE_BY_AVG:
                componentExtent = (float)componentSchema.getMeanValue();
                break;
            case SCALE_BY_EXT:
                componentExtent = (float)(componentSchema.getPreferredPhysMaxValue() - 
                                          componentSchema.getPreferredPhysMinValue());
                break;
            case SCALE_BY_MAX:
                componentExtent = (float)componentSchema.getPreferredPhysMaxValue();
                break;
            case SCALE_BY_RMS:
                componentExtent = (float)componentSchema.getMeanSquaredValue();
                break;
            case SCALE_BY_SIGMA:
                componentExtent = (float)componentSchema.getStandardDeviationValue();
                break;
            }
            updateComponentScale();
        }
    }

    @Override
    public void setContainerSchema(DataContainerSchema containerSchema)
    {
        fireOnUpdate = false;
        super.setContainerSchema(containerSchema);
        if (containerSchema instanceof FieldSchema)
        {
            float[][] extents = ((FieldSchema)containerSchema).getPreferredExtents();
            double s = 0;
            for (int i = 0; i < extents[0].length; i++)
                s += (extents[1][i] - extents[0][i]) * (extents[1][i] - extents[0][i]);
            geometryExtent = (float)Math.sqrt(s);
        }
        updateComponentScale();
        fireOnUpdate = true;
        fireStateChanged();
    }

    @Override
    public void reset()
    {
        if (componentSchema != null)
            setValue(scaleTo / geometryExtent);
        if (valUI != null)
        {
            valUI.updateRange();
            valUI.updateValue();
        }
    }

    @Override
    protected void localUpdateUI()
    {
       if (ui != null && ui instanceof ComponentScaleUI)
          valUI = (ComponentScaleUI)ui;
       valUI.setListener(uiValueChangedListener);
    }
    
    @Override
    protected void localUpdateComponent()
    {
        if (componentSchema == null)
            return;
        if (oldComponentNameInvalid) 
            setValue(scaleTo / geometryExtent);
        if (valUI == null)
           return;
        valUI.updateRange();
        valUI.updateValue();
    }

}
