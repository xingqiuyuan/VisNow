//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>


package pl.edu.icm.visnow.lib.gui.FieldBasedUI.IndexSliceUI;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class IndexSliceParams 
{
    private float[] position = {0, 0, 0};
    private int axis = 0;
    private boolean oneAxis = false;
    private boolean active = false;
    private boolean adjusting = false;

    /**
     * Get the value of axis
     *
     * @return the value of axis
     */
    
    public int getAxis()
    {
        return axis;
    }

    /**
     * Get the value of position
     *
     * @return the value of position
     */
    public float[] getPosition()
    {
        return position;
    }
    /**
     * Set the value of axis
     *
     * @param axis new value of axis
     */
    public void setAxis(int axis)
    {
        this.axis = axis;
    }

    /**
     * Set the value of position
     *
     * @param position new value of position
     */
    public void setPosition(float[] position)
    {
        this.position = position;
    }

    /**
     * Get the value of adjusting
     *
     * @return the value of adjusting
     */
    public boolean isAdjusting()
    {
        return adjusting;
    }

    /**
     * Set the value of adjusting
     *
     * @param adjusting new value of adjusting
     */
    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isOneAxis()
    {
        return oneAxis;
    }

    public void setOneAxis(boolean oneAxis)
    {
        this.oneAxis = oneAxis;
    }
 
    /**
     * Utility field holding list of global ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Clears ChangeListenerList.
     */
    public synchronized void clearChangeListeners()
    {
        changeListenerList.clear();
    }

    /**
     * Notifies all registered listeners about the event (calls
     * <code>stateChanged()</code> on each listener in
     * <code>changeListenerList</code>).
     */
    public void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (int i = 0; i < changeListenerList.size(); i++) 
                changeListenerList.get(i).stateChanged(e);
        }
    }

}
