//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.gui.FieldBasedUI.SubsetGUI;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author know
 */


public class SubsetParams
{
    public static final int ALWAYS_RESET = 0;
    public static final int KEEP_IF_INSIDE = 1;
    
    protected int policy = KEEP_IF_INSIDE;
    protected long preferredSize = 100;
    protected long minSize = 1;
    protected long maxSize = 10000;
    protected int[] dims;
    protected int[] subsetDims = {10, 10, 10};
    protected int[] minSubsetDims = {1, 1, 1};
    protected int[] maxSubsetDims = {100, 100, 100};
    
    protected long nNodes;
    protected int subsetSize = 100;
    protected boolean regularAvailable = false;
    protected boolean random;
    protected boolean centered = true;
    protected boolean adjusting = false;
    protected boolean active = true;
    protected SubsetUI ui = null;

    public SubsetParams(long minSize, long  maxSize, long preferredSize, boolean random, boolean centered)
    {
        this.preferredSize = preferredSize;
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.random = random;
        this.centered = centered;
    }
    
    public SubsetParams(long minSize, long  maxSize, long preferredSize, boolean random)
    {
        this(preferredSize, minSize, maxSize, random, true);
    }
    
    public SubsetParams(int minSize, int maxSize, boolean random)
    {
        this((int)Math.sqrt(maxSize * minSize), minSize, maxSize, random);
    }
    
    protected void reset()
    {
        if (dims != null) 
            setDefaultDims(dims);
    }

    public SubsetUI getUI()
    {
        return ui;
    }

    public void setUI(SubsetUI ui)
    {
        this.ui = ui;
        ui.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }

    public void setPolicy(int policy)
    {
        this.policy = policy;
    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }
    
    public void setFieldData(long nNodes, int[] dims)
    {
        this.nNodes = nNodes;
        this.dims = dims;
        if (dims != null) {
            if (policy == ALWAYS_RESET) 
                setDefaultDims(dims);
            else {    
                nNodes = 1;
                for (int dim : dims) 
                    nNodes *= dim;
                boolean retain = true;
                if (subsetDims == null || subsetDims.length != dims.length)
                    retain = false;
                else
                    for (int i = 0; i < dims.length; i++) 
                        if (dims[i] < 2 * subsetDims[i])
                            retain = false;
                if (retain) {
                    long nDown = nNodes;
                    for (int d : subsetDims) 
                        nDown /= d;
                    if (nDown < minSize || nDown > maxSize)
                        retain = false;
                }
                if (!retain)
                    setDefaultDims(dims);
            }
        }
        else
            subsetDims = null;
        regularAvailable = dims != null;
        if (ui != null)
            ui.updateFromParams();
        fireStateChanged();
    }
    
    protected void setDefaultDims(int[] dims) {
        subsetDims = new int[dims.length];
        nNodes = 1;
        for (int dim : dims) 
            nNodes *= dim;
        double df = Math.pow((double)preferredSize / nNodes, 1./dims.length);
        double dfMax = Math.pow((double)maxSize / nNodes, 1./dims.length);
        double dfMin = Math.pow((double)minSize / nNodes, 1./dims.length);
        for (int i = 0; i < dims.length; i++) {
            maxSubsetDims[i] = Math.min((int)(dfMax * dims[i]), dims[i]);
            minSubsetDims[i] = Math.max((int)(dfMin * dims[i]), 2);
            if (subsetDims[i] > maxSubsetDims[i] || subsetDims[i] < minSubsetDims[i] || 
                policy == ALWAYS_RESET)
            subsetDims[i] = Math.min(Math.max((int)(df * dims[i]), 2), dims[i]);
        }
    }
    
    public boolean isRegularAvailable()
    {
        return regularAvailable;
    }
    
    public int[] getSubsetDims()
    {
        return subsetDims;
    }

    public int[] getMinSubsetDims()
    {
        return minSubsetDims;
    }

    public int[] getMaxSubsetDims()
    {
        return maxSubsetDims;
    }

    public void setSubsetDims(int[] down)
    {
        this.subsetDims = down;
        if (ui != null)
            ui.updateFromParams();
    }

    public long getPreferredSize()
    {
        return preferredSize;
    }

    public long getMinSize()
    {
        return minSize;
    }

    public long getMaxSize()
    {
        return maxSize;
    }

    public void setPreferredSize(long preferredSize)
    {
        this.preferredSize = preferredSize;
    }

    public void setSizes(long minSize, long maxSize, long preferredSize)
    {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.preferredSize = preferredSize;
    }

    public int getSubsetSize()
    {
        return subsetSize;
    }
    public void setSubsetSize(int subsetSize)
    {
        this.subsetSize = subsetSize;
        fireStateChanged();
    }

    public boolean isRandom()
    {
        return random;
    }

    public void setRandom(boolean random)
    {
        this.random = random;
        fireStateChanged();
    }
    
    public boolean isCentered()
    {
        return centered;
    }

    public void setCentered(boolean centered)
    {
        this.centered = centered;
        fireStateChanged();
    }
    
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty()) {
            return;
        }
        String[] c = s.trim().split(" *:* +");
    }
    
    public String[] valuesToStringArray()
    {
        if (dims != null && subsetDims != null && !random) {
            switch (subsetDims.length) {
                case 1:
                    return new String[] {"random: false", 
                                         String.format("dims: {%3d}", 
                                                       subsetDims[0]),
                                         String.format("centered: %b", centered)};
                case 2:
                    return new String[] {"random: false", 
                                         String.format("dims: {%3d %3d}", 
                                                        subsetDims[0], subsetDims[1]),
                                         String.format("centered: %b", centered)};
                case 3:
                    return new String[] {"random: false", 
                                         String.format("dims: {%3d %3d %3d}", 
                                                        subsetDims[0], subsetDims[1], subsetDims[2]),
                                         String.format("centered: %b", centered)};
            }
        }
        return new String[] {String.format("random: %b", random), 
                             String.format("size: %6d", subsetSize)};
    }
    
    public void restoreFromStringArray(String[] input)
    {
        if (input == null || input.length < 2)
            return;
        try {
            String[] tokens;
            for (String in : input) 
                if (in != null && !in.isEmpty()){
                    tokens = in.trim().toLowerCase().split(" *: +");
                    if (tokens[0].equalsIgnoreCase("random"))
                        random = tokens[1].charAt(0) == 't';
                    else if (tokens[0].equalsIgnoreCase("size"))
                        subsetSize = Integer.parseInt(tokens[1]);
                    else if (tokens[0].equalsIgnoreCase("dims")){
                        String t = tokens[1].replaceAll("\\{|\\}", "");
                        tokens = t.trim().split(" +");
                        subsetDims = new int[tokens.length];
                        for (int i = 0; i < subsetDims.length; i++) 
                            subsetDims[i] = Integer.parseInt(tokens[i]);
                    }
                    else if (tokens[0].equalsIgnoreCase("centered"))
                        centered = tokens[1].charAt(0) == 't';
            }
            if (ui != null)
                ui.updateFromParams();
        } catch (Exception e) {
        }
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
    
    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     */
    protected void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (ChangeListener listener : changeListenerList)
                listener.stateChanged(e);
        }
    }
    
    public static void main(String[] args)
    {
        SubsetParams d = new SubsetParams(1000, 10, 100000, false, true);
        d.setPolicy(ALWAYS_RESET);
        d.setFieldData(10000000, new int[] {50, 1000, 200});
        String[] s = d.valuesToStringArray();
        for (String string : s) 
            System.out.println(string);
        SubsetParams d1 = new SubsetParams(100, 10, 1000, false, true);
        d1.setPolicy(ALWAYS_RESET);
        d1.setFieldData(10000000, new int[] {50, 1000, 200});
        d1.restoreFromStringArray(s);
        String[] s1 = d1.valuesToStringArray();
        for (String string : s1) 
            System.out.println(string);
    }
    
}
