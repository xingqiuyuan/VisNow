//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.gui.FieldBasedUI.DownsizeUI;

import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author know
 */


public class DownsizeParams
{
    public static final int ALWAYS_RESET = 0;
    public static final int KEEP_IF_INSIDE = 1;
    
    protected int policy = KEEP_IF_INSIDE;
    protected long preferredSize;
    protected long minSize;
    protected long maxSize;
    protected int[] dims;
    protected int[] down = {1, 1, 1};
    protected int[] minDown = {1, 1, 1};
    protected int[] maxDown = {100, 100, 100};
    
    protected long nNodes;
    protected int downFactor = 100;
    protected int minDownFactor = 1;
    protected int maxDownFactor = 1000;
    protected boolean regularAvailable = false;
    protected boolean random;
    protected boolean centered = true;
    protected boolean adjusting = false;
    protected boolean active = true;
    protected DownsizeUI ui = null;

    public DownsizeParams(long minSize, long  maxSize, long preferredSize, boolean random, boolean centered)
    {
        this.preferredSize = preferredSize;
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.random = random;
        this.centered = centered;
    }
    
    public DownsizeParams(long minSize, long  maxSize, long preferredSize, boolean random)
    {
        this(preferredSize, minSize, maxSize, random, true);
    }
    
    public DownsizeParams(int minSize, int maxSize, boolean random)
    {
        this((int)Math.sqrt(maxSize * minSize), minSize, maxSize, random);
    }
    
    protected void reset()
    {
        if (dims != null) 
            setDefaultDown(dims);
    }

    public DownsizeUI getUI()
    {
        return ui;
    }

    public void setUI(DownsizeUI ui)
    {
        this.ui = ui;
        ui.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                fireStateChanged();
            }
        });
    }

    public void setPolicy(int policy)
    {
        this.policy = policy;
    }

    public boolean isAdjusting()
    {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting)
    {
        this.adjusting = adjusting;
    }
    
    public void setFieldData(long nNodes, int[] dims)
    {
        this.nNodes = nNodes;
            setDefaultDownsize();
        this.dims = dims;
        if (dims != null) {
            if (policy == ALWAYS_RESET) 
                setDefaultDown(dims);
            else {    
                nNodes = 1;
                regularAvailable = true;
                for (int dim : dims) 
                    nNodes *= dim;
                boolean retain = true;
                if (down == null || down.length != dims.length)
                    retain = false;
                else
                    for (int i = 0; i < dims.length; i++) 
                        if (dims[i] < 2 * down[i])
                            retain = false;
                if (retain) {
                    long nDown = nNodes;
                    for (int d : down) 
                        nDown /= d;
                    if (nDown < minSize || nDown > maxSize)
                        retain = false;
                }
                if (!retain)
                    setDefaultDown(dims);
            }
        }
        regularAvailable = dims != null;
        if (ui != null)
            ui.updateFromParams();
        fireStateChanged();
    }
    
    protected void setDefaultDown(int[] dims) {
        down = new int[dims.length];
        nNodes = 1;
        for (int dim : dims) 
            nNodes *= dim;
        double df = Math.pow(nNodes / (double)preferredSize, 1./dims.length);
        double dfMax = Math.pow(nNodes / (double)minSize, 1./dims.length);
        double dfMin = Math.pow(nNodes / (double)maxSize, 1./dims.length);
        for (int i = 0; i < dims.length; i++) {
            maxDown[i] = (int)Math.min(dfMax, dims[i] / 3);
            minDown[i] = (int)Math.max(dfMin, 1);
            if (down[i] > maxDown[i] || down[i] < minDown[i] || 
                policy == ALWAYS_RESET)
            down[i] = (int)Math.max(Math.min(df, dims[i] / 3), 1);
        }
        minDownFactor = Math.max(1, (int)(nNodes / maxSize));
        maxDownFactor = (int)(nNodes / minSize);
        downFactor = 1;
        for (int d : down) 
            downFactor *= d;
    }
    
    protected void setDefaultDownsize()
    {
        dims = null;
        down = null;
        minDownFactor = Math.max(1, (int)(nNodes / maxSize));
        maxDownFactor = (int)(nNodes / minSize);
        if (downFactor > maxDownFactor || 
            downFactor < minDownFactor || 
            policy == ALWAYS_RESET)
            downFactor = Math.max(1, (int)(nNodes / preferredSize));
    }

    public boolean isRegularAvailable()
    {
        return regularAvailable;
    }
    
    public int[] getDown()
    {
        return down;
    }

    public int[] getMinDown()
    {
        return minDown;
    }

    public int[] getMaxDown()
    {
        return maxDown;
    }

    public void setDown(int[] down)
    {
        this.down = down;
        if (ui != null)
            ui.updateFromParams();
    }

    public long getPreferredSize()
    {
        return preferredSize;
    }

    public long getMinSize()
    {
        return minSize;
    }

    public long getMaxSize()
    {
        return maxSize;
    }

    public void setPreferredSize(long preferredSize)
    {
        this.preferredSize = preferredSize;
    }

    public void setSizes(long minSize, long maxSize, long preferredSize)
    {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.preferredSize = preferredSize;
    }

    public int getDownFactor()
    {
        return downFactor;
    }

    public int getMinDownFactor()
    {
        return minDownFactor;
    }

    public int getMaxDownFactor()
    {
        return maxDownFactor;
    }

    public void setDownFactor(int downFactor)
    {
        this.downFactor = downFactor;
        fireStateChanged();
    }

    public boolean isRandom()
    {
        return random;
    }

    public void setRandom(boolean random)
    {
        this.random = random;
        fireStateChanged();
    }
    
    public boolean isCentered()
    {
        return centered;
    }

    public void setCentered(boolean centered)
    {
        this.centered = centered;
        fireStateChanged();
    }
    
    public void updateFromString(String s)
    {
        s = s.trim();
        if (s == null || s.isEmpty()) {
            return;
        }
        String[] c = s.trim().split(" *:* +");
    }
    
    public String[] valuesToStringArray()
    {
        if (dims != null && down != null && !random) {
            switch (down.length) {
                case 1:
                    return new String[] {"random: false", 
                                         String.format("down: {%3d}", 
                                                       down[0]),
                                         String.format("centered: %b", centered)};
                case 2:
                    return new String[] {"random: false", 
                                         String.format("down: {%3d %3d}", 
                                                        down[0], down[1]),
                                         String.format("centered: %b", centered)};
                case 3:
                    return new String[] {"random: false", 
                                         String.format("down: {%3d %3d %3d}", 
                                                        down[0], down[1], down[2]),
                                         String.format("centered: %b", centered)};
            }
        }
        return new String[] {String.format("random: %b", random), 
                             String.format("downsize: %6d", downFactor)};
    }
    
    public void restoreFromStringArray(String[] input)
    {
        if (input == null || input.length < 2)
            return;
        try {
            String[] tokens;
            for (String in : input) 
                if (in != null && !in.isEmpty()){
                    tokens = in.trim().toLowerCase().split(" *: +");
                    if (tokens[0].equalsIgnoreCase("random"))
                        random = tokens[1].charAt(0) == 't';
                    else if (tokens[0].equalsIgnoreCase("downsize"))
                        downFactor = Integer.parseInt(tokens[1]);
                    else if (tokens[0].equalsIgnoreCase("down")){
                        String t = tokens[1].replaceAll("\\{|\\}", "");
                        tokens = t.trim().split(" +");
                        down = new int[tokens.length];
                        for (int i = 0; i < down.length; i++) 
                            down[i] = Integer.parseInt(tokens[i]);
                    }
                    else if (tokens[0].equalsIgnoreCase("centered"))
                        centered = tokens[1].charAt(0) == 't';
            }
            if (ui != null)
                ui.updateFromParams();
        } catch (Exception e) {
        }
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
    
    /**
     * Utility field holding list of ChangeListeners.
     */
    protected transient ArrayList<ChangeListener> changeListenerList = new ArrayList<ChangeListener>();

    /**
     * Registers ChangeListener to receive events.
     *
     * @param listener The listener to register.
     */
    public synchronized void addChangeListener(ChangeListener listener)
    {
        changeListenerList.add(listener);
    }

    /**
     * Removes ChangeListener from the list of listeners.
     *
     * @param listener The listener to remove.
     */
    public synchronized void removeChangeListener(ChangeListener listener)
    {
        changeListenerList.remove(listener);
    }

    /**
     * Notifies all registered listeners about the event.
     *
     */
    protected void fireStateChanged()
    {
        if (active) {
            ChangeEvent e = new ChangeEvent(this);
            for (ChangeListener listener : changeListenerList)
                listener.stateChanged(e);
        }
    }
    
    public static void main(String[] args)
    {
        DownsizeParams d = new DownsizeParams(100, 10, 1000, false, true);
        d.setPolicy(ALWAYS_RESET);
        d.setFieldData(10000000, new int[] {50, 1000, 200});
        String[] s = d.valuesToStringArray();
        for (String string : s) 
            System.out.println(string);
    }
    
}
