//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.templates.visualization.modules;

import org.apache.log4j.Logger;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.FieldSchema;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.visnow.engine.core.Output;
import pl.edu.icm.visnow.engine.main.ModuleSaturation;
import pl.edu.icm.visnow.geometries.events.ProjectionEvent;
import pl.edu.icm.visnow.geometries.events.ProjectionListener;
import pl.edu.icm.visnow.geometries.objects.*;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.parameters.PresentationParams;
import pl.edu.icm.visnow.geometries.parameters.RenderingParams;
import pl.edu.icm.visnow.geometries.utils.transform.LocalToWindow;
import pl.edu.icm.visnow.lib.templates.visualization.guis.FieldVisualizationGUI;
import pl.edu.icm.visnow.lib.types.VNGeometryObject;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.TimeStamper;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public abstract class OutFieldVisualizationModule extends VisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(OutFieldVisualizationModule.class);

    protected Field outField = null;
    protected FieldSchema lastOutFieldSchema = null;
    protected FieldGeometry fieldGeometry = null;
    protected RegularField outRegularField = null;
    protected RegularFieldGeometry regularFieldGeometry;
    protected IrregularField outIrregularField = null;
    protected IrregularFieldGeometry irregularFieldGeometry;
    protected FieldVisualizationGUI ui;
    protected OpenBranchGroup outGroup = null;
    protected LocalToWindow locToWin = null;
    

    /**
     * Creates a new instance of VisualizationModule
     */
    public OutFieldVisualizationModule()
    {
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new FieldVisualizationGUI();
                ui.getPresentationGUI().setPresentationParams(presentationParams);
            }
        });
        presentationParams.getDataMappingParams().getColorMap0().getComponentRange().setPrefereNull(false);
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                setPanel(ui);
            }
        });
        irregularFieldGeometry = new IrregularFieldGeometry(presentationParams);
        timestamp = TimeStamper.getTimestamp();
        outObj.setName("object" + timestamp);
        
        projectionListener = new ProjectionListener()
        {
            @Override
            public void projectionChanged(ProjectionEvent e)
            {
                locToWin = e.getLocalToWindow();
                if (fieldGeometry != null && fieldGeometry instanceof RegularField3DGeometry)
                    ((RegularField3DGeometry) fieldGeometry).updateProjection(locToWin);
            }
        };
    }
    
    protected synchronized void show()
    {
        if (fieldGeometry == null)
            return;
        outObj.clearGeometries2D();
        if (outField == null) {
            outObj.clearAllGeometry();
            return;
        }
        fieldGeometry.updateGeometry();
        for (Geometry2D geom2D : fieldGeometry.getGeometries2D())
            outObj.addGeometry2D(geom2D);
        if (fieldGeometry instanceof IrregularFieldGeometry)
            for (CellSetGeometry csGeometry
                : ((IrregularFieldGeometry) fieldGeometry).getCellSetGeometries())
                outObj.addGeometry2D(csGeometry.getColormapLegend());

        outObj.setExtents(outField.getPreferredExtents());
    }

    protected synchronized void prepareOutputGeometry()
    {
        outObj.clearAllGeometry();
        outObj.clearGeometries2D();
        if (outField == null)
        {
            SwingInstancer.swingRunAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    ui.hidePresentation();
                }
            });
            return;
        }
        presentationParams.setActive(false);
        String presentationParamString = "";
        if (parameters.getParameter(PresentationParams.VNA_PARAMETER_NAME.getName()) != null)
            presentationParamString = parameters.get(PresentationParams.VNA_PARAMETER_NAME);
        if (irregularFieldGeometry != null)
            outObj.removeBgrColorListener(irregularFieldGeometry.getBackgroundColorListener());
        if (regularFieldGeometry != null)
            outObj.removeBgrColorListener(regularFieldGeometry.getBackgroundColorListener());
        if (outField instanceof IrregularField) {
            outIrregularField = (IrregularField) outField;
            outRegularField = null;
            for (CellSet cs : outIrregularField.getCellSets())
                cs.generateDisplayData(outIrregularField.getCurrentCoords());
            irregularFieldGeometry.setIgnoreUpdate(true);
            irregularFieldGeometry.setData(outIrregularField, presentationParamString);
            irregularFieldGeometry.setIgnoreUpdate(false);
            fieldGeometry = irregularFieldGeometry;
        }

        presentationParams.setInField(outField);
        if (outField instanceof RegularField) {
            boolean isVolumeRenderable = false;
            outIrregularField = null;
            outRegularField = (RegularField) outField;
            int dimCount = outRegularField.getDimNum();
            if (dimCount == 3 && !outRegularField.hasCoords()) {
                dataMappingParams.getTransparencyParams().getComponentRange().setAddNull(false);
                isVolumeRenderable = true;
            }
            if (regularFieldGeometry == null || regularFieldGeometry.getDimension() != dimCount)
                regularFieldGeometry = RegularFieldGeometry.create(outRegularField, presentationParams);
            regularFieldGeometry.setIgnoreUpdate(true);
            if (!presentationParamString.isEmpty()) 
                presentationParams.restorePassivelyValuesFrom(presentationParamString);
            regularFieldGeometry.setFieldDisplayParams(presentationParams);
            regularFieldGeometry.setField(outRegularField);
            regularFieldGeometry.setIgnoreUpdate(false);
            if (isVolumeRenderable && regularFieldGeometry instanceof RegularField3DGeometry)
                lightDirectionListener = 
                        ((RegularField3DGeometry)regularFieldGeometry).getLightDirectionListener();
            fieldGeometry = regularFieldGeometry;
        }
            
//        if (!presentationParamString.isEmpty()) presentationParams.restorePassivelyValuesFrom(presentationParamString);

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui.showPresentation();
            }
        });
        presentationParams.setActive(true);
        outGroup = fieldGeometry.getGeometry();
        outObj.addNode(outGroup);
        outObj2DStruct.addChild(fieldGeometry.getGeometryObj2DStruct());
        outObj.addBgrColorListener(fieldGeometry.getBackgroundColorListener());
    }

    protected void defaultDisplayParams()
    {
        if (outField == null)
            return;

        int mode = 0;
        dataMappingParams.getTransparencyParams().getComponentRange().setAddNull(true);
        if (outField instanceof IrregularField) {
            boolean renderSurface = false;
            for (int i = 0; i < outIrregularField.getNCellSets(); i++)
                if (outIrregularField.getCellSet(i).hasCells2D() ||
                    outIrregularField.getCellSet(i).hasCells3D()) {
                    renderSurface = true;
                    break;
                }
            if (renderSurface)
                presentationParams.getRenderingParams().setDisplayMode(mode | RenderingParams.SURFACE);

            boolean renderEdges = false;
            if (!renderSurface) {
                for (int i = 0; i < outIrregularField.getNCellSets(); i++)
                    if (outIrregularField.getCellSet(i).hasCells1D()) {
                        renderEdges = true;
                        break;
                    }

                if (renderEdges)
                    presentationParams.getRenderingParams().setDisplayMode(mode | RenderingParams.EDGES);
            }

            if (!renderSurface && !renderEdges) {
                boolean renderPoints = false;
                for (int i = 0; i < outIrregularField.getNCellSets(); i++)
                    if (outIrregularField.getCellSet(i).getCellArray(CellType.POINT) != null &&
                        outIrregularField.getCellSet(i).getCellArray(CellType.POINT).getNCells() > 0) {
                        renderPoints = true;
                        break;
                    }
                if (renderPoints)
                    presentationParams.getRenderingParams().setDisplayMode(mode | RenderingParams.POINT_CELLS);

                if (((IrregularField) outField).getNCellDims() == 0)
                    presentationParams.getChild(0).getRenderingParams().setLineThickness(5.0f);
            }
        } else if (outField instanceof RegularField) {
            int[] dims = outRegularField.getDims();
            switch (dims.length) {
                case 3:
                    presentationParams.getRenderingParams().setDisplayMode(mode | RenderingParams.SURFACE);
                    break;
                case 2:
                    presentationParams.getRenderingParams().setDisplayMode(RenderingParams.SURFACE | RenderingParams.IMAGE);
                    break;
                case 1:
                    presentationParams.getRenderingParams().setDisplayMode(RenderingParams.EDGES);
                    break;
            }
        }

    }

    @Override
    public void onLocalSaturationChange(ModuleSaturation mSaturation)
    {
        if (mSaturation == ModuleSaturation.wrongData || mSaturation == ModuleSaturation.noData || mSaturation == ModuleSaturation.notLinked) {
            for (Output output : this.getOutputs()) {
                if (output.getType() == VNGeometryObject.class) {
                    continue;
                }
                output.setValue(null);
            }

            outObj.clearAllGeometry();
            outField = null;
            outRegularField = null;
            outIrregularField = null;
            lastOutFieldSchema = null;
        }
    }

    @Override
    public void onDelete()
    {
        super.onDelete();
        if (parent != null) {
            parent.clearAllGeometry();
            parent = null;
        }
        if (presentationParams != null) {
            presentationParams.clearChildParams();
            presentationParams = null;
        }
        if (dataMappingParams != null) {
            dataMappingParams.clearRenderEventListeners();
            dataMappingParams = null;
        }
        renderingParams = null;
        textureImage = null;
        projectionListener = null;
        if (outObj != null) {
            outObj.clearAllGeometry();
            outObj.clearGeometries2D();
            outObj = null;
        }
        if (outObj2DStruct != null) {
            outObj2DStruct.setGeometryObject2D(null);
            outObj2DStruct.removeAllChildren();
            outObj2DStruct = null;
        }
        if (outField != null) {
            outField.removeComponents();
            outField = null;
        }
        lastOutFieldSchema = null;
        if (fieldGeometry != null) {
            fieldGeometry.clearAllGeometry();
            fieldGeometry.clearGeometries2D();
            fieldGeometry = null;
        }
        outRegularField = null;
        regularFieldGeometry = null;
        outIrregularField = null;
        irregularFieldGeometry = null;
        ui = null;
        if (outGroup != null) {
            outGroup.removeAllChildren();
            outGroup = null;
        }
        locToWin = null;
    }

}
