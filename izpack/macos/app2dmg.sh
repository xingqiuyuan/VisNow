#!/bin/sh

if [ ! $# -eq 2 ] 
then
  echo "usage: app2dmg.sh APPDIR DMGFILE"
  exit
fi

APP_DIR=$1
OUT_DMG=$2
TMP_DIR=/tmp/izpack_tmp
TMP_DMG=/tmp/izpack_dmg

SIZE=`du -s $APP_DIR | awk '{ print $1 }'`
SIZE=`expr $SIZE + 5000`

if [ -f $OUT_DMG ]
then
  rm -f $OUT_DMG
fi

if [ -f $TMP_DMG ]
then
  rm -f $TMP_DMG
fi

dd if=/dev/zero of=$TMP_DMG bs=1k count=$SIZE
/sbin/mkfs.hfsplus -v 'VisNowInstaller' $TMP_DMG || /usr/sbin/mkfs.hfsplus -v 'VisNowInstaller' $TMP_DMG

rm -rf $TMP_DIR
mkdir $TMP_DIR

grep -q "$TMP_DMG" /etc/fstab && mount $TMP_DMG || mount -t hfsplus -o loop $TMP_DMG $TMP_DIR

cp -r $APP_DIR $TMP_DIR
umount $TMP_DMG
cp $TMP_DMG $OUT_DMG
rm -rf $TMP_DIR
