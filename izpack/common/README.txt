VisNow
------------------------

1. SYSTEM REQUIREMENTS
----------------------
In order to run VisNow you need a system platform which is capable of running
Java Virtual Machine in version at least 1.6 with Java3D libraries. This includes 
all modern Linux, Windows XP/2000/Vista and MacOS platforms. 

From a hardware point of view it is very strongly recommended to run VisNow on a 
machine with efficient 3D graphics hardware acceleration (e.g. nVidia GeForce 8800).
Although it is data size dependent, it is also recommended to provide at least 1GB 
of memory.


2. INSTALLATION
---------------
On a properly set up Java environment there is no need to instal anything to run 
VisNow. It is enough to start the software.

To set up full Java environment required by VisNow you need to install the following 
required packages for your operating system:

- Java Runtime Environment (JRE) 1.6 or newer
- Java3D 1.5.1 or newer

VisNow also makes some usage of other optional third-party software:

- ffmpeg (for video encoding)


3. CONFIGURATION
----------------
At the moment the only available parameter for configuration is the maximum amount of 
memory designated to Java Virtual Machine at application startup.
To configure it edit the start.bat or start.sh file for Windows or Linux respectively 
and set the Java parameter "-Xmx" to the proper value. For example option "-Xmx1024M" 
assigns 1025 MB of memory to JVM.


4. RUNNING
----------
To run VisNow use one of the provided start scripst start.bat or start.sh for Windows 
or Linux respectively.

You can also run VisNow manually by executing
	
	java -Xmx1024M -jar VisNow.jar

in VisNow directory.

5. LICENSE
----------
VisNow is an open source software and it may be freely used and distributed.
See LICENSE.TXT for licence details.



