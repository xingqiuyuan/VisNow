#!/bin/bash

heap_size () {
    mem=1024
    case "`uname`" in
        Linux*)
        mem=`cat /proc/meminfo | grep MemTotal | tr -d [:space:][:alpha:]:`
        mem=`expr $mem / 1024`
        ;;
    SunOS*)
        mem=`/usr/sbin/prtconf | grep Memory | /usr/bin/tr -dc '[0-9]'`
        ;;
    Darwin*)
        mem=`/usr/sbin/sysctl hw.memsize | tr -d [:alpha:][:space:].:`
        mem=`expr $mem / 1048576`
        ;;
        *) 
        ;;
    esac
    if [ -z "$mem" ] ; then
        mem=1024
    fi
    mem=`expr $mem \* 4`
    mem=`expr $mem / 5`
    max_heap_size=$mem
    return 0
}

JAVA_HOME=%JAVA_HOME
PATH=$JAVA_HOME/bin:${PATH}
VN_PATH="$( cd "$( dirname "$0" )" && pwd )"
VN_CLASSPATH="${VN_PATH}/VisNow.jar"

heap_size
bits=`java -cp "${VN_CLASSPATH}" pl.edu.icm.visnow.system.main.BitTest`
if [ $bits -eq 32 ] ; then
    if [ $max_heap_size -gt 1400 ] ; then
        max_heap_size=1400
    fi
fi
JVM_XMX=${max_heap_size}M
echo Memory available for VisNow: ${JVM_XMX}B

java -Xmx${JVM_XMX} \
     -cp "${VN_CLASSPATH}" \
     -splash:"${VN_PATH}/splash07.png" \
     $APP-STARTCLASS \
     $@

