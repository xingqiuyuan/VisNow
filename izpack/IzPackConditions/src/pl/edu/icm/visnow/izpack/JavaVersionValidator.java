/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.installer.AutomatedInstallData;
import com.izforge.izpack.installer.DataValidator;
import java.util.Properties;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class JavaVersionValidator implements com.izforge.izpack.installer.DataValidator
{

    @Override
    public Status validateData(AutomatedInstallData aid)
    {
        //System.out.println("");
        //System.out.println("HUHU jvv");
        //System.out.println("");

        String jv = System.getProperty("java.version");
        jv = jv.substring(0, 5);
        int jVersion0 = 0;
        int jVersion1 = 0;
        int jVersion2 = 0;
        jVersion0 = Integer.parseInt(jv.substring(0, 1));
        jVersion1 = Integer.parseInt(jv.substring(2, 3));
        jVersion2 = Integer.parseInt(jv.substring(4, 5));
        if (jVersion0 < 1 || (jVersion0 == 1 && jVersion1 < 6)) {
            return DataValidator.Status.ERROR;
        }
        return DataValidator.Status.OK;
    }

    @Override
    public String getErrorMessageId()
    {
        //return "java.version.validation.error.text";
        return "ERROR!\n\nJava version too low.\nVisNow requires at least Java 1.6.\n\nPlease upgrade Java or contact\nyour system adminstrator.\n\n";
    }

    @Override
    public String getWarningMessageId()
    {
        return "";
    }

    @Override
    public boolean getDefaultAnswer()
    {
        return false;
    }

}
