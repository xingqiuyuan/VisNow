/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.installer.AutomatedInstallData;
import com.izforge.izpack.installer.DataValidator;
import com.izforge.izpack.util.Debug;
import java.io.File;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CustomValidator1 implements com.izforge.izpack.installer.DataValidator
{

    private static final String ERROR_MESSAGE_JAVA_VERSION = "ERROR!\n\nJava version too low.\nVisNow requires at least Java 1.6.\n\nPlease upgrade Java or contact\nyour system adminstrator.\n\n";
    private String errorMessage = "";
    private String warningMessage = "";

    @Override
    public Status validateData(AutomatedInstallData aid)
    {
        Debug.trace("CustomValidator: #1");

        //validate java version        
        String jv = System.getProperty("java.version");
        jv = jv.substring(0, 5);
        int jVersion0 = 0;
        int jVersion1 = 0;
        int jVersion2 = 0;
        jVersion0 = Integer.parseInt(jv.substring(0, 1));
        jVersion1 = Integer.parseInt(jv.substring(2, 3));
        jVersion2 = Integer.parseInt(jv.substring(4, 5));
        if (jVersion0 < 1 || (jVersion0 == 1 && jVersion1 < 6)) {
            Debug.trace("CustomValidator: Java version validation failed");
            errorMessage = ERROR_MESSAGE_JAVA_VERSION;
            return DataValidator.Status.ERROR;
        }
        Debug.trace("CustomValidator: Java version validation succeded");

        //linux application path to /opt
        String os = System.getProperty("os.name");
        if (!os.startsWith("Windows") && !os.startsWith("Mac")) {
            //assume unix
            File f = new File("/opt");
            if (f.exists() && f.canWrite()) {
                Debug.trace("CustomValidator: assumed Unix-like system with /opt write permission. Setting installation path to /opt");
                aid.setVariable("APPLICATIONS_DEFAULT_ROOT", "/opt");
                aid.setVariable("INSTALL_PATH", "/opt/" + aid.getVariable("APP_NAME"));
            }
        }

        return DataValidator.Status.OK;
    }

    @Override
    public String getErrorMessageId()
    {
        return errorMessage;
    }

    @Override
    public String getWarningMessageId()
    {
        return warningMessage;
    }

    @Override
    public boolean getDefaultAnswer()
    {
        return false;
    }

}
