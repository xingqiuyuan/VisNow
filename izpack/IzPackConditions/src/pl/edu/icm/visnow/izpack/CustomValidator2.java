/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.installer.AutomatedInstallData;
import com.izforge.izpack.installer.DataValidator;
import com.izforge.izpack.util.Debug;
import java.io.File;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class CustomValidator2 implements com.izforge.izpack.installer.DataValidator
{

    private static final String ERROR_MESSAGE_JAVA3D_LOCALIZED = "ERROR!\n\nYour system Java3D must be updated\nbut VisNow Installer couldn't resolve library location.\n\nUpdate system Java3D manually to version >=1.5.2\nor contact your system administrator.\n\n";
    private static final String ERROR_MESSAGE_JAVA3D_WRITABLE = "ERROR!\n\nYour system Java3D must be updated.\n\nVisNow Installer requires superuser priviliges\nto perform this update.\n\nUpdate your Java3D manually\nor re-run the installer with administrative priviliges\nor contact your system administrator.\n\n";

    private String errorMessage = "";
    private String warningMessage = "";

    @Override
    public Status validateData(AutomatedInstallData aid)
    {
        //executed only for WIndows in case with search registry
        Debug.trace("CustomValidator: #2");

        Debug.trace("CustomValidator: setting var.java3d.registry=" + System.getenv("JAVA3D_REGISTRY_FOUND"));
        aid.setVariable("var.java3d.registry", System.getenv("JAVA3D_REGISTRY_FOUND"));

        Debug.trace("CustomValidator: setting var.java3d.registry.path=" + System.getenv("JAVA3D_REGISTRY_PATH"));
        aid.setVariable("var.java3d.registry.path", System.getenv("JAVA3D_REGISTRY_PATH"));

        //get other condition states
        boolean cond_java3d = aid.getRules().isConditionTrue("java3d");
        boolean cond_java3d_version = aid.getRules().isConditionTrue("java3d.version");
        boolean cond_java3d_registry = aid.getRules().isConditionTrue("java3d.registry");
        String os = System.getProperty("os.name");

        //if Java3D installed, wrong version and not found in registry
        File f1 = null;
        File f2 = null;
        if (cond_java3d && !cond_java3d_version && !cond_java3d_registry) {
            //localize j3d libraries
            Debug.trace("CustomValidator: searching Java3D libraries to update");

            String java_home = System.getProperty("java.home");
            String java_extdirs = System.getProperty("java.ext.dirs");
            String java_libdirs = System.getProperty("java.library.path");

            Debug.trace("CustomValidator: java.home=" + java_home);
            Debug.trace("CustomValidator: java.ext.dirs=" + java_extdirs);
            Debug.trace("CustomValidator: java.library.path=" + java_libdirs);

            String[] extDirs = java_extdirs.split(File.pathSeparator);
            String[] libDirsTmp = java_libdirs.split(File.pathSeparator);
            String[] libDirs = new String[libDirsTmp.length + 1];
            int recursionLevels = 0;

            //search JARs
            String j3dcoreLocation = null;
            for (int i = 0; i < extDirs.length; i++) {
                j3dcoreLocation = searchDir(extDirs[i], "j3dcore.jar", recursionLevels);
                if (j3dcoreLocation != null)
                    break;
            }
            if (j3dcoreLocation == null)
                j3dcoreLocation = searchDir(java_home, "j3dcore.jar", recursionLevels);

            if (j3dcoreLocation == null) {

                Debug.trace("CustomValidator: 'j3dcore.jar' not found");
                aid.setVariable("var.java3d.localized", "false");

            } else {

                //j3dcore localized, continue
                Debug.trace("CustomValidator: j3dcore.jar found - " + j3dcoreLocation);
                f1 = new File(j3dcoreLocation);
                String j3dDirPath = f1.getParent();
                aid.setVariable("java3d.dir.ext", j3dDirPath);

                libDirs[0] = j3dDirPath + File.separator + "..";
                for (int i = 0; i < libDirsTmp.length; i++) {
                    libDirs[i + 1] = libDirsTmp[i];
                }

                //search library files
                String j3dlibLocation = null;
                String libFile = "libj3dcore-ogl.so";
                if (os.startsWith("Windows")) {
                    libFile = "j3dcore-ogl.dll";
                } else if (os.startsWith("Mac")) {
                    libFile = "libjogl.jnilib";
                }
                for (int i = 0; i < libDirs.length; i++) {
                    if ((".").equals(libDirs[i]))
                        continue;
                    j3dlibLocation = searchDir(libDirs[i], libFile, recursionLevels);
                    if (j3dlibLocation != null)
                        break;
                }
                if (j3dlibLocation == null)
                    j3dlibLocation = searchDir(java_home, libFile, recursionLevels);

                if (j3dlibLocation == null) {
                    if (os.startsWith("Mac")) {
                        Debug.trace("CustomValidator: library file not found - using location: " + j3dDirPath);
                        aid.setVariable("java3d.dir.lib", j3dDirPath);
                        aid.setVariable("var.java3d.localized", "true");

                    } else {
                        Debug.trace("CustomValidator: '" + libFile + "' not found");
                        aid.setVariable("var.java3d.localized", "false");
                    }
                } else {
                    Debug.trace("CustomValidator: " + libFile + " found - " + j3dlibLocation);
                    f2 = new File(j3dlibLocation);
                    String j3dLibPath = f2.getParent();
                    aid.setVariable("java3d.dir.lib", j3dLibPath);
                    aid.setVariable("var.java3d.localized", "true");
                }
            }
        }
        boolean cond_java3d_localized = aid.getRules().isConditionTrue("java3d.localized");

        //if Java3D installed, wrong version, not found in registry and not found in system -> ERROR
        if (cond_java3d && !cond_java3d_version && !cond_java3d_registry && !cond_java3d_localized) {
            errorMessage = ERROR_MESSAGE_JAVA3D_LOCALIZED;
            return DataValidator.Status.ERROR;
        }

        //if Java3D installed, wrong version, not found in registry and found in system -> check if writable
        if (cond_java3d && !cond_java3d_version && !cond_java3d_registry && cond_java3d_localized) {
            //check if java3d libraries writable
            Debug.trace("CustomValidator: checking if Java3D libraries can be overwritten");
            boolean writable = false;
            if (f1 != null && f2 != null) {
                writable = (f1.canWrite() && f2.canWrite());
            } else if (f1 != null && f2 == null) {
                writable = f1.canWrite();
            } else {
                writable = false;
            }

            aid.setVariable("var.java3d.canwrite", (writable ? "true" : "false"));
        }
        boolean cond_java3d_canwrite = aid.getRules().isConditionTrue("java3d.canwrite");

        //if Java3D installed, wrong version, not found in registry, found in system and cannot write -> ERROR
        if (cond_java3d && !cond_java3d_version && !cond_java3d_registry && cond_java3d_localized && !cond_java3d_canwrite) {
            errorMessage = ERROR_MESSAGE_JAVA3D_WRITABLE;
            return DataValidator.Status.ERROR;
        }

        //if Java3D not installed or Java3D installed, wrong version and found in registry -> check if can install
        if (!cond_java3d || (cond_java3d && !cond_java3d_version && cond_java3d_registry)) {
            //check if java3d.caninstall
            //TODO

        }
        boolean cond_java3d_caninstall = aid.getRules().isConditionTrue("java3d.caninstall");

        //if Java3D not installed and cannot install, or Java3D installed, wrong version, found in registry and cannot install -> ERROR
        if ((!cond_java3d || (cond_java3d && !cond_java3d_version && cond_java3d_registry)) && !cond_java3d_caninstall) {
            errorMessage = ERROR_MESSAGE_JAVA3D_WRITABLE;
            return DataValidator.Status.ERROR;
        }

        //if Java3D installed, wrong version, not found in registry, found in system and writable -> WARNING overwrite
        if (cond_java3d && !cond_java3d_version && !cond_java3d_registry && cond_java3d_localized && cond_java3d_canwrite) {
            warningMessage = "WARNING!\n\nVisNow Installer will update system Java3D library to version 1.5.2\n\nThis requires overwritting some Java extension files.\n\nPlease abort installation if you wish to update Jav3D manually.\n\n";
            return DataValidator.Status.WARNING;
        }

        //if Java3D installed, wrong version, found in registry and can install -> WARNING uninstall
        if (cond_java3d && !cond_java3d_version && cond_java3d_registry && cond_java3d_caninstall) {
            warningMessage = "WARNING!\n\nVisNow Installer will update system Java3D library to version 1.5.2\n\nThis requires uninstalling old Java version and installing new one.\n\nPlease abort installation if you wish to update Jav3D manually.\n\n";
            return DataValidator.Status.WARNING;
        }

        return DataValidator.Status.OK;
    }

    @Override
    public String getErrorMessageId()
    {
        return errorMessage;
    }

    @Override
    public String getWarningMessageId()
    {
        return warningMessage;
    }

    @Override
    public boolean getDefaultAnswer()
    {
        return false;
    }

    private String searchDir(String searchIn, String searchFor, int recursionLevels)
    {
        if (searchIn == null || searchFor == null)
            return null;

        File dir = new File(searchIn);
        if (!dir.exists()) {
            return null;
        }

        //System.out.println("CustomValidator: searching for '"+searchFor+"' in: "+dir.getAbsolutePath());
        String[] ls = dir.list();
        //search in dir
        File f;
        for (int i = 0; i < ls.length; i++) {
            if (ls[i] == null || ls[i].length() < 1 || ls[i].equals(".") || ls[i].equals(".."))
                continue;
            f = new File(dir.getAbsolutePath() + File.separator + ls[i]);
            if (f.isFile()) {
                //System.out.println("file: '"+f.getName()+"'");
                //System.out.println("result: "+f.getName().equals(searchFor));
                if (f.getName().equals(searchFor)) {
                    return f.getAbsolutePath();
                }
            }
        }

        //search recursively
        if (recursionLevels == -1 || recursionLevels > 0) {
            String tmp;
            for (int i = 0; i < ls.length; i++) {
                if (ls[i] == null || ls[i].length() < 1 || ls[i].equals(".") || ls[i].equals(".."))
                    continue;
                f = new File(dir.getAbsolutePath() + File.separator + ls[i]);
                if (f.isDirectory()) {
                    if (recursionLevels == -1)
                        tmp = searchDir(f.getAbsolutePath(), searchFor, -1);
                    else
                        tmp = searchDir(f.getAbsolutePath(), searchFor, --recursionLevels);
                    if (tmp != null)
                        return tmp;
                }
            }
        }

        return null;
    }

}
