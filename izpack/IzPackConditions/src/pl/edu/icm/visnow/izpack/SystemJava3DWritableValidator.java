/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.izpack;

import com.izforge.izpack.installer.AutomatedInstallData;
import com.izforge.izpack.installer.DataValidator;
import java.io.File;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl)
 * University of Warsaw, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class SystemJava3DWritableValidator implements com.izforge.izpack.installer.DataValidator
{

    @Override
    public Status validateData(AutomatedInstallData aid)
    {

        String os = System.getProperty("os.name");
        String java_home = System.getProperty("java.home");
        String java_extdirs = System.getProperty("java.ext.dirs");
        String java_libdirs = System.getProperty("java.library.path");
        String[] extDirs = java_extdirs.split(File.pathSeparator);
        String[] libDirs = java_libdirs.split(File.pathSeparator);
        boolean superuser = false;

        System.out.println("");

        //search JARs
        String j3dcoreLocation = null;
        for (int i = 0; i < extDirs.length; i++) {
            j3dcoreLocation = searchDir(extDirs[i], "j3dcore.jar");
            if (j3dcoreLocation != null)
                break;
        }
        j3dcoreLocation = searchDir(java_home, "j3dcore.jar");

        if (j3dcoreLocation == null) {
            System.out.println("j3dcore.jar not found");
            //return DataValidator.Status.ERROR;
            return DataValidator.Status.OK;
        } else {
            System.out.println("j3dcore.jar found: " + j3dcoreLocation);
        }

        File f1 = new File(j3dcoreLocation);
        String j3dDirPath = f1.getParent();
        aid.setVariable("java3d.dir.ext", j3dDirPath);

        //search library files
        String j3dlibLocation = null;
        String libFile = "libj3dcore-ogl.so";
        if (os.startsWith("Windows")) {
            libFile = "j3dcore-ogl.dll";
        } else if (os.startsWith("Mac")) {
            libFile = "libjogl.jnilib";
        }
        for (int i = 0; i < libDirs.length; i++) {
            j3dlibLocation = searchDir(libDirs[i], libFile);
            if (j3dcoreLocation != null)
                break;
        }
        j3dlibLocation = searchDir(java_home, libFile);

        if (j3dlibLocation == null) {
            System.out.println("j3d library not found");
            //return DataValidator.Status.ERROR;
            return DataValidator.Status.OK;
        } else {
            System.out.println("j3d library found: " + j3dlibLocation);
        }

        File f2 = new File(j3dlibLocation);
        String j3dLibPath = f2.getParent();
        aid.setVariable("java3d.dir.lib", j3dLibPath);

        System.out.println("");
        superuser = (f1.canWrite() && f2.canWrite());
        aid.setVariable("java3d.superuser", (superuser ? "true" : "false"));

        return DataValidator.Status.OK;
        //        if(superuser)
        //            return DataValidator.Status.OK;
        //        else
        //            return DataValidator.Status.ERROR;
    }

    @Override
    public String getErrorMessageId()
    {
        //return "java.version.validation.error.text";
        return "";
    }

    @Override
    public String getWarningMessageId()
    {
        return "";
    }

    @Override
    public boolean getDefaultAnswer()
    {
        return false;
    }

    private String searchDir(String searchIn, String searchFor)
    {
        if (searchIn == null || searchFor == null)
            return null;

        File dir = new File(searchIn);
        if (!dir.exists()) {
            return null;
        }

        String[] ls = dir.list();
        //search in dir
        File f;
        for (int i = 0; i < ls.length; i++) {
            if (ls[i] == null || ls[i].length() < 1 || ls[i].equals(".") || ls[i].equals(".."))
                continue;
            f = new File(dir.getAbsolutePath() + File.separator + ls[i]);
            if (f.isFile()) {
                if (f.getName().equals(searchFor)) {
                    return f.getAbsolutePath();
                }
            }
        }

        //search recursively
        String tmp;
        for (int i = 0; i < ls.length; i++) {
            if (ls[i] == null || ls[i].length() < 1 || ls[i].equals(".") || ls[i].equals(".."))
                continue;
            f = new File(dir.getAbsolutePath() + File.separator + ls[i]);
            if (f.isDirectory()) {
                tmp = searchDir(f.getAbsolutePath(), searchFor);
                if (tmp != null)
                    return tmp;
            }
        }
        return null;
    }

}
